﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Entidades
{
    public class Usuario 
    {
        /// <summary>
        /// Llave primaria de la entidad, se manejan llaves simples 
        /// autoincrementales, si se requieren un grupo de campos unicos
        /// se maneja un constraint por medio de la propieda Unique en 
        /// conjunto con UniqueKey para agrupar las propiedades pertenecientes
        /// al constraint.
        /// </summary>
        public override Int64 Id { get; set; }

        ///<summary>
        ///</summary>
        public string Nombre { get; set; }

        ///<summary>
        ///</summary>
        public string Email { get; set; }

        ///<summary>
        ///</summary>
        public string ConfirmacionPassword { get; set; }

        ///<summary>
        ///</summary>
        public Puesto Puesto { get; set; }

        ///<summary>
        ///</summary>
        //[BelongsTo(NotFoundBehaviour = NotFoundBehaviour.Exception, NotNull = true, Fetch = FetchEnum.Select)]
        public Perfil Perfil { get; set; }

        ///<summary>
        ///</summary>
        public string NumeroEmpleado { get; set; }

        ///<summary>
        ///</summary>
        public string UltimoGrado { get; set; }

        ///<summary>
        ///</summary>
        public string AlmaMater { get; set; }

        ///<summary>
        ///</summary>
        public DateTime? Experiencia { get; set; }

        ///<summary>
        ///</summary>
        public DateTime? FechaIngresoEmpresa { get; set; }

        ///<summary>
        ///</summary>
        public string Idiomas { get; set; }

        ///<summary>
        ///</summary>
        public EstadoCivilEnum EstadoCivil { get; set; }

        ///<summary>
        ///</summary>
        public string Telefono { get; set; }

        ///<summary>
        ///</summary>
        public string Extension { get; set; }

        ///<summary>
        ///</summary>
        public string Observaciones { get; set; }

        ///<summary>
        ///</summary>
        public IList<HistorialPassword> HistorialPasswords { get; set; }

        ///<summary>
        ///</summary>
        
        public IList<PermisoUsuarioGrupo> PermisosUsuarioGrupo { get; set; }

        ///<summary>
        ///</summary>
        public IList<PermisoUsuarioDivisionOperativa> PermisosUsuarioDivisionOperativa { get; set; }

        ///<summary>
        ///</summary>
        public DateTime? UltimoAcceso { get; set; }

        ///<summary>
        ///</summary>
        public bool SesionIniciada { get; set; }

        #region Vacaciones
        ///<summary>
        ///</summary>
        
        public virtual IList<DiasVacaciones> DiasVacaciones { get; set; }

        ///<summary>
        ///</summary>
        public DateTime FechaIngreso { get; set; }

        ///<summary>
        ///</summary>
        public int TotalDiasVacaciones { get; set; }

        #endregion

        #region PerformanceManagement

        ///<summary>
        ///</summary>
        public IList<HijoEmpleado> HijosEmpleado { get; set; }

        ///<summary>
        ///</summary>
        public IList<Capacitacion> Capacitaciones { get; set; }

        ///<summary>
        ///</summary>
        public IList<DocumentoDigitalizadoActaAdministrativa> ActasAdministrativas { get; set; }

        ///<summary>
        ///</summary>
        public IList<GrupoEmpleado> GruposEmpleado { get; set; }

        ///<summary>
        ///</summary>
        public IList<Prestacion> Prestaciones { get; set; }

        #endregion

        ///<summary>
        ///</summary>
        [JsonIgnore]
        public HistorialPassword Password
        {
            get
            {
                return (from p in HistorialPasswords
                        where p.Activo
                        select p).FirstOrDefault();
            }
        }

        #region Implementation of IPrincipal

        // TODO: implementar chequeo de seguridad
        public bool IsInRole(string serviceName)
        {
            return false;
        }


        [JsonIgnore]
        public IIdentity Identity { get; set; }

        #endregion

        #region IPrincipal Members

        ///<summary>
        ///</summary>
        [Property(NotNull = true, Unique = true, UniqueKey = "Ubicacion_UniqueKey")]
        public string Username { get; set; }

        #endregion

        ///<summary>
        ///</summary>
        public static Usuario Nuevo()
        {
            var usuarioFirmado = (Usuario)Context.User;
            return new Usuario
            {
                CentroDeBeneficio = usuarioFirmado.CentroDeBeneficio,
                Ubicacion = usuarioFirmado.Ubicacion,
                UsuarioAdd = usuarioFirmado,
                FechaAdd = DateTime.Now,
                UsuarioUMod = usuarioFirmado,
                FechaUMod = DateTime.Now,
                Estatus = EstatusEnum.Activo,
                Propietario = usuarioFirmado,
                Permiso = usuarioFirmado.Permiso,
                Perfil = usuarioFirmado.Perfil,
                FechaIngreso = DateTime.Now
            };
        }

        ///<summary>
        ///</summary>
        public static Usuario Nuevo(Ubicacion ubicacion, CentroDeBeneficio centroDeBeneficio)
        {
            var usuarioFirmado = (Usuario)Context.User;
            return new Usuario
            {
                CentroDeBeneficio = centroDeBeneficio,
                Ubicacion = ubicacion,
                UsuarioAdd = usuarioFirmado,
                FechaAdd = DateTime.Now,
                UsuarioUMod = usuarioFirmado,
                FechaUMod = DateTime.Now,
                Estatus = EstatusEnum.Activo,
                Propietario = usuarioFirmado,
                Permiso = usuarioFirmado.Permiso,
                Perfil = usuarioFirmado.Perfil
            };
        }

        ///<summary>
        ///</summary>
        public static Usuario Nuevo(Ubicacion ubicacion)
        {
            var usuarioFirmado = (Usuario)Context.User;
            return new Usuario
            {
                Ubicacion = ubicacion,
                UsuarioAdd = usuarioFirmado,
                FechaAdd = DateTime.Now,
                UsuarioUMod = usuarioFirmado,
                FechaUMod = DateTime.Now,
                Estatus = EstatusEnum.Activo,
                Propietario = usuarioFirmado,
                Permiso = usuarioFirmado.Permiso,
                Perfil = usuarioFirmado.Perfil
            };
        }

        [JsonIgnore]
        [HasMany(typeof(AutorizacionEspecificacionUsuario), Cascade = ManyRelationCascadeEnum.All,
            Lazy = true, ColumnKey = "Usuario", Table = "AutorizacionEspecificacionUsuario", Inverse = true)]
        public IList<AutorizacionEspecificacionUsuario> PermisosAutorizacion { get; set; }

        public IList<Especificacion> EspecificacionesAutorizadas { get; set; }

        public IList<Especificacion> Especificaciones { get; set; }

        public bool PuedeAutorizar(Especificacion especificacion)
        {
            CargaAutorizaciones();

            return EspecificacionesAutorizadas != null &&
                EspecificacionesAutorizadas.Count > 0 &&
                EspecificacionesAutorizadas.Any(x => x.Id == especificacion.Id);
        }

        public void CargaAutorizaciones()
        {
            if (EspecificacionesAutorizadas != null)
            {
                return;
            }

            Especificaciones = LectorEspecificaciones.Especificaciones;

            if (Puesto.JefeInmediato == null) // es el CEO?
            {
                EspecificacionesAutorizadas = Especificaciones;
            }
            else
            {
                List<Especificacion> especificaciones = new List<Especificacion>();
                if (PermisosAutorizacion != null)
                {
                    especificaciones.AddRange(PermisosAutorizacion
                        .Where(x => x.Estatus == EstatusEnum.Activo)
                        .Select(x => x.ConfiguracionEspecificacion.Especificacion));
                }

                if (Puesto != null && Puesto.PermisosAutorizacion != null)
                {
                    especificaciones.AddRange(Puesto.PermisosAutorizacion
                        .Where(x => x.Estatus == EstatusEnum.Activo)
                        .Select(x => x.ConfiguracionEspecificacion.Especificacion));
                }

                EspecificacionesAutorizadas = especificaciones;
            }
        }
    }
}
