﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BusinessLogic.Entidades
{
    [DataContract(Name = "Entity", Namespace = "")]
    public class Entity
    {
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public List<Attribute> Keys { get; set; }
        [DataMember]
        public List<Attribute> Attributes { get; set; }
        public DAC.DAC odac;

        public Entity()
        {
            odac = new DAC.DAC("");
        }

        public static Entity construct(Entity _incoMsg)
        {
            Entity retVal;
            switch (_incoMsg.EntityName.ToLower())
            {
                case "cliente":
                    retVal = new Cliente();
                    break;
                default:
                    retVal = new Entity();
                    break;
            }
            return retVal;
        }

        public virtual OperationResult InsertRecord()
        {
            OperationResult opResult = odac.InsertRecord(this.Attributes, this.EntityName.ToString());
            return opResult;
        }
        public virtual OperationResult UpdateRecord()
        {
            OperationResult opResult = odac.UpdateRecord(this);
            return opResult;
        }
        public virtual OperationResult DeleteRecord()
        {
            OperationResult opResult = odac.DeleteRecord(this);
            return opResult;
        }
        public virtual OperationResult SelectRecord()
        {
            OperationResult opResult = odac.SelectRecord(this);
            return opResult;
        }
    }
    [DataContract(Name = "Attribute", Namespace = "")]
    public class Attribute
    {
        [DataMember]
        public string AttrName { get; set; }
        [DataMember]
        public string AttrValue { get; set; }
        [DataMember]
        public string AttrType { get; set; }

        public Attribute(string _AttrName)
        {
            AttrName = _AttrName;
        }
        public Attribute(string _AttrName, string _AttrValue)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
        }

        public Attribute(string _AttrName, string _AttrValue, string _AttrType)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
        }
    }
}
