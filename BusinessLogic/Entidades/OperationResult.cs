﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BusinessLogic.Entidades
{
    [DataContract(Name = "OperationResult", Namespace = "")]
    public class OperationResult
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public string ErrMessage { get; set; }
        [DataMember]
        public List<Entity> RetVal { get; set; }
    }
}
