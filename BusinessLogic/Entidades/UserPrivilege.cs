﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Entidades
{
    [DataContract(Name = "UserPrivilege", Namespace = "")]
    public class UserEntity : Entity
    {
        [DataMember]
        public Int64 userId { get; set; }
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        List<PrivilegeByEntity> listOfPrivilege;
        [DataMember]
        List<SharedRecords> listOfSharedRecords;

        private void mapUserData()
        {
            Int64 idVal = 0;
            int intVal = 0;
            foreach (Attribute attr in this.Attributes)
            {
                switch (attr.AttrName.ToLower())
                {
                    case "email":
                        this.Email = attr.AttrValue;
                        break;
                    case "nombre":
                        this.Nombre = attr.AttrValue;
                        break;
                    case "username":
                        this.userName = attr.AttrValue;
                        break;
                    case "id":
                        Int64.TryParse(attr.AttrValue, out idVal);
                        this.userId = idVal;
                        break;
                }
            }
        }

        public override OperationResult SelectRecord()
        {
            OperationResult opResult = base.SelectRecord();
            if (opResult.Success)
            {
                mapUserData();
                this.listOfPrivilege = this.getPrivByentity();
                this.listOfSharedRecords = this.getSharedRecords();
                opResult.RetVal = new List<Entity>();
                opResult.RetVal.Add(this);
            }
            return opResult;
        }

        private List<PrivilegeByEntity> getPrivByentity()
        {
            return odac.getPrivByEntity(this.userId);
        }

        private List<SharedRecords> getSharedRecords()
        {
            return odac.getSharedRecords(this.userId);
        }
    }

    [DataContract(Name = "PrivilegeByEntity", Namespace = "")]
    public class PrivilegeByEntity
    {
        [DataMember]
        public string entity { get; set; }
        [DataMember]
        public int entityPriv { get; set; } //1=Read, 2=Write, 3=Delete
        [DataMember]
        public string scope { get; set; } //1="us", 2="bu", 3="gb"


    }

    [DataContract(Name = "SharedRecords", Namespace = "")]
    public class SharedRecords
    {
        [DataMember]
        public string entity { get; set; }
        [DataMember]
        public int entityPriv { get; set; } //1=Read, 2=Write
        [DataMember]
        public string recordId { get; set; }
        [DataMember]
        public string sharedBy { get; set; }
        [DataMember]
        public string sharedByName { get; set; }
    }
}
