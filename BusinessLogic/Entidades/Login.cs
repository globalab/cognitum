﻿using BusinessLogic.Crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Entidades
{
    [DataContract(Name = "Login", Namespace = "")]
    public class Login: Entity
    {
        /// <summary>
        /// Llave primaria de la entidad, se manejan llaves simples 
        /// autoincrementales, si se requieren un grupo de campos unicos
        /// se maneja un constraint por medio de la propieda Unique en 
        /// conjunto con UniqueKey para agrupar las propiedades pertenecientes
        /// al constraint.
        /// </summary>
        [DataMember]
        public Int64 Id { get; set; }

        ///<summary>
        ///</summary>
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public EstatusEnum Estatus { get; set; }
        ///<summary>
        ///</summary>
        [DataMember]
        public DateTime? UltimoAcceso { get; set; }

        ///<summary>
        ///</summary>
        [DataMember]
        public bool SesionIniciada { get; set; }

        public OperationResult LoginUser(string username, string password, string ubicacion)
        {

            Login usuario = ObtenerPorUsername(username, ubicacion);
            if (usuario == null)
            {
                throw new SecurityException("El usuario no existe.");
            }
            if (usuario.Estatus == EstatusEnum.Inactivo)
            {
                throw new SecurityException("El usuario esta inactivo.");
            }
            if (usuario.Estatus == EstatusEnum.Eliminado)
            {
                throw new SecurityException("El usuario ha sido eliminado.");
            }

            
            Cipher Cifrado = new Cipher();
            var cif = Cifrado.CifrarCadena(password);
            //if (!usuario.Password.Equals(cif))
            //{
                
            //    throw new SecurityException("Contraseña incorrecta.");
            //}
            if (usuario.SesionIniciada)
            {
                
                throw new SecurityException("El usuario ya se encuentra firmado.");
            }

            usuario.UltimoAcceso = DateTime.Now;
            OperationResult retval = new OperationResult();
            retval.Success = true;
            retval.RetVal = new List<Entity>();
            retval.RetVal.Add(usuario);
            return retval;
        }
        public string Password
        {
            get
            {
                return ""; //Password.GetUserPassword();
            }
        }

        public Login(int _Id,string _Username,string _Nombre,bool _SesionIniciada)
        {
            Id = _Id;
            Username = _Username;
            Nombre = _Nombre;
            SesionIniciada = _SesionIniciada;
        }

        public Login()
        {
        }

        private Login ObtenerPorUsername(string UserName, string Ubicacion)
        {
            Login Usr = new Login();
            Entity User = new Entity();
            User.EntityName = "Usuario";
            User.Attributes = new List<Attribute>();
            User.Attributes.Add(new Attribute("Id"));
            User.Attributes.Add(new Attribute("Username"));
            User.Attributes.Add(new Attribute("Nombre"));
            User.Attributes.Add(new Attribute("SesionIniciada"));
            //User.Attributes.Add(new Attribute("Ubicacion"));
            User.Keys = new List<Attribute>();
            User.Keys.Add(new Attribute("Username", UserName, "string"));
            User.Keys.Add(new Attribute("Ubicacion", "2185", "int"));
            OperationResult res = User.SelectRecord();

            if (res.Success)
            {
                Entity fetchedUsr = res.RetVal.First();
                foreach(Attribute atr in fetchedUsr.Attributes)
                {
                    
                    Usr.GetType().GetProperty(atr.AttrName).SetValue(Usr, getAttrVal(atr.AttrValue,atr.AttrType));
                }
                
            }
            

            return Usr;
            
        }

        public object getAttrVal(string AttVal, string AttrType)
        {
            object retval = null;
            switch (AttrType)
            {
                case "string":
                    retval = AttVal as object;
                    break;
                case "int":
                    int ivalue;
                    int.TryParse(AttVal, out ivalue);
                    retval = ivalue as object;
                    break;
                case "datetime":
                    DateTime datevalue;
                    DateTime.TryParse(AttVal,out datevalue);
                    retval = datevalue as object;
                    break;
                case "int64":
                    Int64 i64val;
                    Int64.TryParse(AttVal, out i64val);
                    retval = i64val as object;
                    break;
                case "long":
                    long lvalue;
                    long.TryParse(AttVal, out lvalue);
                    retval = lvalue as object;
                    break;
                case "decimal":
                    decimal dvalue;
                    decimal.TryParse(AttVal, out dvalue);
                    retval = dvalue as object;
                    break;
                case "float":
                    float fval;
                    float.TryParse(AttVal, out fval);
                    retval = fval as object;
                    break;

            }

            return retval;
        }
    }
}
