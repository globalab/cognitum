﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Crypto
{
 
    public enum CryptoProvider
    {
        DES,
        TripleDES,
        RC2,
        Rijndael
    }
}
