﻿
using System;
using System.Security.Cryptography;

namespace BusinessLogic.Crypto
{
    internal class CryptoServiceProvider
    {
        private readonly CryptoProvider _algorithm;
        private readonly CryptoAction _cAction;

        internal CryptoServiceProvider(CryptoProvider alg, CryptoAction action)
        {
            this._algorithm = alg;
            this._cAction = action;
        }

        internal System.Security.Cryptography.ICryptoTransform GetServiceProvider(byte[] Key, byte[] IV)
        {
            System.Security.Cryptography.ICryptoTransform transform = null;
            switch (this._algorithm)
            {
                case CryptoProvider.DES:
                    {
                        System.Security.Cryptography.DESCryptoServiceProvider provider = new System.Security.Cryptography.DESCryptoServiceProvider();
                        switch (this._cAction)
                        {
                            case CryptoAction.Encrypt:
                                return provider.CreateEncryptor(Key, IV);

                            case CryptoAction.Desencrypt:
                                return provider.CreateDecryptor(Key, IV);
                        }
                        return transform;
                    }
                case CryptoProvider.TripleDES:
                    {
                        System.Security.Cryptography.TripleDESCryptoServiceProvider provider2 = new System.Security.Cryptography.TripleDESCryptoServiceProvider();
                        switch (this._cAction)
                        {
                            case CryptoAction.Encrypt:
                                return provider2.CreateEncryptor(Key, IV);

                            case CryptoAction.Desencrypt:
                                return provider2.CreateDecryptor(Key, IV);
                        }
                        return transform;
                    }
                case CryptoProvider.RC2:
                    {
                        System.Security.Cryptography.RC2CryptoServiceProvider provider3 = new System.Security.Cryptography.RC2CryptoServiceProvider();
                        switch (this._cAction)
                        {
                            case CryptoAction.Encrypt:
                                return provider3.CreateEncryptor(Key, IV);

                            case CryptoAction.Desencrypt:
                                return provider3.CreateDecryptor(Key, IV);
                        }
                        return transform;
                    }
                case CryptoProvider.Rijndael:
                    {
                        System.Security.Cryptography.Rijndael rijndael = new System.Security.Cryptography.RijndaelManaged();
                        switch (this._cAction)
                        {
                            case CryptoAction.Encrypt:
                                return rijndael.CreateEncryptor(Key, IV);

                            case CryptoAction.Desencrypt:
                                return rijndael.CreateDecryptor(Key, IV);
                        }
                        return transform;
                    }
            }
            throw new System.Security.Cryptography.CryptographicException("Error al inicializar al proveedor de cifrado");
        }

    }
}
