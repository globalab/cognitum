﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public enum EstatusEnum
    {
        ///<summary>
        ///</summary>
        
        Inactivo = 0,
        ///<summary>
        ///</summary>
        
        Activo = 1,
        ///<summary>
        ///</summary>
        
        Eliminado = 2
    }
}
