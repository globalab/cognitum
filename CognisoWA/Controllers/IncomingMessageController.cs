﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAC.Entidades;
using System.Configuration;

namespace CognisoWA.Controllers
{
    public class IncomingMessageController : ApiController
    {
        [Route("api/IncomingMessage/GetList")]
        [HttpPost]
        public List<OperationResult> GetList(List<Entity> _entities)
        {
            List<OperationResult> listResult = new List<OperationResult>();
            foreach (Entity entity in _entities)
            {
                Entity tmpEntity = entity;
                OperationResult opResult = tmpEntity.SelectRecord(entity.useAuditable);
                if (opResult.Success && opResult.RetVal.Count > 0)
                {
                    listResult.Add(opResult);
                }
            }

            return listResult; 
        }
        [Route("api/IncomingMessage/GetEntity")]
        [HttpPost]
        public OperationResult GetEntity(Entity _entity)
        {
            return _entity.SelectRecord(_entity.useAuditable);
        }
        [Route("api/IncomingMessage/GetEntityName")]
        [HttpPost]
        public OperationResult GetEntityName(string _entityName)
        {
            Entity entity = new Entity();
            entity.EntityName = _entityName;
            entity.Attributes = new List<DAC.Entidades.Attribute>();
            entity.Keys = new List<DAC.Entidades.Key>();
            return entity.SelectRecord(entity.useAuditable);
        }

        public OperationResult Post(Entity _entity)
        {
            Entity entity = Entity.construct(_entity);
            return entity.InsertRecord();

        }
        [Route("api/IncomingMessage/DeleteEntity")]
        [HttpPost]
        public OperationResult Delete(Entity _entity)
        {
            return _entity.DeleteRecord();
        }

        public OperationResult Put(Entity _entity)
        {
            Entity entity = Entity.construct(_entity);
            string estatus = string.Empty;
            if (_entity.EntityName.ToUpperInvariant() != "AUDITABLE" && IsEstatusUpd(_entity.Attributes, out estatus))
            {
                if (string.IsNullOrEmpty(estatus))
                {
                    Entity audit = new Entity();
                    audit.Attributes.Add(new DAC.Entidades.Attribute("Estatus", estatus, "int"));
                    audit.Keys.Add(new Key("Id", _entity.getAttrValueByName("Id").ToString(), "int"));
                    audit.UpdateRecord();
                }
            }
            return entity.UpdateRecord();
        }

        private bool IsEstatusUpd(List<DAC.Entidades.Attribute> Attributes, out string estatus)
        {
            bool isestUpd = false;
            estatus = string.Empty;
            List<DAC.Entidades.Attribute> Attr = Attributes.Where(p=>p.AttrName.ToUpper() == "ESTATUS").ToList();
            if(Attr.Count > 0)
            {
                isestUpd = true;
                estatus = Attr.First().AttrValue;
            }
            return isestUpd;
            
        }
        
        
    }
}
