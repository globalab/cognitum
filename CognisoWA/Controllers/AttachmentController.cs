﻿using DAC.Entidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CognisoWA.Controllers
{
    public class AttachmentController : ApiController
    {
        [Route("api/Attachment/getDocumentList")]
        [HttpPost]
        public AttachmentList getDocumentList(Attachment _attachment)
        {
            AttachmentList listResult = new AttachmentList();
            string url, userN,pwd, spFolder, rootFolder;
            this.getConfigData(out url, out userN, out pwd, out spFolder, out rootFolder);
            SpFunctions spFunct = new SpFunctions(url, userN, pwd, spFolder, rootFolder);
            listResult = spFunct.getFilesFromSP(_attachment.EntityName, _attachment.id, _attachment.parentId);
            return listResult;
        }

        [Route("api/Attachment/getAttachment")]
        [HttpPost]
        public bool getAttachment(Attachment _attachment)
        {
            try
            {
                Attachment attch = new Attachment();
                string url, userN, pwd, spFolder, rootFolder;
                this.getConfigData(out url, out userN, out pwd, out spFolder, out rootFolder);
                SpFunctions spFunct = new SpFunctions(url, userN, pwd, spFolder, rootFolder);
                attch = spFunct.getFile(_attachment.EntityName, _attachment.id, _attachment.parentId, _attachment.spUrlFile, _attachment.folderToSave);
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        [Route("api/Attachment/uploadFile")]
        [HttpPost]
        public bool uploadFile(Attachment _attachment)
        {
            Attachment attch = new Attachment();
            string url, userN, pwd, spFolder, rootFolder;
            this.getConfigData(out url, out userN, out pwd, out spFolder, out rootFolder);
            try
            {
                SpFunctions spFunct = new SpFunctions(url, userN, pwd, spFolder, rootFolder);
                Stream stream = new MemoryStream(_attachment.FileContent);
                spFunct.UploadDocument(_attachment.EntityName, _attachment.id, _attachment.parentId, spFolder,_attachment.FileName, stream);

                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }

        private void getConfigData(out string url, out string userN, out string pwd, out string spFolder, out string rootFolder)
        {
            url = ConfigurationManager.AppSettings["SPUrl"].ToString();
            userN = ConfigurationManager.AppSettings["SPUser"].ToString();
            pwd = ConfigurationManager.AppSettings["SPPwd"].ToString();
            spFolder = ConfigurationManager.AppSettings["SPFolder"].ToString();
            rootFolder = ConfigurationManager.AppSettings["RootFolder"].ToString();
        }
    }
}
