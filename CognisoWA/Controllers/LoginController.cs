﻿using DAC.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CognisoWA.Controllers
{
    public class LoginController : ApiController
    {
        public OperationResult Get(string Username,string Password)
        {
            return new Login().LoginUser(Username, Password.Replace("amp;", "&").Replace("esp;", " ").Replace("quest;", "?").Replace("lt;", "<").Replace("gt;", ">").Replace("ast;", "*").Replace("mod;", "%").Replace("doublepoint;", ":").Replace("backs;", @"\"), "");
        }
    }
}
