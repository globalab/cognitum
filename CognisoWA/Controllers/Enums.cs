﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CognisoWA.Controllers
{
    public enum EstatusReciboEnum
    {
        ///<summary>
        ///</summary>
        Pendiente = 0,
        ///<summary>
        ///</summary>
        Cancelado = 1,
        ///<summary>
        ///</summary>
        PorAplicar = 2,
        ///<summary>
        ///</summary>
        Aplicado = 3,
        ///<summary>
        ///</summary>
        PorConciliar = 4,
        ///<summary>
        ///</summary>
        Conciliado = 5,
        ///<summary>
        ///</summary>
        PorPagarComision = 6,
        ///<summary>
        ///</summary>
        ComisionPagada = 7,
        ///<summary>
        ///</summary>
        Eliminado = 8
    }
}