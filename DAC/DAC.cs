﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DAC.Entidades;
using System.Web;
using System.Configuration;

namespace DAC
{
    public class DAC
    {
        private const string AUDITABLECOLUMNS = ", a.Id as Audit_Id, a.FechaAdd as Audit_FechaAdd,a.FechaUMod as Audit_FechaUMod,a.FechaDel as Audit_FechaDel,a.Estatus as Audit_Estatus,a.RefWinsef as Audit_RefWinsef,a.UsuarioAdd as Audit_UsuarioAdd,a.UsuarioUMod as Audit_UsuarioUMod,a.UsuarioDel as Audit_UsuarioDel";
        private const string AUDITJOIN = " left join Auditable a on a.Id = {0}.Id";
        private const string COMMA = ",";
        private const string AND = "and";

        public enum sqlListOf
        {
            Campos = 1, Valores = 2,
        }
        private SqlConnection ConnString;

        public DAC(string _connstring)
        {
            ConnString = new SqlConnection(_connstring);
        }

        public OperationResult InsertRecord(List<Entidades.Attribute> _listOfAttributes, string _entityName, bool getLastIdentity=false)
        {
            string sqlCommText = "insert into {0}({1})Values({2})";
            string listOfFields = this.convertListAttrToSqlFieldList(_entityName, _listOfAttributes, sqlListOf.Campos, false);
            string listOfValues = this.convertListAttrToSqlFieldList(_entityName, _listOfAttributes, sqlListOf.Valores, false);

            string sqlStmt = string.Format(sqlCommText, _entityName, listOfFields, listOfValues);
            return this.executeSQL(sqlStmt, _entityName, false, getLastIdentity);
        }
        public OperationResult UpdateRecord(Entity _entity)
        {
            string sqlCommText = "Update {0} set {1} {2}";
            string listOfAssignFields = this.getSplitList(_entity.EntityAlias, _entity.Attributes, COMMA, false);
            string whereStmt = string.Empty;

            if (_entity.Keys.Count > 0)
            {
                
                whereStmt = this.convertKeysToWhereStmt(_entity.EntityAlias, _entity.Keys, false);
            }

            string sqlStmt = string.Format(sqlCommText, _entity.EntityName, listOfAssignFields, whereStmt);
            return this.executeSQL(sqlStmt, _entity.EntityName);
        }
        public OperationResult DeleteRecord(Entity _entity)
        {
            string sqlCommText = "Delete {0} {1}";
            string whereStmt = string.Empty;

            if (_entity.Keys.Count > 0)
            {
                
                whereStmt = this.convertKeysToWhereStmt(_entity.EntityAlias, _entity.Keys, false);
            }

            string sqlStmt = string.Format(sqlCommText, _entity.EntityName, whereStmt);
            return this.executeSQL(sqlStmt, _entity.EntityName);
        }
        public OperationResult SelectRecord(Entity _entity,bool UseAuditable)
        {
            string sqlCommText = "Select {0} from {1}";
            string listOfAssignFields = "*";
            string sqlStmt = string.Empty;
            string joinStmt = string.Format(AUDITJOIN, _entity.EntityName);
            if (_entity.Attributes.Count > 0)
            {
                listOfAssignFields = this.convertListAttrToSqlFieldList(_entity.EntityAlias, _entity.Attributes, sqlListOf.Campos, true);
                if (_entity.ChildEntities.Count > 0)
                {
                    string listofjoinfields = this.getJoinFields(_entity);
                    if (listofjoinfields.Trim() != string.Empty)
                    {
                        listOfAssignFields = listOfAssignFields + "," + listofjoinfields;
                    }
                }
                if (UseAuditable)
                {
                    listOfAssignFields = listOfAssignFields + AUDITABLECOLUMNS;
                }
            }
            if (!UseAuditable)
            {
                joinStmt = string.Empty;
            }
            string whereStmt = string.Empty;
            
                if (_entity.Keys.Count > 0)
                {

                    whereStmt = this.convertKeysToWhereStmt(_entity.EntityAlias, _entity.Keys, true);
                    //armar where de entidades con join
                    whereStmt = whereStmt + this.getWhereJoin(_entity)  ;
                    sqlStmt = string.Format(sqlCommText, listOfAssignFields, _entity.EntityName == "Usuario" ? _entity.EntityName + " WITH (NOLOCK) " : _entity.EntityName);
                    //armar join
                    joinStmt += this.getJoinStmt(_entity);
                    sqlStmt = string.Format("{0} {1} {2}", sqlStmt, joinStmt, whereStmt);
                }
                else
                {
                    whereStmt = whereStmt + this.getWhereJoin(_entity, false);
                    joinStmt += this.getJoinStmt(_entity);
                    sqlStmt = string.Format(sqlCommText, listOfAssignFields, _entity.EntityName);
                    if (joinStmt != string.Empty)
                    {
                        sqlStmt = string.Format("{0} {1}", sqlStmt, joinStmt);
                    }
                    if (whereStmt != string.Empty)
                    {
                        sqlStmt = string.Format("{0} {1}", sqlStmt, whereStmt);
                    }
                }
                if (_entity.WhereStmt != string.Empty)
                {
                    if (whereStmt == string.Empty)
                    {
                        sqlStmt += "where " + _entity.WhereStmt;
                    }
                    else
                    {
                        sqlStmt += " and " + _entity.WhereStmt;
                    }
                }
                
            
            
            if (_entity.EntityName.ToUpperInvariant() == "RECIBO")
            {
                //sqlStmt += " order by tramite,Numero";
                sqlStmt += " order by recibo.id";
            }
            return this.executeSQL(sqlStmt, _entity.EntityName, true);
        }

        private string getJoinStmt(Entity _mainEntity)
        {
            bool hasJoin = false;
            string joinStmt = " {2} join {0} as {3} on {1} ";
            string sllJoinStmt = string.Empty;
            foreach (JoinEntity joinEntity in _mainEntity.ChildEntities)
            {
                Entity childEntity = joinEntity.ChildEntity;
                bool moreThanOne = false;
                string equalJoinStmt = string.Empty;
                if (joinEntity.selectJoinList != null)
                {
                    foreach (selectJoinEntity selJoinEntity in joinEntity.selectJoinList)
                    {
                        hasJoin = true;
                        if (moreThanOne)
                        {
                            if (selJoinEntity.logicOperator == 1)
                            {
                                equalJoinStmt = string.Format("{0} and ", equalJoinStmt);
                            }
                            else
                            {
                                equalJoinStmt = string.Format("{0} or ", equalJoinStmt);
                            }
                        }
                        Entidades.Attribute childAttr = selJoinEntity.childAttr;
                        Entidades.Attribute mainAttr = selJoinEntity.mainAttr;
                        string onStmt = "{0}.{1} = {2}.{3}";
                        equalJoinStmt += string.Format(onStmt, _mainEntity.EntityAlias, mainAttr.AttrName, childEntity.EntityAlias, childAttr.AttrName);
                        moreThanOne = true;
                    }
                }

                if (hasJoin)
                    sllJoinStmt += string.Format(joinStmt, childEntity.EntityName, equalJoinStmt,joinEntity.JoinType.ToString(),childEntity.EntityAlias);
                else
                    sllJoinStmt = string.Empty;

                if (childEntity.ChildEntities.Count > 0)
                {
                    sllJoinStmt += this.getJoinStmt(childEntity);
                }
            }

            return sllJoinStmt;
        }

        private string getWhereJoin(Entity _mainEntity, bool _parentHasWhere=true)
        {
            bool isFirstWhere = true;
            string whereList = string.Empty;
            bool hasWhere = false;
            foreach (JoinEntity joinEntity in _mainEntity.ChildEntities)
            {
                if (joinEntity.ChildEntity.Keys.Count > 0)
                {
                    hasWhere = true;
                    Entity childEntity = joinEntity.ChildEntity;
                    if (whereList == string.Empty)
                    {
                        whereList += this.convertKeysToWhereStmt(childEntity.EntityAlias, childEntity.Keys, true, true);
                    }
                    else
                    {
                        if (childEntity.logicalOperator == 1)
                        {
                            whereList += " and " + this.convertKeysToWhereStmt(childEntity.EntityAlias, childEntity.Keys, true, true);
                        }
                        else
                        {
                            whereList += " or " + this.convertKeysToWhereStmt(childEntity.EntityAlias, childEntity.Keys, true, true);
                        }
                    }

                    if (isFirstWhere)
                    {
                        isFirstWhere = false;
                        if (_parentHasWhere)
                        {
                            if (_mainEntity.logicalOperator == 1)
                            {
                                whereList = " and " + whereList;
                            }
                            else
                            {
                                whereList = " or " + whereList;
                            }
                        }
                        else
                            whereList = " where " + whereList;
                    }

                    if (childEntity.ChildEntities.Count > 0)
                    {
                        whereList += this.getWhereJoin(childEntity, hasWhere);
                    }
                }
                else
                {
                    Entity childEntity = joinEntity.ChildEntity;
                    if (childEntity.ChildEntities.Count > 0)
                    {
                            whereList += this.getWhereJoin(childEntity, _parentHasWhere);
                    }
                }
            }
            return whereList;
        }

        private string getJoinFields(Entity _mainEntity)
        {
            string listOfFields = string.Empty;
            foreach (JoinEntity joinEntity in _mainEntity.ChildEntities)
            {
                Entity childEntity = joinEntity.ChildEntity;
                if (childEntity.Attributes != null)
                {
                    if (childEntity.Attributes.Count > 0)
                    {
                        if (listOfFields == string.Empty)
                        {
                            listOfFields += this.convertListAttrToSqlFieldList(childEntity.EntityAlias, childEntity.Attributes, sqlListOf.Campos, true, true);
                        }
                        else
                        {
                            listOfFields += "," + this.convertListAttrToSqlFieldList(childEntity.EntityAlias, childEntity.Attributes, sqlListOf.Campos, true, true);
                        }
                    }
                }
                if (childEntity.ChildEntities.Count > 0)
                {
                    string listChildJoin = this.getJoinFields(childEntity);
                    if (listChildJoin != string.Empty)
                    {
                        listOfFields += "," + listChildJoin;
                    }
                }
            }
            return listOfFields;
        }

        public int getNextId()
        {
            int retVal = 0;
            string sqlStmt = "INSERT [dbo].[DomainObject] DEFAULT VALUES;  select SCOPE_IDENTITY() as lastKey";
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }

                SqlCommand sqlComm = new SqlCommand(sqlStmt, ConnString);
                DataTable dtresult = new DataTable();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlComm);
                sqlAdapter.Fill(dtresult);
                if (dtresult.Rows.Count > 0)
                {
                    return int.Parse(dtresult.Rows[0]["lastKey"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            catch(Exception exc)
            {
                return 0;
            }
        }

        public void insertAuditable(int _id, DateTime _dateAuditable, int _stat, int _userId,string entityname)
        {
            string sqlStmt = "Insert into Auditable(Id,FechaAdd,Estatus,UsuarioAdd, FechaUMod, UsuarioUMod)Values(@Id,@FechaAdd,@Estatus,@UsuarioAdd,@FechaAdd, @UsuarioAdd)";
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }

                SqlCommand sqlComm = new SqlCommand(sqlStmt, ConnString);
                sqlComm.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = _id;
                sqlComm.Parameters.Add(new SqlParameter("@FechaAdd", SqlDbType.DateTime)).Value = _dateAuditable;
                sqlComm.Parameters.Add(new SqlParameter("@Estatus", SqlDbType.Int)).Value = _stat;
                sqlComm.Parameters.Add(new SqlParameter("@UsuarioAdd", SqlDbType.Int)).Value = _userId;
                sqlComm.ExecuteNonQuery();
                
                    System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + entityname + _id.ToString()), Environment.NewLine+ "Insertó auditable ", Encoding.UTF8);
                
            }
            catch (Exception exc)
            {

                System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + entityname + _id.ToString()), Environment.NewLine + "Error insert Auditable " + exc.Message, Encoding.UTF8);
                
            }
        }

        public void updateAuditable(int _id, DateTime _dateAuditable, int _stat, int _userId)
        {
            //El Estatus se actualiza en otro punto del proceso. En Incomming Message Controller se arma la petición para actualizar el estatus.
            //string sqlStmt = "Update Auditable set FechaUMod=@FechaAdd,Estatus=@Estatus,UsuarioUMod=@UsuarioAdd where Id=@Id";
            string sqlStmt = "Update Auditable set FechaUMod=@FechaAdd,UsuarioUMod=@UsuarioAdd where Id=@Id";
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }

                SqlCommand sqlComm = new SqlCommand(sqlStmt, ConnString);
                sqlComm.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = _id;
                sqlComm.Parameters.Add(new SqlParameter("@FechaAdd", SqlDbType.DateTime)).Value = _dateAuditable;
                //sqlComm.Parameters.Add(new SqlParameter("@Estatus", SqlDbType.Int)).Value = _stat;
                sqlComm.Parameters.Add(new SqlParameter("@UsuarioAdd", SqlDbType.Int)).Value = _userId;
                sqlComm.ExecuteNonQuery();
            }
            catch (Exception exc)
            {

            }
        }

        private OperationResult executeSQL(string _sqlStmt, string _entityName, bool isSelect = false, bool getLastIdentity=false)
        {
            OperationResult opResult = new OperationResult();
            opResult.Success = true;
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }
                SqlCommand sqlComm = new SqlCommand(_sqlStmt, ConnString);
                if (isSelect)
                {
                    #region Select
                    List<Entity> entities = new List<Entity>();
                    DataTable dtTable = new DataTable();
                    SqlDataAdapter sqlAdpater = new SqlDataAdapter(sqlComm);
                    sqlAdpater.Fill(dtTable);
                    if (dtTable.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtTable.Rows)
                        {
                            Entity entity = new Entity();
                            entity.EntityName = _entityName;
                            entity.Attributes = new List<Entidades.Attribute>();
                            foreach (DataColumn column in dtTable.Columns)
                            {
                                Entidades.Attribute attr = new Entidades.Attribute();
                                attr.AttrName = column.ColumnName;
                                attr.AttrValue = row[column].ToString();
                                attr.AttrType = column.DataType.ToString();
                                entity.Attributes.Add(attr);
                            }
                            entities.Add(entity);
                        }
                        opResult.Success = true;
                        opResult.RetVal = entities;
                    }
                    #endregion
                }
                else
                {
                    try
                    {
                        System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + _entityName + DateTime.Now.ToString("ddMMyyyyhhmmss")), Environment.NewLine + sqlComm.CommandText, Encoding.UTF8);
                    }
                    catch (Exception)
                    {
                    }
                    sqlComm.ExecuteNonQuery();
                    if (getLastIdentity)
                    {
                        sqlComm = new SqlCommand("SELECT @@IDENTITY", ConnString);
                        var id = sqlComm.ExecuteScalar();

                        opResult.returnedId = int.Parse(id.ToString());
                    }
                    opResult.Success = true;
                }
            }
            catch (Exception exc)
            {
                opResult.Success = false;
                if (exc.InnerException != null)
                {
                    opResult.ErrMessage = exc.InnerException.Message;
                }
                else
                {
                    opResult.ErrMessage = exc.Message;
                }
            }
            finally
            {
                ConnString.Close();
            }
            return opResult;
        }
        public List<SharedRecords> getSharedRecords(Int64 _userId)
        {
            List<SharedRecords> listSharedRecords = new List<SharedRecords>();
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }
                SqlCommand sqlComm = new SqlCommand("sp_getSharedRecords @userId", ConnString);
                sqlComm.Parameters.Add(new SqlParameter("@userId", _userId));
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlComm);
                DataTable dtResult = new DataTable();
                sqlAdapter.Fill(dtResult);
                foreach (DataRow row in dtResult.Rows)
                {
                    SharedRecords privEntity = new SharedRecords();
                    privEntity.entity = row["entity"].ToString();
                    privEntity.entityPriv = int.Parse(row["entityPriv"].ToString());
                    privEntity.recordId = row["recordId"].ToString();
                    privEntity.sharedBy = row["sharedBy"].ToString();
                    privEntity.sharedByName = row["sharedByName"].ToString();
                    listSharedRecords.Add(privEntity);
                }
            }
            catch (Exception exc)
            {
            }
            finally
            {
                ConnString.Close();
            }
            return listSharedRecords;
        }
        public List<PrivilegeByEntity> getPrivByEntity(Int64 _userId)
        {
            List<PrivilegeByEntity> listOfPriv = new List<PrivilegeByEntity>();
            try
            {
                if (ConnString.State != ConnectionState.Open)
                {
                    ConnString.Open();
                }
                SqlCommand sqlComm = new SqlCommand("sp_getPrivByEntity @userId", ConnString);
                sqlComm.Parameters.Add(new SqlParameter("@userId", _userId));
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlComm);
                DataTable dtResult = new DataTable();
                sqlAdapter.Fill(dtResult);
                foreach (DataRow row in dtResult.Rows)
                {
                    PrivilegeByEntity privEntity = new PrivilegeByEntity();
                    privEntity.entity = row["entity"].ToString();
                    privEntity.entityPriv = int.Parse(row["entityPriv"].ToString());
                    privEntity.scope = row["scope"].ToString();
                    listOfPriv.Add(privEntity);
                }
            }
            catch (Exception exc)
            {
            }
            finally
            {
                ConnString.Close();
            }
            return listOfPriv;
        }
        //Regresa la lista de atributos para select o insert
        //Ejemplo: fieldName1, fieldName2, fieldName3......,fieldNameN
        private string convertListAttrToSqlFieldList(string _entityName, List<Entidades.Attribute> _listOfAttributes, sqlListOf _sqlListOf, bool isSelect, bool _useAlias = false)
        {
            string listOfFields = string.Empty;
            foreach (Entidades.Attribute attr in _listOfAttributes)
            {
                if (!(_entityName.ToUpperInvariant() == "INTEGRACIONPAGOPASO" && attr.AttrName.ToUpperInvariant() == "ID"))
                {
                    if (_sqlListOf == sqlListOf.Campos)
                    {
                        if (isSelect)
                        {
                            if (_useAlias)
                            {
                                listOfFields = string.Format("{0},{1}.{2} as {1}{2}", listOfFields, _entityName, attr.AttrName);
                            }
                            else
                            {
                                listOfFields = string.Format("{0},{1}.{2}", listOfFields, _entityName, attr.AttrName);
                            }
                        }
                        else
                        {
                            listOfFields = string.Format("{0},{1}", listOfFields, attr.AttrName);
                        }
                    }
                    else
                    {
                        listOfFields = string.Format("{0},{1}", listOfFields, this.sqlValue(attr.AttrType, attr.AttrValue));
                    }
                }
            }
            listOfFields = listOfFields.Substring(1, listOfFields.Length - 1);
            return listOfFields;
        }

        private string convertKeysToWhereStmt(string _entityName, List<Entidades.Key> _keysAttr, bool isSelect, bool isJoin=false)
        {
            string where = "Where  {0} ";
            string listOfValues = string.Empty;
            listOfValues = this.getSplitList(_entityName, _keysAttr, isSelect);
            if (isJoin)
            { 
                return listOfValues;
            }
            else
            {
                return string.Format(where, listOfValues);
            }
        }
        //Crea string para Where o Update dependiendo el separador
        //ejemplo: "campo=valor ," ó "campo=valor and"
        private string getSplitList(string _entityName, List<Entidades.Key> _listAttr, bool isSelect)
        {
            string returnValue = string.Empty;
            //0=fiel dName;
            //1=_separator And or comma
            //2=' or blank
            //3=field Value
            //4=' or blank
            string splitStr = " {1} {0} ";
            string assignSqlStr = string.Empty;
            string valueSQL = string.Empty;
            int intValue = 0;
            Int64 idValue = 0;
            DateTime dtValue;
            decimal decValue;
            float floatVal;
            string strValue;
            int group = -1;
            string _separator = string.Empty;
            int groups = 0;
            foreach (Entidades.Key attr in _listAttr.OrderBy(p=> p.Group))
            {
                if (group == -1)
                {
                    returnValue = " (";
                    groups++;
                }
                if (attr.LogicalOperator == 1)
                {
                    _separator = " and ";
                }
                else
                {
                    _separator = " or ";
                }

                if (group != attr.Group)
                {
                    if (group != -1)
                    {
                        returnValue += ") ";
                        groups--;
                    }
                    group = attr.Group;

                }
                if (returnValue == " (")
                {
                    _separator = string.Empty;
                }
                assignSqlStr = this.convertToQueryData(attr.EntityName==string.Empty?_entityName:attr.EntityName , attr, isSelect, attr.WhereOperator );
                valueSQL = string.Format(splitStr, assignSqlStr, _separator);
                returnValue = string.Format("{0}{1}", returnValue, valueSQL);
                
            }
            
            //returnValue = returnValue.Substring(0, (returnValue.Length - _separator.Length));
            if (groups > 0)
            {
                returnValue += ") ";
            }
            return returnValue;
        }

        private string getSplitList(string _entityName, List<Entidades.Attribute> _listAttr, string _separator, bool isSelect)
        {
            string returnValue = string.Empty;
            //0=fiel dName;
            //1=_separator And or comma
            //2=' or blank
            //3=field Value
            //4=' or blank
            string splitStr = "{0} {1}";
            string assignSqlStr = string.Empty;
            string valueSQL = string.Empty;
            int intValue = 0;
            Int64 idValue = 0;
            DateTime dtValue;
            decimal decValue;
            float floatVal;
            string strValue;
            int group = -1;

            foreach (Entidades.Attribute attr in _listAttr)
            {
                
                assignSqlStr = this.convertToQueryData(_entityName, attr, isSelect);
                valueSQL = string.Format(splitStr, assignSqlStr, _separator);
                returnValue = string.Format("{0}{1}", returnValue, valueSQL);
                
            }

            returnValue = returnValue.Substring(0, (returnValue.Length - _separator.Length));
            return returnValue;
        }

        private string sqlValue(string _typeOf, string _attrValue)
        {
            string valueSQL = string.Empty;
            switch (_typeOf)
            {
                case "string":
                    valueSQL = string.Format("'{0}'", _attrValue);
                    break;
                case "datetime":
                    valueSQL = "1900-01-01";
                    if (_attrValue.Trim() != "")
                    {
                        valueSQL = string.Format("'{0}'", string.Format("{0}-{1}-{2}", _attrValue.Substring(6, 4), _attrValue.Substring(3, 2), _attrValue.Substring(0, 2)));
                    }
                    break;
                case "int":
                case "int64":
                case "long":
                case "decimal":
                case "float":
                    string sqlVal = "0";
                    if (_attrValue != string.Empty)
                    {
                        sqlVal = _attrValue;
                    }
                    else
                    {
                        if(_typeOf == "int" && _attrValue == "null")
                        {
                            sqlVal = "null";
                        }
                    }
                    valueSQL = string.Format("{0}", sqlVal);
                    break;
            }
            return valueSQL;
        }

        //Mapea fieldName con su fieldValue y lo regresa como sql lo aceptaria
        //Ejemplo: Campo=valor ó Campo='valor'
        private string convertToQueryData(string _entityName, Entidades.Attribute _attr, bool isSelect, whereOperator _whereOp = whereOperator.Like)
        {
            string parString = "{0} = {1}";
            string valueSQL = string.Empty;
            if (isSelect)
            {
                if (_whereOp != whereOperator.IsNotNull && _whereOp != whereOperator.IsNull)
                {
                    parString = "{0}.{1}{3}{2}";
                    valueSQL = string.Format(parString, _entityName, _attr.AttrName, this.sqlValue(_attr.AttrType, _attr.AttrValue.ToString()), convertWhereOp(_whereOp));
                }
                else
                {
                    parString = "{0}.{1}{2}";
                    valueSQL = string.Format(parString, _entityName, _attr.AttrName, convertWhereOp(_whereOp));
                }
            }
            else
            {
                parString = "{0}={1}";
                valueSQL = string.Format(parString, _attr.AttrName, this.sqlValue(_attr.AttrType, _attr.AttrValue.ToString()));
            }
            return valueSQL;
        }

        private string convertWhereOp(whereOperator _whereOp)
        {
            switch (_whereOp)
            {
                case whereOperator.Equal:
                    return " = ";
                case whereOperator.Greater:
                    return " > ";
                case whereOperator.GreaterEqual:
                    return " >= ";
                case whereOperator.Like:
                    return " like ";
                case whereOperator.Lower:
                    return " < ";
                case whereOperator.LowerEqual:
                    return " <= ";
                case whereOperator.NotEqual:
                    return " <> ";
                case whereOperator.IsNotNull:
                    return " is not null ";
                case whereOperator.IsNull:
                    return " is null ";
            }

            return " like ";
        }

        /*
        public Usuario LoginUser(string user, string password)
        {

            //if (LOG.IsDebugEnabled)
            //{
            //    LOG.Debug(string.Format("Iniciando sesion para el usuario {0}", username));
            //}
            var usuario = ObtenerPorUsername(username, ubicacion);
            //if (LOG.IsDebugEnabled)
            //{
            //    LOG.Debug(string.Format("Usuario obtenido de la BD: {0}.", usuario));
            //}
            if (usuario == null)
            {
                //if (LOG.IsDebugEnabled)
                //{
                //    LOG.Debug(string.Format("El usuario {0} no se encontro en la BD.", username));
                //}
                throw new SecurityException("El usuario no existe.");
            }
            //if (LOG.IsDebugEnabled)
            //{
            //    LOG.Debug("Checando estatus.");
            //}
            if (usuario.Estatus == EstatusEnum.Inactivo)
            {
                //if (LOG.IsDebugEnabled)
                //{
                //    LOG.Debug(string.Format("Usuario {0} inactivo", username));
                //}
                throw new SecurityException("El usuario esta inactivo.");
            }
            if (usuario.Estatus == EstatusEnum.Eliminado)
            {
                //if (LOG.IsDebugEnabled)
                //{
                //    LOG.Debug(string.Format("Usuario {0} eliminado", username));
                //}
                throw new SecurityException("El usuario ha sido eliminado.");
            }

            //if (LOG.IsDebugEnabled)
            //{
            //    LOG.Debug("Estatus OK.");
            //    LOG.Debug("Checando contraseña.");
            //}
            Cipher Cifrado = new Cipher();
            var cif = Cifrado.CifrarCadena(password);
            if (!usuario.Password.Password.Equals(cif))
            {
                //if (LOG.IsDebugEnabled)
                //{
                //    LOG.Debug(string.Format("Contraseña incorrecta para el usuario {0}.", username));
                //}
                throw new SecurityException("Contraseña incorrecta.");
            }
            if (usuario.SesionIniciada)
            {
                //if (LOG.IsDebugEnabled)
                //{
                //    LOG.Debug(string.Format("El usuario ya se encuentra firmado {0}.", username));
                //}
                throw new SecurityException("El usuario ya se encuentra firmado.");
            }

            //Context.Initialize(usuario);

            usuario.UltimoAcceso = DateTime.Now;
            //usuario.SesionIniciada = true;
            //Repository.Update(usuario);
            //_bitacoraAccesoRepository.Save(new BitacoraAcceso
            //                                  {
            //                                      Usuario = usuario,
            //                                      FechaRegistro = DateTime.Now,
            //                                      Tipo = BitarocaAccesoRegistroTipo.Acceso
            //                                  });

            //if (LOG.IsDebugEnabled)
            //{
            //    LOG.Debug("Contraseña OK.");
            //    LOG.Debug(string.Format("Sesion iniciada, usuario: {0}.", username));
            //}
            return usuario;
        }
         */
    }
}
