﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Entidades
{
    [DataContract(Name = "UserRoles", Namespace = "")]
    class UserRoles : Entity
    {
        public UserRoles()
            : base()
        {
            EntityName = "RoleByUsers";
            EntityAlias = EntityName;
        }

        public override OperationResult InsertRecord()
        {
            Entity valUserRole = new Entity("RoleByUsers");
            Key filter=new Key("UserId", this.getAttrValueByName("UserId"), "int");
            filter.Group = 1;
            filter.LogicalOperator = 1;
            filter.WhereOperator = whereOperator.Equal;
            valUserRole.Keys.Add(filter);

            filter = new Key("RoleId", this.getAttrValueByName("RoleId"), "int");
            filter.Group = 1;
            filter.LogicalOperator = 1;
            filter.WhereOperator = whereOperator.Equal;
            valUserRole.Keys.Add(filter);

            OperationResult result = valUserRole.SelectRecord(false);
            
            if (result.RetVal.Count == 0)
            {

                OperationResult opRes = this.odac.InsertRecord(this.Attributes, this.EntityName);
                return opRes;
            }
            else
            {
                OperationResult opRes = new OperationResult();
                opRes.Success = true;
                return opRes;
            }
        }
    }
}
