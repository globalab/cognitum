﻿using DAC.Crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Entidades
{
    [DataContract(Name = "Login", Namespace = "")]
    public class Login: Entity
    {
        /// <summary>
        /// Llave primaria de la entidad, se manejan llaves simples 
        /// autoincrementales, si se requieren un grupo de campos unicos
        /// se maneja un constraint por medio de la propieda Unique en 
        /// conjunto con UniqueKey para agrupar las propiedades pertenecientes
        /// al constraint.
        /// </summary>
        [DataMember]
        public Int64 Id { get; set; }

        ///<summary>
        ///</summary>
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public EstatusEnum Estatus { get; set; }
        ///<summary>
        ///</summary>
        [DataMember]
        public DateTime? UltimoAcceso { get; set; }

        ///<summary>
        ///</summary>
        [DataMember]
        public bool SesionIniciada { get; set; }

        public OperationResult LoginUser(string username, string password, string ubicacion)
        {
            OperationResult retval = new OperationResult();
            retval.Success = true;
            Login usrlogin = ObtenerPorUsername(username, ubicacion);
            if (usrlogin == null)
            {
                retval.Success = false;
                retval.ErrMessage = "El usuario no existe.";
            }
            if (usrlogin.Estatus == EstatusEnum.Inactivo)
            {
                retval.Success = false;
                retval.ErrMessage = "El usuario esta inactivo.";
                
            }
            if (usrlogin.Estatus == EstatusEnum.Eliminado)
            {
                retval.Success = false;
                retval.ErrMessage = "El usuario ha sido eliminado.";
                
            }

            Entity Password = new Entity( "HistorialPassword");
            Password.Attributes.Add(new Attribute("Password"));
            Password.Keys.Add(new Key("Usuario",usrlogin.Id.ToString(),"int64"));
            Password.Keys.Add(new Key("Activo", "1", "int"));
            OperationResult pwdop =  Password.SelectRecord(false);
            if (pwdop.Success)
            {
                string currPwd = pwdop.RetVal.First().Attributes.First().AttrValue;
                Cipher Cifrado = new Cipher();
                var cif = Cifrado.CifrarCadena(password);
                var descif = Cifrado.DescifrarCadena(currPwd);
                if (!currPwd.Equals(cif))
                {
                    retval.Success = false;
                    retval.ErrMessage = "Contraseña incorrecta.";

                }
                if (usrlogin.SesionIniciada)
                {
                    retval.Success = false;
                    retval.ErrMessage = "El usuario ya se encuentra firmado.";

                }
            }
            else
            {
                retval.Success = false;
                retval.ErrMessage = "Contraseña incorrecta.";
            }
            if (retval.Success)
            {
                usrlogin.UltimoAcceso = DateTime.Now;
                Entity Usuario = new Entity("Usuario");
                Usuario.Attributes.Add(new Attribute("UltimoAcceso", usrlogin.UltimoAcceso.Value.ToString("yyyy-MM-dd HH:mm:ss"),"datetime"));
                Usuario.Keys.Add(new Key("Id", usrlogin.Id.ToString(), "int64"));
                Usuario.UpdateRecord();
                retval.Success = true;
                retval.RetVal = new List<Entity>();
                retval.RetVal.Add(usrlogin);
            }
            return retval;
        }
        public string Password
        {
            get
            {
                return ""; //Password.GetUserPassword();
            }
        }

        public Login(int _Id,string _Username,string _Nombre,bool _SesionIniciada)
        {
            Id = _Id;
            Username = _Username;
            Nombre = _Nombre;
            SesionIniciada = _SesionIniciada;
        }

        public Login()
        {
        }

        private Login ObtenerPorUsername(string UserName, string Ubicacion)
        {
            Login Usr = new Login();
            Entity User = new Entity();
            User.EntityName = "Usuario";
            User.EntityAlias = User.EntityName;
            User.Attributes = new List<Attribute>();
            User.Attributes.Add(new Attribute("Id"));
            User.Attributes.Add(new Attribute("Username"));
            User.Attributes.Add(new Attribute("Nombre"));
            User.Attributes.Add(new Attribute("SesionIniciada"));
            //User.Attributes.Add(new Attribute("Ubicacion"));
            User.Keys = new List<Key>();
            User.Keys.Add(new Key("Username", UserName, "string"));
            User.Keys.Add(new Key("Ubicacion", "2185", "int"));

            OperationResult res = User.SelectRecord(true);

            if (res.Success)
            {
                Entity fetchedUsr = res.RetVal.First();
                foreach(Attribute atr in fetchedUsr.Attributes)
                {
                    if (!atr.AttrName.Contains("Audit_"))
                    {
                        try
                        {
                            Usr.GetType().GetProperty(atr.AttrName).SetValue(Usr, getAttrVal(atr.AttrValue, atr.AttrType));
                        }
                        catch (Exception)
                        {
                        }
                    }
                    else
                    {
                        if (atr.AttrName == "Audit_Estatus")
                        {
                            Usr.Estatus = atr.AttrValue=="1"?EstatusEnum.Activo:atr.AttrValue=="0"?EstatusEnum.Inactivo:EstatusEnum.Eliminado;
                        }
                    }
                }
                
            }
            

            return Usr;
            
        }

        public object getAttrVal(string AttVal, string AttrType)
        {
            object retval = null;
            switch (AttrType)
            {
                case "System.String":
                    retval = AttVal as object;
                    break;
                case "System.Int":
                    int ivalue;
                    int.TryParse(AttVal, out ivalue);
                    retval = ivalue as object;
                    break;
                case "System.DateTime":
                    DateTime datevalue;
                    DateTime.TryParse(AttVal,out datevalue);
                    retval = datevalue as object;
                    break;
                case "System.Int64":
                    Int64 i64val;
                    Int64.TryParse(AttVal, out i64val);
                    retval = i64val as object;
                    break;
                case "System.Long":
                    long lvalue;
                    long.TryParse(AttVal, out lvalue);
                    retval = lvalue as object;
                    break;
                case "System.Decimal":
                    decimal dvalue;
                    decimal.TryParse(AttVal, out dvalue);
                    retval = dvalue as object;
                    break;
                case "System.Float":
                    float fval;
                    float.TryParse(AttVal, out fval);
                    retval = fval as object;
                    break;

            }

            return retval;
        }
    }
}
