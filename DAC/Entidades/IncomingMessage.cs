﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Net.Mail;
using System.Configuration;
using System.Security;
using System.Web;

namespace DAC.Entidades
{
    [DataContract(Name = "Entity", Namespace = "")]
    public class Entity
    {
        private string emailFrom, emailTo, port, usrEmail, pwdEmail, host, subject, urlLead;

        public string UrlLead
        {
            get { return urlLead; }
            set { urlLead = value; }
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string Host
        {
            get { return host; }
            set { host = value; }
        }

        public string PwdEmail
        {
            get { return pwdEmail; }
            set { pwdEmail = value; }
        }

        public string UsrEmail
        {
            get { return usrEmail; }
            set { usrEmail = value; }
        }

        public string Port
        {
            get { return port; }
            set { port = value; }
        }

        public string EmailTo
        {
            get { return emailTo; }
            set { emailTo = value; }
        }

        public string EmailFrom
        {
            get { return emailFrom; }
            set { emailFrom = value; }
        }

        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string EntityAlias { get; set; }
        [DataMember]
        public int PKId { get; set; }
        [DataMember]
        public int Action { get; set; } //ConvertLeadtoCustomer
        [DataMember]
        public List<Key> Keys { get; set; }
        [DataMember]
        public List<Attribute> Attributes { get; set; }
        [DataMember]
        public List<JoinEntity> ChildEntities { get; set; }
        [DataMember]
        public Auditable auditable { get; set; }
        [DataMember]
        public bool useAuditable { get; set; }
        [DataMember]
        public int logicalOperator { get; set; } //1 and 2 or
        [DataMember]
        public Dictionary<int,string> postBackList { get; set; } //1 and 2 or
        [DataMember]
        public string WhereStmt { get; set; }

        public DAC odac;

        public Entity()
        {
            initializeEnt();
        }

        public Entity(string _EntityName)
        {
            initializeEnt();
            EntityName = _EntityName;
            EntityAlias = _EntityName;
            
        }

        public Entity(string _EntityName,string _EntityAlias)
        {
            initializeEnt();
            EntityName = _EntityName;
            EntityAlias = _EntityAlias;
        }


        private void initializeEnt()
        {
            odac = new DAC(ConfigurationManager.AppSettings["dbConn"].ToString());
            Attributes = new List<Attribute>();
            Keys = new List<Key>();
            ChildEntities = new List<JoinEntity>();
            auditable = new Auditable();
            Port = ConfigurationManager.AppSettings["Port"].ToString();
            Host = ConfigurationManager.AppSettings["Host"].ToString();
            UsrEmail = ConfigurationManager.AppSettings["muser"].ToString();
            PwdEmail = ConfigurationManager.AppSettings["mpwd"].ToString();
            EmailFrom = ConfigurationManager.AppSettings["emailfrom"].ToString();
            EmailTo = ConfigurationManager.AppSettings["emailto"].ToString();
            Subject = ConfigurationManager.AppSettings["Subject"].ToString();
            UrlLead = ConfigurationManager.AppSettings["valLead"].ToString();
            logicalOperator = 1;
            postBackList = new Dictionary<int, string>();
            WhereStmt = string.Empty;
        }

        public static Entity construct(Entity _incoMsg)
        {
            Entity retVal = _incoMsg;
            switch (_incoMsg.EntityName.ToLower())
            {
                case "prospecto":
                    retVal = new Prospecto();
                    retVal.odac = _incoMsg.odac;
                    retVal.PKId = _incoMsg.PKId;
                    retVal.Keys = _incoMsg.Keys;
                    retVal.Attributes = _incoMsg.Attributes;
                    retVal.Action = _incoMsg.Action;
                    retVal.auditable = _incoMsg.auditable;
                    retVal.ChildEntities = _incoMsg.ChildEntities;
                    break;

                case "securityrole":
                    retVal = new Role();
                    retVal.odac = _incoMsg.odac;
                    retVal.PKId = _incoMsg.PKId;
                    retVal.Keys = _incoMsg.Keys;
                    retVal.Attributes = _incoMsg.Attributes;
                    retVal.Action = _incoMsg.Action;
                    retVal.auditable = _incoMsg.auditable;
                    retVal.ChildEntities = _incoMsg.ChildEntities;
                    break;

                case "rolebyusers":
                    retVal = new UserRoles();
                    retVal.odac = _incoMsg.odac;
                    retVal.PKId = _incoMsg.PKId;
                    retVal.Keys = _incoMsg.Keys;
                    retVal.Attributes = _incoMsg.Attributes;
                    retVal.Action = _incoMsg.Action;
                    retVal.auditable = _incoMsg.auditable;
                    retVal.ChildEntities = _incoMsg.ChildEntities;
                    break;
            }
            return retVal;
        }
        public OperationResult sendEmail(string _from, string _to, string _subject, string _body)
        {
            OperationResult opRes = new OperationResult();
            try
            {
                MailMessage mail = new MailMessage(_from, _to.Split(';')[0]);
                mail.To.Clear();
                foreach (var address in _to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(this.Port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = this.Host;
                mail.Subject = _subject;

                mail.Body = _body;
                mail.IsBodyHtml = true;
                client.Credentials = new System.Net.NetworkCredential(this.UsrEmail, this.PwdEmail);
                client.EnableSsl = true;
                client.Send(mail);
                opRes.Success = true;
            }
            catch (Exception exc)
            {
                opRes.Success = false;
                opRes.ErrMessage = exc.Message;
            }
            return opRes;
        }

        public virtual OperationResult InsertRecord()
        {
            int pkValue = 0;
            OperationResult opResult = new OperationResult();
            Attribute result = this.Attributes.Find(x => x.AttrName.ToLower() == "id");
            if (result == null)
            {
                pkValue = odac.getNextId();
                Attribute attr = new Attribute("Id", pkValue.ToString(), "int");
                this.Attributes.Add(attr);
            }
            else
            {
                int.TryParse(this.getAttrValueByName("Id"), out pkValue);
            }
            if(pkValue == 0 && this.EntityName.ToUpperInvariant() == "INTEGRACIONPAGOPASO" )
            {
                pkValue = 1;
            }
            if (pkValue != 0 )
            {

                System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + this.EntityName + pkValue.ToString()), Environment.NewLine + EntityName +" " + pkValue.ToString(), Encoding.UTF8);
                
                if (this.auditable.hasValue())
                {

                    System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + this.EntityName + pkValue.ToString()), Environment.NewLine + "Entra insert Auditable " + this.auditable.userId.ToString(), Encoding.UTF8);
                    
                    odac.insertAuditable(pkValue, this.auditable.opDate, this.auditable.recordStatus, this.auditable.userId,this.EntityName);
                    //this.postBackList.Add(pkValue, "Auditable");
                }
                //auto calcula campo folio cuando es liquidación folio = id
                if (this.EntityName.ToLower() == "liquidacion")
                {
                    result = this.Attributes.Find(x => x.AttrName.ToLower() == "folio");
                    if (result == null)
                    {
                        Attribute attr = new Attribute("folio", pkValue.ToString(), "int");
                        this.Attributes.Add(attr);
                    }
                    else
                    {
                        if (this.getAttrValueByName("folio") == string.Empty)
                        {
                            Attribute attr = new Attribute("folio", pkValue.ToString(), "int");
                            this.Attributes.Remove(result);
                            this.Attributes.Add(attr);
                        }
                    }
                }
                opResult = odac.InsertRecord(this.Attributes, this.EntityName.ToString());
                opResult.returnedId = pkValue;

                if (opResult.Success && this.hasChildEntities())
                {
                    //this.postBackList.Add(opResult.returnedId, this.EntityName.ToString());
                    foreach (JoinEntity joinEntity in this.ChildEntities)
                    {
                        Attribute addingJoinAttr = joinEntity.JoinKey;
                        Entity childEntity = joinEntity.ChildEntity;

                        result = childEntity.Attributes.Find(x => x.AttrName == addingJoinAttr.AttrName);
                        if (result != null)
                        {
                            result.AttrValue = pkValue.ToString();
                        }
                        else
                        {
                            result = new Attribute(addingJoinAttr.AttrName, pkValue.ToString(), addingJoinAttr.AttrType);
                            childEntity.Attributes.Add(result);
                        }
                        OperationResult opresChild = childEntity.InsertRecord();

                        if (!opresChild.Success)
                        {
                            opResult.Success = opresChild.Success;
                            opResult.ErrMessage = opresChild.ErrMessage;
                            //doPostBack();
                            break;
                        }
                    }
                }
                else
                {
                    //doPostBack();
                }
            }
            else
            {
                opResult.Success = false;
                opResult.ErrMessage = "No se pudo obtener el valor de la llave primaría correctamente";
            }
            return opResult;
        }

        private void doPostBack()
        {
            foreach (KeyValuePair<int, string> item in this.postBackList)
            {
                Entity entity = new Entity(item.Value);
                entity.Keys.Add(new Key("id", item.Key.ToString(), "int"));

                entity.DeleteRecord();
            }
        }

        public virtual OperationResult UpdateRecord()
        {
            OperationResult opResult = new OperationResult();
            opResult.Success = true;
            try
            {
                

                if (this.Attributes.Count > 0)
                {
                    Key result = this.Keys.Find(x => x.AttrName.ToLower() == "id");
                    opResult = odac.UpdateRecord(this);
                    
                    if (opResult.Success && this.hasChildEntities())
                    {
                        
                        //this.postBackList.Add(opResult.returnedId, this.EntityName.ToString());
                        foreach (JoinEntity joinEntity in this.ChildEntities)
                        {
                            
                            Attribute addingJoinAttr = joinEntity.JoinKey;
                            
                            Entity childEntity = joinEntity.ChildEntity;
                            
                            if (result != null)
                            {
                                
                                childEntity.Keys.Add(new Key(addingJoinAttr.AttrName, result.AttrValue, addingJoinAttr.AttrType));
                                
                                OperationResult opresChild = childEntity.UpdateRecord();
                                
                                if (!opresChild.Success)
                                {
                                    opResult.Success = opresChild.Success;
                                    opResult.ErrMessage = opresChild.ErrMessage;
                                    //doPostBack();
                                    break;
                                }
                            }
                        }
                        if (this.auditable.hasValue())
                        {
                            if (result != null)
                            {
                                int pkValue = 0;

                                if (int.TryParse(result.AttrValue, out pkValue))
                                {

                                    System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + this.EntityName + pkValue.ToString()), Environment.NewLine + "Entra insert Auditable " + this.auditable.userId.ToString(), Encoding.UTF8);

                                    odac.updateAuditable(pkValue, this.auditable.opDate, this.auditable.recordStatus, this.auditable.userId);
                                }
                            }
                            //this.postBackList.Add(pkValue, "Auditable");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                opResult.Success = false;
                opResult.ErrMessage = ex.Message;
                System.IO.File.AppendAllText(HttpContext.Current.Server.MapPath(@"\Logs\" + this.EntityName + DateTime.Now.ToString("ddMMyyyyhhmmss")), Environment.NewLine + ex.StackTrace +Environment.NewLine + ex.InnerException + this.auditable.userId.ToString(), Encoding.UTF8);

            }
            return opResult;
        }
        public virtual OperationResult DeleteRecord()
        {
            OperationResult opResult = odac.DeleteRecord(this);
            return opResult;
        }
        public virtual OperationResult SelectRecord(bool useAuditable)
        {
            if (string.IsNullOrEmpty(this.EntityAlias))
                this.EntityAlias = this.EntityName;
            OperationResult opResult = odac.SelectRecord(this,useAuditable);
            return opResult;
        }
        public string getAttrValueByName(string _attrName)
        {
            Attribute attr = this.Attributes.Find(x => x.AttrName.ToLower() == _attrName.ToLower());
            if (attr != null)
            {
                return attr.AttrValue;
            }
            else
            {
                return string.Empty;
            }
        }
        public string getKeyValueByName(string _attrName)
        {
            Attribute attr = this.Keys.Find(x => x.AttrName.ToLower() == _attrName.ToLower());
            if (attr != null)
            {
                return attr.AttrValue;
            }
            else
            {
                return string.Empty;
            }
        }
        public bool hasChildEntities()
        {
            return this.ChildEntities.Count > 0;
        }
    }
    [DataContract(Name = "Auditable", Namespace = "")]
    public class Auditable
    {
        [DataMember]
        public int entityId { get; set; }
        [DataMember]
        public int userId { get; set; }
        [DataMember]
        public DateTime opDate { get; set; }
        [DataMember]
        public int recordStatus { get; set; }

        public Auditable(int _userId, DateTime _opDate, int _recordStatus)
        {
            userId = _userId;
            opDate = _opDate;
            recordStatus = _recordStatus;
        }
        public Auditable()
        {
        }

        public bool hasValue()
        {
            if (this.userId != null)
            {
                if (this.userId != 0)
                {
                    return true;
                }
            }
            return false;
        }
    }

    [DataContract(Name = "Attribute", Namespace = "")]
    public class Attribute
    {
        [DataMember]
        public string AttrName { get; set; }
        [DataMember]
        public string AttrValue { get; set; }
        [DataMember]
        public string AttrType { get; set; }

        public Attribute(string _AttrName)
        {
            AttrName = _AttrName;
        }
        public Attribute(string _AttrName, string _AttrValue)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
        }

        public Attribute()
        {
        }

        public Attribute(string _AttrName, string _AttrValue, string _AttrType)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
        }
    }

    [DataContract(Name = "Key", Namespace = "")]
    public class Key:Attribute
    {
        [DataMember]
        public int LogicalOperator { get; set; }
        [DataMember]
        public int Group { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public whereOperator WhereOperator { get; set; }

        public Key(string _AttrName)
        {
            AttrName = _AttrName;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }
        public Key(string _AttrName, string _AttrValue)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key()
        {
            LogicalOperator = 1;
            Group = 0;
            WhereOperator = whereOperator.Like;
            EntityName = string.Empty;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, whereOperator _WhereOperator)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = _WhereOperator;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType,int _LogicalOpertor,int _Group)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = _Group;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = 0;
            EntityName = string.Empty; WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor,int _Group,string _EntityName)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = _Group;
            EntityName = _EntityName;
            WhereOperator = whereOperator.Like;
        }

    }

    [DataContract(Name = "JoinEntity", Namespace = "")]
    public class JoinEntity
    {
        [DataMember]
        public Entity ChildEntity { get; set; }
        [DataMember]
        public Attribute JoinKey { get; set; }
        [DataMember]
        public List<selectJoinEntity> selectJoinList { get; set; }
        [DataMember]
        public JoinType JoinType { get; set; }

        public JoinEntity()
        {
            this.selectJoinList = new List<selectJoinEntity>();
            this.JoinType = JoinType.Inner;
        }
    }

    public class selectJoinEntity
    {
        [DataMember]
        public int logicOperator { get; set; }
        [DataMember]
        public Attribute mainAttr { get; set; }
        [DataMember]
        public Attribute childAttr { get; set; }

        public selectJoinEntity(string _mainAttr, int _logicalOperator, string _childAttr)
        {
            this.logicOperator = _logicalOperator;
            this.mainAttr = new Attribute(_mainAttr);
            this.childAttr = new Attribute(_childAttr);

        }

        public selectJoinEntity()
        {
        }
        
    }

    public enum JoinType
    {
        Inner = 1,
        Left = 2,
        Right = 3

    }

    public enum whereOperator
    {
        Equal = 1,
        NotEqual = 2,
        Greater = 3,
        GreaterEqual = 4,
        Lower = 5,
        LowerEqual = 6,
        IsNull = 7,
        IsNotNull = 8,
        Like = 9
    }
}
