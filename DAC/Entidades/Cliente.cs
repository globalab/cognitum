﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace DAC.Entidades
{
    [DataContract(Name = "Cliente", Namespace = "")]
    class Cliente:Entity
    {
        [DataMember]
        public Int64 Id { get; set; }
        [DataMember]
        public int Permiso { get; set; }
        [DataMember]
        public string NombreCompleto { get; set; }
        [DataMember]
        public int Tipo { get; set; } //TODO: buscar valores posibles para documentar
        [DataMember]
        public string Rfc { get; set; }
        [DataMember]
        public Int64 Ubicacion { get; set; }
        [DataMember]
        public Int64 CentroDeBeneficio { get; set; }
        [DataMember]
        public Int64 Propietario { get; set; }
        [DataMember]
        public Int64 Giro { get; set; }
        [DataMember]
        public Int64 Grupo { get; set; }
        [DataMember]
        public string IdSap { get; set; }
        [DataMember]
        public Int64 Registrante { get; set; }
        [DataMember]
        public int EstatusCliente { get; set; }
        [DataMember]
        public Int64 CoRegistrante { get; set; }

        public Cliente()
        {
            mapeaCampos();
        }

        private void mapeaCampos()
        {
            Int64 idVal = 0;
            int intVal = 0;
            foreach (Attribute attr in this.Attributes)
            {
                #region Mapeo de campos
                switch (attr.AttrName)
                {
                    case "Id":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Id = idVal;
                        break;
                    case "Permiso":
                        int.TryParse(attr.AttrValue.ToString(), out intVal);
                        this.Permiso = intVal;
                        break;
                    case "NombreCompleto":
                        this.NombreCompleto = attr.AttrValue;
                        break;
                    case "Tipo":
                        int.TryParse(attr.AttrValue.ToString(), out intVal);
                        this.Tipo = intVal;
                        break;
                    case "Rfc":
                        this.Rfc = attr.AttrValue;
                        break;
                    case "Ubicacion":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Ubicacion = idVal;
                        break;
                    case "CentroDeBeneficio":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.CentroDeBeneficio = idVal;
                        break;
                    case "Propietario":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Propietario = idVal;
                        break;
                    case "Giro":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Propietario = idVal;
                        break;
                    case "Grupo":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Propietario = idVal;
                        break;
                    case "IdSap":
                        this.IdSap = attr.AttrValue;
                        break;
                    case "Registrante":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Registrante = idVal;
                        break;
                    case "EstatusCliente":
                        int.TryParse(attr.AttrValue.ToString(), out intVal);
                        this.Permiso = intVal;
                        break;
                    case "CoRegistrante":
                        Int64.TryParse(attr.AttrValue.ToString(), out idVal);
                        this.Registrante = idVal;
                        break;
                }
                #endregion
            }
        }
    }
}
