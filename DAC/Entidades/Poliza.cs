﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DAC.Entidades
{
    [DataContract(Name = "Prospecto", Namespace = "")]
    class Poliza:Entity
    {
        private int custType;

        public Poliza() : base()
        {
            EntityName = "Poliza";
            EntityAlias = EntityName;
        }

       
        private OperationResult doActions(string _key)
        {
            OperationResult opRes = new OperationResult();
            opRes.Success = false;
            switch (this.Action)
            {
                case 1:
                    opRes = this.CancelPoliza(_key);
                    break;
                
            }
            return opRes;
        }

        private OperationResult CancelPoliza(string key)
        {
            OperationResult opRes = new OperationResult();
            try
            {
                Entity poliza = new Entity("Poliza");
                poliza.Attributes.Add(new Attribute("EstatusPoliza", "2", "int"));
                poliza.Keys.Add(new Key("Id", key, "int"));
                opRes = poliza.UpdateRecord();
                if (opRes.Success)
                {
                    Entity recibo = new Entity("Recibo");
                    recibo.Attributes.Add(new Attribute("Estatus", "1", "int"));
                    recibo.Keys.Add(new Key("tramite", key, "int"));
                    opRes = recibo.UpdateRecord();
                }
            }
            catch (Exception exc)
            {
                opRes.Success = false;
                opRes.ErrMessage = exc.Message;
            }
            return opRes;
        }

        

        
        public override OperationResult UpdateRecord()
        {
            int key;
            int.TryParse(this.getKeyValueByName("Id"), out key);

            OperationResult opRes = base.UpdateRecord();
            if (this.Action != 0 && key != 0 && opRes.Success)
            {
                opRes = this.doActions(key.ToString());
            }

            return opRes;
        }

        


     
    }
}
