﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DAC.Entidades
{
    [DataContract(Name = "Prospecto", Namespace = "")]
    class Prospecto:Entity
    {
        private int custType;

        public Prospecto():base()
        {
            EntityName = "prospecto";
            EntityAlias = EntityName;
        }

        public override OperationResult InsertRecord()
        {
            int.TryParse(this.getAttrValueByName("Tipo"), out custType);
            OperationResult opRes = base.InsertRecord();
            if (this.Action != 0 && opRes.Success)
            {
                opRes = this.doActions(opRes.returnedId.ToString());
            }

            return opRes;
        }
        private OperationResult doActions(string _key)
        {
            OperationResult opRes = new OperationResult();
            opRes.Success = false;
            switch (this.Action)
            {
                case 1:
                    opRes = this.convertToCustomer(_key);
                    break;
                case 2:
                    opRes = this.initSendEmail(_key, HttpContext.Current.Server.MapPath("HtmlTemplates/leadEmailTemplate.html"));
                    break;
            }
            return opRes;
        }
        private OperationResult initSendEmail(string key, string bodyPath)
        {
            OperationResult opRes = new OperationResult();
                      
            string subject = "Aprobación de prospecto";
            
            Entity leadToSend = searchForLead(key);
            if (leadToSend.Attributes.Count > 0)
            {
                string Body = String.Empty;
                string leadId = leadToSend.getAttrValueByName("Id");
                string leadType = leadToSend.getAttrValueByName("TipoPersona");
                string leadName = string.Empty;
                if (leadType == "2")
                {
                    leadName = leadToSend.getAttrValueByName("RazonSocial");
                    leadType = "Persona Moral";
                }
                else
                {
                    leadName = leadToSend.getAttrValueByName("Nombre") + " " + leadToSend.getAttrValueByName("ApellidoPaterno");
                    leadType = "Persona Física";
                }
                
                string rfc = leadToSend.getAttrValueByName("RFC");
                string email = leadToSend.getAttrValueByName("Email");
                string Giro = leadToSend.getAttrValueByName("Giro");
                string Grupo = leadToSend.getAttrValueByName("Grupo");

                Entity Entidad = new Entity("Giro");
                Entidad.Attributes.Add(new Attribute("Nombre"));
                Entidad.Keys.Add(new Key("Id", Giro, "int64"));
                OperationResult res =  Entidad.SelectRecord(false);
                if (res.Success)
                {
                    if (res.RetVal.Count > 0)
                    {
                        Entidad = res.RetVal[0] as Entity;
                        Giro = Entidad.getAttrValueByName("Nombre");
                    }
                }
                Entidad = new Entity("Grupo");
                Entidad.Attributes.Add(new Attribute("Nombre"));
                Entidad.Keys.Add(new Key("Id", Grupo, "int64"));
                res = Entidad.SelectRecord(false);
                if (res.Success)
                {
                    if (res.RetVal.Count > 0)
                    {
                        Entidad = res.RetVal[0] as Entity;
                        Grupo = Entidad.getAttrValueByName("Nombre");
                    }
                }
                if (bodyPath.Contains("\\api"))
                {
                    bodyPath = bodyPath.Replace("\\api", "");
                }
                if (!String.IsNullOrEmpty(bodyPath))
                {
                    Body = File.ReadAllText(bodyPath);
                }

                Body = Body.Replace("%NombreCompleto%", leadName);
                Body = Body.Replace("%RFC%", rfc);
                Body = Body.Replace("%Email%", email);
                Body = Body.Replace("%URL%", UrlLead);
                Body = Body.Replace("%id%", leadId);
                Body = Body.Replace("%TipoProspecto%", leadType);
                Body = Body.Replace("%Giro%", Giro);
                Body = Body.Replace("%Grupo%", Grupo);

                opRes = this.sendEmail(this.EmailFrom, this.EmailTo, this.Subject, Body);
            }
            else
            {
                opRes.ErrMessage = "No se encontro el prospecto que se quiere enviar a la base de datos";
                opRes.Success = false;
            }

            return opRes;
        }

        private Entity searchForLead(string key)
        {
            Prospecto pros = new Prospecto();
            pros.Keys.Add(new Key("Id", key, "int"));
            OperationResult opResult = pros.SelectRecord(false);
            if (opResult.Success)
            {
                if (opResult.RetVal.Count > 0)
                {
                    Entity leadToSend = opResult.RetVal[0] as Entity;
                    return leadToSend;
                }
            }

            return new Entity();
        }
        public override OperationResult UpdateRecord()
        {
            int key;
            int.TryParse(this.getAttrValueByName("Tipo"), out custType);
            int.TryParse(this.getKeyValueByName("Id"), out key);

            OperationResult opRes = base.UpdateRecord();
            if (this.Action != 0 && key != 0 && opRes.Success)
            {
                opRes = this.doActions(key.ToString());
            }

            return opRes;
        }

        private OperationResult convertToCustomer(string _key)
        {
            OperationResult opResult = new OperationResult();
            opResult.Success = false;
            int owner = 0;
            //if(auditable != null)
            //{
            //    owner = auditable.userId;
            //}
            Prospecto pros = new Prospecto();
            pros.Keys.Add(new Key("Id", _key, "int"));
            opResult = pros.SelectRecord(false);
            if (opResult.Success)
            {
                if (opResult.RetVal.Count > 0)
                {
                    Entity leadToConvert = opResult.RetVal[0] as Entity;
                    int.TryParse(leadToConvert.getAttrValueByName("Propietario"), out owner);
                    int.TryParse(leadToConvert.getAttrValueByName("TipoPersona"), out custType);
                    string nombreCliente = custType == 2 ? leadToConvert.getAttrValueByName("RazonSocial") : string.Format("{0} {1} {2}", leadToConvert.getAttrValueByName("Nombre"), leadToConvert.getAttrValueByName("ApellidoPaterno"), leadToConvert.getAttrValueByName("ApellidoMaterno")) ;
                    Entity customer = mapToCustomer(nombreCliente, leadToConvert.getAttrValueByName("RFC"), owner.ToString(), leadToConvert.getAttrValueByName("Giro"), leadToConvert.getAttrValueByName("Grupo"));
                    
                    customer.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                    //Direcciones
                    JoinEntity joinInvcAddr = new JoinEntity();
                    if (leadToConvert.getAttrValueByName("Calle") != string.Empty)
                    {
                        Entity invcAddr = mapToAddress("1", leadToConvert.getAttrValueByName("Calle"), leadToConvert.getAttrValueByName("Colonia"), leadToConvert.getAttrValueByName("CP"), leadToConvert.getAttrValueByName("Delegacion"), "", leadToConvert.getAttrValueByName("numExterior"), leadToConvert.getAttrValueByName("numInterior"), leadToConvert.getAttrValueByName("Estado"), leadToConvert.getAttrValueByName("Pais"),"DireccionFact");
                        invcAddr.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                        joinInvcAddr.ChildEntity = invcAddr;
                        joinInvcAddr.JoinKey = new Attribute("cliente", "", "int");
                        selectJoinEntity invcaddrsj = new selectJoinEntity("Id",1,"Cliente");
                        joinInvcAddr.selectJoinList.Add(invcaddrsj);
                        customer.ChildEntities.Add(joinInvcAddr);
                    }
                    if (leadToConvert.getAttrValueByName("CalleEnv") != string.Empty)
                    {
                        Entity shipAddr = mapToAddress("0", leadToConvert.getAttrValueByName("CalleEnv"), leadToConvert.getAttrValueByName("ColoniaEnv"), leadToConvert.getAttrValueByName("CPEnv"), leadToConvert.getAttrValueByName("DelegacionEnv"), "", leadToConvert.getAttrValueByName("NumExteriorEnv"), leadToConvert.getAttrValueByName("NumInteriorEnv"), leadToConvert.getAttrValueByName("EstadoEnv"), leadToConvert.getAttrValueByName("PaisEnv"),"DireccionEnv");
                        shipAddr.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                        joinInvcAddr = new JoinEntity();
                        joinInvcAddr.ChildEntity = shipAddr;
                        joinInvcAddr.JoinKey = new Attribute("cliente", "", "int");
                        selectJoinEntity envcaddrsj = new selectJoinEntity("Id", 1, "Cliente");
                        joinInvcAddr.selectJoinList.Add(envcaddrsj);
                        customer.ChildEntities.Add(joinInvcAddr);
                    }
                    //Persona
                    Entity persona = mapToPerson(custType, leadToConvert.getAttrValueByName("RazonSocial"), leadToConvert.getAttrValueByName("Nombre"), leadToConvert.getAttrValueByName("ApellidoPaterno"), leadToConvert.getAttrValueByName("ApellidoMaterno"), leadToConvert.getAttrValueByName("Curp"), leadToConvert.getAttrValueByName("Email"), leadToConvert.getAttrValueByName("FechaNacimiento"));
                    persona.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                    JoinEntity joinPerson = new JoinEntity();
                    joinPerson.ChildEntity = persona;
                    joinPerson.JoinKey = new Attribute("id", "", "int");
                    selectJoinEntity personsj = new selectJoinEntity("Id", 1, "id");
                    joinPerson.selectJoinList.Add(personsj);
                    customer.ChildEntities.Add(joinPerson);

                    if (custType == 1)
                    {
                        if (leadToConvert.getAttrValueByName("TelOficina").Trim() != string.Empty)
                        {
                            Entity telContact = new Entity("TelefonoPF");
                            telContact.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                            telContact.Attributes.Add(new Attribute("Tipo", "0", "int"));
                            telContact.Attributes.Add(new Attribute("Nombre", "OF", "string"));
                            telContact.Attributes.Add(new Attribute("Numero", leadToConvert.getAttrValueByName("TelOficina"), "string"));
                            telContact.Attributes.Add(new Attribute("Extension", leadToConvert.getAttrValueByName("txtExtension"), "string"));


                            joinPerson = new JoinEntity();
                            joinPerson.ChildEntity = telContact;
                            joinPerson.JoinKey = new Attribute("Contacto", "", "int");
                            personsj = new selectJoinEntity("Id", 1, "contacto");
                            joinPerson.selectJoinList.Add(personsj);
                            persona.ChildEntities.Add(joinPerson);
                        }
                        if (leadToConvert.getAttrValueByName("TelCelular").Trim() != string.Empty)
                        {
                            Entity telCelular =  new Entity("TelefonoPF","TelCelular");
                            telCelular.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                            telCelular.Attributes.Add(new Attribute("Tipo", "2", "int"));
                            telCelular.Attributes.Add(new Attribute("Nombre", "CEL", "string"));
                            telCelular.Attributes.Add(new Attribute("Numero", leadToConvert.getAttrValueByName("TelCelular"), "string"));
                        
                            joinPerson = new JoinEntity();
                            joinPerson.ChildEntity = telCelular;
                            joinPerson.JoinKey = new Attribute("Contacto", "", "int");
                            personsj = new selectJoinEntity("Id", 1, "contacto");
                            joinPerson.selectJoinList.Add(personsj);
                            persona.ChildEntities.Add(joinPerson);
                        }
                    }

                    Entity contact = new Entity();
                    JoinEntity joinContactMoral = new JoinEntity();
                    //Contacto moral
                    if (custType != 1)
                    {
                        string CALTNombre = leadToConvert.getAttrValueByName("CAltNombre");
                        selectJoinEntity contactsj = new selectJoinEntity();
                        if (CALTNombre != string.Empty)
                        {

                            contact = mapToContact(custType, leadToConvert.getAttrValueByName("CAltNombre"), leadToConvert.getAttrValueByName("CALTEmial"), leadToConvert.getAttrValueByName("CALTPuesto"), owner.ToString());
                            contact.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                            joinContactMoral.ChildEntity = contact;
                            joinContactMoral.JoinKey = new Attribute("PersonaMoral", "", "int");
                            contactsj = new selectJoinEntity("Id", 1, "PersonaMoral");
                            joinContactMoral.selectJoinList.Add(contactsj);
                            persona.ChildEntities.Add(joinContactMoral);
                        }
                        Entity telContact = new Entity("telefonocontacto");
                        telContact.auditable = new Auditable(owner, DateTime.Now, auditable.recordStatus);
                        telContact.Attributes.Add(new Attribute("Tipo", "0", "int"));
                        telContact.Attributes.Add(new Attribute("Nombre", "OF", "string"));
                        telContact.Attributes.Add(new Attribute("Numero", leadToConvert.getAttrValueByName("CALTTelefono"), "string"));
                        joinPerson = new JoinEntity();
                        joinPerson.ChildEntity = telContact;
                        joinPerson.JoinKey = new Attribute("Contacto", "", "int");
                        contactsj = new selectJoinEntity("Id", 1, "Contacto");
                        joinPerson.selectJoinList.Add(contactsj);
                        contact.ChildEntities.Add(joinPerson);
                    }
                    
                   
                    opResult = customer.InsertRecord();
                    if (opResult.Success)
                    {
                        customer.Keys.Add(new Key("id", opResult.returnedId.ToString(), "int64"));
                        return opResult = customer.SelectRecord(false);
                    }
                    else
                    {
                        return opResult;
                    }
                }
                else
                {
                    opResult.Success = false;
                    opResult.ErrMessage = "No se encontro el prospecto con Id: " + _key;
                    return opResult;
                }
            }
            else
            {
            opResult.Success = false;
            opResult.ErrMessage = "No se encontro el prospecto con Id: " + _key;
            return opResult;
        }
        }

        private void removeAttrNotInDB()
        {

        }

        private Entity mapToCustomer(string _fullName, string _rfc, string _owner, string _giro, string _group)
        {
            Entity entity = new Entity("Cliente");
            entity.Attributes.Add(new Attribute("Permiso", "2", "int"));
            entity.Attributes.Add(new Attribute("NombreCompleto", _fullName, "string"));
            entity.Attributes.Add(new Attribute("Rfc", _rfc, "string"));
            entity.Attributes.Add(new Attribute("Ubicacion", "2185", "int"));
            entity.Attributes.Add(new Attribute("CentroDeBeneficio", "4", "int"));
            entity.Attributes.Add(new Attribute("Propietario", _owner, "int"));
            entity.Attributes.Add(new Attribute("Giro", _giro, "int"));
            entity.Attributes.Add(new Attribute("Grupo", _group, "int"));
            entity.Attributes.Add(new Attribute("EstatusCliente", "1", "int"));

            return entity;
        }

        private Entity mapToAddress(string _type, string _street, string _colony, string _zip, string _municipality, string _addrName, string _extNum, string _intNum, string _state, string _country,string EntityAlias)
        {
            Entity entity = new Entity("Direccion", EntityAlias);
            entity.Attributes.Add(new Attribute("Tipo", _type, "int"));
            entity.Attributes.Add(new Attribute("Calle", _street, "string"));
            entity.Attributes.Add(new Attribute("Colonia", _colony, "string"));
            entity.Attributes.Add(new Attribute("Cp", _zip, "string"));
            entity.Attributes.Add(new Attribute("Delegacion", _municipality, "string"));
            entity.Attributes.Add(new Attribute("Nombre", _addrName == string.Empty? _street + " " + _extNum: _addrName , "string"));
            entity.Attributes.Add(new Attribute("NumExterior", _extNum, "string"));
            entity.Attributes.Add(new Attribute("NumInterior", _intNum, "string"));
            entity.Attributes.Add(new Attribute("Estado", _state, "int"));
            entity.Attributes.Add(new Attribute("Pais", _country, "int"));

            return entity;
        }
        private Entity mapToContact(int _type, string _fullName, string _email, string _jobTitle, string _owner)
        {
            Entity entity = new Entity();
            if (_type != 1)
            {
                entity.EntityName = "ContactoPersonaMoral";
                entity.EntityAlias = entity.EntityName;
                entity.Attributes.Add(new Attribute("Nombre", _fullName, "string"));
                entity.Attributes.Add(new Attribute("Email", _email, "string"));
                entity.Attributes.Add(new Attribute("Puesto", _jobTitle, "string"));
                entity.Attributes.Add(new Attribute("Ubicacion", "2185", "int"));
                entity.Attributes.Add(new Attribute("CentroDeBeneficio", "4", "int"));
                entity.Attributes.Add(new Attribute("Propietario", _owner, "string"));
                entity.Attributes.Add(new Attribute("Permiso", "2", "int"));
            }
            else
            {
                entity.EntityName = "ContactoSimple";
                entity.EntityAlias = entity.EntityName;
                entity.Attributes.Add(new Attribute("NombreCompleto", _fullName, "string"));
                entity.Attributes.Add(new Attribute("Email", _email, "string"));
            }
            return entity;
        }

        private Entity mapToPerson(int _type, string _invcName, string _firstName, string _lastName, string _mName, string _curp, string _email, string _birthDate)
        {
            Entity entity = new Entity();
            if (_type == 1)
            {
                entity.EntityName = "PersonaFisica";
                entity.EntityAlias = entity.EntityName;
                entity.Attributes.Add(new Attribute("EstadoCivil", "0", "int"));
                entity.Attributes.Add(new Attribute("Sexo", "0", "int"));
                entity.Attributes.Add(new Attribute("ApellidoMaterno", _mName, "string"));
                entity.Attributes.Add(new Attribute("ApellidoPaterno", _lastName, "string"));
                entity.Attributes.Add(new Attribute("Curp", _curp, "string"));
                entity.Attributes.Add(new Attribute("Email", _email, "string"));
                entity.Attributes.Add(new Attribute("FechaNacimiento", _birthDate, "datetime"));
                entity.Attributes.Add(new Attribute("Nombre", _firstName, "string"));
            }
            else
            {
                entity.EntityName = "PersonaMoral";
                entity.EntityAlias = entity.EntityName;
                entity.Attributes.Add(new Attribute("RazonSocial", _invcName, "string"));
            }
            return entity;
        }
    }
}
