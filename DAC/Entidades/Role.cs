﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Entidades
{
    [DataContract(Name = "Cliente", Namespace = "")]
    class Role : Entity
    {
        public Role(): base()
        {
            EntityName = "securityrole";
            EntityAlias = EntityName;
        }

        public override OperationResult InsertRecord()
        {

            OperationResult opRes = this.odac.InsertRecord(this.Attributes, this.EntityName, true);
            
            if (opRes.Success)
            {
                Entity entityTable = new Entity("EntityTable");
                entityTable.Attributes.Add(new Attribute("EntityId"));
                entityTable.Attributes.Add(new Attribute("EntityName"));

                OperationResult result = entityTable.SelectRecord(false);
                if (result.Success && result.RetVal.Count > 0)
                {
                    foreach (Entity entity in result.RetVal)
                    {
                        Entity secPriv = new Entity("SecurityPrivilege");
                        secPriv.Attributes.Add(new Attribute("RoleId", opRes.returnedId.ToString(), "int"));
                        secPriv.Attributes.Add(new Attribute("EntityId", entity.getAttrValueByName("EntityId"), "int"));
                        secPriv.Attributes.Add(new Attribute("Privilege", "0", "int"));

                        OperationResult secPrivResult = odac.InsertRecord(secPriv.Attributes, secPriv.EntityName);
                    }
                }
            }

            return opRes;
        }
    }
}
