﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Entidades
{
        [DataContract(Name = "UserContext", Namespace = "")]
        public class UserContext
        {
            [DataMember]
            public string userId { get; set; }
            [DataMember]
            public string businessUnit { get; set; }
            [DataMember]
            public string scope { get; set; } //"us", "bu", "gb
        }
}
