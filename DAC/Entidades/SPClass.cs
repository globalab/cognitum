﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Entidades
{
    public class SpFunctions
    {
        private ClientContext context;
        private Web rootWeb;
        private string folderName, folderRoot;
        public SpFunctions(string _url, string _userName, string _password, string _folderName, string _folderRoot)
        {
            string url, username, password;
            folderName = _folderName;
            folderRoot = _folderRoot;
            url = _url;
            username = _userName;
            password = _password;
            context = new ClientContext(url);
            var secure = new SecureString();
            foreach (char c in password)
            {
                secure.AppendChar(c);
            }
            Microsoft.SharePoint.Client.SharePointOnlineCredentials credT = new SharePointOnlineCredentials(username, secure);
            context.Credentials = credT;
            rootWeb = context.Web;
            context.Load(rootWeb);
        }

        public AttachmentList getFilesFromSP(string entityName, string id, string parentId)
        {
            AttachmentList listResults = new AttachmentList();
            listResults.ListFile = new List<Attachment>();
            string path = folderRoot;
            if (entityName.ToLower() != "cliente")
            {
                path = path + "/" + parentId + "/" + id;
            }
            else
            {
                path = path + "/" + id;
            }

            try
            {
                Web web = context.Web;

                Microsoft.SharePoint.Client.List list = web.Lists.GetByTitle(folderName);
                context.Load(list.RootFolder);
                context.ExecuteQuery();

                Folder NewFolder = EnsureFolder(context, list.RootFolder, path);
                context.Load(NewFolder.Files);
                context.ExecuteQuery();
                if (NewFolder.Files.Count > 0)
                {
                    foreach (Microsoft.SharePoint.Client.File file in NewFolder.Files)
                    {
                        listResults.Success = true;
                        string absoluteFileURL = NewFolder.ServerRelativeUrl + "/" + file.Name;
                        string fullFileURL = web.Url + "/" + folderName + "/" + path + "/" + file.Name;
                        Attachment att = new Attachment();
                        att.FileName = file.Name;
                        att.spUrlFile = absoluteFileURL;
                        att.EntityName = entityName;
                        att.parentId = parentId;
                        att.id = id;
                        att.folderToSave = string.Empty;
                        listResults.ListFile.Add(att);
                    }
                }
            }
            catch (Exception exc)
            {
            }
            return listResults;
        }
        public Attachment getFile(string entityName, string id, string parentId, string _url, string folderToSave)
        {
            Attachment att = new Attachment();
            att.FoundIt = false;
            string path = folderRoot;
            if (entityName.ToLower() != "cliente")
            {
                path = path + "/" + parentId + "/" + id;
            }
            else
            {
                path = path + "/" + id;
            }
            try
            {
                Web web = context.Web;

                Microsoft.SharePoint.Client.List list = web.Lists.GetByTitle(folderName);
                context.Load(list.RootFolder);
                context.ExecuteQuery();

                Folder NewFolder = EnsureFolder(context, list.RootFolder, path);
                Microsoft.SharePoint.Client.File file = web.GetFileByServerRelativeUrl(_url);
                context.Load(file);
                context.ExecuteQuery();
                FileInformation fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, _url);

                using (var fileStream = new FileStream(folderToSave + "\\" + file.Name, FileMode.Create))
                    fileInfo.Stream.CopyTo(fileStream);

                att.FoundIt = true;
                att.FileName = file.Name;
            }
            catch (Exception exc)
            {
            }
            return att;
        }
        public string UploadDocument(string entityName, string id, string parentId, string folderName, string fileName, Stream FileContent)
        {
            string resultado = "";
            string path = folderRoot;
            if (entityName.ToLower() != "cliente")
            {
                path = path + "/" + parentId + "/" + id;
            }
            else
            {
                path = path + "/" + id;
            }
            Web web = context.Web;
            fileName = validateFileName(fileName);
            Microsoft.SharePoint.Client.List list = web.Lists.GetByTitle(folderName);
            context.Load(list.RootFolder);
            context.ExecuteQuery();

            Folder NewFolder = EnsureFolder(context, list.RootFolder, path);

            string absoluteFileURL = NewFolder.ServerRelativeUrl + "/" + fileName; //;  // web.ServerRelativeUrl + "/" + folderNAme + "/" + urlNuevoFolder + "/Nuevo.xlsx";
            string fullFileURL = web.Url + "/" + folderName + "/" + path + "/" + fileName; // Nuevo.xlsx";

            Microsoft.SharePoint.Client.File.SaveBinaryDirect(context,
               Uri.EscapeUriString(absoluteFileURL), FileContent, true);
            resultado = absoluteFileURL;

            return resultado;
        }
        private string validateFileName(string _filename)
        {
            string[] validateChars = new string[] { "\"", "#", "$", "%", "&", "¿", "?", "Ñ", "¨", "*", "ñ", "/" };
            foreach (string validate in validateChars)
            {
                while (_filename.Contains(validate))
                {
                    _filename = _filename.Replace(validate, "");
                }
            }
            if (_filename.Trim().Length > 112)
            {
                _filename = _filename.Substring(0, 111);
            }
            return _filename;
        }
        private static Folder EnsureFolder(ClientContext ctx, Folder ParentFolder, string FolderPath)
        {
            //Split up the incoming path so we have the first element as the a new sub-folder name 
            //and add it to ParentFolder folders collection
            string[] PathElements = FolderPath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string Head = PathElements[0];
            Folder NewFolder = ParentFolder.Folders.Add(Head);
            ctx.Load(NewFolder);
            ctx.ExecuteQuery();

            //If we have subfolders to create then the length of PathElements will be greater than 1
            if (PathElements.Length > 1)
            {
                //If we have more nested folders to create then reassemble the folder path using what we have left i.e. the tail
                string Tail = string.Empty;
                for (int i = 1; i < PathElements.Length; i++)
                    Tail = Tail + "/" + PathElements[i];

                //Then make a recursive call to create the next subfolder
                return EnsureFolder(ctx, NewFolder, Tail);
            }
            else
                //This ensures that the folder at the end of the chain gets returned
                return NewFolder;
        }
    }

    [DataContract(Name = "Attachment", Namespace = "")]
    public class Attachment
    {
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string parentId { get; set; }
        [DataMember]
        public byte[] FileContent { get; set; }
        [DataMember]
        public string spUrlFile { get; set; }
        [DataMember]
        public string folderToSave { get; set; }
        [DataMember]
        public bool FoundIt { get; set; }
    }

    [DataContract(Name = "AttachmentList", Namespace = "")]
    public class AttachmentList
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public List<Attachment> ListFile { get; set; }
    }
}
