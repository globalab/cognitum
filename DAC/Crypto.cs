﻿    using System;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography;
    using System.Text;

namespace DAC.Crypto
{
    public class Cipher 
    {
        private const string CIPHER_PASSWORD = "globalab";

        public Cipher()
        {
            this.Key = "globalab";
            this.IV = "globalab";
        }

        public string bytesToHex(byte[] bytes)
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder((int)bytes.Length);
            for (int i = 0; i < bytes.Length; i = (int)(i + 1))
            {
                string str = ((byte)bytes[i]).ToString("X");
                if (str.Length < 2)
                {
                    str = "0" + str;
                }
                builder.Append(str);
            }
            return builder.ToString();
        }

        public string Cifrar(string Folio)
        {
            string str = "";
            try
            {
                if (Folio.Length < 8)
                {
                    Folio = this.ConvertirCad_formtoOchoDg(Folio);
                }
                this.Key = "globalab";
                this.IV = "globalab";
                byte[] bytes = System.Convert.FromBase64String(this.CifrarCadena(Folio));
                str = this.bytesToHex(bytes);
            }
            catch (System.Exception exception)
            {
                System.Console.WriteLine(exception.Message);
            }
            return str;
        }

        public string CifrarCadena(string cadenaOriginal)
        {
            if ((this.Key == null) || (this.IV == null))
            {
                throw new System.Exception("Error al inicializar la clave y el vector");
            }
            byte[] key = this.makeKeyByteArray();
            byte[] iV = this.makeIVByteArray();
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(cadenaOriginal);
            System.IO.MemoryStream stream = new System.IO.MemoryStream((int)(cadenaOriginal.Length * 2));
            System.Security.Cryptography.ICryptoTransform serviceProvider = new CryptoServiceProvider(CryptoProvider.DES, CryptoAction.Encrypt).GetServiceProvider(key, iV);
            System.Security.Cryptography.CryptoStream stream2 = new System.Security.Cryptography.CryptoStream(stream, serviceProvider, System.Security.Cryptography.CryptoStreamMode.Write);
            stream2.Write(bytes, 0, (int)bytes.Length);
            stream2.Close();
            return System.Convert.ToBase64String(stream.ToArray());
        }

        public string ConvertirCad_edo_Original(string CadenaDescifrada)
        {
            string str = "";
            try
            {
                string[] strArray = CadenaDescifrada.Split((char[])new char[] { '*' });
                for (int i = 0; i < strArray.Length; i = (int)(i + 1))
                {
                    if (strArray[i].ToString() != "")
                    {
                        str = str + strArray[i].ToString();
                    }
                }
            }
            catch (System.Exception)
            {
            }
            return str;
        }

        public string ConvertirCad_formtoOchoDg(string Cadena)
        {
            string str = "";
            try
            {
                for (int i = Cadena.Length; i < 8; i = (int)(i + 1))
                {
                    str = str + "*";
                }
                str = str + Cadena;
            }
            catch (System.Exception)
            {
            }
            return str;
        }

        public string Descifrar(string Cifrado)
        {
            string str = "";
            try
            {
                this.Key = "globalab";
                this.IV = "globalab";
                str = this.ConvertirCad_edo_Original(this.DescifrarCadena(this.hexToBytes(Cifrado)));
            }
            catch (System.Exception exception)
            {
                System.Console.WriteLine(exception.Message);
            }
            return str;
        }

        public string DescifrarCadena(string cadenaCifrada)
        {
            cadenaCifrada = cadenaCifrada.Trim();
            if ((this.Key == null) || (this.IV == null))
            {
                throw new System.Exception("Error al inicializar la clave y el vector.");
            }
            byte[] key = this.makeKeyByteArray();
            byte[] iV = this.makeIVByteArray();
            byte[] buffer3 = System.Convert.FromBase64String(cadenaCifrada);
            System.IO.MemoryStream stream = new System.IO.MemoryStream(cadenaCifrada.Length);
            System.Security.Cryptography.ICryptoTransform serviceProvider = new CryptoServiceProvider(CryptoProvider.DES, CryptoAction.Desencrypt).GetServiceProvider(key, iV);
            System.Security.Cryptography.CryptoStream stream2 = new System.Security.Cryptography.CryptoStream(stream, serviceProvider, System.Security.Cryptography.CryptoStreamMode.Write);
            stream2.Write(buffer3, 0, (int)buffer3.Length);
            stream2.Close();
            return System.Text.Encoding.UTF8.GetString(stream.ToArray());
        }

        public string hexToBytes(string Data)
        {
            if ((Data.Length % 2) > 0)
            {
                Data = "0" + Data;
            }
            byte[] buffer = new byte[Data.Length / 2];
            for (int i = 0; i < Data.Length; i = (int)(i + 2))
            {
                buffer[i / 2] = System.Convert.ToByte(Data.Substring(i, 2), 0x10);
            }
            return System.Convert.ToBase64String(buffer);
        }

        private byte[] makeIVByteArray()
        {
            if (this.IV.Length < 8)
            {
                this.IV = this.IV.PadRight(8);
            }
            else if (this.IV.Length > 8)
            {
                this.IV = this.IV.Substring(0, 8);
            }
            return System.Text.Encoding.UTF8.GetBytes(this.IV);
        }

        private byte[] makeKeyByteArray()
        {
            if (this.Key.Length < 8)
            {
                this.Key = this.Key.PadRight(8);
            }
            else if (this.Key.Length > 8)
            {
                this.Key = this.Key.Substring(0, 8);
            }
            return System.Text.Encoding.UTF8.GetBytes(this.Key);
        }

        public string IV { get; set; }

        public string Key { get; set; }

        
    }
}

