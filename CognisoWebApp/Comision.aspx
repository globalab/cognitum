﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comision.aspx.cs" Inherits="CognisoWebApp.Comision" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Comisión Asociado</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <link href="Content/cogniso.css" rel="stylesheet" />
     <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtDirectorAsociado.ClientID%>").select2({
                placeholder: "Seleccione un Director Asociado",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });
      </script>

    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     
        <div class="container">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Comisión asociado</asp:Label></h2>
            </div>
           <hr class="float-left">
            <div class="panel-heading clearfix" >
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" />
                    <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>

            <div class="containerRT3" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <%--<li class="nav-item"  runat="server">
                            <a class="nav-link" id="Members-tab" data-toggle="tab" href="#Miembros" role="tab" aria-controls="Miembros" aria-selected="false">Miembros del Grupo</a>
                      </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>--%>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT4">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDirectorAsociado" runat="server" Text="Director Asociado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtDirectorAsociado" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblComision" runat="server" Text="Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtComision" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtComisionVal" controltovalidate="txtComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPoliza" runat="server" Text="Poliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtPoliza" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT5">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                <asp:HiddenField ID="PaneName" runat="server" />
            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>
