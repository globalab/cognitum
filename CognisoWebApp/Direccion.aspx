﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Direccion.aspx.cs" Inherits="CognisoWebApp.Direccion" %>
<!DOCTYPE html>  
<html lang="en">  
<head>  
    <title>Giros</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
   
<script lang="javascript" type="text/javascript">
    var submit = 0;
    function CheckIsRepeat() {
        if (++submit > 1) {
           return false;
        }
    }
    </script>
</head>  
<body>
     <form id="form1" runat="server">

     
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Dirección</asp:Label>
            </div>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClientClick="return CheckIsRepeat();" OnClick="BtnSave_Click" />
                     <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                </div>
            </div>

            <div class="container" runat="server" id="DireccionGral">
                <div id="errMess" >

                </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="Direccion-tab" data-toggle="tab" href="#Direccion" role="tab" aria-controls="Direccion" aria-selected="false">Dirección</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Direccion" role="tabpanel" aria-labelledby="Direccion-tab">


                            <div class="container">

                                <div class="row">
                                    
                                        <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtName" runat="server" AutoPostBack="true" CssClass="form-control" TabIndex="1" >
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valNombre" runat="server" ErrorMessage="Nombre" ForeColor="Red" ControlToValidate="txtName" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                <div class="row">
                                    
                                        <asp:Label ID="lblTipo" runat="server" Text="Tipo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="cmbTipo" runat="server" AutoPostBack="true" CssClass="form-control" TabIndex="2" >
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valTipo" runat="server" ErrorMessage="Tipo" ForeColor="Red" ControlToValidate="cmbTipo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                <div class="row">
                                    
                                        <asp:Label ID="lblCP" runat="server" Text="C.P."></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCP" runat="server" AutoPostBack="true" CssClass="form-control" TabIndex="3" OnTextChanged="txtCP_TextChanged">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valCP" runat="server" ErrorMessage="C.P." ForeColor="Red" ControlToValidate="txtCP" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                   
                                
                                <div class="row">
                                    
                                        <asp:Label ID="lblCalle" runat="server" Text="Calle"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCalle" runat="server" CssClass="form-control" TabIndex="11">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valCalle" runat="server" ErrorMessage="Calle" ForeColor="Red" ControlToValidate="txtCalle" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    
                                        <asp:Label ID="lblNumeroExt" runat="server" Text="Número Exterior"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNumeroExt" runat="server" CssClass="form-control" TabIndex="11">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valNumInt" runat="server" ErrorMessage="Número Exterior" ForeColor="Red" ControlToValidate="txtNumeroExt" Text="*"></asp:RequiredFieldValidator>
                                        </div>                                  
                                </div>
                                <div class="row">
                                    
                                        <asp:Label ID="lblNumeroInt" runat="server" Text="Número Interior"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNumeroInt" runat="server" CssClass="form-control" TabIndex="11">
                                            </asp:TextBox>
                                        </div>
                                    
                                </div>
                                <div class="row">
                                    
                                        <asp:Label ID="lblColonia" runat="server" Text="Colonia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtColonia" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valColonia" runat="server" ErrorMessage="Colonia" ForeColor="Red" ControlToValidate="txtColonia" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    
                                </div>
                                <div class="row">
                                    <asp:Label ID="lblPais" runat="server" Text="Pais"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPais" runat="server" CssClass="form-control" TabIndex="15">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valPais" runat="server" ErrorMessage="Pais" ForeColor="Red" ControlToValidate="txtPais" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                </div>
                                <div class="row">
                                     <asp:Label ID="lblEstado" runat="server" Text="Estado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstado" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valEstado" runat="server" ErrorMessage="Estado" ForeColor="Red" ControlToValidate="txtEstado" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    
                                </div>
                                <div class="row">
                                        <asp:Label ID="lblDelegacion" runat="server" Text="Delegacion"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDelegacion" runat="server" CssClass="form-control" TabIndex="19">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valDelegacion" runat="server" ErrorMessage="Delegacion" ForeColor="Red" ControlToValidate="txtDelegacion" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    
                                </div>
                                <div class="row">
                                        <asp:Label ID="lblCustId" runat="server" Text="Cliente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCust" runat="server" CssClass="form-control" TabIndex="19">
                                            </asp:TextBox>
                                        </div>
                                    
                                </div>
                            </div>
                               
                            </div>

                      
                      
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                           
                          </div>
                      </div> 
                    </div>
                </div>
                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="DirId" runat="server" />
                <asp:HiddenField ID="CId" runat="server" />
            </div>
        </div>
</form>
</body>
</html>
