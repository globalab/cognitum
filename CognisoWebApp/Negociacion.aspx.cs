﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Negociacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                showPanels();
                initCatalog();
                object id = Request.QueryString["id"];
                if (id != null)
                {
                    NegId.Value = id.ToString();
                    this.searchNegociacion(id.ToString());
                    this.searchForPolizas(id.ToString());
                }
                else
                {
                    txtAseguradora.Items.Add(new ListItem("", ""));
                    txtAseguradora.SelectedValue = "";
                    txtRamo.Items.Add(new ListItem("", ""));
                    txtRamo.SelectedValue = "";
                    txtCliente.Items.Add(new ListItem("", ""));
                    txtCliente.SelectedValue = "";
                }
                
                
                
            }
            else
            {
                this.showTab();
            }
        }

        private void initCatalog()
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Aseguradora.useAuditable = false;
            JoinEntity joinCust = new JoinEntity();
            joinCust.ChildEntity = new Entity("Cliente");
            joinCust.JoinKey = new Models.Attribute("Id");
            joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
            selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
            joinCust.selectJoinList.Add(custsj);
            joinCust.JoinType = JoinType.Inner;
            Aseguradora.ChildEntities.Add(joinCust);
            listResult = SiteFunctions.GetValues(Aseguradora, out errmess);
            txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtAseguradora.DataValueField = "Id";
            txtAseguradora.DataTextField = "ClienteNombreCompleto";
            txtAseguradora.DataBind();

            Entity Ramo = new Entity("Ramo");
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Ramo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Ramo, out errmess);
            txtRamo.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtRamo.DataValueField = "Id";
            txtRamo.DataTextField = "Nombre";
            txtRamo.DataBind();

            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.useAuditable = false;
            listResult = SiteFunctions.GetValues(Cliente, out errmess);
            txtCliente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtCliente.DataValueField = "Id";
            txtCliente.DataTextField = "NombreCompleto";
            txtCliente.DataBind();


        }
        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVPolizasList.PageIndex = e.NewPageIndex;
            GVPolizasList.DataBind();
            searchForPolizas(NegId.Value);

        }
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveNegotiation();
        }

        private void searchNegociacion(string _id)
        {
            string errMsg = string.Empty, idCliente = string.Empty, idAseguradora = string.Empty, idRamo = string.Empty;
            List<Entity> entities = new List<Entity>();
            Entity negociacion = new Entity("Negociacion");
            negociacion.useAuditable = true;
            negociacion.Keys.Add(new Models.Key("id", _id, "int"));

            List<Entity> negResult = SiteFunctions.GetValues(negociacion, out errMsg);
            if (negResult.Count > 0)
            {
                Entity negoc = negResult[0];

                idCliente = negoc.getAttrValueByName("Cliente");
                idAseguradora = negoc.getAttrValueByName("Aseguradora");
                idRamo = negoc.getAttrValueByName("Ramo");
                Owner.Value = negoc.getAttrValueByName("Propietario");
            }

            Entity cliente = new Entity("Cliente");
            cliente.Keys.Add(new Models.Key("id", idCliente, "int"));
            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Keys.Add(new Models.Key("id", idAseguradora, "int"));
            Entity ramo = new Entity("Ramo");
            ramo.Keys.Add(new Models.Key("id", idRamo, "int"));
            Entity slip = new Entity("SlipNegociacion");
            slip.Keys.Add(new Models.Key("id", _id, "int"));

            entities.Add(cliente);
            entities.Add(aseguradora);
            entities.Add(ramo);
            entities.Add(negociacion);
            entities.Add(slip);

            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            mapNegoSearch(result,_id);
           
        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //General.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";

            //Contacto.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //ContactoAlterno.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";


        }
        private void mapNegoSearch(List<OperationResult> result,string id)
        {
            string errMsg;
            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if (opResult.RetVal.Count > 0)
                    {
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {
                            case "cliente":


                                txtCliente.SelectedValue = entity.getAttrValueByName("Id");
                                CustRFC.Value = entity.getAttrValueByName("Rfc");
                                break;
                            case "aseguradora":
                                txtAseguradora.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "ramo":
                                txtRamo.SelectedValue = entity.getAttrValueByName("Id");
                                break;

                            case "negociacion":
                                txtNoNegociacion.Text = entity.getAttrValueByName("NumeroNegociacion");
                                txtPorComision.Text = entity.getAttrValueByName("ComisionPct");
                                txtPorBono.Text = entity.getAttrValueByName("BonoPct");
                                txtPorSobreComision.Text = entity.getAttrValueByName("SobrecomisionPct");
                                txtPrimaNetaAprox.Text = entity.getAttrValueByName("PrimaNetaAproximada");
                                txtDiasNegociacionEntrega.Text = entity.getAttrValueByName("DiasNegociadosEntrega");
                                txtId.Text = id;
                                DateTime fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaAdd"));
                                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaUMod"));
                                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                Entity UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioAdd"), "int64"));
                                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioUMod"), "int64"));
                                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                Entity Ubicacion = new Entity("Ubicacion");
                                Ubicacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
                                Ubicacion.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                                Ubicacion.Keys.Add(new Key("Id", entity.getAttrValueByName("Ubicacion"), "int"));
                                List<Entity> listEntities = SiteFunctions.GetValues(Ubicacion, out errMsg);
                                txtUbicacion.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                                txtUbicacion.DataTextField = "Nombre";
                                txtUbicacion.DataValueField = "Id";
                                txtUbicacion.DataBind();

                                Entity CentroDeBeneficio = new Entity("CentroDeBeneficio");
                                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Id", "", "int"));
                                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                                CentroDeBeneficio.Keys.Add(new Key("Id", entity.getAttrValueByName("CentroDeBeneficio"), "int"));
                                listEntities = SiteFunctions.GetValues(CentroDeBeneficio, out errMsg);
                                txtCentroBeneficio.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                                txtCentroBeneficio.DataTextField = "Nombre";
                                txtCentroBeneficio.DataValueField = "Id";
                                txtCentroBeneficio.DataBind();
                                break;
                            case "slipnegociacion":
                                txtComentarios.Text = entity.getAttrValueByName("Descripcion");
                                break;
                        }
                    }
                }
            }
            showPanels();
        }

        private void SaveNegotiation()
        {

            if (txtNoNegociacion.Text.Trim() == "")
            {
                showMessage("El campo Número de Negocición es requerido",true);
            }
            else
            {
                JoinEntity joinEntity = new JoinEntity();
                Entity Negociacion = new Entity("Negociacion");
                Auditable audi = new Auditable();
                string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
                audi.userId = int.Parse(userId);
                audi.opDate = DateTime.Now;
                if (Owner.Value == "")
                {
                    Owner.Value = userId;
                }
                

                Negociacion.Attributes.Add(new Models.Attribute("Cliente", txtCliente.SelectedValue, "int"));
                Negociacion.Attributes.Add(new Models.Attribute("Aseguradora", txtAseguradora.SelectedValue, "int"));
                Negociacion.Attributes.Add(new Models.Attribute("Ramo", txtRamo.SelectedValue, "int"));
                Negociacion.Attributes.Add(new Models.Attribute("ComisionPct", txtPorComision.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("BonoPct", txtPorBono.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("SobrecomisionPct", txtPorSobreComision.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("PrimaNetaAproximada", txtPrimaNetaAprox.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("DiasNegociadosEntrega", txtDiasNegociacionEntrega.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("NumeroNegociacion", txtNoNegociacion.Text.Trim(), "string"));
                Negociacion.Attributes.Add(new Models.Attribute("Ubicacion", txtUbicacion.SelectedValue, "int"));
                Negociacion.Attributes.Add(new Models.Attribute("CentroDeBeneficio", txtCentroBeneficio.SelectedValue, "int"));

                Negociacion.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
                Negociacion.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
                Negociacion.useAuditable = true;

                Entity slip = new Entity("SlipNegociacion");
                slip.Attributes.Add(new Models.Attribute("Descripcion", txtComentarios.Text, "string"));

                joinEntity = new JoinEntity();
                joinEntity.ChildEntity = slip;
                joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                Negociacion.ChildEntities.Add(joinEntity);

                string errmess = "";
                object id = Request.QueryString["id"];
                Method method = Method.POST;
                if (id != null)
                {
                    method = Method.PUT;
                    Negociacion.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                    audi.entityId = Convert.ToInt32(id);

                }
                Negociacion.auditable = audi;
                if (SiteFunctions.SaveEntity(Negociacion, method, out errmess))
                {
                    showMessage("Se guardo correctamente la negociación", false);
                    if (string.IsNullOrEmpty(NegId.Value))
                    {
                        Response.Redirect(string.Format("Negociacion.aspx?Id={0}", errmess));
                    }
                }
                else
                {
                    showMessage(errmess, true);
                }
            }
        }


        private void searchForPolizas(string _id)
        {

            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity poliza = new Entity("Poliza");
            poliza.Attributes.Add(new Models.Attribute("id", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Contratante", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Agente", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ramo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Aseguradora", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ejecutivo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", "", "string"));
            poliza.Keys.Add(new Models.Key("Negociacion", _id, "int"));
            poliza.useAuditable = false;

            Entity negociacion = new Entity("Negociacion");
            negociacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
            negociacion.Attributes.Add(new Models.Attribute("NumeroNegociacion", "", "string"));
            negociacion.useAuditable = false;
            entities.Add(negociacion);

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto", "", "string"));
            cliente.useAuditable = false;
            entities.Add(cliente);

            Entity agente = new Entity("Agente");
            agente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            agente.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            agente.Attributes.Add(new Models.Attribute("Nombre"));
            agente.useAuditable = false;
            entities.Add(agente);

            Entity ramo = new Entity("Ramo");
            ramo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ramo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ramo.useAuditable = false;
            entities.Add(ramo);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;
            entities.Add(aseguradora);

            Entity ejecutivo = new Entity("Usuario");
            ejecutivo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ejecutivo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ejecutivo.useAuditable = false;
            entities.Add(ejecutivo);


            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            poliza.logicalOperator = 2;

            List<Entity> listLeads = SiteFunctions.GetValues(poliza, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);
            foreach (DataRow row in dtLeadList.Rows)
            {
                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        if(opResult.RetVal.Count > 0)
                        { 
                            Entity entity = opResult.RetVal[0];
                            switch (entity.EntityName.ToLower())
                            {

                                case "cliente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[1].ToString().Trim() == nameId.Trim())
                                        {
                                            row[1] = item.getAttrValueByName("NombreCompleto");
                                            break;
                                        }
                                    }
                                    break;
                                case "agente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[2].ToString().Trim() == nameId.Trim())
                                        {
                                            row[2] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;
                                case "ramo":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[3].ToString().Trim() == nameId.Trim())
                                        {
                                            row[3] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;
                                case "aseguradora":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[4].ToString().Trim() == nameId.Trim())
                                        {
                                            row[4] = item.getAttrValueByName("Clave");
                                            break;
                                        }
                                    }
                                    break;

                                case "usuario":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[5].ToString().Trim() == nameId.Trim())
                                        {
                                            row[5] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;


                            }

                        }
                    }
                }
            }

            GVPolizasList.DataSource = dtLeadList;
            GVPolizasList.DataBind();
        }

        protected void BtnAttachment_Click(object sender, ImageClickEventArgs e)
        {
            var sURL = "Attachment.aspx?id=" + NegId.Value + "&entityName=Negociacion" +"&parentId="+CustRFC.Value;

            string vtn = "window.open(" +"'"+ sURL + "'"+",'scrollbars=yes,resizable=yes','height=100', 'width=100')";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", vtn, true);
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void btnAseguradora_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {

                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Aseguradora = new Entity("Aseguradora");
                Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
                Aseguradora.useAuditable = false;
                JoinEntity joinCust = new JoinEntity();
                joinCust.ChildEntity = new Entity("Cliente");
                joinCust.JoinKey = new Models.Attribute("Id");
                joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
                selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
                joinCust.selectJoinList.Add(custsj);
                joinCust.JoinType = JoinType.Inner;
                Aseguradora.ChildEntities.Add(joinCust);
                listResult = SiteFunctions.GetValues(Aseguradora, out errmess);
                txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtAseguradora.DataValueField = "Id";
                txtAseguradora.DataTextField = "ClienteNombreCompleto";
                txtAseguradora.DataBind();
                txtAseguradora.Text = Session["retval"].ToString();
                Session["retval"] = "";

            }
        }

        protected void btnRamo_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {

                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Ramo = new Entity("Ramo");
                Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Ramo.useAuditable = false;
                listResult = SiteFunctions.GetValues(Ramo, out errmess);
                txtRamo.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtRamo.DataValueField = "Id";
                txtRamo.DataTextField = "Clave";
                txtRamo.DataBind();
                txtRamo.Text = Session["retval"].ToString();
                Session["retval"] = "";

            }
        }

        protected void txtCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            string errmess;
            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Cliente.Attributes.Add(new Models.Attribute("Ubicacion", "", "string"));
            Cliente.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "", "string"));
            Cliente.Keys.Add(new Key("Id", txtCliente.SelectedValue, "int"));

            List<Entity> listResult = SiteFunctions.GetValues(Cliente, out errmess);
            if (listResult.Count > 0)
            {

                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                Ubicacion.Keys.Add(new Key("Id", listResult[0].getAttrValueByName("Ubicacion"), "int"));
                List<Entity>  listEntities = SiteFunctions.GetValues(Ubicacion, out errmess);
                txtUbicacion.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                txtUbicacion.DataTextField = "Nombre";
                txtUbicacion.DataValueField = "Id";
                txtUbicacion.DataBind();

                Entity CentroDeBeneficio = new Entity("CentroDeBeneficio");
                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Id", "", "int"));
                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                CentroDeBeneficio.Keys.Add(new Key("Id", listResult[0].getAttrValueByName("CentroDeBeneficio"), "int"));
                listEntities = SiteFunctions.GetValues(CentroDeBeneficio, out errmess);
                txtCentroBeneficio.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                txtCentroBeneficio.DataTextField = "Nombre";
                txtCentroBeneficio.DataValueField = "Id";
                txtCentroBeneficio.DataBind();

                txtUbicacion.Enabled = false;
                txtCentroBeneficio.Enabled = false;

            }
        }
        private void showTab()
        {

            // Define the name and type of the client scripts on the page.
            String csname1 = "TabScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> collapsediv(); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
    }
}