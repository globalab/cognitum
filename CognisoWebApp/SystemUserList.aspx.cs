﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class SystemUserList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                searchUsers();
            }
        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("UserPage.aspx");
        }

        private void searchUsers(string _filter = "")
        {
            string errmsg = string.Empty;
            Entity usuario = new Entity("Usuario");
            usuario.Attributes.Add(new Models.Attribute("id"));
            usuario.Attributes.Add(new Models.Attribute("Username"));
            usuario.Attributes.Add(new Models.Attribute("Nombre"));
            int filterId = 0;
            int.TryParse(_filter, out filterId);
            if (filterId != 0)
            {
                Key filter = new Key("Id", _filter, "int");
                filter.Group = 1;
                filter.LogicalOperator = 2;
                filter.WhereOperator = whereOperator.Equal;
                usuario.Keys.Add(filter);
            }

            if (_filter != string.Empty)
            {
                Key filter = new Key("Username", _filter, "string");
                filter.Group = 1;
                filter.LogicalOperator = 2;
                filter.WhereOperator = whereOperator.Equal;
                usuario.Keys.Add(filter);

                filter = new Key("Nombre", _filter, "string");
                filter.Group = 1;
                filter.LogicalOperator = 2;
                filter.WhereOperator = whereOperator.Equal;
                usuario.Keys.Add(filter);
            }
            usuario.useAuditable = false;
            List<Entity> result = SiteFunctions.GetValues(usuario, out errmsg);
            DataTable dtResults = SiteFunctions.trnsformEntityToDT(result);
            GVUsers.DataSource = dtResults;
            GVUsers.DataBind();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            searchUsers(txtSearch.Text);
        }

        protected void GVUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUsers.PageIndex = e.NewPageIndex;
            GVUsers.DataBind();
            searchUsers();
        }
    }
}