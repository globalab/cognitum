﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecurityRole.aspx.cs" Inherits="CognisoWebApp.SecurityRole" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Rol de seguridad</asp:Label>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="SecurityRolesList.aspx">Roles de seguridad</a></li>
                    <li class="breadcrumb-item active">Rol <%=RoleId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" />
                </div>
            </div>

            <div class="container" runat="server" id="RoleGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" onclick="selectPane('General');">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRoleName" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRoleName" runat="server" class="form-control" Enabled="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="gridtitle" runat="server" class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSecPriv" runat="server" Text="Privilegios"></asp:Label>
                                    </div>
                                </div>
                                <div id="gridDiv" runat="server" class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvPermisos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Id" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="entity" runat="server" Text='<%#Eval("EntityId")%>' style="font-size: 11px; color: black; font-family: Tahoma; text-align: center"></asp:Label>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Entidad" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="entityName" runat="server" Text='<%#Eval("EntityTableEntityName")%>' style="font-size: 11px; color: black; font-family: Tahoma; text-align: center"></asp:Label>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Seguridad" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="securityPriv" SelectedValue='<%# Bind ("Privilege") %>' runat="server" >
                                                            <asp:ListItem Value="0" Text=""></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="Lectura"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="Creación"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="Actualización"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="Borrado"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="RoleId" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
