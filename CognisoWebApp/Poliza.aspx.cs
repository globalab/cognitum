﻿using CognisoWebApp.Models;
using CognisoWA.Controllers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Services;
using System.Configuration;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace CognisoWebApp
{
    public partial class Poliza : System.Web.UI.Page
    {

       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            
            if (!IsPostBack)
            {
                showPanels();
                object id = Request.QueryString["id"];
                PolizaOT.Value = "OT";
                initCatalog(id);
                
                object polType = Request.QueryString["polType"];

                object descarteOk = Request.QueryString["descarteOk"];
                object panelname = Request.QueryString["panename"];
                if (polType != null)
                {
                    PolizaType.Value = polType.ToString();
                    switch (polType.ToString())
                    {
                        case "0":
                            PolizaTypeStr.Value = "Autos Individual";
                            break;
                        case "6":
                            PolizaTypeStr.Value = "Autos Flotilla";
                            //txtPrimaNeta.Enabled = false;
                            //txtGastosRecibo.Enabled = false;
                            //txtPrima.Enabled = false;
                            break;
                        case "4":
                            PolizaTypeStr.Value = "Vida Individual";
                            break;
                        case "5":
                            PolizaTypeStr.Value = "Vida Pool";
                            break;
                        case "2":
                            PolizaTypeStr.Value = "GMM Individual";
                            break;
                        case "3":
                            PolizaTypeStr.Value = "GMM Pool";
                            break;
                        case "1":
                            PolizaTypeStr.Value = "Daños";
                            break;
                    }

                    ShowTypePolicy();
                    reloadFormaPago();
                }
                if (id != null)
                {

                    initCartas(id.ToString());
                    PoliId.Value = id.ToString();
                    
                    Session["PolizaId"] = PoliId.Value;
                    this.searchPoliza(id.ToString());
                    object isReadOnly = Request.QueryString["readonly"];
                    if (isReadOnly != null)
                    {
                        if (isReadOnly.ToString() == "1")
                        {
                            setReadOnly();
                        }
                    }
                    if (descarteOk != null)
                    {
                        if (descarteOk.ToString() == "1")
                        {
                            showMessage("Descarte correcto", false);
                            txtFormaPago.Enabled = false;
                            NuevoEndoso.Visible = true;
                            divinsrecibo.Visible = true;
                            PolizaOT.Value = "Póliza";
                        }
                        
                    }
                    searchEndosos();
                    if (panelname != null)
                    {
                        PaneName.Value = panelname.ToString();
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);
                    }
                    if (txtNopoliza.Text.Trim() != "")
                    {
                        NuevoEndoso.Visible = true;
                        PolizaOT.Value = "Póliza";
                        divinsrecibo.Visible = true;
                    }
                    string op = string.Empty;
                    try
                    {
                        op = Request.QueryString["op"];
                    }
                    catch (Exception)
                    {
                    }
                    List<Entity> Pendings = new List<Entity>();
                    bool alreadyCanceled = checkifPending(SiteFunctions.getuserid(Context.User.Identity.Name, true), id.ToString(), true, out Pendings);
                    if (op == "1")
                    {
                        BtnCancel.Visible = false;
                        btnAprove.Visible = !alreadyCanceled;
                        btnReject.Visible = !alreadyCanceled;
                    }
                    else
                    {
                        btnAprove.Visible = false;
                        btnReject.Visible = false;
                        BtnCancel.Visible = alreadyCanceled;
                    }
                    if (txtPrima.Text == "")
                    {
                        txtPrima.Text = "0";
                    }
                    
                }
                else
                {
                    txtPrima.Text = "0";
                    txtInicio.Text = DateTime.Now.Date.ToShortDateString();
                    txtFin.Text = DateTime.Now.Date.AddYears(1).ToShortDateString();
                    txtFechaEmision.Text = DateTime.Now.Date.ToShortDateString();
                    txtAseguradora.Items.Add(new ListItem("", ""));
                    txtAseguradora.SelectedValue = "";
                    txtCliente.Items.Add(new ListItem("", ""));
                    txtCliente.SelectedValue = "";
                    txtRamo.Items.Add(new ListItem("", ""));
                    txtRamo.SelectedValue = "";
                    txtSubramo.Items.Add(new ListItem("", ""));
                    txtSubramo.SelectedValue = "";
                    txtAgente.Items.Add(new ListItem("", ""));
                    txtAgente.SelectedValue = "";
                    txtTitular.Items.Add(new ListItem("", ""));
                    txtTitular.SelectedValue = "";
                    txtEstatusCobranza.SelectedValue = "1";
                    txtNegociacion.Items.Add(new ListItem("", ""));
                    txtNegociacion.SelectedValue = "";
                    txtDirector.Items.Add(new ListItem("", ""));
                    txtDirector.SelectedValue = "";
                    txtEjecutivo.Items.Add(new ListItem("", ""));
                    txtEjecutivo.SelectedValue = "";
                    txtSupervisor.Items.Add(new ListItem("", ""));
                    txtSupervisor.SelectedValue = "";
                    txtEjeAseuradora.Items.Add(new ListItem("", ""));
                    txtEjeAseuradora.SelectedValue = "";
                    btnAprove.Visible = false;
                    btnReject.Visible = false;
                    BtnCancel.Visible = false;
                }
                initForm();
            }
            else
            {
                this.showTab();
            }
        }

        private void initCartas(string id)
        {
            List<Entity> plantillaspol = SearchPlantillas.ObtienePlantillasPoliza(id);
            if(plantillaspol.Count == 0)
            {
                plantillaspol = SearchPlantillas.ObtienePlantillasPolizaSinFiltros(id);
            }
            DataTable dt = SiteFunctions.trnsformEntityToDT(plantillaspol);
            rptCartas.DataSource = dt;
            rptCartas.DataBind();
        }

        [WebMethod]
        public static string generaCarta(int idpoliza, int idplantilla, string idusuario, int tipoplantilla)
        {
            CartasFunctions gencarta = new CartasFunctions();
            string carta = string.Empty;
            carta  = gencarta.GeneraCartaPoliza(idpoliza, idplantilla, idusuario);
            return carta;
        }

        public string generaCartaHTML()
        {
            int idpoliza = 0;
            int plantilla = 0;
            int.TryParse(PoliId.Value, out idpoliza);
            int.TryParse(idplantilla.Value, out plantilla);
            CartasFunctions gencarta = new CartasFunctions();
            string carta = string.Empty;
            carta = gencarta.GeneraCartaPoliza(idpoliza, plantilla, this.Page.User.Identity.Name);
            return carta;
        }
        

        private void showTab()
        {

            // Define the name and type of the client scripts on the page.
            String csname1 = "TabScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> collapsediv(); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void reloadFormaPago()
        {
            Array itemValues = System.Enum.GetValues(typeof(FormaPagoEnum));
            Array itemNames = System.Enum.GetNames(typeof(FormaPagoEnum));
            txtFormaPago.Items.Clear();
            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = null;
                switch (i)
                {
                    case 3:

                        item = new ListItem(itemNames.GetValue(13).ToString(), ((int)itemValues.GetValue(13)).ToString());
                        txtFormaPago.Items.Add(item);
                        break;
                    case 4:
                    case 5:
                    case 6:
                        
                            item = new ListItem(itemNames.GetValue(i - 1).ToString(), ((int)itemValues.GetValue(i - 1)).ToString());
                            txtFormaPago.Items.Add(item);
                        
                        break;
                    case 7:
                        item = new ListItem(itemNames.GetValue(12).ToString(), ((int)itemValues.GetValue(12)).ToString());
                        txtFormaPago.Items.Add(item);
                        break;
                    case 8:
                    case 9:
                    case 10:
                        item = new ListItem(itemNames.GetValue(i - 2).ToString(), ((int)itemValues.GetValue(i - 2)).ToString());
                        txtFormaPago.Items.Add(item);
                        break;
                    case 11:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(14).ToString(), ((int)itemValues.GetValue(14)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                    case 12:
                    case 13:
                    case 14:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(i - 3).ToString(), ((int)itemValues.GetValue(i - 3)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                    default:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                }
            }
        }
        private void setReadOnly()
        {
            txtAgente.Enabled = false;
            txtAseguradora.Enabled = false;
            txtBono.Enabled = false;
            txtBonoPor.Enabled = false;
            txtCliente.Enabled = false;
            txtCoberturas.Enabled = false;
            txtComision.Enabled = false;
            txtComisionPor.Enabled = false;
            txtCondSpec.Enabled = false;
            txtDescuento.Enabled = false;
            txtDirCobro.Enabled = false;
            txtDirector.Enabled = false;
            txtDirEnvio.Enabled = false;
            txtDirFiscal.Enabled = false;
            txtEjeAseuradora.Enabled = false;
            txtEjecutivo.Enabled = false;
            txtEstatus.Enabled = false;
            txtEstatusCobranza.Enabled = false;
            txtFechaEnvio.Enabled = false;
            txtFin.Enabled = false;
            txtFormaPago.Enabled = false;
            txtGastos.Enabled = false;
            txtGrupo.Enabled = false;
            txtImpuesto.Enabled = false;
            txtImpuestoPor.Enabled = false;
            txtInicio.Enabled = false;
            //txtMesa.Enabled = false;
            txtMoneda.Enabled = false;
            txtNegociacion.Enabled = false;
            txtNeto.Enabled = false;
            txtNopoliza.Enabled = false;
            txtNumCobranza.Enabled = false;
            txtOt.Enabled = false;
            txtPolNueva.Enabled = false;
            txtPolOriginal.Enabled = false;
            txtPrima.Enabled = false;
            txtRamo.Enabled = false;
            txtRecargos.Enabled = false;
            txtRecargosPor.Enabled = false;
            txtSlip.Enabled = false;
            txtSobreComision.Enabled = false;
            txtSobreComisionPor.Enabled = false;
            txtSubramo.Enabled = false;
            txtSupervisor.Enabled = false;
            txtTitular.Enabled = false;
            txtTotal.Enabled = false;
            txtTotalCom.Enabled = false;
            txtEstatusCobranza.Enabled = false;
        }

        private void initForm()
        {
            valDescarteBtn();
            txtOt.Enabled = false;
            txtNopoliza.Enabled = false;
            if (txtNopoliza.Text != "")
            {
                txtFormaPago.Enabled = false;
            }
        }

        private void initCatalog(object id)
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Aseguradora.useAuditable = false;
            JoinEntity joinCust = new JoinEntity();
            joinCust.ChildEntity = new Entity("Cliente");
            joinCust.JoinKey = new Models.Attribute("Id");
            joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
            selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
            joinCust.selectJoinList.Add(custsj);
            joinCust.JoinType = JoinType.Inner;
            Aseguradora.ChildEntities.Add(joinCust);
            listResult = SiteFunctions.GetValues(Aseguradora, out errmess);
            txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtAseguradora.DataValueField = "Id";
            txtAseguradora.DataTextField = "ClienteNombreCompleto";
            txtAseguradora.DataBind();

            if (id == null)
            {
                ejecutivoAseguradoraFilter();
            }

            Entity Ramo = new Entity("Ramo");
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Ramo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Ramo, out errmess);
            txtRamo.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtRamo.DataValueField = "Id";
            txtRamo.DataTextField = "Nombre";
            txtRamo.DataBind();

            if (id == null)
            {
                filterSubRamo();
            }

            //Entity agente = new Entity("Agente");
            //agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            //agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            //agente.useAuditable = false;
            //listResult = SiteFunctions.GetValues(agente, out errmess);
            //txtAgente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            //txtAgente.DataValueField = "Id";
            //txtAgente.DataTextField = "Clave";
            //txtAgente.DataBind();


            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.useAuditable = false;
            listResult = SiteFunctions.GetValues(Cliente, out errmess);
            txtCliente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtCliente.DataValueField = "Id";
            txtCliente.DataTextField = "NombreCompleto";
            txtCliente.DataBind();

            if (id == null)
            {
                getCatalogLinkToCustomer();
            }
            Entity userEnt = new Entity("Usuario");
            userEnt.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            userEnt.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            listResult = SiteFunctions.GetValues(userEnt, out errmess);
            DataTable dtUsers = SiteFunctions.trnsformEntityToDT(listResult);
            txtDirector.DataSource = dtUsers;
            txtDirector.DataValueField = "Id";
            txtDirector.DataTextField = "Nombre";
            txtDirector.DataBind();
            txtEjecutivo.DataSource = dtUsers;
            txtEjecutivo.DataValueField = "Id";
            txtEjecutivo.DataTextField = "Nombre";
            txtEjecutivo.DataBind();
            txtSupervisor.DataSource = dtUsers;
            txtSupervisor.DataValueField = "Id";
            txtSupervisor.DataTextField = "Nombre";
            txtSupervisor.DataBind();

            Entity moneda = new Entity("Moneda");
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            moneda.useAuditable = false;
            listResult = SiteFunctions.GetValues(moneda, out errmess);
            txtMoneda.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtMoneda.DataValueField = "Id";
            txtMoneda.DataTextField = "Nombre";
            txtMoneda.DataBind();
            try
            {
                txtMoneda.SelectedValue = "2187";
            }
            catch (Exception)
            {
            }

            Entity tax = new Entity("impuesto");
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Porcentaje"));
            tax.useAuditable = false;
            listResult = SiteFunctions.GetValues(tax, out errmess);
            txtImpuestoPor.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtImpuestoPor.DataValueField = "Id";
            txtImpuestoPor.DataTextField = "Nombre";
            txtImpuestoPor.DataBind();

            Entity marca = new Entity("Marca");
            marca.Attributes.Add(new Models.Attribute("id", "", "int"));
            marca.Attributes.Add(new Models.Attribute("Descripcion", "", "string"));
            listResult = SiteFunctions.GetValues(marca, out errmess);
            txtMarca.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtMarca.DataValueField = "Id";
            txtMarca.DataTextField = "Descripcion";
            txtMarca.DataBind();
            //filterNegociacion();//new
            initEnums();

           
           
           
        }

        private void initEnums()
        {
            Array itemValues = System.Enum.GetValues(typeof(EstatusPolizaEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusPolizaEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }

            //itemValues = System.Enum.GetValues(typeof(StatusMesaControl));
            //itemNames = System.Enum.GetNames(typeof(StatusMesaControl));

            //for (int i = 0; i <= itemNames.Length - 1; i++)
            //{
            //    ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
            //    txtMesa.Items.Add(item);
            //}

            itemValues = System.Enum.GetValues(typeof(EstatusCobranzaEnum));
            itemNames = System.Enum.GetNames(typeof(EstatusCobranzaEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatusCobranza.Items.Add(item);
            }

            itemValues = System.Enum.GetValues(typeof(FormaPagoEnum));
            itemNames = System.Enum.GetNames(typeof(FormaPagoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = null;
                switch(i)
                {
                    case 3:

                        item = new ListItem(itemNames.GetValue(13).ToString(), ((int)itemValues.GetValue(13)).ToString());
                        txtFormaPago.Items.Add(item);
                    break;
                    case 4:
                    case 5:
                    case 6:
                        if (!(PolizaType.Value == "1" && i == 4))
                        {
                            item = new ListItem(itemNames.GetValue(i - 1).ToString(), ((int)itemValues.GetValue(i - 1)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                    case 7:
                        item = new ListItem(itemNames.GetValue(12).ToString(), ((int)itemValues.GetValue(12)).ToString());
                        txtFormaPago.Items.Add(item);
                        break;
                    case 8:
                    case 9:
                    case 10:
                        item = new ListItem(itemNames.GetValue(i-2).ToString(), ((int)itemValues.GetValue(i-2)).ToString());
                        txtFormaPago.Items.Add(item);
                        break;
                    case 11:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(14).ToString(), ((int)itemValues.GetValue(14)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                    case 12:
                    case 13:
                    case 14:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(i - 3).ToString(), ((int)itemValues.GetValue(i - 3)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                        break;
                    default:
                        if (PolizaType.Value != "1")
                        {
                            item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                            txtFormaPago.Items.Add(item);
                        }
                    break;
                }
            }
            
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SavePoliza();
        }

        private void searchPoliza(string _id)
        {
            txtId.Text = _id;
            searchTipoPoliza(_id);
            string errMsg = string.Empty,
            idCliente = string.Empty, idAseguradora = string.Empty, idContacto = string.Empty, idMoneda = string.Empty, idImpuesto = string.Empty,
            idRamo = string.Empty, idSubRamo = string.Empty, idAgente = string.Empty;
            List<Entity> entities = new List<Entity>();
            Entity poliza = new Entity("Poliza");
            poliza.useAuditable = true;
            poliza.Keys.Add(new Models.Key("id", _id, "int"));

            List<Entity> polResult = SiteFunctions.GetValues(poliza, out errMsg);
            if (polResult.Count > 0)
            {
                Entity pliza = polResult[0];

                idCliente = pliza.getAttrValueByName("Contratante");
                idAseguradora = pliza.getAttrValueByName("Aseguradora");
                idRamo = pliza.getAttrValueByName("Ramo");
                idSubRamo = pliza.getAttrValueByName("SubRamo");
                idContacto = pliza.getAttrValueByName("Titular");
                idMoneda = pliza.getAttrValueByName("Moneda");
                idAgente = pliza.getAttrValueByName("Agente");
                idImpuesto = pliza.getAttrValueByName("Impuesto");

                txtOt.Text = pliza.getAttrValueByName("Id");
                //txtNopoliza.Text = pliza.getAttrValueByName("Id");
                chckRenovable.Checked = pliza.getAttrValueByName("Renovable") == "True" ? true : false;
                string PolizaOriginal = pliza.getAttrValueByName("PolizaOriginal");
                if(PolizaOriginal != "")
                { 
                    Entity polorig = new Entity("Tramite");
                    polorig.Keys.Add(new Key("Id",PolizaOriginal,"int"));
                    polorig.Attributes.Add(new Models.Attribute("Id"));
                    polorig.Attributes.Add(new Models.Attribute("Folio"));
                    List<Entity> polOriginal = SiteFunctions.GetValues(polorig,out errMsg);
                    txtPolOriginal.DataSource = SiteFunctions.trnsformEntityToDT(polOriginal);
                    txtPolOriginal.DataTextField = "Folio";
                    txtPolOriginal.DataValueField = "Id";
                    txtPolOriginal.DataBind();
                }
                string PolizaNueva = pliza.getAttrValueByName("PolizaNueva");
                if (PolizaNueva != "")
                {
                    Entity polnew = new Entity("Tramite");
                    polnew.Keys.Add(new Key("Id", PolizaNueva, "int"));
                    polnew.Attributes.Add(new Models.Attribute("Id"));
                    polnew.Attributes.Add(new Models.Attribute("Folio"));
                    List<Entity> polNueva = SiteFunctions.GetValues(polnew, out errMsg);
                    txtPolNueva.DataSource = SiteFunctions.trnsformEntityToDT(polNueva);
                    txtPolNueva.DataTextField = "Folio";
                    txtPolNueva.DataValueField = "Id";
                    txtPolNueva.DataBind();
                }
                
                txtDescuento.Text = pliza.getAttrValueByName("Descuento");
                txtSobreComisionPor.Text = pliza.getAttrValueByName("SobreComisionPct");
                txtSobreComision.Text = pliza.getAttrValueByName("SobreComisionImporte");
                txtBonoPor.Text = pliza.getAttrValueByName("BonoPct");
                txtBono.Text = pliza.getAttrValueByName("BonoImporte");
                txtNegociacion.SelectedValue = pliza.getAttrValueByName("Negociacion");
                txtImpuestoPor.SelectedValue = pliza.getAttrValueByName("Impuesto");
                

                txtId.Text = _id;
               
                DateTime fechaaudit = Convert.ToDateTime(pliza.getAttrValueByName("FechaAdd"));
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(pliza.getAttrValueByName("FechaUMod"));
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", pliza.getAttrValueByName("UsuarioAdd"), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", pliza.getAttrValueByName("UsuarioUMod"), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
            
            Entity cliente = new Entity("Cliente");
            cliente.Keys.Add(new Models.Key("id", idCliente, "int"));

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Keys.Add(new Models.Key("id", idAseguradora, "int"));

            Entity ramo = new Entity("Ramo");
            ramo.Keys.Add(new Models.Key("id", idRamo, "int"));

            Entity subramo = new Entity("SubRamo");
            subramo.Keys.Add(new Models.Key("id", idSubRamo, "int"));

            Entity titular = new Entity("ContactoSimple");
            titular.Keys.Add(new Models.Key("id", idContacto, "int"));

            Entity moneda = new Entity("Moneda");
            moneda.Keys.Add(new Models.Key("id", idMoneda, "int"));

            Entity agente = new Entity("Agente");
            agente.Keys.Add(new Models.Key("id", idAgente, "int"));

            Entity slip = new Entity("Slip");
            slip.Keys.Add(new Models.Key("id", _id, "int"));

            Entity cobertura = new Entity("Coberturas");
            cobertura.Keys.Add(new Models.Key("id", _id, "int"));

            Entity condEspec = new Entity("CondicionesEspeciales");
            condEspec.Keys.Add(new Models.Key("id", _id, "int"));

            Entity Impuesto = new Entity("Impuesto");
            Impuesto.Keys.Add(new Models.Key("id", idImpuesto, "int"));
            Impuesto.Attributes.Add(new Models.Attribute("Clave"));
            Impuesto.Attributes.Add(new Models.Attribute("Nombre"));

            Entity Tramite = new Entity("Tramite");
            Tramite.Keys.Add(new Models.Key("id", _id, "int"));
            Tramite.Attributes.Add(new Models.Attribute("FormaPago"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaInicio"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaFin"));
            Tramite.Attributes.Add(new Models.Attribute("Prima"));
            Tramite.Attributes.Add(new Models.Attribute("Gastos"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosPct"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Neto"));
            Tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Total"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionPct"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionImporte"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEmision"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEnvio"));
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            Tramite.Attributes.Add(new Models.Attribute("Ubicacion"));
            Tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));

            entities.Add(cliente);
            entities.Add(aseguradora);
            entities.Add(ramo);
            entities.Add(subramo);
            entities.Add(titular);
            entities.Add(moneda);
            entities.Add(agente);
            entities.Add(slip);
            entities.Add(cobertura);
            entities.Add(condEspec);
            entities.Add(Impuesto);
            entities.Add(Tramite);

            TipoPoliza policyType = (TipoPoliza)(int.Parse(PolizaType.Value));
            #region Buscar adicionales Tipo poliza
            Entity entidadAdicional, entidadAdicional2;
            switch (PolizaType.Value)
            {
                case "0":
                    PolizaTypeStr.Value = "Autos Individual";
                    break;
                case "6":
                    PolizaTypeStr.Value = "Autos Flotilla";
                    break;
                case "4":
                    PolizaTypeStr.Value = "Vida Individual";
                    break;
                case "5":
                    PolizaTypeStr.Value = "Vida Pool";
                    break;
                case "2":
                    PolizaTypeStr.Value = "GMM Individual";
                    break;
                case "3":
                    PolizaTypeStr.Value = "GMM Pool";
                    break;
                case "1":
                    PolizaTypeStr.Value = "Daños";
                    break;
            }
            switch (policyType)
            {
                case TipoPoliza.GMMIndividual:
                    PolizaTypeStr.Value = "Autos Individual";
                    entidadAdicional2 = new Entity("PolizaGmmIndividual");
                    entidadAdicional2.Attributes.Add(new Models.Attribute("SumaAsegurada", "", "float"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", "", "float"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("CoaseguroPct", "", "float"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("DeduciblePct", "", "float"));
                    entidadAdicional2.Keys.Add(new Key("Id", _id, "int"));
                    entities.Add(entidadAdicional2);
                    #region mapea bien asegurado
                    entidadAdicional = new Entity("bienasegurado");
                    entidadAdicional.Attributes.Add(new Models.Attribute("Inciso", "", "string"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entidadAdicional.Keys.Add(new Key("Poliza", _id, "int"));
                    entities.Add(entidadAdicional);
                    #endregion


                    break;
                case TipoPoliza.VidaIndividual:
                    #region mapea bien asegurado
                    entidadAdicional = new Entity("bienasegurado");
                    entidadAdicional.Attributes.Add(new Models.Attribute("Inciso", "", "string"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entidadAdicional.Keys.Add(new Key("Poliza", _id, "int"));
                    entities.Add(entidadAdicional);
                    #endregion
                    break;
                case TipoPoliza.AutosIndividual:
                    entidadAdicional = new Entity("bienasegurado");
                    entidadAdicional.Attributes.Add(new Models.Attribute("inciso", "", "string"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("prima", "", "string"));
                    entidadAdicional.Keys.Add(new Key("poliza", _id, "int"));

                    entidadAdicional2 = new Entity("auto");
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Serie", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Motor", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Placas", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("ConductorHabitual", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Modelo", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Version", "", "string"));
                    entidadAdicional2.Attributes.Add(new Models.Attribute("Marca", "", "int"));
                    entidadAdicional2.Keys.Add(new Key("poliza", _id, "int"));

                    entities.Add(entidadAdicional);
                    entities.Add(entidadAdicional2);
                    break;
                case TipoPoliza.VidaPool:
                    entidadAdicional = new Entity("PolizaVidaPoblacion");
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAsegurada", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAseguradaTopada", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAseguradaTopadaSMGM", "", "float"));
                    entidadAdicional.Keys.Add(new Key("Id", _id, "int"));
                    entities.Add(entidadAdicional);
                    break;
                case TipoPoliza.GMMPool:
                    entidadAdicional = new Entity("PolizaGmmPoblacion");
                    entidadAdicional.Attributes.Add(new Models.Attribute("EsquemaPrimaMinima", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("DeduciblePct", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("CoaseguroPct", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAsegurada", "", "float"));
                    entidadAdicional.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", "", "float"));
                    entidadAdicional.Keys.Add(new Key("Id", _id, "int"));
                    entities.Add(entidadAdicional);
                    break;
            }
            #endregion
            
            DataTable dtRecibo = searchRelRecords(recordType.Recibo, string.Empty, _id);
            gvRecibos.DataSource = dtRecibo;
            gvRecibos.DataBind();

            DataTable dtComisiones = searchRelRecords(recordType.Comision,string.Empty, _id);
            gvComisiones.DataSource = dtComisiones;
            gvComisiones.DataBind();

            DataTable dtBitacora = searchRelRecords(recordType.Bitacora, string.Empty, _id);
            GVBitacora.DataSource = dtBitacora;
            GVBitacora.DataBind();

            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            mapPolizaSearch(result, txtNegociacion.SelectedValue);
            ejecutivoAseguradoraFilter();
            agenteAseguradoraFilter();
            searchBitacora();
            try
            {
                txtDirCobro.SelectedValue = pliza.getAttrValueByName("DireccionCobro");
            }
            catch (Exception)
            { }
            try
            {
                txtDirEnvio.SelectedValue = pliza.getAttrValueByName("DireccionEnvio");
            }
            catch (Exception)
            { }
            try
            {
                txtDirFiscal.SelectedValue = pliza.getAttrValueByName("DireccionFiscal");
            }
            catch (Exception)
            { }
            try
            {
                txtSupervisor.SelectedValue = pliza.getAttrValueByName("Supervisor");
            }
            catch(Exception)
            { }
            try
            {
                txtDirector.SelectedValue = pliza.getAttrValueByName("Director");
            }
            catch (Exception)
            {
            }
            try
            {
                txtTitular.SelectedValue = pliza.getAttrValueByName("Titular");
            }
            catch (Exception)
            {
            }
            try
            {
                txtEjecutivo.SelectedValue = pliza.getAttrValueByName("Ejecutivo");
            }
            catch (Exception)
            {
            }
            try
            {
                txtEjeAseuradora.SelectedValue = pliza.getAttrValueByName("EjecutivoAseguradora");
            }
            catch (Exception)
            {
            }

            try
            {
                txtEstatus.SelectedValue = pliza.getAttrValueByName("EstatusPoliza");
            }
            catch (Exception)
            {
            }
            try
            {
                float primaAmt = 0;
                float.TryParse(txtPrima.Text, out primaAmt);

                if (primaAmt != 0)
                {
                    txtComision.Text = ((primaAmt * float.Parse(txtComisionPor.Text)) / 100).ToString();
                    txtBono.Text = ((primaAmt * float.Parse(txtBonoPor.Text)) / 100).ToString();
                    txtSobreComision.Text = ((primaAmt * float.Parse(txtSobreComisionPor.Text)) / 100).ToString();
                    txtTotalCom.Text = (float.Parse(txtComision.Text) + float.Parse(txtBono.Text) + float.Parse(txtSobreComision.Text)).ToString();
                }
            }
            catch (Exception)
            { }

            }
            searchIncisosAutos();
            RenovarState();
        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            divinsrecibo.Visible = false;
            
            txtEstatusCobranza.Enabled = false;
            NuevoEndoso.Visible = false;

            txtPolOriginal.Enabled = false;
            txtPolNueva.Enabled = false;
        }
        private void mapPolizaSearch(List<OperationResult> result, string idNegociacion)
        {
            string errMsg = string.Empty;
            TipoPoliza policyType = (TipoPoliza)(int.Parse(PolizaType.Value));
            reloadFormaPago();
            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if (opResult.RetVal.Count > 0)
                    {
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {
                            case "auditoria":

                                DateTime fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaAdd"));
                                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaUMod"));
                                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                Entity UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioAdd"), "int64"));
                                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioUMod"), "int64"));
                                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }

                                break;
                            case "polizagmmpoblacion":
                                txtEsqPrimaMinPool.Text = entity.getAttrValueByName("EsquemaPrimaMinima");
                                txtDeducPctGMMPool.Text = entity.getAttrValueByName("DeduciblePct");
                                txtCoaseguroPctGMMPool.Text = entity.getAttrValueByName("CoaseguroPct");
                                txtSumAseguradaGMMPool.Text = entity.getAttrValueByName("SumaAsegurada");
                                txtSumAsegSMGMGMMPool.Text = entity.getAttrValueByName("SumaAseguradaSMGM");
                                break;
                            case "polizagmmindividual":
                                txtDeduciblePrc.Text = entity.getAttrValueByName("DeduciblePct");
                                txtCoaseguroPrc.Text = entity.getAttrValueByName("CoaseguroPct");
                                txtSumAsegurada.Text = entity.getAttrValueByName("SumaAsegurada");
                                txtSumAsegSMGM.Text = entity.getAttrValueByName("SumaAseguradaSMGM");
                                break;
                            case "polizavidapoblacion":
                                txtSumAsegVP.Text = entity.getAttrValueByName("SumaAsegurada");
                                txtSumAsegSMGMVP.Text = entity.getAttrValueByName("SumaAseguradaSMGM");
                                txtSumaAsegTopadaVP.Text = entity.getAttrValueByName("SumaAseguradaTopada");
                                txtSumaAsegTopadaSMGMVP.Text = entity.getAttrValueByName("SumaAseguradaTopadaSMGM");
                                break;
                            case "bienasegurado":
                                if (policyType == TipoPoliza.AutosIndividual)
                                {
                                    txtInciso.Text = entity.getAttrValueByName("Inciso");
                                    txtPrima.Text = entity.getAttrValueByName("Prima");
                                }
                                else if (policyType == TipoPoliza.VidaIndividual || policyType == TipoPoliza.GMMIndividual)
                                {
                                    txtNumCertificado.Text = entity.getAttrValueByName("Inciso");
                                    Entity entidadAdicional2 = new Entity("Certificado");
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("ApellidoPaterno", "", "string"));
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("ApellidoMaterno", "", "string"));
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("Antiguedad", "", "string"));
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("FechaNacimiento", "", "datetime"));
                                    entidadAdicional2.Attributes.Add(new Models.Attribute("Sexo", "", "int"));
                                    entidadAdicional2.Keys.Add(new Key("Id", entity.getAttrValueByName("Id"), "int"));

                                    List<Entity> results = SiteFunctions.GetValues(entidadAdicional2, out errMsg);
                                    if (result.Count > 0)
                                    {
                                        txtVINombre.Text = results[0].getAttrValueByName("Nombre");
                                        txtVIAPaterno.Text = results[0].getAttrValueByName("ApellidoPaterno");
                                        txtVIAMaterno.Text = results[0].getAttrValueByName("ApellidoMaterno");
                                        txtAntiguedad.Text = results[0].getAttrValueByName("Antiguedad");
                                        txtBirthDay.Text = results[0].getAttrValueByName("FechaNacimiento");
                                        txtSex.Text = results[0].getAttrValueByName("Sexo");
                                    }
                                }
                                break;
                            case "auto":
                                txtSerie.Text = entity.getAttrValueByName("Serie");
                                txtMotor.Text = entity.getAttrValueByName("Motor");
                                txtPlacas.Text = entity.getAttrValueByName("Placas");
                                txtConductor.Text = entity.getAttrValueByName("ConductorHabitual");
                                txtModelo.Text = entity.getAttrValueByName("Modelo");
                                txtVersion.Text = entity.getAttrValueByName("Version");
                                txtMarca.Text = entity.getAttrValueByName("Marca");
                                break;
                            case "cliente":
                                try
                                {
                                    txtCliente.SelectedValue = entity.getAttrValueByName("Id");
                                    CustRFC.Value = entity.getAttrValueByName("Rfc");
                                    getCatalogLinkToCustomer();
                                    txtNegociacion.SelectedValue = idNegociacion;
                                }
                                catch (Exception)
                                {
                                }
                                break;
                            case "aseguradora":
                                txtAseguradora.SelectedValue = entity.getAttrValueByName("Id");
                                ejecutivoAseguradoraFilter();
                                break;
                            case "ramo":
                                txtRamo.SelectedValue = entity.getAttrValueByName("Id");
                                filterSubRamo();
                                break;
                            case "subramo":
                                txtSubramo.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "contactosimple":
                                txtTitular.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "moneda":
                                txtMoneda.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "agente":
                                txtAgente.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "slip":
                                txtSlip.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "coberturas":
                                txtCoberturas.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "condicionesespeciales":
                                txtCondSpec.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "tramite":
                                txtFormaPago.SelectedValue = entity.getAttrValueByName("FormaPago");
                                DateTime fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaInicio"));
                                txtInicio.Text = fecha.ToString("dd/MM/yyyy");
                                fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaFin"));
                                txtFin.Text = fecha.ToString("dd/MM/yyyy");
                                txtPrima.Text = entity.getAttrValueByName("Prima");
                                txtGastos.Text = entity.getAttrValueByName("Gastos");
                                txtRecargosPor.Text = entity.getAttrValueByName("RecargosPct");
                                txtRecargos.Text = entity.getAttrValueByName("RecargosImporte");
                                txtNeto.Text = entity.getAttrValueByName("Neto");
                                txtImpuesto.Text = entity.getAttrValueByName("ImpuestoImporte");
                                txtTotal.Text = entity.getAttrValueByName("Total");
                                txtComisionPor.Text = entity.getAttrValueByName("ComisionPct");
                                txtComision.Text = entity.getAttrValueByName("ComisionImporte");
                                txtNopoliza.Text = entity.getAttrValueByName("Folio");
                                txtFechaEnvio.Text = entity.getAttrValueByName("FechaEnvio");
                                txtFechaEmision.Text = entity.getAttrValueByName("FechaEmision");
                                txtPoliza.Text = entity.getAttrValueByName("Folio");
                                Entity Auditfields = new Entity("CentroDeBeneficio");
                                Auditfields.Attributes.Add(new Models.Attribute("Nombre"));
                                Auditfields.Keys.Add(new Key("Id", entity.getAttrValueByName("CentroDeBeneficio"), "int64"));
                                List<Entity> Auditval = SiteFunctions.GetValues(Auditfields, out errMsg);
                                if (Auditval.Count > 0)
                                {
                                    txtCentroBeneficio.Text = Auditval[0].getAttrValueByName("Nombre").ToString();
                                }

                                Entity Ubicacion = new Entity("Ubicacion");
                                Ubicacion.Attributes.Add(new Models.Attribute("Nombre"));
                                Ubicacion.Keys.Add(new Models.Key("id", entity.getAttrValueByName("Ubicacion"), "int"));
                                errMsg = string.Empty;
                                Auditval = SiteFunctions.GetValues(Ubicacion, out errMsg);
                                if (Auditval.Count > 0)
                                {
                                    txtUbicacion.Text = Auditval[0].getAttrValueByName("Nombre").ToString();
                                }
                                break;
                            case "impuesto":
                                txtImpuestoPor.Text = entity.getAttrValueByName("Clave") + " " + entity.getAttrValueByName("Nombre");
                                break;
                        }
                    }
                }
            }
            showPanels();
        }
        [System.Web.Services.WebMethod]
        public static string GetCurrentString(string name)
        {
            return "vishal " + name;
        }

        private void SavePoliza()
        {
            object id = PoliId.Value.Trim() == "" ? null : PoliId.Value.Trim();
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            JoinEntity joinEntity = new JoinEntity();
            Entity Poliza = new Entity("Poliza");
            Auditable audi = new Auditable();
            audi.userId = int.Parse(userId);
            audi.opDate = DateTime.Now;
            
            #region "Map Tramite
            Entity tramite = new Entity("Tramite");
            tramite.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
            tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
            tramite.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
            tramite.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            //tramite.Attributes.Add(new Models.Attribute("FechaEmision", DateTime.Now.Date.ToShortDateString(), "datetime"));
            tramite.Attributes.Add(new Models.Attribute("Folio", txtNopoliza.Text, "string"));
            tramite.Attributes.Add(new Models.Attribute("FormaPago", txtFormaPago.SelectedValue, "int"));
            tramite.Attributes.Add(new Models.Attribute("VigenciaInicio", txtInicio.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("VigenciaFin", txtFin.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("Total", txtTotal.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionPct", txtComisionPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionImporte", txtComision.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosImporte", txtRecargos.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosPct", txtRecargosPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            
                float prima = 0;
                float gastos = 0;
                float recargos = 0;
                float.TryParse(txtPrima.Text, out prima);
                float.TryParse(txtGastos.Text, out gastos);
                float.TryParse(txtRecargos.Text, out recargos);
                if (txtFormaPago.SelectedValue == "8" || txtFormaPago.SelectedValue == "9" || txtFormaPago.SelectedValue == "10" || txtFormaPago.SelectedValue == "11" || txtFormaPago.SelectedValue == "14")
                {
                    recargos = 0;
                    gastos = 0;
                }
                txtNeto.Text = (prima + gastos+recargos).ToString();
            
            
            tramite.Attributes.Add(new Models.Attribute("Neto", txtNeto.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Prima", txtPrima.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Gastos", txtGastos.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("FechaEnvio", txtFechaEnvio.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("FechaEmision", txtFechaEmision.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("EstatusMesaDeControl", "14", "int"));
            tramite.useAuditable = true;
            tramite.auditable = audi;
            #endregion

            #region "Map poliza"
            Poliza.Attributes.Add(new Models.Attribute("Contratante", txtCliente.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Aseguradora", txtAseguradora.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Ramo", txtRamo.SelectedValue, "int"));

            if (chckRenovable.Checked)
                Poliza.Attributes.Add(new Models.Attribute("Renovable", "1", "int"));
            else
                Poliza.Attributes.Add(new Models.Attribute("Renovable", "0", "int"));

            Poliza.Attributes.Add(new Models.Attribute("SobreComisionPct", txtSobreComisionPor.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("SobreComisionImporte", txtSobreComision.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("BonoPct", txtBonoPor.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("BonoImporte", txtBono.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("Descuento", txtDescuento.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", txtEstatus.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("EstatusCobranza", "1", "int"));
            if (txtPolOriginal.Text != string.Empty)
                Poliza.Attributes.Add(new Models.Attribute("PolizaOriginal", txtPolOriginal.Text, "string"));
            if (txtPolNueva.Text != string.Empty)
                Poliza.Attributes.Add(new Models.Attribute("PolizaNueva", txtPolNueva.Text, "string"));
            Poliza.Attributes.Add(new Models.Attribute("Impuesto", txtImpuestoPor.Text, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Moneda", txtMoneda.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Agente", txtAgente.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Ejecutivo", txtEjecutivo.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("SubRamo", txtSubramo.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionCobro", txtDirCobro.Text, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionFiscal", txtDirFiscal.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionEnvio", txtDirEnvio.Text, "int"));
            if (txtEjeAseuradora.SelectedValue != string.Empty)
            {
                Poliza.Attributes.Add(new Models.Attribute("EjecutivoAseguradora", txtEjeAseuradora.SelectedValue, "int"));
            }
            Poliza.Attributes.Add(new Models.Attribute("Supervisor", txtSupervisor.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Director", txtDirector.SelectedValue, "int"));
            if (txtTitular.SelectedValue != string.Empty)
            {
                Poliza.Attributes.Add(new Models.Attribute("Titular", txtTitular.SelectedValue, "int"));
            }
            //Poliza.Attributes.Add(new Models.Attribute("CoTitular", coti, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Grupo", "28181", "int"));
            Poliza.Attributes.Add(new Models.Attribute("Negociacion", txtNegociacion.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("NumeroCobranza", txtNumCobranza.Text, "string"));
            Poliza.useAuditable = true;
            #endregion

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = Poliza;
            joinEntity.JoinKey = new Models.Attribute("id", "", "int");

            tramite.ChildEntities.Add(joinEntity);

            #region Pestañas complementarias todo tipo de polizas
            Entity slip = new Entity("Slip");
            slip.Attributes.Add(new Models.Attribute("Descripcion", txtSlip.Text.Replace("'","''"), "string"));
            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = slip;
            joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            Poliza.ChildEntities.Add(joinEntity);

            Entity cobertura = new Entity("Coberturas");
            cobertura.Attributes.Add(new Models.Attribute("Descripcion", txtCoberturas.Text.Replace("'", "''"), "string"));
            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = cobertura;
            joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            Poliza.ChildEntities.Add(joinEntity);

            Entity CondicionesEspeciales = new Entity("CondicionesEspeciales");
            CondicionesEspeciales.Attributes.Add(new Models.Attribute("Descripcion", txtCondSpec.Text.Replace("'", "''"), "string"));
            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = CondicionesEspeciales;
            joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            Poliza.ChildEntities.Add(joinEntity);
            #endregion

            #region Mapear tipo de poliza
            TipoPoliza tipoPol = (TipoPoliza)int.Parse(PolizaType.Value);
            Entity bienAsegurado, polizaVidaIndv, polizaIndv, certificado, certificadoVida, polizaPool;
            switch (tipoPol)
            {
                case TipoPoliza.AutosFlotilla:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
                case TipoPoliza.AutosIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaAutoIndv = new Entity("PolizaAutoIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaAutoIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #region mapea bien asegurado
                    bienAsegurado = this.llenaBienAsegurado(txtInciso.Text, txtPrima.Text, txtBeneficiarioAuto.SelectedValue, txtInciso.Text);
                    bienAsegurado.auditable = audi;
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = bienAsegurado;
                    joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #endregion
                    #region mapea auto
                    Entity auto = new Entity("auto");
                    auto.Attributes.Add(new Models.Attribute("Serie", txtSerie.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("Motor", txtMotor.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("Placas", txtPlacas.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("ConductorHabitual", txtConductor.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("Modelo", txtModelo.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("Version", txtVersion.Text, "string"));
                    auto.Attributes.Add(new Models.Attribute("Marca", txtMarca.SelectedValue, "string"));
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = auto;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    bienAsegurado.ChildEntities.Add(joinEntity);

                    #endregion
                    if (id != null)
                    {
                        bienAsegurado.Attributes.Add(new Models.Attribute("Poliza", id.ToString(), "int"));
                        auto.Attributes.Add(new Models.Attribute("Poliza", id.ToString(), "int"));
                    }
                    break;
                case TipoPoliza.VidaIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    polizaVidaIndv = new Entity("PolizaVidaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaVidaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #region mapea bien asegurado
                    bienAsegurado = this.llenaBienAsegurado(txtNumCertificado.Text, "0", string.Empty, txtNumCertificado.Text);
                    bienAsegurado.auditable = audi;
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = bienAsegurado;
                    joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #endregion
                    #region Certificado
                    certificado = new Entity("Certificado");
                    certificado.Attributes.Add(new Models.Attribute("Nombre", txtVINombre.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("ApellidoPaterno", txtVIAPaterno.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("ApellidoMaterno", txtVIAMaterno.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("Antiguedad", txtAntiguedad.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("FechaNacimiento", txtBirthDay.Text, "datetime"));
                    certificado.Attributes.Add(new Models.Attribute("Sexo", txtSex.SelectedValue, "int"));
                    certificado.Attributes.Add(new Models.Attribute("Tipo", "0", "int"));
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = certificado;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    bienAsegurado.ChildEntities.Add(joinEntity);

                    certificadoVida = new Entity("CertificadoVida");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = certificadoVida;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    certificado.ChildEntities.Add(joinEntity);
                    #endregion
                    break;
                case TipoPoliza.VidaPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaVidaPool = new Entity("PolizaVidaPoblacion");
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAsegVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaTopada", txtSumaAsegTopadaVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGMVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaTopadaSMGM", txtSumaAsegTopadaSMGMVP.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaVidaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    //Falta Grupos se hace por importación?

                    break;
                case TipoPoliza.GMMIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaGMMIndv = new Entity("PolizaGmmIndividual");
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("EsquemaPrimaMinima", "0", "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAsegurada.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGM.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("CoaseguroPct", txtCoaseguroPrc.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("DeduciblePct", txtDeduciblePrc.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaGMMIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #region mapea bien asegurado
                    bienAsegurado = this.llenaBienAsegurado(txtNumCertificado.Text, "0", string.Empty, txtNumCertificado.Text);
                    bienAsegurado.auditable = audi;
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = bienAsegurado;
                    joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #endregion
                    #region Certificado
                    certificado = new Entity("Certificado");
                    certificado.Attributes.Add(new Models.Attribute("Nombre", txtVINombre.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("ApellidoPaterno", txtVIAPaterno.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("ApellidoMaterno", txtVIAMaterno.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("Antiguedad", txtAntiguedad.Text, "string"));
                    certificado.Attributes.Add(new Models.Attribute("FechaNacimiento", txtBirthDay.Text, "datetime"));
                    certificado.Attributes.Add(new Models.Attribute("Sexo", txtSex.SelectedValue, "int"));
                    certificado.Attributes.Add(new Models.Attribute("Tipo", "0", "int"));
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = certificado;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    bienAsegurado.ChildEntities.Add(joinEntity);

                    certificadoVida = new Entity("CertificadoGmm");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = certificadoVida;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    certificado.ChildEntities.Add(joinEntity);
                    #endregion
                    break;
                case TipoPoliza.GMMPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaGMMPool = new Entity("PolizaGmmPoblacion");
                    polizaGMMPool.Attributes.Add(new Models.Attribute("EsquemaPrimaMinima", txtEsqPrimaMinPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("DeduciblePct", txtDeducPctGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("CoaseguroPct", txtCoaseguroPctGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAseguradaGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGMGMMPool.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaGMMPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
                case TipoPoliza.DanosPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaDanosPool = new Entity("PolizaDanhosPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaDanosPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
            }
            #endregion

            string errmess = "";
            string idPoliza = string.Empty;
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                tramite.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);
                idPoliza = id.ToString();
            }
            Poliza.auditable = audi;
            if (SiteFunctions.SaveEntity(tramite, method, out errmess))
            {
                idPoliza = idPoliza == string.Empty ? errmess : idPoliza;
                PoliId.Value = idPoliza;
                //Update auto con id de poliza
                if (TipoPoliza.AutosIndividual == tipoPol)
                {
                    updAutoForIndv(idPoliza);
                    
                   
                }
                DateTime fechaaudit = audi.opDate;
                if (txtFechaAlta.Text.Trim() == "")
                {
                    txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                }
                txtId.Text = idPoliza;
                fechaaudit = audi.opDate;
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", audi.userId.ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errmess);
                if (AuditUsr.Count > 0)
                {
                    if (txtUsuarioAlta.Text == "")
                    {
                        txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                    }
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                showMessage("Poliza guardada correctamente", false);
                ShowTypePolicy();

                if (openurlatstart.Value != "")
                {

                    string url = openurlatstart.Value + PoliId.Value;
                    openurlatstart.Value = "";
                    // Define the name and type of the client scripts on the page.
                    String csname1 = "openpopup";
                    Type cstype = this.GetType();

                    // Get a ClientScriptManager reference from the Page class.
                    ClientScriptManager cs = Page.ClientScript;

                    // Check to see if the startup script is already registered.
                    if (!cs.IsStartupScriptRegistered(cstype, csname1))
                    {
                        StringBuilder cstext1 = new StringBuilder();
                        cstext1.Append(@"<script type='text/javascript'>$('[id*=openurlatstart]').val('"+PoliId.Value+"');openpopup('" + url + "','Comisiones','#" + btnSearchComisiones.ClientID + "')"+" </");
                        cstext1.Append("script>");
                        cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                    }

                    
        
                   
                }
            }
            else
            {
                showMessage(errmess, true);
            }
        }

        private void updAutosIndividual(string polizaId)
        {
            string errMsg = string.Empty;
            Entity bienAsegurado = new Entity("bienasegurado");
            bienAsegurado.Attributes.Add(new Models.Attribute("id", "", "int"));
            bienAsegurado.Keys.Add(new Key("poliza", polizaId, "int"));
            List<Entity> results = SiteFunctions.GetValues(bienAsegurado, out errMsg);
            foreach (Entity bien in results)
            {
                Entity auto = new Entity("auto");
                auto.Attributes.Add(new Models.Attribute("poliza", polizaId, "int"));
                auto.Keys.Add(new Key("id", bien.getAttrValueByName("id"), "int"));

                SiteFunctions.SaveEntity(auto, Method.PUT, out errMsg);
            }
        }
        private void searchTipoPoliza(string _id)
        {
            string errMsg = string.Empty;
            bool founded = false;
            string[] tipoPolizas = new string[7] { "polizaAutoIndividual", "PolizaDanhosPool", "PolizaGmmIndividual", "PolizaGmmPoblacion", "PolizaVidaIndividual", "PolizaVidaPoblacion", "PolizaPool" };
            Entity polizaTmp = new Entity();
            int counter = 0;
            while (counter < tipoPolizas.Length && !founded)
            {
                polizaTmp = new Entity();
                polizaTmp.EntityName = tipoPolizas[counter];
                polizaTmp.Attributes.Add(new Models.Attribute("id", "", "int"));
                polizaTmp.Keys.Add(new Key("id", _id, "int"));

                List<Entity> result = SiteFunctions.GetValues(polizaTmp, out errMsg);

                founded = result.Count > 0;

                if (!founded)
                    counter++;
            }

            if (founded)
            {
                PolizaType.Value = counter.ToString();
                ShowTypePolicy();
            }
        }

        private void ShowTypePolicy()
        {
            TipoPoliza policyType = (TipoPoliza)(int.Parse(PolizaType.Value));
            IncisoAutosDiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            BitacoraNav.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            AutoIndivDiv.Visible = false;
            VidaIndividualDiv.Visible = false;
            GMMIndividualDiv.Visible = false;
            VidaPoolDiv.Visible = false;
            VPGruposDiv.Visible = false;
            GMMPoolDiv.Visible = false;
            divDependientes.Visible = false;
            disableRequriedValControls();
            string id = PoliId.Value;
            switch (policyType)
            {
                case TipoPoliza.AutosFlotilla:
                    if (!string.IsNullOrEmpty(id))
                    {
                        IncisoAutosDiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                        BitacoraNav.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                        txtPrima.Enabled = false;
                    }
                    break;
                case TipoPoliza.AutosIndividual:
                    AutoIndivDiv.Visible = true;
                    ReqtxtMarca.Enabled = true;
                    ReqtxtModelo.Enabled = true;
                    ReqtxtMotor.Enabled = true;
                    ReqtxtSerie.Enabled = true;
                    ReqtxtVersion.Enabled = true;
                    BitacoraNav.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                    break;
                case TipoPoliza.VidaIndividual:
                    VidaIndividualDiv.Visible = true;
                    ReqtxtVIAMaterno.Enabled = true;
                    ReqtxtVIAPaterno.Enabled = true;
                    ReqtxtVINombre.Enabled = true;
                    ReqtxtNumCertificado.Enabled = true;
                    ReqtxtBirthDay.Enabled = true;
                    ReqtxtAntiguedad.Enabled = true;
                    break;
                case TipoPoliza.GMMIndividual:
                    GMMIndividualDiv.Visible = true;
                    VidaIndividualDiv.Visible = true;
                    ReqtxtCoaseguroPrc.Enabled = true;
                    ReqtxtDeduciblePrc.Enabled = true;
                    ReqtxtSumAsegSMGM.Enabled = true;
                    ReqtxtSumAsegurada.Enabled = true;
                    ReqtxtVIAMaterno.Enabled = true;
                    ReqtxtVIAPaterno.Enabled = true;
                    ReqtxtVINombre.Enabled = true;
                    ReqtxtNumCertificado.Enabled = true;
                    ReqtxtBirthDay.Enabled = true;
                    ReqtxtAntiguedad.Enabled = true;
                    if (!string.IsNullOrEmpty(id))
                    {
                        divDependientes.Visible = true;
                    }
                    break;
                case TipoPoliza.VidaPool:
                    VidaPoolDiv.Visible = true;
                    ReqtxtSumaAsegTopadaSMGMVP.Enabled = true;
                    ReqtxtSumaAsegTopadaVP.Enabled = true;
                    ReqtxtSumAsegSMGMVP.Enabled = true;
                    ReqtxtSumAsegVP.Enabled = true;
                    break;
                case TipoPoliza.GMMPool:
                    GMMPoolDiv.Visible = true;
                    if (!string.IsNullOrEmpty(id))
                    {
                        VPGruposDiv.Visible = true;
                    }
                    break;
            }
            
        }

        private void disableRequriedValControls()
        {
            ReqtxtCoaseguroPrc.Enabled = false;
            ReqtxtDeduciblePrc.Enabled = false;
            ReqtxtSumAsegSMGM.Enabled = false;
            ReqtxtSumAsegurada.Enabled = false;
            ReqtxtVIAMaterno.Enabled = false;
            ReqtxtVIAPaterno.Enabled = false;
            ReqtxtVINombre.Enabled = false;
            ReqtxtNumCertificado.Enabled = false;
            ReqtxtBirthDay.Enabled = false;
            ReqtxtAntiguedad.Enabled = false;
            ReqtxtMarca.Enabled = false;
            ReqtxtModelo.Enabled = false;
            ReqtxtMotor.Enabled = false;
            ReqtxtSerie.Enabled = false;
            ReqtxtVersion.Enabled = false;
            ReqtxtSumaAsegTopadaSMGMVP.Enabled = false;
            ReqtxtSumaAsegTopadaVP.Enabled = false;
            ReqtxtSumAsegSMGMVP.Enabled = false;
            ReqtxtSumAsegVP.Enabled = false;
        }

        /*private void searchTipoPoliza(string _id)
        {
            string errMsg = string.Empty;
            bool founded = false;
            string[] tipoPolizas = new string[7] { "polizaAutoIndividual", "PolizaDanhosPool", "PolizaGmmIndividual", "PolizaGmmPoblacion", "PolizaVidaIndividual", "PolizaVidaPoblacion", "PolizaPool" };
            Entity polizaTmp = new Entity();
            int counter = 0;
            while (counter < tipoPolizas.Length && !founded)
            {
                polizaTmp.EntityName = tipoPolizas[counter];
                polizaTmp.Attributes.Add(new Models.Attribute("id", "", "int"));
                polizaTmp.Keys.Add(new Key("id", _id, "int"));

                List<Entity> result = SiteFunctions.GetValues(polizaTmp, out errMsg);

                founded = result.Count > 0;

                if (!founded)
                    counter++;
            }

            if (founded)
            {
                PolizaType.Value = counter.ToString();
                ShowTypePolicy();
            }
        }

        private void ShowTypePolicy()
        {
            TipoPoliza policyType = (TipoPoliza)(int.Parse(PolizaType.Value));
            IncisoAutosDiv.Visible = false;
            AutoIndivDiv.Visible = false;
            VidaIndividualDiv.Visible = false;
            GMMIndividualDiv.Visible = false;
            VidaPoolDiv.Visible = false;
            VPGruposDiv.Visible = false;
            GMMPoolDiv.Visible = false;
            disableRequriedValControls();
            string id = Request.QueryString["Id"];
            switch (policyType)
            {
                case TipoPoliza.AutosFlotilla:
                    if (!string.IsNullOrEmpty(id))
                    {
                        IncisoAutosDiv.Visible = true;
                    }
                    break;
                case TipoPoliza.AutosIndividual:
                    AutoIndivDiv.Visible = true;
                    ReqtxtMarca.Enabled = true;
                    ReqtxtModelo.Enabled = true;
                    ReqtxtMotor.Enabled = true;
                    ReqtxtSerie.Enabled = true;
                    ReqtxtVersion.Enabled = true;
                    break;
                case TipoPoliza.VidaIndividual:
                    VidaIndividualDiv.Visible = true;
                    ReqtxtVIAMaterno.Enabled = true;
                    ReqtxtVIAPaterno.Enabled = true;
                    ReqtxtVINombre.Enabled = true;
                    ReqtxtNumCertificado.Enabled = true;
                    ReqtxtBirthDay.Enabled = true;
                    ReqtxtAntiguedad.Enabled = true;
                    break;
                case TipoPoliza.GMMIndividual:
                    GMMIndividualDiv.Visible = true;
                    VidaIndividualDiv.Visible = true;
                    ReqtxtCoaseguroPrc.Enabled = true;
                    ReqtxtDeduciblePrc.Enabled = true;
                    ReqtxtSumAsegSMGM.Enabled = true;
                    ReqtxtSumAsegurada.Enabled = true;
                    ReqtxtVIAMaterno.Enabled = true;
                    ReqtxtVIAPaterno.Enabled = true;
                    ReqtxtVINombre.Enabled = true;
                    ReqtxtNumCertificado.Enabled = true;
                    ReqtxtBirthDay.Enabled = true;
                    ReqtxtAntiguedad.Enabled = true;
                    break;
                case TipoPoliza.VidaPool:
                    VidaPoolDiv.Visible = true;
                    ReqtxtSumaAsegTopadaSMGMVP.Enabled = true;
                    ReqtxtSumaAsegTopadaVP.Enabled = true;
                    ReqtxtSumAsegSMGMVP.Enabled = true;
                    ReqtxtSumAsegVP.Enabled = true;
                    break;
                case TipoPoliza.GMMPool:
                    VPGruposDiv.Visible = true;
                    GMMPoolDiv.Visible = true;
                    break;
            }
        }

        private void disableRequriedValControls()
        {
            ReqtxtCoaseguroPrc.Enabled = false;
            ReqtxtDeduciblePrc.Enabled = false;
            ReqtxtSumAsegSMGM.Enabled = false;
            ReqtxtSumAsegurada.Enabled = false;
            ReqtxtVIAMaterno.Enabled = false;
            ReqtxtVIAPaterno.Enabled = false;
            ReqtxtVINombre.Enabled = false;
            ReqtxtNumCertificado.Enabled = false;
            ReqtxtBirthDay.Enabled = false;
            ReqtxtAntiguedad.Enabled = false;
            ReqtxtMarca.Enabled = false;
            ReqtxtModelo.Enabled = false;
            ReqtxtMotor.Enabled = false;
            ReqtxtSerie.Enabled = false;
            ReqtxtVersion.Enabled = false;
            ReqtxtSumaAsegTopadaSMGMVP.Enabled = false;
            ReqtxtSumaAsegTopadaVP.Enabled = false;
            ReqtxtSumAsegSMGMVP.Enabled = false;
            ReqtxtSumAsegVP.Enabled = false;
        }
        */

        /*[System.Web.Services.WebMethod]
        public static string GetCurrentString(string name)
        {
            return "vishal " + name;
        }
        */


        private void updAutoForIndv(string _poliza)
        {
            string errMsg = string.Empty;
            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.Attributes.Add(new Models.Attribute("Id", "", "int"));
            bienAsegurado.Keys.Add(new Key("poliza", _poliza, "int"));
            List<Entity> results = SiteFunctions.GetValues(bienAsegurado, out errMsg);
            foreach (Entity bien in results)
            {
                Entity auto = new Entity("auto");
                auto.Attributes.Add(new Models.Attribute("poliza", _poliza, "int"));
                auto.Attributes.Add(new Models.Attribute("Serie", txtSerie.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("Motor", txtMotor.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("Placas", txtPlacas.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("ConductorHabitual", txtConductor.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("Modelo", txtModelo.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("Version", txtVersion.Text, "string"));
                auto.Attributes.Add(new Models.Attribute("Marca", txtMarca.SelectedValue, "string"));
                auto.Keys.Add(new Key("id", bien.getAttrValueByName("Id"), "int"));
                SiteFunctions.SaveEntity(auto, Method.PUT, out errMsg);
            }
        }

        private Entity llenaBienAsegurado(string _inciso, string _prima, string _beneficiario, string _identificador)
        {
            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.useAuditable = true;

            bienAsegurado.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Propietario", "2343", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Inciso", _inciso, "string"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Prima", _prima, "float"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Identificador", _identificador, "string"));
            if (_beneficiario != string.Empty)
                bienAsegurado.Attributes.Add(new Models.Attribute("BeneficiarioPreferente", _beneficiario, "int"));
            return bienAsegurado;
        }

        protected void BtnAttachment_Click(object sender, ImageClickEventArgs e)
        {
            var sURL = "Attachment.aspx?id=" + PoliId.Value + "&entityName=Poliza" + "&parentId=" + CustRFC.Value;

            string vtn = "window.open(" + "'" + sURL + "'" + ",'scrollbars=yes,resizable=yes','height=100', 'width=100')";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", vtn, true);
        }

        protected void btnBuscarEndoso_Click(object sender, EventArgs e)
        {
            searchEndosos();
        }

        private void searchEndosos()
        {
            PaneName.Value = "Endosos";
            object id = PoliId.Value;
            if (id != null)
            {
                SearchIdEndoso.Value = string.Empty;
                if (txtSearchEndoso.Text != string.Empty)
                {
                    SearchIdEndoso.Value = '%' + txtSearchEndoso.Text + '%';
                }
                DataTable dtEndoso = searchRelRecords(recordType.Endoso, SearchIdEndoso.Value, id.ToString());
                gvEndosos.DataSource = dtEndoso;
                gvEndosos.DataBind();
            }
        }

        protected void btnBuscarComisiones_Click(object sender, EventArgs e)
        {
            
        }

        private DataTable searchRelRecords(recordType _recordType, string _searchText, string _polizaId)
        {
            string errMsg = string.Empty;
            Entity entity = new Entity();

            switch (_recordType)
            {
                case recordType.Dependientes:
                    entity.EntityName = "bienasegurado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    Entity certi = new Entity("Certificado");
                    certi.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("ApellidoPaterno", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("ApellidoMaterno", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("FechaNacimiento", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("Parentesco", "", "string"));
                    

                    selectJoinEntity selJoin = new selectJoinEntity("id", 1, "id");
                    JoinEntity joinEntityCert = new JoinEntity();
                    joinEntityCert.ChildEntity = certi;
                    joinEntityCert.selectJoinList = new List<selectJoinEntity>();
                    joinEntityCert.selectJoinList.Add(selJoin);
                    joinEntityCert.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityCert);
                    break;
                case recordType.Endoso:
                    entity.EntityName = "Endoso";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("EstatusEndoso", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Tipo"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "string", 1));
                    }
                    Entity TramiteEndoso = new Entity("Tramite");
                    TramiteEndoso.Attributes.Add(new Models.Attribute("Folio", "", "string"));
                    TramiteEndoso.Attributes.Add(new Models.Attribute("VigenciaInicio", "", "string"));
                    TramiteEndoso.Attributes.Add(new Models.Attribute("VigenciaFin", "", "string"));
                    

                    selectJoinEntity selJoinEndoso = new selectJoinEntity("id", 1, "id");
                    JoinEntity joinEntityEndoso = new JoinEntity();
                    joinEntityEndoso.ChildEntity = TramiteEndoso;
                    joinEntityEndoso.selectJoinList = new List<selectJoinEntity>();
                    joinEntityEndoso.selectJoinList.Add(selJoinEndoso);
                    joinEntityEndoso.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityEndoso);

                    Entity TipoEndoso = new Entity("TipoEndoso");
                    TipoEndoso.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                    
                    selectJoinEntity selJoinTipoEndoso = new selectJoinEntity("Tipo", 1, "id");
                    JoinEntity joinEntityTipoEndoso = new JoinEntity();
                    joinEntityTipoEndoso.ChildEntity = TipoEndoso;
                    joinEntityTipoEndoso.selectJoinList = new List<selectJoinEntity>();
                    joinEntityTipoEndoso.selectJoinList.Add(selJoinTipoEndoso);
                    joinEntityTipoEndoso.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityTipoEndoso);

                    Entity TipoMovimiento = new Entity("TipoMovimiento");
                    TipoMovimiento.Attributes.Add(new Models.Attribute("Descripcion", "", "string"));
                    
                    selectJoinEntity selJoinTipoMovimiento = new selectJoinEntity("TipoMovimiento", 1, "id");
                    JoinEntity joinEntityTipoMovimiento = new JoinEntity();
                    joinEntityTipoMovimiento.ChildEntity = TipoMovimiento;
                    joinEntityTipoMovimiento.selectJoinList = new List<selectJoinEntity>();
                    joinEntityTipoMovimiento.selectJoinList.Add(selJoinTipoMovimiento);
                    joinEntityTipoMovimiento.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityTipoMovimiento);
                    break;
                case recordType.IncisoAutos:
                    entity.EntityName = "BienAsegurado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Prima", "", "float"));
                    entity.Attributes.Add(new Models.Attribute("Poliza", "", "float"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int",1,2));
                    entity.Attributes.Add(new Models.Attribute("Estatus"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "string", 2,1));
                        entity.Keys.Add(new Key("Inciso", _searchText, "string", 2,1));
                    }
                    Entity auto = new Entity("Auto");
                    auto.Attributes.Add(new Models.Attribute("Serie", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Motor", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Placas", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Modelo", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Marca", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("ConductorHabitual", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Version", "", "string"));
                    if (_searchText != string.Empty)
                    {
                        auto.Keys.Add(new Key("Id", _searchText, "string", 2));
                        auto.Keys.Add(new Key("Serie", _searchText, "string", 2));
                        
                    }
                    selectJoinEntity selJoinAuto = new selectJoinEntity("id", 1, "id");
                    JoinEntity joinEntityAuto = new JoinEntity();
                    joinEntityAuto.ChildEntity = auto;
                    joinEntityAuto.selectJoinList = new List<selectJoinEntity>();
                    joinEntityAuto.selectJoinList.Add(selJoinAuto);
                    joinEntityAuto.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityAuto);
                    break;
                case recordType.Comision:
                    entity.EntityName = "ComisionAsociado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("DirectorAsociado", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "int", 1));
                    }

                    Entity dirAsociado = new Entity("DirectorAsociado");
                    dirAsociado.Attributes.Add(new Models.Attribute("Usuario", "", "int"));

                    Entity usuario = new Entity("Usuario");
                    usuario.Attributes.Add(new Models.Attribute("Nombre", "", "string"));

                    selectJoinEntity selJoinEntity = new selectJoinEntity("DirectorAsociado", 1, "Id");

                    JoinEntity joinEntity = new JoinEntity();
                    joinEntity.selectJoinList = new List<selectJoinEntity>();
                    joinEntity.selectJoinList.Add(selJoinEntity);
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.ChildEntity = dirAsociado;
                    entity.ChildEntities.Add(joinEntity);

                    selJoinEntity = new selectJoinEntity("Usuario", 1, "Id");

                    joinEntity = new JoinEntity();
                    joinEntity.selectJoinList = new List<selectJoinEntity>();
                    joinEntity.selectJoinList.Add(selJoinEntity);
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.ChildEntity = usuario;
                    dirAsociado.ChildEntities.Add(joinEntity);

                    break;
                case recordType.Recibo:
                    entity = new Entity("recibo");
                    entity.useAuditable = false;
                    entity.Attributes.Add(new Models.Attribute("Id"));
                    entity.Attributes.Add(new Models.Attribute("Cobertura"));
                    entity.Attributes.Add(new Models.Attribute("Vencimiento"));
                    entity.Attributes.Add(new Models.Attribute("Concepto"));
                    entity.Attributes.Add(new Models.Attribute("Total"));
                    entity.Attributes.Add(new Models.Attribute("Numero"));
                    entity.Attributes.Add(new Models.Attribute("Estatus"));
                    entity.Attributes.Add(new Models.Attribute("Tipo"));
                    entity.Attributes.Add(new Models.Attribute("Comision"));
                    entity.Keys.Add(new Models.Key("Tramite", _polizaId, "int"));
                    Entity tramite = new Entity("tramite");
                    tramite.Attributes.Add(new Models.Attribute("Folio"));
                    tramite.Attributes.Add(new Models.Attribute("id"));

                    selectJoinEntity selJoinr = new selectJoinEntity("tramite", 1, "id");
                    JoinEntity joinEnt = new JoinEntity();
                    joinEnt.ChildEntity = tramite;
                    joinEnt.JoinType = JoinType.Inner;
                    joinEnt.selectJoinList.Add(selJoinr);

                    entity.ChildEntities.Add(joinEnt);

                    //entity.EntityName = "Recibo";
                    //entity.Keys.Add(new Models.Key("Tramite", _polizaId, "int"));
                    //entity.Attributes.Add(new Models.Attribute("Id"));
                    //entity.Attributes.Add(new Models.Attribute("Numero"));
                    //entity.Attributes.Add(new Models.Attribute("Total"));
                    //entity.Attributes.Add(new Models.Attribute("Concepto"));
                    if (_searchText != string.Empty)
                        entity.Keys.Add(new Models.Key("id", SearchIdRecibo.Value, "string"));
                    break;
                case recordType.Siniestro:
                    entity.EntityName = "Siniestro";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Numero", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("FechaSiniestro", "", "datetime"));
                    entity.Attributes.Add(new Models.Attribute("FechaReporte", "", "datetime"));
                    entity.Attributes.Add(new Models.Attribute("FechaFiniquito", "", "datetime"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "int", 1));
                    }
                    break;
                case recordType.VPGrupos:
                    entity.EntityName = "GrupoAsegurados";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "string"));
                    entity.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                    entity.Attributes.Add(new Models.Attribute("Descripcion", "", "string"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    break;
                case recordType.Bitacora:
                    entity.EntityName = "BitacoraTramite";
                    entity.Attributes.Add(new Models.Attribute("Id"));
                    entity.Attributes.Add(new Models.Attribute("Fecha"));
                    entity.Attributes.Add(new Models.Attribute("Tramite"));
                    entity.Attributes.Add(new Models.Attribute("Usuario"));
                    entity.Keys.Add(new Key("Tramite", _polizaId, "int"));

                    Entity Usuario = new Entity("Usuario");
                    Usuario.Attributes.Add(new Models.Attribute("Nombre"));

                    selectJoinEntity selJoinEnt = new selectJoinEntity("Usuario", 1, "Id");

                    joinEntity = new JoinEntity();
                    joinEntity.selectJoinList = new List<selectJoinEntity>();
                    joinEntity.selectJoinList.Add(selJoinEnt);
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.ChildEntity = Usuario;
                    entity.ChildEntities.Add(joinEntity);
                    break;
            }

            List<Entity> polResult = SiteFunctions.GetValues(entity, out errMsg);
            
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(polResult);
            return dtRes;
        }

        protected void txtCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            getCatalogLinkToCustomer();

        }

        private void getCatalogLinkToCustomer()
        {


            string errmess;

            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Cliente.Attributes.Add(new Models.Attribute("Grupo", "", "string"));
            Cliente.Attributes.Add(new Models.Attribute("Ubicacion", "", "string"));
            Cliente.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "", "string"));

            Cliente.Keys.Add(new Key("Id", txtCliente.SelectedValue, "int"));

            List<Entity> listResult = SiteFunctions.GetValues(Cliente, out errmess);
            if (listResult.Count > 0)
            {
                Entity grupo = new Entity("Grupo");
                grupo.Attributes.Add(new Models.Attribute("Id", "", "int"));
                grupo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                grupo.Keys.Add(new Key("Id", listResult[0].getAttrValueByName("Grupo"), "int"));
                List<Entity> listEntities = SiteFunctions.GetValues(grupo, out errmess);
                txtGrupo.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                txtGrupo.DataTextField = "Nombre";
                txtGrupo.DataValueField = "Id";
                txtGrupo.DataBind();

                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                Ubicacion.Keys.Add(new Key("Id", listResult[0].getAttrValueByName("Ubicacion"), "int"));
                listEntities = SiteFunctions.GetValues(Ubicacion, out errmess);
                txtUbicacion.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                txtUbicacion.DataTextField = "Nombre";
                txtUbicacion.DataValueField = "Id";
                txtUbicacion.DataBind();

                Entity CentroDeBeneficio = new Entity("CentroDeBeneficio");
                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Id", "", "int"));
                CentroDeBeneficio.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                CentroDeBeneficio.Keys.Add(new Key("Id", listResult[0].getAttrValueByName("CentroDeBeneficio"), "int"));
                listEntities = SiteFunctions.GetValues(CentroDeBeneficio, out errmess);
                txtCentroBeneficio.DataSource = SiteFunctions.trnsformEntityToDT(listEntities);
                txtCentroBeneficio.DataTextField = "Nombre";
                txtCentroBeneficio.DataValueField = "Id";
                txtCentroBeneficio.DataBind();

                txtUbicacion.Enabled = false;
                txtCentroBeneficio.Enabled = false;
            }

            Entity Negociaciones = new Entity("Negociacion");
            Negociaciones.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Negociaciones.Attributes.Add(new Models.Attribute("NumeroNegociacion", "", "string"));
            Negociaciones.Keys.Add(new Key("cliente", txtCliente.SelectedValue, "int"));

            listResult = SiteFunctions.GetValues(Negociaciones, out errmess);
            txtNegociacion.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtNegociacion.DataTextField = "NumeroNegociacion";
            txtNegociacion.DataValueField = "Id";
            txtNegociacion.DataBind();
            if (PoliId.Value == "")
            {
                txtNegociacion.Items.Add(new ListItem("", ""));
                txtNegociacion.SelectedValue = "";
            }
            Entity Direcciones = new Entity("Direccion");
            Direcciones.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Direcciones.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            Direcciones.Keys.Add(new Key("cliente", txtCliente.SelectedValue, "int"));

            listResult = SiteFunctions.GetValues(Direcciones, out errmess);
            DataTable dirDT = SiteFunctions.trnsformEntityToDT(listResult);
            txtDirCobro.DataSource = dirDT;
            txtDirCobro.DataTextField = "Nombre";
            txtDirCobro.DataValueField = "Id";
            txtDirCobro.DataBind();

            txtDirEnvio.DataSource = dirDT;
            txtDirEnvio.DataTextField = "Nombre";
            txtDirEnvio.DataValueField = "Id";
            txtDirEnvio.DataBind();

            txtDirFiscal.DataSource = dirDT;
            txtDirFiscal.DataTextField = "Nombre";
            txtDirFiscal.DataValueField = "Id";
            txtDirFiscal.DataBind();

            Entity titular = new Entity("ContactoSimple");
            titular.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            titular.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            titular.Keys.Add(new Key("Cliente", txtCliente.SelectedValue, "int"));
            titular.useAuditable = false;
            listResult = SiteFunctions.GetValues(titular, out errmess);
            txtTitular.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtTitular.DataValueField = "Id";
            txtTitular.DataTextField = "NombreCompleto";
            txtTitular.DataBind();
            txtTitular.Items.Add(new ListItem("", ""));
            txtTitular.SelectedValue = "";
            

        }

        protected void btnBuscarRecibos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            object id = PoliId.Value;
            SearchIdRecibo.Value = string.Empty;
            if (txtSearchRecibos.Text != string.Empty)
            {
                SearchIdRecibo.Value = '%' + txtSearchRecibos.Text + '%';
            }
            DataTable dtRecibo = searchRelRecords(recordType.Recibo, SearchIdRecibo.Value, id.ToString());
            gvRecibos.DataSource = dtRecibo;
            gvRecibos.DataBind();
        }

        protected void btnBuscarSiniestro_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Siniestros";
            object id = PoliId.Value;
            SearchIdSiniestro.Value = string.Empty;
            if (txtSearchSiniestro.Text != string.Empty)
            {
                SearchIdSiniestro.Value = '%' + txtSearchSiniestro.Text + '%';
            }
            DataTable dtSiniestro = searchRelRecords(recordType.Siniestro, SearchIdSiniestro.Value, id.ToString());
            gvSiniestros.DataSource = dtSiniestro;
            gvSiniestros.DataBind();
        }

        protected void BtnImportarIncisos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "IncisosAutos";
            string errmess = string.Empty;
            object id = PoliId.Value;
            if (fileUpload.HasFile)
            {
                string fileName = fileUpload.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        decimal totalPrima = 0;
                        int NoIncisos = 0;
                        if (SiteFunctions.importIncisos(fileUpload.FileContent, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out totalPrima, out errmess,out NoIncisos))
                        {

                            recalcincisos();
                            //txtPrima.Text = totalPrima.ToString();
                            decimal.TryParse(txtPrima.Text, out totalPrima);
                            decimal gastos = 0, recargos = 0, descuentos = 0, impuestos = 0,porComision=0,porBono=0,porSobreCom=0;
                            
                            decimal.TryParse(txtGastos.Text, out gastos);

                            //txtGastos.Text = Math.Round((gastos * NoIncisos),4).ToString();
                            decimal.TryParse(txtGastos.Text, out gastos);
                            decimal.TryParse(txtRecargos.Text, out recargos);
                            decimal.TryParse(txtDescuento.Text, out descuentos);
                            if (txtFormaPago.SelectedValue == "8" || txtFormaPago.SelectedValue == "9" || txtFormaPago.SelectedValue == "10" || txtFormaPago.SelectedValue == "11" || txtFormaPago.SelectedValue == "14")
                            {
                                recargos = 0;
                                gastos = 0;
                            }
                            txtNeto.Text = Math.Round(totalPrima + gastos + recargos,4).ToString();
                            txtImpuesto.Text =  getAmountImpuesto(txtImpuestoPor.SelectedValue, txtNeto.Text);
                            
                            decimal.TryParse(txtImpuesto.Text,out impuestos);
                            
                            txtTotal.Text = Math.Round((totalPrima + gastos + recargos) - descuentos + impuestos,4).ToString();

                            decimal.TryParse(txtComisionPor.Text, out porComision);
                            decimal.TryParse(txtBonoPor.Text, out porBono);
                            decimal.TryParse(txtSobreComisionPor.Text, out porSobreCom);

                            txtComision.Text = Math.Round(((porComision/100)*totalPrima),4).ToString();
                            txtBono.Text = Math.Round(((porBono / 100) * totalPrima), 4).ToString();
                            txtSobreComision.Text = Math.Round(((porSobreCom / 100) * totalPrima), 4).ToString();

                            decimal.TryParse(txtComision.Text, out porComision);
                            decimal.TryParse(txtBono.Text, out porBono);
                            decimal.TryParse(txtSobreComision.Text, out porSobreCom);
                            txtTotalCom.Text = Math.Round(porComision+porBono+porSobreCom, 4).ToString();
                            
                            showMessage(String.Format("Carga Exitosa. Se importaron {0} incisos.", NoIncisos.ToString()),false);
                            SavePoliza();
                        }
                        else
                        {
                            IncisoImportMsg.Value =  errmess;
                        }
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
            if (IncisoImportMsg.Value != "")
            {
                showMessage(IncisoImportMsg.Value, true);
                IncisoImportMsg.Value = "";
            }

        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void btnSearchIncisosAutos_Click(object sender, EventArgs e)
        {
            searchIncisosAutos();
        }

        private void searchIncisosAutos()
        {
            PaneName.Value = "IncisosAutos";
            object id = PoliId.Value;
            SearchIdInciso.Value = string.Empty;
            if (txtSearchIncisosAutos.Text != string.Empty)
            {
                SearchIdInciso.Value = '%' + txtSearchIncisosAutos.Text + '%';
            }
            DataTable dtInciso = searchRelRecords(recordType.IncisoAutos, SearchIdInciso.Value, id.ToString());
            gvIncisosAutos.DataSource = dtInciso;
            gvIncisosAutos.DataBind();
        }

        protected void txtDeduciblePrc_TextChanged(object sender, EventArgs e)
        {
            PaneName.Value = "GMMIndividual";
            TextBox txtMoneyControl = (TextBox)sender;
            double strDouble = 0;
            if (!double.TryParse(txtMoneyControl.Text, out strDouble))
            {
                txtMoneyControl.Text = "0.00";
            }
        }

        protected void btnSearchVPGrupos_Click(object sender, EventArgs e)
        {
            searchVPGrupos();
        }

        private void searchVPGrupos()
        {
            PaneName.Value = "VPGrupos";
            object id = PoliId.Value;
            SearchVPGrupo.Value = string.Empty;
            if (txtSearchVPGrupos.Text != string.Empty)
            {
                SearchVPGrupo.Value = '%' + txtSearchVPGrupos.Text + '%';
            }
            DataTable dtVPGrupos = searchRelRecords(recordType.VPGrupos, SearchVPGrupo.Value, id.ToString());
            gvVPGrupos.DataSource = dtVPGrupos;
            gvVPGrupos.DataBind();
        }

        protected void txtAseguradora_SelectedIndexChanged(object sender, EventArgs e)
        {
            ejecutivoAseguradoraFilter();
            agenteAseguradoraFilter();
        }

        private void ejecutivoAseguradoraFilter()
        {
            string errmess = string.Empty;
            Entity ContactoMoral = new Entity("ContactoPersonaMoral");
            ContactoMoral.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            ContactoMoral.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));

            Entity PersonaMoral = new Entity("PersonaMoral");

            selectJoinEntity selJoin = new selectJoinEntity("PersonaMoral", 1, "Id");
            JoinEntity joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoin);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = PersonaMoral;

            ContactoMoral.ChildEntities.Add(joinEntity);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Keys.Add(new Key("Id", txtAseguradora.SelectedValue, "int"));

            selJoin = new selectJoinEntity("Id", 1, "Id");
            joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoin);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = aseguradora;

            PersonaMoral.ChildEntities.Add(joinEntity);

            List<Entity> listResult = SiteFunctions.GetValues(ContactoMoral, out errmess);
            txtEjeAseuradora.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtEjeAseuradora.DataValueField = "Id";
            txtEjeAseuradora.DataTextField = "Nombre";
            txtEjeAseuradora.DataBind();
        }
        private void agenteAseguradoraFilter()
        {
            try
            {
                string errmess = string.Empty;
                Entity agente = new Entity("Agente");
                agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
                agente.Keys.Add(new Key("Aseguradora", txtAseguradora.Text, "int64"));
                agente.useAuditable = false;
                List<Entity> listResult = SiteFunctions.GetValues(agente, out errmess);
                txtAgente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtAgente.DataValueField = "Id";
                txtAgente.DataTextField = "Clave";
                txtAgente.DataBind();
            }
            catch (Exception)
            {
            }
        }

        protected void txtRamo_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterSubRamo();
        }

        private void filterSubRamo()
        {
            Entity subRamo = new Entity("Subramo");
            subRamo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            subRamo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            subRamo.Attributes.Add(new Models.Attribute("Clave"));
            subRamo.Keys.Add(new Key("Ramo", txtRamo.SelectedValue, "int"));

            string errMsg = string.Empty;
            List<Entity> result = SiteFunctions.GetValues(subRamo, out errMsg);

            txtSubramo.DataSource = SiteFunctions.trnsformEntityToDT(result);
            txtSubramo.DataTextField = "Nombre";
            txtSubramo.DataValueField = "Id";
            txtSubramo.DataBind();
        }

        protected void btnImportVPGrupos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "VPGrupos";
            object id = PoliId.Value;
            if (fUploadGrupos.HasFile)
            {
                string fileName = fUploadGrupos.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        string errMsg = string.Empty;
                        if (SiteFunctions.importGrupos(fUploadGrupos.FileContent, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out errMsg))
                        {
                            searchVPGrupos();
                        }
                        else
                        {
                            showMessage(errMsg, true);
                        }
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
        }

        protected void btnSearchDependientes_Click(object sender, EventArgs e)
        {
            PaneName.Value = "VidaIndividual";
            searchDependientes();
        }

        protected void btnUplDependientes_Click(object sender, EventArgs e)
        {
            PaneName.Value = "VidaIndividual";
            object id = PoliId.Value;
            if (fUpDependientes.HasFile)
            {
                string fileName = fUpDependientes.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        string errMsg = string.Empty;
                        Entity titular = new Entity("bienasegurado");
                        titular.Attributes.Add(new Models.Attribute("id", "", "int"));
                        titular.Keys.Add(new Key("inciso", txtNumCertificado.Text, "string"));
                        titular.Keys.Add(new Key("poliza", id.ToString(), "int"));
                        List<Entity> results = SiteFunctions.GetValues(titular, out errMsg);
                        if (results.Count > 0)
                        {
                            bool isXlsx = splitStr[1] == "xlsx";
                            if (SiteFunctions.importDependientes(fUpDependientes.FileContent, id.ToString(), results[0].getAttrValueByName("id"), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out errMsg))
                            {
                                searchDependientes();
                            }
                            else
                            {
                                showMessage(errMsg, true);
                            }
                        }
                        else
                        {
                            showMessage("No se encontraron titulares ligados a la póliza " + errMsg, true);
                        }
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
        }

        private void searchDependientes()
        {
            PaneName.Value = "VidaIndividual";
            object id = PoliId.Value;
            SearchDependientes.Value = string.Empty;
            if (txtSearchDependientes.Text != string.Empty)
            {
                SearchDependientes.Value = '%' + txtSearchDependientes.Text + '%';
            }
            DataTable dtDependientes = searchRelRecords(recordType.Dependientes, SearchDependientes.Value, id.ToString());



            gvDependientes.DataSource = dtDependientes;
            gvDependientes.DataBind();
        }

        private void valDescarteBtn()
        {
            object id = PoliId.Value.Trim() == "" ? null : PoliId.Value;
            if (id != null && txtPoliza.Text == string.Empty)
            {
                BtnDescarte.Enabled = true;
                switch(txtFormaPago.SelectedValue)
                {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "13":
                    case "12":
                        txtPrimaNeta.Enabled = true;
                        txtGastosRecibo.Enabled = true;
                        txtProrratear.Enabled = false;
                        break;
                    default:
                        txtPrimaNeta.Enabled = false;
                        txtGastosRecibo.Enabled = false;
                        txtProrratear.Enabled = true;
                        break;
                        

                }
                //if (!txtFormaPago.Text.Contains("DxN") && !txtFormaPago.Text.Contains("VP"))
                //{
                //    Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                //}
                //else
                //{
                //    Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                //}
            }
            else
            {
                BtnDescarte.Enabled = false;
            }
        }

        protected void btnDescarteOk_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            decimal primaAmt = 0M;
            decimal.TryParse(txtPrima.Text, out primaAmt);
            object id = PoliId.Value.Trim() == "" ? null : PoliId.Value;

            if (primaAmt == 0M)
            {
                showMessage("La prima no ha sido capturada", true);
                return;
            }
            if (txtPoliza.Text.Trim() == "")
            {
                showMessage("Debe de capturar el número de póliza.", true);
                return;
            }
            if (txtPoliza.Text.Trim() != "")
            {
                Entity Tramiteval = new Entity("Tramite");
                Tramiteval.Attributes.Add(new Models.Attribute("Id"));
                Tramiteval.Keys.Add(new Key("Folio", txtPoliza.Text.Trim(),"string"));
                Tramiteval = SiteFunctions.GetEntity(Tramiteval, out errMsg);
                if(Tramiteval != null)
                {
                    showMessage("El número de póliza ya existe.", true);
                    return;
                }
            }
            if (id != null)
            {
                Entity negociacion = new Entity("Negociacion");
                negociacion.Attributes.Add(new Models.Attribute("id", "", "int"));
                negociacion.Attributes.Add(new Models.Attribute("comisionpct", "", "float"));
                negociacion.Keys.Add(new Key("Id", txtNegociacion.SelectedValue, "int"));

                List<Entity> result = SiteFunctions.GetValues(negociacion, out errMsg);
                if (result.Count > 0)
                {
                    if (txtComisionPor.Text == result[0].getAttrValueByName("comisionpct"))
                    {
                        int numticketsTxt = 0;
                        int numTickets = 0, numSeries = 0, intervalPerTicket = 0;
                        getNumTickets(out numTickets, out numSeries, out intervalPerTicket);

                        int.TryParse(txtNumRecibos.Text, out numticketsTxt);

                        if (numTickets >= numticketsTxt)
                        {
                            numTickets = numticketsTxt != 0 ? numticketsTxt : numTickets;

                            DataTable dtComisiones = searchRelRecords(recordType.Comision, string.Empty, id.ToString());
                            if (dtComisiones.Rows.Count > 0)
                            {
                                descartaOT(numTickets, numSeries, intervalPerTicket);
                            }
                            else
                            {
                                showMessage("La OT no cuenta con comisiones para directores asociados", true);
                            }
                        }
                        else
                        {
                            showMessage("El número de recibos capturados no concuerda con la forma de pago de la OT", true);
                        }
                    }
                    else
                    {
                        showMessage("La comisión de la OT no es igual a la de la negociación asociada", true);
                    }
                }
                else
                {
                    showMessage("La OT no cuenta con negociación asociada", true);
                }
            }


        }

        private void descartaOT(int numTickets, int numSeries, int intervalPerTicket)
        {
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            Auditable audit = new Auditable();
            audit.userId = int.Parse(userId);
            audit.opDate = DateTime.Now;

            float totalComisiones = 0;
            float totalPrima = 0;
            float totalrecargo = 0;
            string errMsg = string.Empty;
            object id = PoliId.Value.Trim() == "" ? null : PoliId.Value;
            if(id == null)
            {
                SavePoliza();
                id = PoliId.Value.Trim() == "" ? null : PoliId.Value;
            }
            if (txtTotalCom.Text != string.Empty)
            {
                float.TryParse(txtTotalCom.Text, out totalComisiones);
            }
            else
            {
                float fValue = 0;

                float.TryParse(txtComision.Text, out fValue);
                totalComisiones += fValue;
                float.TryParse(txtSobreComision.Text, out fValue);
                totalComisiones += fValue;
                float.TryParse(txtBono.Text, out fValue);
                totalComisiones += fValue;

                txtTotalCom.Text = totalComisiones.ToString();
            }
            float.TryParse(txtPrima.Text,out totalPrima);
            float.TryParse(txtRecargos.Text, out totalrecargo);
            Entity taxEntity = new Entity("Impuesto");
            taxEntity.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            taxEntity.Keys.Add(new Key("Id", txtImpuestoPor.SelectedValue, "int"));
            List<Entity> results = SiteFunctions.GetValues(taxEntity, out errMsg);
            bool success = false;
            bool VP = txtFormaPago.SelectedItem.Text.Contains("VP");
            if (txtgenRecibos.SelectedValue == "1")
            {
                for (int cntSeries = 0; cntSeries < numSeries; cntSeries++)
                {
                    float GastosRecibos = 0;
                    float PrimaNeta = 0;
                    float.TryParse(txtGastosRecibo.Text, out GastosRecibos);
                    float.TryParse(txtPrimaNeta.Text, out PrimaNeta);
                    if (PrimaNeta <= totalPrima)
                    {
                        success = SiteFunctions.proccessTickets(totalPrima, numTickets, DateTime.Parse(txtInicio.Text), intervalPerTicket, totalComisiones, float.Parse(txtGastos.Text), PrimaNeta, GastosRecibos, float.Parse(results[0].getAttrValueByName("Porcentaje")), cntSeries + 1, int.Parse(txtMoneda.SelectedValue), int.Parse(txtImpuestoPor.SelectedValue), int.Parse(id.ToString()), int.Parse(txtUbicacion.Text), int.Parse(txtCentroBeneficio.Text), int.Parse(userId), audit, out errMsg, txtProrratear.Checked,txtComisionPor.Text,txtRecargosPor.Text, txtSobreComisionPor.Text,txtBonoPor.Text, totalrecargo, VP, 1);
                    }
                    else
                    {
                        success = false;
                        errMsg = "La prima Neta del primer recibo no puede ser mayor a la prima neta de la póliza";
                    }
                    if (!success)
                    {
                        showMessage(errMsg, true);
                        return;
                    }
                }
            }

            
            Entity poliza = new Entity("Tramite");
            poliza.Attributes.Add(new Models.Attribute("Folio", txtPoliza.Text, "string"));
            poliza.Keys.Add(new Key("Id", id.ToString(), "int"));
            success = SiteFunctions.SaveEntity(poliza, Method.PUT, out errMsg);

            poliza = new Entity("Poliza");
            poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", "1", "string"));
            poliza.Keys.Add(new Key("Id", id.ToString(), "int"));
            success = SiteFunctions.SaveEntity(poliza, Method.PUT, out errMsg);
            if (success)
            {
                BtnDescarte.Visible = false;
                Response.Redirect("Poliza.aspx?id=" + id + "&descarteOk=1");
            }
            else
            {
                showMessage(errMsg, true);
            }
        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {

        }

        private void getNumTickets(out int numTickets, out int numSeries, out int intervalPerTicket)
        {
            numTickets = 0;
            numSeries = 0;
            intervalPerTicket = 0;

            int formaPago = 0;

            int.TryParse(txtFormaPago.SelectedValue, out formaPago);

            FormaPagoEnum formaPagoSelected = (FormaPagoEnum)formaPago;

            switch (formaPagoSelected)
            {
                case FormaPagoEnum.Mensual:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Bimestral:
                    numTickets = 6;
                    numSeries = 1;
                    intervalPerTicket = 2;
                    break;
                case FormaPagoEnum.Trimestral:
                    numTickets = 4;
                    numSeries = 1;
                    intervalPerTicket = 3;
                    break;
                case FormaPagoEnum.Cuatrimestral:
                    numTickets = 3;
                    numSeries = 1;
                    intervalPerTicket = 4;
                    break;
                case FormaPagoEnum.Semestral:
                    numTickets = 2;
                    numSeries = 1;
                    intervalPerTicket = 6;
                    break;
                case FormaPagoEnum.Anual:
                    numTickets = 1;
                    numSeries = 1;
                    intervalPerTicket = 0;
                    break;
                //validar como sacar la serie y si aplica para todas las ots
                case FormaPagoEnum.Mensual_VP:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Trimestral_VP:
                    numTickets = 4;
                    numSeries = 1;
                    intervalPerTicket = 3;
                    break;
                case FormaPagoEnum.Semestral_VP:
                    numTickets = 2;
                    numSeries = 1;
                    intervalPerTicket = 6;
                    break;
                case FormaPagoEnum.Contado_VP:
                    numTickets = 1;
                    numSeries = 1;
                    intervalPerTicket = 0;
                    break;
                //validar como sacar la serie y si aplica para todas las ots
                case FormaPagoEnum.Mensual_DxN:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Quincenal_DxN:
                    numTickets = 24;
                    numSeries = 1;
                    intervalPerTicket = 15;
                    break;
                case FormaPagoEnum.Catorcenal_DxN:
                    numTickets = 26;
                    numSeries = 1;
                    intervalPerTicket = 14;
                    break;
            }
        }

        //new
        protected void txtNegociacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                    filterNegociacion();
            }
            catch (Exception)
            {
                
            }
        
        }
        //new
        private void filterNegociacion()
        {
            if (txtNegociacion.SelectedValue != null)
            {
                Entity Negociacion = new Entity("Negociacion");
                Negociacion.Attributes.Add(new Models.Attribute("ComisionPct", "", "float"));
                Negociacion.Attributes.Add(new Models.Attribute("BonoPct", "", "float"));
                Negociacion.Attributes.Add(new Models.Attribute("SobrecomisionPct", "", "float"));
                Negociacion.Keys.Add(new Key("Id", txtNegociacion.SelectedValue, "int"));

                string errMsg = string.Empty;
                List<Entity> result = SiteFunctions.GetValues(Negociacion, out errMsg);

                if (result.Count > 0)
                {

                    Entity pliza = result[0];
                    float primaAmt = 0;
                    float.TryParse(txtPrima.Text, out primaAmt);

                    txtComisionPor.Text = pliza.getAttrValueByName("ComisionPct");
                    txtBonoPor.Text = pliza.getAttrValueByName("BonoPct");
                    txtSobreComisionPor.Text = pliza.getAttrValueByName("SobrecomisionPct");
        
                                  

                    
                        txtComision.Text = primaAmt == 0 ? "0" : ((primaAmt * float.Parse(txtComisionPor.Text)) / 100).ToString();
                        txtBono.Text = primaAmt == 0 ? "0" : ((primaAmt * float.Parse(txtBonoPor.Text)) / 100).ToString();
                        txtSobreComision.Text = primaAmt == 0 ? "0" : ((primaAmt * float.Parse(txtSobreComisionPor.Text)) / 100).ToString();
                        txtTotalCom.Text = (float.Parse(txtComision.Text) + float.Parse(txtBono.Text) + float.Parse(txtSobreComision.Text)).ToString();
                    

                }
            }
        }


        [WebMethod]
        public static string getAmountImpuesto(string idporcentaje, string neto)
        {
            string result = string.Empty;

            Entity Impuesto = new Entity("Impuesto");
            Impuesto.Keys.Add(new Models.Key("id", idporcentaje, "int"));
            Impuesto.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            string errMsg = string.Empty;
            List<Entity> resultado = SiteFunctions.GetValues(Impuesto, out errMsg);

            if (resultado.Count > 0)
            {
                Entity impuesRes = resultado[0];

                var porcentaje = impuesRes.getAttrValueByName("Porcentaje");
                if (neto != "")
                {
                    float primaval = float.Parse(neto);
                    result = ((primaval * float.Parse(porcentaje)) / 100).ToString();
                }

            }

            return result;
        }

        protected void txtInicio_TextChanged(object sender, EventArgs e)
        {
            DateTime dateinit = new DateTime();

            DateTime.TryParse(txtInicio.Text, out dateinit);

            txtFin.Text = dateinit.AddYears(1).ToShortDateString();
        }

        protected void btnTitular_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {

                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Titular = new Entity("ContactoSimple");
                Titular.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Titular.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
                Titular.Keys.Add(new Key("Cliente", txtCliente.SelectedValue, "int"));
                Titular.useAuditable = false;
                listResult = SiteFunctions.GetValues(Titular, out errmess);
                txtTitular.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtTitular.DataValueField = "Id";
                txtTitular.DataTextField = "NombreCompleto";
                txtTitular.DataBind();
                txtTitular.SelectedValue = Session["retval"].ToString();
                Session["retval"] = "";

            }
        }

        protected void btnSearchComisiones_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Comisiones";

            object id = PoliId.Value;
            if (id != null)
            {
                SearchIdComisiones.Value = string.Empty;
                if (txtSearchComisiones.Text != string.Empty)
                {
                    SearchIdComisiones.Value = '%' + txtSearchComisiones.Text + '%';
                }
                DataTable dtComisiones = searchRelRecords(recordType.Comision, SearchIdComisiones.Value, id.ToString());
                gvComisiones.DataSource = dtComisiones;
                gvComisiones.DataBind();
            }
            var id2 = Request.QueryString["id"];
            if (id2 == null)
            {
                Response.Redirect("poliza.aspx?id=" + PoliId.Value + "&panename=Comisiones");
            }

        }

        protected void btnEjecutivoAseguradora_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Admin";
            if(Session["retval"] != "" && Session["retval"] != null)
            {
                ejecutivoAseguradoraFilter();
                try
                {
                    txtEjeAseuradora.SelectedValue = Session["retval"].ToString();
                }
                catch (Exception)
                {
                }
                Session["retval"] = "";
            }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void btnDeleteIncisos_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";

            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        seleccionado = true;
                        break;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirm_value"];
                if (confirmValue == "Si")
                {
                    foreach (GridViewRow row in gvIncisosAutos.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                string id = gvIncisosAutos.Rows[row.RowIndex].Cells[2].Text;
                                string leadId = string.Empty;
                                Entity Auto = new Entity("Auto");
                                Auto.Keys.Add(new Key("Id", id, "int64"));
                                SiteFunctions.DeleteEntity(Auto, Method.POST, out errmess);
                                Entity Inciso = new Entity("BienAsegurado");
                                Inciso.Keys.Add(new Key("Id", id, "int64"));
                                SiteFunctions.DeleteEntity(Inciso, Method.POST, out errmess);

                            }
                        }
                    }

                    recalcincisos();
                    
                }

            }
            else
            {
                this.showMessage("Debe Seleccionar un Inciso.", true);
            }
        }

        

        protected void btnBitacora_Click(object sender, EventArgs e)
        {
            searchBitacora();
        }

        private void searchBitacora()
        {
            PaneName.Value = "Bitacora";

            object id = PoliId.Value;
            if (id != null)
            {
                DataTable dtBitacora = searchRelRecords(recordType.Bitacora, string.Empty , id.ToString());
                GVBitacora.DataSource = dtBitacora;
                GVBitacora.DataBind();
            }
            
            
        }
        protected void btnSeleccionarTodos_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (!chkBox.Checked)
                    {
                        chkBox.Checked = true;
                    }
                }
            }
        }

        protected void btnDeseleccionarTodos_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        chkBox.Checked = false;
                    }
                }
            }
        }

        protected void txtFormaPago_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!txtFormaPago.Text.Contains("DxN"))
            //{
                //if ( PolizaType.Value == "6")
                //{
                    if (txtFormaPago.SelectedItem.Text.Contains("_VP"))
                    {
                        Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                        txtPrimaNeta.Enabled = false;
                        txtGastosRecibo.Enabled = false;
                        txtProrratear.Enabled = true;
                    }
                    else
                    {
                        Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                        txtPrimaNeta.Enabled = true;
                        txtGastosRecibo.Enabled = true;
                        txtProrratear.Enabled = false;
                    }
                //}
                //else
                //{
                //    Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                //    txtPrimaNeta.Enabled = true;
                //    txtGastosRecibo.Enabled = true;
                //}
            //}
            //else
            //{
            //    Prorrateo.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            //    txtPrimaNeta.Enabled = true;
            //    txtGastosRecibo.Enabled = true;
            //}
        }

        protected void BtnRenovar_Click(object sender, EventArgs e)
        {
            object id = PoliId.Value.Trim() == "" ? null : PoliId.Value.Trim();
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            JoinEntity joinEntity = new JoinEntity();
            Entity Poliza = new Entity("Poliza");
            Auditable audi = new Auditable();
            audi.userId = int.Parse(userId);
            audi.opDate = DateTime.Now;

            #region "Map Tramite
            Entity tramite = new Entity("Tramite");
            tramite.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
            tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
            tramite.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
            tramite.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            //tramite.Attributes.Add(new Models.Attribute("FechaEmision", DateTime.Now.Date.ToShortDateString(), "datetime"));
            tramite.Attributes.Add(new Models.Attribute("FormaPago", txtFormaPago.SelectedValue, "int"));
            tramite.Attributes.Add(new Models.Attribute("VigenciaInicio", txtFin.Text, "datetime"));
            DateTime fechafin = DateTime.Now;
            DateTime.TryParse(txtFin.Text,out fechafin);
            fechafin = fechafin.AddYears(1);
            tramite.Attributes.Add(new Models.Attribute("VigenciaFin", fechafin.ToString("dd/MM/yyyy"), "datetime"));
            tramite.Attributes.Add(new Models.Attribute("Total", txtTotal.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionPct", txtComisionPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionImporte", txtComision.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosImporte", txtRecargos.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosPct", txtRecargosPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            if (txtNeto.Text == "")
            {
                float prima = 0;
                float gastos = 0;
                float recargos = 0;
                float.TryParse(txtPrima.Text, out prima);
                float.TryParse(txtGastos.Text, out gastos);
                float.TryParse(txtRecargos.Text, out recargos);
                if (txtFormaPago.SelectedValue == "8" || txtFormaPago.SelectedValue == "9" || txtFormaPago.SelectedValue == "10" || txtFormaPago.SelectedValue == "11" || txtFormaPago.SelectedValue == "14")
                {
                    recargos = 0;
                    gastos = 0;
                }
                txtNeto.Text = (prima + gastos+recargos).ToString();
            }
            tramite.Attributes.Add(new Models.Attribute("Neto", txtNeto.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Prima", txtPrima.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Gastos", txtGastos.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("FechaEnvio", txtFechaEnvio.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("FechaEmision", txtFechaEmision.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("EstatusMesaDeControl", "14", "int"));
            tramite.useAuditable = true;
            tramite.auditable = audi;
            #endregion

            #region "Map poliza"
            Poliza.Attributes.Add(new Models.Attribute("Contratante", txtCliente.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Aseguradora", txtAseguradora.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Ramo", txtRamo.SelectedValue, "int"));

            Poliza.Attributes.Add(new Models.Attribute("Renovable", "0", "int"));

            Poliza.Attributes.Add(new Models.Attribute("SobreComisionPct", txtSobreComisionPor.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("SobreComisionImporte", txtSobreComision.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("BonoPct", txtBonoPor.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("BonoImporte", txtBono.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("Descuento", txtDescuento.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", "0", "int"));
            Poliza.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            Poliza.Attributes.Add(new Models.Attribute("EstatusCobranza", "1", "int"));
            Poliza.Attributes.Add(new Models.Attribute("PolizaOriginal", id.ToString(), "string"));
            Poliza.Attributes.Add(new Models.Attribute("Impuesto", txtImpuestoPor.Text, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Moneda", txtMoneda.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Agente", txtAgente.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Ejecutivo", txtEjecutivo.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("SubRamo", txtSubramo.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionCobro", txtDirCobro.Text, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionFiscal", txtDirFiscal.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("DireccionEnvio", txtDirEnvio.Text, "int"));
            if (txtEjeAseuradora.SelectedValue != string.Empty)
            {
                Poliza.Attributes.Add(new Models.Attribute("EjecutivoAseguradora", txtEjeAseuradora.SelectedValue, "int"));
            }
            Poliza.Attributes.Add(new Models.Attribute("Supervisor", txtSupervisor.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Director", txtDirector.SelectedValue, "int"));
            if (txtTitular.SelectedValue != string.Empty)
            {
                Poliza.Attributes.Add(new Models.Attribute("Titular", txtTitular.SelectedValue, "int"));
            }
            //Poliza.Attributes.Add(new Models.Attribute("CoTitular", coti, "int"));
            Poliza.Attributes.Add(new Models.Attribute("Grupo", "28181", "int"));
            Poliza.Attributes.Add(new Models.Attribute("Negociacion", txtNegociacion.SelectedValue, "int"));
            Poliza.Attributes.Add(new Models.Attribute("NumeroCobranza", txtNumCobranza.Text, "string"));
            Poliza.useAuditable = true;
            #endregion

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = Poliza;
            joinEntity.JoinKey = new Models.Attribute("id", "", "int");

            tramite.ChildEntities.Add(joinEntity);

            //#region Pestañas complementarias todo tipo de polizas
            //Entity slip = new Entity("Slip");
            //slip.Attributes.Add(new Models.Attribute("Descripcion", txtSlip.Text.Replace("'", "''"), "string"));
            //joinEntity = new JoinEntity();
            //joinEntity.ChildEntity = slip;
            //joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            //Poliza.ChildEntities.Add(joinEntity);

            //Entity cobertura = new Entity("Coberturas");
            //cobertura.Attributes.Add(new Models.Attribute("Descripcion", txtCoberturas.Text.Replace("'", "''"), "string"));
            //joinEntity = new JoinEntity();
            //joinEntity.ChildEntity = cobertura;
            //joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            //Poliza.ChildEntities.Add(joinEntity);

            //Entity CondicionesEspeciales = new Entity("CondicionesEspeciales");
            //CondicionesEspeciales.Attributes.Add(new Models.Attribute("Descripcion", txtCondSpec.Text.Replace("'", "''"), "string"));
            //joinEntity = new JoinEntity();
            //joinEntity.ChildEntity = CondicionesEspeciales;
            //joinEntity.JoinKey = new Models.Attribute("Id", "", "int");
            //Poliza.ChildEntities.Add(joinEntity);
            //#endregion

            #region Mapear tipo de poliza
            TipoPoliza tipoPol = (TipoPoliza)int.Parse(PolizaType.Value);
            Entity bienAsegurado, polizaVidaIndv, polizaIndv, certificado, certificadoVida, polizaPool;
            switch (tipoPol)
            {
                case TipoPoliza.AutosFlotilla:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
                case TipoPoliza.AutosIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaAutoIndv = new Entity("PolizaAutoIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaAutoIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #region mapea bien asegurado
                    bienAsegurado = this.llenaBienAsegurado(txtInciso.Text, txtPrima.Text, txtBeneficiarioAuto.SelectedValue, txtInciso.Text);
                    bienAsegurado.auditable = audi;
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = bienAsegurado;
                    joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    #endregion
                    //#region mapea auto
                    //Entity auto = new Entity("auto");
                    //auto.Attributes.Add(new Models.Attribute("Serie", txtSerie.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("Motor", txtMotor.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("Placas", txtPlacas.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("ConductorHabitual", txtConductor.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("Modelo", txtModelo.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("Version", txtVersion.Text, "string"));
                    //auto.Attributes.Add(new Models.Attribute("Marca", txtMarca.SelectedValue, "string"));
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = auto;
                    //joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    //bienAsegurado.ChildEntities.Add(joinEntity);

                    //#endregion
                    break;
                case TipoPoliza.VidaIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    polizaVidaIndv = new Entity("PolizaVidaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaVidaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    //#region mapea bien asegurado
                    //bienAsegurado = this.llenaBienAsegurado(txtNumCertificado.Text, "0", string.Empty, txtNumCertificado.Text);
                    //bienAsegurado.auditable = audi;
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = bienAsegurado;
                    //joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    //Poliza.ChildEntities.Add(joinEntity);
                    //#endregion
                    //#region Certificado
                    //certificado = new Entity("Certificado");
                    //certificado.Attributes.Add(new Models.Attribute("Nombre", txtVINombre.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("ApellidoPaterno", txtVIAPaterno.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("ApellidoMaterno", txtVIAMaterno.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("Antiguedad", txtAntiguedad.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("FechaNacimiento", txtBirthDay.Text, "datetime"));
                    //certificado.Attributes.Add(new Models.Attribute("Sexo", txtSex.SelectedValue, "int"));
                    //certificado.Attributes.Add(new Models.Attribute("Tipo", "0", "int"));
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = certificado;
                    //joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    //bienAsegurado.ChildEntities.Add(joinEntity);

                    //certificadoVida = new Entity("CertificadoVida");
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = certificadoVida;
                    //joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    //certificado.ChildEntities.Add(joinEntity);
                    //#endregion
                    break;
                case TipoPoliza.VidaPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaVidaPool = new Entity("PolizaVidaPoblacion");
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAsegVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaTopada", txtSumaAsegTopadaVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGMVP.Text, "float"));
                    polizaVidaPool.Attributes.Add(new Models.Attribute("SumaAseguradaTopadaSMGM", txtSumaAsegTopadaSMGMVP.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaVidaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    //Falta Grupos se hace por importación?

                    break;
                case TipoPoliza.GMMIndividual:
                    polizaIndv = new Entity("PolizaIndividual");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaGMMIndv = new Entity("PolizaGmmIndividual");
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("EsquemaPrimaMinima", "0", "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAsegurada.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGM.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("CoaseguroPct", txtCoaseguroPrc.Text, "float"));
                    polizaGMMIndv.Attributes.Add(new Models.Attribute("DeduciblePct", txtDeduciblePrc.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaGMMIndv;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    //#region mapea bien asegurado
                    //bienAsegurado = this.llenaBienAsegurado(txtNumCertificado.Text, "0", string.Empty, txtNumCertificado.Text);
                    //bienAsegurado.auditable = audi;
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = bienAsegurado;
                    //joinEntity.JoinKey = new Models.Attribute("poliza", "", "int");
                    //Poliza.ChildEntities.Add(joinEntity);
                    //#endregion
                    //#region Certificado
                    //certificado = new Entity("Certificado");
                    //certificado.Attributes.Add(new Models.Attribute("Nombre", txtVINombre.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("ApellidoPaterno", txtVIAPaterno.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("ApellidoMaterno", txtVIAMaterno.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("Antiguedad", txtAntiguedad.Text, "string"));
                    //certificado.Attributes.Add(new Models.Attribute("FechaNacimiento", txtBirthDay.Text, "datetime"));
                    //certificado.Attributes.Add(new Models.Attribute("Sexo", txtSex.SelectedValue, "int"));
                    //certificado.Attributes.Add(new Models.Attribute("Tipo", "0", "int"));
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = certificado;
                    //joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    //bienAsegurado.ChildEntities.Add(joinEntity);

                    //certificadoVida = new Entity("CertificadoGmm");
                    //joinEntity = new JoinEntity();
                    //joinEntity.ChildEntity = certificadoVida;
                    //joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    //certificado.ChildEntities.Add(joinEntity);
                    //#endregion
                    break;
                case TipoPoliza.GMMPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaGMMPool = new Entity("PolizaGmmPoblacion");
                    polizaGMMPool.Attributes.Add(new Models.Attribute("EsquemaPrimaMinima", txtEsqPrimaMinPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("DeduciblePct", txtDeducPctGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("CoaseguroPct", txtCoaseguroPctGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("SumaAsegurada", txtSumAseguradaGMMPool.Text, "float"));
                    polizaGMMPool.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", txtSumAsegSMGMGMMPool.Text, "float"));

                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaGMMPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
                case TipoPoliza.DanosPool:
                    polizaPool = new Entity("PolizaPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);

                    Entity polizaDanosPool = new Entity("PolizaDanhosPool");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaDanosPool;
                    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                    Poliza.ChildEntities.Add(joinEntity);
                    break;
            }
            #endregion

            string errmess = "";
            string idPoliza = string.Empty;
            Method method = Method.POST;
            Poliza.auditable = audi;
            if (SiteFunctions.SaveEntity(tramite, method, out errmess))
            {
                idPoliza = idPoliza == string.Empty ? errmess : idPoliza;
                
                if (TipoPoliza.AutosIndividual == tipoPol)
                {
                    updAutoForIndv(idPoliza);

                }
                #region Copiar Comisiones Director
                Entity entity = new Entity("ComisionAsociado");
                entity.Attributes.Add(new Models.Attribute("DirectorAsociado"));
                entity.Attributes.Add(new Models.Attribute("Porcentaje"));
                entity.Keys.Add(new Key("Poliza", id.ToString(), "int"));
                List<Entity> ListEntities = SiteFunctions.GetValues(entity, out errmess);
                foreach (Entity comdir in ListEntities)
                {
                    Entity copyaux = new Entity("ComisionAsociado");
                    copyaux.Attributes.Add(new Models.Attribute("DirectorAsociado", comdir.getAttrValueByName("DirectorAsociado").ToString(), "int"));
                    copyaux.Attributes.Add(new Models.Attribute("Porcentaje", comdir.getAttrValueByName("Porcentaje").ToString(), "float"));
                    copyaux.Attributes.Add(new Models.Attribute("Poliza", comdir.getAttrValueByName("Poliza").ToString(), "int"));
                    SiteFunctions.SaveEntity(copyaux, Method.POST,out errmess);
                }

                #endregion
                txtPolNueva.Text = idPoliza.ToString();
                Entity pol = new Entity("Poliza");
                pol.Attributes.Add(new Models.Attribute("PolizaNueva",idPoliza,"int"));
                pol.Attributes.Add(new Models.Attribute("EstatusPoliza", "4", "int"));
                pol.Keys.Add(new Key("Id",id.ToString(),"int"));
                SiteFunctions.SaveEntity(pol,Method.PUT,out errmess);
                
                showMessage("La operación de renovación se realizó con éxito, descarte la nueva póliza. ", false);
                RenovarState();              
            }
            else
            {
                showMessage(errmess, true);
            }
        }

        protected void chckRenovable_CheckedChanged(object sender, EventArgs e)
        {
            BtnRenovar.Visible = chckRenovable.Checked;
            RenovarState();
        }

        private void RenovarState()
        {
            if (chckRenovable.Checked && txtPolOriginal.Text == string.Empty)
            {
                BtnRenovar.Visible = true;

            }
            else
            {
                BtnRenovar.Visible = false;
            }
        }

        protected void btndeleteComisiones_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";

            foreach (GridViewRow row in gvComisiones.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        seleccionado = true;
                        break;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirm_valuecd"];
                if (confirmValue == "Si")
                {
                    foreach (GridViewRow row in gvComisiones.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                string id = gvComisiones.Rows[row.RowIndex].Cells[2].Text;
                                string leadId = string.Empty;
                                Entity ComAsoc = new Entity("ComisionAsociado");
                                ComAsoc.Keys.Add(new Key("Id", id, "int64"));
                                SiteFunctions.DeleteEntity(ComAsoc, Method.POST, out errmess);
                                

                            }
                        }
                    }

                    PaneName.Value = "Comisiones";

                    object idpol = PoliId.Value;
                    if (idpol != null)
                    {
                        SearchIdComisiones.Value = string.Empty;
                        if (txtSearchComisiones.Text != string.Empty)
                        {
                            SearchIdComisiones.Value = '%' + txtSearchComisiones.Text + '%';
                        }
                        DataTable dtComisiones = searchRelRecords(recordType.Comision, SearchIdComisiones.Value, idpol.ToString());
                        gvComisiones.DataSource = dtComisiones;
                        gvComisiones.DataBind();
                    }
                    var id2 = Request.QueryString["id"];
                    if (id2 == null)
                    {
                        Response.Redirect("poliza.aspx?id=" + PoliId.Value + "&panename=Comisiones");
                    }

                }

            }
            else
            {
                this.showMessage("Debe Seleccionar un Prospecto.", true);
            }
        }

        protected void btnrecalcIncisosAutos_Click(object sender, EventArgs e)
        {
            recalcincisos();
            
        }

        private void recalcincisos()
        {
            PaneName.Value = "IncisosAutos";
            object id = PoliId.Value;
            DataTable dtInciso = searchRelRecords(recordType.IncisoAutos, SearchIdInciso.Value, id.ToString());
            getPrimaIncisos(dtInciso);
            gvIncisosAutos.DataSource = dtInciso;
            gvIncisosAutos.DataBind();
            SavePoliza();
        }

        private void getPrimaIncisos(DataTable dti)
        {
            decimal prima = 0M;
            decimal primaaux = 0M;
            foreach (DataRow dr in dti.Rows)
            {
                decimal.TryParse(dr["Prima"].ToString(), out primaaux);
                prima += primaaux;

            }
            txtPrima.Text = prima.ToString();
        }
        private bool checkifPending(string user, string id, bool checkaprobador, out List<Entity> PendingAprovals)
        {
            string errmess = "";

            Entity checkcanceled = new Entity("AutorizacionCancelPol");
            checkcanceled.Attributes.Add(new Models.Attribute("Id"));
            checkcanceled.Attributes.Add(new Models.Attribute("UsuarioAutorizo"));
            checkcanceled.Keys.Add(new Key("Poliza", id, "int64"));
            if (checkaprobador)
            {
                checkcanceled.Keys.Add(new Key("UsuarioAutorizo", user, "int64"));
            }
            checkcanceled.Keys.Add(new Key("EstatusAutorizacion", "0", "int"));
            List<Entity> estatus = SiteFunctions.GetValues(checkcanceled, out errmess);
            PendingAprovals = estatus;
            return estatus.Count == 0;


        }
        protected void btnSaveReject_Click(object sender, EventArgs e)
        {
            string errmess = string.Empty;
            string id = Request.QueryString["id"];
            string ExecUserid = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            List<Entity> Pendientes = new List<Entity>();

            if (!checkifPending("", id, false, out Pendientes))
            {
                foreach (Entity aprobpend in Pendientes)
                {
                    if (aprobpend.getAttrValueByName("UsuarioAutorizo") == SiteFunctions.getuserid(Context.User.Identity.Name, true))
                    {
                        Entity Aprobar = new Entity("AutorizacionCancelPol");
                        Aprobar.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "2", "int"));
                        if (aprobpend.getAttrValueByName("Aprobador") == ConfigurationManager.AppSettings["UsuarioCobranza"])
                        {
                            Aprobar.Attributes.Add(new Models.Attribute("UsuarioAutorizo", SiteFunctions.getuserid(Context.User.Identity.Name, false), "int64"));
                        }
                        Aprobar.Attributes.Add(new Models.Attribute("FechaRespuesta", DateTime.Now.ToString("yyyy-MM-dd"), "datetime"));
                        Aprobar.Keys.Add(new Key("Poliza", id, "int64"));
                        Aprobar.Keys.Add(new Key("UsuarioAutorizo", aprobpend.getAttrValueByName("UsuarioAutorizo"), "int64"));
                        if (SiteFunctions.SaveEntity(Aprobar, Method.PUT, out errmess))
                        {
                            this.showMessage(errmess, true);
                        }
                        else
                        {
                            Entity Comment = new Entity("ComentariosAut");
                            Comment.Attributes.Add(new Models.Attribute("IdReegistroRelacionado", id, "int64"));
                            Comment.Attributes.Add(new Models.Attribute("Comentario", txtComments.Text.Trim(), "string"));
                            Comment.Attributes.Add(new Models.Attribute("Usuario", SiteFunctions.getuserid(Context.User.Identity.Name, false), "string"));
                            errmess = string.Empty;
                            if (SiteFunctions.SaveEntity(Comment, Method.POST, out errmess))
                            {
                                this.showMessage(errmess, true);
                            }
                        }
                    }
                }

            }
        }
        private void insertAprovals(string id)
        {
            string errmess = string.Empty;
            Entity NoApprovals = new Entity("AutorizacionCancelPol");
            NoApprovals.Attributes.Add(new Models.Attribute("distinct Id"));
            NoApprovals.Keys.Add(new Models.Key("Poliza", id, "int64"));
            List<Entity> Aprov = SiteFunctions.GetValues(NoApprovals, out errmess);
            string AutId = Aprov.Count.ToString();
            //Primero el de cobranza

            Entity Approvals = new Entity("AutorizacionCancelPol");
            Approvals.useAuditable = false;
            //Approvals.Attributes.Add(new Models.Attribute("Id", AutId, "datetime"));
            Approvals.Attributes.Add(new Models.Attribute("FechaNotificacion", DateTime.Now.ToString("dd/MM/yyyy"), "datetime"));
            Approvals.Attributes.Add(new Models.Attribute("FechaRespuesta", "01/01/1900", "datetime"));
            Approvals.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "0", "int"));
            Approvals.Attributes.Add(new Models.Attribute("UsuarioAutorizo", ConfigurationManager.AppSettings["UsuarioCobranza"].ToString(), "int64"));
            Approvals.Attributes.Add(new Models.Attribute("Poliza", id, "int64"));
            SiteFunctions.SaveEntity(Approvals, Method.POST, out errmess);
            Entity Aprob = new Entity("AprobDivisionOp");
            Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("DivisionOperativa"));
            Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("Aprobador"));
            Aprob.Keys.Add(new Models.Key("Operacion", "1", "int"));
            Aprob.useAuditable = false;
            List<Entity> Entities = SiteFunctions.GetValues(Aprob, out errmess);
            DataTable dtAprob = SiteFunctions.trnsformEntityToDT(Entities);

            string emailsaprob = string.Empty;
            foreach (DataRow dr in dtAprob.Rows)
            {
                Approvals = new Entity("AutorizacionCancelPol");
                //Approvals.Attributes.Add(new Models.Attribute("Id", AutId, "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("FechaNotificacion", DateTime.Now.ToString("dd/MM/yyyy"), "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("FechaRespuesta", "01/01/1900", "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "0", "int"));
                Approvals.Attributes.Add(new Models.Attribute("UsuarioAutorizo", dr["Aprobador"].ToString(), "int64"));
                Approvals.Attributes.Add(new Models.Attribute("Poliza", id, "int64"));
                if (!SiteFunctions.SaveEntity(Approvals, Method.POST, out errmess))
                {
                    this.showMessage(errmess, true);
                }
            }

        }
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            SiteFunctions funct = new SiteFunctions();
            string body = "";
            insertAprovals(PoliId.Value);
            try
            {
                body = File.ReadAllText(HttpContext.Current.Server.MapPath("HtmlTemplates/CancelPolizaEmailTemplate.html"));
                body = body.Replace("%OT%", txtOt.Text).Replace("%Folio%", txtNopoliza.Text).Replace("%Ramo%", txtRamo.SelectedItem.Text).Replace("%Estatus%", txtEstatus.SelectedItem.Text).Replace("%URL%", ConfigurationManager.AppSettings["CancelPoliza"].ToString());

            }
            catch (Exception)
            {

            }
            if (body != "" && txtOt.Text != "")
            {
                string errmess = string.Empty;
                Entity Aprob = new Entity("AprobDivisionOp");
                Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("DivisionOperativa"));
                Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("Aprobador"));
                JoinEntity joinUsers = new JoinEntity();
                joinUsers.ChildEntity = new Entity("Usuario");
                joinUsers.JoinKey = new Models.Attribute("Id");
                joinUsers.ChildEntity.Attributes.Add(new Models.Attribute("Email"));
                selectJoinEntity usersj = new selectJoinEntity("Aprobador", 1, "Id");
                joinUsers.selectJoinList.Add(usersj);
                Aprob.useAuditable = false;
                Aprob.Keys.Add(new Models.Key("Operacion", "1", "int"));
                Aprob.ChildEntities.Add(joinUsers);
                List<Entity> Entities = SiteFunctions.GetValues(Aprob, out errmess);
                DataTable dtAprob = SiteFunctions.trnsformEntityToDT(Entities);


                string emailsaprob = string.Empty;
                foreach (DataRow dr in dtAprob.Rows)
                {
                    if (string.IsNullOrEmpty(emailsaprob))
                    {
                        emailsaprob = dr["UsuarioEmail"].ToString();

                    }
                    else
                    {
                        emailsaprob += ";" + dr["UsuarioEmail"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(emailsaprob))
                {
                    if (funct.sendEmail(emailsaprob, "Cognitum - Cancelación Póliza " + txtOt.Text, body))
                    {
                        this.showMessage("Se ha iniciado correctamente el proceso de cancelación", false);
                    }
                    else
                    {
                        this.showMessage("Ha habido un error al iniciar el proceso de cancelación", true);
                    }
                }
            }
        }

        protected void btnAprove_Click(object sender, EventArgs e)
        {
            string errmess = string.Empty;
            string id = Request.QueryString["id"];
            List<Entity> Pendientes = new List<Entity>();

            if (checkifPending("", id, false, out Pendientes))
            {
                foreach (Entity aprobpend in Pendientes)
                {
                    if (aprobpend.getAttrValueByName("UsuarioAutorizo") == SiteFunctions.getuserid(Context.User.Identity.Name, true))
                    {
                        Entity Aprobar = new Entity("AutorizacionCancelPol");
                        Aprobar.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "1", "int"));
                        if (aprobpend.getAttrValueByName("Aprobador") == ConfigurationManager.AppSettings["UsuarioCobranza"])
                        {
                            Aprobar.Attributes.Add(new Models.Attribute("UsuarioAutorizo", SiteFunctions.getuserid(Context.User.Identity.Name, false), "int64"));
                        }
                        Aprobar.Attributes.Add(new Models.Attribute("FechaRespuesta", DateTime.Now.ToString("yyyy-MM-dd"), "datetime"));
                        Aprobar.Keys.Add(new Key("Poliza", id, "int64"));
                        Aprobar.Keys.Add(new Key("UsuarioAutorizo", aprobpend.getAttrValueByName("UsuarioAutorizo"), "int64"));

                        if (SiteFunctions.SaveEntity(Aprobar, Method.PUT, out errmess))
                        {
                            this.showMessage(errmess, true);
                        }
                    }
                }
                if (Pendientes.Count == 1)
                {
                    errmess = string.Empty;
                    Entity CancelaPoliza = new Entity("Poliza");

                    CancelaPoliza.Action = 1;
                    bool Result = SiteFunctions.SaveEntity(CancelaPoliza, Method.PUT, out errmess);
                    if (!Result)
                    {
                        this.showMessage(errmess, true);
                    }
                }
            }
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (txtEmailSend.Text != "" && txtSubject.Text != "")
            {
                string HTML = generaCartaHTML();
                HTML = HTML.Replace("src=\"imgs/", "src=\"");
                SiteFunctions sf = new SiteFunctions();
                sf.sendEmail(txtEmailSend.Text, txtSubject.Text, HTML,Server.MapPath("~/imgs/cabecera.jpg"));
            }
        }

        protected void btnDescargar_Click(object sender, EventArgs e)
        {
            
            string HTML = generaCartaHTML();
            HTML = HTML.Replace("src=\"", "src=\""+ConfigurationManager.AppSettings["APPUrl"].ToString());
            string Guid = System.Guid.NewGuid().ToString();
            string _filename = Server.MapPath("~/Reportes/Polizas/" + Guid + ".pdf");
            
            
            var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(HTML, PdfSharp.PageSize.A4);
            pdf.Save(_filename);
            
            String csname1 = "downloadRpt";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            string url = "Reportes/Polizas/" + Guid+".pdf";
            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> window.open('" + url + "'); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }        
                
            
        }
    }
}