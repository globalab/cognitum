﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using CognisoWebApp.Models;
using System.Web.Security;


namespace CognisoWebApp.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);

            //if (!String.IsNullOrEmpty(returnUrl))
            //{
            //    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            //}
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                string errmess;
               var result =  SiteFunctions.Login(UserName.Text, Password.Text,out errmess);
               switch (result)
               {
                   case SignInStatus.Success:
                       FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
                       //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response) ;
                       break;
                   case SignInStatus.LockedOut:
                       Response.Redirect("/Account/Lockout");
                       break;
                   case SignInStatus.RequiresVerification:
                       Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                       Request.QueryString["ReturnUrl"],
                                                       RememberMe.Checked),
                                         true);
                       break;
                   case SignInStatus.Failure:
                   default:
                       if (errmess == null || errmess=="")
                       {
                           errmess = "Usuario y/o contraseña incorrectos, favor de verificar.";
                       }
                       FailureText.Text = errmess;
                       ErrorMessage.Visible = true;
                       break;
               }
            }
        }
    }
}