﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchIncisoEndosoD.aspx.cs" Inherits="CognisoWebApp.SearchIncisoEndosoD" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seleccionar recibos</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Seleccionar recibos a Conciliar</asp:Label>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div id="accordiontable" runat="server">
                    <div id="myTabContent">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchIncisos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="BtnSearchIncisos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="BtnSearchIncisos_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvSearchInciso" AllowPaging="true" OnPageIndexChanging="gvSearchInciso_PageIndexChanging" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkBxSelect" runat="server" />
                                                                <asp:HiddenField ID="hdnFldId" runat="server" Value='<%# Eval("Id") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkBxHeader" onclick="javascript:HeaderClick(this);" runat="server" />
                                                    </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="AutoSerie" HeaderText="Serie" />
                                                <asp:BoundField DataField="AutoMotor" HeaderText="Motor" />
                                                <asp:BoundField DataField="AutoPlacas" HeaderText="Placas" />
                                                <asp:BoundField DataField="AutoModelo" HeaderText="Modelo" />
                                                <asp:BoundField DataField="Prima" HeaderText="Prima" />
                                                <asp:BoundField DataField="Poliza" Visible ="false" />
                                                </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Button ID="BtnAceptar" Text="Agregar Incisos" runat="server" OnClick="BtnAceptar_Click" />
                                    </div>
                                </div>
                            </div>     
                   </div>     
                </div>     
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="taxPrc" runat="server" />
                 <asp:HiddenField ID="EndosoId" runat="server" />
                 <asp:HiddenField ID="PolizaId" runat="server" />
            </div>
        </div>
  </form>
</body>
</html>
