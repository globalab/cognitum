﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class Conciliacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                string Id = Request.QueryString["id"];
                if (ConId.Value != string.Empty)
                {
                    Id = ConId.Value;
                }
                if (SiteFunctions.IsUsuarioCobranza(this.Page.User.Identity.Name))
                {
                    btnAplicar.Enabled = false;
                }
                else
                {
                    btnAplicar.Enabled = true;
                }
                initCatalogos();
                if (!string.IsNullOrEmpty(Id))
                {
                    ConId.Value = Id;
                    RecibosDiv.Visible = true;
                    mapConciliacion(Id.ToString());
                    if (Estatus.Value != "0")
                    {
                        

                   
                        btnAplicar.Enabled = false;
                    }
                }
                else
                {
                    RecibosDiv.Visible = false;
                    
                }
            }
        }

        private void mapConciliacion(string _id)
        {
            Entity Conciliacion = new Entity("Conciliacion");

            Conciliacion.Attributes.Add(new Models.Attribute("Permiso"));
            Conciliacion.Attributes.Add(new Models.Attribute("Propietario"));
            Conciliacion.Attributes.Add(new Models.Attribute("Ubicacion"));
            Conciliacion.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            Conciliacion.useAuditable = true;

            Conciliacion.Attributes.Add(new Models.Attribute("Estatus"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComision"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionPagada"));
            Conciliacion.Attributes.Add(new Models.Attribute("Observaciones"));
            Conciliacion.Attributes.Add(new Models.Attribute("NumeroFactura"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaAplicacionCia"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaAbono"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaConciliacion"));
            Conciliacion.Attributes.Add(new Models.Attribute("Aseguradora"));
            Conciliacion.Attributes.Add(new Models.Attribute("Moneda"));
            Conciliacion.Attributes.Add(new Models.Attribute("UsuarioConcilio"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionNotas"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionConciliacion"));
            
            
            Conciliacion.Keys.Add(new Key("id", _id, "int"));
            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(Conciliacion, out errMsg);
            if (results.Count > 0)
            {
                txtTotalComision.Text = results[0].getAttrValueByName("TotalComision");
                txtTotalComisionPagada.Text = results[0].getAttrValueByName("TotalComisionPagada");
                txtObservaciones.Text = results[0].getAttrValueByName("Observaciones");
                txtFactura.Text = results[0].getAttrValueByName("NumeroFactura");
                txtAplicacionCia.Text = results[0].getAttrValueByName("FechaAplicacionCia");
                txtFechaAbono.Text = results[0].getAttrValueByName("FechaAbono");
                txtFechaCon.Text = results[0].getAttrValueByName("FechaConciliacion");
                txtAseguradora.SelectedValue = results[0].getAttrValueByName("Aseguradora");
                txtMoneda.SelectedValue = results[0].getAttrValueByName("Moneda");
                txtConcilio.SelectedValue = results[0].getAttrValueByName("UsuarioConcilio");
                txtTotalNotas.Text = results[0].getAttrValueByName("TotalComisionNotas");
                txtTotalCRecibos.Text = results[0].getAttrValueByName("TotalComisionConciliacion");
                Estatus.Value = results[0].getAttrValueByName("Estatus");

                DateTime fechaaudit = Convert.ToDateTime(results[0].getAttrValueByName("Audit_FechaAdd").ToString());
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(results[0].getAttrValueByName("Audit_FechaUMod").ToString());
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", results[0].getAttrValueByName("Audit_UsuarioAdd").ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", results[0].getAttrValueByName("Audit_UsuarioUMod").ToString(), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtId.Text = _id;
                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre"));
                Ubicacion.Keys.Add(new Models.Key("id", results[0].getAttrValueByName("Ubicacion"), "int"));
                errMsg = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(Ubicacion, out errMsg);
                if (listResult.Count > 0)
                {
                    txtUbicacion.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                Entity CB = new Entity("CentroDeBeneficio");
                CB.Attributes.Add(new Models.Attribute("Nombre"));
                CB.Keys.Add(new Models.Key("id", results[0].getAttrValueByName("CentroDeBeneficio"), "int"));
                errMsg = string.Empty;
                listResult = SiteFunctions.GetValues(CB, out errMsg);
                if (listResult.Count > 0)
                {
                    txtCentroBeneficio.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }
                searchConRecibos(_id);

                searchLiquidaciones();

                searchBitacora();
            }

        }

        private Entity initRecibosSearch()
        {
            Entity recibos = new Entity("recibo");
            recibos.useAuditable = false;
            recibos.Attributes.Add(new Models.Attribute("Id"));
            recibos.Attributes.Add(new Models.Attribute("Cobertura"));
            recibos.Attributes.Add(new Models.Attribute("Vencimiento"));
            recibos.Attributes.Add(new Models.Attribute("Concepto"));
            recibos.Attributes.Add(new Models.Attribute("Total"));
            recibos.Attributes.Add(new Models.Attribute("Numero"));
            recibos.Attributes.Add(new Models.Attribute("Estatus"));
            recibos.Attributes.Add(new Models.Attribute("Tipo"));
            recibos.Attributes.Add(new Models.Attribute("ComisionPagada"));
            recibos.Attributes.Add(new Models.Attribute("Comision"));

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Folio"));
            tramite.Attributes.Add(new Models.Attribute("id"));

            selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
            JoinEntity joinEnt = new JoinEntity();
            joinEnt.ChildEntity = tramite;
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.selectJoinList.Add(selJoin);

            recibos.ChildEntities.Add(joinEnt);

            return recibos;
        }

        private void searchConRecibos(string id)
        {
            Entity recibos = initRecibosSearch();

            recibos.Keys.Add(new Key("Conciliacion", id, "int"));

            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(recibos, out errMsg);
            updateTotalValues(results);
            gvRecibos.DataSource = SiteFunctions.trnsformEntityToDT(results);
            gvRecibos.DataBind();

        }

        private void updateTotalValues(List<Entity> _recibos)
        {
            float totalRecibo = 0;
            float totalNC = 0;
            float totalValores = 0;
            float totalComRecibo = 0;
            float totalComNC = 0;
            float totalComisionPagada = 0;

            foreach (Entity Recibo in _recibos)
            {
                string tipo = Recibo.getAttrValueByName("tipo");
                string totalStr = Recibo.getAttrValueByName("Total");
                string comision = Recibo.getAttrValueByName("Comision");
                string comisionPagadaStr = Recibo.getAttrValueByName("ComisionPagada");
                float totalAmt = 0, comisionAmt = 0, comisionPagada=0;
                float.TryParse(totalStr, out totalAmt);
                float.TryParse(comision, out comisionAmt);
                float.TryParse(comisionPagadaStr, out comisionPagada);
                if (tipo == "0")
                {
                    totalRecibo += totalAmt;
                    totalComRecibo += comisionAmt;
                }
                else
                {
                    totalNC += totalAmt;
                    totalComNC += comisionAmt;
                }
                totalComisionPagada += comisionPagada;
            }

            txtTotalCRecibos.Text = totalRecibo.ToString();
            txtTotalNotas.Text = totalNC.ToString();
            
            txtTotalComision.Text = (totalComRecibo - totalComNC).ToString();
            txtTotalComisionPagada.Text = totalComisionPagada.ToString();
        }

        public void searchLiquidaciones()
        {
            string errMsg = string.Empty;
            Entity liquidacion = new Entity("liquidacion");
            liquidacion.Attributes.Add(new Models.Attribute("Id"));
            liquidacion.Attributes.Add(new Models.Attribute("Folio"));
            liquidacion.Keys.Add(new Key("Estatus", "1", "int"));
            liquidacion.Keys.Add(new Key("Aseguradora", txtAseguradora.SelectedValue, "int"));
            liquidacion.useAuditable = false;
            List<Entity> results = SiteFunctions.GetValues(liquidacion, out errMsg);
            txtLiquidacion.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtLiquidacion.DataValueField = "Id";
            txtLiquidacion.DataTextField = "Folio";
            txtLiquidacion.DataBind();
        }

        private void initCatalogos()
        {
            string errMsg = string.Empty;

            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Aseguradora.useAuditable = false;
            JoinEntity joinCust = new JoinEntity();
            joinCust.ChildEntity = new Entity("Cliente");
            joinCust.JoinKey = new Models.Attribute("Id");
            joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
            selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
            joinCust.selectJoinList.Add(custsj);
            joinCust.JoinType = JoinType.Inner;
            Aseguradora.ChildEntities.Add(joinCust);
            List<Entity> results = SiteFunctions.GetValues(Aseguradora, out errMsg);
            txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtAseguradora.DataValueField = "Id";
            txtAseguradora.DataTextField = "ClienteNombreCompleto";
            txtAseguradora.DataBind();

            Entity moneda = new Entity("Moneda");
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            moneda.useAuditable = false;
            results = SiteFunctions.GetValues(moneda, out errMsg);
            txtMoneda.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtMoneda.DataValueField = "Id";
            txtMoneda.DataTextField = "Nombre";
            txtMoneda.DataBind();
            try
            {
                txtMoneda.SelectedValue = "2187";
            }
            catch (Exception)
            {
            }

            Entity UserSys = new Entity("Usuario");
            UserSys.Attributes.Add(new Models.Attribute("Id"));
            UserSys.Attributes.Add(new Models.Attribute("Nombre"));
            UserSys.useAuditable = false;
            results = SiteFunctions.GetValues(UserSys, out errMsg);
            txtConcilio.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtConcilio.DataValueField = "Id";
            txtConcilio.DataTextField = "Nombre";
            txtConcilio.DataBind();
            txtConcilio.Items.Add("");
            txtConcilio.SelectedValue = "";
            txtConcilio.Enabled = false;
            txtConcilio.SelectedValue = SiteFunctions.getuserid(Context.User.Identity.Name, false);
        }

        private void saveConciliacion()
        {
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;

            Entity Conciliacion = new Entity("Conciliacion");
            Conciliacion.Attributes.Add(new Models.Attribute("Permiso", "2", "int64"));
            Conciliacion.Attributes.Add(new Models.Attribute("Propietario", userid.ToString(), "int64"));
            int ids = 0;
            int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
            Conciliacion.Attributes.Add(new Models.Attribute("Ubicacion", ids.ToString(), "int64"));
            ids = 0;
            int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
            Conciliacion.Attributes.Add(new Models.Attribute("CentroDeBeneficio", ids.ToString(), "int64"));
            Conciliacion.useAuditable = true;
            Conciliacion.auditable = audi;

            Conciliacion.Attributes.Add(new Models.Attribute("TotalComision", txtTotalComision.Text != string.Empty ? txtTotalComision.Text: "0", "float"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionPagada", txtTotalComisionPagada.Text != string.Empty ? txtTotalComisionPagada.Text : "0", "float"));
            if (txtObservaciones.Text != string.Empty)
            {
                Conciliacion.Attributes.Add(new Models.Attribute("Observaciones", txtObservaciones.Text, "string"));
            }
            Conciliacion.Attributes.Add(new Models.Attribute("NumeroFactura", txtFactura.Text, "string"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaAplicacionCia", txtAplicacionCia.Text != string.Empty ? txtAplicacionCia.Text : "01/01/1900", "datetime"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaAbono", txtFechaAbono.Text != string.Empty ? txtFechaAbono.Text : "01/01/1900", "datetime"));
            Conciliacion.Attributes.Add(new Models.Attribute("FechaConciliacion", txtFechaCon.Text != string.Empty ? txtFechaCon.Text : "01/01/1900", "datetime"));
            if (txtAseguradora.SelectedValue != string.Empty)
            {
                Conciliacion.Attributes.Add(new Models.Attribute("Aseguradora", txtAseguradora.SelectedValue, "int"));
            }
            if (txtMoneda.SelectedValue != string.Empty)
            {
                Conciliacion.Attributes.Add(new Models.Attribute("Moneda", txtMoneda.SelectedValue, "int"));
            }
            if (txtConcilio.SelectedValue != string.Empty)
            {
                Conciliacion.Attributes.Add(new Models.Attribute("UsuarioConcilio", SiteFunctions.getuserid(Context.User.Identity.Name, false), "int"));
            }
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionNotas", txtTotalNotas.Text != string.Empty ? txtTotalNotas.Text : "0", "float"));
            Conciliacion.Attributes.Add(new Models.Attribute("TotalComisionConciliacion", txtTotalCRecibos.Text != string.Empty ? txtTotalCRecibos.Text : "0", "float"));
            string errmsg = string.Empty;
            object Id = ConId.Value == string.Empty ? null : ConId.Value;
            RestSharp.Method method = RestSharp.Method.POST;

            if (Id != null)
            {
                Conciliacion.Keys.Add(new Key("Id", Id.ToString(), "int"));
                method = RestSharp.Method.PUT;
            }
            else
            {
                Conciliacion.Attributes.Add(new Models.Attribute("Estatus", "0", "int"));
            }
            if (SiteFunctions.SaveEntity(Conciliacion, method, out errmsg))
            {
                string newId = Id != null ? Id.ToString() : errmsg;
                //Response.Redirect("Conciliacion?id=" + newId);
                ConId.Value = newId;
                showMessage("Conciliación guardada correctamente", false);
                RecibosDiv.Visible = true;
            }
            else
            {
                showMessage(errmsg, true);
            }
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            saveConciliacion();
        }

        protected void btnRefreshRecibos_Click(object sender, EventArgs e)
        {
            object Id = ConId.Value == string.Empty ? null : ConId.Value;
            if (Id != null)
            {
                searchConRecibos(Id.ToString());
            }
        }

        protected void BtnImportRecibos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            object id = ConId.Value;
            if (fileUploadRecibos.HasFile)
            {
                string fileName = fileUploadRecibos.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        float totalPrima = 0;
                        List<string> listErrors;
                        if (!SiteFunctions.importRecibosConciliacion(fileUploadRecibos.FileContent, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out listErrors))
                        {
                            string errMsg = SiteFunctions.formatListErrors(listErrors);
                            showMessage(errMsg, true);
                        }

                        searchConRecibos(id.ToString());
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .xlsx o .xls", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
        }

        protected void btnAplicar_Click(object sender, EventArgs e)
        {
            bool aplicar = true;
            Entity recibos = new Entity("recibo");//initRecibosSearch();
            recibos.Attributes.Add(new Models.Attribute("Id"));
            string Id = ConId.Value == string.Empty ? null : ConId.Value;
            recibos.Keys.Add(new Key("conciliacion", Id, "int"));

            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(recibos, out errMsg);
            if (results.Count > 0)
            {
                foreach (Entity recibo in results)
                {
                    string Idrec = recibo.getAttrValueByName("Id").ToString();
                    recibo.Attributes.Clear();

                    recibo.Attributes.Add(new Models.Attribute("Estatus", "5", "int"));
                    recibo.Keys.Add(new Key("Id", Idrec, "int"));
                    SiteFunctions.SaveEntity(recibo, RestSharp.Method.PUT, out errMsg);
                }

                if (aplicar)
                {
                    Entity conciliacion = new Entity("conciliacion");
                    conciliacion.Attributes.Add(new Models.Attribute("id", Id, "int"));
                    conciliacion.Attributes.Add(new Models.Attribute("estatus", "1", "int"));
                    conciliacion.Keys.Add(new Key("id", Id, "int"));
                    SiteFunctions.SaveEntity(conciliacion, RestSharp.Method.PUT, out errMsg);
                }
                searchConRecibos(Id.ToString());
                showMessage("Conciliación Aplicada!!!", false);
                btnAplicar.Enabled = false;
            }
            else
            {
                showMessage("No hay recibos ligados a la liquidación", true);
            }
        }

        protected void txtAseguradora_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchLiquidaciones();
        }

        protected void btnBitacora_Click(object sender, EventArgs e)
        {
            searchBitacora();
        }

        private void searchBitacora()
        {
            string errMsg = string.Empty;
            Entity entity = new Entity("BitacoraConciliacion");
            entity.Attributes.Add(new Models.Attribute("Id"));
            entity.Attributes.Add(new Models.Attribute("Fecha"));
            entity.Attributes.Add(new Models.Attribute("Tramite"));
            entity.Attributes.Add(new Models.Attribute("Usuario"));
            entity.Keys.Add(new Key("Tramite", ConId.Value, "int"));

            Entity Usuario = new Entity("Usuario");
            Usuario.Attributes.Add(new Models.Attribute("Nombre"));

            selectJoinEntity selJoinEnt = new selectJoinEntity("Usuario", 1, "Id");

            JoinEntity joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEnt);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = Usuario;
            entity.ChildEntities.Add(joinEntity);
            
            List<Entity> polResult = SiteFunctions.GetValues(entity, out errMsg);
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(polResult);
        }
    }
}