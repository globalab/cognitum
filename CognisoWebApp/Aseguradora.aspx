﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Aseguradora.aspx.cs" Inherits="CognisoWebApp.Aseguradora" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Aseguradora</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script lang="javascript" type="text/javascript">
    var submit = 0;
    function CheckIsRepeat() {
        if (++submit > 1) {
           return false;
        }
    }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
     
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Aseguradora</asp:Label>
            </div>
           
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" />
                    <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Ejecutivos-tab" data-toggle="tab" href="#Ejecutivos" role="tab" aria-controls="Ejecutivos" aria-selected="false">Ejecutivos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Direcciones" role="tab" aria-controls="Direcciones" aria-selected="false">Direcciones</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Tramites-tab" data-toggle="tab" href="#Tramites" role="tab" aria-controls="Tramites" aria-selected="false">Tramites</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Documentos-tab" data-toggle="tab" href="#Documentos" role="tab" aria-controls="Documentos" aria-selected="false">Documentos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Seguridad-tab" data-toggle="tab" href="#Seguridad" role="tab" aria-controls="Seguridad" aria-selected="false">Seguridad</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblGrupo" runat="server" Text="Grupo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtGroup" runat="server" class="form-control" style="width:90%">
                                            </asp:DropDownList>
                                            <div class="input-group-append">
                                                <!--  <input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Grupo')"/> -->
                                                &nbsp;<a id="popupGrupo" href='#' data-image='Grupo.aspx?popup=1'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblGiro" runat="server" Text="Giro"></asp:Label>
                                        <div class="form-group input-group">
                                             <asp:DropDownList ID="txtGiro" runat="server" class="form-control" style="width:90%"></asp:DropDownList>
                                            <div class="input-group-append">
                                                <!--<input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Giro')"/> -->
                                                &nbsp;<a id="popupGiro" href='#' data-image='Giro.aspx?popup=1' ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblRazonSocial" runat="server" Text="Razón Social"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtRazonSocial" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblRFC" runat="server" Text="RFC"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtRFC" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblClave" runat="server" Text="Clave"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtClave" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatus" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDeshabilitaCalculos" runat="server" Text="Deshabilita Cálculos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="txtDeshabilitaCalculos" runat="server" class="form-control"></asp:CheckBox>
                                        </div>
                                    </div>
                                </div>
                                <%--<div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblReqCaratula" runat="server" Text="Req. Carátula"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="txtReqCaratula" runat="server" class="form-control"></asp:CheckBox>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblReqIncDer" runat="server" Text="Rec Incluye Der."></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="txtReqIncDer" runat="server" class="form-control"></asp:CheckBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblConInclReq" runat="server" Text="Com. Incluye Req."></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="txtConInclReq" runat="server" class="form-control"></asp:CheckBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblIdentificadorSAP" runat="server" Text="Identificador SAP"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtIdentificadorSAP" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           </div>     
                        <div class="tab-pane fade" id="Ejecutivos" role="tabpanel" aria-labelledby="Ejecutivos-tab">
                            <div class="container">
                                <div class="row">
                                    
                                    <a data-image='Contacto.aspx?popup=1&custid=<%=AseguradoraId.Value%>' onclick="openpopup('Contacto.aspx?popup=1&custid=<%#AseguradoraId.Value%>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVEjecutivoList" runat="server" OnPageIndexChanging="GVEjecutivoList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a href='#' data-image='Contacto.aspx?popup=1&id=<%# Eval("Id") %>' onclick="openpopup('Contacto.aspx?popup=1&id=<%# Eval("Id") %>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="Puesto" HeaderText="Puesto" />
                                    <asp:BoundField DataField="Id" Visible="false" />
                                </Columns>
                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>
                                    </div>
                                </div>
                            
                         </div>
                           </div>     
                        <div class="tab-pane fade" id="Direcciones" role="tabpanel" aria-labelledby="Direcciones-tab">
                             <div class="container">
                                <div class="row">
                                    <a class="popup" href='#' data-image='Direccion.aspx?popup=1&custid=<%=AseguradoraId.Value %>' onclick="openpopup('Direccion.aspx?popup=1&custid=<%=AseguradoraId.Value %>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a> 
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVDirList" runat="server" OnPageIndexChanging="GVDirList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a class="popup" href='#' data-image='Direccion.aspx?popup=1&id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="Calle" HeaderText="Calle" />
                                    <asp:BoundField DataField="NumInterior" HeaderText="Número (Int)" />
                                    <asp:BoundField DataField="NumExterior" HeaderText="Número (Ext)" />
                                    <asp:BoundField DataField="Id" Visible="false" />

                            
                                </Columns>
                                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>
                                    </div>
                                </div>
                            
                            </div>
                           </div>     
                        <div class="tab-pane fade" id="Tramites" role="tabpanel" aria-labelledby="Tramites-tab">
                            <div class="container">
                                <div class="row">
                                    <a class="popup" href='#' data-image='Tramites.aspx?popup=1&AseguradoraId=<%=AseguradoraId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a> 
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVTramites" runat="server" OnPageIndexChanging="GVTramites_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a class="popup" href='#' data-image='Tramites.aspx?popup=1&AseguradoraId=<%# Eval("Aseguradora") %>&id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="DuracionDias" HeaderText="Duración" />
                                    <asp:BoundField DataField="Aseguradora" HeaderText="Aseguradora" />
                                    

                            
                                </Columns>
                                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>
                                    </div>
                                </div>
                            
                            </div>
                           </div>     
                        <div class="tab-pane fade" id="Documentos" role="tabpanel" aria-labelledby="Documentos-tab">
                            <div class="container">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="Label4" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="Textbox4" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           </div>     
                        <div class="tab-pane fade" id="Seguridad" role="tabpanel" aria-labelledby="Seguridad-tab">
                            <div class="container">

                               <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicación" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficios" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                           </div>
                           </div>     
                      
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            
                          </div>
                      </div> 
                   
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="AseguradoraId" runat="server" />

            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>  

