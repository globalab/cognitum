﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inciso.aspx.cs" Inherits="CognisoWebApp.Inciso" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Inciso</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <link href="/Content/cogniso.css" rel="stylesheet" />
    <link href="//Content/jquery-ui.css" rel="stylesheet" />
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtMarca.ClientID%>").select2({
                placeholder: "Seleccione una Marca",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });
        function openpopup(url,title,btnname) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: title,
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $(btnname).click();
                }
            });
            $dialog.dialog('open');
        }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
        <div class="container">
        <div class="container" runat="server">
                 
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ToolTip="Guardar" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                </div>
            </div>

            <div class="container" runat="server" id="IncisoGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="DatosAuto-tab" data-toggle="tab" href="#DatosAuto" role="tab" aria-controls="DatosAuto" aria-selected="false">Datos del Auto</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Contactos-tab" data-toggle="tab" href="#Contactos" role="tab" aria-controls="Contactos" aria-selected="false">Contactos</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="DatosAuto" role="tabpanel" aria-labelledby="DatosAuto-tab">
                            <div class="container">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblInciso" runat="server" Text="Inciso"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtInciso" runat="server" class="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valInciso" runat="server" ErrorMessage="Inciso" ForeColor="Red" ControlToValidate="txtInciso" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblIdentificador" runat="server" Text="Identificador"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtIdentificador" runat="server" class="form-control" TabIndex="2"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valIdentificador" runat="server" ErrorMessage="Identificador" ForeColor="Red" ControlToValidate="txtIdentificador" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSerie" runat="server" Text="Serie"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSerie" runat="server" class="form-control" TabIndex="3"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valSerie" runat="server" ErrorMessage="Serie" ForeColor="Red" ControlToValidate="txtSerie" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblMotor" runat="server" Text="Motor"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtMotor" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valMotor" runat="server" ErrorMessage="Motor" ForeColor="Red" ControlToValidate="txtMotor" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPlacas" runat="server" Text="Placas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPlacas" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valPlacas" runat="server" ErrorMessage="Placas" ForeColor="Red" ControlToValidate="txtPlacas" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblConductorHabitual" runat="server" Text="Conductor Habitual"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtConductorHabitual" runat="server" CssClass="form-control" TabIndex="6">
                                            </asp:TextBox>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblMarca" runat="server" Text="Marca"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMarca" runat="server" CssClass="form-control" TabIndex="7">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valMarca" runat="server" ErrorMessage="Marca" ForeColor="Red" ControlToValidate="txtMarca" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblModelo" runat="server" Text="Modelo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtModelo" runat="server" CssClass="form-control" TabIndex="8">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valModelo" runat="server" ErrorMessage="Modelo" ForeColor="Red" ControlToValidate="txtModelo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblVersion" runat="server" Text="Versión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVersion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valVersion" runat="server" ErrorMessage="Versión" ForeColor="Red" ControlToValidate="txtVersion" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPrima" runat="server" Text="Prima"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrima" runat="server" CssClass="form-control" TabIndex="10">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valPrima" runat="server" ErrorMessage="Prima" ForeColor="Red" ControlToValidate="txtPrima" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBeneficiarioPreferente" runat="server" Text="BeneficiarioPreferente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtBeneficiarioPreferente" runat="server" CssClass="form-control" TabIndex="11">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPoliza" runat="server" Text="Póliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPoliza" runat="server" CssClass="form-control" TabIndex="12" disabled>
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEndosoAlta" runat="server" Text="Endoso Alta"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtEndosoAlta" runat="server" CssClass="form-control" TabIndex="13">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEndosoBaja" runat="server" Text="Endoso Baja"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtEndosoBaja" runat="server" CssClass="form-control" TabIndex="14">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatus" runat="server" CssClass="form-control" TabIndex="15">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCobertura" runat="server" Text="Cobertura"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCobertura" runat="server" CssClass="form-control" TabIndex="16">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEmpleado" runat="server" Text="Empleado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEmpleado" runat="server" CssClass="form-control" TabIndex="17">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTarjetaBancaria" runat="server" Text="Tarjeta Bancaria"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtTarjetabancaria" runat="server" CssClass="form-control" TabIndex="18">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Contactos" role="tabpanel" aria-labelledby="Contactos-tab">
                            <div class="container">
                                <div class="panel-body" runat="server">
                                    <div class="table-responsive" runat="server">
                                        <asp:GridView ID="GVContactos" runat="server" OnPageIndexChanging="GVContactos_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col">
                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="IncisoId" runat="server" />
                <asp:HiddenField ID="PolId" runat="server" />
                <asp:HiddenField ID="EndosoId" runat="server" />
                
            </div>
        </div>
    </div>
 </form>
</body>
</html>  
