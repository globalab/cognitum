﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp
{
    public class EndosoToXML
    {
        /// <summary>
        /// Permite tomar una instancia de PolizaGeneral para extraer la lista de Endosos asociados
        /// y transformarlos en formato XML.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaGeneral de donde se obtendran los Endosos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de los Endosos de la PolizaGeneral en formato XML.</returns>
        public static XElement serializaEndosos(Entity poliza)
        {
            string errMsg = string.Empty;
            Entity endososSearch = new Entity("Endoso");
            endososSearch.Attributes.Add(new Models.Attribute("Id"));
            endososSearch.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            endososSearch.Attributes.Add(new Models.Attribute("EstatusEndoso"));
            endososSearch.Attributes.Add(new Models.Attribute("InstruccionesAseguradora"));
            endososSearch.Attributes.Add(new Models.Attribute("Prima"));
            endososSearch.Attributes.Add(new Models.Attribute("Tipo"));
            endososSearch.Attributes.Add(new Models.Attribute("Poliza"));
            endososSearch.Attributes.Add(new Models.Attribute("Impuesto"));
            endososSearch.Attributes.Add(new Models.Attribute("TipoMovimiento"));
            endososSearch.Attributes.Add(new Models.Attribute("EndosoOriginal"));
            endososSearch.Attributes.Add(new Models.Attribute("EndosoNuevo"));
            endososSearch.Keys.Add(new Key("Poliza", poliza.getAttrValueByName("Id"), "int"));

            List<Entity> results = SiteFunctions.GetValues(endososSearch, out errMsg);
            XElement endosoX = new XElement("Endosos");

            foreach(Entity endoso in results)
            {
                endosoX.Add(serializaEndoso(endoso));
            }

            return endosoX;
        }

        /// <summary>
        /// Permite tomar una instancia de Endoso para transformarla en formato XML.
        /// </summary>
        /// <param name="endoso">Instancia de Endoso de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del Endoso en formato XML.</returns>
        public static XElement serializaEndoso(Entity endoso)
        {
            string errmsg = string.Empty;
            Entity Tramite = new Entity("Tramite");
            #region campos
            Tramite.Attributes.Add(new Models.Attribute("Id"));
            Tramite.Attributes.Add(new Models.Attribute("Permiso"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEmision"));
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            Tramite.Attributes.Add(new Models.Attribute("FormaPago"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaInicio"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaFin"));
            Tramite.Attributes.Add(new Models.Attribute("Total"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionPct"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Derechos"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosImporte"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosPct"));
            Tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Neto"));
            Tramite.Attributes.Add(new Models.Attribute("Prima"));
            Tramite.Attributes.Add(new Models.Attribute("Gastos"));
            Tramite.Attributes.Add(new Models.Attribute("Descripcion"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEnvio"));
            Tramite.Attributes.Add(new Models.Attribute("EstatusMesaDeControl"));
            Tramite.Attributes.Add(new Models.Attribute("EnviadoAMesaControl"));
            Tramite.Attributes.Add(new Models.Attribute("Ubicacion"));
            Tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            Tramite.Attributes.Add(new Models.Attribute("Propietario"));
            Tramite.Attributes.Add(new Models.Attribute("LoteProduccion"));
            #endregion
            Tramite.Keys.Add(new Key("Id", endoso.getAttrValueByName("Id"), "int"));

            Tramite = SiteFunctions.GetEntity(Tramite, out errmsg);

            /*if (endoso.MovimientosAlta == null)
                endoso.MovimientosAlta = new List<BienAsegurado>();
            if (endoso.MovimientosBaja == null)
                endoso.MovimientosBaja = new List<BienAsegurado>();*/

            Entity entitySearch = new Entity("CentroDeBeneficio");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", Tramite.getAttrValueByName("CentroDeBeneficio"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);
            
            string CentroBeneficio = entitySearch.getAttrValueByName("Nombre");

            entitySearch = new Entity("Ubicacion");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", Tramite.getAttrValueByName("Ubicacion"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string Ubicacion = entitySearch.getAttrValueByName("Nombre");

            entitySearch = new Entity("Usuario");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", Tramite.getAttrValueByName("Propietario"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string propietario = entitySearch.getAttrValueByName("Nombre");

            entitySearch = new Entity("Usuario");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", endoso.getAttrValueByName("Audit_UsuarioAdd"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string UsuarioAdd = entitySearch.getAttrValueByName("Nombre");

            entitySearch = new Entity("Usuario");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", endoso.getAttrValueByName("Audit_UsuarioUMod"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string UsuarioUMod = entitySearch.getAttrValueByName("Nombre");

            Entity poliza = new Entity("Poliza");
            poliza.Attributes.Add(new Models.Attribute("Aseguradora"));
            poliza.Attributes.Add(new Models.Attribute("Contratante"));
            poliza.Keys.Add(new Key("Id", endoso.getAttrValueByName("Poliza"), "int"));
            poliza = SiteFunctions.GetEntity(poliza, out errmsg);
            string contratante = string.Empty;
            string aseguradora = string.Empty;

            if(poliza != null)
            {
                entitySearch = new Entity("PersonaMoral");
                entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("RazonSocial"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Contratante"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

                contratante = entitySearch.getAttrValueByName("RazonSocial");

                entitySearch = new Entity("Cliente");
                entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Aseguradora"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

                aseguradora = entitySearch.getAttrValueByName("NombreCompleto");
            }

            entitySearch = new Entity("TipoEndoso");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            entitySearch.Keys.Add(new Key("Id", endoso.getAttrValueByName("Tipo"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string TipoEndoso = entitySearch.getAttrValueByName("Nombre");

            entitySearch = new Entity("TipoMovimiento");
            entitySearch.Attributes.Add(new CognisoWebApp.Models.Attribute("Descripcion"));
            entitySearch.Keys.Add(new Key("Id", endoso.getAttrValueByName("TipoMovimiento"), "int"));

            entitySearch = SiteFunctions.GetEntity(entitySearch, out errmsg);

            string TipoMovimiento = entitySearch.getAttrValueByName("Descripcion");

            var xml =
                new XElement("Endoso",
                         new XElement("Descripcion", Tramite.getAttrValueByName("Descripcion")),
                         new XElement("CentroDeBeneficio", CentroBeneficio),
                         new XElement("ComisionImporte", Tramite.getAttrValueByName("ComisionImporte", true)),
                         new XElement("ComisionPct", Tramite.getAttrValueByName("ComisionPct")),
                         //endoso.Descripcion != null ? new XElement("Descripcion", endoso.Descripcion) : new XElement("Descripcion"),
                         new XElement("Derechos", Tramite.getAttrValueByName("Derechos", true)),
                         new XElement("EstatusEndoso", endoso.getAttrValueByName("EstatusEndoso")),
                         new XElement("Folio", Tramite.getAttrValueByName("Folio")),
                         new XElement("FechaAdd", endoso.getAttrValueByName("AuditableFechaAdd", true)),
                         new XElement("FechaEmision",
                                      Tramite.getAttrValueByName("FechaEmision", true)),
                         new XElement("FechaEnvio",
                                      Tramite.getAttrValueByName("FechaEnvio", true)),
                         new XElement("FechaUMod", endoso.getAttrValueByName("AuditableFechaUMod", true)),
                         new XElement("Gastos", Tramite.getAttrValueByName("Gastos", true)),
                         new XElement("ImpuestoImporte", Tramite.getAttrValueByName("ImpuestoImporte")),
                         new XElement("InstruccionesAseguradora",
                                      endoso.getAttrValueByName("InstruccionesAseguradora")),
                         new XElement("Neto", Tramite.getAttrValueByName("Neto", true)),
                         new XElement("Prima", endoso.getAttrValueByName("Prima", true)),
                         new XElement("Propietario", propietario),
                         new XElement("RecargosImporte", Tramite.getAttrValueByName("RecargosImporte", true)),
                         new XElement("RecargosPct", Tramite.getAttrValueByName("RecargosPct")),
                         new XElement("Tipo", TipoEndoso),
                         new XElement("TipoMovimiento", TipoMovimiento),
                         new XElement("Total", Tramite.getAttrValueByName("Total", true)),
                         new XElement("Ubicacion", Ubicacion),
                         new XElement("UsuarioAdd", UsuarioAdd),
                         new XElement("UsuarioUMod", UsuarioUMod),
                         new XElement("VigenciaInicio", Tramite.getAttrValueByName("VigenciaInicio", true)),
                         new XElement("VigenciaFin", Tramite.getAttrValueByName("VigenciaFin", true)),
                         new XElement("Aseguradora", aseguradora),
                         new XElement("Contratante", contratante),
                        serializaMovimientosAlta(endoso.getAttrValueByName("Id")),
                        serializaMovimientosBaja(endoso.getAttrValueByName("Id"))
                );


            /*if (typeof(EndosoPool).IsAssignableFrom(endoso.GetType()))
            {
                var end = endoso as EndosoPool;
                serializaEndosoPool(end, xml);
            }*/
            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de EndosoPool para transformarla en formato XML.
        /// </summary>
        /// <param name="endoso">Instancia de EndosoPool de la cual se tomaran los datos.</param>
        /// <param name="xml"></param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del EndosoPool en formato XML.</returns>
        /*private static void serializaEndosoPool(EndosoPool endoso, XElement xml)
        {

        }*/

        /// <summary>
        /// Permite tomar una instancia de Endoso para transformar los MovientosAlta que son instancia de BienAsegurado en formato XML.
        /// </summary>
        /// <param name="endoso">Instancia de Endoso de la cual se tomaran los MovimientosAlta(BienAsegurado).</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de los MovimientosAlta del Endoso en formato XML.</returns>
        public static XElement serializaMovimientosAlta(string endosoId)
        {
            string errMsg = string.Empty;
            XElement movAltaX = new XElement("MovimientosAlta");

            Entity searchAlta = new Entity("BienAsegurado");
            searchAlta.Attributes.Add(new Models.Attribute("Id"));
            searchAlta.Attributes.Add(new Models.Attribute("Permiso"));
            searchAlta.Attributes.Add(new Models.Attribute("Inciso"));
            searchAlta.Attributes.Add(new Models.Attribute("Estatus"));
            searchAlta.Attributes.Add(new Models.Attribute("Prima"));
            searchAlta.Attributes.Add(new Models.Attribute("Identificador"));
            searchAlta.Attributes.Add(new Models.Attribute("Ubicacion"));
            searchAlta.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            searchAlta.Attributes.Add(new Models.Attribute("Propietario"));
            searchAlta.Attributes.Add(new Models.Attribute("Poliza"));
            searchAlta.Attributes.Add(new Models.Attribute("EndosoAlta"));
            searchAlta.Attributes.Add(new Models.Attribute("EndosoBaja"));
            searchAlta.Attributes.Add(new Models.Attribute("GrupoAsegurados"));
            searchAlta.Attributes.Add(new Models.Attribute("BeneficiarioPreferente"));
            searchAlta.Attributes.Add(new Models.Attribute("Empleado"));
            searchAlta.Attributes.Add(new Models.Attribute("TarjetaBancaria"));
            searchAlta.Keys.Add(new Key("EndosoAlta", endosoId, "int"));

            List<Entity> results = SiteFunctions.GetValues(searchAlta, out errMsg);

            foreach (Entity movAlta in results)
            {
                string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

                PolizaToXML.getFieldRelatedToMov(movAlta.getAttrValueByName("CentroDeBeneficio"), movAlta.getAttrValueByName("Propietario")
                    ,movAlta.getAttrValueByName("Ubicacion"), movAlta.getAttrValueByName("AuditableUsuarioAdd"), movAlta.getAttrValueByName("AuditableUsuarioUMod")
                    , movAlta.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);
                movAltaX.Add(PolizaToXML.serializaMovimiento(movAlta, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario));
            }

            return movAltaX;
        }

        /// <summary>
        /// Permite tomar una instancia de Endoso para transformar los MovientosBaja que son instancia de BienAsegurado en formato XML.
        /// </summary>
        /// <param name="endoso">Instancia de Endoso de la cual se tomaran los MovimientosBaja(BienAsegurado).</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de los MovimientosBaja del Endoso en formato XML.</returns>
        public static XElement serializaMovimientosBaja(string endosoId)
        {
            string errMsg = string.Empty;
            XElement movAltaX = new XElement("MovimientosBaja");

            Entity searchAlta = new Entity("BienAsegurado");
            searchAlta.Attributes.Add(new Models.Attribute("Id"));
            searchAlta.Attributes.Add(new Models.Attribute("Permiso"));
            searchAlta.Attributes.Add(new Models.Attribute("Inciso"));
            searchAlta.Attributes.Add(new Models.Attribute("Estatus"));
            searchAlta.Attributes.Add(new Models.Attribute("Prima"));
            searchAlta.Attributes.Add(new Models.Attribute("Identificador"));
            searchAlta.Attributes.Add(new Models.Attribute("Ubicacion"));
            searchAlta.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            searchAlta.Attributes.Add(new Models.Attribute("Propietario"));
            searchAlta.Attributes.Add(new Models.Attribute("Poliza"));
            searchAlta.Attributes.Add(new Models.Attribute("EndosoAlta"));
            searchAlta.Attributes.Add(new Models.Attribute("EndosoBaja"));
            searchAlta.Attributes.Add(new Models.Attribute("GrupoAsegurados"));
            searchAlta.Attributes.Add(new Models.Attribute("BeneficiarioPreferente"));
            searchAlta.Attributes.Add(new Models.Attribute("Empleado"));
            searchAlta.Attributes.Add(new Models.Attribute("TarjetaBancaria"));
            searchAlta.Keys.Add(new Key("EndosoBaja", endosoId, "int"));

            List<Entity> results = SiteFunctions.GetValues(searchAlta, out errMsg);

            foreach (Entity movAlta in results)
            {
                string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

                PolizaToXML.getFieldRelatedToMov(movAlta.getAttrValueByName("CentroDeBeneficio"), movAlta.getAttrValueByName("Propietario")
                    , movAlta.getAttrValueByName("Ubicacion"), movAlta.getAttrValueByName("AuditableUsuarioAdd"), movAlta.getAttrValueByName("AuditableUsuarioUMod")
                    , movAlta.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);
                movAltaX.Add(PolizaToXML.serializaMovimiento(movAlta, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario));
            }

            return movAltaX;
        }
    }
}