﻿<%@ Page Title="Prospecto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Prospecto.aspx.cs" Inherits="CognisoWebApp.Prospecto" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
   
   <script lang="javascript" type="text/javascript">
       var submit = 0;
       function CheckIsRepeat() {
           if (++submit > 1) {
               return false;
           }
       }
    </script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(document).ready(function () {

            $("#<%=txtGroup.ClientID%>").select2({


                placeholder: "Seleccione un Grupo",
                minimumResultsForSearch: 1,
                allowClear: true


            });

        });

        $(document).ready(function () {

            $("#<%=txtGiro.ClientID%>").select2({
            placeholder: "Seleccione un Giro",
            minimumResultsForSearch: 1,
            allowClear: true
        });

    });

    $('body').on('click', '#popup', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var page = $(this).attr("data-image")  //get url of link

        var $dialog = $('<div></div>')
        .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
        .dialog({
            autoOpen: false,
            modal: true,
            center: true,
            height: 610,
            width: 800,
            title: "Giro",
            buttons: {
                "Cerrar": function () { $dialog.dialog('close'); }
            },
            close: function (event, ui) {

                $("#<%=reloadGiro.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });

            $('body').on('click', '#popupGrupo', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link

                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: 610,
                    width: 800,
                    title: "Grupo",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=reloadGrupo.ClientID%>").click();
                    }
                });
                $dialog.dialog('open');

            });


</script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            $('#myTab a[href="#' + paneName + '"]').tab('show')


        }
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Requiere eliminar el Prospecto?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
        function ShowModal() {
            try {
                $('#exampleModal').modal('show');

            } catch (e) {
                console.error(e.message);
                alert(e.message);

            }

        }


</script>

  
        <div class="containerRT1" runat="server">    
                <div class="panel-heading">    
                    <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Prospecto</asp:Label></h2>
                </div> 
                <hr class="float-left">   
                <nav aria-label="breadcrumb" class="clearfix">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="ProspectosList.aspx">Lista de Prospectos</a></li>
                        <li class="breadcrumb-item txt-white active">Prospecto <%=LeadId.Value %> </li>
                      </ol>
                    </nav>
              
           
                <div class="panel-heading">
                    <div class="btn-group">
                        <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" data-target="#pnlModal" data-toggle="modal" runat="server" OnClick="BtnSave_Click" CausesValidation="true" OnClientClick="return CheckIsRepeat();" />
                        <asp:ImageButton ID="btnEnviarValidacionCobranza"  ToolTip="Enviar a validación con cobranza" data-target="#pnlModal" data-toggle="modal"  OnClick="btnEnviarValidacionCobranza_Click" ImageUrl="/Content/Imgs/send.png" runat="server"  />
                        <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick = "Confirm()" OnClick="btnDelete_Click" ImageUrl="/Content/Imgs/delete.png" runat="server" CausesValidation="false" />
                        <asp:ImageButton ID="btnConvLead" ImageUrl="/Content/Imgs/Persona-aprobar.png" ToolTip="Aprobar Prospecto" runat="server" Height="35px" Width="34px" OnClick="btnConvLead_Click"  />
                         <asp:ImageButton ID="btnRechazar" ImageUrl="/Content/Imgs/Persona-rechazar.png" ToolTip="Rechazar Prospecto" runat="server" data-toggle="modal" data-target="#exampleModal" CausesValidation="false" />
                   
                    </div>
                </div>
              
                <div class="containerRT2" runat="server" id="ProspectoGral" >
                    <div id="errMess" class="col-md-12">

                    </div>
                    <div class="col-md-12">
                        <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                    </div>
                    <div class="col-md-12"> 
                        <div class="row">
                        <div class="col-md-4"><asp:CheckBox CssClass="form-check-inputRT form-control" ID="txtExtranjero" Text="Extranjero" runat="server"  TabIndex="1"></asp:CheckBox></div>
                        <!--br><br--><div class="col-md-8">
                        <asp:DropDownList CssClass="form-check-inputRT form-control" ID="cmbTipoPersona" runat="server"  TabIndex="2" OnCheckedChanged="txtMoral_CheckedChanged" AutoPostBack="true">

                            <asp:ListItem Selected="True" Value="1">Persona F&#237;sica</asp:ListItem>
                            <asp:ListItem Value="2">Persona Moral</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        </div>
                        <br />
                        <br />
                        <div class="row">
                        <div class="col-md-12">
                        <asp:Label ID="lblRFC" runat="server" Text="RFC"></asp:Label>
                                    <div class="input-group mb-3">    
                                        <asp:TextBox ID="txtRFC" class="form-control" runat="server" CssClass="form-control" TabIndex="3" OnTextChanged="txtRFC_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                         <div class="input-group-append">
                                            <asp:Button CssClass="btn btn-outline-secondary" type="button" tabindex="4" runat="server" Text="Validar" OnClick="btnValidate_Click1" ID="btnValidate" UseSubmitBehavior="false" CausesValidation="false"></asp:Button>
                                        </div>
                                        
                                    </div>
                        <asp:RequiredFieldValidator id="valRFC" runat="server" ControlToValidate="txtRFC" CssClass="text-danger" ErrorMessage="RFC" Text="*"  />              
                        </div>
                        </div>
                    </div>
                    <div runat="server" id="Prospectodiv" class="containerRT12" style="display:none" >

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="PF-tab" data-toggle="tab" href="#DatosGenerales" role="tab" aria-controls="DatosGenerales" aria-selected="true">Datos Generales</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                      </li>
                        <li class="nav-item">
                        <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Direcciones" role="tab" aria-controls="Direcciones" aria-selected="false">Direcciones</a>
                      </li>
                        <li class="nav-item" id="tel" runat="server">
                        <a class="nav-link" id="Telefono-tab" data-toggle="tab" href="#Telefono" role="tab" aria-controls="Telefono" aria-selected="false">Teléfono</a>
                       </li>
                        <li class="nav-item" id="contalt" runat="server">
                        <a class="nav-link" id="ContactoAlterno-tab" data-toggle="tab" href="#ContactoAlterno" role="tab" aria-controls="ContactoAlterno" aria-selected="false">Contacto</a>
                      </li>
                      <li class="nav-item" id="Audit" runat="server">
                        <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="DatosGenerales" role="tabpanel" aria-labelledby="DatosGenerales-tab" >
                          <div id="PF" class="containerRT13" runat="server">
                                <div class="row" > 
                                    <div class="col">
                                        <asp:Label ID="lblName" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">    
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="30"   TabIndex="5"></asp:TextBox>
                                            <asp:RequiredFieldValidator id="valName" runat="server" ControlToValidate="txtName" CssClass="text-danger" ErrorMessage="Nombre" Text="*" />   
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMiddleName" runat="server" Text="Apellido Paterno"></asp:Label>
                                        <div class="form-group input-group">     
                                            <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control"  MaxLength="30"  TabIndex="6"></asp:TextBox>
                                            <asp:RequiredFieldValidator id="valApPat" runat="server" ControlToValidate="txtMiddleName" CssClass="text-danger" ErrorMessage="Apellido Paterno" Text="*" />   
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblLastName" runat="server" Text="Apellido Materno"></asp:Label>
                                        <div class="form-group input-group">   
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"  MaxLength="30" TabIndex="7"></asp:TextBox> 
                                            
                                        </div>
                                     </div>
                                    <div class="col">
                                        <asp:Label ID="lblBirthDate" runat="server" Text="Fecha de Nacimiento"></asp:Label>
                                        <div class="form-group input-group">  
                                            <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>     
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                            <asp:Label ID="lblCurp" runat="server" Text="CURP"></asp:Label>
                                            <div class="form-group input-group">      
                                                <asp:TextBox ID="txtCurp" runat="server" CssClass="form-control"  TabIndex="9"></asp:TextBox>
                                            </div>
                                    </div>
                                    <div class="col">
                                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                            <div class="form-group input-group">    
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" TabIndex="10"></asp:TextBox>
                                            </div>
                                    </div>
                                 </div>
                            </div>
                          <div id="PM" class="containerRT14" runat="server">  
                            <div class="row">
                                <div class="col" >
                                    <asp:Label ID="lblRazonSocial" runat="server" Text="Razón Social"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control" TabIndex="5" CausesValidation="true">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator id="valRazSoc" runat="server" ControlToValidate="txtRazonSocial" CssClass="text-danger" ErrorMessage="Razón Social" Text="*" />     
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="General" role="tabpanel" aria-labelledby="General-tab">
                          <div class="containerRT15" >
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblEstatusCliente" runat="server" Text="Estatus Prospecto"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:DropDownList ID="txtEstatusCliente" runat="server" Cssclass="form-control" Enabled="false">
                                            
                                        </asp:DropDownList>            
                                    </div>
                                </div>
                                <div class="col">
                                <asp:Label ID="lblGrupo" runat="server" Text="Grupo"></asp:Label>
                                <div class="input-group mb-3">
                                    <asp:DropDownList ID="txtGroup" runat="server"  CssClass="form-control" style="width:90%" CausesValidation="false">
                                        
                                    </asp:DropDownList> 
                                    <div class="input-group-append">
                                      <!--  <input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Grupo')"/> -->
                                     &nbsp;<a id="popupGrupo" href='#' data-image='Grupo.aspx?popup=1'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col">
                                <asp:Label ID="lblGiro" runat="server" Text="Giro"></asp:Label>
                                <div class="input-group mb-3">
                                    <asp:DropDownList ID="txtGiro" runat="server" CssClass="form-control" style="width:90%" CausesValidation="false"></asp:DropDownList>
                                    <div class="input-group-append">
                                        <!--<input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Giro')"/> -->
                                      &nbsp;<a id="popup" href='#' data-image='Giro.aspx?popup=1' ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                    </div>
                                </div>
                            </div>
                        </div>
                          </div>
                      </div>
                      <div class="tab-pane fade" id="Direcciones" role="tabpanel" aria-labelledby="Direcciones-tab">
                        <div class="containerRT16" >
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblTtiuloDireccion" runat="server" Text="Dirección Facturación"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblCPFiscal" runat="server" Text="C.P."></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtCPFiscal" runat="server" CssClass="form-control" TabIndex="9" OnTextChanged="txtCPFiscal_TextChanged" AutoPostBack="true" >
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator id="valCP" runat="server" ControlToValidate="txtCPFiscal" CssClass="text-danger" ErrorMessage="Código Postal Fiscal" Text="*"  />     
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblTituloDEnvio" runat="server" Text="Dirección Envío"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblCPEnvio" runat="server" Text="C.P."></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtCPEnvio" runat="server" CssClass="form-control" TabIndex="10" OnTextChanged="txtCPEnvio_TextChanged" AutoPostBack="true"  >
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCalleFiscal" runat="server" Text="Calle"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCalleFiscal" runat="server" CssClass="form-control" TabIndex="11">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCalleFiscal" CssClass="text-danger" ErrorMessage="Calle Fiscal" Text="*" />     
                                    </div>
                                </div>
                                <div class="col">
                                    <asp:Label ID="lblCalleEnvio" runat="server" Text="Calle"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCalleEnvio" runat="server" CssClass="form-control" TabIndex="12">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNumeroExt" runat="server" Text="Número Exterior"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNumeroExt" runat="server" CssClass="form-control" TabIndex="11">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNumeroExt" CssClass="text-danger" ErrorMessage="Número Exterior Fiscal" Text="*"  />     
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblNumeroExtEnv" runat="server" Text="Número Exterior Envío"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNumeroExtEnv" runat="server" CssClass="form-control" TabIndex="12">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNumeroInt" runat="server" Text="Número Interior"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNumeroInt" runat="server" CssClass="form-control" TabIndex="11">
                                                        </asp:TextBox>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblNumeroIntEnv" runat="server" Text="Número Interior Envío"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNumeroIntEnv" runat="server" CssClass="form-control" TabIndex="12">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblColoniaFiscal" runat="server" Text="Colonia"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:DropDownList ID="txtColoniaFiscal" runat="server" class="form-control"></asp:DropDownList> 
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtColoniaFiscal" CssClass="text-danger" ErrorMessage="Colonia Fiscal" Text="*"/>     
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblColoniaEnvio" runat="server" Text="Colonia"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:DropDownList ID="txtColoniaEnvio" runat="server" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblPaisFacturacion" runat="server" Text="Pais"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtPaisFacturacion" runat="server" CssClass="form-control" Enabled="false" TabIndex="15">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPaisFacturacion" CssClass="text-danger" ErrorMessage="Pais Fiscal"  Text="*"/>     
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblPaisEnvio" runat="server" Text="Pais"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtPaisEnvio" runat="server" CssClass="form-control" Enabled="false" TabIndex="16" >
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblEstadoFacturacion" runat="server" Text="Estado"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:DropDownList ID="txtEstadoFacturacion" runat="server" class="form-control" Enabled="false" ></asp:DropDownList>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEstadoFacturacion" CssClass="text-danger" ErrorMessage="Estado Fiscal" Text="*" />     
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <asp:Label ID="lblEstadoEnvio" runat="server" Text="Envio"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:DropDownList ID="txtEstadoEnvio" runat="server" class="form-control" Enabled="false" ></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                            
                                                <div class="col">
                                
                                                    <asp:Label ID="lblDelegacionFacturacion" runat="server" Text="Delegacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtDelegacionFacturacion" runat="server" CssClass="form-control" TabIndex="19" Enabled="false">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDelegacionFacturacion" CssClass="text-danger" ErrorMessage="Delegación Fiscal" Text="*"  />     
                                                    </div>
                                                </div>
                                                <div class="col">
                                
                                                    <asp:Label ID="lblDelegacionEnvio" runat="server" Text="Delegacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtDelegacionEnvio" runat="server" CssClass="form-control" TabIndex="20" Enabled="false" >
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="Telefono" role="tabpanel" aria-labelledby="Telefono-tab" >
                        <div class="containerRT18" >
                            <div class="row">
                            <div class="col">
                                <asp:Label ID="lblTelefonoTitulo" runat="server" Text="Teléfono"></asp:Label>
                                <br/>
                                 <asp:Label ID="lblOficina" runat="server" Text="Oficina"></asp:Label>
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtOficina" runat="server" CssClass="form-control" TabIndex="9">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="col">
                                 <asp:Label ID="lblExtension" runat="server" Text="Extensión"></asp:Label>
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtExtension" runat="server" CssClass="form-control" TabIndex="9">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                            <div class="col">
                                <asp:Label ID="lblCelular" runat="server" Text="Celular"></asp:Label>
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control" TabIndex="9">
                                    </asp:TextBox>
                                </div>
                            </div>
                        </div>
                        </div>
                      </div>
                     <div class="tab-pane fade" id="ContactoAlterno" role="tabpanel" aria-labelledby="ContactoAlterno-tab">
                          <div class="containerRT19">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNombreCA" runat="server" Text="Nombre Completo"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNombreCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblEmailCA" runat="server" Text="Email"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtEmailCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblTelefonoCA" runat="server" Text="Teléfono"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtTelefonoCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                           <%-- <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblPuestoCA" runat="server" Text="Puesto"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtPuestoCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>--%>
                          </div>
                      </div>
                      <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT20">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                              <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                          <div>
                              <asp:GridView ID="GVCommentList" runat="server" OnPageIndexChanging="GVCommentList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                            <Columns>
                            
                            <asp:BoundField DataField="Comentario" HeaderText="Comentario de Rechazo" />
                            <asp:BoundField DataField="Nombre" HeaderText="Usuario" />
                            
                        </Columns>
                                  <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                    </asp:GridView>
                          </div>
                      </div>  

                    </div>
                    </div>
                    <asp:HiddenField ID="PaneName" runat="server" />
                    <asp:HiddenField ID="LeadId" runat="server" />
                    <asp:HiddenField ID="OwnerId" runat="server" />
                    <asp:HiddenField ID="retVal" runat="server" />
                 </div>
         
         <div class="modal fade" id="exampleModal" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Comentarios de Rechazo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="400px" Height="200px"></asp:TextBox>
                    </div>
            
                 <div class="modal-footer">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <asp:Button ID="btnSaveReject" runat="server" Text="Rechazar" OnClick="btnReject_Click"  Cssclass="btn btn-outline-secondary" />
                  </div>
                </div>
              </div>
            </div>
        </div>  
        <div class="modal">
           
        </div>  
        <asp:Button ID="reloadGiro" runat="server" OnClick="reloadGiro_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
    <asp:Button ID="reloadGrupo" runat="server" OnClick="reloadGrupo_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
</asp:Content>
