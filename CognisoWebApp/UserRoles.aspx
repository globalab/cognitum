﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserRoles.aspx.cs" Inherits="CognisoWebApp.UserRoles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
                $("#<%=txtNewRole.ClientID%>").select2({
                    placeholder: "Seleccione un rol",
                    minimumResultsForSearch: 1,
                    allowClear: true
                });
        });
    </script>
    <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Roles de usuario</asp:Label>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="SecurityRolesList.aspx">Roles de usuario</a></li>
                    <li class="breadcrumb-item active">Usuario <%=UserId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <%--<asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" />--%>
                </div>
            </div>

            <div class="container" runat="server" id="RoleGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" onclick="selectPane('General');">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUserId" runat="server" Text="Usuario"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUserId" runat="server" class="form-control" Enabled="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNombre" runat="server" class="form-control" Enabled="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div id="gridtitle" runat="server" class="row">
                                    <div class="col">
                                        <asp:Label ID="lbluserRoles" runat="server" Text="Privilegios"></asp:Label>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblNewRole" runat="server" Text="Nuevo Rol"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtNewRole" Style="width: 90%" runat="server" class="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:ImageButton ID="btnNewRole" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClick="btnNewRole_Click" />
                                    </div>
                                </div>
                                <div id="gridDiv" runat="server" class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvRole" AutoGenerateColumns="False" OnSelectedIndexChanging="gvRole_SelectedIndexChanging" CssClass="table table-hover table-striped" runat="server">
                                                <Columns>
                                                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/Content/Imgs/delete.png" ShowSelectButton="True" />
                                                    <asp:TemplateField HeaderText="Id">
                                                        <ItemTemplate>
                                                            <asp:Label ID="SecurityRoleId" runat="server" Text='<%#Eval("SecurityRoleId")%>' style="font-size: 14px; color: black; font-family: Arial; text-align: left"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="SecurityRoleRoleName" HeaderText="Rol" />
                                                </Columns>
                                            </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="UserId" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
