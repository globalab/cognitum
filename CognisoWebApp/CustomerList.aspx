﻿<%@ Page Title="Prospecto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="CognisoWebApp.CustomerList" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
</script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
</script>

    <div class="containerRT1">
        <div class="containerRT2" runat="server">
                 <div class="panel-heading">    
                    <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Clientes</asp:Label></h2>
                </div> 
                <hr class="float-left">
  
                 <nav aria-label="breadcrumb" class="clearfix">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                        <li class="breadcrumb-item active"><a href="#">Lista de Clientes</a></li>
                      </ol>
                </nav>  
                   
               
                <div class="panel-heading">
                    <div class="btn-group">
                        <!--<asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/writing.png" runat="server" OnClick="BtnSave_Click"/>-->
                        <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server"  />-->
                    </div>
                </div>
              <div class="input-group mb-3">
                         <asp:TextBox Id="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id,Nombre o RFC a buscar" aria-label="Id,Nombre o RFC a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                         <div class="input-group-append">
                            <asp:Button Id="btnBuscar" runat="server" Cssclass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click" ></asp:Button>
                           <!--<label class="form-control">Filtro: </label>
                              <asp:DropDownList Id="ProspectView" runat="server" Cssclass="form-control">
                                <asp:ListItem Value="1">Todos los prospectos</asp:ListItem>
                                <asp:ListItem Value ="2">Prospectos nuevos</asp:ListItem>
                                <asp:ListItem Value="3">Prospectos en validación</asp:ListItem>
                                <asp:ListItem Value="4">Prospectos rechazados</asp:ListItem>
                                <asp:ListItem Value="5">Prospectos convertidos en clientes</asp:ListItem>
                            </asp:DropDownList>-->
                        </div>
                </div>
                <div class="panel-body" runat="server">
                    
                    <div class="table-responsive" runat="server">
                        <asp:GridView ID="GVCustList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                            <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a   href='Cliente.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                            <!--<asp:CheckBox ID="chkEliminar" runat="server"/>-->
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre" />
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" />
                                    <asp:BoundField DataField="Audit_Estatus" HeaderText="Estatus" />
                                </Columns>
                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="SearchId" runat="server" Value="" />
                 </div>
             
    </div>    
</div>
</asp:Content>
