﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class Comision : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                string polizaId = Request.QueryString["poliza"];
                polizaId = string.IsNullOrEmpty(polizaId) ? string.Empty : polizaId;
                initCatalogos(polizaId);
                if (!string.IsNullOrEmpty(polizaId))
                {
                    searchComision(polizaId);
                }
            }
        }

        private void initCatalogos(string idPoliza = "")
        {
            dirAsociados();
            populatePolizas(idPoliza);
        }

        private void dirAsociados()
        {
            string errmess = string.Empty;
            JoinEntity joinEnt = new JoinEntity();
            Entity dirAs = new Entity("DirectorAsociado");
            dirAs.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));

            Entity usuario = new Entity("Usuario");
            usuario.Attributes.Add(new Models.Attribute("Nombre"));            

            selectJoinEntity selJoin = new selectJoinEntity("Id", 1, "Usuario");
            joinEnt.ChildEntity = dirAs;
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.selectJoinList.Add(selJoin);

            usuario.ChildEntities.Add(joinEnt);

            List<Entity> listResult = SiteFunctions.GetValues(usuario, out errmess);
            DataTable dtUsers = SiteFunctions.trnsformEntityToDT(listResult);

            txtDirectorAsociado.DataSource = dtUsers;
            txtDirectorAsociado.DataValueField = "DirectorAsociadoId";
            txtDirectorAsociado.DataTextField = "Nombre";
            txtDirectorAsociado.DataBind();
        }

        private void populatePolizas(string idPoliza)
        {
            string errMsg = string.Empty;
            Entity polizas = new Entity("Tramite");
            polizas.Attributes.Add(new Models.Attribute("id", "", "int"));
            polizas.Attributes.Add(new Models.Attribute("Folio", "", "string"));

            if (idPoliza != string.Empty)
            {
                polizas.Keys.Add(new Key("id", idPoliza, "int"));
            }

            List<Entity> results = SiteFunctions.GetValues(polizas, out errMsg);
            DataTable dtResults = SiteFunctions.trnsformEntityToDT(results);
            txtPoliza.DataValueField = "id";
            txtPoliza.DataTextField = "id";

            txtPoliza.DataSource = dtResults;
            txtPoliza.DataBind();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            saveComision();
        }

        private void saveComision()
        {
            string errMsg = string.Empty;
            string polizaId = Request.QueryString["poliza"];
            if (!string.IsNullOrEmpty(polizaId))
            {
                int userid = 0;
                int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
                Auditable audi = new Auditable();
                audi.userId = userid;
                audi.opDate = DateTime.Now;
                Entity comision = new Entity("ComisionAsociado");
                comision.Attributes.Add(new Models.Attribute("Poliza", polizaId, "int"));
                comision.Attributes.Add(new Models.Attribute("Porcentaje", txtComision.Text, "float"));
                comision.Attributes.Add(new Models.Attribute("DirectorAsociado", txtDirectorAsociado.SelectedValue, "int"));

                string id = Request.QueryString["id"];
                Method method = Method.POST;
                if (id != null)
                {
                    method = Method.PUT;
                    comision.Keys.Add(new Models.Key("Id", id.ToString(), "int"));
                    audi.entityId = Convert.ToInt32(id);
                }

                comision.auditable = audi;

                if (SiteFunctions.SaveEntity(comision, method, out errMsg))
                {
                    showMessage("Se guardo correctamente la comisión", false);
                    Session["retVal"] = errMsg;
                }
                else
                {
                    showMessage(errMsg, true);
                }
            }
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void searchComision(string id)
        {
            string errMsg = string.Empty;
            Entity comision = new Entity("ComisionAsociado");

            comision.Attributes.Add(new Models.Attribute("Id", "", "int"));
            comision.Attributes.Add(new Models.Attribute("Poliza", "", "int"));
            comision.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            comision.Attributes.Add(new Models.Attribute("DirectorAsociado", "", "int"));

            comision.Keys.Add(new Key("Id", id, "int"));

            List<Entity> results = SiteFunctions.GetValues(comision, out errMsg);

            if (results.Count > 0)
            {
                Entity comToMap = results[0];
                txtPoliza.SelectedValue = comToMap.getAttrValueByName("Poliza");
                txtComision.Text = comToMap.getAttrValueByName("Porcentaje");
                txtDirectorAsociado.SelectedValue = comToMap.getAttrValueByName("DirectorAsociado");
                txtId.Text = id;

                txtCentroBeneficio.Text = comToMap.getAttrValueByName("auditableCentroBeneficio");
                txtFechaAlta.Text = comToMap.getAttrValueByName("auditableFechaAlta");
                txtFechaUltimaMod.Text = comToMap.getAttrValueByName("auditableFechaUltimaMod");
                txtUbicacion.Text = comToMap.getAttrValueByName("auditableUbicacion");
                txtUsuarioAlta.Text = comToMap.getAttrValueByName("auditableUsuarioAlta");
                txtUsuarioUltimaMod.Text = comToMap.getAttrValueByName("auditableUsuarioUltimaMod");
            }
            else if (errMsg != string.Empty)
            {
                showMessage(errMsg, true);
            }
        }
    }
}