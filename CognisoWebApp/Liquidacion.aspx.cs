﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using CognisoWebApp.Models;
using System.Data;
using System.Text;
using CognisoWA.Controllers;
using Microsoft.Reporting.WebForms;
using System.IO;
using RestSharp;
using System.Configuration;
using System.Threading;

namespace CognisoWebApp
{
    public partial class Liquidacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                string Id = Request.QueryString["id"];
                if (!SiteFunctions.IsUsuarioCobranza(this.Page.User.Identity.Name))
                {
                    btnAplicar.Enabled = false;
                }
                else
                {
                    btnAplicar.Enabled = true;
                }
                initCatalogos();
               if (!string.IsNullOrEmpty(Id))
                {
                    LiqId.Value = Id;
                    RecibosDiv.Visible = true; 
                    mapLiquidacion(Id.ToString());
                    if (txtStatus.SelectedValue != "0")
                    {
                        
                    
                        btnAplicar.Enabled = false;
                    }
                    searchIntegraciones();
                }
                else
                {
                    txtStatus.SelectedValue = "0";
                    txtTipoCambio.Text = "1";
                    txtAseguradora.Items.Add("");
                    txtAseguradora.SelectedValue = "";
                    RecibosDiv.Visible = false;
                }
            }
            else
            {
                this.showTab();
            }

        }

        private void showTab()
        {

            // Define the name and type of the client scripts on the page.
            String csname1 = "TabScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> collapsediv(); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void searchLiqRecibos(string id)
        {
            PaneName.Value = "Recibos";
            Entity recibos = initRecibosSearch();

            recibos.Keys.Add(new Key("liquidacion", id, "int"));

            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(recibos, out errMsg);
            updateTotalValues(results);
            gvRecibos.DataSource = SiteFunctions.trnsformEntityToDT(results);
            gvRecibos.DataBind();



        }

        private void updateTotalValues(List<Entity> _recibos)
        {
            float totalRecibo = 0;
            float totalNC = 0;
            float totalValores = 0;
            float totalComRecibo = 0;
            float totalComNC = 0;
            float.TryParse(txtTotalValores.Text, out totalValores);

            foreach (Entity Recibo in _recibos)
            {
                string tipo = Recibo.getAttrValueByName("tipo");
                string totalStr = Recibo.getAttrValueByName("Total");
                string comision = Recibo.getAttrValueByName("Comision");
                float totalAmt = 0, comisionAmt = 0;
                float.TryParse(totalStr, out totalAmt);
                float.TryParse(comision, out comisionAmt);
                if (tipo == "0")
                {
                    totalRecibo += totalAmt;
                    totalComRecibo += comisionAmt;
                }
                else
                {
                    totalNC += totalAmt;
                    totalComNC += comisionAmt;
                }
            }
            bool save = false;
            if (txtTotalRecibos.Text != totalRecibo.ToString())
            {
                save = true;
            }
            txtTotalRecibos.Text = totalRecibo.ToString();
            txtTotalNC.Text = totalNC.ToString();
            txtTotalLiquidacion.Text = (totalRecibo - totalNC).ToString();

            txtTotalCRecibos.Text = totalComRecibo.ToString();
            txtTotalCNotas.Text = totalComNC.ToString();
            txtTotalComision.Text = (totalComRecibo - totalComNC).ToString();

            txtDiferencia.Text = (totalValores - (totalRecibo - totalNC)).ToString();
            if (save)
            {
                saveLiquidacion();
            }
        }

        private Entity initRecibosSearch()
        {
            Entity recibos = new Entity("recibo");
            recibos.useAuditable = false;
            recibos.Attributes.Add(new Models.Attribute("Id"));
            recibos.Attributes.Add(new Models.Attribute("Cobertura"));
            recibos.Attributes.Add(new Models.Attribute("Vencimiento"));
            recibos.Attributes.Add(new Models.Attribute("Concepto"));
            recibos.Attributes.Add(new Models.Attribute("Total"));
            recibos.Attributes.Add(new Models.Attribute("Numero"));
            recibos.Attributes.Add(new Models.Attribute("Estatus"));
            recibos.Attributes.Add(new Models.Attribute("Tipo"));
            recibos.Attributes.Add(new Models.Attribute("Comision"));

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Folio"));
            tramite.Attributes.Add(new Models.Attribute("id"));

            selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
            JoinEntity joinEnt = new JoinEntity();
            joinEnt.ChildEntity = tramite;
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.selectJoinList.Add(selJoin);

            recibos.ChildEntities.Add(joinEnt);

            return recibos;
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            saveLiquidacion();
        }

        private void initCatalogos()
        {
            string errMsg = string.Empty;

            
            Entity polizas = new Entity("Poliza");
            polizas.Attributes.Add(new Models.Attribute("id", "", "int"));
            polizas.Keys.Add(new Key("EstatusPoliza", "0", "int", 2));
            polizas.Keys.Add(new Key("EstatusPoliza", "1", "int", 2));

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Folio", "", "string"));

            selectJoinEntity selJoin = new selectJoinEntity("Id", 1, "Id");
            JoinEntity joinEntity = new JoinEntity();
            joinEntity.ChildEntity = tramite;
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.selectJoinList.Add(selJoin);

            polizas.ChildEntities.Add(joinEntity);

            List<Entity> results = new List<Entity>();
            results = SiteFunctions.GetValues(polizas, out errMsg);
            DataTable dtResults = SiteFunctions.trnsformEntityToDT(results, true);
            txtPoliza.DataSource = dtResults;
            txtPoliza.DataValueField = "Id";
            txtPoliza.DataTextField = "tramiteFolio";
            txtPoliza.DataBind();

            Entity entGRP = new Entity("Grupo");
            entGRP.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            entGRP.Attributes.Add(new CognisoWebApp.Models.Attribute("Descripcion"));
            entGRP.useAuditable = false;
            results = SiteFunctions.GetValues(entGRP, out errMsg);
            txtGrupo.DataSource = SiteFunctions.trnsformEntityToDT(results, true);
            txtGrupo.DataValueField = "Id";
            txtGrupo.DataTextField = "Descripcion";
            txtGrupo.DataBind();

            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Aseguradora.useAuditable = false;
            JoinEntity joinCust = new JoinEntity();
            joinCust.ChildEntity = new Entity("Cliente");
            joinCust.JoinKey = new Models.Attribute("Id");
            joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
            selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
            joinCust.selectJoinList.Add(custsj);
            joinCust.JoinType = JoinType.Inner;
            Aseguradora.ChildEntities.Add(joinCust);
            results = SiteFunctions.GetValues(Aseguradora, out errMsg);
            txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtAseguradora.DataValueField = "Id";
            txtAseguradora.DataTextField = "ClienteNombreCompleto";
            txtAseguradora.DataBind();

            Entity moneda = new Entity("Moneda");
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            moneda.useAuditable = false;
            results = SiteFunctions.GetValues(moneda, out errMsg);
            txtMoneda.DataSource = SiteFunctions.trnsformEntityToDT(results);
            txtMoneda.DataValueField = "Id";
            txtMoneda.DataTextField = "Nombre";
            txtMoneda.DataBind();

            try
            {
                txtMoneda.SelectedValue = "2187";
            }
            catch (Exception)
            {
            }
            initEnums();
        }

        private void saveLiquidacion()
        {
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;
            object Id = LiqId.Value == string.Empty ? null : LiqId.Value;//Request.QueryString["id"];
            Entity liquidacion = new Entity("Liquidacion");
            liquidacion.Attributes.Add(new Models.Attribute("Permiso", "2", "int64"));
            if (Id == null)
            {
                liquidacion.Attributes.Add(new Models.Attribute("Propietario", userid.ToString(), "int64"));
                int ids = 0;
                int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
                liquidacion.Attributes.Add(new Models.Attribute("Ubicacion", ids.ToString(), "int64"));
                ids = 0;
                int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
                liquidacion.Attributes.Add(new Models.Attribute("CentroDeBeneficio", ids.ToString(), "int64"));
            }
            liquidacion.useAuditable = true;
            liquidacion.auditable = audi;

            //liquidacion.Attributes.Add(new Models.Attribute("Folio", txtFolio.Text, "string"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoCheque", txtTotalCheques.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoTransferencia", txtTotalTransferencia.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoPrimasEnDeposito", txtTotalPrimasDep.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalValores", txtTotalValores.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalRecibos", txtTotalRecibos.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComision", txtTotalComision.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("DiferenciaPago", txtDiferencia.Text, "float"));
            //liquidacion.Attributes.Add(new Models.Attribute("MontoSobreComision", "", ""));
            liquidacion.Attributes.Add(new Models.Attribute("Estatus", txtStatus.SelectedValue, "int"));
            liquidacion.Attributes.Add(new Models.Attribute("FechaLiquidacion", txtFecha.Text, "datetime"));
            liquidacion.Attributes.Add(new Models.Attribute("Observaciones", txtObservaciones.Text, "string"));
            liquidacion.Attributes.Add(new Models.Attribute("Aseguradora", txtAseguradora.SelectedValue, "int"));
            liquidacion.Attributes.Add(new Models.Attribute("Moneda", txtMoneda.SelectedValue, "int"));
            if (txtPoliza.SelectedValue != string.Empty)
            {
                liquidacion.Attributes.Add(new Models.Attribute("Poliza", txtPoliza.SelectedValue, "int"));
            }
            if (txtGrupo.SelectedValue != string.Empty)
            {
                liquidacion.Attributes.Add(new Models.Attribute("Grupo", txtGrupo.SelectedValue, "int"));
            }
            liquidacion.Attributes.Add(new Models.Attribute("MontoTarjetaCredito", txtTotalTC.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalNotasCredito", txtTotalNC.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComisionNotas", txtTotalCNotas.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalLiquidacion", txtTotalLiquidacion.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComisionLiquidacion", txtTotalComision.Text, "float"));
            liquidacion.Attributes.Add(new Models.Attribute("TipoCambio", txtTipoCambio.Text, "float"));

            string errmsg = string.Empty;
            
            RestSharp.Method method = RestSharp.Method.POST;

            if (Id != null)
            {
                liquidacion.Keys.Add(new Key("Id", Id.ToString(), "int"));
                method = RestSharp.Method.PUT;
            }
            if (SiteFunctions.SaveEntity(liquidacion, method, out errmsg))
            {
                liquidacion = new Entity("liquidacion");
                string newId = Id != null ? Id.ToString() : errmsg;
                LiqId.Value = newId;
                RecibosDiv.Visible = true;
                txtFolio.Text = newId;
                //liquidacion.Attributes.Add(new Models.Attribute("Folio", errmsg, "string"));

                //liquidacion.Keys.Add(new Key("Id", errmsg, "int"));
                //SiteFunctions.SaveEntity(liquidacion, RestSharp.Method.PUT, out errmsg);
                //Response.Redirect("Liquidacion?id=" + newId);
                showMessage("Liquidación guardada correctamente", false);
            }
            else
            {
                showMessage(errmsg, true);
            }
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void initEnums()
        {
            Array itemValues = System.Enum.GetValues(typeof(EstatusLiquidacionEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusLiquidacionEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtStatus.Items.Add(item);
            }
            txtStatus.SelectedValue = "0";
        }

        private void mapLiquidacion(string _id)
        {
            Entity liquidacion = new Entity("Liquidacion");

            liquidacion.Attributes.Add(new Models.Attribute("Permiso"));
            liquidacion.Attributes.Add(new Models.Attribute("Propietario"));
            liquidacion.Attributes.Add(new Models.Attribute("Ubicacion")); 
            liquidacion.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            liquidacion.useAuditable = true;

            liquidacion.Attributes.Add(new Models.Attribute("Folio"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoCheque"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoTransferencia"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoPrimasEnDeposito"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalValores"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalRecibos"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComision"));
            liquidacion.Attributes.Add(new Models.Attribute("DiferenciaPago"));
            //liquidacion.Attributes.Add(new Models.Attribute("MontoSobreComision", "", ""));
            liquidacion.Attributes.Add(new Models.Attribute("Estatus"));
            liquidacion.Attributes.Add(new Models.Attribute("FechaLiquidacion"));
            liquidacion.Attributes.Add(new Models.Attribute("Observaciones"));
            liquidacion.Attributes.Add(new Models.Attribute("Aseguradora"));
            liquidacion.Attributes.Add(new Models.Attribute("Moneda"));
            liquidacion.Attributes.Add(new Models.Attribute("Poliza"));
            liquidacion.Attributes.Add(new Models.Attribute("Grupo"));
            liquidacion.Attributes.Add(new Models.Attribute("MontoTarjetaCredito"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalNotasCredito"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComisionNotas"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalLiquidacion"));
            liquidacion.Attributes.Add(new Models.Attribute("TotalComisionLiquidacion"));
            liquidacion.Attributes.Add(new Models.Attribute("TipoCambio"));

            liquidacion.Keys.Add(new Key("id", _id, "int"));
            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(liquidacion, out errMsg);
            if(results.Count > 0)
            {
                txtFolio.Text = results[0].getAttrValueByName("Folio");
                txtTotalCheques.Text = results[0].getAttrValueByName("MontoCheque");
                txtTotalTransferencia.Text = results[0].getAttrValueByName("MontoTransferencia");
                txtTotalPrimasDep.Text = results[0].getAttrValueByName("MontoPrimasEnDeposito");
                txtTotalValores.Text = results[0].getAttrValueByName("TotalValores");
                txtTotalRecibos.Text = results[0].getAttrValueByName("TotalRecibos");
                txtTotalComision.Text = results[0].getAttrValueByName("TotalComision");
                txtDiferencia.Text = results[0].getAttrValueByName("DiferenciaPago");
                txtStatus.SelectedValue = results[0].getAttrValueByName("Estatus");
                txtFecha.Text = results[0].getAttrValueByName("FechaLiquidacion");
                txtObservaciones.Text = results[0].getAttrValueByName("Observaciones");
                txtAseguradora.SelectedValue = results[0].getAttrValueByName("Aseguradora");
                txtMoneda.SelectedValue = results[0].getAttrValueByName("Moneda");
                txtGrupo.SelectedValue = results[0].getAttrValueByName("Grupo");
                if (txtGrupo.SelectedValue != string.Empty)
                {
                    txtPoliza.Enabled = false;
                }
                else
                {
                    txtGrupo.Enabled = false;
                    txtPoliza.SelectedValue = results[0].getAttrValueByName("Poliza");
                }
                txtTotalTC.Text = results[0].getAttrValueByName("MontoTarjetaCredito");
                txtTotalNC.Text = results[0].getAttrValueByName("TotalNotasCredito");
                txtTotalCNotas.Text = results[0].getAttrValueByName("TotalComisionNotas");
                txtTotalLiquidacion.Text = results[0].getAttrValueByName("TotalLiquidacion");
                txtTotalComision.Text = results[0].getAttrValueByName("TotalComisionLiquidacion");
                txtTipoCambio.Text = results[0].getAttrValueByName("TipoCambio");

                DateTime fechaaudit = Convert.ToDateTime(results[0].getAttrValueByName("Audit_FechaAdd").ToString());
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(results[0].getAttrValueByName("Audit_FechaUMod").ToString());
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", results[0].getAttrValueByName("Audit_UsuarioAdd").ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", results[0].getAttrValueByName("Audit_UsuarioUMod").ToString(), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtId.Text = _id;
                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre"));
                Ubicacion.Keys.Add(new Models.Key("id", results[0].getAttrValueByName("Ubicacion"), "int"));
                errMsg = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(Ubicacion, out errMsg);
                if (listResult.Count > 0)
                {
                    txtUbicacion.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                Entity CB = new Entity("CentroDeBeneficio");
                CB.Attributes.Add(new Models.Attribute("Nombre"));
                CB.Keys.Add(new Models.Key("id", results[0].getAttrValueByName("CentroDeBeneficio"), "int"));
                errMsg = string.Empty;
                listResult = SiteFunctions.GetValues(CB, out errMsg);
                if (listResult.Count > 0)
                {
                    txtCentroBeneficio.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                searchLiqRecibos(_id);
            }
        }

        protected void btnAddRecibosLiq_Click(object sender, EventArgs e)
        {
            
        }

        protected void txtPoliza_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtPoliza.SelectedValue != string.Empty)
            {
                txtGrupo.Enabled = false;
            }
            else
            {
                txtGrupo.Enabled = true;
            }
        }

        protected void txtGrupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtGrupo.SelectedValue != string.Empty)
            {
                txtPoliza.Enabled = false;
            }
            else
            {
                txtPoliza.Enabled = true;
            }
        }

        protected void btnRefreshRecibos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            string Id = LiqId.Value;//.QueryString["id"];
            searchLiqRecibos(Id);
        }

        protected void BtnImportRecibos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            object id = LiqId.Value;
            if (fileUploadRecibos.HasFile)
            {
                string fileName = fileUploadRecibos.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        float totalPrima = 0;
                        List<string> listErrors;
                        if (!SiteFunctions.importRecibosLiquidacion(fileUploadRecibos.FileContent, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out listErrors))
                        {
                            string errMsg = SiteFunctions.formatListErrors(listErrors);
                            showMessage(errMsg, true);
                        }
                        
                        searchLiqRecibos(id.ToString());
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
        }

        protected void btnAplicar_Click(object sender, EventArgs e)
        {
            bool aplicar = true;
            Entity recibos = initRecibosSearch();
            string Id = LiqId.Value;// Request.QueryString["id"];
            recibos.Keys.Add(new Key("liquidacion", Id, "int"));

            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(recibos, out errMsg);
            if (results.Count > 0)
            {
                foreach (Entity recibo in results)
                {
                    if (!validaAplicacion(recibo.getAttrValueByName("tramiteId"), txtAseguradora.SelectedValue))
                    {
                        aplicar = false;
                        break;
                    }
                }

                if (aplicar)
                {
                    Aplicarliquidacion(Id, results);
                    searchLiqRecibos(Id);
                }
            }
            else
            {
                showMessage("No hay recibos ligados a la liquidación", true);
            }
        }

        private void Aplicarliquidacion(string Id, List<Entity> _recibos)
        {
            string errmgs = string.Empty;
            bool updLiquidacion = true;

            Entity liquidacion = new Entity("liquidacion");
            liquidacion.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
            liquidacion.Keys.Add(new Key("Id", Id, "int"));
            txtStatus.SelectedValue = "1";
            foreach (Entity recibo in _recibos)
            {
                Entity reciboUpd = new Entity("recibo");
                reciboUpd.Attributes.Add(new Models.Attribute("Estatus", "3", "int"));
                reciboUpd.Keys.Add(new Key("id", recibo.getAttrValueByName("id"), "int"));

                if(!SiteFunctions.SaveEntity(reciboUpd, RestSharp.Method.PUT, out errmgs))
                {
                    updLiquidacion = false;
                    showMessage(errmgs, true);
                    break;
                }
            }
            
            if (updLiquidacion)
            {
                if (!SiteFunctions.SaveEntity(liquidacion, RestSharp.Method.PUT, out errmgs))
                {
                    showMessage(errmgs, true);
                }
                else
                {
                    showMessage("Aplicación correcta!!!", false);
                    btnAplicar.Enabled = false;
                }
            }
        }



        private string printRpt(out string fileName)
        {
            fileName = string.Empty;
            LiquidacionViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

            LocalReport localReport = LiquidacionViewer.LocalReport;
            localReport.EnableExternalImages = true;
            DataSet dataset = new DataSet("LiquidacionQuery");

            ReportDataSource dsLiquidacion = new ReportDataSource();
            dsLiquidacion.Name = "LiquidacionQuery";
            string recordId = LiqId.Value;// Request.QueryString["id"];
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(recordId))
            {
                Entity liquidacion = new Entity("Liquidacion");
                liquidacion.useAuditable = false;
                liquidacion.Attributes.Add(new Models.Attribute("Folio"));
                liquidacion.Attributes.Add(new Models.Attribute("FechaLiquidacion"));
                liquidacion.Attributes.Add(new Models.Attribute("Aseguradora"));
                liquidacion.Attributes.Add(new Models.Attribute("Observaciones"));
                liquidacion.Keys.Add(new Key("Id", recordId.ToString(), "int"));

                Entity aseguradora = new Entity("cliente");
                aseguradora.Attributes.Add(new Models.Attribute("NombreCompleto"));

                selectJoinEntity selJoin = new selectJoinEntity("Aseguradora", 1, "Id");
                JoinEntity joinEntity = new JoinEntity();
                joinEntity.ChildEntity = aseguradora;
                joinEntity.JoinType = JoinType.Inner;
                joinEntity.selectJoinList.Add(selJoin);

                liquidacion.ChildEntities.Add(joinEntity);

                DataTable dtResults = new DataTable();
                #region crea tabla de reporte
                dtResults.Columns.Add(new DataColumn("Folio", typeof(string)));
                dtResults.Columns.Add(new DataColumn("Fecha", typeof(string)));
                dtResults.Columns.Add(new DataColumn("AseguradoraCve", typeof(string)));
                dtResults.Columns.Add(new DataColumn("AseguradoraName", typeof(string)));
                dtResults.Columns.Add(new DataColumn("PolizaCve", typeof(string)));
                dtResults.Columns.Add(new DataColumn("PolizaName", typeof(string)));
                dtResults.Columns.Add(new DataColumn("ReciboNum", typeof(string)));
                dtResults.Columns.Add(new DataColumn("Asegurado", typeof(string)));
                dtResults.Columns.Add(new DataColumn("Vigencia", typeof(string)));
                dtResults.Columns.Add(new DataColumn("ObsPago", typeof(string)));
                dtResults.Columns.Add(new DataColumn("PrimaTotal", typeof(decimal)));
                dtResults.Columns.Add(new DataColumn("Comision", typeof(string)));
                dtResults.Columns.Add(new DataColumn("NumCobranza", typeof(string)));
                dtResults.Columns.Add(new DataColumn("TipoIngreso", typeof(string)));
                dtResults.Columns.Add(new DataColumn("SubRamo", typeof(string)));
                dtResults.Columns.Add(new DataColumn("AgenteCve", typeof(string)));
                dtResults.Columns.Add(new DataColumn("AgenteName", typeof(string)));
                dtResults.Columns.Add(new DataColumn("Nota", typeof(string)));
                dtResults.Columns.Add(new DataColumn("Observaciones", typeof(string)));
                dtResults.Columns.Add(new DataColumn("UserCve", typeof(string)));
                dtResults.Columns.Add(new DataColumn("UserName", typeof(string)));
                dtResults.Columns.Add(new DataColumn("UserEmail", typeof(string)));
                dtResults.Columns.Add(new DataColumn("UserRole", typeof(string)));
                #endregion

                #region buscar información de la liquidacion para reporte
                List<Entity> results = SiteFunctions.GetValues(liquidacion, out errorMsg);
                if (results.Count > 0)
                {
                    Entity recibo = new Entity("Recibo");
                    recibo.Attributes.Add(new Models.Attribute("Tramite"));
                    recibo.Attributes.Add(new Models.Attribute("Numero"));
                    recibo.Attributes.Add(new Models.Attribute("Vencimiento"));
                    recibo.Attributes.Add(new Models.Attribute("ObservacionPago"));
                    recibo.Attributes.Add(new Models.Attribute("Comision"));
                    recibo.Attributes.Add(new Models.Attribute("Total"));
                    recibo.Attributes.Add(new Models.Attribute("TipoIngreso"));
                    recibo.Keys.Add(new Key("liquidacion", recordId.ToString(), "int"));

                    #region Add tramite
                    Entity tramite = new Entity("Tramite");
                    tramite.Attributes.Add(new Models.Attribute("Folio"));

                    selJoin = new selectJoinEntity("Tramite", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = tramite;
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.selectJoinList.Add(selJoin);

                    recibo.ChildEntities.Add(joinEntity);
                    #endregion

                    #region si viene de poliza
                    #region Add Poliza
                    Entity poliza = new Entity("poliza");
                    poliza.Attributes.Add(new Models.Attribute("subRamo"));
                    poliza.Attributes.Add(new Models.Attribute("Agente"));
                    poliza.Attributes.Add(new Models.Attribute("Titular"));

                    selJoin = new selectJoinEntity("Id", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = poliza;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    tramite.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Subramo
                    Entity subramo = new Entity("subramo");
                    subramo.Attributes.Add(new Models.Attribute("Nombre"));

                    selJoin = new selectJoinEntity("subRamo", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = subramo;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    poliza.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Cliente
                    Entity Cliente = new Entity("cliente");
                    Cliente.Attributes.Add(new Models.Attribute("NombreCompleto"));

                    selJoin = new selectJoinEntity("Contratante", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = Cliente;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    poliza.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Agente
                    Entity agente = new Entity("agente");
                    agente.Attributes.Add(new Models.Attribute("Nombre"));

                    selJoin = new selectJoinEntity("agente", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = agente;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    poliza.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add titular
                    Entity titular = new Entity("ContactoSimple");
                    titular.Attributes.Add(new Models.Attribute("NombreCompleto"));

                    selJoin = new selectJoinEntity("Titular", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = titular;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    poliza.ChildEntities.Add(joinEntity);
                    #endregion
                    #endregion

                    #region si viene de Endoso

                    #region Add Endoso
                    Entity endoso = new Entity("Endoso");
                    endoso.Attributes.Add(new Models.Attribute("poliza"));
                  
                    selJoin = new selectJoinEntity("Id", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = endoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    tramite.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Poliza
                    Entity polizaEndoso = new Entity("poliza");
                    polizaEndoso.EntityAlias = "polizaEndoso";
                    polizaEndoso.Attributes.Add(new Models.Attribute("subRamo"));
                    polizaEndoso.Attributes.Add(new Models.Attribute("Agente"));
                    polizaEndoso.Attributes.Add(new Models.Attribute("Titular"));

                    selJoin = new selectJoinEntity("poliza", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = polizaEndoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    endoso.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Subramo
                    Entity subramoEndoso = new Entity("subramo");
                    subramoEndoso.EntityAlias = "subramoEndoso";
                    subramoEndoso.Attributes.Add(new Models.Attribute("Nombre"));

                    selJoin = new selectJoinEntity("subRamo", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = subramoEndoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    polizaEndoso.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Cliente
                    Entity ClienteEndoso = new Entity("cliente","clienteEndoso");
                    ClienteEndoso.Attributes.Add(new Models.Attribute("NombreCompleto"));

                    selJoin = new selectJoinEntity("Contratante", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = ClienteEndoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    polizaEndoso.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add Agente
                    Entity agenteEndoso = new Entity("agente");
                    agenteEndoso.EntityAlias = "agenteEndoso";
                    agenteEndoso.Attributes.Add(new Models.Attribute("Nombre"));

                    selJoin = new selectJoinEntity("agente", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = agenteEndoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    polizaEndoso.ChildEntities.Add(joinEntity);
                    #endregion

                    #region Add titular
                    Entity titularEndoso = new Entity("ContactoSimple");
                    titularEndoso.EntityAlias = "titularEndoso";
                    titularEndoso.Attributes.Add(new Models.Attribute("NombreCompleto"));

                    selJoin = new selectJoinEntity("Titular", 1, "Id");
                    joinEntity = new JoinEntity();
                    joinEntity.ChildEntity = titularEndoso;
                    joinEntity.JoinType = JoinType.Left;
                    joinEntity.selectJoinList.Add(selJoin);

                    polizaEndoso.ChildEntities.Add(joinEntity);
                    #endregion
                    #endregion

                    List<Entity> recibos = SiteFunctions.GetValues(recibo, out errorMsg);

                    if (recibos.Count > 0)
                    {
                        Entity Usuario = SiteFunctions.getUserEntity(Context.User.Identity.Name, new string[] { "Id", "Nombre", "Email" });
                        foreach (Entity reciboR in recibos)
                        {
                            DataRow newRow = dtResults.NewRow();
                            newRow["Folio"] = results[0].getAttrValueByName("Folio") + (reciboR.getAttrValueByName("Folio") != "" ? (" / " + reciboR.getAttrValueByName("Folio")) : "");
                            newRow["Fecha"] = results[0].getAttrValueByName("FechaLiquidacion");
                            newRow["AseguradoraCve"] = results[0].getAttrValueByName("Aseguradora");
                            newRow["AseguradoraName"] = results[0].getAttrValueByName("clienteNombreCompleto");
                            newRow["PolizaCve"] = reciboR.getAttrValueByName("tramite");
                            newRow["PolizaName"] = reciboR.getAttrValueByName("tramitefolio");
                            newRow["ReciboNum"] = reciboR.getAttrValueByName("Numero");
                            newRow["Asegurado"] = reciboR.getAttrValueByName("clienteNombreCompleto") != "" ? reciboR.getAttrValueByName("clienteNombreCompleto") : reciboR.getAttrValueByName("clienteEndosoNombreCompleto");
                            newRow["Vigencia"] = reciboR.getAttrValueByName("Vencimiento");
                            newRow["ObsPago"] = reciboR.getAttrValueByName("ObservacionPago");
                            newRow["PrimaTotal"] = Convert.ToDouble(reciboR.getAttrValueByName("Total")==string.Empty?"0":reciboR.getAttrValueByName("Total"));
                            newRow["Comision"] = reciboR.getAttrValueByName("Comision");
                            newRow["NumCobranza"] = "";
                            newRow["TipoIngreso"] = getTipoIngreso(reciboR.getAttrValueByName("TipoIngreso").ToString());
                            newRow["SubRamo"] = reciboR.getAttrValueByName("SubRamoNombre") != "" ? reciboR.getAttrValueByName("SubRamoNombre") : reciboR.getAttrValueByName("SubRamoEndosoNombre");
                            newRow["AgenteName"] = reciboR.getAttrValueByName("AgenteNombre") != "" ? reciboR.getAttrValueByName("AgenteNombre") : reciboR.getAttrValueByName("AgenteEndosoNombre");
                            newRow["Nota"] = "";
                            newRow["Observaciones"] = results[0].getAttrValueByName("Observaciones"); ;

                            newRow["UserCve"] = Usuario.getAttrValueByName("Id");
                            newRow["UserName"] = Usuario.getAttrValueByName("Nombre");
                            newRow["UserEmail"] = Usuario.getAttrValueByName("Email");
                            newRow["UserRole"] = Usuario.getAttrValueByName("PuestoNombre");
                            dtResults.Rows.Add(newRow);
                        }
                    }
                }
                #endregion

                dataset.Tables.Add(dtResults);
                dsLiquidacion.Value = dataset.Tables[0];
                localReport.DataSources.Clear();
                localReport.DataSources.Add(dsLiquidacion);

                Warning[] warnings;
                string[] stream;
                string mimeType;
                string encoding;
                string fileNameExtension;
                const string deviceInfo = "<DeviceInfo>" +
                                          "  <OutputFormat>PDF</OutputFormat>" +
                                          "  <PageWidth>21.59cm</PageWidth>" +
                                          "  <PageHeight>27.94cm</PageHeight>" +
                                          "  <MarginTop>1cm</MarginTop>" +
                                          "  <MarginLeft>1cm</MarginLeft>" +
                                          "  <MarginRight>1cm</MarginRight>" +
                                          "  <MarginBottom>1cm</MarginBottom>" +
                                          "</DeviceInfo>";
                Byte[] rptResult = localReport.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out stream, out warnings);
                string Guid = System.Guid.NewGuid().ToString();
                System.IO.FileStream wFile;
                wFile = new FileStream(Server.MapPath("~/Reportes/Liquidaciones/" + Guid + ".pdf"), FileMode.Create);
                wFile.Write(rptResult, 0, rptResult.Length);
                wFile.Close();

                fileName = Guid + ".pdf";

                return Server.MapPath("~/Reportes/Liquidaciones/" + Guid + ".pdf");
            }

            return string.Empty;
        }

        private string getTipoIngreso(string TipoIngreso)
        {
            try
            {
                var EnumValue = (TipoIngresoEnum)int.Parse(TipoIngreso);
                return EnumValue.ToString();
            }
            catch (Exception)
            {
                return TipoIngresoEnum.NoEspecificado.ToString();
            }

        }

        private bool validaAplicacion(string _idTramite, string _idAdeguradora)
        {
            string cvePolizaTramite = string.Empty;
            string errMsg = string.Empty;
            Entity poliza = new Entity("poliza");
            poliza.Attributes.Add(new Models.Attribute("Aseguradora"));
            poliza.Keys.Add(new Key("id", _idTramite, "int"));

            List<Entity> results = SiteFunctions.GetValues(poliza, out errMsg);
            if (results.Count > 0)
            {
                cvePolizaTramite = results[0].getAttrValueByName("Aseguradora");
                if (cvePolizaTramite == _idAdeguradora)
                {
                    return true;
                }
                else
                {
                    showMessage("Los recibos adjuntos a una liquidación deben ser de la misma aseguradora, verifique y reintente.", true);
                    return false;
                }
            }
            else
            {
                Entity endoso = new Entity("endoso");
                endoso.Attributes.Add(new Models.Attribute("poliza"));
                endoso.Keys.Add(new Key("id", _idTramite, "int"));

                poliza = new Entity("poliza");
                poliza.Attributes.Add(new Models.Attribute("Aseguradora"));

                selectJoinEntity selJoin = new selectJoinEntity("poliza", 1, "id");
                JoinEntity joinEntity = new JoinEntity();
                joinEntity.JoinType = JoinType.Inner;
                joinEntity.selectJoinList.Add(selJoin);
                joinEntity.ChildEntity = poliza;

                endoso.ChildEntities.Add(joinEntity);
                results = SiteFunctions.GetValues(endoso, out errMsg);
                if (results.Count > 0)
                {
                    cvePolizaTramite = results[0].getAttrValueByName("polizaAseguradora");
                    if (cvePolizaTramite == _idAdeguradora)
                    {
                        return true;
                    }
                    else
                    {
                        showMessage("Los recibos adjuntos a una liquidación deben ser de la misma aseguradora, verifique y reintente.", true);
                        return false;
                    }
                }
                else
                {
                    showMessage("No sé encontró ningún Endoso o Póliza del recibo seleccionado", true);
                    return false;
                }
            }
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (txtEmailSend.Text != string.Empty)
            {
                string fileName = string.Empty;
                string url = printRpt(out fileName);
                StreamReader reader = new StreamReader(url);
                SiteFunctions siteFunct = new SiteFunctions();
                string body = File.ReadAllText(HttpContext.Current.Server.MapPath("HtmlTemplates/liquidacionEmailTemplate.html"));
                if (siteFunct.sendEmailWAttachtment(txtEmailSend.Text, "Liquidacion", body, reader.BaseStream, "liquidacion.pdf")) ;
                {
                    this.showMessage("Enviado correctamente", false);
                }
            }
        }

        protected void btnDescargar_Click(object sender, EventArgs e)
        {
            String csname1 = "downloadRpt";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            string fileName = string.Empty;
            string url = printRpt(out fileName);
            url = "Reportes/Liquidaciones/" + fileName;
            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> window.open('" + url + "'); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

       

        protected void BtnIntegPAg_Click(object sender, EventArgs e)
        {
            PaneName.Value = "IntegracionPago";
            string errmess = string.Empty;
            object id = LiqId.Value;
            bool ok = true;
            Dictionary<string, string> Poliza = new Dictionary<string, string>();
            if (fileUpload.HasFile)
            {
                string fileName = fileUpload.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        decimal totalPrima = 0;
                        decimal totComision = 0;
                        int NoIncisos = 0;
                        if (txtPoliza.Text.Trim() == string.Empty)
                        {
                            if (txtGrupo.Text.Trim() != string.Empty)
                            {
                                getPolizasfromReceipts(ref Poliza, id.ToString());
                                if (Poliza.Count == 0)
                                {
                                    showMessage("No se han seleccionado recibos para la Liquidación", true);
                                }
                            }
                            else
                            {
                                showMessage("No se ha seleccionado un grupo o póliza para la Liquidación", true);
                            }
                        }
                        else
                        {
                            Poliza.Add(txtPoliza.Text, "");
                        }
                        if (ok)
                        {
                            if (SiteFunctions.importLiq(fileUpload.FileContent, Poliza, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out totalPrima, out errmess, out totComision))
                            {

                                searchIntegraciones();
                                txtTotalPrima.Text = totalPrima.ToString();
                                txtTotalImporteComision.Text = totComision.ToString();

                                showMessage(String.Format("Carga Exitosa.", NoIncisos.ToString()), false);

                            }
                            else
                            {
                                showMessage(errmess, true);
                            }
                        }
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
            
        }

        private void getPolizasfromReceipts(ref Dictionary<string,string> Poliza,string  id)
        {
            Entity recibos = initRecibosSearch();

            recibos.Keys.Add(new Key("liquidacion", id, "int"));

            string errMsg = string.Empty;
            List<Entity> results = SiteFunctions.GetValues(recibos, out errMsg);

            foreach (Entity result in results)
            {
                if (result != null)
                {
                    if (!Poliza.ContainsKey(result.getAttrValueByName("Tramite")))
                    {
                        Poliza.Add(result.getAttrValueByName("Tramite"), "");
                    }
                }
            }
        }

        private void searchIntegraciones()
        {
            Entity IntegracionPago = new Entity("IntegracionPagoPaso");
            IntegracionPago.useAuditable = false;
            string errmess = string.Empty;
              IntegracionPago.Attributes.Add(new Models.Attribute("Id","","int"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Poliza"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Inciso"));
            IntegracionPago.Attributes.Add(new Models.Attribute("SecuenciaRecibo"));
            IntegracionPago.Attributes.Add(new Models.Attribute("NombreCliente"));
            IntegracionPago.Attributes.Add(new Models.Attribute("FormaPago"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Prima"));
            IntegracionPago.Attributes.Add(new Models.Attribute("ImporteComision"));
            IntegracionPago.Keys.Add(new Key("Liquidacion",LiqId.Value,"int"));
            List<Entity> ListEntities = SiteFunctions.GetValues(IntegracionPago, out errmess);

            DataTable dtResults = SiteFunctions.trnsformEntityToDT(ListEntities);
            GvIntegracionesPago.DataSource = dtResults;
            GvIntegracionesPago.DataBind();
            decimal totprima = 0M;
            decimal totcomision = 0M;

            foreach(DataRow dr in dtResults.Rows)
            {
                decimal prima = Convert.ToDecimal(dr["Prima"].ToString()==""?"0":dr["Prima"].ToString());
                decimal comision = Convert.ToDecimal(dr["ImporteComision"].ToString() == "" ? "0" : dr["ImporteComision"].ToString());

                totprima += prima;
                totcomision += comision;

            }
            txtTotalPrima.Text = totprima.ToString();
            txtTotalImporteComision.Text = totcomision.ToString();
        }

        protected void btnDeleteRecibos_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";

            foreach (GridViewRow row in gvRecibos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        seleccionado = true;
                        break;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirm_value"];
                
                if (confirmValue == "Si")
                {
                    PaneName.Value = "Recibos";
                    foreach (GridViewRow row in gvRecibos.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                string id = gvRecibos.Rows[row.RowIndex].Cells[2].Text;
                                string leadId = string.Empty;
                                Entity Recibo = new Entity("Recibo");
                                Recibo.Keys.Add(new Key("Id", id, "int64"));
                                Recibo.Attributes.Add(new Models.Attribute("Liquidacion", "null", "int"));
                                Recibo.Attributes.Add(new Models.Attribute("Estatus", "0", "int"));
                                SiteFunctions.SaveEntity(Recibo, Method.PUT, out errmess);

                            }
                        }
                    }

                    string Id = LiqId.Value;//.QueryString["id"];
                    searchLiqRecibos(Id);

                }

            }
            else
            {
                this.showMessage("Debe Seleccionar un recibo.", true);
            }
        }

        protected void btnDownloadInt_Click(object sender, EventArgs e)
        {
            Entity IntegracionPago = new Entity("IntegracionPagoPaso");
            string errmess = string.Empty;
            IntegracionPago.useAuditable = false;
            IntegracionPago.Attributes.Add(new Models.Attribute("Id"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Poliza"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Inciso"));
            IntegracionPago.Attributes.Add(new Models.Attribute("SecuenciaRecibo"));
            IntegracionPago.Attributes.Add(new Models.Attribute("NombreCliente"));
            IntegracionPago.Attributes.Add(new Models.Attribute("FormaPago"));
            IntegracionPago.Attributes.Add(new Models.Attribute("Prima"));
            IntegracionPago.Attributes.Add(new Models.Attribute("ImporteComision"));
            IntegracionPago.Keys.Add(new Key("Liquidacion", LiqId.Value, "int"));
            List<Entity> ListEntities = SiteFunctions.GetValues(IntegracionPago, out errmess);

            DataTable dtResults = SiteFunctions.trnsformEntityToDT(ListEntities);

            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook excelworkBook;
            Microsoft.Office.Interop.Excel.Worksheet excelSheet;
            Microsoft.Office.Interop.Excel.Range excelCellrange;

            // Start Excel and get Application object.  
            excel = new Microsoft.Office.Interop.Excel.Application();
            // for making Excel visible  
            excel.Visible = false;
            excel.DisplayAlerts = false;
            // Creation a new Workbook  
            excelworkBook = excel.Workbooks.Add(Type.Missing);
            // Workk sheet  
            excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelworkBook.ActiveSheet;
            excelSheet.Name = "Integracion Pagos";  
            int x = 1;
            int y=1;
            foreach(DataRow dr in dtResults.Rows)
            {
                y = 1;
                if(x==1)
                {
                    foreach(DataColumn dc in dr.Table.Columns)
                    {
                        if (!dc.ColumnName.Contains("Audit"))
                        {
                            excelSheet.Cells[x, y] = dc.ColumnName;
                            y++;
                        }
                    }
                    x++;
                    y = 1;
                }
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    excelSheet.Cells[x, y] = dr[dc.ColumnName];
                    y++;
                }
                x++;
                 
            }
            string fileName = LiqId.Value.ToString() + DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
            string ExcelFilePath = ConfigurationManager.AppSettings["filesFolder"].ToString() + @"\" + fileName; 
            if ( ExcelFilePath != null && ExcelFilePath != "")
            {
                try
                {
                    excelSheet.SaveAs(ExcelFilePath);
                    excel.Quit();
                    //Response.AddHeader("Content-Disposition",
                    //           "attachment; filename=" + fileName + ";");
                    //Response.ContentType = "application/vnd.ms-excel";
                    //Response.WriteFile(ExcelFilePath,false);
                    //Response.Flush();
                    ////response.End();
                    
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    ////HttpContext.Current.Response.SuppressContent = true;
                    Thread.Sleep(2000);
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AppendHeader("Content-Disposition", "Attachment; filename=\""+fileName+"\"");
                    Response.AppendHeader("Content-Length", File.ReadAllBytes(ExcelFilePath).Length.ToString());
                    Response.TransmitFile(ExcelFilePath);
                    Response.Flush();
                    Response.End();
                    
                }
                catch (Exception ex)
                {
                    showMessage("No se pudo exportar la información"+ ex.Message,true);
                }
            }
            
           
        }

        protected void btnDelIntegracion_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";

            foreach (GridViewRow row in GvIntegracionesPago.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        seleccionado = true;
                        break;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirmdelint_value"];

                if (confirmValue == "Si")
                {
                    PaneName.Value = "IntegracionPago";
                    foreach (GridViewRow row in GvIntegracionesPago.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                string id = GvIntegracionesPago.Rows[row.RowIndex].Cells[1].Text;
                                Entity IntegracionPago = new Entity("IntegracionPagoPaso");
                                IntegracionPago.Keys.Add(new Key("Id", id, "int"));
                                SiteFunctions.DeleteEntity(IntegracionPago, Method.POST, out errmess);

                            }
                        }
                    }

                    string Id = LiqId.Value;//.QueryString["id"];
                    searchIntegraciones();

                }

            }
            else
            {
                this.showMessage("Debe Seleccionar un registro.", true);
            }
        }

        
    }
}