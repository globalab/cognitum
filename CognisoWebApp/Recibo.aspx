﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Recibo.aspx.cs" Inherits="CognisoWebApp.Recibo" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Recibo</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <link href="/Content/cogniso.css" rel="stylesheet"/>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtCobertura]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtVencimiento]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtExpedicion]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(function () {
            $('[id*=txtFechaPagoComision]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(function () {
            $('[id*=txtFechaMaximaP]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        function copyvalue() {
            document.getElementById('<%=txtComisionPagadaML.ClientID%>').value = document.getElementById('<%=txtComisionPagada.ClientID%>').value;
        }
        function CalculateAmountsNeto() {
            try {
                debugger;
                var amount = 0;
                var total = 0;

                var prima = document.getElementById("<%=txtPrimaNeta.ClientID%>").value;
                var gastos = document.getElementById("<%=txtGastos.ClientID%>").value;
                var recargos = document.getElementById("<%=txtRecargo.ClientID%>").value;
                

                if (prima.length > 0) {
                    if (isNaN(prima)) {
                        alert("Prima acepta números unicamente !!");
                        return false;
                    }
                    else {
                        amount = amount + parseFloat(prima);
                    }
                }

                if (gastos.length > 0) {
                    if (isNaN(gastos)) {
                        alert("Gastos acepta números unicamente !!");
                        return false;
                    }
                    else {
                        amount = amount + parseFloat(gastos);
                    }
                }
                if (recargos.length > 0) {
                    if (isNaN(recargos)) {
                        alert("Recargos acepta números unicamente !!");
                        return false;
                    }
                    else {
                        var recargoAmount = parseFloat(recargos);
                        amount = amount + recargoAmount;
                    }
                }

                


                document.getElementById("<%=TotalNeto.ClientID%>").value = amount;

                var neto = document.getElementById("<%=TotalNeto.ClientID%>").value;
                SetImpuesto();
               


            } catch (e) {

            }


        }
        function SetImpuesto() {
            try {
                var idporcen = document.getElementById("<%=txtImpuesto.ClientID%>").value;
                var neto = document.getElementById("<%=TotalNeto.ClientID%>").value;
                $.ajax({
                    type: "POST",

                    url: '<%= ResolveUrl("Recibo.aspx/getAmountImpuesto") %>',
                    data: "{'idporcentaje':'" + idporcen + "', 'neto':'" + neto + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        document.getElementById("<%=txtImpuestoAmt.ClientID%>").value = msg.d;
                        var neto = document.getElementById("<%=TotalNeto.ClientID%>").value;
                        total = parseFloat(neto) + parseFloat(msg.d);
                        document.getElementById("<%=txtTotal.ClientID%>").value = total.toFixed(4);
                        //CalculateAmountsNeto();
                    },
                    error: function (msg) {
                        console.log(msg.d + "error");

                    }
                });

            } catch (e) {
                console.log(e.message);
            }
        }

        function generaCarta(idplantilla, tipoplantilla) {
            try {

                document.getElementById("<%=idplantilla.ClientID%>").value = idplantilla;


            } catch (e) {
                console.log(e.message);
            }
        }

    </script>
    
</head>  
<body>
    <form id="form1" runat="server">
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Recibo <%=ReciboId.Value %></asp:Label>
            </div>
           
            <div class="panel-heading">
                <div class="row">
                    <div class="col">
                
                        <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server"  OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" CausesValidation="true" />
                    
                    </div>
                    <div class="col">
                        <ul class="nav navbar-nav">
                            <li class="dropdown" id="Li1" runat="server">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cartas<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <asp:Repeater ID="rptCartas" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#ConfirmCartas" onclick="generaCarta(<%# Eval("Id") %>,<%# Eval("TipoPlantilla") %>)" ><%# Eval("Nombre") %><span class="caret"></span></a>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Bitacora-tab" data-toggle="tab" href="#Bitacora" role="tab" aria-controls="Bitacora" aria-selected="false">Bitacora</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="CargoTC-tab" data-toggle="tab" href="#Bitacora" role="tab" aria-controls="CargoTC" aria-selected="false">CargoTC</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">
                                 <div class="row">
                                     <div class="col">
                                        <asp:Label ID="lblSerie" runat="server" Text="Serie"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSerie" TextMode="Number" runat="server" CssClass="form-control" TabIndex="9" disabled>
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMoneda" runat="server" Text="Moneda"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMoneda" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                  </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNumero" runat="server" Text="Numero"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNumero" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valNumero" runat="server" ErrorMessage="Número" ForeColor="Red" ControlToValidate="txtNumero" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblPrimaNeta" runat="server" Text="Prima neta"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrimaNeta" runat="server" CssClass="form-control" TabIndex="9" onchange="CalculateAmountsNeto();">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="valPrima" runat="server" ErrorMessage="Prima" ForeColor="Red" ControlToValidate="txtPrimaNeta" Text="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat="server" id="txtPrimaNetaVal" controltovalidate="txtPrimaNeta" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTipoRecibo" runat="server" Text="Tipo de Recibo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtTipoRecibo" runat="server"  CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblGastos" runat="server" Text="Gastos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtGastos" runat="server"  CssClass="form-control" onchange="CalculateAmountsNeto();"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatus" runat="server"  CssClass="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblRecargo" runat="server" Text="Recargo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRecargo" onchange="CalculateAmountsNeto();" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtRecargoVal" controltovalidate="txtRecargo" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCobertura" runat="server" Text="Cobertura"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCobertura" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblImpuesto" runat="server" Text="Impuesto %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtImpuesto" CssClass="form-control" runat="server" TabIndex="9" onchange="SetImpuesto();">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valImpuesto" runat="server" ErrorMessage="Impuesto %" ForeColor="Red" ControlToValidate="txtImpuesto" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblVencimiento" runat="server" Text="Vencimiento"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVencimiento" runat="server" CssClass="form-group input-group" TabIndex="9" Enabled="false">

                                            </asp:TextBox>
                                            
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblImpuestosAmt" runat="server" Text="Impuesto importe"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtImpuestoAmt" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaMaximaP" runat="server" Text="Fecha máxima de pago"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaMaximaP" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valFechaMaxP" runat="server" ErrorMessage="Fecha máxima de pago" ForeColor="Red" ControlToValidate="txtFechaMaximaP" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                     <div class="col">
                                        <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBienAsegurado" runat="server" Text="Bien Asegurado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBienAsegurado" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator2" controltovalidate="txtComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                     <div class="col">
                                        <asp:Label ID="lblComision" runat="server" Text="Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComision" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator3" controltovalidate="txtComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTramite" runat="server" Text="Trámite"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTramite" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="RegularExpressionValidator1" controltovalidate="txtComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                     <div class="col">
                                        <asp:Label ID="lblComisionPagada" runat="server" Text="Comisión Pagada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComisionPagada" runat="server" CssClass="form-control" TabIndex="9" OnChange="copyvalue();">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtComisionVal" controltovalidate="txtComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblLiquidacion" runat="server" Text="Liquidación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtLiquidacion" CssClass="form-control" runat="server" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblComisionML" runat="server" Text="Comisión M.L"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComisionML" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblConciliacion" runat="server" Text="Conciliación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtConciliacion" CssClass="form-control" runat="server" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblComisionPagadaML" runat="server" Text="Comisión Pagada M.L"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComisionPagadaML" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                        <asp:Label ID="lblObservacion" runat="server" Text="Observación pago" ></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtObservacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                  </div>
                                
                                  <div class="col">
                                        <asp:Label ID="lblFechaPagoComision" runat="server" Text="Fecha Pago Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaPagoComision" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                   </div>   
                                   
                                </div>
                                
                                <div class="row">

                                    
                                    <div class="col">
                                        <asp:Label ID="lblTipoIngreso" runat="server" Text="Tipo Ingreso" ></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtTipoIngreso" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                
                                  <div class="col">
                                        <asp:Label ID="lblTCLiq" runat="server" Text="T.C. Liquidación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTCLiq" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>   
                                   </div>
                                
                                <div class="row">
                                    <div class="col" style="width:50%">
                                       
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTCAbono" runat="server" Text="T.C. Abono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTCAbono" runat="server" CssClass="form-group input-group" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>   
                                   
                                </div>
                                 
                           </div>
                           </div>     
                         <div class="tab-pane fade" id="Bitacora" role="tabpanel" aria-labelledby="Bitacora-tab">
                             <div class="container">
                                 <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="gvBitacora" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                             <a id="popupBitacora" href='#' data-image='Bitacora.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                                                    <asp:BoundField DataField="Observaciones" HeaderText="Observaciones" />
                                                    <asp:BoundField DataField="Usuario" HeaderText="Nombre" />
                                                </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                             </div>
                             </div>
                        <div class="tab-pane fade" id="CargoTC" role="tabpanel" aria-labelledby="CargoTC-tab">
                             <div class="container">
                                 <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="gvCargoTC" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                             <a id="popupcargoTC" href='#' data-image='CargoTC.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                    <asp:BoundField DataField="Caratula" HeaderText="Carátula" />
                                                    <asp:BoundField DataField="Poliza" HeaderText="Poliza" />
                                                    <asp:BoundField DataField="Importe" HeaderText="Importe" />
                                                    <asp:BoundField DataField="Respuesta" HeaderText="Respuesta" />
                                                    <asp:BoundField DataField="Rechazos" HeaderText="Rechazos" />
                                                    <asp:BoundField DataField="Accion" HeaderText="Accion" />
                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                                    <asp:BoundField DataField="FechaAdd" HeaderText="Fecha cargo" />
                                                </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                             </div>
                             </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                   
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="ReciboId" runat="server" />
                <asp:HiddenField ID="TotalNeto" runat="server" />
                <asp:HiddenField ID="idplantilla" runat="server" />
                <asp:HiddenField ID="idPoliza" runat="server" />
                 <div class="modal fade" id="ConfirmCartas" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ConfirmCartasLabel">Generar Cartas</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="containerRT22" runat="server">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblSendEmail" runat="server" Text="Enviar por correo electronico"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtEmailSend" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblSubject" runat="server" Text="Asunto"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtSubject" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Button ID="btnSendEmail" OnClick="btnSendEmail_Click" runat="server" Text="Enviar correo" />
                                            </div>
                                            <div class="col">
                                                <asp:Button ID="btnDescargar" OnClick="btnDescargar_Click" runat="server" Text="Descargar" />
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>  

