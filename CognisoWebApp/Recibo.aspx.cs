﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Recibo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                string polizaid = Request.QueryString["poliza"];
                string Id = Request.QueryString["Id"];
                Id = string.IsNullOrEmpty(Id) ? string.Empty : Id;
                initCatalogos(Id);
                if (!string.IsNullOrEmpty(Id))
                {
                    ReciboId.Value = Id;
                    populateRecibo(Id);
                    initCartas(Id);
                }
                else
                {
                    if (!string.IsNullOrEmpty(polizaid))
                    {
                        initReceiptTramite(polizaid);
                    }
                }
                
            }
        }

        private void initReceiptTramite(string polizaid)
        {
            idPoliza.Value = polizaid;
            string errmess = string.Empty;
            Entity Tramite = new Entity("Tramite");
            Tramite.Attributes.Add(new Models.Attribute("Ubicacion"));
            Tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaFin"));
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            
            Tramite.Keys.Add(new Key("Id",polizaid,"int64"));
            List<Entity> listResult = SiteFunctions.GetValues(Tramite, out errmess);

            Entity usraudit = new Entity("Usuario");
            usraudit.Attributes.Add(new Models.Attribute("Id"));
            usraudit.Attributes.Add(new Models.Attribute("Ubicacion"));
            usraudit.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            usraudit.Keys.Add(new Key("Username", Context.User.Identity.Name, "string"));
            
            
            if(listResult.Count > 0)
            {
                txtUbicacion.Text = listResult[0].getAttrValueByName("Ubicacion");
                txtVencimiento.Text = listResult[0].getAttrValueByName("VigenciaFin");
                txtTramite.Text = listResult[0].getAttrValueByName("Folio"); ;
                
            }
            
            txtFechaAlta.Text = DateTime.Now.ToShortDateString();
            
        }
        [WebMethod]
        public static string getAmountImpuesto(string idporcentaje, string neto)
        {
            string result = string.Empty;

            Entity Impuesto = new Entity("Impuesto");
            Impuesto.Keys.Add(new Models.Key("id", idporcentaje, "int"));
            Impuesto.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            string errMsg = string.Empty;
            List<Entity> resultado = SiteFunctions.GetValues(Impuesto, out errMsg);

            if (resultado.Count > 0)
            {
                Entity impuesRes = resultado[0];

                var porcentaje = impuesRes.getAttrValueByName("Porcentaje");
                if (neto != "")
                {
                    float primaval = float.Parse(neto);
                    result = ((primaval * float.Parse(porcentaje)) / 100).ToString();
                }

            }

            return result;
        }
        private void initCatalogos(string Id = "")
        {
            Array itemValues = System.Enum.GetValues(typeof(EstatusReciboEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusReciboEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }

            Entity tax = new Entity("impuesto");
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Porcentaje"));
            tax.useAuditable = false;
            string errmess = string.Empty;
            List<Entity> listResult = SiteFunctions.GetValues(tax, out errmess);
            txtImpuesto.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtImpuesto.DataValueField = "Id";
            txtImpuesto.DataTextField = "Nombre";
            txtImpuesto.DataBind();

            itemValues = System.Enum.GetValues(typeof(TipoIngresoEnum));
            itemNames = System.Enum.GetNames(typeof(TipoIngresoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtTipoIngreso.Items.Add(item);
            }




            itemValues = System.Enum.GetValues(typeof(TipoReciboEnum));
            itemNames = System.Enum.GetNames(typeof(TipoReciboEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtTipoRecibo.Items.Add(item);
            }
            Entity moneda = new Entity("Moneda");
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            //moneda.Keys.Add(new Key("Id", recibo.getAttrValueByName("Moneda"), "int64"));
            moneda.useAuditable = false;
            listResult = SiteFunctions.GetValues(moneda, out errmess);
            txtMoneda.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtMoneda.DataTextField = "Nombre";
            txtMoneda.DataValueField = "Id";
            txtMoneda.DataBind();
            txtMoneda.SelectedValue = "2187";
            if (Id != null || Id != "")
            {
                Entity Bitacora = new Entity("BitacoraRecibo");
                Bitacora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Bitacora.Attributes.Add(new CognisoWebApp.Models.Attribute("Fecha"));
                Bitacora.Attributes.Add(new CognisoWebApp.Models.Attribute("Observaciones"));
                Bitacora.Attributes.Add(new CognisoWebApp.Models.Attribute("Usuario"));
                Bitacora.Attributes.Add(new CognisoWebApp.Models.Attribute("Recibo"));
                Bitacora.Keys.Add(new CognisoWebApp.Models.Key("Recibo", Id));

                Entity usr = new Entity("Usuario");
                usr.Attributes.Add(new Models.Attribute("Nombre"));
                
                selectJoinEntity selJoin = new selectJoinEntity("Usuario", 1, "id");
                JoinEntity joinEntityCert = new JoinEntity();
                joinEntityCert.ChildEntity = usr;
                joinEntityCert.selectJoinList = new List<selectJoinEntity>();
                joinEntityCert.selectJoinList.Add(selJoin);
                joinEntityCert.JoinType = JoinType.Inner;
                Bitacora.ChildEntities.Add(joinEntityCert);

                Bitacora.useAuditable = false;
                errmess = string.Empty;
                listResult = SiteFunctions.GetValues(Bitacora, out errmess);
                DataTable dtRecibo = SiteFunctions.trnsformEntityToDT(listResult);
                gvBitacora.DataSource = dtRecibo;
                gvBitacora.DataBind();

                Entity CargosTC = new Entity("CargoTC");
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Caratula"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Poliza"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Importe"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Respuesta"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Rechazos"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Accion"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus"));
                CargosTC.Attributes.Add(new CognisoWebApp.Models.Attribute("FechaAdd"));

                CargosTC.Keys.Add(new CognisoWebApp.Models.Key("Recibo", Id));

                CargosTC.ChildEntities.Add(joinEntityCert);

                CargosTC.useAuditable = false;
                errmess = string.Empty;
                listResult = SiteFunctions.GetValues(CargosTC, out errmess);
                DataTable dtCargoTC= SiteFunctions.trnsformEntityToDT(listResult);
                gvBitacora.DataSource = dtRecibo;
                gvBitacora.DataBind();
            }
            
            
        }

        private void populateRecibo(string Id)
        {
            string errMsg = string.Empty;
            Entity recibo = new Entity("Recibo");
            recibo.Attributes.Add(new Models.Attribute("Id"));
            recibo.Attributes.Add(new Models.Attribute("Serie"));
            recibo.Attributes.Add(new Models.Attribute("Numero"));
            recibo.Attributes.Add(new Models.Attribute("Tipo"));
            recibo.Attributes.Add(new Models.Attribute("Estatus"));
            recibo.Attributes.Add(new Models.Attribute("Cobertura"));
            recibo.Attributes.Add(new Models.Attribute("Vencimiento"));
            recibo.Attributes.Add(new Models.Attribute("Tramite"));
            recibo.Attributes.Add(new Models.Attribute("Liquidacion"));
            recibo.Attributes.Add(new Models.Attribute("Conciliacion"));
            recibo.Attributes.Add(new Models.Attribute("ObservacionPago"));
            recibo.Attributes.Add(new Models.Attribute("TipoIngreso"));
            recibo.Attributes.Add(new Models.Attribute("Moneda"));
            recibo.Attributes.Add(new Models.Attribute("PrimaNeta"));
            recibo.Attributes.Add(new Models.Attribute("Gastos"));
            recibo.Attributes.Add(new Models.Attribute("Recargo"));
            recibo.Attributes.Add(new Models.Attribute("Impuesto"));
            recibo.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            recibo.Attributes.Add(new Models.Attribute("Total"));
            recibo.Attributes.Add(new Models.Attribute("Comision"));
            recibo.Attributes.Add(new Models.Attribute("ComisionPagada"));
            recibo.Attributes.Add(new Models.Attribute("FechaPagoComision"));
            recibo.Attributes.Add(new Models.Attribute("TipoCambio"));
            recibo.Attributes.Add(new Models.Attribute("TipoCambioAbono"));
            recibo.Attributes.Add(new Models.Attribute("Inciso"));


            List<Entity> results = new List<Entity>();
            if (Id != string.Empty)
            {
                recibo.Keys.Add(new Key("id", Id, "int"));
            }
            results = SiteFunctions.GetValues(recibo, out errMsg);
            if (results.Count > 0)
            {
                recibo = results[0];
                txtSerie.Text = recibo.getAttrValueByName("Serie").ToString().Trim();
                txtNumero.Text = recibo.getAttrValueByName("Numero").ToString().Trim();
                txtTipoRecibo.SelectedValue = recibo.getAttrValueByName("Tipo").ToString().Trim();
                txtPrimaNeta.Text = recibo.getAttrValueByName("PrimaNeta").ToString();
                txtGastos.Text = recibo.getAttrValueByName("Gastos").ToString().Trim();
                txtEstatus.SelectedValue = recibo.getAttrValueByName("Estatus").ToString();
                txtRecargo.Text = recibo.getAttrValueByName("Recargo").ToString();
                txtCobertura.Text = recibo.getAttrValueByName("Cobertura").ToString();
                txtImpuesto.SelectedValue = recibo.getAttrValueByName("Impuesto").ToString();
                txtVencimiento.Text = recibo.getAttrValueByName("Vencimiento").ToString();
                txtImpuestoAmt.Text = recibo.getAttrValueByName("ImpuestoImporte").ToString();
                txtFechaMaximaP.Text = txtVencimiento.Text;
                txtTotal.Text = recibo.getAttrValueByName("Total").ToString();
                //txtBienAsegurado.Text 
                txtComision.Text = recibo.getAttrValueByName("Comision").ToString();
                idPoliza.Value = recibo.getAttrValueByName("Tramite").ToString();
                txtComisionPagada.Text = recibo.getAttrValueByName("ComisionPagada").ToString();
                txtLiquidacion.Text = recibo.getAttrValueByName("Liquidacion").ToString();
                decimal comision = 0M;
                decimal TipoCambio = 0M;
                decimal.TryParse(recibo.getAttrValueByName("Comision"), out comision);
                decimal.TryParse(recibo.getAttrValueByName("TipoCambio"), out TipoCambio);
                txtComisionML.Text = (comision * TipoCambio).ToString();
                txtConciliacion.Text = recibo.getAttrValueByName("Conciliacion").ToString();
                decimal.TryParse(recibo.getAttrValueByName("ComisionPagada"),out comision);
                decimal.TryParse(recibo.getAttrValueByName("TipoCambioAbono"),out TipoCambio);
                txtComisionPagadaML.Text = (comision * TipoCambio).ToString();
                txtObservacion.Text = recibo.getAttrValueByName("ObservacionPago").ToString();
                txtFechaPagoComision.Text = recibo.getAttrValueByName("FechaPagoComision").ToString();
                txtTipoIngreso.SelectedValue = recibo.getAttrValueByName("TipoIngreso").ToString();
                txtTCLiq.Text = recibo.getAttrValueByName("TipoCambio").ToString();
                txtTCAbono.Text = recibo.getAttrValueByName("TipoCambioAbono").ToString();
                txtMoneda.SelectedValue = recibo.getAttrValueByName("Moneda");
                if (recibo.getAttrValueByName("Inciso") != string.Empty)
                {
                    Entity BienAsegurado = new Entity("BienAsegurado");
                    BienAsegurado.Attributes.Add(new Models.Attribute("Inciso"));
                    BienAsegurado.Keys.Add(new Key("Id", recibo.getAttrValueByName("Inciso"), "int"));
                    BienAsegurado = SiteFunctions.GetEntity(BienAsegurado, out errMsg);
                    if(errMsg ==string.Empty)
                    {
                        Entity Auto = new Entity("Auto");
                        Auto.Attributes.Add(new Models.Attribute("Version"));
                        Auto.Keys.Add(new Key("Id", recibo.getAttrValueByName("Inciso"), "int"));
                        Auto = SiteFunctions.GetEntity(Auto, out errMsg);

                        if(errMsg == string.Empty)
                        {
                            txtBienAsegurado.Text = BienAsegurado.getAttrValueByName("Inciso") + "-" + Auto.getAttrValueByName("Version");
                        }
                    }
                }
                if (txtTramite.Text == string.Empty)
                {
                    Entity Tramite = new Entity("Tramite");
                    Tramite.Attributes.Add(new Models.Attribute("Folio"));

                    Tramite.Keys.Add(new Key("Id", idPoliza.Value, "int64"));
                    Tramite  = SiteFunctions.GetEntity(Tramite, out errMsg);
                    if (Tramite != null)
                    {
                        txtTramite.Text = "Folio: " + Tramite.getAttrValueByName("Folio");
                    }
                }

                DateTime fechaaudit = Convert.ToDateTime(recibo.getAttrValueByName("Audit_FechaAdd").ToString());
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(recibo.getAttrValueByName("Audit_FechaUMod").ToString());
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", recibo.getAttrValueByName("Audit_UsuarioAdd").ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", recibo.getAttrValueByName("Audit_UsuarioUMod").ToString(), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }

                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre"));
                Ubicacion.Keys.Add(new Models.Key("id", recibo.getAttrValueByName("Ubicacion"), "int"));
                errMsg = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(Ubicacion, out errMsg);
                if (listResult.Count > 0)
                {
                    txtUbicacion.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                Entity CB = new Entity("CentroDeBeneficio");
                CB.Attributes.Add(new Models.Attribute("Nombre"));
                CB.Keys.Add(new Models.Key("id", recibo.getAttrValueByName("CentroDeBeneficio"), "int"));
                errMsg = string.Empty;
                listResult = SiteFunctions.GetValues(CB, out errMsg);
                if (listResult.Count > 0)
                {
                    txtCentroBeneficio.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                txtId.Text = ReciboId.Value.ToString();

            }

        }

        
        
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            string errmsg;
            Auditable audit = new Auditable();
            object Id = Request.QueryString["id"];
            RestSharp.Method method = RestSharp.Method.POST;

            int ids = 0;
            int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
            string ubicacion = ids.ToString();
            ids = 0;
            int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
            string centroBen = ids.ToString();
            DateTime fechapago;
            DateTime? fechapagonull = null;
            
            
            if(DateTime.TryParse(txtFechaPagoComision.Text,out fechapago))
            {
                fechapagonull = fechapago;
            }

            if (txtPrimaNeta.Text != "")
            {

                if (!saveRecibo(txtComision.Text, txtGastos.Text == "" ? "0" : txtGastos.Text, DateTime.Parse(txtFechaAlta.Text), DateTime.Parse(txtVencimiento.Text),
                    txtImpuestoAmt.Text, txtNumero.Text, txtPrimaNeta.Text, txtSerie.Text, txtMoneda.SelectedValue, txtImpuesto.Text,
                    idPoliza.Value, ubicacion, centroBen, SiteFunctions.getuserid(Context.User.Identity.Name, false),
                    txtTCLiq.Text, txtTCAbono.Text, Id, txtComisionPagada.Text, txtComisionML.Text, "", txtTipoIngreso.SelectedValue, fechapagonull, txtObservacion.Text, out errmsg))
                {
                    showMessage(errmsg, true);
                }
                else
                {

                    showMessage("Recibo guardado", false);
                }
            }
        }


        public bool saveRecibo(string _commission, string _expense, DateTime _dateInit, DateTime _dueDateTicket, 
            string _taxAmt, string _ticketNum, string _prima, string _serie, string _curyId, string _taxId, 
            string _poliza, string _location, string _benefitCenter, string _owner,
            string _curyRate, string _curyRateAbono, object recordId, string _paymCommission, string _cxcCommission, string _charge,string TipoIngreso,DateTime? FechaPagoComision,string ObservacionesPago, out string errmsg)
        {
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(_owner, out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;

            Entity recibo = new Entity("Recibo");
            recibo.auditable = audi;
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Permiso", "2", "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Tipo", "0", "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus", "0", "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Ubicacion", _location.ToString(), "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("CentroDeBeneficio", _benefitCenter.ToString(), "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Propietario", _owner.ToString(), "int"));

            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Comision", _commission.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Recargo", _charge, "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Comisioncobranza", _cxcCommission, "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("comisionPagada", _paymCommission, "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Cobertura", _dueDateTicket.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Expedicion", _dateInit.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Vencimiento", _dueDateTicket.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Gastos", _expense.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("ImpuestoImporte", _taxAmt.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Numero", _ticketNum, "string"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("PrimaNeta", _prima.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Serie", _serie.ToString(), "int"));
            decimal total = decimal.Parse(_prima) + decimal.Parse(_expense) + decimal.Parse(_taxAmt);
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Total", total.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Concepto", _ticketNum, "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Moneda", _curyId.ToString(), "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Impuesto", _taxId.ToString(), "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("TipoCambio", _curyRate.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("TipoCambioAbono", _curyRateAbono.ToString(), "float"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("Tramite", _poliza.ToString(), "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("TipoIngreso", TipoIngreso, "int"));
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("ObservacionPago", ObservacionesPago, "string"));
            if(FechaPagoComision != null)
            recibo.Attributes.Add(new CognisoWebApp.Models.Attribute("FechaPagoComision", FechaPagoComision.Value.ToShortDateString(), "datetime"));
            errmsg = string.Empty;

            object Id = recordId;
            RestSharp.Method method = RestSharp.Method.POST;

            if (Id != null)
            {
                recibo.Keys.Add(new Key("Id", Id.ToString(), "int"));
                method = RestSharp.Method.PUT;
            }

            bool ok =  SiteFunctions.SaveEntity(recibo, method, out errmsg);

            if (ok)
            {
                initCartas(errmsg);

            }

            return ok;
            
                
            
        }


        

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                 cstext1.Append("script>");
                
                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void txtComisionPagada_TextChanged(object sender, EventArgs e)
        {
            txtComisionPagadaML.Text = txtComisionPagada.Text;
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (txtEmailSend.Text != "" && txtSubject.Text != "")
            {
                string HTML = generaCartaHTML();
                HTML = HTML.Replace("src=\"imgs/", "src=\"");
                SiteFunctions sf = new SiteFunctions();
                sf.sendEmail(txtEmailSend.Text, txtSubject.Text, HTML, Server.MapPath("~/imgs/cabecera.jpg"));
            }
        }

        protected void btnDescargar_Click(object sender, EventArgs e)
        {
            string HTML = generaCartaHTML();
            HTML = HTML.Replace("src=\"", "src=\"" + ConfigurationManager.AppSettings["APPUrl"].ToString());
            string Guid = System.Guid.NewGuid().ToString();
            string _filename = Server.MapPath("~/Reportes/Recibos/" + Guid + ".pdf");


            var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(HTML, PdfSharp.PageSize.A4);
            pdf.Save(_filename);

            String csname1 = "downloadRpt";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            string url = "Reportes/Recibos/" + Guid + ".pdf";
            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> window.open('" + url + "'); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }        
        }

        private void initCartas(string id)
        {
            List<Entity> plantillaspol = SearchPlantillas.ObtienePlantillasRecibo();
            DataTable dt = SiteFunctions.trnsformEntityToDT(plantillaspol);
            rptCartas.DataSource = dt;
            rptCartas.DataBind();
        }

        public string generaCartaHTML()
        {
            int idrecibo = 0;
            int plantilla = 0;
            int.TryParse(ReciboId.Value, out idrecibo);
            int.TryParse(idplantilla.Value, out plantilla);
            CartasFunctions gencarta = new CartasFunctions();
            string carta = string.Empty;
            carta = gencarta.GeneraCartaRecibo(idrecibo, plantilla, this.Page.User.Identity.Name, idPoliza.Value);
            return carta;
        }
    }
}