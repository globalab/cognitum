﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="searchRecibosLiq.aspx.cs" Inherits="CognisoWebApp.searchRecibosLiq" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Seleccionar recibos</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        //Reference of the GridView. 
        var TargetBaseControl = null;
        //Total no of checkboxes in a particular column inside the GridView.
        var CheckBoxes;
        //Total no of checked checkboxes in a particular column inside the GridView.
        var CheckedCheckBoxes;
        //Array of selected item's Ids.
        var SelectedItems;
        //Hidden field that wil contain string of selected item's Ids separated by '|'.
        var SelectedValues;
        
        window.onload = function()
        {
            //Get reference of the GridView. 
            try
            {
                TargetBaseControl = document.getElementById('<%= this.gvSearchRecibo.ClientID %>');
            }
            catch(err)
            {
                TargetBaseControl = null;
            }
            
            //Get total no of checkboxes in a particular column inside the GridView.
            try
            {
                CheckBoxes = parseInt('<%= this.gvSearchRecibo.Rows.Count %>');
            }
            catch(err)
            {
                CheckBoxes = 0;
            }
            
            //Get total no of checked checkboxes in a particular column inside the GridView.
            CheckedCheckBoxes = 0;
            
            //Get hidden field that wil contain string of selected item's Ids separated by '|'.
            SelectedValues = document.getElementById('<%= this.hdnFldSelectedValues.ClientID %>');
            
            //Get an array of selected item's Ids.
            if(SelectedValues.value == '')
                SelectedItems = new Array();
            else
                SelectedItems = SelectedValues.value.split('|');
                
            //Restore selected CheckBoxes' states.
            if(TargetBaseControl != null)
                RestoreState();
        }
        
        function HeaderClick(CheckBox)
        {            
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName('input');
            
            //Checked/Unchecked all the checkBoxes in side the GridView & modify selected items array.
            for(var n = 0; n < Inputs.length; ++n)
                if(Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf('chkBxSelect',0) >= 0)
                {
                    Inputs[n].checked = CheckBox.checked;
                    if(CheckBox.checked)
                        SelectedItems.push(document.getElementById(Inputs[n].id.replace('chkBxSelect','hdnFldId')).value);
                    else
                        DeleteItem(document.getElementById(Inputs[n].id.replace('chkBxSelect','hdnFldId')).value);
                }
                        
            //Update Selected Values. 
            SelectedValues.value = SelectedItems.join('|');
                        
            //Reset Counter
            CheckedCheckBoxes = CheckBox.checked ? CheckBoxes : 0;
        }
        
        function ChildClick(CheckBox, HCheckBox, Id)
        {               
            //Modifiy Counter;            
            if(CheckBox.checked && CheckedCheckBoxes < CheckBoxes)
                CheckedCheckBoxes++;
            else if(CheckedCheckBoxes > 0) 
                CheckedCheckBoxes--;
                
            //Change state of the header CheckBox.
            if(CheckedCheckBoxes < CheckBoxes)
                HCheckBox.checked = false;
            else if(CheckedCheckBoxes == CheckBoxes)
                HCheckBox.checked = true;
                
            //Modify selected items array.
            if(CheckBox.checked)
                SelectedItems.push(Id);
            else
                DeleteItem(Id);
                
            //Update Selected Values. 
            SelectedValues.value = SelectedItems.join('|');
        }   
        
        function RestoreState()
        {
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName('input');
            
            //Header CheckBox
            var HCheckBox = null;
            
            //Restore previous state of the all checkBoxes in side the GridView.
            for(var n = 0; n < Inputs.length; ++n)
                if(Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf('chkBxSelect',0) >= 0)
                    if(IsItemExists(document.getElementById(Inputs[n].id.replace('chkBxSelect','hdnFldId')).value) > -1)
                    {
                        Inputs[n].checked = true;          
                        CheckedCheckBoxes++;      
                    }
                    else
                        Inputs[n].checked = false;   
                else if(Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf('chkBxHeader',0) >= 0)	
                    HCheckBox = Inputs[n];  
                    
            //Change state of the header CheckBox.
            if(CheckedCheckBoxes < CheckBoxes)
                HCheckBox.checked = false;
            else if(CheckedCheckBoxes == CheckBoxes)
                HCheckBox.checked = true;  
        }
        
        function DeleteItem(Text)
	    {
	        var n = IsItemExists(Text);
	        if( n > -1)
	            SelectedItems.splice(n,1);
	    }
        
        function IsItemExists(Text)
	    {
	        for(var n = 0; n < SelectedItems.length; ++n)
	            if(SelectedItems[n] == Text)
	                return n;
    	            
	        return -1;  
	    }     
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Seleccionar recibos a liquidar</asp:Label>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div id="accordiontable" runat="server">
                    <div id="myTabContent">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchRecibos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="BtnSearchRecibos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="BtnSearchRecibos_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvSearchRecibo" AllowPaging="true" OnPageIndexChanging="gvSearchRecibo_PageIndexChanging" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server" OnRowDataBound="gvSearchRecibo_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkBxSelect" runat="server" />
                                                                <asp:HiddenField ID="hdnFldId" runat="server" Value='<%# Eval("Id") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkBxHeader" onclick="javascript:HeaderClick(this);" runat="server" />
                                                    </HeaderTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="tramiteFolio" HeaderText="Tramite" />
                                                    <asp:BoundField DataField="BienAseguradoIdentificador" HeaderText="Identificador" />
                                                    <asp:BoundField DataField="numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="Cobertura" HeaderText="Cobertura" />
                                                    <asp:BoundField DataField="Vencimiento" HeaderText="Vencimiento" />
                                                    <asp:BoundField DataField="Total" HeaderText="Total" />
                                                    <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                                                </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Button ID="BtnAceptar" Text="Agregar recibos" runat="server" OnClick="BtnAceptar_Click" />
                                    </div>
                                </div>
                            </div>     
                   </div>     
                </div>     
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="taxPrc" runat="server" />
                <asp:HiddenField ID="hdnFldSelectedValues" runat="server" />
            </div>
        </div>
  </form>
</body>
</html>
