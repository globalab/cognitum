﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Ramo.aspx.cs" Inherits="CognisoWebApp.Ramo" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Ramo</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script lang="javascript" type="text/javascript">
    var submit = 0;
    function CheckIsRepeat() {
        if (++submit > 1) {
           return false;
        }
    }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
     
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Ramo</asp:Label>
            </div>
           
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                    <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="Ramo-tab" data-toggle="tab" href="#Ramo" role="tab" aria-controls="Ramo" aria-selected="false">Datos Ramo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="SubRamo-tab" data-toggle="tab" href="#Subramo" role="tab" aria-controls="SubRamo" aria-selected="false">Datos Sub Ramo</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Ramo" role="tabpanel" aria-labelledby="Ramo-tab">
                            <div class="container">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblClave" runat="server" Text="Clave"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtClave" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtNombre" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblXtype" runat="server" Text="XType"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtXType" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDivisionOp" runat="server" Text="División Operativa"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtDivisionOp" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblIdentificadorSAP" runat="server" Text="Identificación SAP"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtIdentificadorSAP" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                           </div>
                           </div> 
                        <div class="tab-pane fade show active" id="Subramo" role="tabpanel" aria-labelledby="Subramo-tab">
                            <div class="container">
                                <div class="row">
                                    
                                    <a data-image='Subramo.aspx?popup=1&ramoid=<%=RamoId.Value %>'  onclick="openpopup('RamoId.aspx?popup=1&ramoid=<%=RamoId.Value %>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVSubRamoList" runat="server" OnPageIndexChanging="GVSubRamoList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a href='#' data-image='Subramo.aspx?popup=1&ramoid==<%# Eval("Ramo") %>&id=<%# Eval("Clave") %>' onclick="openpopup('Telefono.aspx?popup=1&ramoid==<%# Eval("Ramo") %>&id=<%# Eval("Clave") %>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Flotilla" HeaderText="Flotilla" />
                                    <asp:BoundField DataField="Ramo" Visible="false" />
                            
                                </Columns>
                            </asp:GridView>
                                    </div>
                                </div>
                            
                         </div>
                           </div>     
                      
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                           
                          </div>
                      </div> 
                   
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="RamoId" runat="server" />

            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>  

