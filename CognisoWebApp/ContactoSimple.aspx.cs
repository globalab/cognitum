﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class ContactoSimple : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                object clientid = Request.QueryString["custid"];
                object created = Request.QueryString["created"];
                if (clientid != null)
                {
                    CId.Value = clientid.ToString();
                }
                showPanels();
                initcatalog();
                object id = Request.QueryString["id"];

                if (id != null)
                {
                    ContactId.Value = id.ToString();
                    this.searchContact(id.ToString());
                    
                }
                if (created != null)
                {
                    this.showMessage("Se guardo correctamente la dirección", false);
                }
                
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);

            }
        }

        private void initcatalog()
        {
            string errmess = string.Empty;
            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.Keys.Add(new Key("Id", CId.Value, "int64"));
            Cliente.useAuditable = false;

            List<Entity> listResult = SiteFunctions.GetValues(Cliente, out errmess);
            if (listResult.Count > 0)
            {
                txtCliente.Text = listResult[0].getAttrValueByName("NombreCompleto").ToString();
            }
        }
       
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveContact();
        }

        private void searchContact(string _id)
        {
            string errMsg = string.Empty, idCliente = string.Empty,idUbicacion = string.Empty,idCentroBeneficio = string.Empty,idPropietario = string.Empty;

            Entity Contacto = new Entity("ContactoSimple");
            Contacto.useAuditable = true;
            Contacto.Keys.Add(new Models.Key("id", _id, "int64"));

            List<Entity> dirResult = SiteFunctions.GetValues(Contacto, out errMsg);
            if (dirResult.Count > 0)
            {
                Entity cont = dirResult[0];
                idCliente = cont.getAttrValueByName("Cliente");
                               
                txtNombreCA.Text = cont.getAttrValueByName("NombreCompleto");
                txtEmailCA.Text = cont.getAttrValueByName("Email");
                
                try
                {

                    DateTime fechaaudit = Convert.ToDateTime(cont.getAttrValueByName("FechaAdd"));

                    txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                    fechaaudit = Convert.ToDateTime(cont.getAttrValueByName("FechaUMod"));
                    txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch (Exception)
                {

                }
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", cont.getAttrValueByName("UsuarioAdd"), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", cont.getAttrValueByName("UsuarioUMod"), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtId.Text = _id;
                CId.Value = idCliente;
            }

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto"));
            cliente.Keys.Add(new Models.Key("id", idCliente, "int"));
            string errmess = string.Empty;
            List<Entity> listResult = SiteFunctions.GetValues(cliente, out errmess);
            if (listResult.Count > 0)
            {
                txtCliente.Text = listResult[0].getAttrValueByName("NombreCompleto").ToString();
            }

            Entity Owner = new Entity("Usuario");
            Owner.Attributes.Add(new Models.Attribute("Nombre"));
            Owner.Keys.Add(new Models.Key("id", idPropietario, "int"));
            errmess = string.Empty;
            listResult = SiteFunctions.GetValues(Owner, out errmess);
            if (listResult.Count > 0)
            {
                txtPropietario.Text = listResult[0].getAttrValueByName("Nombre").ToString();
            }
            loadTel(_id);
        }

     
        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
        }

        private void loadTel(string id)
        {
            string errmess = string.Empty;
            Entity Tel = new Entity("TelefonoContactoSimple");
            Tel.useAuditable = false;

            Tel.Keys.Add(new Models.Key("Contacto", id, "int64"));

            List<Entity> retTel = SiteFunctions.GetValues(Tel, out errmess);
            DataTable dt = new DataTable();
            dt = SiteFunctions.trnsformEntityToDT(retTel);
            GVTelList.DataSource = dt;
            GVTelList.DataBind();
        }

        private void SaveContact()
        {

            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            Entity Contact = new Entity("ContactoSimple");
            Auditable audi = new Auditable();
            audi.userId = userid;
            audi.opDate = DateTime.Now;

            string ownerid = string.Empty,CentroBeneficio = string.Empty,Ubicacion = string.Empty;
            if (ContactId.Value.Trim() == string.Empty)
            {
                ownerid = userid.ToString();
                int ids = 0;
                int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
                Ubicacion = ids.ToString();
                ids = 0;
                int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
                
                CentroBeneficio = ids.ToString();
            }
            else
            {
                ownerid = txtPropietario.Text.Trim();
                CentroBeneficio = txtCentroBeneficio.Text.Trim();
                Ubicacion = txtUbicacion.Text.Trim();

            }
            Contact.Attributes.Add(new Models.Attribute("Cliente", CId.Value, "int"));
            Contact.Attributes.Add(new Models.Attribute("NombreCompleto", txtNombreCA.Text, "string"));
            Contact.Attributes.Add(new Models.Attribute("Email", txtEmailCA.Text.Trim(), "string"));
                

            Contact.useAuditable = true;

           string errmess = "";
           object id = ContactId.Value == "" ?null:ContactId.Value;
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                Contact.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);

            }
            Contact.auditable = audi;
            if (SiteFunctions.SaveEntity(Contact, method, out errmess))
            {
                showMessage("Se guardo correctamente el contacto", false);
                if(method == Method.PUT)
                {
                    errmess = ContactId.Value;
                }
                Session["retval"] = errmess;
                ContactId.Value = errmess;
            }
            else
            {
                showMessage(errmess, true);
            }
        }

               
        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        

        protected void GVTelList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Telefono";
            GVTelList.PageIndex = e.NewPageIndex;
            GVTelList.DataBind();
            object _id = Request.QueryString["id"];
            if (_id != null)
            {
                loadTel(_id.ToString());
            }
        }

        protected void reloadCont_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Telefonos";
            string id = ContactId.Value;
            loadTel(id);
            Session["retval"] = id;
        }

        

       
    }
}