﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Endoso.aspx.cs" Inherits="CognisoWebApp.Endoso" ValidateRequest="false" %>

<!DOCTYPE html>


<html lang="en">
<head>
    <title>Recibo</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" />
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script src="Scripts/tinymce/tinymce.min.js"></script>
    <link href="/Content/jquery-ui.css" rel="stylesheet" />
    <link href="/Content/cogniso.css" rel="stylesheet"/>
    <script type="text/javascript">
       

        tinymce.init({
            selector: 'textarea',
            language: 'es_MX',
            language_url: '/Scripts/tinymce/langs/es_MX.js',
            plugins: 'print preview fullpage paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true,
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tiny.cloud/css/codepen.min.css'
            ],
            link_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
              { title: 'None', value: '' },
              { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
                }
            },
            templates: [
                  { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
              { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
              { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
            ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: "mceNonEditable",
            toolbar_drawer: 'sliding',
            contextmenu: "link image imagetools table",
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtCobertura]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtVencimiento]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtExpedicion]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFechaEnvio]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFechaEmision]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtVigenciaInicio]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtVigenciaFin]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
             
              
        $(document).ready(function () {
            $('body').on('click', '#popupNuevoRecibo', function (e) {
             
             
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;

         
                var $dialog = parent.$('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibo",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=txtSearchRecibos.ClientID%>").click();
                    }
                });
                $dialog.dialog('open');
              });

        });

        $(document).ready(function () {
            $('body').on('click', '#popupNuevoInciso', function (e) {


                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;


                var $dialog = parent.$('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibo",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=txtSearchRecibos.ClientID%>").click();
                    }
                });
                $dialog.dialog('open');
            });

        });

        $(document).ready(function () {
            $('body').on('click', '#popupEditarRecibo', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;
                var $dialog = parent.$('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibo",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=txtSearchRecibos.ClientID%>").click();
                    }
                });
                $dialog.dialog('open');
            });

        });

        function SetImpuesto() {
            try {
                var idporcen = document.getElementById("<%=txtImpuestoPor.ClientID%>").value;
                var neto = document.getElementById("<%=txtNeto.ClientID%>").value;
                $.ajax({
                    type: "POST",

                    url: '<%= ResolveUrl("Endoso.aspx/getAmountImpuesto") %>',
                    data: "{'idporcentaje':'" + idporcen + "', 'neto':'" + neto + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        document.getElementById("<%=txtImpuesto.ClientID%>").value = msg.d;
                        CalculateAmountsNeto();
                    },
                    error: function (msg) {
                        console.log(msg.d + "error");

                    }
                });

                } catch (e) {
                    console.log(e.message);
                }
            }

            function CalculateAmountsNeto() {
                try {

                    var amount = 0;
                    var total = 0;

                    var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                    var gastos = document.getElementById("<%=txtGastos.ClientID%>").value;
                    var recargos = document.getElementById("<%=txtRecargosImporte.ClientID%>").value;


                    if (prima.length > 0) {
                        if (isNaN(prima)) {
                            alert("Prima acepta números unicamente !!");
                            return false;
                        }
                        else {
                            amount = amount + parseFloat(prima);
                        }
                    }

                    if (gastos.length > 0) {
                        if (isNaN(gastos)) {
                            alert("Gastos acepta números unicamente !!");
                            return false;
                        }
                        else {
                            amount = amount + parseFloat(gastos);
                        }
                    }
                    if (recargos.length > 0) {
                        if (isNaN(recargos)) {
                            alert("Recargos acepta números unicamente !!");
                            return false;
                        }
                        else {
                            var recargoAmount = parseFloat(recargos);
                            amount = amount + recargoAmount;
                        }
                    }

                    document.getElementById("<%=txtNeto.ClientID%>").value = amount;
                    var neto = document.getElementById("<%=txtNeto.ClientID%>").value;
                    SetImpuesto();
                    var impuestos = document.getElementById("<%=txtImpuesto.ClientID%>").value;
                    total = parseFloat(neto) + parseFloat(impuestos);

                    document.getElementById("<%=txtTotal.ClientID%>").value = total;


                } catch (e) {

                }

            }
            function CalculateAmountRecargoByPercent() {
                var amount = 0;
                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                var percentRecargo = document.getElementById("<%=txtRecargoPor.ClientID%>").value;

                if (prima.length > 0) {
                    if (isNaN(prima)) {
                        alert("Prima acepta números unicamente !!");
                        return false;
                    }
                    else {

                        if (percentRecargo.length > 0) {
                            if (isNaN(percentRecargo)) {
                                alert("Porcentaje de Recargos acepta números unicamente !!");
                                return false;
                            }
                            else {
                                var result = parseFloat(prima) * parseFloat(percentRecargo);
                                amount = result / 100;
                                document.getElementById("<%=txtRecargosImporte.ClientID%>").value = amount;
                            }
                        }


                    }
                }
                CalculateAmountsNeto();

            }
            function CalculatePercentRecargoByAmount() {
                var percentRecargo = 0;
                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
           var recargos = document.getElementById("<%=txtRecargosImporte.ClientID%>").value;
           if (recargos.length > 0) {
               if (isNaN(recargos)) {
                   alert("Recargos acepta números unicamente !!");
                   return false;
               }
               else {
                   var recargoAmount = parseFloat(recargos);
                   percentRecargo = (recargoAmount / parseFloat(prima)) * 100;
                   percentRecargo = percentRecargo.toFixed(2);
                   document.getElementById("<%=txtRecargoPor.ClientID%>").value = percentRecargo;
               }
           }
           CalculateAmountsNeto();
       }
       function CalculateAmountComision() {
           var amountcomision = 0;
           var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                      var compercent = document.getElementById("<%=txtComisionPor.ClientID%>").value;
                      if (prima.length > 0 && compercent.length > 0) {
                          if (isNaN(prima)) {
                              return false;
                          }
                          else {
                              var primaAmount = parseFloat(prima);
                              amountcomision = ((primaAmount * parseFloat(compercent)) / 100);
                              amountcomision = amountcomision.toFixed(2);
                              document.getElementById("<%=txtComisionImporte.ClientID%>").value = amountcomision;
                }
            }
        }
        function CalculatePercentComision() {
            var amountcom = 0;
            var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            var comamt = document.getElementById("<%=txtComisionImporte.ClientID%>").value;
            if (prima.length > 0 && comamt.length > 0) {
                if (isNaN(prima)) {
                    return false;
                }
                else {
                    var primaAmount = parseFloat(prima);
                    amountcom = (comamt / parseFloat(prima)) * 100;
                    amountcom = amountcom.toFixed(2);
                    document.getElementById("<%=txtComisionPor.ClientID%>").value = amountcom;
                }

            }
        }

        function generaCarta(idplantilla, tipoplantilla) {
            try {

                document.getElementById("<%=idplantilla.ClientID%>").value = idplantilla;


            } catch (e) {
                console.log(e.message);
            }
        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Desea eliminar los Incisos seleccionados?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        //function ConfirmDescarte() {
            
        //        var confirm_value = document.createElement("INPUT");
        //        confirm_value.type = "hidden";
        //        confirm_value.name = "confirmDescarte_value";
        //        if (confirm("Esta apunto de realizar un descarte")) {
        //            confirm_value.value = "Si";
        //        } else {
        //            confirm_value.value = "No";
        //        }
        //        document.forms[0].appendChild(confirm_value);
                
               
          
        //}

        function openpopup(url, title, btnname) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;
            if ("<%= EndosoId.Value %>" == "") {
                $("[id*=openurlatstart]").val('Inciso.aspx?polizaid=');

                $("[id*=PaneName]").val("AutoIndiv");
                $("#<%=BtnSave.ClientID%>").click();
                page = url;
            }
            else {
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: title,
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {
                        $(btnname).click();
                    }
                });
                $dialog.dialog('open');
            }
        }
    </script>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";
            $('#myTab a[href="#' + paneName + '"]').tab('show')

        }

        function selectPane(PaneName) {
            $("[id*=PaneName]").val(PaneName);

        }


    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="container" runat="server">

                <div class="panel-heading">
                    <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Endoso</asp:Label>
                </div>

                <div class="panel-heading">
                    <div class="row">
                        <div class="col">
                            <div class="btn-group">
                                <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                                <asp:Button ID="BtnDescarte" OnClientClick="javascript:return false;" data-toggle="modal" data-target="#exampleModal" runat="server" Text="Descarte" />
                                <!--<asp:Button ID="btnDescarteEndosoD" OnClientClick="ConfirmDescarte();" runat="server" OnClick="btnDescarteEndosoD_Click" Text="Descarte" />-->
                            </div>
                        </div>
                        <div class="col">
                            <ul class="nav navbar-nav">
                                <li class="dropdown" id="Li1" runat="server">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cartas<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <asp:Repeater ID="rptCartas" runat="server">
                                            <ItemTemplate>
                                                <li>
                                                    <a href="#" data-toggle="modal" data-target="#ConfirmCartas" onclick="generaCarta(<%# Eval("Id") %>,<%# Eval("TipoPlantilla") %>)" ><%# Eval("Nombre") %><span class="caret"></span></a>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="container" runat="server" id="cnEndoso">
                    <div id="errMess">
                    </div>
                    <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                    <div class="accordion" id="accordiontable" runat="server">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="Instrucciones-tab" data-toggle="tab" href="#Instrucciones" role="tab" aria-controls="Instrucciones" aria-selected="false">Instrucciones Aseguradora</a>
                            </li>
                            <li class="nav-item" id="IncisoAutosDiv" runat="server" onclick="selectPane('IncisosAutos');">
                            <a class="nav-link" id="AutoIndiv-tab" data-toggle="tab" href="#AutoIndiv" role="tab" aria-controls="AutoIndiv" aria-selected="false">Inciso</a>
                            </li>
                            <li class="nav-item" id="IncisoEndosoD" runat="server" onclick="selectPane('IncisoEndosoD');">
                            <a class="nav-link" id="EndosoD-tab" data-toggle="tab" href="#EndosoD" role="tab" aria-controls="EndosoD" aria-selected="false">Inciso</a>
                            </li>
                            <li class="nav-item" id="Recibotab" runat="server">
                                <a class="nav-link" id="Recibos-tab" data-toggle="tab" href="#Recibos" role="tab" aria-controls="Recibos" aria-selected="false">Recibos</a>
                            </li>

                            <li class="nav-item" id="Audit" runat="server">
                                <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblFolio" runat="server" Text="Folio"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtFolio" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblOt" runat="server" Text="OT"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtOt" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblDesc" runat="server" Text="Descripción"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblFechaEnvio" runat="server" Text="Fecha Envio"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtFechaEnvio" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblFechaEmision" runat="server" Text="Fecha Emisión"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtEstatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblTipo" runat="server" Text="Tipo de Endoso"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtTipo" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblTipoMov" runat="server" Text="Tipo de Movimiento"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtTipoMov" CssClass="form-control" runat="server" TabIndex="9">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="valTipoMov" runat="server" ErrorMessage="Tipo Movimiento" ForeColor="Red" ControlToValidate="txtTipoMov" Text="*"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblMesa" runat="server" Text="Mesa de Control"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtMesa" CssClass="form-control" runat="server" TabIndex="9" disabled>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblPoliza" runat="server" Text="Poliza"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtNoPoliza" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblFPago" runat="server" Text="F. de Pago"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtFormaPago" CssClass="form-control" runat="server" TabIndex="9">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblVigenciaInicio" runat="server" Text="Vigencia Inicio"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtVigenciaInicio" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblVigenciaFin" runat="server" Text="Vigencia Fin"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtVigenciaFin" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblPrima" runat="server" Text="Prima"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtPrima" onchange="CalculateAmountsNeto();CalculateAmountComision();" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator6" ControlToValidate="txtPrima" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />

                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblGastos" runat="server" Text="Gastos"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtGastos" runat="server" onchange="CalculateAmountsNeto();" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator10" ControlToValidate="txtGastos" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblRecargoPor" runat="server" Text="Recargo %"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtRecargoPor" runat="server" onchange="CalculateAmountRecargoByPercent();" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="txtRecargoPor" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblRecargoImporte" runat="server" Text="Recargos Importe"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtRecargosImporte" runat="server" onchange="CalculatePercentRecargoByAmount();" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ControlToValidate="txtRecargosImporte" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblComisionPor" runat="server" Text="Comisión %"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtComisionPor" runat="server" onchange="CalculateAmountComision();" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator3" ControlToValidate="txtComisionPor" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblComisionImporte" runat="server" Text="Comisión Importe"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtComisionImporte" runat="server" onchange="CalculatePercentComision();" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator4" ControlToValidate="txtComisionImporte" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblImpuestoPor" runat="server" Text="Impuesto %"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtImpuestoPor" runat="server" CssClass="form-control" TabIndex="7" onchange="SetImpuesto();">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblImpuesto" runat="server" Text="Impuesto"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtImpuesto" runat="server" CssClass="form-control" TabIndex="8">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblNeto" runat="server" Text="Neto"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtNeto" CssClass="form-control" runat="server" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator5" ControlToValidate="txtNeto" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />

                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtTotal" CssClass="form-control" runat="server" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator7" ControlToValidate="txtTotal" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />

                                        </div>

                                    </div>
                                    <%--<div class="row" runat="server" id="EndososRel">
                                        <div class="col">
                                            <asp:Label ID="lblEndosoOriginal" runat="server" Text="Endoso Original"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtEndosoOriginal" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator8" ControlToValidate="txtTotal" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />


                                        </div>

                                        <div class="col">
                                            <asp:Label ID="lblEndosoNuevo" runat="server" Text="Endoso Nuevo"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtEndosoNuevo" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator9" ControlToValidate="txtTotal" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />

                                        </div>

                                    </div>--%>



                                </div>
                            </div>
                            <div class="tab-pane fade" id="Instrucciones" role="tabpanel" aria-labelledby="Instrucciones-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblInstrucciones" runat="server" Text="Instrucciones Aseguradora"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtInstrucciones" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="10" Columns="10" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="AutoIndiv" role="tabpanel" aria-labelledby="AutoIndiv-tab">
                            <div class="containerRT18">
                               <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col" style="flex-basis:30%" >
                                        <asp:TextBox ID="txtSearchIncisosAutos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col" style="flex-basis:5%">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnSearchIncisosAutos" runat="server" CssClass="btn btn-default btn-sm" type="button" Text="Buscar" OnClick="btnSearchIncisosAutos_Click" CausesValidation= "false" style="z-index:0"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%" >
                                        <div class="input-group-append">
                                            <asp:ImageButton ID="btnDeleteIncisos" data-target="#pnlModal" data-toggle="modal" OnClick="btnDeleteIncisos_Click" OnClientClick="Confirm();" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%">
                                        <div class="input-group-append">
                                            <a  href="#" onclick="openpopup('Inciso.aspx?popup=1&polizaid=<%= PoliId.Value %>&EndosoId=<%= EndosoId.Value %>','Inciso','#<%=btnrecalcIncisosAutos.ClientID%>')"><img src="/Content/Imgs/add-square-button.png"  /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Button ID="btnSeleccionarTodos" CssClass="btn btn-default btn-sm" Text="Seleccionar Todos" runat="server" OnClick="btnSeleccionarTodos_Click" CausesValidation= "false" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnDeseleccionarTodos" CssClass="btn btn-default btn-sm" Text="Deseleccionar Todos" runat="server" OnClick="btnDeseleccionarTodos_Click" CausesValidation= "false" />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvIncisosAutos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a  onclick="openpopup('Inciso.aspx?popup=1&id=<%# Eval("Id")%>&polizaid=<%# Eval("Poliza") %>','Inciso','#<%= btnSearchIncisosAutos.ClientID%>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="AutoSerie" HeaderText="Serie" />
                                                <asp:BoundField DataField="AutoMotor" HeaderText="Motor" />
                                                <asp:BoundField DataField="AutoPlacas" HeaderText="Placas" />
                                                <asp:BoundField DataField="AutoModelo" HeaderText="Modelo" />
                                                <asp:BoundField DataField="Prima" HeaderText="Prima" />
                                                <asp:BoundField DataField="Poliza" Visible ="false" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="tab-pane fade" id="EndosoD" role="tabpanel" aria-labelledby="AutoIndiv-tab">
                            <div class="containerRT18">
                               <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col" style="flex-basis:30%" >
                                        <asp:TextBox ID="txtIncisoEndosoD" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col" style="flex-basis:5%">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnIncisosEndosoD" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnIncisosEndosoD_Click" CausesValidation= "false"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%" >
                                        <div class="input-group-append">
                                            <asp:ImageButton ID="btnDeleteIncisoEndosoD" data-target="#pnlModal" data-toggle="modal" OnClick="btnDeleteIncisoEndosoD_Click" OnClientClick="Confirm();" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%">
                                        <div class="input-group-append">
                                            <a  href="#"  Onclick="openpopup('SearchIncisoEndosoD.aspx?popup=1&polizaid= <%= PoliId.Value %>&EndosoId=<%= EndosoId.Value %>','Incisos','#<%=btnrecalcIncisosAutosD.ClientID%>')"><img src="/Content/Imgs/add-square-button.png"  /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Button ID="Button2" CssClass="btn btn-default btn-sm" Text="Seleccionar Todos" runat="server" OnClick="btnSeleccionarTodos_Click" CausesValidation= "false" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="Button3" CssClass="btn btn-default btn-sm" Text="Deseleccionar Todos" runat="server" OnClick="btnDeseleccionarTodos_Click" CausesValidation= "false" />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvIncisoEndosoD" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="AutoSerie" HeaderText="Serie" />
                                                <asp:BoundField DataField="AutoMotor" HeaderText="Motor" />
                                                <asp:BoundField DataField="AutoPlacas" HeaderText="Placas" />
                                                <asp:BoundField DataField="AutoModelo" HeaderText="Modelo" />
                                                <asp:BoundField DataField="Prima" HeaderText="Prima" />
                                                <asp:BoundField DataField="Poliza" Visible ="false" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="tab-pane fade" id="Recibos" role="tabpanel" aria-labelledby="Recibos-tab">
                                <div class="container">
                                    <div class="row">
                                        <br />
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:TextBox ID="txtSearchRecibos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                        </div>
                                        <div class="col">
                                            <div class="input-group-append">
                                                <asp:Button ID="BtnSearchRecibos" runat="server" CssClass="form-control" type="button" Text="Buscar" OnClick="btnBuscarRecibos_Click"></asp:Button>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="input-group-append">
                                                <div class="input-group-append" runat="server" id="NuevoRecibo">
                                                    <a id="popupNuevoRecibo" href='#' data-image='Recibo.aspx?popup=1&poliza=<%= EndosoId.Value %>'>
                                                        <img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="gvRecibos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <a id="popupEditarRecibo" href='#' data-image='Recibo.aspx?Id=<%# Eval("Id") %>'>
                                                                <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="tramiteFolio" HeaderText="Tramite" />
                                                    <asp:BoundField DataField="numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                                                    <asp:BoundField DataField="Cobertura" HeaderText="Cobertura" />
                                                    <asp:BoundField DataField="Vencimiento" HeaderText="Vencimiento" />
                                                    <asp:BoundField DataField="FechaPagoComision" HeaderText="F Max Pago" />
                                                    <asp:BoundField DataField="PrimaNeta" HeaderText="Prima" />
                                                    <asp:BoundField DataField="Gastos" HeaderText="Gastos" />
                                                    <asp:BoundField DataField="Recargo" HeaderText="Recargo" />
                                                    <asp:BoundField DataField="ImpuestoImporte" HeaderText="Impuesto" />
                                                    <asp:BoundField DataField="Total" HeaderText="Total" />
                                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                                    
                                            </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <asp:HiddenField ID="PaneName" runat="server" />
                            <asp:HiddenField ID="EndosoId" runat="server" />
                            <asp:HiddenField ID="EndosoTypeVal" runat="server" />
                            <asp:HiddenField ID="PoliId" runat="server" />
                            <asp:HiddenField ID="SearchIdRecibo" runat="server" />
                            <asp:HiddenField ID="idplantilla" runat="server" />
                            <asp:HiddenField ID="SearchIdInciso" runat="server" />
                            <asp:HiddenField ID="IncisoImportMsg" runat="server" />
                            <asp:HiddenField ID="openurlatstart" runat="server"/>
                            <asp:Button ID="btnrecalcIncisosAutos" runat="server" OnClick="btnrecalcIncisosAutos_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
                            <asp:Button ID="btnrecalcIncisosAutosD" runat="server" OnClick="btnrecalcIncisosAutosD_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
                            
                        </div>
                    </div>
                    <div class="modal" id="ConfirmDelete">
           
                    </div>  
                    <div class="modal fade" id="exampleModal" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Descartar Endoso</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container" runat="server">
                                        <div class="panel-heading">
                                            <asp:Label ID="Label1" CssClass="panel-title" runat="server">Descartar Endoso</asp:Label>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNoPoliza" runat="server" Text="No. Endoso:"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtPoliza" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblrecibos" runat="server" Text="¿Generar recibos?"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:CheckBox ID="txtgenRecibos" runat="server" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNumRecibos" runat="server" Text="¿Cuántos recibos?"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNumRecibos" runat="server" TextMode="Number" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblPrimaNeta" runat="server" Text="P. Neta primer recibo"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtPrimaNeta" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:RegularExpressionValidator runat="server" ID="txtPrimaNetaVal" ControlToValidate="txtPrimaNeta" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblGastosRecibo" runat="server" Text="Gastos primer recibo"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtGastosRecibo" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:RegularExpressionValidator runat="server" ID="txtGastosReciboVal" ControlToValidate="txtGastosRecibo" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Button ID="btnDescarteOk" OnClick="btnDescarteOk_Click" runat="server" Text="Descarte" />
                                                </div>
                                                <div class="col">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="ConfirmCartas" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="ConfirmCartasLabel">Generar Cartas</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="containerRT22" runat="server">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblSendEmail" runat="server" Text="Enviar por correo electronico"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtEmailSend" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblSubject" runat="server" Text="Asunto"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtSubject" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="row">
                                                <div class="col">
                                                    <asp:Button ID="btnSendEmail" OnClick="btnSendEmail_Click" runat="server" Text="Enviar correo" />
                                                </div>
                                                <div class="col">
                                                    <asp:Button ID="btnDescargar" OnClick="btnDescargar_Click" runat="server" Text="Descargar" />
                                                </div>
                                                <div class="col">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
