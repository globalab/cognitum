﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Grupo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                
                
                
            }
        }

        
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveGiro();
        }

        
        private void SaveGiro()
        {

            JoinEntity joinEntity = new JoinEntity();
            Entity Giro = new Entity("Grupo");
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;
            audi.recordStatus = (int)EstatusEnum.Activo;


            Giro.Attributes.Add(new Models.Attribute("Nombre", txtNombre.Text, "string"));
            Giro.Attributes.Add(new Models.Attribute("Propietario", userid.ToString(), "int64"));
            Giro.Attributes.Add(new Models.Attribute("Descripcion", txtDescription.Text, "string"));
            Giro.Attributes.Add(new Models.Attribute("Permiso","2", "string"));
            int ids = 0;
            int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
            Giro.Attributes.Add(new Models.Attribute("Ubicacion", ids.ToString(), "int64"));
            ids = 0;
            int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
            Giro.useAuditable = true;
            Giro.auditable = audi;
            Giro.Attributes.Add(new Models.Attribute("CentroDeBeneficio", ids.ToString(), "int64"));
           
            string errmess = "";
            object id = Request.QueryString["id"];
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                Giro.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);

            }
            Giro.auditable = audi;
            if (SiteFunctions.SaveEntity(Giro, method, out errmess))
            {
                showMessage("Se guardo correctamente el Grupo", false);
                Session["retVal"] = errmess;
                //if (Request.QueryString["popup"] != null)
                //{
                //    // Define the name and type of the client scripts on the page.
                //    String csname1 = "returnval";
                //    Type cstype = this.GetType();
                //    // Get a ClientScriptManager reference from the Page class.
                //    ClientScriptManager cs = Page.ClientScript;
                //    if (!cs.IsStartupScriptRegistered(cstype, csname1))
                //    {
                //        StringBuilder cstext1 = new StringBuilder();
                //        cstext1.Append(@"<script type='text/javascript'> windows.close();");
                //        cstext1.Append("</script>");

                //        cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                //    }
                    
                //}
                
            }
            else
            {
                showMessage(errmess, true);
            }
        }


        

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                 cstext1.Append("script>");
                
                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void GCMembers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}