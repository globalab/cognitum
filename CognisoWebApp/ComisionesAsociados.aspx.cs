﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class ComisionesAsociados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                object _id = Request.QueryString["id"];
                if(_id != null)
                {
                    Id.Value = _id.ToString();
                    searchComision(_id.ToString());
                }
                
            }
        }

        private void searchComision(string id)
        {
            string errmsg = string.Empty;
            Entity entity = new Entity("ComisionAsociado");
            entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
            entity.Attributes.Add(new Models.Attribute("DirectorAsociado", "", "int"));
            entity.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            entity.Attributes.Add(new Models.Attribute("Poliza"));
            entity.Keys.Add(new Key("Id", id, "int", 1));
           

            Entity dirAsociado = new Entity("DirectorAsociado");
            dirAsociado.Attributes.Add(new Models.Attribute("Usuario", "", "int"));

            Entity usuario = new Entity("Usuario");
            usuario.Attributes.Add(new Models.Attribute("Nombre", "", "string"));

            selectJoinEntity selJoinEntity = new selectJoinEntity("DirectorAsociado", 1, "Id");

            JoinEntity joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = dirAsociado;
            entity.ChildEntities.Add(joinEntity);

            selJoinEntity = new selectJoinEntity("Usuario", 1, "Id");

            joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = usuario;
            dirAsociado.ChildEntities.Add(joinEntity);
            List<Entity> polResult = SiteFunctions.GetValues(entity, out errmsg);
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(polResult);
            txtComision.Text = dtRes.Rows[0]["Porcentaje"].ToString();
            txtDirectorAsociado.Text = dtRes.Rows[0]["UsuarioNombre"].ToString();
            txtPoliza.Text = "Folio: " + dtRes.Rows[0]["Poliza"].ToString();


            DateTime fechaaudit = Convert.ToDateTime(dtRes.Rows[0]["Audit_FechaAdd"].ToString());
            txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
            fechaaudit = Convert.ToDateTime(dtRes.Rows[0]["Audit_FechaUMod"].ToString());
            txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
            Entity UserAudit = new Entity("Usuario");
            UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
            UserAudit.Keys.Add(new Key("Id", dtRes.Rows[0]["Audit_UsuarioAdd"].ToString(), "int64"));
            List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errmsg);
            if (AuditUsr.Count > 0)
            {
                txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
            }
            UserAudit = new Entity("Usuario");
            UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
            UserAudit.Keys.Add(new Key("Id", dtRes.Rows[0]["Audit_UsuarioUMod"].ToString(), "int64"));
            AuditUsr = SiteFunctions.GetValues(UserAudit, out errmsg);
            if (AuditUsr.Count > 0)
            {
                txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
            }
            txtId.Text = Id.Value.ToString();

        }
        
       

        
        


        

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                 cstext1.Append("script>");
                
                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void GVSubRamoList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
    }
}