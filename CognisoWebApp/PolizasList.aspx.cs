﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class PolizasList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                searchForPolizas();

            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);

            }
        }
        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("poliza.aspx");
        }
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in GVPolizasList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string id = GVPolizasList.Rows[row.RowIndex].Cells[1].Text;
                        Response.Redirect(string.Format("Poliza.aspx?Id={0}&readonly=1", id));
                    }
                }
            }
        }


        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVPolizasList.PageIndex = e.NewPageIndex;
            GVPolizasList.DataBind();
            searchForPolizas();

        }
        private void searchForPolizas()
        {
            JoinEntity joinEntity = new JoinEntity();
            selectJoinEntity selEntity = new selectJoinEntity();
            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity poliza = new Entity("Poliza");
            poliza.Attributes.Add(new Models.Attribute("id", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Contratante", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Agente", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ramo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Aseguradora", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ejecutivo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", "", "string"));
            //poliza.Keys.Add(new Models.Key("Estatus", "1", "int", 1, 1, "a"));

            Entity negociacion = new Entity("Negociacion");
            negociacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
            negociacion.Attributes.Add(new Models.Attribute("NumeroNegociacion", "", "string"));
            negociacion.useAuditable = false;

            selEntity = new selectJoinEntity("negociacion", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = negociacion;
            poliza.ChildEntities.Add(joinEntity);

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto", "", "string"));
            cliente.useAuditable = false;

            selEntity = new selectJoinEntity("contratante", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = cliente;
            poliza.ChildEntities.Add(joinEntity);

            Entity agente = new Entity("Agente");
            agente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            agente.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            agente.useAuditable = false;

            selEntity = new selectJoinEntity("agente", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = agente;
            poliza.ChildEntities.Add(joinEntity);

            Entity ramo = new Entity("Ramo");
            ramo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ramo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ramo.useAuditable = false;

            selEntity = new selectJoinEntity("ramo", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = ramo;
            poliza.ChildEntities.Add(joinEntity);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;

            selEntity = new selectJoinEntity("aseguradora", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = aseguradora;
            poliza.ChildEntities.Add(joinEntity);

            Entity ejecutivo = new Entity("Usuario");
            ejecutivo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ejecutivo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ejecutivo.useAuditable = false;

            selEntity = new selectJoinEntity("ejecutivo", 1, "id");
            joinEntity = new JoinEntity();
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selEntity);
            joinEntity.ChildEntity = ejecutivo;
            poliza.ChildEntities.Add(joinEntity);

            Entity Tramite = new Entity("Tramite");
            Tramite.Attributes.Add(new Models.Attribute("Folio", "", "string"));

            selectJoinEntity selJoin = new selectJoinEntity("Id", 2, "id");
            JoinEntity joinEnt = new JoinEntity();
            joinEnt.selectJoinList = new List<selectJoinEntity>();
            joinEnt.selectJoinList.Add(selJoin);
            joinEnt.ChildEntity = Tramite;
            joinEnt.JoinType = JoinType.Inner;

            poliza.ChildEntities.Add(joinEnt);

            poliza.logicalOperator = 2;
            
           if (SearchId.Value != "" && SearchId.Value != "%%")
            {
                Int64 id = 0;
                poliza.Keys.Add(new Key("NombreCompleto", SearchId.Value, "string", 2,1,"Cliente"));
                poliza.Keys.Add(new Key("Folio", SearchId.Value, "string", 2,1,"Tramite"));
                poliza.Keys.Add(new Key("Nombre", SearchId.Value, "string", 2, 1, "Agente"));
                poliza.Keys.Add(new Key("Clave", SearchId.Value, "string", 2, 1, "Aseguradora"));
                poliza.Keys.Add(new Key("Nombre", SearchId.Value, "string", 2, 1, "Ramo"));
                poliza.Keys.Add(new Key("Nombre", SearchId.Value, "string", 2, 1, "Usuario"));
            
                if (Int64.TryParse(SearchId.Value.Replace("%", ""), out id))
                {
                    poliza.Keys.Add(new Models.Key("Id", id.ToString() + "%", "string", 2, 1));
                }
                           

            }

            
            
            //Agregar filtro decimal estado por vistas
            switch (PolizaView.SelectedValue)
            {
                case "1":
                    poliza.Keys.Add(new Key("EstatusPoliza", "0", "int",1,2));
                    break;
                case "2":
                    poliza.Keys.Add(new Key("EstatusPoliza", "3", "int", 1,2));
                    break;
                case "4":
                    poliza.Keys.Add(new Key("EstatusPoliza", "1", "int", 1,2));
                    break;
                case "5":
                    poliza.Keys.Add(new Key("EstatusPoliza", "2", "int", 1,2));
                    break;
                case "6":
                    poliza.Keys.Add(new Key("EstatusPoliza", "4", "int", 1,2));
                    break;
            }


            List<Entity> listLeads = SiteFunctions.GetValues(poliza, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);
            /*foreach (DataRow row in dtLeadList.Rows)
            {
                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {

                            case "cliente":
                                foreach (var item in opResult.RetVal)
                                {
                                    string nameId = item.getAttrValueByName("Id");
                                    if (row[1].ToString().Trim() == nameId.Trim())
                                    {
                                        row[1] = item.getAttrValueByName("NombreCompleto");
                                        break;
                                    }
                                }
                                break;
                            case "agente":
                                foreach (var item in opResult.RetVal)
                                {
                                    string nameId = item.getAttrValueByName("Id");
                                    if (row[2].ToString().Trim() == nameId.Trim())
                                    {
                                        row[2] = item.getAttrValueByName("Clave");
                                        break;
                                    }
                                }
                                break;
                            case "ramo":
                                foreach (var item in opResult.RetVal)
                                {
                                    string nameId = item.getAttrValueByName("Id");
                                    if (row[3].ToString().Trim() == nameId.Trim())
                                    {
                                        row[3] = item.getAttrValueByName("Nombre");
                                        break;
                                    }
                                }
                                break;
                            case "aseguradora":
                                foreach (var item in opResult.RetVal)
                                {
                                    string nameId = item.getAttrValueByName("Id");
                                    if (row[4].ToString().Trim() == nameId.Trim())
                                    {
                                        row[4] = item.getAttrValueByName("Clave");
                                        break;
                                    }
                                }
                                break;

                            case "usuario":
                                foreach (var item in opResult.RetVal)
                                {
                                    string nameId = item.getAttrValueByName("Id");
                                    if (row[5].ToString().Trim() == nameId.Trim())
                                    {
                                        row[5] = item.getAttrValueByName("Nombre");
                                        break;
                                    }
                                }
                                break;



                        }
                    }
                }
            }*/

            GVPolizasList.DataSource = dtLeadList;
            GVPolizasList.DataBind();
        }

        protected void PolizaView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForPolizas();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
           searchForPolizas();
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForPolizas();
        }
    }
}