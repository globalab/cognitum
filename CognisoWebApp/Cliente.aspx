﻿<%@ Page Title="Cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cliente.aspx.cs" Inherits="CognisoWebApp.Cliente" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
     <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(document).ready(function () {

            $("#<%=txtGroup.ClientID%>").select2({


                placeholder: "Seleccione un Grupo",
                minimumResultsForSearch: 1,
                allowClear: true


            });

        });

        $(document).ready(function () {

            $("#<%=txtGiro.ClientID%>").select2({
                placeholder: "Seleccione un Giro",
                minimumResultsForSearch: 1,
                allowClear: true
            });

        });

        $('body').on('click', '.popup', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var page = $(this).attr("data-image")  //get url of link
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;
            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: "Dirección",
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {

                    $("#<%=reloadDir.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });

            $('body').on('click', '.popupCont', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.9;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Contacto",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=reloadCont.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });
            $('body').on('click', '.popupTel', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Teléfono",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=reloadTel.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });

            $('body').on('click', '.popupattc', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Documentos",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {
                    }
                });
                $dialog.dialog('open');

            });

            $('body').on('click', '#popupGrupo', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Grupo",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=reloadGrupo.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });

            $('body').on('click', '#popupGiro', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Giro",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=reloadGiro.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            $('#myTab a[href="#' + paneName + '"]').tab('show')

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
    </script>

    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>

    <div class="containerRT0">
        <div class="containerRT1" runat="server">
            <div class="panel-heading">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="CustomerList.aspx">Lista de Clientes</a></li>
                        <li class="breadcrumb-item active">Cliente <%=CustId.Value %> </li>
                    </ol>
                </nav>
            </div>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                    <a class="popupattc" href='#' data-image='Attachment.aspx?id=<%=CustId.Value %>&entityName=Cliente&parentId=0'><img src="/Content/Imgs/clip.png" width="30" height="30" /></a> 
                    <!--<asp:ImageButton ID="btnAttachment" ImageUrl="/Content/Imgs/clip.png" runat="server" OnClick="BtnAttachment_Click" />-->
                    <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>
            <div id="errMess">
            </div>
            <div class="containerRT2" runat="server" id="ProspectoGral">
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="PF-tab" data-toggle="tab" href="#DatosGenerales" role="tab" aria-controls="DatosGenerales" aria-selected="true">Datos Generales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Direcciones" role="tab" aria-controls="Direcciones" aria-selected="false">Direcciones</a>
                        </li>
                        <li class="nav-item" id="telPF" runat="server">
                            <a class="nav-link" id="Telefono-tab" data-toggle="tab" href="#Telefono" role="tab" aria-controls="Telefono" aria-selected="false">Teléfono</a>
                        </li>
                        <li class="nav-item" id="ContactPM" runat="server">
                            <a class="nav-link" id="Contacto-tab" data-toggle="tab" href="#Contacto" role="tab" aria-controls="Contacto" aria-selected="false">Contacto</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="DatosGenerales" role="tabpanel" aria-labelledby="DatosGenerales-tab">
                            <div class="containerRt5"> 
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRFC" runat="server" Text="RFC"></asp:Label>
                                        <div class="input-group mb-3">
                                            <asp:TextBox ID="txtRFC" class="form-control" runat="server" CssClass="form-control" TabIndex="3"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:CheckBox CssClass="form-check-input" ID="txtMoral" Text="Persona Moral" runat="server" TabIndex="2"></asp:CheckBox>
                                        <br />
                                        <asp:CheckBox CssClass="form-check-input" ID="txtExtranjero" Text="Extranjero" runat="server" TabIndex="1"></asp:CheckBox>
                                    </div>
                                </div>
                            </div>
                            <div id="PF" class="containerRT4" runat="server">
                                
                                <div class="row">
                                    <div class="col">

                                        <asp:Label ID="lblName" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="15" TabIndex="5"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMiddleName" runat="server" Text="Apellido Paterno"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtMiddleName" runat="server" CssClass="form-control" MaxLength="150" TabIndex="6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblLastName" runat="server" Text="Apellido Materno"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" MaxLength="15" TabIndex="7"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblBirthDate" runat="server" Text="Fecha de Nacimiento"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDate" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCurp" runat="server" Text="CURP"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCurp" runat="server" CssClass="form-control" TabIndex="9"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" TabIndex="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="PM" class="containerRT3" runat="server">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRazonSocial" runat="server" Text="Razón Social"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT6">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEstatusCliente" runat="server" Text="Estatus Cliente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatusCliente" runat="server" class="form-control" disabled>
                                                <asp:ListItem Enabled="True" Selected="True" Value="1">Nuevo</asp:ListItem>
                                                <asp:ListItem Enabled="True" Value="2">En Validaci&#243;n</asp:ListItem>
                                                <asp:ListItem Enabled="True" Value="3">Cliente</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblGrupo" runat="server" Text="Grupo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtGroup" runat="server" class="form-control" style="width:90%">
                                            </asp:DropDownList>
                                            <div class="input-group-append">
                                                <!--  <input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Grupo')"/> -->
                                                &nbsp;<a id="popupGrupo" href='#' data-image='Grupo.aspx?popup=1'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblGiro" runat="server" Text="Giro"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtGiro" runat="server" class="form-control" style="width:90%"></asp:DropDownList>
                                            <div class="input-group-append">
                                                <!--<input type="image" src="/Content/Imgs/add-square-button.png" width="25" height="25" onclick="OpenPopup('Giro')"/> -->
                                                &nbsp;<a id="popupGiro" href='#' data-image='Giro.aspx?popup=1' ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Direcciones" role="tabpanel" aria-labelledby="Direcciones-tab">
                            <div class="container sin-sombras">
                                <div class="row">
                                    <a class="popup" href='#' data-image='Direccion.aspx?popup=1&custid=<%=CustId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a> 
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVDirList" runat="server" OnPageIndexChanging="GVDirList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a class="popup" href='#' data-image='Direccion.aspx?popup=1&custid=<%# Eval("Cliente") %>&id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="Calle" HeaderText="Calle" />
                                    <asp:BoundField DataField="NumInterior" HeaderText="Número (Int)" />
                                    <asp:BoundField DataField="NumExterior" HeaderText="Número (Ext)" />

                            
                                </Columns>
                                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Telefono" role="tabpanel" aria-labelledby="Telefono-tab">
                            <div class="containerRT8">
                                <div class="row">
                                       <a class="popupTel" href='#' data-image='Telefono.aspx?popup=1&custid=<%=CustId.Value %>' ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVTelList" runat="server" OnPageIndexChanging="GVTelList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a class="popupTel" href='#' data-image='Telefono.aspx?popup=1&custid=<%=CustId.Value %>&id=<%# Eval("Id") %>' )"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="LadaNacional" HeaderText="Lada (nac)" />
                                    <asp:BoundField DataField="LadaInternacional" HeaderText="Lada (int)" />
                                    <asp:BoundField DataField="Numero" HeaderText="Número" />
                                    <asp:BoundField DataField="Extension" HeaderText="Extension" />
                            
                                </Columns>
                                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Contacto" role="tabpanel" aria-labelledby="Contacto-tab">
                            <div id="CPM" runat="server">
                            <div class="container sin-sombras">
                                <div class="row">
                                    <a class="popupCont" href='#' data-image='Contacto.aspx?popup=1&custid=<%=CustId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a> 
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GVContacto" runat="server" OnPageIndexChanging="GVContacto_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" >
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a class="popupCont" href='#' data-image='Contacto.aspx?popup=1&custid=<%# Eval("PersonaMoral") %>&id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" />
                                                <asp:BoundField DataField="Puesto" HeaderText="Puesto" />
                                                <asp:BoundField DataField="Id" Visible="False"></asp:BoundField>
                                                <asp:BoundField DataField="PersonaMoral" Visible="False"></asp:BoundField>
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                            </asp:GridView>
                                    </div>
                                </div>
                            
                            </div>
                            </div>
                            
                            <div id="CPF" runat="server">
                            <div class="containerRT10">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNombreCA" runat="server" Text="Nombre Completo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNombreCA" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEmailCA" runat="server" Text="Email"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtEmailCA" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTelefonoCA" runat="server" Text="Teléfono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTelefonoCA" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT11">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div>  
                    </div>
                    </div>
                    <asp:HiddenField ID="PaneName" runat="server" />
                    <asp:HiddenField ID="CustId" runat="server" />
                    <asp:HiddenField ID="CustRFC" runat="server" />
                    <asp:Button ID="reloadDir" runat="server" OnClick="reloadDir_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="reloadCont" runat="server" OnClick="reloadCont_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="reloadTel" runat="server" OnClick="reloadTel_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="reloadGiro" runat="server" OnClick="reloadGiro_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="reloadGrupo" runat="server" OnClick="reloadGrupo_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                </div>
            </div>
        </div>
    
</asp:Content>
