﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecurityRolesList.aspx.cs" Inherits="CognisoWebApp.SecurityRolesList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Roles</asp:Label>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="#">Lista de Roles</a></li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" runat="server" Height="30px" OnClick="btnNew_Click" Width="31px" />
                    <%--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />--%>
                </div>
            </div>
            <div class="input-group mb-3">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                <div class="input-group-append">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button>
                </div>
            </div>
            <div class="panel-body" runat="server">
                <div class="table-responsive" runat="server">
                    <asp:GridView ID="GVRoles" runat="server" OnPageIndexChanging="GVRoles_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a   href='SecurityRole.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="Id" />
                            <asp:BoundField DataField="Rolename" HeaderText="Nombre" />
                        </Columns>
                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="SearchId" runat="server" Value="" />
            </div>
        </div>
    </div>
</asp:Content>
