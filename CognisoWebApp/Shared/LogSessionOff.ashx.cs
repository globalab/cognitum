﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace CognisoWebApp.Shared
{
    /// <summary>
    /// Summary description for LogSessionOff
    /// </summary>
    public class LogSessionOff : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Session.Clear(); 
            context.Session.Abandon();
            FormsAuthentication.SignOut();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}