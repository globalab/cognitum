﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class UserRoles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                searchRoles();
                object id = Request.QueryString["id"];
                if(id != null)
                {
                    string userName = Request.QueryString["Username"].ToString();
                    string nombre = Request.QueryString["Nombre"].ToString();

                    txtNombre.Text = nombre;
                    txtUserId.Text = userName;

                    txtNombre.Enabled = false;
                    txtUserId.Enabled = false;

                    UserId.Value = id.ToString();
                    searchRolesByUser(id.ToString());
                }
            }
        }

        private void searchRolesByUser(string userId)
        {
            Entity userRoles = new Entity("RoleByUsers");
            userRoles.Attributes.Add(new Models.Attribute("UserId"));
            userRoles.Keys.Add(new Key("UserId", userId, "int"));
            userRoles.useAuditable = false;

            Entity roles = new Entity("SecurityRole");
            roles.Attributes.Add(new Models.Attribute("Id"));
            roles.Attributes.Add(new Models.Attribute("RoleName"));

            JoinEntity joinEnt = new JoinEntity();
            
            selectJoinEntity selJoin = new selectJoinEntity("RoleId", 1, "Id");
            joinEnt.selectJoinList.Add(selJoin);
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.ChildEntity = roles;

            userRoles.ChildEntities.Add(joinEnt);
            string errMsg;

            List<Entity> result = SiteFunctions.GetValues(userRoles, out errMsg);
            DataTable dtResults = SiteFunctions.trnsformEntityToDT(result);
            gvRole.DataSource = dtResults;
            gvRole.DataBind();
        }

        private void searchRoles()
        {
            Entity role = new Entity("SecurityRole");
            role.Attributes.Add(new Models.Attribute("Id"));
            role.Attributes.Add(new Models.Attribute("RoleName"));
            role.useAuditable = false;
            string errMsg;
            List<Entity> result = SiteFunctions.GetValues(role, out errMsg);
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(result);
            txtNewRole.DataSource = dtRes;
            txtNewRole.DataTextField = "RoleName";
            txtNewRole.DataValueField = "Id";
            txtNewRole.DataBind();
        }

        protected void btnNewRole_Click(object sender, ImageClickEventArgs e)
        {
            string errMsg;
            object id = Request.QueryString["id"];
            if (id != null)
            {
                Entity userRoles = new Entity("RoleByUsers");
                userRoles.useAuditable = false;
                userRoles.Attributes.Add(new Models.Attribute("RoleId", txtNewRole.SelectedValue, "int"));
                userRoles.Attributes.Add(new Models.Attribute("UserId", id.ToString(), "int"));

                if (SiteFunctions.SaveEntity(userRoles, RestSharp.Method.POST, out errMsg))
                {
                    searchRolesByUser(id.ToString());
                }
                else
                {
                    showMessage(errMsg, true);
                }
            }
        }
        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void gvRole_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            object id = Request.QueryString["id"];
            if (id != null)
            {
                GridViewRow select = gvRole.Rows[e.NewSelectedIndex];
                Label lblEntity = (Label)select.Cells[1].FindControl("SecurityRoleId");
                Entity UserRole = new Entity("RoleByUsers");
                Key filter = new Key("UserId", id.ToString(), "int");
                filter.Group = 1;
                filter.LogicalOperator = 1;
                filter.WhereOperator = whereOperator.Equal;
                UserRole.Keys.Add(filter);

                filter = new Key("RoleId", lblEntity.Text, "int");
                filter.Group = 1;
                filter.LogicalOperator = 1;
                filter.WhereOperator = whereOperator.Equal;
                UserRole.Keys.Add(filter);

                string errMsg;

                SiteFunctions.DeleteEntity(UserRole, RestSharp.Method.POST, out errMsg);

                searchRolesByUser(id.ToString());
            }
        }
    }
}