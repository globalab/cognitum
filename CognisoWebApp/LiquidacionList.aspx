﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LiquidacionList.aspx.cs" Inherits="CognisoWebApp.LiquidacionList" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
    </script>

    <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Liquidaciones</asp:Label></h2>
            </div>
            <hr class="float-left">

            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="#">Lista de Liquidaciones</a></li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" runat="server" Height="30px" OnClick="btnNew_Click" Width="31px" />
                    <%--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />--%>
                </div>
            </div>
            <div class="input-group mb-3">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                <div class="input-group-append col-md-7">
                    <div class="col-md-2"><asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button></div>
                        <div class="col-md-4"><label class="form-control">Filtro: </label></div>
                        <div class="col-md-6">
                                  <asp:DropDownList Id="LiquidacionesView" runat="server" Cssclass="form-control " OnSelectedIndexChanged="LiquidacionesView_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="1">Todos los tipos de liquidaciones</asp:ListItem>
                                    <asp:ListItem Value ="2" Selected="True">Todas las solicitudes de liquidación</asp:ListItem>
                                    <asp:ListItem Value="3">Todas las liquidaciones aplicadas</asp:ListItem>
                                    <asp:ListItem Value="4">Todas las liquidaciones canceladas</asp:ListItem>
                                    <asp:ListItem Value="5">Todas las liquidaciones reabiertas</asp:ListItem>
                                </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body" runat="server">
                <div class="table-responsive" runat="server">
                    <asp:GridView ID="GVLiquidacion" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a   href='Liquidacion.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="Id" />
                            <asp:BoundField DataField="Folio" HeaderText="Folio" />
                            <asp:BoundField DataField="TramiteFolio" HeaderText="No. Poliza" />
                            <asp:BoundField DataField="FechaLiquidacion" HeaderText="Fecha Liquidación" />
                            <asp:BoundField DataField="AseguradoraClave" HeaderText="Aseguradora" />
                            <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        </Columns>
                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="SearchId" runat="server" Value="" />
            </div>
        </div>
    </div>
</asp:Content>
