﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class Attachment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                btnUpload.Attributes["onclick"] = "javascript:gfProceso()";
                object id = Request.QueryString["id"];
                object entityName = Request.QueryString["entityName"];
                object parentId = Request.QueryString["parentId"];
                if (id != null && entityName != null && parentId != null && id != "" && entityName != "" && parentId != "")
                {
                    Id.Value = id.ToString();
                    EntityName.Value = entityName.ToString();
                    ParentId.Value = parentId.ToString();
                    this.searchDocuments(entityName.ToString(), id.ToString(), parentId.ToString());

                }
                else 
                {
                    lblInfo.Text = "La página no puede ser abierta";

                }
               
            }


        }
        private void searchDocuments(string _entityName, string _id, string _parentId)
        {
            try
            {
                AttachmentList listResult = new AttachmentList();

                listResult = SiteFunctions.execListAttachment(_entityName, _id, _parentId);
                if (listResult.Success)
                {
                    GVDocumentsList.DataSource = listResult.ListFile;// cada elemento que va a traer? es decir nombre documento , ruta SP?
                    GVDocumentsList.DataBind();
                }
            }
            catch (Exception ex)
            {

            }

        }
        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVDocumentsList.PageIndex = e.NewPageIndex;
            GVDocumentsList.DataBind();
            searchDocuments(EntityName.Value, Id.Value, ParentId.Value);
        }
        protected void GridList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow row = GVDocumentsList.SelectedRow;

                string url = row.Cells[2].Text;
                string fileName = row.Cells[1].Text;
                SiteFunctions.getDocumentSP(EntityName.Value, Id.Value, ParentId.Value, url);
                string urlfolder = ConfigurationManager.AppSettings["urlFileFolder"].ToString();

                String csname1 = "fileScript";
                Type cstype = this.GetType();
                ClientScriptManager cs = Page.ClientScript;

                // Check to see if the startup script is already registered.
                if (!cs.IsStartupScriptRegistered(cstype, csname1))
                {
                    StringBuilder cstext1 = new StringBuilder();
                    cstext1.Append(@"<script type='text/javascript'> window.open('" + urlfolder + fileName + "') </");
                    cstext1.Append("script>");

                    cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                }

                

            }
            catch (Exception ex)
            {
            }

        }
        public void UploadDocument(object sender, EventArgs e)
        {
            try
            {

                for (int i = 0; i < FileUploadDoc.PostedFiles.Count; i++)
                {
                    try
                    {
                        string filename = Path.GetFileName(FileUploadDoc.PostedFiles[i].FileName);
                        //FileUploadDoc.SaveAs(Server.MapPath("~/Documents/") + filename);

                        byte[] fileBytes = FileUploadDoc.FileBytes;

                        //FileInfo fi = new FileInfo(FileUploadDoc.PostedFiles[i].FileName);
                        //StreamReader reader = new StreamReader(Server.MapPath("~/Documents/") + filename);

                        ////UploadDocumentSP reader.BaseStream
                        //reader.Close();
                        //System.IO.File.Delete(Server.MapPath("~/Documents/") + filename);

                        OperationResult opresult = SiteFunctions.uploadfile(EntityName.Value, Id.Value, ParentId.Value, filename, fileBytes);
                        if (!opresult.Success)
                        {
                            showMessage(opresult.ErrMessage, true);
                        }
                    }
                    catch(Exception exc)
                    {
                        showMessage(exc.Message, true);
                    }
                    
                }
                searchDocuments(EntityName.Value, Id.Value, ParentId.Value);
               
            }
            catch (Exception ex)
            {
             
            }

        }

        protected void lnkDescarga_Click(object sender, EventArgs e)
        {
            try
            {
                string nameFile = HdnFileName.Value.ToString().Trim();
                nameFile = nameFile.Replace(',', '_');
                FileStream sourceFile = new FileStream(Server.MapPath("~/Documents/" + HdnName.Value.ToString()), FileMode.Open);
                float FileSize;
                FileSize = sourceFile.Length;
                byte[] getContent = new byte[(int)FileSize];
                sourceFile.Read(getContent, 0, (int)sourceFile.Length);
                sourceFile.Close();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameFile);
                Response.BinaryWrite(getContent);
                Response.End();
            }
            catch (Exception ex)
            {


            }


        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
    }
}