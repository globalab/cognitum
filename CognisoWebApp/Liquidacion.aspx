﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Liquidacion.aspx.cs" Inherits="CognisoWebApp.Liquidacion" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <link href="Content/cogniso.css" rel="stylesheet" />
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function CalculateTotals() {
            debugger;
            var totalValores = 0;

            var totalTransferStr = document.getElementById("<%=txtTotalTransferencia.ClientID%>").value;
            var totalChequesStr = document.getElementById("<%=txtTotalCheques.ClientID%>").value;
            var totalPrimasStr = document.getElementById("<%=txtTotalPrimasDep.ClientID%>").value;
            var totalTCStr = document.getElementById("<%=txtTotalTC.ClientID%>").value;
            var totalDiferenciaStr = document.getElementById("<%=txtTotalLiquidacion.ClientID%>").value;

            var totalTransfer = convertToAmt(totalTransferStr);
            var totalCheques = convertToAmt(totalChequesStr);
            var totalPrimas = convertToAmt(totalPrimasStr);
            var totalTC = convertToAmt(totalTCStr);
            var totalDiferencia = convertToAmt(totalDiferenciaStr);

            totalValores = totalTransfer + totalCheques + totalPrimas + totalTC;

            document.getElementById("<%=txtDiferencia.ClientID%>").value = totalValores - totalDiferencia;
            document.getElementById("<%=txtTotalValores.ClientID%>").value = totalValores;
        }

        function convertToAmt(textAmt) {
            if (textAmt.length > 0) {
                if (isNaN(textAmt)) {
                    return 0;
                }
                else {
                    return parseFloat(textAmt);
                }
            }
            else {
                return 0;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtPoliza.ClientID%>").select2({
                placeholder: "Seleccione una Póliza",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtAseguradora.ClientID%>").select2({
                placeholder: "Seleccione una Aseguradora",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtGrupo.ClientID%>").select2({
                placeholder: "Seleccione un Grupo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(function () {
            $('[id*=txtFecha]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });



        $('body').on('click', '#popupReciboLiq', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var page = $(this).attr("data-image")  //get url of link

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: 610,
                width: 800,
                title: "Recibos",
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {

                    $("#<%=btnRefreshRecibos.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });

            function selectPane(PaneName) {
                $("[id*=PaneName]").val(PaneName);

            }

            function collapsediv() {
                var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";
                $('#myTab a[href="#' + paneName + '"]').tab('show')

            }

            function Confirm() {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                if (confirm("Desea eliminar los recibos seleccionados?")) {
                    confirm_value.value = "Si";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }

            function ConfirmdelIntegracion() {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirmdelint_value";
                if (confirm("Desea eliminar las integraciones seleccionadas?")) {
                    confirm_value.value = "Si";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }

            $('body').on('click', '#popupRecibos', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibos",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {


                    }
                });
                $dialog.dialog('open');

            });
    </script>
    <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Liquidación</asp:Label></h2>
            </div>
            <hr class="float-left">
            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="LiquidacionList.aspx">Lista de Liquidaciones</a></li>
                    <li class="breadcrumb-item active">Liquidación <%=LiqId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                    <asp:Button ID="btnRefreshRecibos" runat="server" CssClass="btn btn-outline-secondary" type="button" style="visibility: hidden; display: none;"  OnClick="btnRefreshRecibos_Click" CausesValidation="false" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button ID="btnAplicar" runat="server" CssClass="btn btn-outline-secondary" type="button" Visible="true" Text="Aplicar" OnClick="btnAplicar_Click"></asp:Button>
                    <asp:Button ID="btnPrint" OnClientClick="javascript:return false;" data-toggle="modal" data-target="#exampleModal" runat="server" Text="Imprimir" />
                </div>
            </div>

            <div class="containerRT3" runat="server" id="LiquidacionGral">
                <div id="errMess" >

                </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item" onclick="selectPane('General');">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Recibos');" id="RecibosDiv" runat="server">
                            <a class="nav-link" id="Recibos-tab" data-toggle="tab" href="#Recibos" role="tab" aria-controls="Recibos" aria-selected="false">Recibos</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Cobranza');">
                            <a class="nav-link" id="Cobranza-tab" data-toggle="tab" href="#Cobranza" role="tab" aria-controls="Cobranza" aria-selected="false">Cobranza</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Totales');">
                            <a class="nav-link" id="Totales-tab" data-toggle="tab" href="#Totales" role="tab" aria-controls="Totales" aria-selected="false">Totales</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('IntegracionPago');">
                            <a class="nav-link" id="IntegracionPago-tab" data-toggle="tab" href="#IntegracionPago" role="tab" aria-controls="IntegracionPago" aria-selected="false">Integración Pagos</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server" onclick="selectPane('Auditoria');">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT4">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFolio" runat="server" Text="Folio"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFolio" runat="server" class="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblFecha" runat="server" Text="Fecha liquidación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFecha" runat="server" class="form-control">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="valFechaLiq" runat="server" ErrorMessage="Fecha Liquidación" ForeColor="Red" ControlToValidate="txtFecha" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPoliza" runat="server" Text="Poliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtPoliza" Style="width: 90%" AutoPostBack="true" OnSelectedIndexChanged="txtPoliza_SelectedIndexChanged" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblStatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtStatus" runat="server" class="form-control" disabled></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblGrupo" runat="server" Text="Grupo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtGrupo" Style="width: 90%" AutoPostBack="true" OnSelectedIndexChanged="txtGrupo_SelectedIndexChanged" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" runat="server" class="form-control" Style="width:90%"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valAseguradora" runat="server" ErrorMessage="Aseguradora" ForeColor="Red" ControlToValidate="txtAseguradora" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblMoneda" runat="server" Text="Moneda"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMoneda" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valMoneda" runat="server" ErrorMessage="Moneda" ForeColor="Red" ControlToValidate="txtMoneda" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTipoCambio" runat="server" Text="Tipo de cambio"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTipoCambio" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorComisionVal" controltovalidate="txtTipoCambio" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblObservaciones" runat="server" Text="Observaciones"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tab-pane fade" id="Recibos" role="tabpanel" aria-labelledby="Recibos-tab">
                            <div class="containerRT4">
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group-append">
                                                <div class="input-group-append">
                                                    &nbsp;<a id="popupReciboLiq" href='#' data-image='searchRecibosLiq.aspx?popup=1&liq=<%= LiqId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col" style="flex-basis:2%;width:4%" >
                                        <div class="input-group-append">
                                            <asp:ImageButton ID="btnDeleteRecibos" data-target="#pnlModal" data-toggle="modal" OnClick="btnDeleteRecibos_Click" OnClientClick="Confirm();" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                                        </div>
                                    </div>
                                        <div class="col">
                                            <div class="input-group-append">
                                                <p>
                                                    <asp:FileUpload ID="fileUploadRecibos" CssClass="btn btn-outline-secondary" runat="server" />
                                                </p>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="input-group-append">
                                                <p>
                                                    <asp:Button ID="BtnImportRecibos" CssClass="btn btn-default btn-sm" Text="Importar Recibos" runat="server" OnClick="BtnImportRecibos_Click" />
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="gvRecibos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a id="popupRecibos" href='#' data-image='Recibo.aspx?Id=<%# Eval("Id") %>'>
                                                            <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="tramiteFolio" HeaderText="Tramite" />
                                                    <asp:BoundField DataField="numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="Cobertura" HeaderText="Cobertura" />
                                                    <asp:BoundField DataField="Vencimiento" HeaderText="Vencimiento" />
                                                    <asp:BoundField DataField="Total" HeaderText="Total" />
                                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                                    <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                                                </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                        </div>  
                        <div class="tab-pane fade" id="Cobranza" role="tabpanel" aria-labelledby="Cobranza-tab">
                            <div class="containerRT5">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalTransferencia" runat="server" Text="Total Transferencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalTransferencia" onchange="CalculateTotals()" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalTransferenciaVal" controltovalidate="txtTotalTransferencia" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalCheques" runat="server" Text="Total Cheques"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCheques" runat="server" onchange="CalculateTotals()" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalChequesVal" controltovalidate="txtTotalCheques" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalPrimasDep" runat="server" Text="Total primas en deposito"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalPrimasDep" runat="server" onchange="CalculateTotals()" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server"  id="txtTotalPrimasDepVal" controltovalidate="txtTotalPrimasDep" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalTC" runat="server" Text="Total Tarjeta Crédito"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalTC" runat="server" onchange="CalculateTotals()" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalTCVal" controltovalidate="txtTotalTC" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Totales" role="tabpanel" aria-labelledby="Totales-tab">
                            <div class="containerRT6">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalValores" runat="server" Text="Total valores"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalValores" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalValoresVal" controltovalidate="txtTotalValores" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalRecibos" runat="server" Text="Total Recibos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalRecibos" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalRecibosVal" controltovalidate="txtTotalRecibos" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalNC" runat="server" Text="Total N. Crédito"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalNC" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalNCVal" controltovalidate="txtTotalNC" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalLiquidacion" runat="server" Text="Total Liquidación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalLiquidacion" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalLiquidacionVal" controltovalidate="txtTotalLiquidacion" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDiferencia" runat="server" Text="Diferencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDiferencia" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtDiferenciaVal" controltovalidate="txtDiferencia" validationexpression="^(-?\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalCRecibos" runat="server" Text="Total C. Recibos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCRecibos" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalCRecibosVal" controltovalidate="txtTotalCRecibos" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalCNotas" runat="server" Text="Total C. Notas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCNotas" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalCNotasVal" controltovalidate="txtTotalCNotas" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalComision" runat="server" Text="Total Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalComision" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalComisionVal" controltovalidate="txtTotalComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                            </div>

                        </div>  
                        <div class="tab-pane fade" id="IntegracionPago" role="tabpanel" aria-labelledby="IntegracionPago-tab">
                          <div class="containerRT7">
                            <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblTotalPrima" runat="server" Text="Total Prima"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtTotalPrima" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblTotalImporteComision" runat="server" Text="Total Importe Comisión"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtTotalImporteComision" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                              <div class="row">
                                <div class="col">
                                    <div class="form-group input-group">
                                        <div class="input-group-append">
                                            <div class="col" style="flex-basis:30%" >
                                                <div class="input-group-append">
                                                    <asp:Button ID="btnIntegracion" OnClientClick="javascript:return false;" data-toggle="modal" data-target="#ModalIntegraciones" runat="server" Text="Añadir" />
                                                    <asp:Button ID="btnDownloadInt" OnClick="btnDownloadInt_Click" runat="server" Text="Descargar" />
                                                    
                                                </div>
                                            </div>
                                            <div class="col" style="flex-basis:30%" >
                                                <asp:ImageButton ID="btnDelIntegracion" data-target="#pnlModal" data-toggle="modal" OnClick="btnDelIntegracion_Click" OnClientClick="ConfirmdelIntegracion();" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="GvIntegracionesPago" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="Poliza" HeaderText="No Póliza o Carátula" />
                                                    <asp:BoundField DataField="Inciso" HeaderText="Inciso o Póliza" />
                                                    <asp:BoundField DataField="SecuenciaRecibo" HeaderText="Secuencia" />
                                                    <asp:BoundField DataField="NombreCliente" HeaderText="Forma de Pago" />
                                                    <asp:BoundField DataField="FormaPago" HeaderText="Nombre del Cliente" />
                                                    <asp:BoundField DataField="Prima" HeaderText="Prima Total" />
                                                    <asp:BoundField DataField="ImporteComision" HeaderText="Importe Comisión" />
                                                </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                          </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT8">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="LiqId" runat="server" />

                <div style="visibility:hidden">
                    <rsweb:reportviewer id="LiquidacionViewer" runat="server" width="1807px" font-names="Verdana" font-size="8pt" waitmessagefont-names="Verdana" waitmessagefont-size="14pt">
                    <LocalReport ReportPath="LiquidacionRpt.rdlc"></LocalReport></rsweb:reportviewer>
                </div>

                <div class="modal fade" id="exampleModal" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Imprimir liquidación</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="containerRT22" runat="server">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblSendEmail" runat="server" Text="Enviar por correo electronico"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtEmailSend" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Button ID="btnSendEmail" OnClick="btnSendEmail_Click" runat="server" Text="Enviar correo" />
                                            </div>
                                            <div class="col">
                                                <asp:Button ID="btnDescargar" OnClick="btnDescargar_Click" runat="server" Text="Descargar" />
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="ModalIntegraciones" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="">Importar Integración de Pago</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="containerRT20" runat="server">
                                    <div class="containerRT21">
                                        <div class="row">
                                            <div class="col" style="flex-basis:35%" >
                                                <div class="input-group-append">
                                                    <p>
                                                        <asp:FileUpload ID="fileUpload" CssClass="btn btn-outline-secondary" runat="server" />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col" style="flex-basis:15%">
                                                <div class="input-group-append">
                                                    <p>
                                                        <asp:Button ID="BtnIntegPAg" CssClass="btn btn-default btn-sm" Text="Importar Integración" runat="server" OnClick="BtnIntegPAg_Click" CausesValidation= "false" />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
