﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NegociacionList.aspx.cs" Inherits="CognisoWebApp.NegociacionList" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
    </script>

    <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Negociaciones</asp:Label></h2>
            </div>
            <hr class="float-left">

            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="#">Lista de Negociaciones</a></li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" runat="server" Height="30px" OnClick="btnNew_Click" Width="31px" />
                    <%--<asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/writing.png" runat="server" OnClick="BtnSave_Click" />--%>
                    <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                </div>
            </div>
            <div class="input-group mb-3">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id, No.Negociación, Cliente, Aseguradora o Ramo a buscar" aria-label="Id,No.Negociación o Cliente a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                <div class="input-group-append">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button>
                </div>
            </div>
            <div class="panel-body" runat="server">
                <div class="table-responsive" runat="server">
                    <asp:GridView ID="GVNegociacionList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a   href='Negociacion.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="Id" />
                            <asp:BoundField DataField="NumeroNegociacion" HeaderText="No. Negociación" />
                            <asp:BoundField DataField="Cliente" HeaderText="Cliente" />
                            <asp:BoundField DataField="Aseguradora" HeaderText="Aseguradora" />
                            <asp:BoundField DataField="Ramo" HeaderText="Ramo" />
                        </Columns>
                        <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="SearchId" runat="server" Value="" />
            </div>
        </div>
    </div>
</asp:Content>
