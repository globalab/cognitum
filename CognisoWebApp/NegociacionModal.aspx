﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NegociacionModal.aspx.cs" Inherits="CognisoWebApp.NegociacionModal" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Ramo</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <link href="/Content/cogniso.css" rel="stylesheet"/>

     <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCliente.ClientID%>").select2({
                placeholder: "Seleccione un Cliente",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtAseguradora.ClientID%>").select2({
                placeholder: "Seleccione una Aseguradora",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtRamo.ClientID%>").select2({
                placeholder: "Seleccione un Ramo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        function openpopup(url, title, btnname) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: title,
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $(btnname).click();
                }
            });
            $dialog.dialog('open');
        }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
        <div class="container">
        <div class="containerRT1" runat="server">
                 

            <div class="containerRT2" runat="server" id="NegociacionGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Polizas" role="tab" aria-controls="Polizas" aria-selected="false">Polizas</a>
                        </li>
                    
                        <li class="nav-item">
                            <a class="nav-link" id="Contacto-tab" data-toggle="tab" href="#Comentarios" role="tab" aria-controls="Comentarios" aria-selected="false">Comentarios</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">


                            <div class="containerRT3">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCliente" runat="server" class="form-control">
                                            </asp:DropDownList>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" runat="server" class="form-control"></asp:DropDownList>
                                          <!--  <div class="input-group-append">
                                                &nbsp;<a id="modifyAseguradora" href='#' onclick="openpopup('Aseguradora.aspx?id=<%=txtAseguradora.SelectedValue%>','Aseguradora','#<%=btnAseguradora.ClientID%>');" ><img src="/Content/Imgs/edit.png" width="25" height="25" /></a>     
                                                &nbsp;<a id="createAseguradora" href='#' onclick="openpopup('Aseguradora.aspx','Aseguradora','#<%=btnAseguradora.ClientID%>');" ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRamo" runat="server" Text="Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtRamo" runat="server" class="form-control"></asp:DropDownList>
                                            <!--<div class="input-group-append">
                                                &nbsp;<a href='#' onclick="openpopup('Ramo.aspx?id=<%=txtRamo.SelectedValue%>','Ramo','#<%=btnRamo.ClientID%>');" ><img src="/Content/Imgs/edit.png" width="25" height="25" /></a>     
                                                &nbsp;<a href='#' onclick="openpopup('Ramo.aspx','Ramo','#<%=btnRamo.ClientID%>');" ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorComision" runat="server" Text="% Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorComision" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorComisionVal" controltovalidate="txtPorComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorBono" runat="server" Text="% Bono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorBono" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorBonoVal" controltovalidate="txtPorBono" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorSobreComision" runat="server" Text="% Sobrecomisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorSobreComision" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorSobreComisionVal" controltovalidate="txtPorSobreComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPrimaNetaAprox" runat="server" Text="PNA Aproximada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrimaNetaAprox" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPrimaNetaAproxVal" controltovalidate="txtPrimaNetaAprox" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDiasNegociacionEntrega" runat="server" Text="Días negociación de entrega"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDiasNegociacionEntrega" TextMode="Number" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNoNegociacion" runat="server" Text="Número de negociación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNoNegociacion" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtNoNegociacion" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtNoNegociacion"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Polizas" role="tabpanel" aria-labelledby="Polizas-tab">
                            <div class="containerRT5">
                                <div class="panel-body" runat="server">
                                    <div class="table-responsive" runat="server">
                                        <asp:GridView ID="GVPolizasList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Contratante" HeaderText="Contratante" />
                                                <asp:BoundField DataField="Agente" HeaderText="Agente" />
                                                <asp:BoundField DataField="Ramo" HeaderText="Ramo" />
                                                <asp:BoundField DataField="Aseguradora" HeaderText="Aseguradora" />
                                                <asp:BoundField DataField="Ejecutivo" HeaderText="Ejecutivo" />
                                                <asp:BoundField DataField="EstatusPoliza" HeaderText="Estatus" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>

                        </div>
                    
                        <div class="tab-pane fade" id="Comentarios" role="tabpanel" aria-labelledby="Comentarios-tab">
                            <div class="containerRT6">

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblComentarios" runat="server" Text="Comentarios"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComentarios" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT7">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="NegId" runat="server" />
                <asp:HiddenField ID="CustRFC" runat="server" />
                <asp:Button ID="btnAseguradora" runat="server" OnClick="btnAseguradora_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="btnRamo" runat="server" OnClick="btnRamo_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
            </div>
        </div>
    </div>
 </form>
</body>
</html>  
