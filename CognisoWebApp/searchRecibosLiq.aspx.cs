﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class searchRecibosLiq : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string Id = Request.QueryString["liq"];
                if (!string.IsNullOrEmpty(Id))
                {
                    searchRecibosNoLiquidacion(Id);
                }
            }
        }

        private Entity initRecibosSearch()
        {
            Entity recibos = new Entity("recibo");
            recibos.useAuditable = false;
            recibos.Attributes.Add(new Models.Attribute("Id"));
            recibos.Attributes.Add(new Models.Attribute("Cobertura"));
            recibos.Attributes.Add(new Models.Attribute("Vencimiento"));
            recibos.Attributes.Add(new Models.Attribute("Concepto"));
            recibos.Attributes.Add(new Models.Attribute("Total"));
            recibos.Attributes.Add(new Models.Attribute("Numero"));
            recibos.Attributes.Add(new Models.Attribute("Estatus"));
            recibos.Attributes.Add(new Models.Attribute("Tipo"));
            recibos.Attributes.Add(new Models.Attribute("Comision"));

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Folio"));
            tramite.Attributes.Add(new Models.Attribute("id"));

            selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
            JoinEntity joinEnt = new JoinEntity();
            joinEnt.ChildEntity = tramite;
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.selectJoinList.Add(selJoin);

            recibos.ChildEntities.Add(joinEnt);

            Entity BienAsegurado = new Entity("BienAsegurado");
            BienAsegurado.Attributes.Add(new Models.Attribute("Identificador"));
            

            selectJoinEntity selJoin2 = new selectJoinEntity("inciso", 1, "id");
            JoinEntity joinEnt2 = new JoinEntity();
            joinEnt2.ChildEntity = BienAsegurado;
            joinEnt2.JoinType = JoinType.Left;
            joinEnt2.selectJoinList.Add(selJoin2);

            recibos.ChildEntities.Add(joinEnt2);

            return recibos;
        }

        private void searchRecibosNoLiquidacion(string id)
        {
            string ErrMsg = string.Empty;
            List<Entity> resultsEndosos;
            Entity liquidacion = new Entity("Liquidacion");
            liquidacion.Attributes.Add(new Models.Attribute("poliza"));
            liquidacion.Attributes.Add(new Models.Attribute("grupo"));
            liquidacion.Keys.Add(new Key("id", id, "int"));

            List<Entity> results = SiteFunctions.GetValues(liquidacion, out ErrMsg);

            string poliza = results[0].getAttrValueByName("poliza");
            string grupo = results[0].getAttrValueByName("grupo");
            Entity recibos = initRecibosSearch();
            List<Entity> result = new List<Entity>();

            Key filter = new Key("liquidacion", "", "int", whereOperator.IsNull,2);
            filter.LogicalOperator = 1;
            recibos.Keys.Add(filter);

            recibos.Keys.Add(new Key("Estatus", "0", "int", 1,2));
            Entity Endoso = new Entity("Endoso");
            Endoso.Attributes.Add(new Models.Attribute("Id"));
            Endoso.Keys.Add(new Key("Poliza", poliza, "int"));
            result = SiteFunctions.GetValues(Endoso, out ErrMsg);

            if (poliza != string.Empty)
            {
                recibos.Keys.Add(new Key("tramite", poliza, "int",2,1));
                foreach (Entity end in result)
                {
                    recibos.Keys.Add(new Key("tramite", end.getAttrValueByName("Id"), "int",2,1));
                }
            }
            else
            {
                Entity polizaEnt = new Entity("poliza");
                polizaEnt.Attributes.Add(new Models.Attribute("Id"));
                polizaEnt.Keys.Add(new Key("Grupo", grupo, "int", whereOperator.Equal));

                string filterByGrp = string.Empty;
                result = SiteFunctions.GetValues(polizaEnt, out ErrMsg);
                bool isFirst = true;
                Entity EndosoGrupo = new Entity("Endoso");
                foreach (Entity filterPoliza in result)
                {
                    resultsEndosos = new List<Entity>();
                    if (isFirst)
                    {
                        isFirst = false;
                        recibos.Keys.Add(new Key("tramite", filterPoliza.getAttrValueByName("id"), "int", 1, 2));
                        EndosoGrupo = new Entity("Endoso");
                        EndosoGrupo.Attributes.Add(new Models.Attribute("Id"));
                        EndosoGrupo.Keys.Add(new Key("Poliza", poliza, "int"));

                        resultsEndosos = SiteFunctions.GetValues(Endoso, out ErrMsg);
                        foreach (Entity Endosoaux in resultsEndosos)
                        {
                            recibos.Keys.Add(new Key("tramite", Endosoaux.getAttrValueByName("id"), "int", 2, 2));
                        }
                    }
                    else
                    {
                        recibos.Keys.Add(new Key("tramite", filterPoliza.getAttrValueByName("id"), "int", 2, 2));
                        EndosoGrupo = new Entity("Endoso");
                        EndosoGrupo.Attributes.Add(new Models.Attribute("Id"));
                        EndosoGrupo.Keys.Add(new Key("Poliza", poliza, "int"));

                        resultsEndosos = SiteFunctions.GetValues(Endoso, out ErrMsg);
                        foreach (Entity Endosoaux in resultsEndosos)
                        {
                            recibos.Keys.Add(new Key("tramite", Endosoaux.getAttrValueByName("id"), "int", 2, 1));
                        }
                    }
                }
            }

            if(txtSearchRecibos.Text.Trim() != string.Empty)
            {
                recibos.WhereStmt = "( Bienasegurado.Identificador like '%" + txtSearchRecibos.Text.Trim() + "%' or recibo.id like '%" + txtSearchRecibos.Text.Trim() + "%' or recibo.Numero like '%" + txtSearchRecibos.Text.Trim() + "%')";
            }
            result = SiteFunctions.GetValues(recibos, out ErrMsg);
            gvSearchRecibo.DataSource = SiteFunctions.trnsformEntityToDT(result);
            gvSearchRecibo.DataBind();
        }

        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            string Id = Request.QueryString["liq"];
            //foreach (GridViewRow row in gvSearchRecibo.Rows)
            //{
            //    CheckBox chkBox = row.FindControl("chkSel") as CheckBox;
            //    if (chkBox != null)
            //    {
            //        if (chkBox.Checked)
            //        {
            //            string ReciboId = gvSearchRecibo.Rows[row.RowIndex].Cells[1].Text;
            //            Entity recibo = new Entity("Recibo");
            //            recibo.Attributes.Add(new Models.Attribute("liquidacion", Id, "int"));
            //            recibo.Attributes.Add(new Models.Attribute("Estatus", "2", "int"));
            //            recibo.Keys.Add(new Key("Id", ReciboId, "int"));
            //            SiteFunctions.SaveEntity(recibo, RestSharp.Method.PUT, out errMsg);
                        
            //        }
            //    }
            //}
            string[] IDs = hdnFldSelectedValues.Value.Trim().Split('|');

            
            foreach (string Item in IDs)
            {
                string ReciboId = Item;
                Entity recibo = new Entity("Recibo");
                recibo.Attributes.Add(new Models.Attribute("liquidacion", Id, "int"));
                recibo.Attributes.Add(new Models.Attribute("Estatus", "2", "int"));
                recibo.Keys.Add(new Key("Id", ReciboId, "int"));
                SiteFunctions.SaveEntity(recibo, RestSharp.Method.PUT, out errMsg);
            }
            showMessage("Recibos agregados correctamente", false);
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void gvSearchRecibo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string Id = Request.QueryString["liq"];
            gvSearchRecibo.PageIndex = e.NewPageIndex;
            gvSearchRecibo.DataBind();
            searchRecibosNoLiquidacion(Id);
        }

        protected void gvSearchRecibo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
            {
                CheckBox chkBxSelect = (CheckBox)e.Row.Cells[0].FindControl("chkBxSelect");
                CheckBox chkBxHeader = (CheckBox)this.gvSearchRecibo.HeaderRow.FindControl("chkBxHeader");
                HiddenField hdnFldId = (HiddenField)e.Row.Cells[0].FindControl("hdnFldId");

                chkBxSelect.Attributes["onclick"] = string.Format("javascript:ChildClick(this,document.getElementById('{0}'),'{1}');", chkBxHeader.ClientID, hdnFldId.Value.Trim());
            }
        }

        protected void BtnSearchRecibos_Click(object sender, EventArgs e)
        {
            string Id = Request.QueryString["liq"];
            searchRecibosNoLiquidacion(Id);
        }

    }
}