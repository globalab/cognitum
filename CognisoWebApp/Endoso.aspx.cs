﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace CognisoWebApp
{
    public partial class Endoso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                string endosoId = Request.QueryString["Id"];
                string polizaId = Request.QueryString["poliza"];
                polizaId = string.IsNullOrEmpty(polizaId) ? string.Empty : polizaId;
                string EndosoType = Request.QueryString["endType"];
                EndosoTypeVal.Value = EndosoType;
                if (EndosoType == "2201")
                {
                    Recibotab.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                    IncisoAutosDiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                    IncisoEndosoD.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                    

                }
                else
                {
                    if (EndosoType != "2205")
                    {
                        IncisoEndosoD.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                        
                    }
                    else
                    {
                        //Recibotab.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                        IncisoAutosDiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                        
                    }
                }
                //if (EndosoType != "2205")
                //{
                //    EndososRel.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";

                //}
                if (!string.IsNullOrEmpty(polizaId))
                {
                    PoliId.Value = polizaId;
                }
                
                initForm();
                initCatalogos(EndosoType);
                if (endosoId != null)
                {
                    EndosoId.Value = endosoId;
                    initCartas(endosoId);
                    this.searchEndoso(endosoId);
                    if (EndosoType == "2205")
                    {
                        searchIncisosEndosoD();
                        buscarrecibos();
                    }
                    else
                    {
                        
                        buscarrecibos();
                        searchIncisosAutos();
                    }
                    
                }
                else
                {
                    initEndosofromPoliza(polizaId);
                }
                txtFormaPago.Enabled = false;
                valDescarteBtn();



            }
            else
            {
                if (openurlatstart.Value != "")
                {

                    string url = openurlatstart.Value + PoliId.Value + "&EndosoId=" + EndosoId.Value;
                    openurlatstart.Value = "";
                    // Define the name and type of the client scripts on the page.
                    String csname1 = "openpopup";
                    Type cstype = this.GetType();

                    // Get a ClientScriptManager reference from the Page class.
                    ClientScriptManager cs = Page.ClientScript;

                    // Check to see if the startup script is already registered.
                    if (!cs.IsStartupScriptRegistered(cstype, csname1))
                    {
                        StringBuilder cstext1 = new StringBuilder();
                        cstext1.Append(@"<script type='text/javascript'>openpopup('" + url + "','Incisos','#" + btnSearchIncisosAutos.ClientID + "')" + " </");
                        cstext1.Append("script>");
                        cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                    }




                }
                else
                {
                    this.showTab();
                }
            }
        }

        private void showTab()
        {

            // Define the name and type of the client scripts on the page.
            String csname1 = "TabScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> collapsediv(); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void initForm()
        {
            NuevoRecibo.Visible = false;
            txtOt.Enabled = false;
            //txtImpuesto.Enabled = false;
            txtEstatus.Enabled = false;
            txtTipo.Enabled = false;
            txtNoPoliza.Enabled = false;
            txtFolio.Enabled = false;
            initEnums();

        }
        private void searchEndoso(string _id)
        {
            string errMsg = string.Empty;
            string caseTipo = string.Empty;
            string valTipo = string.Empty, Id = string.Empty, poliza = string.Empty;
            List<Entity> entities = new List<Entity>();
            Entity Endoso = new Entity("Endoso");
            Endoso.useAuditable = true;
            Endoso.Keys.Add(new Models.Key("id", _id, "int"));

            List<Entity> endResult = SiteFunctions.GetValues(Endoso, out errMsg);
            if (endResult.Count > 0)
            {
                Entity endoso = endResult[0];

                //Entity Poliza = new Entity("Tramite");
                //Poliza.Attributes.Add(new Models.Attribute("Folio"));
                //Poliza.Keys.Add(new Key("Id", PoliId.Value, "int"));
                //List<Entity> listResult = SiteFunctions.GetValues(Poliza, out errMsg);
                //if (listResult.Count > 0)
                //{
                //    txtNoPoliza.Text = listResult[0].getAttrValueByName("Folio");
                //}
               
                txtTipo.SelectedValue = endoso.getAttrValueByName("Tipo");
                txtTipoMov.SelectedValue = endoso.getAttrValueByName("TipoMovimiento");
                txtDesc.Text = endoso.getAttrValueByName("Descripcion");
                txtImpuestoPor.SelectedValue = endoso.getAttrValueByName("Impuesto");
                //txtEndosoOriginal.Text = endoso.getAttrValueByName("EndosoOriginal");
                //txtEndosoNuevo.Text = endoso.getAttrValueByName("EndosoNuevo");
                txtInstrucciones.Text = endoso.getAttrValueByName("InstruccionesAseguradora");
                txtPrima.Text = endoso.getAttrValueByName("Prima");
                txtImpuesto.Text = endoso.getAttrValueByName("ImpuestoImporte");
                //llenar auditoria
                txtId.Text = _id;
                DateTime fechaaudit = Convert.ToDateTime(endoso.getAttrValueByName("FechaAdd"));
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(endoso.getAttrValueByName("FechaUMod"));
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", endoso.getAttrValueByName("UsuarioAdd"), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", endoso.getAttrValueByName("UsuarioUMod"), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                //fin llenar auditoria


            }
            Entity Tramite = new Entity("Tramite");
            Tramite.Keys.Add(new Models.Key("id", _id, "int"));
            Tramite.Attributes.Add(new Models.Attribute("FormaPago"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaInicio"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaFin"));
            Tramite.Attributes.Add(new Models.Attribute("Prima"));
            Tramite.Attributes.Add(new Models.Attribute("Gastos"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosPct"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Neto"));
            Tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Total"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionPct"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionImporte"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEmision"));
            Tramite.Attributes.Add(new Models.Attribute("FechaEnvio"));
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            Tramite.Attributes.Add(new Models.Attribute("Descripcion"));
            Tramite.Attributes.Add(new Models.Attribute("Id"));

            entities.Add(Tramite);
            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            mapTramiteSearch(result);
            if (txtFolio.Text.Trim() != "")
            {
                NuevoRecibo.Visible = true;
            }
            //if (txtTipo.SelectedValue.ToString() != "2201")
            //{
            //    Entity bienAsegurado = new Entity("BienAsegurado");
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Permiso"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Propietario"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Inciso"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Estatus"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Prima"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Identificador"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Poliza"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("BeneficiarioPreferente"));
            //    bienAsegurado.Attributes.Add(new Models.Attribute("Id"));
                                
            //    bienAsegurado.Keys.Add(new Models.Key("EndosoAlta", _id, "int"));
            //    bienAsegurado = SiteFunctions.GetEntity(bienAsegurado, out errMsg);
            //    if (bienAsegurado != null)
            //    {
            //      txtInciso.Text = bienAsegurado.getAttrValueByName("Inciso");
            //      #region mapea auto
            //        Entity auto = new Entity("auto");
            //        auto.Attributes.Add(new Models.Attribute("Serie"));
            //        auto.Attributes.Add(new Models.Attribute("Motor"));
            //        auto.Attributes.Add(new Models.Attribute("Placas"));
            //        auto.Attributes.Add(new Models.Attribute("ConductorHabitual"));
            //        auto.Attributes.Add(new Models.Attribute("Modelo"));
            //        auto.Attributes.Add(new Models.Attribute("Version"));
            //        auto.Attributes.Add(new Models.Attribute("Marca"));
            //        auto.Attributes.Add(new Models.Attribute("Poliza"));
            //        auto.Keys.Add(new Key("Id",bienAsegurado.getAttrValueByName("Id"),"int"));
            //        auto = SiteFunctions.GetEntity(auto,out errMsg);
            //        if(auto != null)
            //        {
            //            txtSerie.Text = auto.getAttrValueByName("Serie");
            //            txtMotor.Text = auto.getAttrValueByName("Motor");
            //            txtPlacas.Text = auto.getAttrValueByName("Placas");
            //            txtConductor.Text = auto.getAttrValueByName("ConsuctorHabitual");
            //            txtModelo.Text = auto.getAttrValueByName("Modelo");
            //            txtVersion.Text = auto.getAttrValueByName("Version");
            //            txtMarca.SelectedValue = auto.getAttrValueByName("Marca");

            //        }
            //      #endregion
            //    }
                

                
            //}
        }
        private void mapTramiteSearch(List<OperationResult> result)
        {
            string errMsg = string.Empty;

            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if (opResult.RetVal.Count > 0)
                    {
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {

                            case "tramite":
                                txtFormaPago.SelectedValue = entity.getAttrValueByName("FormaPago");
                                DateTime fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaInicio"));
                                txtVigenciaInicio.Text = fecha.ToString("dd/MM/yyyy");
                                fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaFin"));
                                txtVigenciaFin.Text = fecha.ToString("dd/MM/yyyy");
                                txtPrima.Text = entity.getAttrValueByName("Prima");
                                txtGastos.Text = entity.getAttrValueByName("Gastos");
                                txtRecargoPor.Text = entity.getAttrValueByName("RecargosPct");
                                txtRecargosImporte.Text = entity.getAttrValueByName("RecargosImporte");
                                txtNeto.Text = entity.getAttrValueByName("Neto");
                                txtImpuesto.Text = entity.getAttrValueByName("ImpuestoImporte");
                                txtTotal.Text = entity.getAttrValueByName("Total");
                                txtComisionPor.Text = entity.getAttrValueByName("ComisionPct");
                                txtComisionImporte.Text = entity.getAttrValueByName("ComisionImporte");
                                txtFechaEnvio.Text = entity.getAttrValueByName("FechaEnvio");
                                txtFechaEmision.Text = entity.getAttrValueByName("FechaEmision");
                                txtDesc.Text = entity.getAttrValueByName("Descripcion");
                                txtFolio.Text = entity.getAttrValueByName("Folio");
                                txtOt.Text = entity.getAttrValueByName("Id");//revisar
                                break;

                        }
                    }
                }
            }

        }
        private void initCatalogos(string EndosoType)
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            txtFechaEnvio.Text = DateTime.Now.ToShortDateString();
            txtFechaEmision.Text = DateTime.Now.ToShortDateString();
            txtVigenciaInicio.Text = DateTime.Now.ToShortDateString();
            txtVigenciaFin.Text = DateTime.Now.AddYears(1).ToShortDateString();
            txtTipo.SelectedValue = EndosoType;

            Entity Poliza = new Entity("Tramite");
            Poliza.Attributes.Add(new Models.Attribute("Folio"));
            Poliza.Keys.Add(new Key("Id", PoliId.Value, "int"));
            listResult = SiteFunctions.GetValues(Poliza, out errmess);
            if (listResult.Count > 0)
            {
                txtNoPoliza.Text = listResult[0].getAttrValueByName("Folio");
            }
             


            Entity tax = new Entity("impuesto");
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            tax.Attributes.Add(new CognisoWebApp.Models.Attribute("Porcentaje"));
            tax.useAuditable = false;
            listResult = SiteFunctions.GetValues(tax, out errmess);
            txtImpuestoPor.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtImpuestoPor.DataValueField = "Id";
            txtImpuestoPor.DataTextField = "Nombre";
            txtImpuestoPor.DataBind();
            

            //Entity Marca = new Entity("Marca");
            //Marca.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            //Marca.Attributes.Add(new CognisoWebApp.Models.Attribute("Descripcion"));
            //Marca.useAuditable = false;
            //listResult = SiteFunctions.GetValues(Marca, out errmess);
            //txtMarca.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            //txtMarca.DataValueField = "Id";
            //txtMarca.DataTextField = "Descripcion";
            //txtMarca.DataBind();

            //txtMarca.Items.Add(new ListItem("", ""));
            //txtMarca.SelectedValue = "";

            filterTipoMovimiento();
        }

        private void initEndosofromPoliza(string Polid)
        {
            Entity Tramite = new Entity("Tramite");
            Tramite.Attributes.Add(new Models.Attribute("FormaPago", "", "int"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionPct", "", "int"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosPct", "", "int"));
            
            
            Tramite.Keys.Add(new Key("Id", Polid, "int"));
            string errMsg = string.Empty;
            List<Entity> result = SiteFunctions.GetValues(Tramite, out errMsg);
            if (result.Count > 0)
            {
                try
                {
                    txtFormaPago.SelectedValue = result[0].getAttrValueByName("FormaPago").ToString();
                    bool VP = txtFormaPago.Text.Contains("VP");
                    if (VP)
                    {
                        
                        txtPrimaNeta.Enabled = false;
                        txtGastosRecibo.Enabled = false;
                        
                    }
                    else
                    {
                        
                        txtPrimaNeta.Enabled = true;
                        txtGastosRecibo.Enabled = true;
                        
                    }
                    
                    txtComisionPor.Text = result[0].getAttrValueByName("ComisionPct").ToString();
                    txtRecargoPor.Text = result[0].getAttrValueByName("RecargosPct").ToString();
                }
                catch (Exception)
                {
                }
            }
        }
        private void filterTipoMovimiento()
        {
            Entity TipoMovimiento = new Entity("TipoMovimiento");
            TipoMovimiento.Attributes.Add(new Models.Attribute("Id", "", "int"));
            TipoMovimiento.Attributes.Add(new Models.Attribute("Descripcion", "", "string"));
            TipoMovimiento.Keys.Add(new Key("TipoEndoso", txtTipo.SelectedValue, "int"));

            string errMsg = string.Empty;
            List<Entity> result = SiteFunctions.GetValues(TipoMovimiento, out errMsg);

            txtTipoMov.DataSource = SiteFunctions.trnsformEntityToDT(result);
            txtTipoMov.DataTextField = "Descripcion";
            txtTipoMov.DataValueField = "Id";
            txtTipoMov.DataBind();
        }

        private void initEnums()
        {
            Array itemValues = System.Enum.GetValues(typeof(EstatusEndosoEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusEndosoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }

            itemValues = System.Enum.GetValues(typeof(StatusMesaControl));
            itemNames = System.Enum.GetNames(typeof(StatusMesaControl));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtMesa.Items.Add(item);
            }
            itemValues = System.Enum.GetValues(typeof(FormaPagoEnum));
            itemNames = System.Enum.GetNames(typeof(FormaPagoEnum));
            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtFormaPago.Items.Add(item);
            }
            itemValues = System.Enum.GetValues(typeof(TipoEndosoEnum));
            itemNames = System.Enum.GetNames(typeof(TipoEndosoEnum));
            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtTipo.Items.Add(item);
            }



        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveEndoso();

        }
        private void SaveEndoso()
        {
            object id = EndosoId.Value.Trim() == "" ? null : EndosoId.Value.Trim();
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            JoinEntity joinEntity = new JoinEntity();
            Entity Endoso = new Entity("Endoso");
            Auditable audi = new Auditable();
            audi.userId = int.Parse(userId);
            audi.opDate = DateTime.Now;
            #region "Map Tramite
            Entity tramite = new Entity("Tramite");
            tramite.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
            tramite.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
            tramite.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
            tramite.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            if (txtFolio.Text.Trim() != string.Empty)
            {
                tramite.Attributes.Add(new Models.Attribute("Folio", txtFolio.Text, "string"));
            }
            else
            {
                tramite.Attributes.Add(new Models.Attribute("Folio", PoliId.Value, "string"));
                
            }
            tramite.Attributes.Add(new Models.Attribute("FormaPago", txtFormaPago.SelectedValue, "int"));
            tramite.Attributes.Add(new Models.Attribute("VigenciaInicio", txtVigenciaInicio.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("VigenciaFin", txtVigenciaFin.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("Total", txtTotal.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionPct", txtComisionPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("ComisionImporte", txtComisionImporte.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosImporte", txtRecargosImporte.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("RecargosPct", txtRecargoPor.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Neto", txtNeto.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Prima", txtPrima.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Gastos", txtGastos.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("FechaEnvio", txtFechaEnvio.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("FechaEmision", txtFechaEmision.Text, "datetime"));
            tramite.Attributes.Add(new Models.Attribute("EstatusMesaDeControl", txtMesa.SelectedValue, "int"));
            tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            tramite.Attributes.Add(new Models.Attribute("Descripcion", txtDesc.Text, "string"));
            tramite.useAuditable = true;
            tramite.auditable = audi;
            #endregion
            #region "Map endoso"

            Endoso.Attributes.Add(new Models.Attribute("EstatusEndoso", txtEstatus.SelectedValue, "int"));
            Endoso.Attributes.Add(new Models.Attribute("Tipo", txtTipo.SelectedValue, "int"));
            Endoso.Attributes.Add(new Models.Attribute("TipoMovimiento", txtTipoMov.SelectedValue, "int"));
            Endoso.Attributes.Add(new Models.Attribute("Poliza", PoliId.Value, "int"));
            Endoso.Attributes.Add(new Models.Attribute("Prima", txtPrima.Text, "float"));//
            Endoso.Attributes.Add(new Models.Attribute("Impuesto", txtImpuestoPor.SelectedValue, "int"));
            //Endoso.Attributes.Add(new Models.Attribute("EndosoOriginal", txtEndosoOriginal.Text, "int"));
            //Endoso.Attributes.Add(new Models.Attribute("EndosoNuevo", txtEndosoNuevo.Text, "int"));
            Endoso.Attributes.Add(new Models.Attribute("ImpuestoImporte", txtImpuesto.Text, "float"));
            Endoso.Attributes.Add(new Models.Attribute("InstruccionesAseguradora", txtInstrucciones.Text, "string"));
            Endoso.useAuditable = true;
            #endregion
            //if (txtTipo.SelectedValue.ToString() != "2201")
            //{
            //    #region mapea bien asegurado
            //    Entity bienAsegurado = this.llenaBienAsegurado(txtInciso.Text, txtPrima.Text, txtBeneficiarioAuto.SelectedValue, txtInciso.Text);
                
            //    bienAsegurado.auditable = audi;
            //    joinEntity = new JoinEntity();
            //    joinEntity.ChildEntity = bienAsegurado;
            //    joinEntity.JoinKey = new Models.Attribute("EndosoAlta", "", "int");
            //    Endoso.ChildEntities.Add(joinEntity);
            //    #endregion
            //    #region mapea auto
            //    Entity auto = new Entity("auto");
            //    auto.Attributes.Add(new Models.Attribute("Serie", txtSerie.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Motor", txtMotor.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Placas", txtPlacas.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("ConductorHabitual", txtConductor.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Modelo", txtModelo.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Version", txtVersion.Text, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Marca", txtMarca.SelectedValue, "string"));
            //    auto.Attributes.Add(new Models.Attribute("Poliza", PoliId.Value.ToString(), "int"));
            //    joinEntity = new JoinEntity();
            //    joinEntity.ChildEntity = auto;
            //    joinEntity.JoinKey = new Models.Attribute("id", "", "int");
            //    bienAsegurado.ChildEntities.Add(joinEntity);

            //    #endregion
            
            //    if (id != null)
            //    {
            //        bienAsegurado.Attributes.Add(new Models.Attribute("EndosoAlta", id.ToString(), "int"));

            //    }
                
            //}
            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = Endoso;
            joinEntity.JoinKey = new Models.Attribute("id", "", "int");
            tramite.ChildEntities.Add(joinEntity);
            
            string errmess = "";
            string idEndoso = string.Empty;
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                tramite.Keys.Add(new Models.Key("Id", id.ToString(), "int"));
                audi.entityId = Convert.ToInt32(id);
                idEndoso = id.ToString();

            }
            Endoso.auditable = audi;

            if (SiteFunctions.SaveEntity(tramite, method, out errmess))
            {
                idEndoso = idEndoso == string.Empty ? errmess : idEndoso;
                EndosoId.Value = idEndoso;
                initCartas(idEndoso);
                txtOt.Text = idEndoso;
                valDescarteBtn();
                if (txtFolio.Text.Trim() == string.Empty)
                {
                    txtFolio.Text = EndosoId.Value;
                }

                showMessage("Endoso guardado correctamente", false);
            }
            else
            {
                showMessage(errmess, true);
            }

            

        }

        private Entity llenaBienAsegurado(string _inciso, string _prima, string _beneficiario, string _identificador)
        {
            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.useAuditable = true;

            bienAsegurado.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Propietario", "2343", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Inciso", _inciso, "string"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Prima", _prima, "float"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Identificador", _identificador, "string"));
            bienAsegurado.Attributes.Add(new Models.Attribute("Poliza", PoliId.Value, "int"));
            if (_beneficiario != string.Empty)
                bienAsegurado.Attributes.Add(new Models.Attribute("BeneficiarioPreferente", _beneficiario, "int"));
            return bienAsegurado;
        }

        protected void btnBuscarRecibos_Click(object sender, EventArgs e)
        {
            buscarrecibos();
        }

        private void buscarrecibos()
        {
            PaneName.Value = "Recibos";
            object id = EndosoId.Value;
            SearchIdRecibo.Value = string.Empty;
            if (txtSearchRecibos.Text != string.Empty)
            {
                SearchIdRecibo.Value = '%' + txtSearchRecibos.Text + '%';
            }
            DataTable dtRecibo = searchRelRecords(recordType.Recibo, SearchIdRecibo.Value, id.ToString());
            System.Data.DataColumn newColumn = new System.Data.DataColumn("tramiteFolio", typeof(System.String));
            newColumn.DefaultValue = txtNoPoliza.Text;
            dtRecibo.Columns.Add(newColumn);
            gvRecibos.DataSource = dtRecibo;
            gvRecibos.DataBind();
        }
        private void valDescarteBtn()
        {
            object id = EndosoId.Value.Trim() == "" ? null : EndosoId.Value;
            if (id != null && txtEstatus.SelectedValue == "0")
            {
                BtnDescarte.Enabled = true;
            }
            else
            {
                BtnDescarte.Enabled = false;
            }
        }




        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        [WebMethod]
        public static string getAmountImpuesto(string idporcentaje, string neto)
        {
            string result = string.Empty;

            Entity Impuesto = new Entity("Impuesto");
            Impuesto.Keys.Add(new Models.Key("id", idporcentaje, "int"));
            Impuesto.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
            string errMsg = string.Empty;
            List<Entity> resultado = SiteFunctions.GetValues(Impuesto, out errMsg);

            if (resultado.Count > 0)
            {
                Entity impuesRes = resultado[0];

                var porcentaje = impuesRes.getAttrValueByName("Porcentaje");
                if (neto != "")
                {
                    float primaval = float.Parse(neto);
                    result = ((primaval * float.Parse(porcentaje)) / 100).ToString();
                }

            }

            return result;
        }

        protected void btnDescarteOk_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            decimal primaAmt = 0M;
            decimal.TryParse(txtPrima.Text, out primaAmt);
            object id = EndosoId.Value.Trim() == "" ? null : EndosoId.Value;
            string EndosoType = Request.QueryString["endType"];
            
                if (primaAmt == 0M)
                {
                    showMessage("La prima no ha sido capturada", true);
                    return;
                }
                if (txtPoliza.Text.Trim() == "")
                {
                    showMessage("Debe de capturar el número de endoso.", true);
                    return;
                }
                if (txtPoliza.Text.Trim() != "")
                {
                    Entity Tramiteval = new Entity("Tramite");
                    Tramiteval.Attributes.Add(new Models.Attribute("Id"));
                    Tramiteval.Keys.Add(new Key("Folio", txtPoliza.Text.Trim(), "string"));
                    Tramiteval = SiteFunctions.GetEntity(Tramiteval, out errMsg);
                    if (Tramiteval != null)
                    {
                        showMessage("El número de endoso ya existe.", true);
                        return;
                    }
                }
                if (id != null)
                {
                    Entity tramite = new Entity("Tramite");
                    tramite.Attributes.Add(new Models.Attribute("id", "", "int"));
                    tramite.Attributes.Add(new Models.Attribute("comisionpct", "", "float"));
                    tramite.Keys.Add(new Key("Id", txtOt.Text, "int"));

                    List<Entity> result = SiteFunctions.GetValues(tramite, out errMsg);
                    if (result.Count > 0)
                    {
                        if (txtComisionPor.Text == result[0].getAttrValueByName("comisionpct"))
                        {
                            int numticketsTxt = 0;
                            int numTickets = 0, numSeries = 0, intervalPerTicket = 0;
                            getNumTickets(out numTickets, out numSeries, out intervalPerTicket);

                            int.TryParse(txtNumRecibos.Text, out numticketsTxt);

                            if (numTickets >= numticketsTxt)
                            {
                                numTickets = numticketsTxt != 0 ? numticketsTxt : numTickets;
                                descartaOT(numTickets, numSeries, intervalPerTicket);

                            }
                            else
                            {
                                showMessage("El número de recibos capturados no concuerda con la forma de pago de la OT", true);
                            }
                        }
                        else
                        {
                            showMessage("La comisión de la OT no es igual a la de la negociación asociada", true);
                        }
                    }
                    else
                    {
                        showMessage("La OT no cuenta con negociación asociada", true);
                    }
                }
            


        }
        private void getNumTickets(out int numTickets, out int numSeries, out int intervalPerTicket)
        {
            numTickets = 0;
            numSeries = 0;
            intervalPerTicket = 0;

            int formaPago = 0;

            int.TryParse(txtFormaPago.SelectedValue, out formaPago);

            FormaPagoEnum formaPagoSelected = (FormaPagoEnum)formaPago;

            switch (formaPagoSelected)
            {
                case FormaPagoEnum.Mensual:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Bimestral:
                    numTickets = 6;
                    numSeries = 1;
                    intervalPerTicket = 2;
                    break;
                case FormaPagoEnum.Trimestral:
                    numTickets = 4;
                    numSeries = 1;
                    intervalPerTicket = 3;
                    break;
                case FormaPagoEnum.Cuatrimestral:
                    numTickets = 3;
                    numSeries = 1;
                    intervalPerTicket = 4;
                    break;
                case FormaPagoEnum.Semestral:
                    numTickets = 2;
                    numSeries = 1;
                    intervalPerTicket = 6;
                    break;
                case FormaPagoEnum.Anual:
                    numTickets = 1;
                    numSeries = 1;
                    intervalPerTicket = 0;
                    break;
                //validar como sacar la serie y si aplica para todas las ots
                case FormaPagoEnum.Mensual_VP:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Trimestral_VP:
                    numTickets = 4;
                    numSeries = 1;
                    intervalPerTicket = 3;
                    break;
                case FormaPagoEnum.Semestral_VP:
                    numTickets = 2;
                    numSeries = 1;
                    intervalPerTicket = 6;
                    break;
                case FormaPagoEnum.Contado_VP:
                    numTickets = 1;
                    numSeries = 1;
                    intervalPerTicket = 0;
                    break;
                //validar como sacar la serie y si aplica para todas las ots
                case FormaPagoEnum.Mensual_DxN:
                    numTickets = 12;
                    numSeries = 1;
                    intervalPerTicket = 1;
                    break;
                case FormaPagoEnum.Quincenal_DxN:
                    numTickets = 24;
                    numSeries = 1;
                    intervalPerTicket = 15;
                    break;
                case FormaPagoEnum.Catorcenal_DxN:
                    numTickets = 26;
                    numSeries = 1;
                    intervalPerTicket = 14;
                    break;
            }
        }
        private void descartaOT(int numTickets, int numSeries, int intervalPerTicket)
        {
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            Auditable audit = new Auditable();
            audit.userId = int.Parse(userId);
            audit.opDate = DateTime.Now;

            float totalComisiones = 0;
            string errMsg = string.Empty;
            object id = EndosoId.Value.Trim() == "" ? null : EndosoId.Value;
            string Tipo = "0";
            if (txtTipo.SelectedValue == "2205")
            {
                Tipo = "1";
            }
            if (txtComisionImporte.Text != string.Empty)
            {
                float.TryParse(txtComisionImporte.Text, out totalComisiones);
            }
            bool success = false;
            Entity TipoEndoso = new Entity("TipoEndoso");
            TipoEndoso.Keys.Add(new Key("Id", txtTipo.SelectedValue, "int"));
            TipoEndoso.Attributes.Add(new CognisoWebApp.Models.Attribute("GenerarRecibo"));
            TipoEndoso = SiteFunctions.GetEntity(TipoEndoso, out errMsg);
            if(Convert.ToBoolean(TipoEndoso.getAttrValueByName("GenerarRecibo")))
            {
                Entity taxEntity = new Entity("Impuesto");
                taxEntity.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
                taxEntity.Keys.Add(new Key("Id", txtImpuestoPor.SelectedValue, "int"));
                List<Entity> results = SiteFunctions.GetValues(taxEntity, out errMsg);


                Entity Poliza = new Entity("Poliza");
                Poliza.Attributes.Add(new Models.Attribute("Moneda", "", "int"));
                Poliza.Keys.Add(new Key("Id", PoliId.Value, "int"));
                List<Entity> resultsPol = SiteFunctions.GetValues(Poliza, out errMsg);
                bool VP = txtFormaPago.Text.Contains("VP");
                
                if (txtgenRecibos.Checked)
                {
                    for (int cntSeries = 0; cntSeries < numSeries; cntSeries++)
                    {
                        DateTime VigenciaInicio = DateTime.Now;
                        DateTime.TryParse(txtVigenciaInicio.Text, out VigenciaInicio);

                        success = SiteFunctions.proccessTickets(float.Parse(txtPrima.Text == "" ? "0" : txtPrima.Text), numTickets, VigenciaInicio, intervalPerTicket, totalComisiones, float.Parse(txtGastos.Text == "" ? "0" : txtGastos.Text), float.Parse(txtPrimaNeta.Text == "" ? "0" : txtPrimaNeta.Text), float.Parse(txtGastosRecibo.Text == "" ? "0" : txtGastosRecibo.Text), float.Parse(results[0].getAttrValueByName("Porcentaje")), cntSeries + 1, int.Parse(resultsPol[0].getAttrValueByName("Moneda")), int.Parse(txtImpuestoPor.SelectedValue), int.Parse(id.ToString()), 2185, 4, int.Parse(userId), audit, out errMsg, false,txtComisionPor.Text,txtRecargoPor.Text,"","", 0, VP,1,Tipo);
                        if (!success)
                        {
                            showMessage(errMsg, true);
                            return;
                        }
                    }
                }
            }
            Entity poliza = new Entity("Tramite");
            poliza.Attributes.Add(new Models.Attribute("Folio", txtPoliza.Text, "string"));
            poliza.Keys.Add(new Key("Id", id.ToString(), "string"));

            success = SiteFunctions.SaveEntity(poliza, Method.PUT, out errMsg);

            if (success)
            {
                Entity Endoso = new Entity("Endoso");
                Endoso.Attributes.Add(new Models.Attribute("EstatusEndoso", "1", "int"));
                Endoso.Keys.Add(new Key("Id", id.ToString(), "int"));
                success = SiteFunctions.SaveEntity(Endoso, Method.PUT, out errMsg);
                if(success)
                {
                    BtnDescarte.Visible = false;
                    showMessage("Desacarte correcto.", false);
                    txtFolio.Text = txtPoliza.Text;
                    NuevoRecibo.Visible = true;
                    foreach (GridViewRow row in gvIncisoEndosoD.Rows)
                    {
                        if (txtTipo.SelectedValue == "2205")
                        {
                            string IncisoId = gvIncisoEndosoD.Rows[row.RowIndex].Cells[1].Text;
                            Entity Inciso = new Entity("BienAsegurado");

                            Inciso.Attributes.Add(new Models.Attribute("Estatus", "5", "int"));
                            Inciso.Keys.Add(new Key("Id", IncisoId, "int"));
                            SiteFunctions.SaveEntity(Inciso, RestSharp.Method.PUT, out errMsg);
                        }



                    }
                    buscarrecibos();
                }
                else
                {
                    showMessage(errMsg, true);
                }
            }
            else
            {
                showMessage(errMsg, true);
            }
        }

        private DataTable searchRelRecords(recordType _recordType, string _searchText, string _polizaId)
        {
            string errMsg = string.Empty;
            Entity entity = new Entity();

            switch (_recordType)
            {
                case recordType.Dependientes:
                    entity.EntityName = "bienasegurado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    Entity certi = new Entity("Certificado");
                    certi.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("ApellidoPaterno", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("ApellidoMaterno", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("FechaNacimiento", "", "string"));
                    certi.Attributes.Add(new Models.Attribute("Parentesco", "", "string"));

                    selectJoinEntity selJoin = new selectJoinEntity("id", 1, "id");
                    JoinEntity joinEntityCert = new JoinEntity();
                    joinEntityCert.ChildEntity = certi;
                    joinEntityCert.selectJoinList = new List<selectJoinEntity>();
                    joinEntityCert.selectJoinList.Add(selJoin);
                    joinEntityCert.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityCert);
                    break;
                case recordType.Endoso:
                    entity.EntityName = "Endoso";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("EstatusEndoso", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("ImpuestoImporte", "", "float"));
                    entity.Attributes.Add(new Models.Attribute("Prima", "", "float"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "string", 1));
                    }
                    break;
                case recordType.IncisoAutos:
                    entity.EntityName = "BienAsegurado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Prima", "", "float"));
                    entity.Attributes.Add(new Models.Attribute("Poliza"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    entity.Keys.Add(new Key("EndosoAlta", EndosoId.Value, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "string", 1));
                    }
                    Entity auto = new Entity("Auto");
                    auto.Attributes.Add(new Models.Attribute("Serie", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Motor", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Placas", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Modelo", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Marca", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("ConductorHabitual", "", "string"));
                    auto.Attributes.Add(new Models.Attribute("Version", "", "string"));

                    selectJoinEntity selJoinAuto = new selectJoinEntity("id", 1, "id");
                    JoinEntity joinEntityAuto = new JoinEntity();
                    joinEntityAuto.ChildEntity = auto;
                    joinEntityAuto.selectJoinList = new List<selectJoinEntity>();
                    joinEntityAuto.selectJoinList.Add(selJoinAuto);
                    joinEntityAuto.JoinType = JoinType.Inner;
                    entity.ChildEntities.Add(joinEntityAuto);
                    break;
                case recordType.Comision:
                    entity.EntityName = "ComisionAsociado";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("DirectorAsociado", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Porcentaje", "", "float"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "int", 1));
                    }

                    Entity dirAsociado = new Entity("DirectorAsociado");
                    dirAsociado.Attributes.Add(new Models.Attribute("Usuario", "", "int"));

                    Entity usuario = new Entity("Usuario");
                    usuario.Attributes.Add(new Models.Attribute("Nombre", "", "string"));

                    selectJoinEntity selJoinEntity = new selectJoinEntity("DirectorAsociado", 1, "Id");

                    JoinEntity joinEntity = new JoinEntity();
                    joinEntity.selectJoinList = new List<selectJoinEntity>();
                    joinEntity.selectJoinList.Add(selJoinEntity);
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.ChildEntity = dirAsociado;
                    entity.ChildEntities.Add(joinEntity);

                    selJoinEntity = new selectJoinEntity("Usuario", 1, "Id");

                    joinEntity = new JoinEntity();
                    joinEntity.selectJoinList = new List<selectJoinEntity>();
                    joinEntity.selectJoinList.Add(selJoinEntity);
                    joinEntity.JoinType = JoinType.Inner;
                    joinEntity.ChildEntity = usuario;
                    dirAsociado.ChildEntities.Add(joinEntity);

                    break;
                case recordType.Recibo:
                    entity.EntityName = "Recibo";
                    entity.Keys.Add(new Models.Key("Tramite", _polizaId, "int"));
                    entity.Attributes.Add(new Models.Attribute("Id"));
                    entity.Attributes.Add(new Models.Attribute("Numero"));
                    entity.Attributes.Add(new Models.Attribute("Total"));
                    entity.Attributes.Add(new Models.Attribute("Concepto"));
                    entity.Attributes.Add(new Models.Attribute("FechaPagoComision"));
                    entity.Attributes.Add(new Models.Attribute("PrimaNeta"));
                    entity.Attributes.Add(new Models.Attribute("Gastos"));
                    entity.Attributes.Add(new Models.Attribute("Recargo"));
                    entity.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
                    entity.Attributes.Add(new Models.Attribute("Recargo"));
                    entity.Attributes.Add(new Models.Attribute("Estatus"));
                    entity.Attributes.Add(new Models.Attribute("Cobertura"));
                    entity.Attributes.Add(new Models.Attribute("Vencimiento"));
                    entity.Attributes.Add(new Models.Attribute("Tipo"));
                    if (_searchText != string.Empty)
                        //  entity.Keys.Add(new Models.Key("id", SearchIdRecibo.Value, "string"));
                        entity.Keys.Add(new Models.Key("id", "", "string"));
                    break;
                case recordType.Siniestro:
                    entity.EntityName = "Siniestro";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("Numero", "", "int"));
                    entity.Attributes.Add(new Models.Attribute("FechaSiniestro", "", "datetime"));
                    entity.Attributes.Add(new Models.Attribute("FechaReporte", "", "datetime"));
                    entity.Attributes.Add(new Models.Attribute("FechaFiniquito", "", "datetime"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    if (_searchText != string.Empty)
                    {
                        entity.Keys.Add(new Key("Id", _searchText, "int", 1));
                    }
                    break;
                case recordType.VPGrupos:
                    entity.EntityName = "GrupoAsegurados";
                    entity.Attributes.Add(new Models.Attribute("Id", "", "string"));
                    entity.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                    entity.Attributes.Add(new Models.Attribute("Descripcion", "", "string"));
                    entity.Keys.Add(new Key("Poliza", _polizaId, "int"));
                    break;
            }

            List<Entity> polResult = SiteFunctions.GetValues(entity, out errMsg);
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(polResult);
            return dtRes;
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            if (txtEmailSend.Text != "" && txtSubject.Text != "")
            {
                string HTML = generaCartaHTML();
                HTML = HTML.Replace("src=\"imgs/", "src=\"");
                SiteFunctions sf = new SiteFunctions();
                sf.sendEmail(txtEmailSend.Text, txtSubject.Text, HTML, Server.MapPath("~/imgs/cabecera.jpg"));
            }
        }

        protected void btnDescargar_Click(object sender, EventArgs e)
        {
            string HTML = generaCartaHTML();
            HTML = HTML.Replace("src=\"", "src=\"" + ConfigurationManager.AppSettings["APPUrl"].ToString());
            string Guid = System.Guid.NewGuid().ToString();
            string _filename = Server.MapPath("~/Reportes/Polizas/" + Guid + ".pdf");
            

            var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(HTML, PdfSharp.PageSize.A4);
            pdf.Save(_filename);

            String csname1 = "downloadRpt";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;
            string url = "Reportes/Polizas/" + Guid + ".pdf";
            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> window.open('" + url + "'); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }     
        }

        private void initCartas(string id)
        {
            List<Entity> plantillaspol = SearchPlantillas.ObtienePlantillasEndoso(id);
            if (plantillaspol.Count == 0)
            {
                plantillaspol = SearchPlantillas.ObtienePlantillasEndosoSinFiltros(id);
            }
            DataTable dt = SiteFunctions.trnsformEntityToDT(plantillaspol);
            rptCartas.DataSource = dt;
            rptCartas.DataBind();
        }

        public string generaCartaHTML()
        {
            int idpoliza = 0;
            int plantilla = 0;
            int.TryParse(PoliId.Value, out idpoliza);
            int.TryParse(idplantilla.Value, out plantilla);
            CartasFunctions gencarta = new CartasFunctions();
            string carta = string.Empty;
            carta = gencarta.GeneraCartaEndoso(EndosoId.Value, plantilla, this.Page.User.Identity.Name);
            
            return carta;
        }

        protected void btnrecalcIncisosAutos_Click(object sender, EventArgs e)
        {
            recalcincisos();
        }

        private void recalcincisos()
        {
            PaneName.Value = "AutoIndiv";
            object id = PoliId.Value;
            DataTable dtInciso = searchRelRecords(recordType.IncisoAutos, SearchIdInciso.Value, id.ToString());
            getPrimaIncisos(dtInciso);
            gvIncisosAutos.DataSource = dtInciso;
            gvIncisosAutos.DataBind();
            SaveEndoso();
        }

        private void getPrimaIncisos(DataTable dti)
        {
            decimal prima = 0M;
            decimal primaaux = 0M;
            foreach (DataRow dr in dti.Rows)
            {
                decimal.TryParse(dr["Prima"].ToString(), out primaaux);
                prima += primaaux;

            }
            txtPrima.Text = prima.ToString();
            if (txtRecargoPor.Text != "")
            {
                decimal recargopor = Convert.ToDecimal(txtRecargoPor.Text);
                decimal recargo = prima * (recargopor / 100);
                txtRecargosImporte.Text = recargo.ToString();
            }
            if (txtComisionPor.Text != "")
            {
                decimal comisionpor = Convert.ToDecimal(txtComisionPor.Text);
                decimal comision = prima * (comisionpor / 100);
                txtComisionImporte.Text = comision.ToString();
            }
        }

        protected void btnSearchIncisosAutos_Click(object sender, EventArgs e)
        {
            searchIncisosAutos();
        }

        private void searchIncisosAutos()
        {
            PaneName.Value = "AutoIndiv";
            object id = PoliId.Value;
            SearchIdInciso.Value = string.Empty;
            if (txtSearchIncisosAutos.Text != string.Empty)
            {
                SearchIdInciso.Value = '%' + txtSearchIncisosAutos.Text + '%';
            }
            DataTable dtInciso = searchRelRecords(recordType.IncisoAutos, SearchIdInciso.Value, id.ToString());
            gvIncisosAutos.DataSource = dtInciso;
            gvIncisosAutos.DataBind();
        }

        protected void btnSeleccionarTodos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "AutoIndiv";
            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (!chkBox.Checked)
                    {
                        chkBox.Checked = true;
                    }
                }
            }
        }

        protected void btnDeseleccionarTodos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "AutoIndiv";
            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        chkBox.Checked = false;
                    }
                }
            }
        }

        /*protected void BtnImportarIncisos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "IncisosAutos";
            string errmess = string.Empty;
            object id = EndosoId.Value;
            if (fileUpload.HasFile)
            {
                string fileName = fileUpload.FileName;
                string[] splitStr = fileName.Split('.');
                if (splitStr.Length == 2)
                {
                    if (splitStr[1] == "xls" || splitStr[1] == "xlsx")
                    {
                        bool isXlsx = splitStr[1] == "xlsx";
                        decimal totalPrima = 0;
                        int NoIncisos = 0;
                        if (SiteFunctions.importIncisos(fileUpload.FileContent, id.ToString(), isXlsx, SiteFunctions.getuserid(Context.User.Identity.Name, false), out totalPrima, out errmess, out NoIncisos))
                        {

                            recalcincisos();
                            //txtPrima.Text = totalPrima.ToString();
                            decimal.TryParse(txtPrima.Text, out totalPrima);
                            decimal gastos = 0, recargos = 0, descuentos = 0, impuestos = 0, porComision = 0, porBono = 0, porSobreCom = 0;

                            decimal.TryParse(txtGastos.Text, out gastos);

                            //txtGastos.Text = Math.Round((gastos * NoIncisos),4).ToString();
                            decimal.TryParse(txtGastos.Text, out gastos);
                            decimal.TryParse(txtRecargosImporte.Text, out recargos);
                            
                            if (txtFormaPago.SelectedValue == "8" || txtFormaPago.SelectedValue == "9" || txtFormaPago.SelectedValue == "10" || txtFormaPago.SelectedValue == "11" || txtFormaPago.SelectedValue == "14")
                            {
                                recargos = 0;
                                gastos = 0;
                            }
                            txtNeto.Text = Math.Round(totalPrima + gastos + recargos, 4).ToString();
                            txtImpuesto.Text = getAmountImpuesto(txtImpuestoPor.SelectedValue, txtNeto.Text);

                            decimal.TryParse(txtImpuesto.Text, out impuestos);

                            txtTotal.Text = Math.Round((totalPrima + gastos + recargos) - descuentos + impuestos, 4).ToString();

                            decimal.TryParse(txtComisionPor.Text, out porComision);
                            

                            txtComisionImporte.Text = Math.Round(((porComision / 100) * totalPrima), 4).ToString();
                            
                            SaveEndoso();
                        }
                        else
                        {
                            IncisoImportMsg.Value = errmess;
                        }
                    }
                    else
                    {
                        showMessage("La extensión del archivo debe de ser .csv", true);
                    }
                }
            }
            else
            {
                showMessage("No ha seleccionado archivo para importar!!!", true);
            }
            if (IncisoImportMsg.Value != "")
            {
                showMessage(IncisoImportMsg.Value, true);
                IncisoImportMsg.Value = "";
            }

        }*/

        
        protected void btnDeleteIncisos_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";

            foreach (GridViewRow row in gvIncisosAutos.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        seleccionado = true;
                        break;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirm_value"];
                if (confirmValue == "Si")
                {
                    foreach (GridViewRow row in gvIncisosAutos.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                string id = gvIncisosAutos.Rows[row.RowIndex].Cells[2].Text;
                                string leadId = string.Empty;
                                Entity Auto = new Entity("Auto");
                                Auto.Keys.Add(new Key("Id", id, "int64"));
                                SiteFunctions.DeleteEntity(Auto, Method.POST, out errmess);
                                Entity Inciso = new Entity("BienAsegurado");
                                Inciso.Keys.Add(new Key("Id", id, "int64"));
                                SiteFunctions.DeleteEntity(Inciso, Method.POST, out errmess);

                            }
                        }
                    }

                    recalcincisos();
                    
                }

            }
            else
            {
                this.showMessage("Debe Seleccionar un Inciso.", true);
            }
        
        }

        protected void btnrecalcIncisosAutosD_Click(object sender, EventArgs e)
        {
            searchIncisosEndosoD();
        }

        private void searchIncisosEndosoD()
        {
            string errMsg = string.Empty;
            PaneName.Value = "IncisoEndosoD";
            object id = PoliId.Value;
            SearchIdInciso.Value = string.Empty;
            if (txtSearchIncisosAutos.Text != string.Empty)
            {
                SearchIdInciso.Value = '%' + txtSearchIncisosAutos.Text + '%';
            }
            Entity entity = new Entity("BienAsegurado");
            entity.EntityName = "BienAsegurado";
            entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
            entity.Attributes.Add(new Models.Attribute("Prima", "", "float"));
            entity.Attributes.Add(new Models.Attribute("Poliza"));
            entity.Keys.Add(new Key("Poliza", id.ToString(), "int",1,2));
            entity.Keys.Add(new Key("EndosoBaja", EndosoId.Value, "int",1,2));
            entity.Keys.Add(new Key("Estatus", "5", "int", 1, 1));
            entity.Keys.Add(new Key("Estatus", "4", "int",2,1));
            if (txtSearchIncisosAutos.Text != string.Empty)
            {
                entity.Keys.Add(new Key("Id", SearchIdInciso.Value, "string", 1));
            }
            Entity auto = new Entity("Auto");
            auto.Attributes.Add(new Models.Attribute("Serie", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Motor", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Placas", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Modelo", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Marca", "", "string"));
            auto.Attributes.Add(new Models.Attribute("ConductorHabitual", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Version", "", "string"));

            selectJoinEntity selJoinAuto = new selectJoinEntity("id", 1, "id");
            JoinEntity joinEntityAuto = new JoinEntity();
            joinEntityAuto.ChildEntity = auto;
            joinEntityAuto.selectJoinList = new List<selectJoinEntity>();
            joinEntityAuto.selectJoinList.Add(selJoinAuto);
            joinEntityAuto.JoinType = JoinType.Inner;
            entity.ChildEntities.Add(joinEntityAuto);
            List<Entity> polResult = SiteFunctions.GetValues(entity, out errMsg);
            DataTable dtRes = SiteFunctions.trnsformEntityToDT(polResult);
            getPrimaIncisos(dtRes);
            gvIncisoEndosoD.DataSource = dtRes;
            gvIncisoEndosoD.DataBind();
        }

        protected void btnIncisosEndosoD_Click(object sender, EventArgs e)
        {
            searchIncisosEndosoD();
        }

        protected void btnDeleteIncisoEndosoD_Click(object sender, ImageClickEventArgs e)
        {
            string errMsg = string.Empty;
            string Id = PoliId.Value;
            foreach (GridViewRow row in gvIncisoEndosoD.Rows)
            {
                CheckBox chkBox = row.FindControl("chkSel") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string IncisoId = gvIncisoEndosoD.Rows[row.RowIndex].Cells[1].Text;
                        Entity Inciso = new Entity("BienAsegurado");
                        Inciso.Attributes.Add(new Models.Attribute("EndosoBaja", " null" , "int"));
                        Inciso.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
                        Inciso.Keys.Add(new Key("Id", IncisoId, "int"));
                        SiteFunctions.SaveEntity(Inciso, RestSharp.Method.PUT, out errMsg);

                    }
                }
            }
            showMessage("Incisos agregados correctamente", false);
        }

        protected void btnDescarteEndosoD_Click(object sender, EventArgs e)
        {
            //string confirmValue = Request.Form["confirmDescarte_value"];
            //string errMsg = string.Empty;
            //if (confirmValue == "Si")
            //{
            //    foreach (GridViewRow row in gvIncisoEndosoD.Rows)
            //    {

            //        string IncisoId = gvIncisoEndosoD.Rows[row.RowIndex].Cells[1].Text;
            //        Entity Inciso = new Entity("BienAsegurado");

            //        Inciso.Attributes.Add(new Models.Attribute("Estatus", "5", "int"));
            //        Inciso.Keys.Add(new Key("Id", IncisoId, "int"));
            //        SiteFunctions.SaveEntity(Inciso, RestSharp.Method.PUT, out errMsg);



            //    }

            //    Entity Endoso = new Entity("Endoso");
            //    Endoso.Attributes.Add(new CognisoWebApp.Models.Attribute("EstatusEndoso","1","int"));
            //    Endoso.Keys.Add(new Key("Id",EndosoId.Value,"int"));
            //    SiteFunctions.SaveEntity(Endoso, RestSharp.Method.PUT, out errMsg);
            //}

            //showMessage("Endoso descartado correctamente",false);
           
        }
    }
}