﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Attachment.aspx.cs" Inherits="CognisoWebApp.Attachment" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <webopt:BundleReference runat="server" Path="~/Content/css" />
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script>
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <link href="/Scripts/styleCogniso.css" rel="stylesheet" />
    <script type="text/javascript">
        function UploadFile(fileUpload) {
            try {
                if (fileUpload.value != '') {

                    document.getElementById("btnUpload").click();
                }

            } catch (e) {
                console.error(e.message);
            }


        }
        function chooseUpload() {

            document.getElementById("FileUploadDoc").click();
        }
        function gfProcesoEsconde() {

            if (document.getElementById("divProceso").style.visibility == "visible") {

                document.getElementById("divProceso").style.visibility = "hidden";
            }

            __doPostBack('lnkDescarga', '');
        }
        function gfProceso() {
            if (document.getElementById("divProceso").style.visibility == "visible")
                document.getElementById("divProceso").style.visibility = "hidden";
            else
                document.getElementById("divProceso").style.visibility = "visible";
        }
    </script>

</head>

<body>

    <div id="divProceso" class="cCargando" style="visibility: hidden;">
        <div id="divProcesoMsg" class="cCargandoImg">
            <br />
            <img src="Content/Imgs/ajax-loader.gif" />
            <br />
        </div>
    </div>
    <form id="form1" runat="server">


        <div class="container" runat="server">

            <div class="panel-heading">
                <div>
                    <asp:Label ID="lblInfo" runat="server" Text="" CssClass="w3-codespan"></asp:Label>
                </div>
                <div>
                    <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Documentos</asp:Label>
                </div>
                <div class="btn-group">

                    <asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" OnClientClick="chooseUpload(); return false" runat="server" Height="30px" Width="31px" />

                    <br />

                </div>
            </div>

        <div class="panel-body" runat="server">

            <div class="table-responsive" runat="server">
                <asp:GridView ID="GVDocumentsList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" OnSelectedIndexChanged="GridList_SelectedIndexChanged" AllowPaging="True" AutoGenerateColumns="False" CssClass="table table-hover table-striped">
                    <Columns>

                        <asp:CommandField SelectText="Abrir" ShowSelectButton="True" />

                        <asp:BoundField DataField="FileName" HeaderText="Nombre documento" />
                        <asp:BoundField DataField="spUrlFile"  />

                    </Columns>
                </asp:GridView>
                <asp:HiddenField ID="HdnName" runat="server" />
                <asp:HiddenField ID="HdnFileName" runat="server" />
                <asp:FileUpload ID="FileUploadDoc" runat="server" AllowMultiple="True" Style="display: none" onchange="UploadFile(this);" />
                <asp:Button ID="btnUpload" Text="Upload" runat="server" Style="display: none" OnClick="UploadDocument" />
            </div>
            <asp:HiddenField ID="Id" runat="server" />
            <asp:HiddenField ID="ParentId" runat="server" />
            <asp:HiddenField ID="EntityName" runat="server" />
        </div>
        <div>

            <asp:LinkButton ID="lnkDescarga" runat="server" OnClick="lnkDescarga_Click"></asp:LinkButton>
        </div>


        </div>

    </form>
</body>
</html>
