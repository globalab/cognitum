﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="ComisionesAsociados.aspx.cs" Inherits="CognisoWebApp.ComisionesAsociados" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Ramo</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
     <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtDirectorAsociado.ClientID%>").select2({
                placeholder: "Seleccione un Director Asociado",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });
      </script>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
     
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Ramo</asp:Label>
            </div>
           
            <div class="panel-heading">
                <div class="btn-group">
                    
                </div>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="ComAsoc-tab" data-toggle="tab" href="#ComAsoc" role="tab" aria-controls="ComAsoc" aria-selected="false">Comisión Asociados</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="ComAsoc" role="tabpanel" aria-labelledby="ComAsoc-tab">
                            <div class="container">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDirectorAsociado" runat="server" Text="Director Asociado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtDirectorAsociado" runat="server" class="form-control" ></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblComision" runat="server" Text="Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtComision" runat="server" class="form-control" Enabled="false"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblPoliza" runat="server" Text="Poliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtPoliza" runat="server" class="form-control" Enabled="false"></asp:Textbox>
                                        </div>
                                    </div>

                                </div>
                           </div>
                           </div> 
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9" Enabled="false">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9" Enabled="false">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9" Enabled="false">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9" Enabled="false">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9" Enabled="false">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                           
                          </div>
                      </div> 
                   
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="Id" runat="server" />

            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>  

