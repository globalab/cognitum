﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="ContactoSimple.aspx.cs" Inherits="CognisoWebApp.ContactoSimple" %>
<!DOCTYPE html>  
<html lang="en">  
<head>  
    <title>Contacto</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <link href="/Content/cogniso.css" rel="stylesheet"/>
    <link href="/Content/jquery-ui.css" rel="stylesheet"/>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">


        function openpopup(url) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;
            if (document.getElementById("<%=ContactId.ClientID%>").value == "") {
                alert("Debe de guardar el titular antes de capturar el teléfono");
                return;
            }
            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: "Teléfono",
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {

                    $("#<%=reloadCont.ClientID%>").click();
                }
            });
                $dialog.dialog('open');
            }

            function collapsediv() {
                var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

                $('#myTab a[href="#' + paneName + '"]').tab('show')

            }
    </script>

    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
</head>  
<body id="bodyT">
     <form id="form1" runat="server">
          
        <div class="container" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Contacto</asp:Label></h2>
            </div>
            <hr class="float-left">

            <div class="panel-heading clearfix">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();"/>
                     <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                </div>
            </div>

            <div class="containerRT1" runat="server" id="NegociacionGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="ContactoAlterno-tab" data-toggle="tab" href="#ContactoAlterno" role="tab" aria-controls="ContactoAlterno" aria-selected="false">Contacto</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Telefonos-tab" data-toggle="tab" href="#Telefonos" role="tab" aria-controls="Telefonos" aria-selected="false">Teléfonos</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                       <div class="tab-pane fade show active" id="ContactoAlterno" role="tabpanel" aria-labelledby="ContactoAlterno-tab">
                          <div class="containerRT2">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblNombreCA" runat="server" Text="Nombre Completo"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtNombreCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblEmailCA" runat="server" Text="Email"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtEmailCA" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtCliente" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                          </div>
                      </div>
                      <div class="tab-pane fade" id="Telefonos" role="tabpanel" aria-labelledby="Telefonos-tab">
                            <div class="containerRT3">
                                <div class="row">
                                    
                                    <a data-image='TelefonoContactoSimple.aspx?popup=1&contactid=<%=ContactId.Value %>'  onclick="openpopup('TelefonoContactoSimple.aspx?popup=1&contactid=<%=ContactId.Value %>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVTelList" runat="server" OnPageIndexChanging="GVTelList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <a href='#' data-image='TelefonoContactoSimple.aspx?popup=1&contactid=<%# Eval("Contacto") %>&id=<%# Eval("Id") %>' onclick="openpopup('TelefonoContactoSimple.aspx?popup=1&contactid=<%# Eval("Contacto") %>&id=<%# Eval("Id") %>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                    <asp:BoundField DataField="LadaNacional" HeaderText="Lada (nac)" />
                                    <asp:BoundField DataField="LadaInternacional" HeaderText="Lada (int)" />
                                    <asp:BoundField DataField="Numero" HeaderText="Número" />
                                    <asp:BoundField DataField="Extension" HeaderText="Extension" />
                            
                                </Columns>
                            </asp:GridView>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT4">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblPropietario" runat="server" Text="Propietario"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtPropietario" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="ContactId" runat="server" />
                <asp:HiddenField ID="CId" runat="server" />
                <asp:Button ID="reloadCont" runat="server" OnClick="reloadCont_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
            </div>
        </div>
</form>
</body>
</html>
