﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class CancelPoliza : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                showPanels();
                initCatalog();
                object id = Request.QueryString["id"];
                if (id != null)
                {
                    PoliId.Value = id.ToString();
                    this.searchPoliza(id.ToString());

                }
                string op = string.Empty;
                try
                {
                    op = Request.QueryString["op"];
                }
                catch (Exception)
                {
                }
                List<Entity> Pendings = new List<Entity>();
                bool alreadyCanceled = checkifPending(SiteFunctions.getuserid(Context.User.Identity.Name, true), id.ToString(), true, out Pendings);
                if (op == "1")
                {
                    BtnCancel.Visible = false;
                   
                    
                    btnAprove.Visible = !alreadyCanceled;
                    btnReject.Visible = !alreadyCanceled;
                }
                else
                {
                    btnAprove.Visible = false;
                    btnReject.Visible = false;
                    BtnCancel.Visible = alreadyCanceled;
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);

            }
        }
        
        private bool checkifPending(string user, string id,bool checkaprobador,out List<Entity> PendingAprovals)
        {
            string errmess = "";
            
            Entity checkcanceled = new Entity("AutorizacionCancelPol");
            checkcanceled.Attributes.Add(new Models.Attribute("Id"));
            checkcanceled.Attributes.Add(new Models.Attribute("UsuarioAutorizo"));
            checkcanceled.Keys.Add(new Key("Poliza", id, "int64"));
            if (checkaprobador)
            {
                checkcanceled.Keys.Add(new Key("UsuarioAutorizo", user, "int64"));
            }
            checkcanceled.Keys.Add(new Key("EstatusAutorizacion", "0", "int"));
            List<Entity> estatus = SiteFunctions.GetValues(checkcanceled, out errmess);
            PendingAprovals = estatus;
            return estatus.Count == 0;
           

        }

        private void initCatalog()
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Aseguradora.useAuditable = false;
            JoinEntity joinCust = new JoinEntity();
            joinCust.ChildEntity = new Entity("Cliente");
            joinCust.JoinKey = new Models.Attribute("Id");
            joinCust.ChildEntity.Attributes.Add(new Models.Attribute("NombreCompleto"));
            selectJoinEntity custsj = new selectJoinEntity("Id", 1, "Id");
            joinCust.selectJoinList.Add(custsj);
            joinCust.JoinType = JoinType.Inner;
            Aseguradora.ChildEntities.Add(joinCust);
            listResult = SiteFunctions.GetValues(Aseguradora, out errmess);
            txtAseguradora.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtAseguradora.DataValueField = "Id";
            txtAseguradora.DataTextField = "ClienteNombreCompleto";
            txtAseguradora.DataBind();

            Entity Ramo = new Entity("Ramo");
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            Ramo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Ramo, out errmess);
            txtRamo.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtRamo.DataValueField = "Id";
            txtRamo.DataTextField = "Nombre";
            txtRamo.DataBind();

            Entity SubRamo = new Entity("SubRamo");
            SubRamo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            SubRamo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            SubRamo.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            SubRamo.useAuditable = false;
            listResult = SiteFunctions.GetValues(SubRamo, out errmess);
            txtSubramo.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtSubramo.DataValueField = "Id";
            txtSubramo.DataTextField = "Nombre";
            txtSubramo.DataBind();

            Entity agente = new Entity("Agente");
            agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            agente.Attributes.Add(new CognisoWebApp.Models.Attribute("Clave"));
            agente.useAuditable = false;
            listResult = SiteFunctions.GetValues(agente, out errmess);
            txtAgente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtAgente.DataValueField = "Id";
            txtAgente.DataTextField = "Clave";
            txtAgente.DataBind();


            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.useAuditable = false;
            listResult = SiteFunctions.GetValues(Cliente, out errmess);
            txtCliente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtCliente.DataValueField = "Id";
            txtCliente.DataTextField = "NombreCompleto";
            txtCliente.DataBind();

            Entity titular = new Entity("ContactoSimple");
            titular.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            titular.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            titular.useAuditable = false;
            listResult = SiteFunctions.GetValues(titular, out errmess);
            txtTitular.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtTitular.DataValueField = "Id";
            txtTitular.DataTextField = "NombreCompleto";
            txtTitular.DataBind();

            //Entity direccion = new Entity("Direccion");
            //direccion.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            //direccion.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            //direccion.useAuditable = false;
            //listResult = SiteFunctions.GetValues(direccion, out errmess);
            //txtDirCobro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            //txtDirCobro.DataValueField = "Id";
            //txtDirCobro.DataTextField = "NombreCompleto";
            //txtDirCobro.DataBind();
            //txtDirFiscal.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            //txtDirFiscal.DataValueField = "Id";
            //txtDirFiscal.DataTextField = "NombreCompleto";
            //txtDirFiscal.DataBind();
            //txtDirEnvio.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            //txtDirEnvio.DataValueField = "Id";
            //txtDirEnvio.DataTextField = "NombreCompleto";
            //txtDirEnvio.DataBind();

            Entity moneda = new Entity("Moneda");
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            moneda.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            moneda.useAuditable = false;
            listResult = SiteFunctions.GetValues(moneda, out errmess);
            txtMoneda.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtMoneda.DataValueField = "Id";
            txtMoneda.DataTextField = "Nombre";
            txtMoneda.DataBind();


            Array itemValues = System.Enum.GetValues(typeof(EstatusPolizaEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusPolizaEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }

            itemValues = System.Enum.GetValues(typeof(StatusMesaControl));
            itemNames = System.Enum.GetNames(typeof(StatusMesaControl));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtMesa.Items.Add(item);
            }

            itemValues = System.Enum.GetValues(typeof(EstatusCobranzaEnum));
            itemNames = System.Enum.GetNames(typeof(EstatusCobranzaEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatusCobranza.Items.Add(item);
            }

            itemValues = System.Enum.GetValues(typeof(FormaPagoEnum));
            itemNames = System.Enum.GetNames(typeof(FormaPagoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtFormaPago.Items.Add(item);
            }


        }

        

        private void searchPoliza(string _id)
        {
            string errMsg = string.Empty,
            idCliente = string.Empty, idAseguradora = string.Empty, idContacto = string.Empty, idMoneda = string.Empty,idImpuesto = string.Empty,
            idRamo = string.Empty, idSubRamo = string.Empty, idAgente = string.Empty,idDirFiscal = string.Empty,idDirCobro = string.Empty,idDirEnvio = string.Empty;
            List<Entity> entities = new List<Entity>();
            Entity poliza = new Entity("Poliza");
            poliza.Keys.Add(new Models.Key("id", _id, "int"));
            poliza.Attributes.Add(new Models.Attribute("Id"));
            poliza.Attributes.Add(new Models.Attribute("Renovable"));
            poliza.Attributes.Add(new Models.Attribute("PolizaOriginal"));
            poliza.Attributes.Add(new Models.Attribute("PolizaNueva"));
            poliza.Attributes.Add(new Models.Attribute("Descuento"));
            poliza.Attributes.Add(new Models.Attribute("SobreComisionPct"));
            poliza.Attributes.Add(new Models.Attribute("SobreComisionImporte"));
            poliza.Attributes.Add(new Models.Attribute("BonoPct"));
            poliza.Attributes.Add(new Models.Attribute("BonoImporte"));
            poliza.Attributes.Add(new Models.Attribute("Impuesto"));
            poliza.Attributes.Add(new Models.Attribute("Contratante"));
            poliza.Attributes.Add(new Models.Attribute("Aseguradora"));
            poliza.Attributes.Add(new Models.Attribute("Ramo"));
            poliza.Attributes.Add(new Models.Attribute("SubRamo"));
            poliza.Attributes.Add(new Models.Attribute("Titular"));
            poliza.Attributes.Add(new Models.Attribute("Moneda"));
            poliza.Attributes.Add(new Models.Attribute("Agente"));
            poliza.Attributes.Add(new Models.Attribute("EstatusPoliza"));
            poliza.Attributes.Add(new Models.Attribute("DireccionFiscal"));
            poliza.Attributes.Add(new Models.Attribute("DireccionEnvio"));
            poliza.Attributes.Add(new Models.Attribute("DireccionCobro"));
            poliza.Attributes.Add(new Models.Attribute("EstatusCobranza"));

            List<Entity> polResult = SiteFunctions.GetValues(poliza, out errMsg);
            if (polResult.Count > 0)
            {
                Entity pliza = polResult[0];

                idCliente = pliza.getAttrValueByName("Contratante");
                idAseguradora = pliza.getAttrValueByName("Aseguradora");
                idRamo = pliza.getAttrValueByName("Ramo");
                idSubRamo = pliza.getAttrValueByName("SubRamo");
                idContacto = pliza.getAttrValueByName("Titular");
                idMoneda = pliza.getAttrValueByName("Moneda");
                idAgente = pliza.getAttrValueByName("Agente");
                idImpuesto = pliza.getAttrValueByName("Impuesto");
                idDirFiscal = pliza.getAttrValueByName("DireccionFiscal");
                idDirEnvio = pliza.getAttrValueByName("DireccionEnvio");
                idDirCobro = pliza.getAttrValueByName("DireccionCobro");



                txtOt.Text = pliza.getAttrValueByName("Id");
                
                chckRenovable.Checked = pliza.getAttrValueByName("Renovable") == "1" ? true : false;
                txtPolOriginal.Text = pliza.getAttrValueByName("PolizaOriginal");
                txtPolNueva.Text = pliza.getAttrValueByName("PolizaNueva");
                txtDescuento.Text = pliza.getAttrValueByName("Descuento");
                txtSobreComisionPor.Text = pliza.getAttrValueByName("SobreComisionPct");
                txtSobreComision.Text = pliza.getAttrValueByName("SobreComisionImporte");
                txtBonoPor.Text = pliza.getAttrValueByName("BonoPct");
                txtBono.Text = pliza.getAttrValueByName("BonoImporte");
                txtEstatus.SelectedIndex = txtEstatus.Items.IndexOf(txtEstatus.Items.FindByValue(pliza.getAttrValueByName("EstatusPoliza").ToString()));
                txtEstatusCobranza.SelectedIndex = txtEstatusCobranza.Items.IndexOf(txtEstatusCobranza.Items.FindByValue(pliza.getAttrValueByName("EstatusCobranza").ToString()));
            }
            
            Entity cliente = new Entity("Cliente");
            cliente.Keys.Add(new Models.Key("id", idCliente, "int"));

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Keys.Add(new Models.Key("id", idAseguradora, "int"));

            Entity ramo = new Entity("Ramo");
            ramo.Keys.Add(new Models.Key("id", idRamo, "int"));
            ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            ramo.Attributes.Add(new CognisoWebApp.Models.Attribute("DivisionOperativa"));

            Entity subramo = new Entity("SubRamo");
            subramo.Keys.Add(new Models.Key("id", idSubRamo, "int"));

            Entity titular = new Entity("ContactoSimple");
            titular.Keys.Add(new Models.Key("id", idContacto, "int"));

            Entity moneda = new Entity("Moneda");
            moneda.Keys.Add(new Models.Key("id", idMoneda, "int"));

            Entity agente = new Entity("Agente");
            agente.Keys.Add(new Models.Key("id", idAgente, "int"));

            Entity slip = new Entity("Slip");
            slip.Keys.Add(new Models.Key("id", _id, "int"));

            Entity cobertura = new Entity("Coberturas");
            cobertura.Keys.Add(new Models.Key("id", _id, "int"));

            Entity condEspec = new Entity("CondicionesEspeciales");
            condEspec.Keys.Add(new Models.Key("id", _id, "int"));

            Entity Impuesto = new Entity("Impuesto");
            Impuesto.Keys.Add(new Models.Key("id", idImpuesto, "int"));
            Impuesto.Attributes.Add(new Models.Attribute("Clave"));
            Impuesto.Attributes.Add(new Models.Attribute("Nombre"));

            Entity DirFiscal = new Entity("Direccion");
            DirFiscal.Keys.Add(new Models.Key("id", idDirFiscal, "int"));
            DirFiscal.Attributes.Add(new Models.Attribute("Id"));
            DirFiscal.Attributes.Add(new Models.Attribute("Calle"));
            DirFiscal.Attributes.Add(new Models.Attribute("Colonia"));
            DirFiscal.Attributes.Add(new Models.Attribute("CP"));
            DirFiscal.Attributes.Add(new Models.Attribute("Delegacion"));

            Entity DirEnvio = new Entity("Direccion");
            DirEnvio.Keys.Add(new Models.Key("id", idDirEnvio, "int"));
            DirEnvio.Attributes.Add(new Models.Attribute("Id"));
            DirEnvio.Attributes.Add(new Models.Attribute("Calle"));
            DirEnvio.Attributes.Add(new Models.Attribute("Colonia"));
            DirEnvio.Attributes.Add(new Models.Attribute("CP"));
            DirEnvio.Attributes.Add(new Models.Attribute("Delegacion"));

            Entity DirCobro = new Entity("Direccion");
            DirCobro.Keys.Add(new Models.Key("id", idDirCobro, "int"));
            DirCobro.Attributes.Add(new Models.Attribute("Id"));
            DirCobro.Attributes.Add(new Models.Attribute("Calle"));
            DirCobro.Attributes.Add(new Models.Attribute("Colonia"));
            DirCobro.Attributes.Add(new Models.Attribute("CP"));
            DirCobro.Attributes.Add(new Models.Attribute("Delegacion"));


            Entity Tramite = new Entity("Tramite");
            Tramite.Keys.Add(new Models.Key("id", _id, "int"));
            Tramite.Attributes.Add(new Models.Attribute("FormaPago"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaInicio"));
            Tramite.Attributes.Add(new Models.Attribute("VigenciaFin"));
            Tramite.Attributes.Add(new Models.Attribute("Prima"));
            Tramite.Attributes.Add(new Models.Attribute("Gastos"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosPct"));
            Tramite.Attributes.Add(new Models.Attribute("RecargosImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Neto"));
            Tramite.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Total"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionPct"));
            Tramite.Attributes.Add(new Models.Attribute("ComisionImporte"));
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            Tramite.Attributes.Add(new Models.Attribute("EstatusMesaDeControl"));

            entities.Add(poliza);
            entities.Add(cliente);
            entities.Add(aseguradora);
            entities.Add(ramo);
            entities.Add(subramo);
            entities.Add(titular);
            entities.Add(moneda);
            entities.Add(agente);
            entities.Add(slip);
            entities.Add(cobertura);
            entities.Add(condEspec);
            entities.Add(Tramite);
            entities.Add(Impuesto);
            entities.Add(DirFiscal);
            if (idDirFiscal != idDirEnvio)
            {
                entities.Add(DirEnvio);
            }
            if (idDirFiscal != idDirCobro && idDirEnvio != idDirCobro)
            {
                entities.Add(DirCobro);
            }

            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            mapPolizaSearch(result,idDirFiscal,idDirEnvio,idDirCobro);
        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //General.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";

            //Contacto.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //ContactoAlterno.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";


        }
        private void mapPolizaSearch(List<OperationResult> result,string iddirFiscal,string iddirEnvio,string iddirCobro)
        {
            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if(opResult.RetVal.Count > 0)
                    { 
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {
                            case "cliente":
                                txtCliente.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "aseguradora":
                                txtAseguradora.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "ramo":
                                txtRamo.SelectedValue = entity.getAttrValueByName("Id");
                                DivisionOperativa.Value = entity.getAttrValueByName("DivisionOperativa").ToString();

                                break;
                            case "subramo":
                                txtSubramo.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "contactosimple":
                                txtTitular.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "moneda":
                                txtMoneda.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "agente":
                                txtAgente.SelectedValue = entity.getAttrValueByName("Id");
                                break;
                            case "slip":
                                txtSlip.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "coberturas":
                                txtCoberturas.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "condicionesespeciales":
                                txtCondSpec.Text = entity.getAttrValueByName("Descripcion");
                                break;
                            case "tramite":
                                txtFormaPago.SelectedValue = entity.getAttrValueByName("FormaPago");
                                DateTime fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaInicio"));
                                txtInicio.Text = fecha.ToString("dd/MM/yyyy");
                                fecha = Convert.ToDateTime(entity.getAttrValueByName("VigenciaFin"));
                                txtFin.Text = fecha.ToString("dd/MM/yyyy");
                                txtPrima.Text = entity.getAttrValueByName("Prima");
                                txtGastos.Text = entity.getAttrValueByName("Gastos");
                                txtRecargosPor.Text = entity.getAttrValueByName("RecargosPct");
                                txtRecargos.Text = entity.getAttrValueByName("RecargosImporte");
                                txtNeto.Text = entity.getAttrValueByName("Neto");
                                txtImpuesto.Text = entity.getAttrValueByName("ImpuestoImporte");
                                txtTotal.Text = entity.getAttrValueByName("Total");
                                txtComisionPor.Text = entity.getAttrValueByName("ComisionPct");
                                txtComision.Text = entity.getAttrValueByName("ComisionImporte");
                                txtNopoliza.Text = entity.getAttrValueByName("Folio");
                                txtMesa.SelectedIndex = txtMesa.Items.IndexOf(txtMesa.Items.FindByValue(entity.getAttrValueByName("EstatusMesaDeControl").ToString()));
                                break;
                            case "impuesto":
                                txtImpuestoPor.Text = entity.getAttrValueByName("Clave") + " " + entity.getAttrValueByName("Nombre");
                                break;
                            case "direccion":
                                string iddireccion = entity.getAttrValueByName("Id");
                                string calle = entity.getAttrValueByName("Calle").Trim();
                                string colonia = entity.getAttrValueByName("Colonia").Trim();
                                string cp = entity.getAttrValueByName("CP");
                                string delegacion = entity.getAttrValueByName("Delegacion");

                                if (iddireccion == iddirFiscal)
                                {
                                    txtDirFiscal.Text = (calle + " " + (colonia != "" ? "Colonia " + colonia + " " : "") + (cp != "" ? "C.P. " + cp + " " : "") + (delegacion != "" ? "Delegación " + delegacion : "")).ToUpper();
                                }
                                if (iddireccion == iddirEnvio)
                                {
                                    txtDirEnvio.Text = (calle + " " + (colonia != "" ? "Colonia " + colonia + " " : "") + (cp != "" ? "C.P. " + cp + " " : "") + (delegacion != "" ? "Delegación " + delegacion : "")).ToUpper();
                                }
                                if (iddireccion == iddirCobro)
                                {
                                    txtDirCobro.Text = (calle + " " + (colonia != "" ? "Colonia " + colonia + " " : "") + (cp != "" ? "C.P. " + cp + " " : "") + (delegacion != "" ? "Delegación " + delegacion : "")).ToUpper();
                                }
                                break;
                        }

                    }
                }
            }
            showPanels();
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
     
        protected void btnBuscarRecibos_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            object id = Request.QueryString["id"];
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchRecibos(id.ToString());

        }

        private void searchRecibos(string _id)
        {
            string errMsg = string.Empty;
            
            List<Entity> entities = new List<Entity>();
            Entity recibo = new Entity("Recibo");
            recibo.Keys.Add(new Models.Key("Tramite", _id, "int"));
            recibo.Keys.Add(new Models.Key("id", SearchId.Value, "string"));
            recibo.Attributes.Add(new Models.Attribute("Id"));
            recibo.Attributes.Add(new Models.Attribute("Estatus"));
            recibo.Attributes.Add(new Models.Attribute("Numero"));
            recibo.Attributes.Add(new Models.Attribute("Total"));
            recibo.Attributes.Add(new Models.Attribute("Concepto"));
            
            
            List<Entity> polResult = SiteFunctions.GetValues(recibo, out errMsg);

            DataTable dtRecibos = SiteFunctions.trnsformEntityToDT(polResult);

            GVRecibosList.DataSource = dtRecibos;
            GVRecibosList.DataBind();

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            PaneName.Value = "Recibos";
            object id = Request.QueryString["id"];
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchRecibos(id.ToString());
        }

        protected void GVRecibosList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Recibos";
            GVRecibosList.PageIndex = e.NewPageIndex;
            GVRecibosList.DataBind();
            object id = Request.QueryString["id"];
            searchRecibos(id.ToString());
        }

        private void insertAprovals(string id)
        {
            string errmess = string.Empty;
            Entity NoApprovals = new Entity("AutorizacionCancelPol");
            NoApprovals.Attributes.Add(new Models.Attribute("distinct Id"));
            NoApprovals.Keys.Add(new Models.Key("Poliza",id,"int64"));
            List<Entity> Aprov = SiteFunctions.GetValues(NoApprovals,out errmess);
            string AutId = Aprov.Count.ToString();
            //Primero el de cobranza
            Entity Approvals = new Entity("AutorizacionCancelPol");
            Approvals.Attributes.Add(new Models.Attribute("Id", AutId, "datetime"));
            Approvals.Attributes.Add(new Models.Attribute("FechaNotificacion",DateTime.Now.ToString("yyyy-MM-dd"),"datetime"));
            Approvals.Attributes.Add(new Models.Attribute("FechaRespuesta","1900-01-01","datetime"));
            Approvals.Attributes.Add(new Models.Attribute("EstatusAutorizacion","0","int"));
            Approvals.Attributes.Add(new Models.Attribute("UsuarioAutorizo",ConfigurationManager.AppSettings["UsuarioCobranza"].ToString(),"int64"));
            Approvals.Attributes.Add(new Models.Attribute("Poliza",id,"int64"));
            SiteFunctions.SaveEntity(Approvals, Method.POST, out errmess);
            Entity Aprob = new Entity("AprobDivisionOp");
            Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("DivisionOperativa"));
            Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("Aprobador"));
            Aprob.Keys.Add(new Models.Key("Operacion", "1", "int"));
            List<Entity> Entities = SiteFunctions.GetValues(Aprob, out errmess);
            DataTable dtAprob = SiteFunctions.trnsformEntityToDT(Entities);

            string emailsaprob = string.Empty;
            foreach (DataRow dr in dtAprob.Rows)
            {
                Approvals = new Entity("AutorizacionCancelPol");
                Approvals.Attributes.Add(new Models.Attribute("Id", AutId, "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("FechaNotificacion", DateTime.Now.ToString("yyyy-MM-dd"), "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("FechaRespuesta", "1900-01-01", "datetime"));
                Approvals.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "0", "int"));
                Approvals.Attributes.Add(new Models.Attribute("UsuarioAutorizo",dr["Aprobador"].ToString() , "int64"));
                Approvals.Attributes.Add(new Models.Attribute("Poliza", id, "int64"));
                if (SiteFunctions.SaveEntity(Approvals, Method.POST, out errmess))
                {
                    this.showMessage(errmess, true);
                }
            }
            
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            SiteFunctions funct = new SiteFunctions();
            string body = "";
            insertAprovals(txtNopoliza.Text.Trim());
            try
            {
                body = File.ReadAllText(HttpContext.Current.Server.MapPath("HtmlTemplates/CancelPolizaEmailTemplate.html"));
                body = body.Replace("%OT%", txtOt.Text).Replace("%Folio%",txtNopoliza.Text).Replace("%Ramo%",txtRamo.SelectedItem.Text).Replace("%Estatus%",txtEstatus.SelectedItem.Text).Replace("%URL%",ConfigurationManager.AppSettings["CancelPoliza"].ToString());

            }
            catch(Exception)
            {

            }
            if (body != "" && txtOt.Text != "")
            {
                string errmess = string.Empty;
                Entity Aprob = new Entity("AprobDivisionOp");
                Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("DivisionOperativa"));
                Aprob.Attributes.Add(new CognisoWebApp.Models.Attribute("Aprobador"));
                JoinEntity joinUsers = new JoinEntity();
                joinUsers.ChildEntity = new Entity("Usuario");
                joinUsers.JoinKey = new Models.Attribute("Id");
                joinUsers.ChildEntity.Attributes.Add(new Models.Attribute("Email"));
                selectJoinEntity usersj = new selectJoinEntity("Id",1,"Aprobador");
                
                Aprob.Keys.Add(new Models.Key("Operacion", "1", "int"));
                List<Entity> Entities = SiteFunctions.GetValues(Aprob, out errmess);
                DataTable dtAprob = SiteFunctions.trnsformEntityToDT(Entities);


                string emailsaprob =  string.Empty;
                foreach(DataRow dr in dtAprob.Rows)
                {
                    if (string.IsNullOrEmpty(emailsaprob))
                    {
                        emailsaprob = dr["Email"].ToString();

                    }
                    else
                    {
                        emailsaprob += ";" + dr["Email"].ToString();
                    }
                }
                if(!string.IsNullOrEmpty(emailsaprob))
                {
                    if (funct.sendEmail(emailsaprob, "Cognitum - Cancelación Póliza " + txtOt.Text, body))
                    {
                        this.showMessage("Se ha iniciado correctamente el proceso de cancelación", false);
                    }
                    else
                    {
                        this.showMessage("Ha habido un error al iniciar el proceso de cancelación", true);
                    }
                }
            }
        }

        protected void btnAprove_Click(object sender, EventArgs e)
        {
            string errmess=string.Empty;
            string id = Request.QueryString["id"];
            List<Entity> Pendientes = new List<Entity>();

            if(checkifPending("",id,false,out Pendientes))
            {
                foreach (Entity aprobpend in Pendientes)
                {
                    if(aprobpend.getAttrValueByName("UsuarioAutorizo") == SiteFunctions.getuserid(Context.User.Identity.Name,true))
                    {
                        Entity Aprobar = new Entity("AutorizacionCancelPol");
                        Aprobar.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "1", "int"));
                        if(aprobpend.getAttrValueByName("Aprobador") == ConfigurationManager.AppSettings["UsuarioCobranza"])
                        {
                            Aprobar.Attributes.Add(new Models.Attribute("UsuarioAutorizo",SiteFunctions.getuserid(Context.User.Identity.Name,false),"int64"));
                        }
                        Aprobar.Attributes.Add(new Models.Attribute("FechaRespuesta",DateTime.Now.ToString("yyyy-MM-dd"),"datetime"));
                        Aprobar.Keys.Add(new Key("Poliza",id,"int64"));
                        Aprobar.Keys.Add(new Key("UsuarioAutorizo",aprobpend.getAttrValueByName("UsuarioAutorizo"),"int64"));

                        if (SiteFunctions.SaveEntity(Aprobar, Method.PUT, out errmess))
                        {
                            this.showMessage(errmess,true);
                        }
                    }
                }
                if(Pendientes.Count == 1)
                {
                    errmess = string.Empty;
                    Entity CancelaPoliza = new Entity("Poliza");
            
                    CancelaPoliza.Action = 1;
                    bool Result = SiteFunctions.SaveEntity(CancelaPoliza, Method.PUT, out errmess);
                    if (!Result)
                    {
                        this.showMessage(errmess, true);
                    }
                }
            }
            
            
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            string errmess=string.Empty;
            string id = Request.QueryString["id"];
            string ExecUserid = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            List<Entity> Pendientes = new List<Entity>();

            if(!checkifPending("",id,false,out Pendientes))
            {
                foreach (Entity aprobpend in Pendientes)
                {
                    if(aprobpend.getAttrValueByName("UsuarioAutorizo") == SiteFunctions.getuserid(Context.User.Identity.Name,true))
                    {
                        Entity Aprobar = new Entity("AutorizacionCancelPol");
                        Aprobar.Attributes.Add(new Models.Attribute("EstatusAutorizacion", "2", "int"));
                        if(aprobpend.getAttrValueByName("Aprobador") == ConfigurationManager.AppSettings["UsuarioCobranza"])
                        {
                            Aprobar.Attributes.Add(new Models.Attribute("UsuarioAutorizo",SiteFunctions.getuserid(Context.User.Identity.Name,false),"int64"));
                        }
                        Aprobar.Attributes.Add(new Models.Attribute("FechaRespuesta",DateTime.Now.ToString("yyyy-MM-dd"),"datetime"));
                        Aprobar.Keys.Add(new Key("Poliza",id,"int64"));
                        Aprobar.Keys.Add(new Key("UsuarioAutorizo",aprobpend.getAttrValueByName("UsuarioAutorizo"),"int64"));
                        if(SiteFunctions.SaveEntity(Aprobar,Method.PUT,out errmess))
                        {
                            this.showMessage(errmess,true);
                        }
                        else
                        {
                            Entity Comment = new Entity("ComentariosAut");
                            Comment.Attributes.Add(new Models.Attribute("IdReegistroRelacionado", id, "int64"));
                            Comment.Attributes.Add(new Models.Attribute("Comentario", txtComments.Text.Trim(), "string"));
                            Comment.Attributes.Add(new Models.Attribute("Usuario", SiteFunctions.getuserid(Context.User.Identity.Name, false), "string"));
                            errmess = string.Empty;
                            if (SiteFunctions.SaveEntity(Comment, Method.POST, out errmess))
                            {
                                this.showMessage(errmess, true);
                            }
                        }
                    }
                }
                
            }
            
        }

        



    }
}