﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CancelPoliza.aspx.cs" Inherits="CognisoWebApp.CancelPoliza" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtInicio]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFin]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            $('#myTab a[href="#' + paneName + '"]').tab('show')

        }


    </script>

    <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Poliza</asp:Label>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="CancelPolizasList.aspx">Lista de Pólizas a Cancelar</a></li>
                </ol>
            </nav>
            <div id="errMess" >

             </div>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Comentarios de Rechazo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="400px" Height="200px"></asp:TextBox>
                    </div>
            
                 <div class="modal-footer">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <asp:Button ID="btnSaveReject" runat="server" Text="Rechazar" OnClick="btnReject_Click"  Cssclass="btn btn-outline-secondary" />
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-heading">
                <div class="btn-group">

                     
                    <asp:LinkButton ID="BtnCancel" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;" OnClick="BtnCancel_Click">
                        <asp:Image ID="Image1" runat="server" ImageUrl="/Content/Imgs/cancel.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                        <asp:Label ID="Label5" runat="server" Text="Iniciar Proceso de Cancelacion de Póliza"></asp:Label>
                     </asp:LinkButton>
                    <asp:LinkButton ID="btnAprove" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;" OnClick="btnAprove_Click">
                        <asp:Image ID="Image2" runat="server" ImageUrl="/Content/Imgs/ok.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                        <asp:Label ID="Label1" runat="server" Text="Aprobar Cancelación"></asp:Label>
                     </asp:LinkButton>
                    <asp:LinkButton ID="btnReject" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;"  data-toggle="modal" data-target="#exampleModal">
                        <asp:Image ID="Image3" runat="server" ImageUrl="/Content/Imgs/Reject.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                        <asp:Label ID="Label2" runat="server" Text="RechazarCancelación"></asp:Label>
                     </asp:LinkButton>
                </div>
            </div>

            <div class="container" runat="server" id="PolizaGral">

                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Direcciones" role="tab" aria-controls="Direcciones" aria-selected="false">Direcciones</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Gastos-tab" data-toggle="tab" href="#Gastos" role="tab" aria-controls="Gastos" aria-selected="false">Gastos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Comisiones-tab" data-toggle="tab" href="#Comisiones" role="tab" aria-controls="Comisiones" aria-selected="false">Comisiones</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Slip-tab" data-toggle="tab" href="#Slip" role="tab" aria-controls="Slip" aria-selected="false">Slip</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Coberturas-tab" data-toggle="tab" href="#Coberturas" role="tab" aria-controls="Coberturas" aria-selected="false">Coberturas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Recibos-tab" data-toggle="tab" href="#Recibos" role="tab" aria-controls="Recibos" aria-selected="false">Recibos</a>
                        </li>
                        
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblOt" runat="server" Text="OT"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtOt" runat="server" CssClass="form-control" TabIndex="1" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblNoPoliza" runat="server" Text="No. Poliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNopoliza" runat="server" CssClass="form-control" TabIndex="2" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblCliente" runat="server" Text="Contratante"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCliente" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblTitular" runat="server" Text="Titular"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtTitular" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFormaPago" runat="server" Text="F. de Pago"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtFormaPago" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblInicio" runat="server" Text="Inicio de Vigencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtInicio" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblFin" runat="server" Text="Fin de Vigencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtFin" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRamo" runat="server" Text="Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtRamo" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblSubramo" runat="server" Text="Sub Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtSubramo" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatus" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblAgente" runat="server" Text="Agente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAgente" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRenovable" runat="server" Text="Renovable"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="chckRenovable" runat="server" Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblPolOriginal" runat="server" Text="Poliza original"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtPolOriginal" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblPolNueva" runat="server" Text="Poliza nueva"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtPolNueva" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                        <div class="tab-pane fade" id="Direcciones" role="tabpanel" aria-labelledby="Direcciones-tab">
                            <div class="container">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDirFiscal" runat="server" Text="Dirección Fiscal"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDirFiscal" runat="server" class="form-control" Enabled="false" TextMode="MultiLine" Width="450px" >
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblDirEnvio" runat="server" Text="Dirección Envío"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDirEnvio" runat="server" class="form-control" Enabled="false" TextMode="MultiLine" Width="450px">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDirCobro" runat="server" Text="Dirección Cobro"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDirCobro" runat="server" class="form-control" Enabled="false" TextMode="MultiLine" Width="450px">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Gastos" role="tabpanel" aria-labelledby="Gastos-tab">
                            <div class="container">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblMoneda" runat="server" Text="Moneda"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMoneda" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPrima" runat="server" Text="Prima"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrima" runat="server" CssClass="form-control" TabIndex="1" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblGastos" runat="server" Text="Gastos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtGastos" runat="server" CssClass="form-control" TabIndex="2" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRecargosPor" runat="server" Text="Recargos %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRecargosPor" runat="server" CssClass="form-control" TabIndex="3" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblRecargos" runat="server" Text="Recargos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRecargos" runat="server" CssClass="form-control" TabIndex="4" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNeto" runat="server" Text="Neto"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNeto" runat="server" CssClass="form-control" TabIndex="5" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblDescuento" runat="server" Text="Descuento"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDescuento" runat="server" CssClass="form-control" TabIndex="6" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblImpuestoPor" runat="server" Text="Impuesto %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtImpuestoPor" runat="server" CssClass="form-control" TabIndex="7" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblImpuesto" runat="server" Text="Impuesto"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtImpuesto" runat="server" CssClass="form-control" TabIndex="8" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" TabIndex="7" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblEstatusCobranza" runat="server" Text="Estatus Cobranza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatusCobranza" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Comisiones" role="tabpanel" aria-labelledby="Comisiones-tab">
                            <div class="container">
                               
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblComisionPor" runat="server" Text="Comision %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComisionPor" runat="server" CssClass="form-control" TabIndex="1" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblComision" runat="server" Text="Comision"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComision" runat="server" CssClass="form-control" TabIndex="2" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSobreComisionPor" runat="server" Text="SobreComision %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSobreComisionPor" runat="server" CssClass="form-control" TabIndex="3" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblSobreComision" runat="server" Text="SobreComision"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSobreComision" runat="server" CssClass="form-control" TabIndex="4" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBonoPor" runat="server" Text="Bono %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBonoPor" runat="server" CssClass="form-control" TabIndex="5" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblBono" runat="server" Text="Bono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBono" runat="server" CssClass="form-control" TabIndex="6" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                               
                            <div class="row">
                                  <div class="col">
                                        <asp:Label ID="LblTotalCom" runat="server" Text="Total Comisiones"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCom" runat="server" CssClass="form-control" TabIndex="5" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMesa" runat="server" Text="Mesa de Control"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMesa" runat="server" class="form-control" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </div>
                                 </div>

                        </div>
                        <div class="tab-pane fade" id="Slip" role="tabpanel" aria-labelledby="Slip-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSlip" runat="server" Text="Slip"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSlip"  CssClass="form-control"  runat="server" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Coberturas" role="tabpanel" aria-labelledby="Coberturas-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCoberturas" runat="server" Text="Coberturas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCoberturas" CssClass="form-control"  runat="server" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="CondSpec" role="tabpanel" aria-labelledby="CondSpec-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCondSpec" runat="server" Text="Condiciones especiales"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCondSpec"  CssClass="form-control"  runat="server" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Recibos" role="tabpanel" aria-labelledby="Recibos-tab">
                            <div class="container">
                                <div class="input-group mb-3">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search"   OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <div class="input-group-append">
                                        <asp:Button ID="btnBuscarRecibos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscarRecibos_Click"></asp:Button>
                    
                                    </div>
                                </div>
                                <div class="panel-body" runat="server">

                                    <div class="table-responsive" runat="server">
                                        <asp:GridView ID="GVRecibosList" runat="server" OnPageIndexChanging="GVRecibosList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Concepto" HeaderText="Concepto" />
                                                <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                <asp:BoundField DataField="Total" HeaderText="Total" />
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <asp:HiddenField ID="SearchId" runat="server" Value="" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="PoliId" runat="server" />
                <asp:HiddenField ID="DivisionOperativa" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
