﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Aseguradora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                initCatalog();
                object id = Request.QueryString["id"];
                if (id != null)
                {
                    AseguradoraId.Value = id.ToString();
                    searchAseguradora();
                }
                
            }
        }

        protected void searchAseguradora()
        {
            string errmsg = string.Empty;

            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new Models.Attribute("Id"));
            Aseguradora.Attributes.Add(new Models.Attribute("Clave"));
            //Aseguradora.Attributes.Add(new Models.Attribute("DiasAno"));
            Aseguradora.Attributes.Add(new Models.Attribute("DeshabilitaCalculos"));
            Aseguradora.Attributes.Add(new Models.Attribute("CalculoComisionIncluyeRecargo"));
            Aseguradora.Attributes.Add(new Models.Attribute("CalculoRecargoIncluyeDerechos"));
            //Aseguradora.Attributes.Add(new Models.Attribute("PolizasRequierenCaratula"));
            Aseguradora.Keys.Add(new Key("Id", AseguradoraId.Value, "int64"));

            Entity PersonaMoral = new Entity("PersonaMoral");
            PersonaMoral.Attributes.Add(new Models.Attribute("RazonSocial"));
            
            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new Models.Attribute("RFC"));
            Cliente.Attributes.Add(new Models.Attribute("Giro"));
            Cliente.Attributes.Add(new Models.Attribute("Grupo"));
            Cliente.Attributes.Add(new Models.Attribute("IdSAP"));


            selectJoinEntity selJoinEntity = new selectJoinEntity("Id", 1, "Id");

            JoinEntity joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = Cliente;
            PersonaMoral.ChildEntities.Add(joinEntity);

            selJoinEntity = new selectJoinEntity("Id", 1, "Id");

            joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = PersonaMoral;
            Aseguradora.ChildEntities.Add(joinEntity);

             List<Entity> AseguradoraResult = SiteFunctions.GetValues(Aseguradora, out errmsg);

            if (AseguradoraResult.Count > 0)
            {
                Entity Asegura = AseguradoraResult[0];
                txtId.Text = AseguradoraId.Value;
                txtGiro.SelectedValue =  Asegura.getAttrValueByName("ClienteGiro").ToString();
                txtGroup.SelectedValue = Asegura.getAttrValueByName("ClienteGrupo").ToString();
                txtRazonSocial.Text = Asegura.getAttrValueByName("PersonaMoralRazonSocial").ToString();
                //txtReqCaratula.Checked = Convert.ToBoolean(Asegura.getAttrValueByName("PolizasRequierenCaratula"));
                txtDeshabilitaCalculos.Checked = Convert.ToBoolean(Asegura.getAttrValueByName("DeshabilitaCalculos"));
                txtReqIncDer.Checked = Convert.ToBoolean(Asegura.getAttrValueByName("CalculoRecargoIncluyeDerechos"));
                txtConInclReq.Checked = Convert.ToBoolean(Asegura.getAttrValueByName("CalculoComisionIncluyeRecargo"));
                txtRFC.Text = Asegura.getAttrValueByName("ClienteRFC").ToString();
                txtClave.Text = Asegura.getAttrValueByName("Clave").ToString();
                txtEstatus.SelectedValue = Asegura.getAttrValueByName("Audit_Estatus").ToString();
                txtIdentificadorSAP.Text = Asegura.getAttrValueByName("ClienteIdSAP").ToString();
                DateTime fechaaudit = Convert.ToDateTime(Asegura.getAttrValueByName("Audit_FechaAdd").ToString());
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(Asegura.getAttrValueByName("Audit_FechaUMod").ToString());
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", Asegura.getAttrValueByName("Audit_UsuarioAdd").ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errmsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", Asegura.getAttrValueByName("Audit_UsuarioUMod").ToString(), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errmsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }

                Entity Ubicacion = new Entity("Ubicacion");
                Ubicacion.Attributes.Add(new Models.Attribute("Nombre"));
                Ubicacion.Keys.Add(new Models.Key("id", Asegura.getAttrValueByName("Ubicacion"), "int"));
                errmsg = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(Ubicacion, out errmsg);
                if (listResult.Count > 0)
                {
                    txtUbicacion.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }

                Entity CB = new Entity("CentroDeBeneficio");
                CB.Attributes.Add(new Models.Attribute("Nombre"));
                CB.Keys.Add(new Models.Key("id", Asegura.getAttrValueByName("CentroDeBeneficio"), "int"));
                errmsg = string.Empty;
                listResult = SiteFunctions.GetValues(CB, out errmsg);
                if (listResult.Count > 0)
                {
                    txtCentroBeneficios.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }
            }
        }
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveAseguradora();
        }

        private void initCatalog()
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Giro = new Entity("Giro");
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Giro.useAuditable = false;
            listResult = SiteFunctions.GetValues(Giro, out errmess);
            txtGiro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGiro.DataValueField = "Id";
            txtGiro.DataTextField = "Nombre";
            txtGiro.DataBind();

            Entity Grupo = new Entity("Grupo");
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Descripcion"));
            Grupo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Grupo, out errmess);
            txtGroup.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGroup.DataValueField = "Id";
            txtGroup.DataTextField = "Descripcion";
            txtGroup.DataBind();

            Array itemValues = System.Enum.GetValues(typeof(EstatusEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }
        }

        private void SaveAseguradora()
        {

                        
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;


            Entity Aseguradora = new Entity("Aseguradora");
            Aseguradora.Attributes.Add(new Models.Attribute("Clave",txtClave.Text.ToString(),"string"));
            //Aseguradora.Attributes.Add(new Models.Attribute("DiasAno",txt));
            Aseguradora.Attributes.Add(new Models.Attribute("DeshabilitaCalculos",txtDeshabilitaCalculos.Checked?"1":"0","int"));
            Aseguradora.Attributes.Add(new Models.Attribute("CalculoComisionIncluyeRecargo",txtConInclReq.Checked?"1":"0","int"));
            Aseguradora.Attributes.Add(new Models.Attribute("CalculoRecargoIncluyeDerechos",txtReqIncDer.Checked?"1":"0","int"));
            //Aseguradora.Attributes.Add(new Models.Attribute("PolizasRequierenCaratula"));
            
            Entity PersonaMoral = new Entity("PersonaMoral");
            PersonaMoral.Attributes.Add(new Models.Attribute("RazonSocial",txtRazonSocial.Text,"string"));

            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new Models.Attribute("RFC",txtRFC.Text,"string"));
            Cliente.Attributes.Add(new Models.Attribute("Giro",txtGiro.Text,"int64"));
            Cliente.Attributes.Add(new Models.Attribute("Grupo",txtGroup.Text,"int64"));
            Cliente.Attributes.Add(new Models.Attribute("IdSAP",txtIdentificadorSAP.Text,"string"));


            selectJoinEntity selJoinEntity = new selectJoinEntity("Id", 1, "Id");

            JoinEntity joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.JoinKey = new Models.Attribute("Id", "", "int64");
            joinEntity.ChildEntity = Cliente;
            PersonaMoral.ChildEntities.Add(joinEntity);

            selJoinEntity = new selectJoinEntity("Id", 1, "Id");

            joinEntity = new JoinEntity();
            joinEntity.selectJoinList = new List<selectJoinEntity>();
            joinEntity.selectJoinList.Add(selJoinEntity);
            joinEntity.JoinType = JoinType.Inner;
            joinEntity.ChildEntity = PersonaMoral;
            joinEntity.JoinKey = new Models.Attribute("Id", "", "int64");
            Aseguradora.ChildEntities.Add(joinEntity);

            //int ids = 0;
            //int.TryParse(SiteFunctions.getUserUbicacion(Context.User.Identity.Name), out ids);
            //Aseguradora.Attributes.Add(new Models.Attribute("Ubicacion", ids.ToString(), "int64"));
            //ids = 0;
            //int.TryParse(SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name), out ids);
            //Aseguradora.useAuditable = true;
            //Aseguradora.auditable = audi;
            //Aseguradora.Attributes.Add(new Models.Attribute("CentroDeBeneficio", ids.ToString(), "int64"));
           
            string errmess = "";
            string id = AseguradoraId.Value;
            Method method = Method.POST;
            if (!String.IsNullOrEmpty(id))
            {
                method = Method.PUT;
                Aseguradora.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);

            }
            Aseguradora.auditable = audi;
            if (SiteFunctions.SaveEntity(Aseguradora, method, out errmess))
            {
                showMessage("Se guardo correctamente la Aseguradora", false);
                Session["retVal"] = errmess == "0" ?AseguradoraId.Value:errmess;
                //if (Request.QueryString["popup"] != null)
                //{
                //    // Define the name and type of the client scripts on the page.
                //    String csname1 = "returnval";
                //    Type cstype = this.GetType();
                //    // Get a ClientScriptManager reference from the Page class.
                //    ClientScriptManager cs = Page.ClientScript;
                //    if (!cs.IsStartupScriptRegistered(cstype, csname1))
                //    {
                //        StringBuilder cstext1 = new StringBuilder();
                //        cstext1.Append(@"<script type='text/javascript'> windows.close();");
                //        cstext1.Append("</script>");

                //        cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
                //    }
                    
                //}
                
            }
            else
            {
                showMessage(errmess, true);
            }
        }


        

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                 cstext1.Append("script>");
                
                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        public void searchEjecutivos()
        {
            string Errmess = string.Empty;
            Entity Ejecutivos = new Entity("ContactoPersonaMoral");
            Ejecutivos.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Ejecutivos.Attributes.Add(new Models.Attribute("Email"));
            Ejecutivos.Attributes.Add(new Models.Attribute("Puesto"));
            Ejecutivos.Keys.Add(new Key("PersonaMoral", AseguradoraId.Value, "int64"));
            List<Entity> result = SiteFunctions.GetValues(Ejecutivos, out Errmess);
            DataTable Ejecutivo = SiteFunctions.trnsformEntityToDT(result, false);
            GVEjecutivoList.DataSource = Ejecutivo;
            GVEjecutivoList.DataBind();

        }

        public void searchDirecciones()
        {
            string Errmess = string.Empty;
            Entity Direcciones = new Entity("Direcciones");
            Direcciones.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Direcciones.Attributes.Add(new Models.Attribute("Tipo"));
            Direcciones.Attributes.Add(new Models.Attribute("NumExterior"));
            Direcciones.Attributes.Add(new Models.Attribute("NumInterior"));
            Direcciones.Keys.Add(new Key("Cliente", AseguradoraId.Value, "int64"));
            List<Entity> result = SiteFunctions.GetValues(Direcciones, out Errmess);
            DataTable Dir = SiteFunctions.trnsformEntityToDT(result, false);
            GVDirList.DataSource = Dir;
            GVDirList.DataBind();

        }
        public void searchTramites()
        {
            string Errmess = string.Empty;
            Entity Tramites = new Entity("TramitesAseguradora");
            Tramites.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Tramites.Attributes.Add(new Models.Attribute("Nombre"));
            Tramites.Attributes.Add(new Models.Attribute("DuracionDias"));
            Tramites.Attributes.Add(new Models.Attribute("Aseguradora"));
            Tramites.Keys.Add(new Key("Aseguradora", AseguradoraId.Value, "int64"));
            List<Entity> result = SiteFunctions.GetValues(Tramites, out Errmess);
            DataTable tramite = SiteFunctions.trnsformEntityToDT(result, false);
            GVDirList.DataSource = tramite;
            GVDirList.DataBind();

        }

        protected void GVEjecutivoList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Ejecutivos";
            GVEjecutivoList.PageIndex = e.NewPageIndex;
            GVEjecutivoList.DataBind();
            searchEjecutivos();
        }

        protected void GVTramites_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Tramites";
            GVTramites.PageIndex = e.NewPageIndex;
            GVTramites.DataBind();
            searchTramites();
        }

        protected void GVDirList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Direcciones";
            GVDirList.PageIndex = e.NewPageIndex;
            GVDirList.DataBind();
            searchDirecciones();

        }
    }
}