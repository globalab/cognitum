﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class SiniestroToXML
    {
        /// <summary>
        /// Permite tomar una instancia de PolizaGeneral para transformarla en formato XML.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaGeneral de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del PolizaGeneral en formato XML.</returns>
        public static XElement serializaSiniestros(List<Entity> Siniestros)
        {
            XElement _Siniestros = new XElement("Siniestros");
            foreach(Entity Siniestro in Siniestros)
            {
                _Siniestros.Add(serializaSiniestro(Siniestro));
            }
            return _Siniestros;
        }

        /// <summary>
        /// Permite tomar una instancia de Siniestro para transformarla en formato XML.
        /// </summary>
        /// <param name="s">Instancia de Siniestro de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del Siniestro en formato XML.</returns>
        public static XElement serializaSiniestro(Entity s)
        {
            string errmess = string.Empty;
            int value = Convert.ToInt32(s.getAttrValueByName("Estatus"));
            var enumDisplayStatus = (EstatusEnum )value;
            value = Convert.ToInt32(s.getAttrValueByName("EstatusSiniestro"));
            var EstatusSiniestro = (EstatusSiniestroEnum)value;
            var FechaAdd = Convert.ToDateTime(s.getAttrValueByName("Audit_FechaAdd"));
            var FechaMod = Convert.ToDateTime(s.getAttrValueByName("Audit_FechaUMod"));
            var UsuarioAdd = new Entity("Usuario");
            UsuarioAdd.Attributes.Add(new Attribute("Nombre"));
            UsuarioAdd.Keys.Add(new Key("Id",s.getKeyValueByName("Audit_UsuarioAdd").ToString(),"int"));
            UsuarioAdd = SiteFunctions.GetEntity(UsuarioAdd,out errmess);
            var UsuarioUmod = new Entity("Usuario");
            UsuarioUmod.Attributes.Add(new Attribute("Nombre"));
            UsuarioUmod.Keys.Add(new Key("Id",s.getKeyValueByName("Audit_UsuarioUMod").ToString(),"int"));
            UsuarioUmod = SiteFunctions.GetEntity(UsuarioUmod,out errmess);
            var xml = new XElement("Siniestro",
                                   new XElement("Estatus", enumDisplayStatus),
                                   new XElement("EstatusSiniestro",
                                                EstatusSiniestro),
                                   new XElement("FechaAdd",
                                                FechaAdd.ToString("dd/MM/yyyy")),
                                   new XElement("FechaUMod",
                                                FechaMod.ToString("dd/MM/yyyy")),
                                   new XElement("Id", s.getAttrValueByName("Id").ToString()),
                                   new XElement("Numero", s.getAttrValueByName("Numero").ToString()),
                                   new XElement("UsuarioAdd", UsuarioAdd.getAttrValueByName("Nombre").ToString()),
                                   new XElement("UsuarioUMod", UsuarioUmod.getAttrValueByName("Nombre").ToString())
                );
            Entity SiniestroType = new Entity("SiniestroAuto");
            SiniestroType.Keys.Add(new Key("Id", s.getKeyValueByName("Id").ToString(), "int"));
            List<Entity> result = SiteFunctions.GetValues(SiniestroType, out errmess);
            if(result.Count > 0)
            {
                var sinAut = result[0];
                serializaSiniestroAuto(s,sinAut, xml);
            }
            else 
            {
                SiniestroType = new Entity("SiniestroBeneficios");
                SiniestroType.Keys.Add(new Key("Id", s.getKeyValueByName("Id").ToString(), "int"));
                result = SiteFunctions.GetValues(SiniestroType, out errmess);
                if(result.Count > 0)
                {
                    var sinBen = result[0];
                    serializaSiniestroBeneficios(sinBen, xml);
                }
                else 
                {
                    SiniestroType = new Entity("SiniestroDanhos");
                    SiniestroType.Keys.Add(new Key("Id", s.getKeyValueByName("Id").ToString(), "int"));
                    result = SiteFunctions.GetValues(SiniestroType, out errmess);
                    if (result.Count > 0)
                    {
                        var sinDan = result[0];
                        serializaSiniestroDanhos(s, sinDan, xml);
                    }
                }
            }
            

            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroAuto para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroAuto de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        private static void serializaSiniestroAuto(Entity siniestro, Entity siniestroaut,XElement xml)
        {
            string Errmsg = string.Empty;
            Entity ApoyoMensajeria = new Entity("ApoyoMensajeria");
            ApoyoMensajeria.Keys.Add(new Key("Id", siniestro.getKeyValueByName("Id").ToString(), "int"));
            ApoyoMensajeria.Attributes.Add(new Attribute("ContactoEntrega"));
            ApoyoMensajeria.Attributes.Add(new Attribute("FechaEntrega"));
            ApoyoMensajeria = SiteFunctions.GetEntity(ApoyoMensajeria, out Errmsg);

            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.Attributes.Add(new Attribute("Id"));
            bienAsegurado.Attributes.Add(new Attribute("Permiso"));
            bienAsegurado.Attributes.Add(new Attribute("Inciso"));
            bienAsegurado.Attributes.Add(new Attribute("Estatus"));
            bienAsegurado.Attributes.Add(new Attribute("Prima"));
            bienAsegurado.Attributes.Add(new Attribute("Identificador"));
            bienAsegurado.Attributes.Add(new Attribute("Ubicacion"));
            bienAsegurado.Attributes.Add(new Attribute("CentroDeBeneficio"));
            bienAsegurado.Attributes.Add(new Attribute("Propietario"));
            bienAsegurado.Attributes.Add(new Attribute("Poliza"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoAlta"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoBaja"));
            bienAsegurado.Attributes.Add(new Attribute("GrupoAsegurados"));
            bienAsegurado.Attributes.Add(new Attribute("BeneficiarioPreferente"));
            bienAsegurado.Attributes.Add(new Attribute("Empleado"));
            bienAsegurado.Attributes.Add(new Attribute("TarjetaBancaria"));
            bienAsegurado.Keys.Add(new Key("Id", siniestro.getAttrValueByName("BienAsegurado"), "int"));
            bienAsegurado.useAuditable=true;
            bienAsegurado = SiteFunctions.GetEntity(bienAsegurado, out Errmsg);

            Entity conductor = new Entity("ContactoSimple");
            conductor.Attributes.Add(new Attribute("NombreCompleto"));
            conductor.Attributes.Add(new Attribute("Email"));
            conductor.Keys.Add(new Key("Id", siniestroaut.getAttrValueByName("Conductor"), "int"));

            conductor = SiteFunctions.GetEntity(conductor, out Errmsg);

            Entity contactoSin = new Entity("ContactoSimple");
            contactoSin.Attributes.Add(new Attribute("NombreCompleto"));
            contactoSin.Attributes.Add(new Attribute("Email"));
            contactoSin.Keys.Add(new Key("Id", siniestroaut.getAttrValueByName("ContactoSiniestro"), "int"));

            contactoSin = SiteFunctions.GetEntity(contactoSin, out Errmsg);

            string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

            PolizaToXML.getFieldRelatedToMov(bienAsegurado.getAttrValueByName("CentroDeBeneficio"), bienAsegurado.getAttrValueByName("Propietario")
                ,bienAsegurado.getAttrValueByName("Ubicacion"), bienAsegurado.getAttrValueByName("AuditableUsuarioAdd"), bienAsegurado.getAttrValueByName("AuditableUsuarioUMod")
                , bienAsegurado.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);

            Entity ReparacionVehiculo = new Entity("ReparacionVehiculo");
            ReparacionVehiculo.Attributes.Add(new Attribute("Taller"));
            ReparacionVehiculo.Attributes.Add(new Attribute("TipoTaller"));
            ReparacionVehiculo.Attributes.Add(new Attribute("Piramide"));
            ReparacionVehiculo.Attributes.Add(new Attribute("OrdenServicio"));
            ReparacionVehiculo.Attributes.Add(new Attribute("FechaIngreso"));
            ReparacionVehiculo.Attributes.Add(new Attribute("FechaEntrega"));
            ReparacionVehiculo.Attributes.Add(new Attribute("FechaPromesaEntrega"));
            ReparacionVehiculo.Attributes.Add(new Attribute("NumeroTelefonico"));
            ReparacionVehiculo.Attributes.Add(new Attribute("JefeServicio"));
            ReparacionVehiculo.Attributes.Add(new Attribute("Id"));
            ReparacionVehiculo.Keys.Add(new Key("Id", siniestroaut.getAttrValueByName("Id"), "int"));

            ReparacionVehiculo = SiteFunctions.GetEntity(ReparacionVehiculo, out Errmsg);

            Entity JefeServicio = new Entity("ContactoSimple");
            if (ReparacionVehiculo != null)
            {
                JefeServicio.Attributes.Add(new Attribute("NombreCompleto"));
                JefeServicio.Attributes.Add(new Attribute("Email"));
                JefeServicio.Keys.Add(new Key("Id", ReparacionVehiculo.getAttrValueByName("JefeServicio"), "int"));
                JefeServicio = SiteFunctions.GetEntity(JefeServicio, out Errmsg);
            }

            Entity siniestroDanos = new Entity("SiniestroDanhos");
            siniestroDanos.Attributes.Add(new Attribute("TramiteAdmin"));
            siniestroDanos.Attributes.Add(new Attribute("Documentacion"));
            siniestroDanos.Keys.Add(new Key("Id", siniestro.getAttrValueByName("Id"), "int"));

            siniestroDanos = SiteFunctions.GetEntity(siniestroDanos, out Errmsg);
            string docCompleta = string.Empty, docFaltante = string.Empty;
            Entity tramiteAdmin = new Entity("TramiteAdmin");
            if (siniestroDanos != null)
            {
                Entity docs = new Entity("Documentacion");
                docs.Attributes.Add(new Attribute("DoctosFaltantes"));
                docs.Attributes.Add(new Attribute("Completa"));
                docs.Keys.Add(new Key("Id", siniestroDanos.getAttrValueByName("Documentacion"), "int"));

                docs = SiteFunctions.GetEntity(docs, out Errmsg);

                if (docs != null)
                {
                    docCompleta = docs.getAttrValueByName("Completa");
                    docFaltante = docs.getAttrValueByName("DoctosFaltantes");
                }

                tramiteAdmin.Attributes.Add(new Attribute("Descripcion"));
                tramiteAdmin.Attributes.Add(new Attribute("FechaInicio"));
                tramiteAdmin.Attributes.Add(new Attribute("FechaConclusion"));
                tramiteAdmin.Keys.Add(new Key("Id", siniestroDanos.getAttrValueByName("TramiteAdmin"), "int"));

                tramiteAdmin = SiteFunctions.GetEntity(tramiteAdmin, out Errmsg);
            }

            string NombreCompleto = string.Empty;
            string Email = string.Empty;
            DateTime? FechaEntrega = null;
            string FechaEntregastr = string.Empty;
            Entity Contacto = new Entity("ContactoSimple");
            if (ApoyoMensajeria != null)
            {
                
                Contacto.Attributes.Add(new Attribute("NombreCompleto"));
                Contacto.Attributes.Add(new Attribute("Email"));
                Contacto.Keys.Add(new Key("Id",ApoyoMensajeria.getKeyValueByName("ContactoEntrega").ToString(),"int"));
                Contacto = SiteFunctions.GetEntity(Contacto, out Errmsg);
               
                
            }
            string NombreCompletoAbogado = string.Empty;
            string EmailAbogado = string.Empty;
            string NumeroReporte = string.Empty;
            string Provedor = string.Empty;
            Entity AsesoriaLegal = new Entity("AsesoriaLegal");
            AsesoriaLegal.Keys.Add(new Key("Id", siniestro.getKeyValueByName("Id").ToString(), "int"));
            AsesoriaLegal.Attributes.Add(new Attribute("Abogado"));
            AsesoriaLegal.Attributes.Add(new Attribute("NumeroReporte"));
            AsesoriaLegal.Attributes.Add(new Attribute("Provedor"));
            AsesoriaLegal = SiteFunctions.GetEntity(ApoyoMensajeria, out Errmsg);
            Entity ContactoAbogado = new Entity("ContactoSimple");
            if (AsesoriaLegal != null)
            {
                
                ContactoAbogado.Attributes.Add(new Attribute("NombreCompleto"));
                ContactoAbogado.Attributes.Add(new Attribute("Email"));
                ContactoAbogado.Keys.Add(new Key("Id", AsesoriaLegal.getKeyValueByName("Abogado").ToString(), "int"));
                ContactoAbogado = SiteFunctions.GetEntity(ContactoAbogado, out Errmsg);
                

            }

            xml.Add(
                    new XElement("ApoyoMensajeria",
                        new XElement("ContactoEntrega",
                            new XElement("NombreCompleto", getEntityValue(Contacto, "NombreCompleto")),
                            new XElement("Email", Email)
                        )
                    ),
                    new XElement("FechaEntrega", getEntityValue(ApoyoMensajeria,"FechaEntrega")),
                    new XElement("AsesoriaLegal",
                        new XElement("Abogado",
                            new XElement("NombreCompleto", getEntityValue(ContactoAbogado,"NombreCompleto")),
                            new XElement("Email", getEntityValue(ContactoAbogado, "Email"))
                        ),
                        new XElement("NumeroReporte", getEntityValue(AsesoriaLegal, "NumeroReporte")),
                        new XElement("Provedor", getEntityValue(AsesoriaLegal, "Proveedor"))
                    ),
                    PolizaToXML.serializaMovimiento(bienAsegurado, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario),
                    new XElement("Conductor",
                        new XElement("Email", conductor.getAttrValueByName("Email")),
                        new XElement("NombreCompleto", conductor.getAttrValueByName("NombreCompleto"))
                    ),
                    new XElement("ContactoSiniestro",
                            new XElement("NombreCompleto", contactoSin.getAttrValueByName("NombreCompleto")),
                            new XElement("Email", contactoSin.getAttrValueByName("Email"))
                        ),
                    new XElement("Documentacion",
                        new XElement("Documentacion", docCompleta),
                        new XElement("Documentacion", docFaltante)
                    ),
                    new XElement("ReparacionVehiculo",
                        new XElement("FechaEntrega", DateTime.Parse(ReparacionVehiculo.getAttrValueByName("FechaEntrega")).ToString("dd/MM/yyyy")),
                        new XElement("FechaIngreso", DateTime.Parse(ReparacionVehiculo.getAttrValueByName("FechaIngreso")).ToString("dd/MM/yyyy")),
                        new XElement("FechaPromesaEntrega", DateTime.Parse(ReparacionVehiculo.getAttrValueByName("FechaPromesaEntrega")).ToString("dd/MM/yyyy")),
                        new XElement("JefeServicio",
                            new XElement("NombreCompleto", JefeServicio.getAttrValueByName("NombreCompleto")),
                            new XElement("Email", JefeServicio.getAttrValueByName("Email"))
                        ),
                        new XElement("NumeroTelefonico", ReparacionVehiculo.getAttrValueByName("NumeroTelefonico")),
                        new XElement("OrdenServicio", ReparacionVehiculo.getAttrValueByName("OrdenServicio")),
                        new XElement("Piramide", ReparacionVehiculo.getAttrValueByName("Piramide")),
                        new XElement("Taller", ReparacionVehiculo.getAttrValueByName("Taller")),
                        new XElement("TipoTaller", ReparacionVehiculo.getAttrValueByName("TipoTaller"))
                    ),
                    new XElement("TipoSiniestroAuto", siniestroaut.getAttrValueByName("TipoSiniestroAuto")),
                    new XElement("TramiteAdmin",
                        new XElement("Descripcion", tramiteAdmin.getAttrValueByName("Descripcion")),
                        new XElement("FechaConclusion", tramiteAdmin.getAttrValueByName("FechaConclusion")),
                        new XElement("FechaInicio", tramiteAdmin.getAttrValueByName("FechaInicio"))
                    )
            );

        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroBeneficios para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroBeneficios de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del SiniestroBeneficios en formato XML.</returns>
        private static XElement serializaSiniestroBeneficios(Entity siniestro, XElement xml)
        {
            string errMsg = string.Empty;
            Entity Certificado = new Entity("Certificado");
            Certificado.Attributes.Add(new Attribute("Nombre"));
            Certificado.Attributes.Add(new Attribute("ApellidoPaterno"));
            Certificado.Attributes.Add(new Attribute("ApellidoMaterno"));
            Certificado.Attributes.Add(new Attribute("Titular"));
            Certificado.Keys.Add(new Key("Id", siniestro.getAttrValueByName("BienAsegurado"), "int"));

            Certificado = SiteFunctions.GetEntity(Certificado, out errMsg);

            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.Attributes.Add(new Attribute("Id"));
            bienAsegurado.Attributes.Add(new Attribute("Permiso"));
            bienAsegurado.Attributes.Add(new Attribute("Inciso"));
            bienAsegurado.Attributes.Add(new Attribute("Estatus"));
            bienAsegurado.Attributes.Add(new Attribute("Prima"));
            bienAsegurado.Attributes.Add(new Attribute("Identificador"));
            bienAsegurado.Attributes.Add(new Attribute("Ubicacion"));
            bienAsegurado.Attributes.Add(new Attribute("CentroDeBeneficio"));
            bienAsegurado.Attributes.Add(new Attribute("Propietario"));
            bienAsegurado.Attributes.Add(new Attribute("Poliza"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoAlta"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoBaja"));
            bienAsegurado.Attributes.Add(new Attribute("GrupoAsegurados"));
            bienAsegurado.Attributes.Add(new Attribute("BeneficiarioPreferente"));
            bienAsegurado.Attributes.Add(new Attribute("Empleado"));
            bienAsegurado.Attributes.Add(new Attribute("TarjetaBancaria"));
            bienAsegurado.useAuditable = true;
            bienAsegurado.Keys.Add(new Key("Id", siniestro.getAttrValueByName("BienAsegurado"), "int"));

            string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

            PolizaToXML.getFieldRelatedToMov(bienAsegurado.getAttrValueByName("CentroDeBeneficio"), bienAsegurado.getAttrValueByName("Propietario")
                , bienAsegurado.getAttrValueByName("Ubicacion"), bienAsegurado.getAttrValueByName("AuditableUsuarioAdd"), bienAsegurado.getAttrValueByName("AuditableUsuarioUMod"),
                bienAsegurado.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);

            var afectado = Certificado.getAttrValueByName("Nombre") + " " + Certificado.getAttrValueByName("ApellidoPaterno") + " " +
                           Certificado.getAttrValueByName("ApellidoMaterno");

            string titular = null;
            if (Certificado.getAttrValueByName("Titular") != string.Empty)
            {
                Entity titularEnt = new Entity("Certificado");
                titularEnt.Attributes.Add(new Attribute("Nombre"));
                titularEnt.Attributes.Add(new Attribute("ApellidoPaterno"));
                titularEnt.Attributes.Add(new Attribute("ApellidoMaterno"));
                titularEnt.Keys.Add(new Key("Id", Certificado.getAttrValueByName("Titular"), "int"));

                titularEnt = SiteFunctions.GetEntity(titularEnt, out errMsg);

                if (titularEnt != null)
                {
                    titular = titularEnt.getAttrValueByName("Nombre") + " " + titularEnt.getAttrValueByName("ApellidoPaterno") + " " +
                               titularEnt.getAttrValueByName("ApellidoMaterno");
                }
                else
                {
                    titular = afectado;
                }
            }
            else
            {
                titular = afectado;
            }

            Entity Contacto = new Entity("ContactoSimple");
            Contacto.Attributes.Add(new Attribute("NombreCompleto"));
            Contacto.Attributes.Add(new Attribute("Email"));
            Contacto.Keys.Add(new Key("Id", siniestro.getAttrValueByName("Contacto"), "int"));

            xml.Add(
                PolizaToXML.serializaMovimiento(bienAsegurado, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario),
                new XElement("Coaseguro", siniestro.getAttrValueByName("Coaseguro")),
                //siniestro.Complementos != null ? serializaComplementosBeneficios(siniestro, xml) : new XElement("Complementos"),
                new XElement("Contacto",
                             new XElement("NombreCompleto", Contacto.getAttrValueByName("NombreCompleto")),
                             new XElement("Email", Contacto.getAttrValueByName("Email"))
                ),
                 new XElement("Titular", titular),
                 new XElement("Afectado", afectado),
                 new XElement("Deducible", Convert.ToDecimal(siniestro.getAttrValueByName("Deducible")).ToString("C")),
                 new XElement("GastosNoCubiertos", Convert.ToDecimal(siniestro.getAttrValueByName("GastosNoCubiertos")).ToString("C")),
                 new XElement("isComplemento", siniestro.getAttrValueByName("IsComplemento")),
                 new XElement("MontoRestante", Convert.ToDecimal(siniestro.getAttrValueByName("MontoRestante")).ToString("C")),
                 new XElement("Padecimiento", siniestro.getAttrValueByName("Padecimiento")),serializaPagosBeneficios(siniestro.getAttrValueByName("Id")),
                 new XElement("TipoReclamacion", siniestro.getAttrValueByName("TipoReclamacion")),
                 new XElement("TipoSiniestro", siniestro.getAttrValueByName("TipoSiniestro")),
                 new XElement("TotalAnticipado", Convert.ToDecimal(siniestro.getAttrValueByName("TotalAnticipado")).ToString("C")),
                 new XElement("TotalPagado", Convert.ToDecimal(siniestro.getAttrValueByName("TotalPagado")).ToString("C")),
                 new XElement("TotalReclamado", Convert.ToDecimal(siniestro.getAttrValueByName("TotalReclamado")).ToString("C")),
                 new XElement("TotalPagar", GetTotalPagar(siniestro).ToString("C"))
                );
            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroBeneficios para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroBeneficios de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del SiniestroBeneficios en formato XML.</returns>
        private static XElement serializaComplementosBeneficios(string siniestroId, XElement xml)
        {
            string errMsg = string.Empty;
            Entity Complementos = new Entity("ComplementoSiniestro");
            Complementos.Attributes.Add(new Attribute("Id"));
            Complementos.Attributes.Add(new Attribute("Siniestro"));
            Complementos.Attributes.Add(new Attribute("Poliza"));
            Complementos.Keys.Add(new Key("Id", siniestroId, "int"));

            List<Entity> result = SiteFunctions.GetValues(Complementos, out errMsg);

            XElement xEComplementos = new XElement("Complementos");

            foreach (Entity compl in result)
            {
                Entity siniestro = new Entity("SiniestroBeneficios");
                siniestro.Attributes.Add(new Attribute("Id"));
                siniestro.Attributes.Add(new Attribute("TipoReclamacion"));
                siniestro.Attributes.Add(new Attribute("TipoSiniestro"));
                siniestro.Attributes.Add(new Attribute("Especialidad"));
                siniestro.Attributes.Add(new Attribute("Padecimiento"));
                siniestro.Attributes.Add(new Attribute("TotalReclamado"));
                siniestro.Attributes.Add(new Attribute("Deducible"));
                siniestro.Attributes.Add(new Attribute("Coaseguro"));
                siniestro.Attributes.Add(new Attribute("DeduciblePct"));
                siniestro.Attributes.Add(new Attribute("CoaseguroPct"));
                siniestro.Attributes.Add(new Attribute("GastosNoCubiertos"));
                siniestro.Attributes.Add(new Attribute("IsComplemento"));
                siniestro.Attributes.Add(new Attribute("Contacto"));
                siniestro.Attributes.Add(new Attribute("BienAsegurado"));
                siniestro.Attributes.Add(new Attribute("MedicoTratante"));
                siniestro.Attributes.Add(new Attribute("Tratamiento"));
                siniestro.Attributes.Add(new Attribute("EstatusSiniestroBeneficios"));
                siniestro.Attributes.Add(new Attribute("Reproceso"));

                siniestro = SiteFunctions.GetEntity(siniestro, out errMsg);

                if(siniestro != null)
                {
                    XElement compleE = serializaSiniestroBeneficios(siniestro, xml);
                    xEComplementos.Add(compleE);
                }
            }

            return xEComplementos;
        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroDanhos para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroDanhos de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del SiniestroDanhos en formato XML.</returns>
        private static XElement serializaSiniestroDanhos(Entity siniestro, Entity siniestroDanhos, XElement xml)
        {
            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.Attributes.Add(new Attribute("Id"));
            bienAsegurado.Attributes.Add(new Attribute("Permiso"));
            bienAsegurado.Attributes.Add(new Attribute("Inciso"));
            bienAsegurado.Attributes.Add(new Attribute("Estatus"));
            bienAsegurado.Attributes.Add(new Attribute("Prima"));
            bienAsegurado.Attributes.Add(new Attribute("Identificador"));
            bienAsegurado.Attributes.Add(new Attribute("Ubicacion"));
            bienAsegurado.Attributes.Add(new Attribute("CentroDeBeneficio"));
            bienAsegurado.Attributes.Add(new Attribute("Propietario"));
            bienAsegurado.Attributes.Add(new Attribute("Poliza"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoAlta"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoBaja"));
            bienAsegurado.Attributes.Add(new Attribute("GrupoAsegurados"));
            bienAsegurado.Attributes.Add(new Attribute("BeneficiarioPreferente"));
            bienAsegurado.Attributes.Add(new Attribute("Empleado"));
            bienAsegurado.Attributes.Add(new Attribute("TarjetaBancaria"));
            bienAsegurado.useAuditable = true;
            bienAsegurado.Keys.Add(new Key("Id", siniestro.getAttrValueByName("BienAsegurado"), "int"));

            string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario, ErrMsg;

            PolizaToXML.getFieldRelatedToMov(bienAsegurado.getAttrValueByName("CentroDeBeneficio"), bienAsegurado.getAttrValueByName("Propietario")
                , bienAsegurado.getAttrValueByName("Ubicacion"), bienAsegurado.getAttrValueByName("AuditableUsuarioAdd"), bienAsegurado.getAttrValueByName("AuditableUsuarioUMod"),
                bienAsegurado.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);

            Entity Ajustador = new Entity("ContactoSimple");
            Ajustador.Attributes.Add(new Attribute("NombreCompleto"));
            Ajustador.Keys.Add(new Key("Id", siniestroDanhos.getAttrValueByName("Ajustador"), "int"));

            Ajustador = SiteFunctions.GetEntity(Ajustador, out ErrMsg);

            Entity tramiteAdmin = new Entity("TramiteAdmin");
            tramiteAdmin.Attributes.Add(new Attribute("Descripcion"));
            tramiteAdmin.Attributes.Add(new Attribute("FechaConclusion"));
            tramiteAdmin.Attributes.Add(new Attribute("FechaInicio"));
            tramiteAdmin.Keys.Add(new Key("Id", siniestroDanhos.getAttrValueByName("TramiteAdmin"), "int"));

            tramiteAdmin = SiteFunctions.GetEntity(tramiteAdmin, out ErrMsg);

            xml.Add(
                 PolizaToXML.serializaMovimiento(bienAsegurado, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario),
                 Ajustador != null ? new XElement("Ajustador", Ajustador.getAttrValueByName("NombreCompleto")) : new XElement("Ajustador"),
                 new XElement("Descripcion", siniestroDanhos.getAttrValueByName("Descripcion")),
                 new XElement("FechaSiniestro", siniestro.getAttrValueByName("FechaSiniestro", true)),
                 new XElement("TotalIndemnizado", siniestro.getAttrValueByName("TotalIndemnizado", true)),serializaPagosDanhos(siniestro.getAttrValueByName("Id")),
                 new XElement("TotalReclamado", siniestro.getAttrValueByName("TotalReclamado", true)),
                 tramiteAdmin != null ?
                     new XElement("TramiteAdmin",
                         new XElement("Descripcion", tramiteAdmin.getAttrValueByName("Descripcion")),
                         new XElement("FechaConclusion", tramiteAdmin.getAttrValueByName("FechaConclusion")),
                         new XElement("FechaInicio", tramiteAdmin.getAttrValueByName("FechaInicio"))
                         )
                 : new XElement("TramiteAdmin")
                );
            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroBeneficios para transformar sus pagos en formato XML.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroBeneficios de la cual se tomaran los pagos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de la lista de pagos en formato XML.</returns>
        private static XElement serializaPagosBeneficios(string siniestroId)
        {
            string errmsg = string.Empty;

            Entity Pagos = new Entity("PagoBeneficio");
            Pagos.Attributes.Add(new Attribute("Id"));
            Pagos.Attributes.Add(new Attribute("Concepto"));
            Pagos.Attributes.Add(new Attribute("TipoPago"));
            Pagos.Attributes.Add(new Attribute("Monto"));
            Pagos.Attributes.Add(new Attribute("EmisorPagoBeneficio"));
            Pagos.Attributes.Add(new Attribute("MontoRecuperado"));
            Pagos.Attributes.Add(new Attribute("Siniestro"));
            Pagos.Attributes.Add(new Attribute("Moneda"));
            Pagos.Keys.Add(new Key("Siniestro", siniestroId, "int"));

            List<Entity> result = SiteFunctions.GetValues(Pagos, out errmsg);

            XElement pagosEl = new XElement("Pagos");

            foreach (Entity pago in result)
            {
                XElement pagoXEl = serializaPagoBen(pago);
                pagosEl.Add(pagoXEl);
            }

            return pagosEl;

        }

        /// <summary>
        /// Permite tomar una instancia de SiniestroDanhos para transformar sus pagos en formato XML.
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroDanhos de la cual se tomaran los pagos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de la lista de pagos en formato XML.</returns>
        private static XElement serializaPagosDanhos(string siniestroId)
        {
            string errmsg = string.Empty;

            Entity Pagos = new Entity("PagoBeneficio");
            Pagos.Attributes.Add(new Attribute("Id"));
            Pagos.Attributes.Add(new Attribute("Concepto"));
            Pagos.Attributes.Add(new Attribute("TipoPago"));
            Pagos.Attributes.Add(new Attribute("Monto"));
            Pagos.Attributes.Add(new Attribute("EmisorPagoBeneficio"));
            Pagos.Attributes.Add(new Attribute("MontoRecuperado"));
            Pagos.Attributes.Add(new Attribute("Siniestro"));
            Pagos.Attributes.Add(new Attribute("Moneda"));
            Pagos.Keys.Add(new Key("Siniestro", siniestroId, "int"));

            List<Entity> result = SiteFunctions.GetValues(Pagos, out errmsg);

            XElement pagosEl = new XElement("Pagos");

            foreach (Entity pago in result)
            {
                XElement pagoXEl = serializaPagoBen(pago);
                pagosEl.Add(pagoXEl);
            }

            return pagosEl;
        }

        /// <summary>
        /// Permite tomar una instancia de PagoBeneficio para transformarlo en formato XML.
        /// </summary>
        /// <param name="pago">Instancia de PagoBeneficio de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del PagoBeneficio en formato XML.</returns>
        private static XElement serializaPagoBen(Entity pago)
        {
            return new XElement("Pago",
                         new XElement("Concepto", pago.getAttrValueByName("Concepto")),
                         new XElement("EmisorPagoBeneficio", pago.getAttrValueByName("EmisorPagoBeneficio")),
                         new XElement("Monto", Convert.ToDecimal(pago.getAttrValueByName("Monto")).ToString("C")),
                         new XElement("MontoRecuperado", Convert.ToDecimal(pago.getAttrValueByName("MontoRecuperado")).ToString("C")),
                         new XElement("TipoPago", pago.getAttrValueByName("TipoPago"))
                );
        }

        /// <summary>
        /// Calcula el total a pagar de un siniestro en base a:
        /// total reclamado, gastos no cubiertos, deducible y coaseguro
        /// </summary>
        /// <param name="siniestro">Instancia de SiniestroBeneficios de la cual se tomaran los datos.</param>
        /// <returns>Devuelve un double con el total del calculo realizado.</returns>
        private static double GetTotalPagar(Entity siniestro)
        {
            double TotalReclamado = siniestro.getAttrValueByName("TotalReclamado") != string.Empty ? Convert.ToDouble(siniestro.getAttrValueByName("TotalReclamado")) : 0;
            double GastosNoCubiertos = siniestro.getAttrValueByName("GastosNoCubiertos") != string.Empty ? Convert.ToDouble(siniestro.getAttrValueByName("GastosNoCubiertos")) : 0;
            double Deducible = siniestro.getAttrValueByName("Deducible") != string.Empty ? Convert.ToDouble(siniestro.getAttrValueByName("Deducible")) : 0;
            double Coaseguro = siniestro.getAttrValueByName("Coaseguro") != string.Empty ? Convert.ToDouble(siniestro.getAttrValueByName("Coaseguro")) : 0;

            return (TotalReclamado - GastosNoCubiertos - Deducible - Coaseguro);
        }

        private static string getEntityValue(Entity ent, string Attribute)
        {
            string retval = string.Empty;
            if (ent != null)
            {
                var Attr = ent.Attributes.Where(p => p.AttrName == Attribute);
                if(Attr.Count() > 0)
                {
                    if (Attr.First().AttrType == "datetime")
                    {
                        DateTime dval;
                        if(DateTime.TryParse(Attr.First().AttrValue,out dval))
                        {
                            retval = dval.ToShortDateString();
                        }
                    }
                    else
                    {
                        retval = Attr.First().AttrValue.ToString();
                    }
                }
            }
            return retval;
        }

    }    
}