﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class GeneraCartaEndosoStrategy
    {
        public string GeneraCarta(Entity o, Entity Poliza, Entity plantilla, Entity usuarioFirmado)
        {
            string impuesto = string.Empty, moneda = string.Empty, paisFiscal = string.Empty, edoFiscal = string.Empty, paisCobro = string.Empty, edoCobro = string.Empty,
                                aseguradora = string.Empty, CentroBeneficio = string.Empty, Supervisor = string.Empty, Director = string.Empty, Ejecutivo = string.Empty,
                                EjecutivoAseguradora = string.Empty, Ubicacion = string.Empty, polizaOrig = string.Empty;
            Entity Propietario = new Entity(), ramo = new Entity(), Titular = new Entity(), CoTitular = new Entity(), SubRamo = new Entity(), Slip = new Entity(),
                DirFiscal = new Entity(), DirCobro = new Entity(), Agente = new Entity(), Contratante = new Entity();

            GeneraCartaPolizaStrategy.getObjectToGenPolizaLetter(ref Poliza, ref impuesto, ref moneda, ref paisFiscal, ref edoFiscal, ref paisCobro, ref edoCobro, ref aseguradora,
                ref CentroBeneficio, ref Supervisor, ref Director, ref Ejecutivo, ref EjecutivoAseguradora, ref Ubicacion, ref polizaOrig, ref Propietario, ref ramo, ref Titular,
                ref CoTitular, ref SubRamo, ref Slip, ref DirFiscal, ref DirCobro, ref Agente, ref Contratante); 
            
            var xml = EndosoToXML.serializaEndoso(o);
            xml.Add(PolizaToXML.serializaPolizaGeneral(Poliza, impuesto, Propietario, ramo, Titular, CoTitular, SubRamo, Slip, moneda, DirFiscal, paisFiscal, edoFiscal, DirCobro, paisCobro, edoCobro, Agente, aseguradora, CentroBeneficio, Contratante, Supervisor, Director, Ejecutivo, EjecutivoAseguradora, Ubicacion, "", "", polizaOrig));

            xml.FirstNode.AddAfterSelf(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.getAttrValueByName("Nombre")),
                new XElement("Email", usuarioFirmado.getAttrValueByName("Email")),
                new XElement("Puesto", usuarioFirmado.getAttrValueByName("Puesto.Nombre")))
            );
            xml.Add(new XElement("FechaOperacion", DateTime.Now.ToString("dd/MM/yyyy")));
            var result = SiteFunctions.Transform(xml, plantilla);

            result = result.Replace("?Slip", o.getAttrValueByName("InstruccionesAseguradora"));

            return result;
        }
    }
}