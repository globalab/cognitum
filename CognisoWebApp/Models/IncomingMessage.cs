﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace CognisoWebApp.Models
{
    [DataContract(Name = "Entity", Namespace = "")]
    public class Entity
    {
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string EntityAlias { get; set; }
        [DataMember]
        public int PKId { get; set; }
        [DataMember]
        public int Action { get; set; } //ConvertLeadtoCustomer
        [DataMember]
        public List<Key> Keys { get; set; }
        [DataMember]
        public List<Attribute> Attributes { get; set; }
        [DataMember]
        public List<JoinEntity> ChildEntities { get; set; }
        [DataMember]
        public Auditable auditable { get; set; }
        [DataMember]
        public bool useAuditable { get; set; }
        [DataMember]
        public int logicalOperator { get; set; } //1 and 2 or
        [DataMember]
        public string WhereStmt { get; set; }

        public Entity()
        {

            Attributes = new List<Attribute>();
            Keys = new List<Key>();
            ChildEntities = new List<JoinEntity>();
            auditable = new Auditable();
            useAuditable = true;
            WhereStmt = string.Empty;
        }

        public Entity(string _EntityName)
        {

            EntityName = _EntityName;
            EntityAlias = _EntityName;
            Attributes = new List<Attribute>();
            Keys = new List<Key>();
            ChildEntities = new List<JoinEntity>();
            auditable = new Auditable();
            useAuditable = true;
            WhereStmt = string.Empty;
        }

        public Entity(string _EntityName,string _EntityAlias)
        {

            EntityName = _EntityName;
            EntityAlias = _EntityAlias;
            Attributes = new List<Attribute>();
            Keys = new List<Key>();
            ChildEntities = new List<JoinEntity>();
            auditable = new Auditable();
            useAuditable = true;
            WhereStmt = string.Empty;
        }

        public static Entity construct(Entity _incoMsg)
        {
            Entity retVal = _incoMsg;

            return retVal;
        }

        public string getAttrValueByName(string _attrName,bool format = false)
        {
            Attribute attr = this.Attributes.Find(x => x.AttrName.ToLower() == _attrName.ToLower());
            if (attr != null)
            {
                if (attr.AttrType.ToLower().Contains("datetime") && attr.AttrValue != string.Empty && attr.AttrName != "FechaAdd" && attr.AttrName != "FechaUMod") 
                {
                    return attr.AttrValue.Substring(0, 10);
                }
                if ((attr.AttrType.ToLower().Contains("decimal") || attr.AttrType.ToLower().Contains("double")) && attr.AttrValue != string.Empty && format)
                {
                    decimal attval = Convert.ToDecimal(attr.AttrValue);
                    return attval.ToString("C");
                }

                return attr.AttrValue;
            }
            else
            {
                return string.Empty;
            }
        }
        public string getKeyValueByName(string _attrName)
        {
            Attribute attr = this.Keys.Find(x => x.AttrName == _attrName);
            if (attr != null)
            {
                return attr.AttrValue;
            }
            else
            {
                return string.Empty;
            }
        }
        public bool hasChildEntities()
        {
            return this.ChildEntities.Count > 0;
        }
    }
    [DataContract(Name = "Auditable", Namespace = "")]
    public class Auditable
    {
        [DataMember]
        public int entityId { get; set; }
        [DataMember]
        public int userId { get; set; }
        [DataMember]
        public DateTime opDate { get; set; }
        [DataMember]
        public int recordStatus { get; set; }

        public bool hasValue;

        public Auditable(int _userId, DateTime _opDate, int _recordStatus)
        {
            userId = _userId;
            opDate = _opDate;
            recordStatus = _recordStatus;
            hasValue = true;
        }
        public Auditable()
        {
            hasValue = false;
        }
    }

    [DataContract(Name = "Attribute", Namespace = "")]
    public class Attribute
    {
        [DataMember]
        public string AttrName { get; set; }
        [DataMember]
        public string AttrValue { get; set; }
        [DataMember]
        public string AttrType { get; set; }

        public Attribute(string _AttrName)
        {
            AttrName = _AttrName;
        }
        public Attribute(string _AttrName, string _AttrValue)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
        }

        public Attribute()
        {
        }

        public Attribute(string _AttrName, string _AttrValue, string _AttrType)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
        }
    }

    [DataContract(Name = "Key", Namespace = "")]
    public class Key : Attribute
    {
        [DataMember]
        public int LogicalOperator { get; set; }
        [DataMember]
        public int Group { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public whereOperator WhereOperator { get; set; }

        public Key(string _AttrName)
        {
            AttrName = _AttrName;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }
        public Key(string _AttrName, string _AttrValue)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key()
        {
            LogicalOperator = 1;
            Group = 0;
            WhereOperator = whereOperator.Like;
            EntityName = string.Empty;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, whereOperator _WhereOperator)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = 1;
            Group = 0;
            EntityName = string.Empty;
            WhereOperator = _WhereOperator;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, whereOperator _WhereOperator,int _Group)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = 1;
            Group = _Group;
            EntityName = string.Empty;
            WhereOperator = _WhereOperator;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor, int _Group)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = _Group;
            EntityName = string.Empty;
            WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = 0;
            EntityName = string.Empty; WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor, int _Group, string _EntityName)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = _Group;
            EntityName = _EntityName;
            WhereOperator = whereOperator.Like;
        }

        public Key(string _AttrName, string _AttrValue, string _AttrType, int _LogicalOpertor, int _Group, whereOperator _whereoperator)
        {
            AttrName = _AttrName;
            AttrValue = _AttrValue;
            AttrType = _AttrType;
            LogicalOperator = _LogicalOpertor;
            Group = _Group;
            EntityName = string.Empty;
            WhereOperator = _whereoperator;
        }

    }
    [DataContract(Name = "JoinEntity", Namespace = "")]
    public class JoinEntity
    {
        [DataMember]
        public Entity ChildEntity { get; set; }
        [DataMember]
        public Attribute JoinKey { get; set; }
        [DataMember]
        public List<selectJoinEntity> selectJoinList { get; set; }
        [DataMember]
        public JoinType JoinType { get; set; }

        public JoinEntity()
        {
            this.selectJoinList = new List<selectJoinEntity>();
            this.JoinType = JoinType.Inner;
        }
    }

    public enum JoinType
    {
        Inner = 1,
        Left = 2,
        Right = 3

    }

    public enum whereOperator
    {
        Equal = 1,
        NotEqual = 2,
        Greater = 3,
        GreaterEqual = 4,
        Lower = 5,
        LowerEqual = 6,
        IsNull = 7,
        IsNotNull = 8,
        Like = 9
    }

    public class selectJoinEntity
    {
        [DataMember]
        public int logicOperator { get; set; }
        [DataMember]
        public Attribute mainAttr { get; set; }
        [DataMember]
        public Attribute childAttr { get; set; }

        public selectJoinEntity(string _mainAttr, int _logicalOperator, string _childAttr)
        {
            this.logicOperator = _logicalOperator;
            this.mainAttr = new Attribute(_mainAttr);
            this.childAttr = new Attribute(_childAttr);

        }

        public selectJoinEntity()
        {
        }


    }
}
