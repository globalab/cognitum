﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CognisoWebApp.Models
{
    [DataContract(Name = "AttachmentSP", Namespace = "")]
    public class AttachmentSP
    {
            [DataMember]
            public string EntityName { get; set; }
            [DataMember]
            public string FileName { get; set; }
            [DataMember]
            public string id { get; set; }
            [DataMember]
            public string parentId { get; set; }
            [DataMember]
            public byte[] FileContent { get; set; }
            [DataMember]
            public string spUrlFile { get; set; }
            [DataMember]
            public string folderToSave { get; set; }
            [DataMember]
            public bool FoundIt { get; set; }
    }

    [DataContract(Name = "AttachmentList", Namespace = "")]
    public class AttachmentList
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public List<AttachmentSP> ListFile { get; set; }
    }
}