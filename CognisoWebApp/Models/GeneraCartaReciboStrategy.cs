﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class GeneraCartaReciboStrategy
    {
        /// <summary>
        /// Permite generar una carta tomando una plantilla.
        /// </summary>
        /// <param name="o">Instancia de donde se obtendra la informacion con
        /// la que se llenara la carta.</param>
        /// <param name="plantilla">Plantilla que se ocupara.</param>
        /// <param name="usuarioFirmado">Usuario que aparecera como informacion
        /// adicional a la informacion de la carta por motivos de seguridad.</param>
        /// <returns>Devuelve cadena con la informacion de la carta.</returns>
        public string GeneraCarta(Entity o, Entity Poliza,Entity Endoso, Entity plantilla, Entity usuarioFirmado)
        {
            string errMsg = string.Empty;
            var xml = ReciboToXML.serializaRecibo(o,Poliza,Endoso);
            #region Puesto
            Entity Puesto = new Entity("Puesto");
            Puesto.Attributes.Add(new Attribute("Nombre"));
            Puesto.Keys.Add(new Key("Id", usuarioFirmado.getAttrValueByName("Puesto"), "int"));

            Puesto = SiteFunctions.GetEntity(Puesto, out errMsg);

            if (usuarioFirmado == null)
            {
                usuarioFirmado = new Entity("Usuario");
                usuarioFirmado.Attributes.Add(new Attribute("Nombre", ""));
                usuarioFirmado.Attributes.Add(new Attribute("Email", ""));
                usuarioFirmado.Attributes.Add(new Attribute("Nombre", ""));
            }
            #endregion
            xml.FirstNode.AddAfterSelf(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.getAttrValueByName("Nombre")),
                new XElement("Email", usuarioFirmado.getAttrValueByName("Email")),
                new XElement("Puesto", Puesto.getAttrValueByName("Nombre")))
            );
            xml.Add(new XElement("FechaOperacion", DateTime.Now.ToString("dd/MM/yyyy")));
            ///***XSL Transformation
            var result = SiteFunctions.Transform(xml, plantilla);
            return result;
            
        }
       
    }
}