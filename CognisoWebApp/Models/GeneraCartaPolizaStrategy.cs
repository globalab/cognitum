﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class GeneraCartaPolizaStrategy
    {
        /// <summary>
        /// Permite generar una carta tomando una plantilla.
        /// </summary>
        /// <param name="o">Instancia de donde se obtendra la informacion con
        /// la que se llenara la carta.</param>
        /// <param name="plantilla">Plantilla que se ocupara.</param>
        /// <param name="usuarioFirmado">Usuario que aparecera como informacion
        /// adicional a la informacion de la carta por motivos de seguridad.</param>
        /// <returns>Devuelve cadena con la informacion de la carta.</returns>
        public string GeneraCarta(Entity o, Entity plantilla, Entity usuarioFirmado)
        {
            Entity bienAsegurado = new Entity("BienAsegurado");
            bienAsegurado.Attributes.Add(new Attribute("Id"));
            bienAsegurado.Attributes.Add(new Attribute("Permiso"));
            bienAsegurado.Attributes.Add(new Attribute("Inciso"));
            bienAsegurado.Attributes.Add(new Attribute("Estatus"));
            bienAsegurado.Attributes.Add(new Attribute("Prima"));
            bienAsegurado.Attributes.Add(new Attribute("Identificador"));
            bienAsegurado.Attributes.Add(new Attribute("Ubicacion"));
            bienAsegurado.Attributes.Add(new Attribute("CentroDeBeneficio"));
            bienAsegurado.Attributes.Add(new Attribute("Propietario"));
            bienAsegurado.Attributes.Add(new Attribute("Poliza"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoAlta"));
            bienAsegurado.Attributes.Add(new Attribute("EndosoBaja"));
            bienAsegurado.Attributes.Add(new Attribute("Id"));
            bienAsegurado.Attributes.Add(new Attribute("GrupoAsegurados"));
            bienAsegurado.Attributes.Add(new Attribute("BeneficiarioPreferente"));
            bienAsegurado.Attributes.Add(new Attribute("Empleado"));
            bienAsegurado.Attributes.Add(new Attribute("TarjetaBancaria"));
            bienAsegurado.Keys.Add(new Key("Poliza", o.getAttrValueByName("Id"), "int"));

            string impuesto = string.Empty, moneda = string.Empty, paisFiscal = string.Empty, edoFiscal = string.Empty, paisCobro = string.Empty, edoCobro = string.Empty,
                    aseguradora = string.Empty, CentroBeneficio = string.Empty, Supervisor = string.Empty, Director = string.Empty, Ejecutivo = string.Empty,
                    EjecutivoAseguradora = string.Empty, Ubicacion = string.Empty, polizaOrig = string.Empty;
                Entity Propietario = new Entity(), ramo = new Entity(), Titular = new Entity(), CoTitular = new Entity(), SubRamo = new Entity(), Slip = new Entity(), 
                    DirFiscal = new Entity(), DirCobro = new Entity(), Agente = new Entity(), Contratante = new Entity();

                GeneraCartaPolizaStrategy.getObjectToGenPolizaLetter(ref o, ref impuesto, ref moneda, ref paisFiscal, ref edoFiscal, ref paisCobro, ref edoCobro, ref aseguradora,
                    ref CentroBeneficio, ref Supervisor, ref Director, ref Ejecutivo, ref EjecutivoAseguradora, ref Ubicacion, ref polizaOrig, ref Propietario, ref ramo, ref Titular,
                    ref CoTitular, ref SubRamo, ref Slip, ref DirFiscal, ref DirCobro, ref Agente, ref Contratante);

            var xml = PolizaToXML.serializaPolizaGeneral(o, impuesto, Propietario, ramo, Titular, CoTitular, SubRamo
                    , Slip, moneda, DirFiscal, paisFiscal, edoFiscal, DirCobro, paisCobro, edoCobro, Agente
                    , aseguradora, CentroBeneficio, Contratante, Supervisor, Director, Ejecutivo
                    , EjecutivoAseguradora, Ubicacion, "", "", polizaOrig);

            xml.FirstNode.AddAfterSelf(EndosoToXML.serializaEndosos(o));

            Entity siniestros = new Entity("Siniestro");
            siniestros.Attributes.Add(new Attribute("Id"));
            siniestros.Attributes.Add(new Attribute("Numero"));
            siniestros.Attributes.Add(new Attribute("FechaSiniestro"));
            siniestros.Attributes.Add(new Attribute("FechaFiniquito"));
            siniestros.Attributes.Add(new Attribute("FechaReporte"));
            siniestros.Attributes.Add(new Attribute("FechaRecepcionCia"));
            siniestros.Attributes.Add(new Attribute("FechaDoc"));
            siniestros.Attributes.Add(new Attribute("Observaciones"));
            siniestros.Attributes.Add(new Attribute("EstatusPagoSiniestro"));
            siniestros.Attributes.Add(new Attribute("EstatusSiniestro"));
            siniestros.Attributes.Add(new Attribute("BienAsegurado"));
            siniestros.Attributes.Add(new Attribute("Poliza"));
            
            siniestros.Keys.Add(new Key("Poliza", o.getAttrValueByName("Id"), "int"));

            string errMsg = string.Empty;

            List<Entity> listSiniestros = SiteFunctions.GetValues(siniestros, out errMsg);

            if(listSiniestros.Count>0)
            {
                xml.FirstNode.AddAfterSelf(SiniestroToXML.serializaSiniestros(listSiniestros));
            }

            Entity polizaType = new Entity("PolizaIndividual");
            polizaType.Attributes.Add(new Attribute("Id"));
            polizaType.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

            string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

            List<Entity> results = SiteFunctions.GetValues(polizaType, out errMsg);
            ///***Individuales
            if (results.Count > 0)
            {
                Entity polInd = new Entity("PolizaGmmIndividual");
                polInd.Attributes.Add(new Attribute("Id"));
                polInd.Attributes.Add(new Attribute("EsquemaPrimaMinima"));
                polInd.Attributes.Add(new Attribute("SumaAsegurada"));
                polInd.Attributes.Add(new Attribute("SumaAseguradaSMGM"));
                polInd.Attributes.Add(new Attribute("CoaseguroPct"));
                polInd.Attributes.Add(new Attribute("DeduciblePct"));
                polInd.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                polInd = SiteFunctions.GetEntity(polInd, out errMsg);
                //*****GMM 
                if (polInd != null)
                {
                    PolizaToXML.serializaPolizaGmmIndividual(polInd, xml);
                }
                else
                {
                    polInd = new Entity("PolizaVidaIndividual");
                    polInd.Attributes.Add(new Attribute("Id"));
                    polInd.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                    polInd = SiteFunctions.GetEntity(polInd, out errMsg);
                    //****Vida
                    if (polInd != null)
                    {
                        bienAsegurado = SiteFunctions.GetEntity(bienAsegurado, out errMsg);

                        if (bienAsegurado != null)
                        {
                            PolizaToXML.getFieldRelatedToMov(bienAsegurado.getAttrValueByName("CentroDeBeneficio"), bienAsegurado.getAttrValueByName("Propietario")
                                , bienAsegurado.getAttrValueByName("Ubicacion"), bienAsegurado.getAttrValueByName("AuditableUsuarioAdd"), bienAsegurado.getAttrValueByName("AuditableUsuarioUMod")
                                , bienAsegurado.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);

                            xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimiento(bienAsegurado, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario));
                        }
                    }
                    else
                    {
                        polInd = new Entity("PolizaAutoIndividual");
                        polInd.Attributes.Add(new Attribute("Id"));
                        polInd.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                        polInd = SiteFunctions.GetEntity(polInd, out errMsg);
                        //****Autos
                        if (polInd != null)
                        {
                            bienAsegurado = SiteFunctions.GetEntity(bienAsegurado, out errMsg);
                            if (bienAsegurado != null)
                            {
                                PolizaToXML.getFieldRelatedToMov(bienAsegurado.getAttrValueByName("CentroDeBeneficio"), bienAsegurado.getAttrValueByName("Propietario")
                                    , bienAsegurado.getAttrValueByName("Ubicacion"), bienAsegurado.getAttrValueByName("AuditableUsuarioAdd"), bienAsegurado.getAttrValueByName("AuditableUsuarioUMod")
                                    , bienAsegurado.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);

                                xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimiento(bienAsegurado, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario));
                            }
                        }
                    }
                }
            }

            ///***Pool
            else
            { 
                polizaType = new Entity("PolizaPool");
                polizaType.Attributes.Add(new Attribute("Id"));
                polizaType.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                results = SiteFunctions.GetValues(polizaType, out errMsg);
                if (results.Count > 0)
                {
                    List<Entity> listMov = SiteFunctions.GetValues(bienAsegurado, out errMsg);
                    if (listMov.Count > 0)
                    {
                        xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimientos(o, listMov));
                    }

                    Entity polizaPool = new Entity("PolizaGmmPoblacion");
                    polizaPool.Attributes.Add(new Attribute("Id"));
                    polizaPool.Attributes.Add(new Attribute("EsquemaPrimaMinima"));
                    polizaPool.Attributes.Add(new Attribute("SumaAsegurada"));
                    polizaPool.Attributes.Add(new Attribute("SumaAseguradaSMGM"));
                    polizaPool.Attributes.Add(new Attribute("CoaseguroPct"));
                    polizaPool.Attributes.Add(new Attribute("DeduciblePct"));
                    polizaPool.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                    polizaPool = SiteFunctions.GetEntity(polizaPool, out errMsg);
                    //*****GMM
                    if (polizaPool != null)
                    {
                        PolizaToXML.serializaPolizaGmmPoblacion(polizaPool, xml);
                    }
                    else
                    {
                        polizaPool = new Entity("PolizaVidaPoblacion");
                        polizaPool.Attributes.Add(new Attribute("Id"));
                        polizaPool.Attributes.Add(new Attribute("SumaAsegurada"));
                        polizaPool.Attributes.Add(new Attribute("SumaAseguradaSMGM"));
                        polizaPool.Attributes.Add(new Attribute("SumaAseguradaTopada"));
                        polizaPool.Attributes.Add(new Attribute("SumaAseguradaTopadaSMGM"));
                        polizaPool.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));
                        polizaPool = SiteFunctions.GetEntity(polizaPool, out errMsg);
                        //*****VIDA
                        if (polizaPool != null)
                        {
                            xml = PolizaToXML.serializaPolizaVidaPoblacion(polizaPool, xml);
                        }
                    }
                }
                else
                {
                    throw new Exception("El objeto " + o.GetType() + " no es asignable para nigun tipo.");
                }
            }

            #region Puesto
            Entity Puesto = new Entity("Puesto");
            Puesto.Attributes.Add(new Attribute("Nombre"));
            Puesto.Keys.Add(new Key("Id", usuarioFirmado.getAttrValueByName("Puesto"), "int"));

            Puesto = SiteFunctions.GetEntity(Puesto, out errMsg);

            #endregion

            xml.FirstNode.AddAfterSelf(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.getAttrValueByName("Nombre")),
                new XElement("Email", usuarioFirmado.getAttrValueByName("Email")),
                new XElement("Puesto", Puesto.getAttrValueByName("Nombre")))
            );

            xml.Add(new XElement("FechaOperacion", DateTime.Now.ToString("dd/MM/yyyy")));

            //if (Logger.IsDebugEnabled)
            //{
            //    Logger.DebugFormat("XML generado: {0}", xml);
            //}

            ///***XSL Transformation
            var result = SiteFunctions.Transform(xml, plantilla);
            Entity Ramo = new Entity("Ramo");
            Ramo.Attributes.Add(new Attribute("DivisionOperativa"));
            Ramo.Keys.Add(new Key("Id", o.getAttrValueByName("Ramo"), "int"));

            Ramo = SiteFunctions.GetEntity(Ramo, out errMsg);
            if (int.Parse(Ramo.getAttrValueByName("DivisionOperativa")) == (int)DivisionOperativaEnum.Autos)
            {
                Entity cobertura = new Entity("Coberturas");
                cobertura.Attributes.Add(new Attribute("Descripcion"));
                cobertura.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                cobertura = SiteFunctions.GetEntity(cobertura, out errMsg);
                if (cobertura != null)
                {
                    result = result.Replace("?Slip",
                        string.Format(@"
                            ?Slip
                        </td>
                      </tr>
                    </table><table class='slip'>
                      <tr>
                        <td>
                          Coberturas:
                        </td>
                      </tr>
                      <tr>
                        <td>
                          {0}", cobertura.getAttrValueByName("Descripcion")));
                }
               
                Entity Condiciones = new Entity("CondicionesEspeciales");
                Condiciones.Attributes.Add(new Attribute("Descripcion"));
                Condiciones.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

                Condiciones = SiteFunctions.GetEntity(Condiciones, out errMsg);
                if (Condiciones != null)
                {
                    result = result.Replace("?Slip",
                        string.Format(@"
                                ?Slip
                            </td>
                          </tr>
                        </table><table class='slip'>
                          <tr>
                            <td>
                              Condiciones Especiales:
                            </td>
                          </tr>
                          <tr>
                            <td>
                              {0}", Condiciones.getAttrValueByName("Descripcion")));
                }
            }

            Slip = new Entity("Slip");
            Slip.Attributes.Add(new Attribute("Descripcion"));
            Slip.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));

            Slip = SiteFunctions.GetEntity(Slip, out errMsg);

            result = result.Replace("?Slip", Slip.getAttrValueByName("Descripcion"));

            //return WebUtility.HtmlEncode(result);
            return result;
        }

        public static void getObjectToGenPolizaLetter(ref Entity poliza,ref string impuesto,ref string moneda,ref string paisFiscal,ref string edoFiscal,ref string paisCobro,
            ref string edoCobro, ref string aseguradora,ref string CentroBeneficio,ref string Supervisor,ref string Director,ref string Ejecutivo,ref string EjecutivoAseguradora,
            ref string Ubicacion,ref string polizaOrig, ref Entity Propietario,ref Entity ramo,ref Entity Titular,ref Entity CoTitular,ref Entity SubRamo,ref Entity Slip,
            ref Entity DirFiscal,ref Entity DirCobro,ref Entity Agente,ref Entity Contratante)
        {
            string errMsg = string.Empty;
            #region busca impuesto
                Entity entitySearch = new Entity("Impuesto");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Impuesto"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                impuesto = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region tramite
                entitySearch = new Entity("Tramite");
                entitySearch.Attributes.Add(new Attribute("Propietario"));
                entitySearch.Attributes.Add(new Attribute("CentroDeBeneficio"));
                entitySearch.Attributes.Add(new Attribute("Ubicacion"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Id"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                
                string propietario = entitySearch.getAttrValueByName("Propietario");
                string centroBenStr = entitySearch.getAttrValueByName("CentroDeBeneficio");
                string ubicacionStr = entitySearch.getAttrValueByName("Ubicacion");

                entitySearch = new Entity("CentroDeBeneficio");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", centroBenStr, "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                CentroBeneficio = entitySearch.getAttrValueByName("Nombre");

                entitySearch = new Entity("Ubicacion");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", ubicacionStr, "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                Ubicacion = entitySearch.getAttrValueByName("Nombre");
                
                #endregion
                #region propietairo
                Propietario = new Entity("Usuario");
                Propietario.Attributes.Add(new Attribute("Id"));
                Propietario.Attributes.Add(new Attribute("Nombre"));
                Propietario.Attributes.Add(new Attribute("Email"));
                Propietario.Attributes.Add(new Attribute("UserName"));
                Propietario.Attributes.Add(new Attribute("Ubicacion"));
                Propietario.Attributes.Add(new Attribute("CentroDeBeneficio"));
                Propietario.Attributes.Add(new Attribute("Propietario"));
                Propietario.Attributes.Add(new Attribute("Puesto"));
                Propietario.Attributes.Add(new Attribute("Perfil"));
                Propietario.Attributes.Add(new Attribute("FechaIngreso"));
                Propietario.Attributes.Add(new Attribute("Telefono"));
                Propietario.Attributes.Add(new Attribute("NumeroEmpleado"));
                Propietario.Attributes.Add(new Attribute("Observaciones"));
                Propietario.Keys.Add(new Key("Id", propietario, "int"));

                Propietario = SiteFunctions.GetEntity(Propietario, out errMsg);
                #endregion
                #region Ramo
                ramo = new Entity("Ramo");
                ramo.Attributes.Add(new Attribute("Id"));
                ramo.Attributes.Add(new Attribute("DivisionOperativa"));
                ramo.Attributes.Add(new Attribute("Clave"));
                ramo.Attributes.Add(new Attribute("Nombre"));
                ramo.Attributes.Add(new Attribute("Xtype"));
                ramo.Attributes.Add(new Attribute("IdSap"));
                ramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ramo"), "int"));

                ramo = SiteFunctions.GetEntity(ramo, out errMsg);
                #endregion
                #region Titular
                Titular = new Entity("ContactoSimple");
                Titular.Attributes.Add(new Attribute("Id"));
                Titular.Attributes.Add(new Attribute("Email"));
                Titular.Attributes.Add(new Attribute("NombreCompleto"));
                Titular.Attributes.Add(new Attribute("Cliente"));
                Titular.Attributes.Add(new Attribute("BienAsegurado"));
                Titular.Keys.Add(new Key("Id", poliza.getAttrValueByName("Titular"), "int"));

                Titular = SiteFunctions.GetEntity(Titular, out errMsg);
                #endregion
                #region CoTitular
                CoTitular = new Entity("ContactoSimple");
                CoTitular.Attributes.Add(new Attribute("Id"));
                CoTitular.Attributes.Add(new Attribute("Email"));
                CoTitular.Attributes.Add(new Attribute("NombreCompleto"));
                CoTitular.Attributes.Add(new Attribute("Cliente"));
                CoTitular.Attributes.Add(new Attribute("BienAsegurado"));
                CoTitular.Keys.Add(new Key("Id", poliza.getAttrValueByName("CoTitular"), "int"));

                CoTitular = SiteFunctions.GetEntity(CoTitular, out errMsg);
                #endregion
                #region SubRamo
                SubRamo = new Entity("SubRamo");
                SubRamo.Attributes.Add(new Attribute("Id"));
                SubRamo.Attributes.Add(new Attribute("Clave"));
                SubRamo.Attributes.Add(new Attribute("Nombre"));
                SubRamo.Attributes.Add(new Attribute("Flotilla"));
                SubRamo.Attributes.Add(new Attribute("Ramo"));
                SubRamo.Keys.Add(new Key("Id", poliza.getAttrValueByName("SubRamo"), "int"));

                SubRamo = SiteFunctions.GetEntity(SubRamo, out errMsg);
                #endregion
                #region Slip
                Slip = new Entity("Slip");
                Slip.Attributes.Add(new Attribute("Id"));
                Slip.Attributes.Add(new Attribute("Descripcion"));
                Slip.Keys.Add(new Key("Id", poliza.getAttrValueByName("Id"), "int"));

                Slip = SiteFunctions.GetEntity(Slip, out errMsg);
                #endregion
                #region busca Moneda
                entitySearch = new Entity("Moneda");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Moneda"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                if (entitySearch != null)
                {
                    moneda = entitySearch.getAttrValueByName("Nombre");
                }
                #endregion
                #region DirFiscal
                DirFiscal = new Entity("Direccion");
                DirFiscal.Attributes.Add(new Attribute("Id"));
                DirFiscal.Attributes.Add(new Attribute("Tipo"));
                DirFiscal.Attributes.Add(new Attribute("Calle"));
                DirFiscal.Attributes.Add(new Attribute("Colonia"));
                DirFiscal.Attributes.Add(new Attribute("Cp"));
                DirFiscal.Attributes.Add(new Attribute("Delegacion"));
                DirFiscal.Attributes.Add(new Attribute("Nombre"));
                DirFiscal.Attributes.Add(new Attribute("NumExterior"));
                DirFiscal.Attributes.Add(new Attribute("NumInterior"));
                DirFiscal.Attributes.Add(new Attribute("Cliente"));
                DirFiscal.Attributes.Add(new Attribute("Estado"));
                DirFiscal.Attributes.Add(new Attribute("Pais"));
                DirFiscal.Keys.Add(new Key("Id", poliza.getAttrValueByName("DireccionFiscal"), "int"));

                DirFiscal = SiteFunctions.GetEntity(DirFiscal, out errMsg);

                paisFiscal = string.Empty;
                edoFiscal = string.Empty;

                if (DirFiscal != null)
                {
                    entitySearch = new Entity("Estado");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirFiscal.getAttrValueByName("Estado")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                    if (entitySearch != null)
                    {
                        edoFiscal = entitySearch.getAttrValueByName("Nombre");
                    }
                    entitySearch = new Entity("Pais");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirFiscal.getAttrValueByName("Pais")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                    if (entitySearch != null)
                    {
                        paisFiscal = entitySearch.getAttrValueByName("Nombre");
                    }
                }
                
                #endregion
                #region DirCobro
                DirCobro = new Entity("Direccion");
                DirCobro.Attributes.Add(new Attribute("Id"));
                DirCobro.Attributes.Add(new Attribute("Tipo"));
                DirCobro.Attributes.Add(new Attribute("Calle"));
                DirCobro.Attributes.Add(new Attribute("Colonia"));
                DirCobro.Attributes.Add(new Attribute("Cp"));
                DirCobro.Attributes.Add(new Attribute("Delegacion"));
                DirCobro.Attributes.Add(new Attribute("Nombre"));
                DirCobro.Attributes.Add(new Attribute("NumExterior"));
                DirCobro.Attributes.Add(new Attribute("NumInterior"));
                DirCobro.Attributes.Add(new Attribute("Cliente"));
                DirCobro.Attributes.Add(new Attribute("Estado"));
                DirCobro.Attributes.Add(new Attribute("Pais"));
                DirCobro.Keys.Add(new Key("Id", poliza.getAttrValueByName("DireccionCobro"), "int"));

                DirCobro = SiteFunctions.GetEntity(DirCobro, out errMsg);


                paisCobro = string.Empty;
                edoCobro = string.Empty;

                if (DirCobro != null)
                {
                    entitySearch = new Entity("Estado");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirCobro.getAttrValueByName("Estado")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                    if (entitySearch != null)
                    {
                        edoFiscal = entitySearch.getAttrValueByName("Nombre");
                    }
                    entitySearch = new Entity("Pais");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirCobro.getAttrValueByName("Pais")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);
                    if (entitySearch != null)
                    {
                        paisFiscal = entitySearch.getAttrValueByName("Nombre");
                    }
                }

                #endregion
                #region Agente
                Agente = new Entity("Agente");
                Agente.Attributes.Add(new Attribute("Id"));
                Agente.Attributes.Add(new Attribute("Clave"));
                Agente.Attributes.Add(new Attribute("Nombre"));
                Agente.Attributes.Add(new Attribute("Ramo"));
                Agente.Keys.Add(new Key("Id", poliza.getAttrValueByName("Agente"), "int"));

                Agente = SiteFunctions.GetEntity(Agente, out errMsg);
                #endregion
                #region busca Aseguradora
                entitySearch = new Entity("PersonaMoral");
                entitySearch.Attributes.Add(new Attribute("RazonSocial"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Aseguradora"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                aseguradora = entitySearch.getAttrValueByName("RazonSocial");
                #endregion
                #region Contratante
                Contratante = new Entity("Cliente");
                Contratante.Attributes.Add(new Attribute("Id"));
                Contratante.Attributes.Add(new Attribute("Tipo"));
                Contratante.Attributes.Add(new Attribute("NombreCompleto"));
                Contratante.Attributes.Add(new Attribute("RFC"));
                Contratante.Attributes.Add(new Attribute("Giro"));
                Contratante.Attributes.Add(new Attribute("Grupo"));
                Contratante.Attributes.Add(new Attribute("IdSap"));
                Contratante.Attributes.Add(new Attribute("Registrante"));
                Contratante.Attributes.Add(new Attribute("EstatusCliente"));
                Contratante.Attributes.Add(new Attribute("CoRegistrante"));
                Contratante.Keys.Add(new Key("Id", poliza.getAttrValueByName("Contratante"), "int"));

                Contratante = SiteFunctions.GetEntity(Contratante, out errMsg);
                #endregion
                #region busca Supervisor
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Supervisor"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                Supervisor = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region busca Director
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Director"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                Director = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region busca Ejecutivo
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ejecutivo"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                Ejecutivo = entitySearch.getAttrValueByName("Nombre");
                #endregion
                 #region busca EjecutivoAseguradora
                entitySearch = new Entity("ContactoPersonaMoral");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("EjecutivoAseguradora"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                EjecutivoAseguradora = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region polizaOriginal
                polizaOrig = string.Empty;
                if (poliza.getAttrValueByName("PolizaOriginal") != string.Empty)
                {
                    entitySearch = new Entity("Tramite");
                    entitySearch.Attributes.Add(new Attribute("Folio"));
                    entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("PolizaOriginal"), "int"));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    polizaOrig = entitySearch.getAttrValueByName("Folio");
                }
                #endregion
        }
    }
}