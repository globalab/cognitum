﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CognisoWA.Controllers
{
    public enum DivisionOperativaEnum
    {

        ///<summary>
        ///</summary>
        Autos = 0,

        ///<summary>
        ///</summary>
        GMM = 1,

        ///<summary>
        ///</summary>
        Vida = 2,

        ///<summary>
        ///</summary>
        Danos = 3,

        ///<summary>
        ///</summary>
        Fianzas = 4,

        ///<summary>
        ///</summary>
        Gobierno = 5

    }

    public enum EstatusConciliacionEnum
    {
        ///<summary>
        ///</summary>
        Pendiente = 0,
        ///<summary>
        ///</summary>
        Aplicada = 1,
        ///<summary>
        ///</summary>
        Reabierta = 2,
        ///<summary>
        ///</summary>
        Cancelada = 3,
        ///<summary>
        ///</summary>
        Eliminada = 4,
        ///<summary>
        ///</summary>
        Facturada = 5
    }

    

    public enum EstatusReciboEnum
    {
        ///<summary>
        ///</summary>
        Pendiente = 0,
        ///<summary>
        ///</summary>
        Cancelado = 1,
        ///<summary>
        ///</summary>
        PorAplicar = 2,
        ///<summary>
        ///</summary>
        Aplicado = 3,
        ///<summary>
        ///</summary>
        PorConciliar = 4,
        ///<summary>
        ///</summary>
        Conciliado = 5,
        ///<summary>
        ///</summary>
        PorPagarComision = 6,
        ///<summary>
        ///</summary>
        ComisionPagada = 7,
        ///<summary>
        ///</summary>
        Eliminado = 8
    }

    public enum StatusMesaControl
    {
        /// <summary>
        /// </summary>
        EnTramite = 0,
        /// <summary>
        /// </summary>
        EnRevision = 1,
        /// <summary>
        /// </summary>
        Correcto = 2,
        /// <summary>
        /// </summary>
        ConErrores = 3,
        /// <summary>
        /// </summary>
        Rechazado = 4,
        /// <summary>
        /// </summary>
        AceptadoConErrores = 5,
        /// <summary>
        /// </summary>
        Liberado = 6,
        /// <summary>
        /// </summary>
        SolicitaCorreccion = 7,
        /// <summary>
        /// </summary>
        CorreccionEnTramite = 8,
        /// <summary>
        /// </summary>
        CorreccionEnRevision = 9,
        /// <summary>
        /// </summary>
        CorreccionCorrecta = 10,
        /// <summary>
        /// </summary>
        CorreccionConErrores = 11,
        /// <summary>
        /// </summary>
        CorreccionRechazada = 12,
        /// <summary>
        /// </summary>
        CorreccionLiberado = 13,
        /// <summary>
        /// </summary>
        NoAsignado = 14
    }

    public enum FormaPagoEnum
    {
        ///<summary>
        ///</summary>
        Catorcenal_DxN = 0,
        ///<summary>
        ///</summary>
        Quincenal_DxN = 1,
        ///<summary>
        ///</summary>
        Mensual_DxN = 2,
        ///<summary>
        ///</summary>
        Semanal = 13,
        ///<summary>
        ///</summary>
        Mensual = 3,
        ///<summary>
        ///</summary>
        Bimestral = 4,
        ///<summary>
        ///</summary>
        Trimestral = 5,
        ///<summary>
        ///</summary>
        Cuatrimestral = 12,
        ///<summary>
        ///</summary>
        Semestral = 6,
        ///<summary>
        ///</summary>
        Anual = 7,
        ///<summary>
        ///</summary>
        Contado_VP = 8,
        ///<summary>
        ///</summary>
        Semanal_VP = 14,
        ///<summary>
        ///</summary>
        Mensual_VP = 9,
        ///<summary>
        ///</summary>
        Trimestral_VP = 10,
        ///<summary>
        ///</summary>
        Semestral_VP = 11
        
        
        
       
    }

    public enum EstatusCobranzaEnum
    {
        ///<summary>
        ///</summary>
        Pagada=0,
        ///<summary>
        ///</summary>
        Pendiente=1
    }

    public enum EstatusProspectoEnum
    {
        
        ///<summary>
        ///</summary>
        Nuevo = 1,
        En_Validacion= 2,
        Cliente = 3,
        Rechazado = 4,
        Eliminado = 99

    }

    public enum EstatusPolizaEnum
    {
        ///<summary>
        ///</summary>
        EnTramite = 0,
        ///<summary>
        ///</summary>
        Vigente = 1,
        ///<summary>
        ///</summary>
        Cancelada = 2,
        ///<summary>
        ///</summary>
        Renovada = 3,
        ///<summary>
        ///</summary>
        Renovando = 4,
        ///<summary>
        ///</summary>
        RenovacionCancelada = 5,
        ///<summary>
        ///</summary>
        ModificacionPorEndoso = 6,
        ///<summary>
        ///</summary>
        Vencida = 7
    }



    public enum TipoPoliza
    {
        AutosIndividual = 0,
        DanosPool = 1,
        GMMIndividual = 2,
        GMMPool = 3,
        VidaIndividual = 4,
        VidaPool = 5,
        AutosFlotilla = 6,
    }

    public enum TipoDireccionEnum
    {
        ///<summary>
        ///</summary>
        Envio=0,
        ///<summary>
        ///</summary>
        Cobro=1,
        ///<summary>
        ///</summary>
        Oficina=2,
        ///<summary>
        ///</summary>
        Casa=3,
        ///<summary>
        ///</summary>
        Otro=4,
        ///<summary>
        ///</summary>
        Ubicacion=5
    }

    public enum EstatusClienteEnum
    {
        ///<summary>
        ///</summary>
        Prospecto=0,
        ///<summary>
        ///</summary>
        Cliente=1
    }

    public enum TipoTelefonoEnum
    {
        ///<summary>
        ///</summary>
        Oficina = 0,
        ///<summary>
        ///</summary>
        Casa = 1,
        ///<summary>
        ///</summary>
        Movil = 2,
        ///<summary>
        ///</summary>
        Nextel = 3
    }

    public enum EstatusLiquidacionEnum
    {
        ///<summary>
        ///</summary>
        Solicitud = 0,
        ///<summary>
        ///</summary>
        Aplicada = 1,
        ///<summary>
        ///</summary>
        Reabierta = 2,
        ///<summary>
        ///</summary>
        Cancelada = 3,
        ///<summary>
        ///</summary>
        Eliminada = 4
    }

    

    public enum TipoIngresoEnum
    {
        ///<summary>
        ///</summary>
        NoEspecificado=0,

        ///<summary>
        ///</summary>
        Comision=1,

        ///<summary>
        ///</summary>
        SobreComision=2,

        ///<summary>
        ///</summary>
        UsoDerechoInstalacion=3,

        ///<summary>
        ///</summary>
        Honorarios=4,

        ///<summary>
        ///</summary>
        Bono=5,

        ///<summary>
        ///</summary>
        Otro=6
    }

    public enum TipoReciboEnum
    {
        ///<summary>
        ///</summary>
        Recibo = 0,
        ///<summary>
        ///</summary>
        NotaCredito = 1
    }
    public enum EstatusEndosoEnum
    {
        ///<summary>
        ///</summary>
        EnTramite = 0,
        ///<summary>
        ///</summary>
        Vigente = 1,
        ///<summary>
        ///</summary>
        Cancelado = 2,

        Renovando = 3,

        Renovado = 4
    }
    public enum TipoEndosoEnum
    {
        A = 2198,
        B = 2201,
        D = 2205
     
    }
   public enum recordType
    {
        Endoso = 1,
        Siniestro = 2,
        Recibo = 3,
        Comision = 4,
        IncisoAutos = 5,
        VPGrupos = 6,
        Dependientes = 7,
        Bitacora = 8
    }

    public enum EstatusEnum
    {
        ///<summary>
        ///</summary>
        Inactivo = 0,
        ///<summary>
        ///</summary>
        Activo = 1,
        ///<summary>
        ///</summary>
        Eliminado = 2
    }

    public enum EstatusBienAseguradoEnum
    {
        ///<summary>
        ///</summary>
        Inactivo = 0,
        ///<summary>
        ///</summary>
        Activo = 1,
        ///<summary>
        ///</summary>
        Eliminado = 2,
        ///<summary>
        ///</summary>
        AltaEnTramite = 3,
        ///<summary>
        ///</summary>
        BajaEnTramite = 4,
        ///<summary>
        ///</summary>
        Baja = 5
    }

    public enum CoberturaAutoEnum
    {
        ///<summary>
        ///</summary>
        Amplia = 0,

        ///<summary>
        ///</summary>
        Limitada = 1,

        ///<summary>
        ///</summary>
        RC = 2

    }
    public enum TipoPlantillaEnum
    {
        ///<summary>
        ///</summary>
        Poliza = 0,
        ///<summary>
        ///</summary>
        Endoso = 1,
        ///<summary>
        ///</summary>
        Siniestro = 2,
        ///<summary>
        ///</summary>
        SolicitudMensajeria = 3,
        ///<summary>
        ///</summary>
        Recibo = 4,
        ///<summary>
        ///</summary>
        MesaControl = 5,
        ///<summary>
        ///</summary>
        Liquidacion = 6,
        ///<summary>
        ///</summary>
        SolicitudPagoProveedor = 7,
        ///<summary>
        ///</summary>
        SolicitudReembolso = 8,
        ///<summary>
        ///</summary>
        SolicitudAnticipo = 9,
        ///<summary>
        ///</summary>
        Otro = 10,
        ///<summary>
        ///</summary>
        SolicitudVacaciones = 11
    }

    public enum TipoSolicitudPagoEnum
    {
        ///<summary>
        ///</summary>
        PagoProveedor,
        ///<summary>
        ///</summary>
        Reembolso,
        ///<summary>
        ///</summary>
        Anticipo
    }

    ///<summary>
    ///</summary>
    public enum EstatusSiniestroEnum
    {
        ///<summary>
        ///</summary>
        Abierto,
        ///<summary>
        ///</summary>
        
        Cerrado,
        ///<summary>
        ///</summary>
        
        Reabierto,
        ///<summary>
        ///</summary>
        
        Rechazado
    }

}