﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class ReciboToXML
    {
        public static XElement serializaRecibo(Entity recibo,Entity Poliza,Entity Endoso)
        {
            string errmess;
            Entity Conciliacion = null;
            Entity Liquidacion = null;
            if(recibo.getAttrValueByName("Conciliacion") != string.Empty)
            {
                Conciliacion = new Entity("Conciliacion");
                Conciliacion.Attributes.Add(new Attribute("NumeroFactura"));
                Conciliacion.Attributes.Add(new Attribute("FechaConciliacion"));
                Conciliacion.Attributes.Add(new Attribute("FechaAbono"));
                Conciliacion.Attributes.Add(new Attribute("FechaAplicacionCia"));
                Conciliacion.Keys.Add(new Key("Id", recibo.getAttrValueByName("Conciliacion").ToString(),"int"));
                Conciliacion = SiteFunctions.GetEntity(Conciliacion,out errmess);
            }

            if(recibo.getAttrValueByName("Liquidacion") != string.Empty)
            {
                Liquidacion = new Entity("Liquidacion");
                Liquidacion.Attributes.Add(new Attribute("FechaLiquidacion"));
                Liquidacion.Attributes.Add(new Attribute("Folio"));
                Liquidacion.Attributes.Add(new Attribute("Observaciones"));
                
                Liquidacion.Keys.Add(new Key("Id", recibo.getAttrValueByName("Liquidacion").ToString(),"int"));
                Liquidacion = SiteFunctions.GetEntity(Liquidacion,out errmess);
            }

            int Tipo = recibo.getAttrValueByName("Tipo") != string.Empty ? Convert.ToInt32(recibo.getAttrValueByName("Tipo")) :0;
            var TipoRec = (TipoReciboEnum)Tipo;
            string TipoRecibo = TipoRec.ToString();

            var reciboFinal = new XElement("Recibo",
                new XElement("Cobertura", recibo.getAttrValueByName("Cobertura") != string.Empty ? Convert.ToDateTime(recibo.getAttrValueByName("Cobertura")).ToString("dd/MM/yyyy"):""),
                new XElement("Comision", recibo.getAttrValueByName("Comision") != string.Empty ? Convert.ToDecimal(recibo.getAttrValueByName("Comision")).ToString("C"):"0.00"),
                new XElement("ComisionCobranza", recibo.getAttrValueByName("ComisionCobranza") != string.Empty ?Convert.ToDecimal(recibo.getAttrValueByName("ComisionCobranza")).ToString("C"):"0.00"),
                new XElement("ComisionPagada", recibo.getAttrValueByName("ComisionPagada") != string.Empty ? Convert.ToDecimal(recibo.getAttrValueByName("ComisionPagada")).ToString("C"):"0.00"),
                new XElement("Concepto", recibo.getAttrValueByName("Concepto")),
                recibo.getAttrValueByName("Conciliacion") != "" ? new XElement("Conciliacion",
                    new XElement("NumeroFactura", Conciliacion.getAttrValueByName("Conciliacion.NumeroFactura")),
                    new XElement("FechaConciliacion",  Conciliacion.getAttrValueByName("FechaConciliacion") != string.Empty ? Convert.ToDateTime( Conciliacion.getAttrValueByName("FechaConciliacion")).ToString("dd/MM/yyyy") : ""),
                    new XElement("FechaAbono",  Conciliacion.getAttrValueByName("FechaAbono") != string.Empty ? Convert.ToDateTime( Conciliacion.getAttrValueByName("FechaAbono")).ToString("dd/MM/yyyy") : ""),
                    new XElement("FechaAplicacionCia",  Conciliacion.getAttrValueByName("FechaAplicacionCia") != string.Empty ? Convert.ToDateTime( Conciliacion.getAttrValueByName("FechaAplicacionCia")).ToString("dd/MM/yyyy") : "")
                    ) : new XElement("Conciliacion"),
                new XElement("Expedicion",  recibo.getAttrValueByName("Expedicion") != string.Empty ? Convert.ToDateTime(recibo.getAttrValueByName("Expedicion")).ToString("dd/MM/yyyy"):""),
                new XElement("Gastos",  recibo.getAttrValueByName("Gastos") != string.Empty  ? Convert.ToDecimal(recibo.getAttrValueByName("Gastos")).ToString("C"):"0.00"),
                new XElement("Impuesto",  recibo.getAttrValueByName("Cobertura") != string.Empty  ? Convert.ToDecimal(recibo.getAttrValueByName("ImpuestoImporte")).ToString("C"):"0.00"),
                new XElement("Inciso", recibo.getAttrValueByName("Inciso")),
                recibo.getAttrValueByName("Liquidacion") != "" ? new XElement("Liquidacion",
                    new XElement("FechaLiquidacion", Liquidacion.getAttrValueByName("FechaLiquidacion") != string.Empty ? Convert.ToDateTime(Liquidacion.getAttrValueByName("FechaLiquidacion")).ToString("dd/MM/yyyy"):""),
                    new XElement("Folio", Liquidacion.getAttrValueByName("Folio")),
                    new XElement("Observaciones", Liquidacion.getAttrValueByName("Observaciones"))
                    ) : new XElement("Liquidacion"),
                new XElement("Numero", recibo.getAttrValueByName("Numero")),
                new XElement("PagoComision", recibo.getAttrValueByName("PagoComision")),
                new XElement("PrimaNeta", recibo.getAttrValueByName("PrimaNeta") != string.Empty ? Convert.ToDecimal(recibo.getAttrValueByName("PrimaNeta")).ToString("C"):"0.00"),
                new XElement("Recargo", recibo.getAttrValueByName("Recargo") != string.Empty ? Convert.ToDecimal(recibo.getAttrValueByName("Recargo")).ToString("C"):"0.00"),
                new XElement("Serie", recibo.getAttrValueByName("Serie")),
                new XElement("Tipo", TipoRecibo),
                //new XElement("TipoIngreso", recibo.getAttrValueByName("TipoIngreso),
                new XElement("Total", recibo.getAttrValueByName("Total") != string.Empty ? Convert.ToDecimal(recibo.getAttrValueByName("Total")).ToString("C"):""),
                new XElement("Vencimiento",  recibo.getAttrValueByName("Total") != string.Empty ? Convert.ToDateTime(recibo.getAttrValueByName("Vencimiento")).ToString("dd/MM/yyyy"):""),
                serializaPoliza(Poliza),
                serializaEndoso(Endoso)
                );
            return reciboFinal;
        }

        /// <summary>
        /// Permite tomar una instancia de Recibo para transformar su Poliza en formato XML.
        /// </summary>
        /// <param name="recibo">Instancia de Recibo de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de la Poliza en formato XML.</returns>
        private static XElement serializaPoliza(Entity Poliza)
        {
            XElement element = null;
            string impuesto = string.Empty, moneda = string.Empty, paisFiscal = string.Empty, edoFiscal = string.Empty, paisCobro = string.Empty, edoCobro = string.Empty,
                    aseguradora = string.Empty, CentroBeneficio = string.Empty, Supervisor = string.Empty, Director = string.Empty, Ejecutivo = string.Empty,
                    EjecutivoAseguradora = string.Empty, Ubicacion = string.Empty, polizaOrig = string.Empty;
                Entity Propietario = new Entity(), ramo = new Entity(), Titular = new Entity(), CoTitular = new Entity(), SubRamo = new Entity(), Slip = new Entity(), 
                    DirFiscal = new Entity(), DirCobro = new Entity(), Agente = new Entity(), Contratante = new Entity();

                GeneraCartaPolizaStrategy.getObjectToGenPolizaLetter(ref Poliza, ref impuesto, ref moneda, ref paisFiscal, ref edoFiscal, ref paisCobro, ref edoCobro, ref aseguradora,
                    ref CentroBeneficio, ref Supervisor, ref Director, ref Ejecutivo, ref EjecutivoAseguradora, ref Ubicacion, ref polizaOrig, ref Propietario, ref ramo, ref Titular,
                    ref CoTitular, ref SubRamo, ref Slip, ref DirFiscal, ref DirCobro, ref Agente, ref Contratante);

                element = PolizaToXML.serializaPolizaGeneral(Poliza, impuesto, Propietario, ramo, Titular, CoTitular, SubRamo
                    , Slip, moneda, DirFiscal, paisFiscal, edoFiscal, DirCobro, paisCobro, edoCobro, Agente
                    , aseguradora, CentroBeneficio, Contratante, Supervisor, Director, Ejecutivo
                    , EjecutivoAseguradora, Ubicacion, "", "", polizaOrig);
           
           return element;
        }

        /// <summary>
        /// Permite tomar una instancia de Recibo para transformar su Endoso en formato XML.
        /// </summary>
        /// <param name="recibo">Instancia de Recibo de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion del Endoso en formato XML.</returns>
        private static XElement serializaEndoso(Entity Endoso)
        {
            XElement element = null;
            if (Endoso != null)
            {
                element = EndosoToXML.serializaEndoso(Endoso);
            }
            return element;
        }   

        private static string getEntityValue(Entity ent, string Attribute)
        {
            string retval = string.Empty;
            if (ent != null)
            {
                var Attr = ent.Attributes.Where(p => p.AttrName == Attribute);
                if(Attr.Count() > 0)
                {
                    if (Attr.First().AttrType == "datetime")
                    {
                        DateTime dval;
                        if(DateTime.TryParse(Attr.First().AttrValue,out dval))
                        {
                            retval = dval.ToShortDateString();
                        }
                    }
                    else
                    {
                        retval = Attr.First().AttrValue.ToString();
                    }
                }
            }
            return retval;
        }

    }    
}