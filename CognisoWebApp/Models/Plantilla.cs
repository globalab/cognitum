﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CognisoWebApp.Models
{
    public class Plantilla
    {
        /// <summary>
        /// Llave primaria de la entidad, se manejan llaves simples 
        /// autoincrementales, si se requieren un grupo de campos unicos
        /// se maneja un constraint por medio de la propieda Unique en 
        /// conjunto con UniqueKey para agrupar las propiedades pertenecientes
        /// al constraint.
        /// </summary>
        
        public Int64 Id { get; set; }

        ///<summary>
        ///</summary>
        public string Nombre { get; set; }

        ///<summary>
        ///</summary>
        public string Clave { get; set; }

        ///<summary>
        ///</summary>
        public string Valor { get; set; }

        ///<summary>
        ///</summary>
        public int Ramo { get; set; }

        ///<summary>
        ///</summary>
        public int Aseguradora { get; set; }

        ///<summary>
        ///</summary>
        public bool IsEditable { get; set; }

        ///<summary>
        ///</summary>
        public EstatusPolizaEnum EstatusPoliza { get; set; }

        public int SubRamo { get; set; }
        ///<summary>
        ///</summary>
        public bool Flotilla { get; set; }

        
        ///<summary>
        ///</summary>
        public TipoPlantillaEnum TipoPlantilla { get; set; }
    }
}