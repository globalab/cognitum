﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class GeneraCartaSiniestroStrategy
    {
        /// <summary>
        /// Permite generar una carta tomando una plantilla.
        /// </summary>
        /// <param name="o">Instancia de donde se obtendra la informacion con
        /// la que se llenara la carta.</param>
        /// <param name="plantilla">Plantilla que se ocupara.</param>
        /// <param name="usuarioFirmado">Usuario que aparecera como informacion
        /// adicional a la informacion de la carta por motivos de seguridad.</param>
        /// <returns>Devuelve cadena con la informacion de la carta.</returns>
        public string GeneraCarta(Entity o, Entity plantilla, Entity usuarioFirmado)
        {
            string errMsg = string.Empty;
            Entity poliza = null;
            
            #region campos de poliza
            poliza.Attributes.Add(new Attribute("Id"));
            poliza.Attributes.Add(new Attribute("Renovable"));
            poliza.Attributes.Add(new Attribute("SobreComisionPct"));
            poliza.Attributes.Add(new Attribute("SobreComisionImporte"));
            poliza.Attributes.Add(new Attribute("BonoPct"));
            poliza.Attributes.Add(new Attribute("BonoImporte"));
            poliza.Attributes.Add(new Attribute("Descuento"));
            poliza.Attributes.Add(new Attribute("EstatusPoliza"));
            poliza.Attributes.Add(new Attribute("ImpuestoImporte"));
            poliza.Attributes.Add(new Attribute("EstatusCobranza"));
            poliza.Attributes.Add(new Attribute("PolizaOriginal"));
            poliza.Attributes.Add(new Attribute("PolizaNueva"));
            poliza.Attributes.Add(new Attribute("contratante"));
            poliza.Attributes.Add(new Attribute("Impuesto"));
            poliza.Attributes.Add(new Attribute("Moneda"));
            poliza.Attributes.Add(new Attribute("Aseguradora"));
            poliza.Attributes.Add(new Attribute("Agente"));
            poliza.Attributes.Add(new Attribute("Ramo"));
            poliza.Attributes.Add(new Attribute("Ejecutivo"));
            poliza.Attributes.Add(new Attribute("SubRamo"));
            poliza.Attributes.Add(new Attribute("DirecciónCobro"));
            poliza.Attributes.Add(new Attribute("DireccionFiscal"));
            poliza.Attributes.Add(new Attribute("DireccionEnvio"));
            poliza.Attributes.Add(new Attribute("EjecutivoAseguradora"));
            poliza.Attributes.Add(new Attribute("Supervisor"));
            poliza.Attributes.Add(new Attribute("Director"));
            poliza.Attributes.Add(new Attribute("Titular"));
            poliza.Attributes.Add(new Attribute("CoTitular"));
            poliza.Attributes.Add(new Attribute("Grupo"));
            poliza.Attributes.Add(new Attribute("Negociacion"));
            poliza.Attributes.Add(new Attribute("Calendario"));
            poliza.Attributes.Add(new Attribute("NueroCobranza"));
            poliza.Attributes.Add(new Attribute("OcultaCaratula"));
            poliza.Attributes.Add(new Attribute("Caratula"));
            poliza.Attributes.Add(new Attribute("NumeroCaratula"));
            #endregion
            Entity Siniestro;
            if (o.EntityName.ToLower() == "complementosiniestro")
            {
                #region camposSiniestro
                Siniestro = new Entity("Siniestro");
                Siniestro.Attributes.Add(new Attribute("Id"));
                Siniestro.Attributes.Add(new Attribute("Numero"));
                Siniestro.Attributes.Add(new Attribute("FechaSiniestro"));
                Siniestro.Attributes.Add(new Attribute("FechaFiniquito"));
                Siniestro.Attributes.Add(new Attribute("FechaReporte"));
                Siniestro.Attributes.Add(new Attribute("FechaRecepcionCia"));
                Siniestro.Attributes.Add(new Attribute("FechaDoc"));
                Siniestro.Attributes.Add(new Attribute("Observaciones"));
                Siniestro.Attributes.Add(new Attribute("EstatusPagoSiniestro"));
                Siniestro.Attributes.Add(new Attribute("EstatusSiniestro"));
                Siniestro.Attributes.Add(new Attribute("BienAsegurado"));
                Siniestro.Attributes.Add(new Attribute("Poliza"));
                #endregion    

                Siniestro.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));
                Siniestro = SiteFunctions.GetEntity(Siniestro, out errMsg);
            }
            else
            {
                Siniestro = o;
            }

            poliza.Keys.Add(new Key("Id", o.getAttrValueByName("Poliza"), "int"));
            poliza = SiteFunctions.GetEntity(poliza, out errMsg);
            var xml = SiniestroToXML.serializaSiniestro(Siniestro);

            if (poliza != null)
            {
                #region serializapoliza
                string impuesto = string.Empty, moneda = string.Empty, paisFiscal = string.Empty, edoFiscal = string.Empty, paisCobro = string.Empty, edoCobro = string.Empty,
                    aseguradora = string.Empty, CentroBeneficio = string.Empty, Supervisor = string.Empty, Director = string.Empty, Ejecutivo = string.Empty,
                    EjecutivoAseguradora = string.Empty, Ubicacion = string.Empty, polizaOrig = string.Empty;
                Entity Propietario = new Entity(), ramo = new Entity(), Titular = new Entity(), CoTitular = new Entity(), SubRamo = new Entity(), Slip = new Entity(), 
                    DirFiscal = new Entity(), DirCobro = new Entity(), Agente = new Entity(), Contratante = new Entity();

                GeneraCartaPolizaStrategy.getObjectToGenPolizaLetter(ref poliza, ref impuesto, ref moneda, ref paisFiscal, ref edoFiscal, ref paisCobro, ref edoCobro, ref aseguradora,
                    ref CentroBeneficio, ref Supervisor, ref Director, ref Ejecutivo, ref EjecutivoAseguradora, ref Ubicacion, ref polizaOrig, ref Propietario, ref ramo, ref Titular,
                    ref CoTitular, ref SubRamo, ref Slip, ref DirFiscal, ref DirCobro, ref Agente, ref Contratante);

                xml.Add(PolizaToXML.serializaPolizaGeneral(poliza, impuesto, Propietario, ramo, Titular, CoTitular, SubRamo
                    , Slip, moneda, DirFiscal, paisFiscal, edoFiscal, DirCobro, paisCobro, edoCobro, Agente
                    , aseguradora, CentroBeneficio, Contratante, Supervisor, Director, Ejecutivo
                    , EjecutivoAseguradora, Ubicacion, "", "", polizaOrig));
                #endregion
            }

                #region Puesto
                Entity Puesto = new Entity("Puesto");
                Puesto.Attributes.Add(new Attribute("Nombre"));
                Puesto.Keys.Add(new Key("Id", usuarioFirmado.getAttrValueByName("Puesto"), "int"));

                Puesto = SiteFunctions.GetEntity(Puesto, out errMsg);
               
                #endregion

            xml.FirstNode.AddAfterSelf(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.getAttrValueByName("Nombre")),
                new XElement("Email", usuarioFirmado.getAttrValueByName("Email")),
                new XElement("Puesto", Puesto.getAttrValueByName("Nombre")))
            );

            return SiteFunctions.Transform(xml, plantilla);
        }

        /// <summary>
        /// Permite generar una carta de un complemento tomando una plantilla.
        /// </summary>
        /// <param name="o">Instancia de donde se obtendra la informacion con
        /// la que se llenara la carta.</param>
        /// <param name="plantilla">Plantilla que se ocupara.</param>
        /// <param name="usuarioFirmado">Usuario que aparecera como informacion
        /// adicional a la informacion de la carta por motivos de seguridad.</param>
        /// <returns>Devuelve cadena con la informacion de la carta.</returns>
        public string GeneraCartaComplemento(Entity o, Entity plantilla, Entity usuarioFirmado)
        {
            #region camposSiniestro
            Entity Siniestro = new Entity("Siniestro");
            Siniestro.Attributes.Add(new Attribute("Id"));
            Siniestro.Attributes.Add(new Attribute("Numero"));
            Siniestro.Attributes.Add(new Attribute("FechaSiniestro"));
            Siniestro.Attributes.Add(new Attribute("FechaFiniquito"));
            Siniestro.Attributes.Add(new Attribute("FechaReporte"));
            Siniestro.Attributes.Add(new Attribute("FechaRecepcionCia"));
            Siniestro.Attributes.Add(new Attribute("FechaDoc"));
            Siniestro.Attributes.Add(new Attribute("Observaciones"));
            Siniestro.Attributes.Add(new Attribute("EstatusPagoSiniestro"));
            Siniestro.Attributes.Add(new Attribute("EstatusSiniestro"));
            Siniestro.Attributes.Add(new Attribute("BienAsegurado"));
            Siniestro.Attributes.Add(new Attribute("Poliza"));
            #endregion
            Siniestro.Keys.Add(new Key("Id", o.getAttrValueByName("Id"), "int"));
            string errMsg = string.Empty;
            Siniestro = SiteFunctions.GetEntity(Siniestro, out errMsg);

            var xml = SiniestroToXML.serializaSiniestro(Siniestro);

            if (Siniestro.getAttrValueByName("Poliza") != string.Empty)
            {
                Entity poliza = new Entity("Poliza");
                #region campos de poliza
                poliza.Attributes.Add(new Attribute("Id"));
                poliza.Attributes.Add(new Attribute("Renovable"));
                poliza.Attributes.Add(new Attribute("SobreComisionPct"));
                poliza.Attributes.Add(new Attribute("SobreComisionImporte"));
                poliza.Attributes.Add(new Attribute("BonoPct"));
                poliza.Attributes.Add(new Attribute("BonoImporte"));
                poliza.Attributes.Add(new Attribute("Descuento"));
                poliza.Attributes.Add(new Attribute("EstatusPoliza"));
                poliza.Attributes.Add(new Attribute("ImpuestoImporte"));
                poliza.Attributes.Add(new Attribute("EstatusCobranza"));
                poliza.Attributes.Add(new Attribute("PolizaOriginal"));
                poliza.Attributes.Add(new Attribute("PolizaNueva"));
                poliza.Attributes.Add(new Attribute("contratante"));
                poliza.Attributes.Add(new Attribute("Impuesto"));
                poliza.Attributes.Add(new Attribute("Moneda"));
                poliza.Attributes.Add(new Attribute("Aseguradora"));
                poliza.Attributes.Add(new Attribute("Agente"));
                poliza.Attributes.Add(new Attribute("Ramo"));
                poliza.Attributes.Add(new Attribute("Ejecutivo"));
                poliza.Attributes.Add(new Attribute("SubRamo"));
                poliza.Attributes.Add(new Attribute("DirecciónCobro"));
                poliza.Attributes.Add(new Attribute("DireccionFiscal"));
                poliza.Attributes.Add(new Attribute("DireccionEnvio"));
                poliza.Attributes.Add(new Attribute("EjecutivoAseguradora"));
                poliza.Attributes.Add(new Attribute("Supervisor"));
                poliza.Attributes.Add(new Attribute("Director"));
                poliza.Attributes.Add(new Attribute("Titular"));
                poliza.Attributes.Add(new Attribute("CoTitular"));
                poliza.Attributes.Add(new Attribute("Grupo"));
                poliza.Attributes.Add(new Attribute("Negociacion"));
                poliza.Attributes.Add(new Attribute("Calendario"));
                poliza.Attributes.Add(new Attribute("NueroCobranza"));
                poliza.Attributes.Add(new Attribute("OcultaCaratula"));
                poliza.Attributes.Add(new Attribute("Caratula"));
                poliza.Attributes.Add(new Attribute("NumeroCaratula"));
                #endregion
                poliza.Keys.Add(new Key("Id", Siniestro.getAttrValueByName("Poliza"), "int"));
                poliza = SiteFunctions.GetEntity(poliza, out errMsg);
                #region serializapoliza
                #region busca impuesto
                Entity entitySearch = new Entity("Impuesto");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Impuesto"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string impuesto = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region tramite
                entitySearch = new Entity("Tramite");
                entitySearch.Attributes.Add(new Attribute("Propietario"));
                entitySearch.Attributes.Add(new Attribute("CentroDeBeneficio"));
                entitySearch.Attributes.Add(new Attribute("Ubicacion"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Id"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string propietario = entitySearch.getAttrValueByName("Propietario");
                string centroBenStr = entitySearch.getAttrValueByName("CentroDeBeneficio");
                string ubicacionStr = entitySearch.getAttrValueByName("Ubicacion");

                entitySearch = new Entity("CentroDeBeneficio");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", centroBenStr, "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string CentroBeneficio = entitySearch.getAttrValueByName("Nombre");

                entitySearch = new Entity("Ubicacion");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", ubicacionStr, "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string Ubicacion = entitySearch.getAttrValueByName("Nombre");

                #endregion
                #region propietairo
                Entity Propietario = new Entity("Usuario");
                Propietario.Attributes.Add(new Attribute("Id"));
                Propietario.Attributes.Add(new Attribute("Nombre"));
                Propietario.Attributes.Add(new Attribute("Email"));
                Propietario.Attributes.Add(new Attribute("UserName"));
                Propietario.Attributes.Add(new Attribute("Ubicacion"));
                Propietario.Attributes.Add(new Attribute("CentroDeBeneficio"));
                Propietario.Attributes.Add(new Attribute("Propietario"));
                Propietario.Attributes.Add(new Attribute("Puesto"));
                Propietario.Attributes.Add(new Attribute("Perfil"));
                Propietario.Attributes.Add(new Attribute("FechaIngreso"));
                Propietario.Attributes.Add(new Attribute("Telefono"));
                Propietario.Attributes.Add(new Attribute("NumeroEmpleado"));
                Propietario.Attributes.Add(new Attribute("Observaciones"));
                Propietario.Keys.Add(new Key("Id", propietario, "int"));

                Propietario = SiteFunctions.GetEntity(Propietario, out errMsg);
                #endregion
                #region Ramo
                Entity Ramo = new Entity("Ramo");
                Ramo.Attributes.Add(new Attribute("Id"));
                Ramo.Attributes.Add(new Attribute("DivisionOperativa"));
                Ramo.Attributes.Add(new Attribute("Clave"));
                Ramo.Attributes.Add(new Attribute("Nombre"));
                Ramo.Attributes.Add(new Attribute("Xtype"));
                Ramo.Attributes.Add(new Attribute("IdSap"));
                Ramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ramo"), "int"));

                Ramo = SiteFunctions.GetEntity(Ramo, out errMsg);
                #endregion
                #region Titular
                Entity Titular = new Entity("ContactoSimple");
                Titular.Attributes.Add(new Attribute("Id"));
                Titular.Attributes.Add(new Attribute("Email"));
                Titular.Attributes.Add(new Attribute("NombreCompleto"));
                Titular.Attributes.Add(new Attribute("Cliente"));
                Titular.Attributes.Add(new Attribute("BienAsegurado"));
                Titular.Keys.Add(new Key("Id", poliza.getAttrValueByName("Titular"), "int"));

                Titular = SiteFunctions.GetEntity(Titular, out errMsg);
                #endregion
                #region CoTitular
                Entity CoTitular = new Entity("ContactoSimple");
                CoTitular.Attributes.Add(new Attribute("Id"));
                CoTitular.Attributes.Add(new Attribute("Email"));
                CoTitular.Attributes.Add(new Attribute("NombreCompleto"));
                CoTitular.Attributes.Add(new Attribute("Cliente"));
                CoTitular.Attributes.Add(new Attribute("BienAsegurado"));
                CoTitular.Keys.Add(new Key("Id", poliza.getAttrValueByName("CoTitular"), "int"));

                CoTitular = SiteFunctions.GetEntity(CoTitular, out errMsg);
                #endregion
                #region SubRamo
                Entity SubRamo = new Entity("SubRamo");
                SubRamo.Attributes.Add(new Attribute("Id"));
                SubRamo.Attributes.Add(new Attribute("Clave"));
                SubRamo.Attributes.Add(new Attribute("Nombre"));
                SubRamo.Attributes.Add(new Attribute("Flotilla"));
                SubRamo.Attributes.Add(new Attribute("Ramo"));
                SubRamo.Keys.Add(new Key("Id", poliza.getAttrValueByName("SubRamo"), "int"));

                SubRamo = SiteFunctions.GetEntity(SubRamo, out errMsg);
                #endregion
                #region Slip
                Entity Slip = new Entity("Slip");
                Slip.Attributes.Add(new Attribute("Id"));
                Slip.Attributes.Add(new Attribute("Descripcion"));
                Slip.Keys.Add(new Key("Id", poliza.getAttrValueByName("Id"), "int"));

                Slip = SiteFunctions.GetEntity(Slip, out errMsg);
                #endregion
                #region busca Moneda
                entitySearch = new Entity("Moneda");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Moneda"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string moneda = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region DirFiscal
                Entity DirFiscal = new Entity("Direccion");
                DirFiscal.Attributes.Add(new Attribute("Id"));
                DirFiscal.Attributes.Add(new Attribute("Tipo"));
                DirFiscal.Attributes.Add(new Attribute("Calle"));
                DirFiscal.Attributes.Add(new Attribute("Colonia"));
                DirFiscal.Attributes.Add(new Attribute("Cp"));
                DirFiscal.Attributes.Add(new Attribute("Delegacion"));
                DirFiscal.Attributes.Add(new Attribute("Nombre"));
                DirFiscal.Attributes.Add(new Attribute("NumExterior"));
                DirFiscal.Attributes.Add(new Attribute("NumInterior"));
                DirFiscal.Attributes.Add(new Attribute("Cliente"));
                DirFiscal.Attributes.Add(new Attribute("Estado"));
                DirFiscal.Attributes.Add(new Attribute("Pais"));
                DirFiscal.Keys.Add(new Key("Id", poliza.getAttrValueByName("DireccionFiscal"), "int"));

                DirFiscal = SiteFunctions.GetEntity(DirFiscal, out errMsg);


                string paisFiscal = string.Empty;
                string edoFiscal = string.Empty;

                if (DirFiscal != null)
                {
                    entitySearch = new Entity("Estado");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirFiscal.getAttrValueByName("Estado")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    edoFiscal = entitySearch.getAttrValueByName("Nombre");

                    entitySearch = new Entity("Pais");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirFiscal.getAttrValueByName("Pais")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    paisFiscal = entitySearch.getAttrValueByName("Nombre");
                }

                #endregion
                #region DirCobro
                Entity DirCobro = new Entity("Direccion");
                DirCobro.Attributes.Add(new Attribute("Id"));
                DirCobro.Attributes.Add(new Attribute("Tipo"));
                DirCobro.Attributes.Add(new Attribute("Calle"));
                DirCobro.Attributes.Add(new Attribute("Colonia"));
                DirCobro.Attributes.Add(new Attribute("Cp"));
                DirCobro.Attributes.Add(new Attribute("Delegacion"));
                DirCobro.Attributes.Add(new Attribute("Nombre"));
                DirCobro.Attributes.Add(new Attribute("NumExterior"));
                DirCobro.Attributes.Add(new Attribute("NumInterior"));
                DirCobro.Attributes.Add(new Attribute("Cliente"));
                DirCobro.Attributes.Add(new Attribute("Estado"));
                DirCobro.Attributes.Add(new Attribute("Pais"));
                DirCobro.Keys.Add(new Key("Id", poliza.getAttrValueByName("DireccionCobro"), "int"));

                DirCobro = SiteFunctions.GetEntity(DirCobro, out errMsg);


                string paisCobro = string.Empty;
                string edoCobro = string.Empty;

                if (DirCobro != null)
                {
                    entitySearch = new Entity("Estado");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirCobro.getAttrValueByName("Estado")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    edoFiscal = entitySearch.getAttrValueByName("Nombre");

                    entitySearch = new Entity("Pais");
                    entitySearch.Attributes.Add(new Attribute("Nombre"));
                    entitySearch.Keys.Add(new Key("Id", DirCobro.getAttrValueByName("Pais")));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    paisFiscal = entitySearch.getAttrValueByName("Nombre");
                }

                #endregion
                #region Agente
                Entity Agente = new Entity("Agente");
                Agente.Attributes.Add(new Attribute("Id"));
                Agente.Attributes.Add(new Attribute("Clave"));
                Agente.Attributes.Add(new Attribute("Nombre"));
                Agente.Attributes.Add(new Attribute("Ramo"));
                Agente.Keys.Add(new Key("Id", poliza.getAttrValueByName("Agente"), "int"));

                Agente = SiteFunctions.GetEntity(Agente, out errMsg);
                #endregion
                #region busca Aseguradora
                entitySearch = new Entity("PersonaMoral");
                entitySearch.Attributes.Add(new Attribute("RazonSocial"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Aseguradora"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string Aseguradora = entitySearch.getAttrValueByName("RazonSocial");
                #endregion
                #region Contratante
                Entity Contratante = new Entity("Cliente");
                Contratante.Attributes.Add(new Attribute("Id"));
                Contratante.Attributes.Add(new Attribute("Tipo"));
                Contratante.Attributes.Add(new Attribute("NombreCompleto"));
                Contratante.Attributes.Add(new Attribute("RFC"));
                Contratante.Attributes.Add(new Attribute("Giro"));
                Contratante.Attributes.Add(new Attribute("Grupo"));
                Contratante.Attributes.Add(new Attribute("IdSap"));
                Contratante.Attributes.Add(new Attribute("Registrante"));
                Contratante.Attributes.Add(new Attribute("EstatusCliente"));
                Contratante.Attributes.Add(new Attribute("CoRegistrante"));
                Contratante.Keys.Add(new Key("Id", poliza.getAttrValueByName("Contratante"), "int"));

                Contratante = SiteFunctions.GetEntity(Contratante, out errMsg);
                #endregion
                #region busca Supervisor
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Supervisor"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string Supervisor = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region busca Director
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Director"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string Director = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region busca Ejecutivo
                entitySearch = new Entity("Usuario");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ejecutivo"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string Ejecutivo = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region busca EjecutivoAseguradora
                entitySearch = new Entity("ContactoPersonaMoral");
                entitySearch.Attributes.Add(new Attribute("Nombre"));
                entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("EjecutivoAseguradora"), "int"));

                entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                string EjecutivoAseguradora = entitySearch.getAttrValueByName("Nombre");
                #endregion
                #region polizaOriginal
                string polizaOrig = string.Empty;
                if (poliza.getAttrValueByName("PolizaOriginal") != string.Empty)
                {
                    entitySearch = new Entity("Tramite");
                    entitySearch.Attributes.Add(new Attribute("Folio"));
                    entitySearch.Keys.Add(new Key("Id", poliza.getAttrValueByName("PolizaOriginal"), "int"));

                    entitySearch = SiteFunctions.GetEntity(entitySearch, out errMsg);

                    polizaOrig = entitySearch.getAttrValueByName("Folio");
                }
                #endregion
                xml.Add(PolizaToXML.serializaPolizaGeneral(poliza, impuesto, Propietario, Ramo, Titular, CoTitular, SubRamo
                    , Slip, moneda, DirFiscal, paisFiscal, edoFiscal, DirCobro, paisCobro, edoCobro, Agente
                    , Aseguradora, CentroBeneficio, Contratante, Supervisor, Director, Ejecutivo
                    , EjecutivoAseguradora, Ubicacion, "", "", polizaOrig));
                #endregion
            }

            #region Puesto
            Entity Puesto = new Entity("Puesto");
            Puesto.Attributes.Add(new Attribute("Nombre"));
            Puesto.Keys.Add(new Key("Id", usuarioFirmado.getAttrValueByName("Puesto"), "int"));

            Puesto = SiteFunctions.GetEntity(Puesto, out errMsg);

            #endregion

            xml.FirstNode.AddAfterSelf(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.getAttrValueByName("Nombre")),
                new XElement("Email", usuarioFirmado.getAttrValueByName("Email")),
                new XElement("Puesto", Puesto.getAttrValueByName("Nombre")))
            );
            xml.Add(new XElement("FechaOperacion", DateTime.Now.ToString("dd/MM/yyyy")));

            return SiteFunctions.Transform(xml, plantilla);
        }
    }
}