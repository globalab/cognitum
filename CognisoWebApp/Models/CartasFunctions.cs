﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CognisoWebApp.Models
{
    public static class SearchPlantillas
    {
        public static List<Entity> GetByTipo_EstatusPoliza_EstatusPlan_DivisionOperativa(Entity poliza, Entity tramite,TipoPlantillaEnum tipo)
        {
            string errmess = string.Empty;
            Entity Subramo = new Entity("Subramo");
            Subramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("SubRamo").ToString(), "int"));
            Subramo.Attributes.Add(new Attribute("Flotilla"));
            Subramo = SiteFunctions.GetEntity(Subramo, out errmess);

            //Entity Ramo = new Entity("Ramo");
            //Ramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ramo").ToString(), "int"));
            //Ramo.Attributes.Add(new Attribute("DivisionOperativa"));
            //Ramo = SiteFunctions.GetEntity(Ramo, out errmess);
            
            Entity Plantillaaux = new Entity("Plantilla");
            Plantillaaux.Keys.Add(new Key("Flotilla", "", "int", whereOperator.IsNull, 1));
            Plantillaaux.Keys.Add(new Key("Flotilla", Subramo.getAttrValueByName("Flotilla").ToString()=="True"?"1":"0","int",2,1));
            Plantillaaux.Keys.Add(new Key("TipoPlantilla", ((int)tipo).ToString(), "int",1,2));
            //Plantillaaux.Keys.Add(new Key("Estatus", ((int)EstatusEnum.Activo).ToString(), "int",1,1));
            Plantillaaux.Keys.Add(new Key("Ramo", poliza.getAttrValueByName("Ramo").ToString(), "int",1,2));
            Plantillaaux.Keys.Add(new Key("SubRamo", poliza.getAttrValueByName("SubRamo").ToString(), "int",1,2));
            Plantillaaux.Keys.Add(new Key("Aseguradora", poliza.getAttrValueByName("Aseguradora").ToString(), "int", 1, 2));

            if (poliza.getKeyValueByName("EstatusPoliza").ToString() != ((int)EstatusPolizaEnum.Vigente).ToString() && poliza.getKeyValueByName("EstatusPoliza").ToString() != ((int)EstatusPolizaEnum.EnTramite).ToString())
                Plantillaaux.Keys.Add(new Key("estatusPoliza", ((int)EstatusPolizaEnum.Vigente).ToString(),"int",1,2));
            else
                Plantillaaux.Keys.Add(new Key("estatusPoliza", poliza.getAttrValueByName("EstatusPoliza").ToString(),"int",1,2));



            List<Entity> results = SiteFunctions.GetValues(Plantillaaux, out errmess);
            
            return results;
        }

        public static List<Entity> GetByTipo_EstatusPoliza(Entity poliza, Entity tramite, TipoPlantillaEnum tipo)
        {
            string errmess = string.Empty;
            Entity Subramo = new Entity("Subramo");
            Subramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("SubRamo").ToString(), "int"));
            Subramo.Attributes.Add(new Attribute("Flotilla"));
            Subramo = SiteFunctions.GetEntity(Subramo, out errmess);

            //Entity Ramo = new Entity("Ramo");
            //Ramo.Keys.Add(new Key("Id", poliza.getAttrValueByName("Ramo").ToString(), "int"));
            //Ramo.Attributes.Add(new Attribute("DivisionOperativa"));
            //Ramo = SiteFunctions.GetEntity(Ramo, out errmess);

            Entity Plantillaaux = new Entity("Plantilla");
            Plantillaaux.Keys.Add(new Key("Flotilla", "", "int", whereOperator.IsNull, 1));
            Plantillaaux.Keys.Add(new Key("Flotilla", Subramo.getAttrValueByName("Flotilla").ToString() == "True" ? "1" : "0", "int", 2, 1));
            Plantillaaux.Keys.Add(new Key("TipoPlantilla", ((int)tipo).ToString(), "int", 1, 2));
            //Plantillaaux.Keys.Add(new Key("Estatus", ((int)EstatusEnum.Activo).ToString(), "int",1,1));
            Plantillaaux.Keys.Add(new Key("Ramo", poliza.getAttrValueByName("Ramo").ToString(), "int", 1, 2));
            Plantillaaux.Keys.Add(new Key("SubRamo", poliza.getAttrValueByName("SubRamo").ToString(), "int",1,2,whereOperator.IsNull));
            //Plantillaaux.Keys.Add(new Key("Aseguradora", poliza.getAttrValueByName("Aseguradora").ToString(), "int", 1, 2));

            if (poliza.getKeyValueByName("EstatusPoliza").ToString() != ((int)EstatusPolizaEnum.Vigente).ToString() && poliza.getKeyValueByName("EstatusPoliza").ToString() != ((int)EstatusPolizaEnum.EnTramite).ToString())
                Plantillaaux.Keys.Add(new Key("estatusPoliza", ((int)EstatusPolizaEnum.Vigente).ToString(), "int", 1, 2));
            else
                Plantillaaux.Keys.Add(new Key("estatusPoliza", poliza.getAttrValueByName("EstatusPoliza").ToString(), "int", 1, 2));



            List<Entity> results = SiteFunctions.GetValues(Plantillaaux, out errmess);

            return results;
        }

        public static List<Entity> GetByTipo(TipoPlantillaEnum tipo)
        {
            string errmess = string.Empty;
            Entity Plantillaaux = new Entity("Plantilla");
            Plantillaaux.Keys.Add(new Key("TipoPlantilla", ((int)tipo).ToString(), "int",1,1));
            Plantillaaux.Keys.Add(new Key("Estatus", ((int)EstatusEnum.Activo).ToString(), "int",1,1,"a"));
            List<Entity> results = SiteFunctions.GetValues(Plantillaaux, out errmess);

            return results;
        }

        public static List<Entity> ObtienePlantillasSiniestro(string siniestroId)
        {
            string Errmsg = string.Empty;
            List<Plantilla> retval = new List<Plantilla>();
            Entity siniestro = new Entity("Siniestro");
            siniestro.Attributes.Add(new Attribute("Poliza"));
            siniestro.Keys.Add(new Key("Id",siniestroId,"int"));
            var siniestros = SiteFunctions.GetEntity(siniestro,out Errmsg);
            if(siniestros != null)
            {
                Entity poliza = new Entity("Poliza");
                poliza.Keys.Add(new Key("Id",siniestros.getAttrValueByName("Poliza").ToString(),"int"));
                Entity Tramite = new Entity("Tramite");
                Tramite.Keys.Add(new Key("Id", siniestros.getAttrValueByName("Poliza").ToString(), "int"));
                SiteFunctions funct = new SiteFunctions();
                funct.GetPolizaTramite(ref poliza,ref Tramite);
                if (poliza == null)
                {
                    return new List<Entity>();
                }

                return GetByTipo_EstatusPoliza_EstatusPlan_DivisionOperativa(poliza,Tramite, TipoPlantillaEnum.Siniestro);
            }
            else
            {
                return new List<Entity>();
            }
        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad ComplementoSiniestro a partir del identificador del complemento de siniestro.
        /// </summary>
        /// <param name="complementoId">El idientificador del complemento de siniestro.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad complemento de siniestro.</returns>
        public static List<Entity> ObtienePlantillasComplementoSin(string complementoId)
        {
            string Errmsg = string.Empty;
            Entity Complemento = new Entity("ComplementoSiniestro");
            Complemento.Attributes.Add(new Attribute("Poliza"));
            Complemento.Keys.Add(new Key("Id", complementoId, "int"));
            Complemento = SiteFunctions.GetEntity(Complemento,out Errmsg);
            if (Complemento != null)
            {
                Entity poliza = new Entity("Poliza");
                poliza.Keys.Add(new Key("Id", Complemento.getAttrValueByName("Poliza").ToString(), "int"));
                Entity Tramite = new Entity("Tramite");
                Tramite.Keys.Add(new Key("Id", Complemento.getAttrValueByName("Poliza").ToString(), "int"));
                SiteFunctions funct = new SiteFunctions();
                funct.GetPolizaTramite(ref poliza, ref Tramite);
                if (poliza == null)
                {
                    return new List<Entity>();
                }

                return GetByTipo_EstatusPoliza_EstatusPlan_DivisionOperativa(poliza, Tramite, TipoPlantillaEnum.Siniestro);
            }
            else
            {
                return new List<Entity>();
            }
                
        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad Endoso a partir del identificador del endoso.
        /// </summary>
        /// <param name="endosoId">El idientificador del endoso.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad endoso.</returns>
        public static List<Entity> ObtienePlantillasEndoso(string endosoId)
        {
            string Errmsg = string.Empty;
            Entity Endoso = new Entity("Endoso");
            Endoso.Attributes.Add(new Attribute("Poliza"));
            Endoso.Keys.Add(new Key("Id", endosoId, "int"));
            Endoso = SiteFunctions.GetEntity(Endoso, out Errmsg);
            if (Endoso != null)
            {
                Entity poliza = new Entity("Poliza");
                poliza.Keys.Add(new Key("Id", Endoso.getAttrValueByName("Poliza").ToString(), "int"));
                Entity Tramite = new Entity("Tramite");
                Tramite.Keys.Add(new Key("Id", Endoso.getAttrValueByName("Poliza").ToString(), "int"));
                SiteFunctions funct = new SiteFunctions();
                funct.GetPolizaTramite(ref poliza, ref Tramite);
                if (poliza == null)
                {
                    return new List<Entity>();
                }

                return GetByTipo_EstatusPoliza_EstatusPlan_DivisionOperativa(poliza, Tramite, TipoPlantillaEnum.Endoso);
            }
            else
            {
                return new List<Entity>();
            }

        }

        public static List<Entity> ObtienePlantillasEndosoSinFiltros(string endosoId)
        {
            string Errmsg = string.Empty;
            Entity Endoso = new Entity("Endoso");
            Endoso.Attributes.Add(new Attribute("Poliza"));
            Endoso.Keys.Add(new Key("Id", endosoId, "int"));
            Endoso = SiteFunctions.GetEntity(Endoso, out Errmsg);
            if (Endoso != null)
            {
                Entity poliza = new Entity("Poliza");
                poliza.Keys.Add(new Key("Id", Endoso.getAttrValueByName("Poliza").ToString(), "int"));
                Entity Tramite = new Entity("Tramite");
                Tramite.Keys.Add(new Key("Id", Endoso.getAttrValueByName("Poliza").ToString(), "int"));
                SiteFunctions funct = new SiteFunctions();
                funct.GetPolizaTramite(ref poliza, ref Tramite);
                if (poliza == null)
                {
                    return new List<Entity>();
                }

                return GetByTipo_EstatusPoliza(poliza, Tramite, TipoPlantillaEnum.Endoso);
            }
            else
            {
                return new List<Entity>();
            }

        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad Poliza a partir del identificador del poliza.
        /// </summary>
        /// <param name="polizaId">El idientificador de la píliza.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad Poliza.</returns>
        public static List<Entity> ObtienePlantillasPoliza(string polizaId)
        {
            Entity poliza = new Entity("Poliza");
            poliza.Keys.Add(new Key("Id", polizaId, "int"));
            Entity Tramite = new Entity("Tramite");
            Tramite.Keys.Add(new Key("Id", polizaId, "int"));
            SiteFunctions funct = new SiteFunctions();
            funct.GetPolizaTramite(ref poliza, ref Tramite);
            if (poliza == null)
            {
                return new List<Entity>();
            }

            return GetByTipo_EstatusPoliza_EstatusPlan_DivisionOperativa(poliza, Tramite, TipoPlantillaEnum.Poliza);
        }


        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad Poliza a partir del identificador del poliza.
        /// </summary>
        /// <param name="polizaId">El idientificador de la píliza.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad Poliza.</returns>
        public static List<Entity> ObtienePlantillasPolizaSinFiltros(string polizaId)
        {
            Entity poliza = new Entity("Poliza");
            poliza.Keys.Add(new Key("Id", polizaId, "int"));
            Entity Tramite = new Entity("Tramite");
            Tramite.Keys.Add(new Key("Id", polizaId, "int"));
            SiteFunctions funct = new SiteFunctions();
            funct.GetPolizaTramite(ref poliza, ref Tramite);
            if (poliza == null)
            {
                return new List<Entity>();
            }

            return GetByTipo_EstatusPoliza(poliza, Tramite, TipoPlantillaEnum.Poliza);
        }

        public static List<Entity> ObtienePlantillasRecibo()
        {
            return GetByTipo(TipoPlantillaEnum.Recibo);
        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad Solicitudmensajeria a partir del identificador de la solicitud.
        /// </summary>
        /// <param name="solicitudId">El idientificador de la solicitud.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad Solicitudmensajeria.</returns>
        public static List<Entity> ObtienePlantillasSolicitudMensajeria()
        {
            return GetByTipo(TipoPlantillaEnum.SolicitudMensajeria);
        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad Liquidacion a partir del identificador de la solicitud.
        /// </summary>
        /// <param name="liqId">El idientificador de la liquidacion.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad Liquidacion.</returns>
        public static List<Entity> ObtienePlantillasLiquidacion()
        {
            return GetByTipo(TipoPlantillaEnum.Liquidacion);
        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad SolicitudPago a partir del identificador de la solicitud.
        /// </summary>
        /// <param name="solicitudPagoId">El idientificador de la solicitud de pago.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad SolicitudPago.</returns>
        public static List<Entity> ObtienePlantillasSolPago(string solicitudPagoId)
        {
            string errmess = string.Empty;
            var tpe = TipoPlantillaEnum.SolicitudPagoProveedor;
            Entity SolPago = new Entity("SolicitudPago");
            SolPago.Keys.Add(new Key("Id", solicitudPagoId, "int"));
            SolPago.Attributes.Add(new Attribute("TipoSolicitudPago"));
            SolPago = SiteFunctions.GetEntity(SolPago, out errmess);

            if (SolPago.getAttrValueByName("TipoSolicitudPago").ToString() == ((int)TipoSolicitudPagoEnum.PagoProveedor).ToString())
            {
                tpe = TipoPlantillaEnum.SolicitudPagoProveedor;
            }
            else if (SolPago.getAttrValueByName("TipoSolicitudPago").ToString() == ((int)TipoSolicitudPagoEnum.Reembolso).ToString())
            {
                tpe = TipoPlantillaEnum.SolicitudReembolso;
            }
            else if (SolPago.getAttrValueByName("TipoSolicitudPago").ToString() == ((int)TipoSolicitudPagoEnum.Anticipo).ToString())
            {
                tpe = TipoPlantillaEnum.SolicitudAnticipo;
            }

            return GetByTipo(tpe);

        }

        /// <summary>
        /// Obtiene las plantillas XSL de Cartas para la entidad SolicitudVacaciones a partir del identificador de la solicitud.
        /// </summary>
        /// <param name="solicitudVacacionesId">El idientificador de la solicitud de pago.</param>
        /// <returns>Lista de plantillas XSL correspondientes a la entidad SolicitudPago.</returns>
        public static List<Entity> ObtienePlantillasSolVacaciones()
        {

            var tpe = TipoPlantillaEnum.SolicitudVacaciones;
            return GetByTipo(tpe);

        }
    }
    public class CartasFunctions
    {
        
        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad siniestro y la plantilla dadas.
        /// </summary>
        /// <param name="siniestroId">Siniestro para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        public string GeneraCartaSiniestro(int siniestroId, int plantillaId, string userId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            Entity siniestro = this.searchSiniestro(siniestroId.ToString());

            GeneraCartaSiniestroStrategy genCartaSiniestroStrategy = new GeneraCartaSiniestroStrategy();

            return genCartaSiniestroStrategy.GeneraCarta(siniestro, plantilla, usuarioFirmado);
        }

        private Entity searchPlantilla(string _plantillaId)
        {
            string errMsg = string.Empty;
            Entity plantilla = new Entity("Plantilla");
            #region campos
            plantilla.Attributes.Add(new Attribute("Id"));
            plantilla.Attributes.Add(new Attribute("Permiso"));
            plantilla.Attributes.Add(new Attribute("Nombre"));
            plantilla.Attributes.Add(new Attribute("Clave"));
            plantilla.Attributes.Add(new Attribute("Valor"));
            plantilla.Attributes.Add(new Attribute("IsEditable"));
            plantilla.Attributes.Add(new Attribute("EstatusPoliza"));
            plantilla.Attributes.Add(new Attribute("TipoPlantilla"));
            plantilla.Attributes.Add(new Attribute("Ubicacion"));
            plantilla.Attributes.Add(new Attribute("CentroDeBeneficio"));
            plantilla.Attributes.Add(new Attribute("Propietario"));
            plantilla.Attributes.Add(new Attribute("Ramo"));
            plantilla.Attributes.Add(new Attribute("Aseguradora"));
            plantilla.Attributes.Add(new Attribute("SubRamo"));
            plantilla.Attributes.Add(new Attribute("Flotilla"));
            #endregion
            plantilla.Keys.Add(new Key("Id", _plantillaId, "int"));

            return SiteFunctions.GetEntity(plantilla, out errMsg);
        }

        private Entity searchSiniestro(string _siniestroId)
        {
            string errMsg = string.Empty;
            Entity siniestro = new Entity("Siniestro");
            #region campos
            siniestro.Attributes.Add(new Attribute("Id"));
            siniestro.Attributes.Add(new Attribute("Numero"));
            siniestro.Attributes.Add(new Attribute("FechaSiniestro"));
            siniestro.Attributes.Add(new Attribute("FechaFiniquito"));
            siniestro.Attributes.Add(new Attribute("FechaReporte"));
            siniestro.Attributes.Add(new Attribute("FechaRecepcionCia"));
            siniestro.Attributes.Add(new Attribute("FechaDoc"));
            siniestro.Attributes.Add(new Attribute("Observaciones"));
            siniestro.Attributes.Add(new Attribute("EstatusPagoSiniestro"));
            siniestro.Attributes.Add(new Attribute("EstatusSiniestro"));
            siniestro.Attributes.Add(new Attribute("BienAsegurado"));
            siniestro.Attributes.Add(new Attribute("Poliza"));
            #endregion
            siniestro.Keys.Add(new Key("Id", _siniestroId, "int"));

            return SiteFunctions.GetEntity(siniestro, out errMsg);
        }

        private Entity searchSiniestroFromCompl(string _complementoId)
        {
            string errMsg = string.Empty;
            Entity complemento = new Entity("ComplementoSiniestro");
            #region campos
            complemento.Attributes.Add(new Attribute("Id"));
            complemento.Attributes.Add(new Attribute("Siniestro"));
            complemento.Attributes.Add(new Attribute("Poliza"));
            #endregion
            complemento.Keys.Add(new Key("Id", _complementoId, "int"));

            complemento = SiteFunctions.GetEntity(complemento, out errMsg);
            if (complemento != null)
            {
                return this.searchSiniestro(complemento.getAttrValueByName("Siniestro"));
            }

            return new Entity("Siniestro");
        }

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad ComplementoSiniestro y la plantilla dadas.
        /// </summary>
        /// <param name="complementoId">ComplementoSiniestro para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        public string GeneraCartaComplementoSin(int complementoId, int plantillaId, string userId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            var siniestro = this.searchSiniestroFromCompl(complementoId.ToString());

            GeneraCartaSiniestroStrategy genCartaSiniestroStrategy = new GeneraCartaSiniestroStrategy();

            return genCartaSiniestroStrategy.GeneraCartaComplemento(siniestro, plantilla, usuarioFirmado);
        }

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad SolicitudMensajeria y la plantilla dadas.
        /// </summary>
        /// <param name="id">SolicitudMensajeria para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        /*public string GeneraCartaSolicitudMensajeria(int id, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            var plantilla = PlantillaRepository.Get(plantillaId);
            var carta = SolicitudMensajeriaRepository.Get(id);

            var webPath = ConfigurationManager.AppSettings.Get("barcode.imgs.path");
            var physicalPath = HttpContext.Current.Server.MapPath(webPath);
            physicalPath = physicalPath.Replace("Service\\", "");
            Barcode.GenerateBarCode("" + id, physicalPath);
            carta.BarcodeFile = webPath + "mensajeria_" + id + ".png";

            return new GeneraCartaSolicitudMsgStretegy().GeneraCarta(carta, plantilla, usuarioFirmado);
        }*/

        /// <summary>
        /// Guarda la CartaSiniestro editada por el usuario.
        /// </summary>
        /// <param name="siniestroId">Identificador del sisniestro al cual pertence la carta.</param>
        /// <param name="cartaId"></param>
        /// <param name="nombre">Nombre de la carta.</param>
        /// <param name="desc">Descripcion de la carta.</param>
        /// <param name="contenido">Contenido HTML de la carta.</param>
        /// <param name="plantillaId">identificador de la plantilla de la carta</param>
        /// <returns>Devuelve el identificador generado por el sistema.</returns>
        /*
        public Int64 GuardaCartaSiniestro(int siniestroId, int cartaId, string nombre, string desc, string contenido, int plantillaId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Carta carta;
            if (cartaId == 0)
            {
                
                Entity plantilla = this.searchPlantilla(plantillaId.ToString());
                Entity siniestro = this.searchSiniestro(siniestroId.ToString());
                carta = new CartaSiniestro
                {
                    Siniestro = siniestro,
                    Id = 0,
                    Nombre = nombre,
                    Plantilla = plantilla,
                    Valor = contenido,
                    Descripcion = desc,
                    UsuarioAdd = usuarioFirmado,
                    Estatus = EstatusEnum.Activo,
                    FechaAdd = DateTime.Now,
                    FechaUMod = DateTime.Now,
                    Impresa = false,
                    UsuarioUMod = usuarioFirmado
                };
                base.Save(carta);
            }
            else
            {
                carta = Repository.Get(cartaId);
                carta.Nombre = nombre;
                carta.Descripcion = desc;
                carta.Valor = contenido;
                base.Update(carta);
            }

            return carta.Id;
        }
        */

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad Liquidacion y la plantilla dadas.
        /// </summary>
        /// <param name="liqId">Liquidacion para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        /*public string GeneraCartaLiquidacion(int liqId, int plantillaId, string userId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            var liq = LiquidacionRepository.Get(liqId);
            return new GeneraCartaLiquidacionStrategy().GeneraCarta(liq, plantilla, usuarioFirmado);
        }*/

        

        

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad SolicitudPago y la plantilla dadas.
        /// </summary>
        /// <param name="solPagoId">SolicitudPago para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        /*public string GeneraCartaSolPago(int solPagoId, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            var plantilla = PlantillaRepository.Get(plantillaId);
            var solicitudPago = SolPagoRepository.Get(solPagoId);
            return new GeneraCartaSolicitudPagoStrategy().GeneraCarta(solicitudPago, plantilla, usuarioFirmado);
        }*/

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad SolicitudPago y la plantilla dadas.
        /// </summary>
        /// <param name="solVacacionesId">SolicitudVacaciones para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        /*public string GeneraCartaSolVacaciones(int solVacacionesId, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            var plantilla = PlantillaRepository.Get(plantillaId);
            var solicitudVacaciones = SolVacacionesRepository.Get(solVacacionesId);
            return new GeneraCartaSolicitudVacacionesStrategy().GeneraCarta(solicitudVacaciones, plantilla, usuarioFirmado);
        }*/

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad Liquidacion y la plantilla dadas.
        /// </summary>
        /// <param name="reciboId">Liquidacion para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        /*public string GeneraCartaRecibo(int reciboId, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            var plantilla = PlantillaRepository.Get(plantillaId);
            var recibo = ReciboRepository.Get(reciboId);
            return new GeneraCartaReciboStrategy().GeneraCarta(recibo, plantilla, usuarioFirmado);
        }*/

        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad Poliza y la plantilla dadas.
        /// </summary>
        /// <param name="polizaId">Poliza para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        public string GeneraCartaPoliza(int polizaId, int plantillaId, string userId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            Entity poliza = this.searchPoliza(polizaId.ToString());

            

            return new GeneraCartaPolizaStrategy().GeneraCarta(poliza, plantilla, usuarioFirmado);
        }

        public string GeneraCartaRecibo(int reciboId, int plantillaId,string userId,string TramiteId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            Entity poliza = this.searchPoliza(TramiteId.ToString());
            Entity Endoso  = null;
            if (poliza == null)
            {
                Endoso = this.searchEndoso(TramiteId.ToString());
                if (Endoso != null)
                {
                    poliza = this.searchPoliza(Endoso.getAttrValueByName("Poliza"));
                }

            }
            
            Entity recibo = this.searchRecibo(reciboId.ToString());
            return new GeneraCartaReciboStrategy().GeneraCarta(recibo,poliza,Endoso, plantilla, usuarioFirmado);
            return string.Empty;
        }

        private Entity searchPoliza(string _polizaId)
        {
            string errMsg = string.Empty;
            Entity poliza = new Entity("Poliza");
            #region campos
            poliza.Attributes.Add(new Attribute("Id"));
            poliza.Attributes.Add(new Attribute("Renovable"));
            poliza.Attributes.Add(new Attribute("SobreComisionPct"));
            poliza.Attributes.Add(new Attribute("SobreComisionImporte"));
            poliza.Attributes.Add(new Attribute("BonoPct"));
            poliza.Attributes.Add(new Attribute("BonoImporte"));
            poliza.Attributes.Add(new Attribute("Descuento"));
            poliza.Attributes.Add(new Attribute("EstatusPoliza"));
            poliza.Attributes.Add(new Attribute("ImpuestoImporte"));
            poliza.Attributes.Add(new Attribute("EstatusCobranza"));
            poliza.Attributes.Add(new Attribute("PolizaOriginal"));
            poliza.Attributes.Add(new Attribute("PolizaNueva"));
            poliza.Attributes.Add(new Attribute("Contratante"));
            poliza.Attributes.Add(new Attribute("Impuesto"));
            poliza.Attributes.Add(new Attribute("Moneda"));
            poliza.Attributes.Add(new Attribute("Aseguradora"));
            poliza.Attributes.Add(new Attribute("Agente"));
            poliza.Attributes.Add(new Attribute("Ramo"));
            poliza.Attributes.Add(new Attribute("Ejecutivo"));
            poliza.Attributes.Add(new Attribute("SubRamo"));
            poliza.Attributes.Add(new Attribute("DireccionCobro"));
            poliza.Attributes.Add(new Attribute("DireccionFiscal"));
            poliza.Attributes.Add(new Attribute("DireccionEnvio"));
            poliza.Attributes.Add(new Attribute("EjecutivoAseguradora"));
            poliza.Attributes.Add(new Attribute("Supervisor"));
            poliza.Attributes.Add(new Attribute("Director"));
            poliza.Attributes.Add(new Attribute("Titular"));
            poliza.Attributes.Add(new Attribute("CoTitular"));
            poliza.Attributes.Add(new Attribute("Grupo"));
            poliza.Attributes.Add(new Attribute("Negociacion"));
            poliza.Attributes.Add(new Attribute("Calendario"));
            poliza.Attributes.Add(new Attribute("NumeroCobranza"));
            poliza.Attributes.Add(new Attribute("OcultaCaratula"));
            //poliza.Attributes.Add(new Attribute("Caratula"));
            //poliza.Attributes.Add(new Attribute("NumeroCaratula"));
            #endregion
            poliza.Keys.Add(new Key("Id", _polizaId, "int"));

            return SiteFunctions.GetEntity(poliza, out errMsg);
        }

        /// <summary>
        /// Guarda la CartaPoliza editada por el usuario.
        /// </summary>
        /// <param name="polizaId">Identificador del endoso al cual pertence la carta.</param>
        /// <param name="cartaId"></param>
        /// <param name="nombre">Nombre de la carta.</param>
        /// <param name="desc">Descripcion de la carta.</param>
        /// <param name="contenido">Contenido HTML de la carta.</param>
        /// <param name="plantillaId">identificador de la plantilla de la carta</param>
        /// <returns>Devuelve el identificador generado por el sistema.</returns>
        /*public Int64 GuardaCartaPoliza(int polizaId, int cartaId, string nombre, string desc, string contenido, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            Carta carta;
            if (cartaId == 0)
            {
                var plantilla = PlantillaRepository.Get(plantillaId);
                var poliza = PolizaRepository.Get(polizaId);
                carta = new CartaPoliza
                {
                    Poliza = poliza,
                    Id = 0,
                    Nombre = nombre,
                    Plantilla = plantilla,
                    Valor = contenido,
                    Descripcion = desc,
                    UsuarioAdd = usuarioFirmado,
                    Estatus = EstatusEnum.Activo,
                    FechaAdd = DateTime.Now,
                    FechaUMod = DateTime.Now,
                    Impresa = false,
                    UsuarioUMod = usuarioFirmado
                };
                base.Save(carta);
            }
            else
            {
                carta = Repository.Get(cartaId);
                carta.Nombre = nombre;
                carta.Descripcion = desc;
                carta.Valor = contenido;
                base.Update(carta);
            }

            return carta.Id;
        }
        */
        /// <summary>
        /// Genera la carta o documento en formato HTML a partir de los datos de la entidad Endoso y la plantilla dadas.
        /// </summary>
        /// <param name="endosoId">Endoso para obtener los datos a serializar.</param>
        /// <param name="plantillaId">Identificador de la plantilla.</param>
        /// <returns>Cadena HTML con los datos del siniestro en el formato especificado en la plantilla.</returns>
        public string GeneraCartaEndoso(string endosoId, int plantillaId, string userId)
        {
            Entity usuarioFirmado = SiteFunctions.getUserEntity(userId, new string[] { "Nombre", "Email", "UserName", "Ubicacion", "CentroDeBeneficio", "Propietario", "Puesto", "Perfil", "Telefono", "Extension", "NumeroEmpleado", "FechaIngresoEmpresa", "Observaciones" });
            Entity plantilla = this.searchPlantilla(plantillaId.ToString());
            Entity endoso = this.searchEndoso(endosoId.ToString());
            Entity poliza = this.searchPoliza(endoso.getAttrValueByName("Poliza"));

            GeneraCartaEndosoStrategy GenCartaEndosoStrategy = new GeneraCartaEndosoStrategy();
            return GenCartaEndosoStrategy.GeneraCarta(endoso,poliza, plantilla, usuarioFirmado);
        }

        private Entity searchEndoso(string endosoId)
        {
            string errMsg = string.Empty;
            Entity endososSearch = new Entity("Endoso");
            endososSearch.Attributes.Add(new Models.Attribute("Id"));
            endososSearch.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            endososSearch.Attributes.Add(new Models.Attribute("EstatusEndoso"));
            endososSearch.Attributes.Add(new Models.Attribute("InstruccionesAseguradora"));
            endososSearch.Attributes.Add(new Models.Attribute("Prima"));
            endososSearch.Attributes.Add(new Models.Attribute("Tipo"));
            endososSearch.Attributes.Add(new Models.Attribute("Poliza"));
            endososSearch.Attributes.Add(new Models.Attribute("Impuesto"));
            endososSearch.Attributes.Add(new Models.Attribute("TipoMovimiento"));
            //endososSearch.Attributes.Add(new Models.Attribute("EndosoOriginal"));
            //endososSearch.Attributes.Add(new Models.Attribute("EndosoNuevo"));
            endososSearch.Keys.Add(new Key("Id", endosoId, "int"));

            return SiteFunctions.GetEntity(endososSearch, out errMsg);
        }


        private Entity searchRecibo(string reciboId)
        {
            string errMsg = string.Empty;
            Entity reciboSearch = new Entity("Recibo");
            reciboSearch.Attributes.Add(new Models.Attribute("Id"));
            reciboSearch.Attributes.Add(new Models.Attribute("Tipo"));
            reciboSearch.Attributes.Add(new Models.Attribute("Estatus"));
            reciboSearch.Attributes.Add(new Models.Attribute("Comision"));
            reciboSearch.Attributes.Add(new Models.Attribute("Recargo"));
            reciboSearch.Attributes.Add(new Models.Attribute("ComisionCobranza"));
            reciboSearch.Attributes.Add(new Models.Attribute("ComisionPagada"));
            reciboSearch.Attributes.Add(new Models.Attribute("Cobertura"));
            reciboSearch.Attributes.Add(new Models.Attribute("Expedicion"));
            reciboSearch.Attributes.Add(new Models.Attribute("Vencimiento"));
            reciboSearch.Attributes.Add(new Models.Attribute("Gastos"));
            reciboSearch.Attributes.Add(new Models.Attribute("ImpuestoImporte"));
            reciboSearch.Attributes.Add(new Models.Attribute("Numero"));
            reciboSearch.Attributes.Add(new Models.Attribute("PrimaNeta"));
            reciboSearch.Attributes.Add(new Models.Attribute("Serie"));
            reciboSearch.Attributes.Add(new Models.Attribute("Total"));
            reciboSearch.Attributes.Add(new Models.Attribute("Concepto"));
            reciboSearch.Attributes.Add(new Models.Attribute("ObservacionPago"));
            reciboSearch.Attributes.Add(new Models.Attribute("Propietario"));
            reciboSearch.Attributes.Add(new Models.Attribute("Tramite"));
            reciboSearch.Attributes.Add(new Models.Attribute("Liquidacion"));
            reciboSearch.Attributes.Add(new Models.Attribute("Conciliacion"));
            reciboSearch.Attributes.Add(new Models.Attribute("PagoComision"));
            reciboSearch.Attributes.Add(new Models.Attribute("Inciso"));
            reciboSearch.Attributes.Add(new Models.Attribute("Moneda"));
            reciboSearch.Attributes.Add(new Models.Attribute("Impuesto"));
            reciboSearch.Attributes.Add(new Models.Attribute("FechaPagoComision"));
            reciboSearch.Attributes.Add(new Models.Attribute("TipoCambio"));
            reciboSearch.Attributes.Add(new Models.Attribute("TipoCambioAbono"));
            reciboSearch.Attributes.Add(new Models.Attribute("TipoIngreso"));
            reciboSearch.Attributes.Add(new Models.Attribute("Tramite"));

            reciboSearch.Keys.Add(new Key("Id", reciboId, "int"));

            return SiteFunctions.GetEntity(reciboSearch, out errMsg);
        }
        /// <summary>
        /// Guarda la CartaEndoso editada por el usuario.
        /// </summary>
        /// <param name="endosoId">Identificador del endoso al cual pertence la carta.</param>
        /// <param name="cartaId"></param>
        /// <param name="nombre">Nombre de la carta.</param>
        /// <param name="desc">Descripcion de la carta.</param>
        /// <param name="contenido">Contenido HTML de la carta.</param>
        /// <param name="plantillaId">identificador de la plantilla de la carta</param>
        /// <returns>Devuelve el identificador generado por el sistema.</returns>
        /*public Int64 GuardaCartaEndoso(int endosoId, int cartaId, string nombre, string desc, string contenido, int plantillaId)
        {
            var usuarioFirmado = (Usuario)Context.User;
            Carta carta;
            if (cartaId == 0)
            {
                var plantilla = PlantillaRepository.Get(plantillaId);
                var endoso = EndosoRepository.Get(endosoId);
                carta = new CartaEndoso
                {
                    Endoso = endoso,
                    Id = 0,
                    Nombre = nombre,
                    Plantilla = plantilla,
                    Valor = contenido,
                    Descripcion = desc,
                    UsuarioAdd = usuarioFirmado,
                    Estatus = EstatusEnum.Activo,
                    FechaAdd = DateTime.Now,
                    FechaUMod = DateTime.Now,
                    Impresa = false,
                    UsuarioUMod = usuarioFirmado
                };
                base.Save(carta);
            }
            else
            {
                carta = Repository.Get(cartaId);
                carta.Nombre = nombre;
                carta.Descripcion = desc;
                carta.Valor = contenido;
                base.Update(carta);
            }

            return carta.Id;
        }
        */
        /// <summary>
        /// Ejecuta InputBox.
        /// </summary>
        /// <param name="contenido">Cadena de pregunta del InputBox.</param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <returns>Devuelve cadena introducida por el usuario si DialgoResult es OK.</returns>
        /*
        public bool EnviarPorEmail(string contenido, string to, string subject)
        {

            var usuarioFirmado = (Usuario)Context.User;
            try
            {
                //var message = new MailMessage(usuarioFirmado.Email, to, subject, contenido) {IsBodyHtml = true};
                var service = new EmailService();
                char[] splitter = { ';' };
                service.SendEmail(usuarioFirmado.Email, to.Split(splitter), subject, contenido, Encoding.UTF8, true);
            }
            catch (Exception e)
            {
                throw e;
            }
            return true;
        }
        */
        
    }
}