﻿using CognisoWA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace CognisoWebApp.Models
{
    public class PolizaToXML
    {

        /// <summary>
        /// Permite tomar una instancia de PolizaGmmIndividual para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaGmmIndividual de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        public static void serializaPolizaGmmIndividual(Entity PolizaGmmIndividual, XElement xml)
        {
            xml.Add(
                new XElement("CoaseguroPct", PolizaGmmIndividual.getAttrValueByName("CoaseguroPct").ToString()),
                new XElement("DeduciblePct", PolizaGmmIndividual.getAttrValueByName("DeduciblePct").ToString()),
                new XElement("EsquemaPrimaMinima", PolizaGmmIndividual.getAttrValueByName("EsquemaPrimaMinima").ToString()),
                new XElement("SumaAsegurada", PolizaGmmIndividual.getAttrValueByName("SumaAsegurada").ToString()),
                new XElement("SumaAseguradaSMGM", PolizaGmmIndividual.getAttrValueByName("SumaAseguradaSMGM"))
                );
        }

        /// <summary>
        /// Permite tomar una instancia de PolizaGmmPoblacion para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaGmmPoblacion de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        public static void serializaPolizaGmmPoblacion(Entity PolizaGmmPoblacion, XElement xml)
        {
            xml.Add(
                new XElement("CoaseguroPct", PolizaGmmPoblacion.getAttrValueByName("CoaseguroPct").ToString()),
                new XElement("DeduciblePct", PolizaGmmPoblacion.getAttrValueByName("DeduciblePct").ToString()),
                new XElement("EsquemaPrimaMinima", PolizaGmmPoblacion.getAttrValueByName("EsquemaPrimaMinima").ToString()),
                new XElement("SumaAsegurada", PolizaGmmPoblacion.getAttrValueByName("SumaAsegurada").ToString()),
                new XElement("SumaAseguradaSMGM", PolizaGmmPoblacion.getAttrValueByName("SumaAseguradaSMGM"))
                );
        }

        /// <summary>
        /// Permite tomar una instancia de PolizaVidaPoblacion para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaVidaPoblacion de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        public static XElement serializaPolizaVidaPoblacion(Entity PolizaVidaPoblacion, XElement xml)
        {
            
            xml.Add(
                new XElement("CoaseguroPct", PolizaVidaPoblacion.getAttrValueByName("SumaAsegurada").ToString()),
                new XElement("DeduciblePct", PolizaVidaPoblacion.getAttrValueByName("SumaAseguradaSMGM").ToString()),
                new XElement("EsquemaPrimaMinima", PolizaVidaPoblacion.getAttrValueByName("SumaAseguradaTopada").ToString()),
                new XElement("SumaAsegurada", PolizaVidaPoblacion.getAttrValueByName("SumaAseguradaTopadaSMGM").ToString())
                );
            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de PolizaGeneral para transformarla en formato XML.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaGeneral de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de la PolizaGeneral en formato XML.</returns>
        public static XElement serializaPolizaGeneral(Entity poliza, string Impuesto, Entity Propietario, Entity RamoE, Entity Titular, 
            Entity CoTitular, Entity SubRamo, Entity Slip, string Moneda, Entity DireccionFiscal, string PaisFiscal, string EstadoFiscal, 
            Entity DireccionCobro, string PaisCobro, string EstadoCobro, Entity Agente, string Aseguradora, string CentroDeBeneficio, 
            Entity Contratante, string Supervisor, string Director, string Ejecutivo, string EjecutivoAseguradora, string Ubicacion,
            string UsuarioAdd, string UsuarioUMod, string PolizaOriginal)
        {
            string errmess=string.Empty;
            string FormaPago=string.Empty;
            try
            {

                Entity Tramite = new Entity("Tramite");
                Tramite.Attributes.Add(new Attribute("Folio"));
                Tramite.Attributes.Add(new Attribute("FormaPago"));
                Tramite.Keys.Add(new Key("Id", poliza.getAttrValueByName("Id"), "int"));
                Tramite = SiteFunctions.GetEntity(Tramite, out errmess);
                if (Tramite == null)
                {
                    Tramite = new Entity();
                }
                else
                {
                    int value = 0;
                    int.TryParse(Tramite.getAttrValueByName("FormaPago"), out value);
                    var enumDisplayStatus = (FormaPagoEnum)value;
                    FormaPago = enumDisplayStatus.ToString();


                }

                var slip = HttpUtility.HtmlEncode(Slip.getAttrValueByName("Descripcion"));
                var polizaXML = new XElement("Poliza",
                                             new XElement("Agente",
                                                 new XElement("Clave", Agente.getAttrValueByName("Clave")),
                                                 new XElement("Nombre", Agente.getAttrValueByName("Nombre"))),
                                             new XElement("Aseguradora", Aseguradora),
                                             new XElement("Folio", Tramite.getAttrValueByName("Folio")),
                                             new XElement("CentroDeBeneficio", CentroDeBeneficio),
                                             new XElement("ComisionImporte", poliza.getAttrValueByName("ComisionImporte") != string.Empty ?Convert.ToDecimal(poliza.getAttrValueByName("ComisionImporte")).ToString("C"):"0.00"),
                                             new XElement("Contratante", Contratante.getAttrValueByName("NombreCompleto") + (Contratante.getAttrValueByName("Rfc") != null ? " - RFC: " + Contratante.getAttrValueByName("Rfc") : "")),
                                             new XElement("ComisionPct", poliza.getAttrValueByName("ComisionPct")),
                                             new XElement("Derechos", poliza.getAttrValueByName("Derechos") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("Derechos")).ToString("C") : "0.00"),
                                             new XElement("NumeroCobranza", poliza.getAttrValueByName("NumeroCobranza")),
                                             new XElement("DireccionCobro",
                                                          new XElement("Calle", DireccionCobro.getAttrValueByName("Calle")),
                                                          new XElement("NumExterior", DireccionCobro.getAttrValueByName("NumExterior")),
                                                          new XElement("NumInterior", DireccionCobro.getAttrValueByName("NumInterior")),
                                                          new XElement("Colonia", DireccionCobro.getAttrValueByName("Colonia")),
                                                          new XElement("Delegacion", DireccionCobro.getAttrValueByName("Delegacion")),
                                                          new XElement("Estado", EstadoCobro),
                                                          new XElement("Cp", DireccionCobro.getAttrValueByName("Cp")),
                                                          new XElement("Pais", PaisCobro)
                                                 ),
                                             new XElement("DireccionFiscal",
                                                          new XElement("Calle", DireccionFiscal.getAttrValueByName("Calle")),
                                                          new XElement("NumExterior", DireccionFiscal.getAttrValueByName("NumExterior")),
                                                          new XElement("NumInterior", DireccionFiscal.getAttrValueByName("NumInterior")),
                                                          new XElement("Colonia", DireccionFiscal.getAttrValueByName("Colonia")),
                                                          new XElement("Delegacion", DireccionFiscal.getAttrValueByName("Delegacion")),
                                                          new XElement("Estado", EstadoFiscal),
                                                          new XElement("Cp", DireccionFiscal.getAttrValueByName("Cp")),
                                                          new XElement("Pais", PaisFiscal)
                                                 ),
                                             new XElement("DireccionEnvio",
                                                          new XElement("Calle", DireccionFiscal.getAttrValueByName("Calle")),
                                                          new XElement("NumExterior", DireccionFiscal.getAttrValueByName("NumExterior")),
                                                          new XElement("NumInterior", DireccionFiscal.getAttrValueByName("NumInterior")),
                                                          new XElement("Colonia", DireccionFiscal.getAttrValueByName("Colonia")),
                                                          new XElement("Delegacion", DireccionFiscal.getAttrValueByName("Delegacion")),
                                                          new XElement("Estado", EstadoFiscal),
                                                          new XElement("Cp", DireccionFiscal.getAttrValueByName("Cp")),
                                                          new XElement("Pais", PaisFiscal)
                                                 ),
                                             new XElement("EstatusPoliza", poliza.getAttrValueByName("EstatusPoliza")),
                                             new XElement("FechaAdd", poliza.getAttrValueByName("FechaAdd") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("FechaAdd")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("FechaEmision", poliza.getAttrValueByName("FechaEmision") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("FechaEmision")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("FechaEnvio", poliza.getAttrValueByName("FechaEnvio") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("FechaEnvio")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("FechaUMod", poliza.getAttrValueByName("FechaUMod") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("FechaUMod")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("FormaPago", FormaPago),
                                             new XElement("Gastos", poliza.getAttrValueByName("Gastos") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("Gastos")).ToString("C") : "0.00"),
                                             new XElement("Impuesto", Impuesto),
                                             new XElement("ImpuestoImporte", poliza.getAttrValueByName("ImpuestoImporte") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("ImpuestoImporte")).ToString("C") : "0.00"),
                                             new XElement("Moneda", Moneda),
                                             new XElement("Neto", poliza.getAttrValueByName("Neto") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("Neto")).ToString("C") : "0.00"),
                                             new XElement("Prima", poliza.getAttrValueByName("Prima") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("Prima")).ToString("C") : "0.00"),
                                             new XElement("Propietario", Propietario.getAttrValueByName("Nombre")),
                                             new XElement("Ramo", new XElement("Clave", RamoE.getAttrValueByName("Clave")),
                                                          new XElement("Nombre", RamoE.getAttrValueByName("Nombre"))),
                                             new XElement("SubRamo", new XElement("Clave", SubRamo.getAttrValueByName("Clave")),
                                                          new XElement("Nombre", SubRamo.getAttrValueByName("Nombre"))),
                                             new XElement("RecargosImporte", poliza.getAttrValueByName("RecargosImporte") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("RecargosImporte")).ToString("C") : "0.00"),
                                             new XElement("RecargosPct", poliza.getAttrValueByName("RecargosPct")),
                                             slip != null ? new XElement("Slip", slip) : new XElement("Slip"),
                                             Titular != null ? new XElement("Titular", Titular.getAttrValueByName("NombreCompleto")) : new XElement("Titular", ""),
                                             CoTitular != null ? new XElement("CoTitular", CoTitular.getAttrValueByName("NombreCompleto")) : new XElement("CoTitular", ""),
                                             new XElement("Supervisor", Supervisor),
                                             new XElement("Director", Director),
                                             new XElement("Ejecutivo", Ejecutivo),
                                             new XElement("EjecutivoAseguradora", EjecutivoAseguradora),
                                             new XElement("Total", poliza.getAttrValueByName("Total") != string.Empty ? Convert.ToDecimal(poliza.getAttrValueByName("Total")).ToString("C") : "0.00"),
                                             new XElement("Ubicacion", Ubicacion),
                                             new XElement("UsuarioAdd", UsuarioAdd),
                                             new XElement("UsuarioUMod", UsuarioUMod),
                                             new XElement("VigenciaInicio", poliza.getAttrValueByName("VigenciaInicio") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("VigenciaInicio")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("VigenciaFin", poliza.getAttrValueByName("VigenciaFin") != string.Empty ? DateTime.Parse(poliza.getAttrValueByName("VigenciaFin")).ToString("dd/MM/yyyy") : string.Empty),
                                             new XElement("Ejecutivo", Ejecutivo),
                                             new XElement("PolizaOriginal", PolizaOriginal != null ? new XElement("Folio", PolizaOriginal) : new XElement("Folio", ""))
                                             );
                return polizaXML;
            }
            catch (Exception ex)
            {
                return new XElement("Poliza");
            }
            
        }

        /// <summary>
        /// Permite tomar una instancia de BienAsegurado para transformarla en formato XML.
        /// </summary>
        /// <param name="movimiento">Instancia de BienAsegurado de la cual se tomaran los datos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de la factura en formato XML.</returns>
        public static XElement serializaMovimiento(Entity movimiento, string CentroDeBeneficio, string Propietario, string Ubicacion, string UsuarioAdd, string UsuarioUMod, string beneficiarioPref)
        {
            var xml = new XElement("Movimiento",
                                    new XElement("CentroDeBeneficio",
                                                 CentroDeBeneficio),
                                    new XElement("Estatus", movimiento.getAttrValueByName("Estatus")),
                                    new XElement("FechaAdd", movimiento.getAttrValueByName("FechaAdd") != string.Empty ? DateTime.Parse(movimiento.getAttrValueByName("FechaAdd")).ToString("dd/MM/yyyy"):string.Empty),
                                    new XElement("FechaUMod",movimiento.getAttrValueByName("FechaUMod") != string.Empty ? DateTime.Parse(movimiento.getAttrValueByName("FechaUMod")).ToString("dd/MM/yyyy"):string.Empty),
                                    new XElement("Id", movimiento.getAttrValueByName("Id")),
                                    new XElement("Permiso", movimiento.getAttrValueByName("Permiso")),
                                    new XElement("Prima", movimiento.getAttrValueByName("Prima") != string.Empty ? Convert.ToDecimal(movimiento.getAttrValueByName("Prima")).ToString("C") : "0.00"),
                                    new XElement("Propietario", Propietario),
                                    new XElement("Ubicacion", Ubicacion),
                                    new XElement("UsuarioAdd", UsuarioAdd),
                                    new XElement("UsuarioUMod", UsuarioUMod),
                                    new XElement("Inciso", movimiento.getAttrValueByName("Inciso") != null ? (movimiento.getAttrValueByName("Inciso").IndexOf("-") == -1 ? movimiento.getAttrValueByName("Inciso") : movimiento.getAttrValueByName("Inciso").Substring(0, movimiento.getAttrValueByName("Inciso").IndexOf("-"))) : "")
                       );

            #region Busca auto
            Entity Auto = new Entity("Auto");
            Auto.Attributes.Add(new Attribute("Id"));
            Auto.Attributes.Add(new Attribute("Serie"));
            Auto.Attributes.Add(new Attribute("Motor"));
            Auto.Attributes.Add(new Attribute("Placas"));
            Auto.Attributes.Add(new Attribute("ConductorHabitual"));
            Auto.Attributes.Add(new Attribute("Modelo"));
            Auto.Attributes.Add(new Attribute("Version"));
            Auto.Attributes.Add(new Attribute("Poliza"));
            Auto.Attributes.Add(new Attribute("Marca"));
            Auto.Attributes.Add(new Attribute("CoberturaAuto"));
            Auto.Keys.Add(new Key("Id", movimiento.getAttrValueByName("Id"), "int" ));
            #endregion

            Entity res = new Entity();
            string errMsg = string.Empty;

            res = SiteFunctions.GetEntity(res, out errMsg);

            if (res != null)
            {
                Entity marca = new Entity("Marca");
                marca.Attributes.Add(new Attribute("Descripcion"));
                marca.Keys.Add(new Key("Id", res.getAttrValueByName("Marca"), "int"));

                marca = SiteFunctions.GetEntity(marca, out errMsg);

                string marcaDescr = marca.Attributes.Count>0? marca.getAttrValueByName("Descripcion"):string.Empty;

                serializaAuto(res, marcaDescr, xml);
            }
            else
            { 
                #region busca certificadoGMM
                Entity certGMM = new Entity("CertificadoGmm");
                certGMM.Attributes.Add( new Attribute("Id"));
                certGMM.Attributes.Add( new Attribute("Titular"));
                certGMM.Keys.Add(new Key("Id", movimiento.getAttrValueByName("Id"), "int"));

                res = SiteFunctions.GetEntity(certGMM, out errMsg);

                if (res != null)
                {
                    Entity cert = new Entity("Certificado");
                    cert.Attributes.Add(new Attribute("Id"));
                    cert.Attributes.Add(new Attribute("Nombre"));
                    cert.Attributes.Add(new Attribute("Antiguedad"));
                    cert.Attributes.Add(new Attribute("ApellidoPaterno"));
                    cert.Attributes.Add(new Attribute("ApellidoMaterno"));
                    cert.Attributes.Add(new Attribute("FechaNacimiento"));
                    cert.Attributes.Add(new Attribute("Sexo"));
                    cert.Attributes.Add(new Attribute("Tipo"));
                    cert.Attributes.Add(new Attribute("Titular"));
                    cert.Attributes.Add(new Attribute("Parentesco"));
                    cert.Keys.Add(new Key("Id", res.getAttrValueByName("Id"), "int"));

                    cert = SiteFunctions.GetEntity(cert, out errMsg);
                    serlializaCertificadoGmm(cert, xml, new List<Entity>(), CentroDeBeneficio, Propietario, Ubicacion, UsuarioAdd, UsuarioUMod, beneficiarioPref);
                }
                #endregion
                else
                {
                    #region busca certificadoVida
                    Entity certVida = new Entity("CertificadoVida");
                    certVida.Attributes.Add( new Attribute("Id"));
                    certVida.Keys.Add(new Key("Id", movimiento.getAttrValueByName("Id"), "int"));

                    res = SiteFunctions.GetEntity(certVida, out errMsg);

                    if (res != null)
                    {
                        Entity cert = new Entity("Certificado");
                        cert.Attributes.Add(new Attribute("Id"));
                        cert.Attributes.Add(new Attribute("Nombre"));
                        cert.Attributes.Add(new Attribute("Antiguedad"));
                        cert.Attributes.Add(new Attribute("ApellidoPaterno"));
                        cert.Attributes.Add(new Attribute("ApellidoMaterno"));
                        cert.Attributes.Add(new Attribute("FechaNacimiento"));
                        cert.Attributes.Add(new Attribute("Sexo"));
                        cert.Attributes.Add(new Attribute("Tipo"));
                        cert.Attributes.Add(new Attribute("Titular"));
                        cert.Attributes.Add(new Attribute("Parentesco"));
                        cert.Keys.Add(new Key("Id", res.getAttrValueByName("Id"), "int"));

                        cert = SiteFunctions.GetEntity(cert, out errMsg);
                        serlializaCertificadoVida(cert, xml);
                    }
                    #endregion
                    else
                    {
                        #region Ubicacion Asegurada
                        Entity ubicacion = new Entity("UbicacionAsegurada");
                        ubicacion.Attributes.Add(new Attribute("Id"));
                        ubicacion.Attributes.Add(new Attribute("Rfc"));
                        ubicacion.Attributes.Add(new Attribute("SumaAsegurada"));
                        ubicacion.Attributes.Add(new Attribute("PrimaPct"));
                        ubicacion.Attributes.Add(new Attribute("Direccion"));
                        ubicacion.Keys.Add(new Key("Id", movimiento.getAttrValueByName("Id"), "int"));

                        ubicacion = SiteFunctions.GetEntity(ubicacion, out errMsg);
                        if (ubicacion != null)
                        {
                            string edo = string.Empty;
                            string pais = string.Empty;

                            Entity direccion = new Entity("Direccion");
                            direccion.Attributes.Add(new Attribute("Id"));
                            direccion.Attributes.Add(new Attribute("Tipo"));
                            direccion.Attributes.Add(new Attribute("Calle"));
                            direccion.Attributes.Add(new Attribute("Colonia"));
                            direccion.Attributes.Add(new Attribute("Cp"));
                            direccion.Attributes.Add(new Attribute("Delegacion"));
                            direccion.Attributes.Add(new Attribute("Nombre"));
                            direccion.Attributes.Add(new Attribute("NumExterior"));
                            direccion.Attributes.Add(new Attribute("NumInterior"));
                            direccion.Attributes.Add(new Attribute("Cliente"));
                            direccion.Attributes.Add(new Attribute("Estado"));
                            direccion.Attributes.Add(new Attribute("Pais"));
                            direccion.Keys.Add(new Key("Id", ubicacion.getAttrValueByName("Direccion"), "int"));

                            direccion = SiteFunctions.GetEntity(direccion, out errMsg);

                            if (direccion != null)
                            {
                                Entity Estado = new Entity("Estado");
                                Estado.Attributes.Add(new Attribute("Clave"));
                                Estado.Attributes.Add(new Attribute("Nombre"));
                                Estado.Keys.Add(new Key("Id", direccion.getAttrValueByName("Estado"), "int"));

                                Estado = SiteFunctions.GetEntity(Estado, out errMsg);
                                if(Estado.Attributes.Count>0)
                                {
                                    edo = Estado.getAttrValueByName("Nombre");
                                }

                                Entity Pais = new Entity("Pais");
                                Pais.Attributes.Add(new Attribute("Clave"));
                                Pais.Attributes.Add(new Attribute("Nombre"));
                                Pais.Keys.Add(new Key("Id", direccion.getAttrValueByName("Pais"), "int"));

                                Pais = SiteFunctions.GetEntity(Pais, out errMsg);
                                if (Pais != null)
                                {
                                    pais = Pais.getAttrValueByName("Nombre");
                                }
                            }

                            serlializaUbicacionAsegurada(ubicacion, direccion, edo, pais, beneficiarioPref, xml);
                        }
                        #endregion
                    }
                }
            }
            return xml;
        }

        /// <summary>
        /// Permite tomar una instancia de CertificadoGmm para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="cert">Instancia de CertificadoGmm de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        private static void serlializaCertificadoGmm(Entity cert, XElement xml, List<Entity> Dependientes, string CentroDeBeneficio, string Propietario, string Ubicacion, string UsuarioAdd, string UsuarioUMod, string beneficiarioPref)
        {
            XElement elDependientes = new XElement("Dependientes");
            if(Dependientes.Count>0)
            {
                foreach(Entity dep in Dependientes)
                {
                    //TODO: obtener datos faltantes
                    XElement elReturnMov = serializaMovimiento(dep, CentroDeBeneficio, Propietario, Ubicacion, UsuarioAdd, UsuarioUMod, beneficiarioPref);
                    elDependientes.Add(elReturnMov);
                }
            }

            xml.Add(
                    new XElement("Nombre", cert.getAttrValueByName("Nombre") + " " + cert.getAttrValueByName("ApellidoPaterno") + " " + cert.getAttrValueByName("ApellidoMaterno")),
                    elDependientes
                );
        }

        /// <summary>
        /// Permite tomar una instancia de CertificadoVida para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="cert">Instancia de CertificadoVida de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        private static void serlializaCertificadoVida(Entity cert, XElement xml)
        {
            xml.Add(
                    new XElement("Nombre", cert.getAttrValueByName("Nombre") + " " + cert.getAttrValueByName("ApellidoPaterno") + " " + cert.getAttrValueByName("ApellidoMaterno"))
                );
        }

        /// <summary>
        /// Permite tomar una instancia de UbicacionAsegurada para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="ubi">Instancia de UbicacionAsegurada de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        private static void serlializaUbicacionAsegurada(Entity ubi, Entity Direccion, string Estado, string Pais, string BeneficiarioPreferente, XElement xml)
        {
            xml.Add(
                BeneficiarioPreferente != null ?
                    new XElement("BeneficiarioPreferente", BeneficiarioPreferente)
                : new XElement("BeneficiarioPreferente"),
                    new XElement("Direccion",
                                                      new XElement("Calle", Direccion.getAttrValueByName("Calle")),
                                                      new XElement("NumExterior", Direccion.getAttrValueByName("NumExterior")),
                                                      new XElement("NumInterior", Direccion.getAttrValueByName("NumInterior")),
                                                      new XElement("Colonia", Direccion.getAttrValueByName("Colonia")),
                                                      new XElement("Delegacion", Direccion.getAttrValueByName("Delegacion")),
                                                      new XElement("Estado", Estado),
                                                      new XElement("Cp", Direccion.getAttrValueByName("Cp")),
                                                      new XElement("Pais", Pais)
                                             ),
                    ubi.getAttrValueByName("Identificador") != null ? new XElement("Identificador", ubi.getAttrValueByName("Identificador")) : new XElement("Identificador"),
                    new XElement("Inciso", ubi.getAttrValueByName("Inciso")),
                    new XElement("Prima", ubi.getAttrValueByName("Prima")),
                    new XElement("PrimaPct", ubi.getAttrValueByName("PrimaPct")),
                    new XElement("RazonSocial", ubi.getAttrValueByName("RazonSocial")),
                    new XElement("Rfc", ubi.getAttrValueByName("Rfc")),
                    new XElement("SumaAsegurada", ubi.getAttrValueByName("SumaAsegurada"))

                );
        }

        /// <summary>
        /// Permite tomar una instancia de AutoValueObject para transformarla en formato XML
        /// y agregarla a un Nodo ya existente.
        /// </summary>
        /// <param name="auto">Instancia de AutoValueObject de la cual se tomaran los datos.</param>
        /// <param name="xml">Nodo al que se agregara el resultado de la serializacion</param>
        private static void serializaAuto(Entity auto, string Marca, XElement xml)
        {
            xml.Add(
                new XElement("ConductorHabitual", auto.getAttrValueByName("ConductorHabitual")),
                new XElement("Marca", Marca),
                new XElement("Modelo", auto.getAttrValueByName("Modelo")),
                new XElement("Version", auto.getAttrValueByName("Version")),
                new XElement("Motor", auto.getAttrValueByName("Motor")),
                new XElement("Placas", auto.getAttrValueByName("Placas")),
                new XElement("Serie", auto.getAttrValueByName("Serie"))
                );
        }

        /// <summary>
        /// Permite tomar una instancia de PolizaPool, extraer sus movimientos y transformarlos en formato XML.
        /// </summary>
        /// <param name="poliza">Instancia de PolizaPool de la cual se tomaran los movimientos.</param>
        /// <returns>Devuelve una instancia XElement que contendra la informacion de los Movimientos de la PolizaPool en formato XML.</returns>
        public static XElement serializaMovimientos(Entity poliza, List<Entity> Movimientos)
        {
            XElement elMovimientos = new XElement("Movimientos");
            if (Movimientos.Count > 0)
            {
                foreach (Entity mov in Movimientos)
                {
                    string centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario;

                    PolizaToXML.getFieldRelatedToMov(mov.getAttrValueByName("CentroDeBeneficio"), mov.getAttrValueByName("Propietario")
                        , mov.getAttrValueByName("Ubicacion"), mov.getAttrValueByName("AuditableUsuarioAdd"), mov.getAttrValueByName("AuditableUsuarioUMod"),
                        mov.getAttrValueByName("BeneficiarioPreferente"), out centroBeneficio, out propietario, out ubicacion, out usuarioAdd, out usuarioMod, out beneficiario);
                    //TODO: obtener datos faltantes
                    XElement elReturnMov = serializaMovimiento(mov, centroBeneficio, propietario, ubicacion, usuarioAdd, usuarioMod, beneficiario);
                    elMovimientos.Add(elReturnMov);
                }
            }

            return elMovimientos;
        }

        public static void getFieldRelatedToMov(string _idCentro, string _idPropietario, string _idUbicacion, string _idUsuarioAdd, string _idUsuarioMod, string _idBeneficiario, out string _centroBeneficio, out string _propietario, out string _ubicacion, out string _usuarioAdd, out string _usuarioMod, out string _beneficiario)
        {
            string errMsg = string.Empty;
            _beneficiario = string.Empty;
            _centroBeneficio = string.Empty;
            _propietario = string.Empty;
            _ubicacion = string.Empty;
            _usuarioAdd = string.Empty;
            _usuarioMod = string.Empty;

            Entity centro = new Entity("CentroDeBeneficio");
            centro.Attributes.Add(new Attribute("Nombre"));
            centro.Keys.Add(new Key("Id", _idCentro, "int"));

            centro = SiteFunctions.GetEntity(centro, out errMsg);
            if (centro != null)
            {
                _centroBeneficio = centro.getAttrValueByName("Nombre");
            }

            Entity prop = new Entity("Usuario");
            prop.Attributes.Add(new Attribute("Nombre"));
            prop.Keys.Add(new Key("Id", _idPropietario, "int"));

            prop = SiteFunctions.GetEntity(prop, out errMsg);
            if (prop != null)
            {
                _propietario = prop.getAttrValueByName("Nombre");
            }

            Entity ubi = new Entity("Ubicacion");
            ubi.Attributes.Add(new Attribute("Nombre"));
            ubi.Keys.Add(new Key("Id", _idUbicacion, "int"));

            ubi = SiteFunctions.GetEntity(ubi, out errMsg);
            if (ubi != null)
            {
                _ubicacion = ubi.getAttrValueByName("Nombre");
            }

            Entity userAdd = new Entity("Usuario");
            userAdd.Attributes.Add(new Attribute("Nombre"));
            userAdd.Keys.Add(new Key("Id", _idUsuarioAdd, "int"));

            userAdd = SiteFunctions.GetEntity(userAdd, out errMsg);
            if (userAdd != null)
            {
                _usuarioAdd = userAdd.getAttrValueByName("Nombre");
            }

            Entity userMod = new Entity("Usuario");
            userMod.Attributes.Add(new Attribute("Nombre"));
            userMod.Keys.Add(new Key("Id", _idUsuarioMod, "int"));
            userMod = SiteFunctions.GetEntity(userMod, out errMsg);
            if (userMod != null)
            {
                _usuarioMod = userMod.getAttrValueByName("Nombre");
            }

            Entity beneficiarioPref = new Entity("ContactoSimple");
            beneficiarioPref.Attributes.Add(new Attribute("NombreCompleto"));
            beneficiarioPref.Keys.Add(new Key("Id", _idBeneficiario, "int"));
            beneficiarioPref = SiteFunctions.GetEntity(beneficiarioPref, out errMsg);
            if (beneficiarioPref != null)
            {
                _beneficiario = beneficiarioPref.getAttrValueByName("NombreCompleto");
            }
        }
    }        
}