﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class LiquidacionList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                
                searchForLiquidacion();
            }
        }

        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVLiquidacion.PageIndex = e.NewPageIndex;
            GVLiquidacion.DataBind();
            searchForLiquidacion();
        }

        private void searchForLiquidacion(string searchVal = "")
        {
            JoinEntity joinEntity = new JoinEntity();
            selectJoinEntity selJoin = new selectJoinEntity();
            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity Liquidacion = new Entity("Liquidacion");
            Liquidacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Liquidacion.Attributes.Add(new Models.Attribute("Folio", "", "string"));
            Liquidacion.Attributes.Add(new Models.Attribute("FechaLiquidacion", "", "datetime"));
            Liquidacion.Attributes.Add(new Models.Attribute("Estatus"));

            if (searchVal != string.Empty)
            {
                Liquidacion.Keys.Add(new Key("Id", searchVal, "int"));
            }
            switch (LiquidacionesView.SelectedValue)
            {
                case "2":
                    Liquidacion.Keys.Add(new Key("Estatus", "0", "int",1));
                    break;
                case "3":
                    Liquidacion.Keys.Add(new Key("Estatus", "1", "int",1));
                    break;
                case "4":
                    Liquidacion.Keys.Add(new Key("Estatus", "3", "int",1));
                    break;
                case "5":
                    Liquidacion.Keys.Add(new Key("Estatus", "2", "int",1));
                    break;

            }
            Liquidacion.useAuditable = false;

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Id", "", "int"));
            tramite.Attributes.Add(new Models.Attribute("Folio", "", "int"));
            tramite.useAuditable = false;

            selJoin = new selectJoinEntity("Poliza", 1, "id");

            joinEntity.ChildEntity = tramite;
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList.Add(selJoin);

            Liquidacion.ChildEntities.Add(joinEntity);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;

            selJoin = new selectJoinEntity("Aseguradora", 1, "id");

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = aseguradora;
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList.Add(selJoin);

            Liquidacion.ChildEntities.Add(joinEntity);

            

            List<Entity> listLiq = SiteFunctions.GetValues(Liquidacion, out errmess);
            DataTable dtLiqList = SiteFunctions.trnsformEntityToDT(listLiq);
            GVLiquidacion.DataSource = dtLiqList;
            GVLiquidacion.DataBind();
        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Liquidacion.aspx");
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForLiquidacion(SearchId.Value);
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForLiquidacion(SearchId.Value);
        }

        protected void LiquidacionesView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForLiquidacion(SearchId.Value);
        }
    }
}