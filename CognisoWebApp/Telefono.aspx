﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="Telefono.aspx.cs" Inherits="CognisoWebApp.Telefono" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Teléfono</title>
    <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    
    <script src="/Scripts/jquery-ui.js" type="text/javascript"></script> 
    <script src="/Scripts/popper.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script> 
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <script lang="javascript" type="text/javascript">
    var submit = 0;
    function CheckIsRepeat() {
        if (++submit > 1) {
           return false;
        }
    }
    </script>
</head>  
<body>
    <form id="form1" runat="server">
     
        <div class="container">
        <div class="container" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Teléfono</asp:Label>
            </div>
           
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" />
                    <!--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>

            <div class="container" runat="server" id="GiroGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="container">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtNombre" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblTipo" runat="server" Text="Tipo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="cmbTipo" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblLadaN" runat="server" Text="Lada (nac)"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtLadaN" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblLadaI" runat="server" Text="Lada (int)"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtLadaI" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblNumero" runat="server" Text="Número"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtNumero" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblExt" runat="server" Text="Extensión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtExt" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblContacto" runat="server" Text="Contacto"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:Textbox ID="txtContacto" runat="server" class="form-control"></asp:Textbox>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           </div>     
                      
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="container">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            
                          </div>
                      </div> 
                   
                <asp:HiddenField ID="PaneName" runat="server" />
                        <asp:HiddenField ID="ContactoPM" runat="server" />
                        <asp:HiddenField ID="CustId" runat="server" />
                        <asp:HiddenField ID="TelId" runat="server" />
            </div>
        </div>
    </div>
        </div>
            </div>
  </form>
</body>
</html>  

