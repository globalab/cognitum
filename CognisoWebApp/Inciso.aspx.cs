﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Inciso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                object id = Request.QueryString["id"];
                object poliza = Request.QueryString["polizaid"];
                object endoso = Request.QueryString["EndosoId"];
                if (poliza != null)
                {
                    PolId.Value = poliza.ToString();
                }
                if (endoso != null)
                {
                    EndosoId.Value = endoso.ToString();
                }
                showPanels();
                initCatalog();

                if (id != null)
                {
                    IncisoId.Value = id.ToString();
                    this.searchInciso(id.ToString());
                    //this.DisableControls(Page,false);
                    searchContactos(id.ToString());
                }
                else
                {
                    txtEndosoAlta.Text = EndosoId.Value;
                }
                
                
                
                
            }
        }

        protected void DisableControls(Control parent, bool State)
        {
            foreach (Control c in parent.Controls)
            {
                if (c is DropDownList)
                {
                    ((DropDownList)(c)).Enabled = State;
                }

                DisableControls(c, State);
            }
        }

        private void initCatalog()
        {

            Array itemValues = System.Enum.GetValues(typeof(EstatusBienAseguradoEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusBienAseguradoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatus.Items.Add(item);
            }

            txtEstatus.SelectedValue = "1";
            txtEstatus.Enabled = false;
            itemValues = System.Enum.GetValues(typeof(CoberturaAutoEnum));
            itemNames = System.Enum.GetNames(typeof(CoberturaAutoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtCobertura.Items.Add(item);
            }

            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Poliza = new Entity("Poliza");
            Poliza.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Poliza.Attributes.Add(new CognisoWebApp.Models.Attribute("Contratante"));
            Poliza.Keys.Add(new Key("Id", txtPoliza.Text, "int64"));
            Poliza.useAuditable = false;
            listResult = SiteFunctions.GetValues(Poliza, out errmess);
            if(listResult.Count > 0)
            {
                var Pol = listResult[0];
                Entity Beneficiario = new Entity("ContactoSimple");
                Beneficiario.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Beneficiario.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Beneficiario.Keys.Add(new Key("Cliente", Pol.getAttrValueByName("Contratante").ToString(), "int"));
                Beneficiario.useAuditable = false;
                listResult = SiteFunctions.GetValues(Beneficiario, out errmess);
                txtBeneficiarioPreferente.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtBeneficiarioPreferente.DataValueField = "Id";
                txtBeneficiarioPreferente.DataTextField = "Nombre";
                txtBeneficiarioPreferente.DataBind();

                //Entity Empleado = new Entity("ContactoSimple");
                //Empleado.Attributes.Add(new Models.Attribute("Id"));
                //Empleado.Attributes.Add(new Models.Attribute("NombreCompleto"));
                //Empleado.Keys.Add(new Key("Id"))
            }


            Entity Marca = new Entity("Marca");
            Marca.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Marca.Attributes.Add(new CognisoWebApp.Models.Attribute("Descripcion"));
            Marca.useAuditable = false;
            listResult = SiteFunctions.GetValues(Marca, out errmess);
            txtMarca.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtMarca.DataValueField = "Id";
            txtMarca.DataTextField = "Descripcion";
            txtMarca.DataBind();

            txtMarca.Items.Add(new ListItem("", ""));
            txtMarca.SelectedValue = "";

            

            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.useAuditable = false;
            listResult = SiteFunctions.GetValues(Cliente, out errmess);
            txtEmpleado.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtEmpleado.DataValueField = "Id";
            txtEmpleado.DataTextField = "NombreCompleto";
            txtEmpleado.DataBind();

            txtEmpleado.Items.Add(new ListItem("", ""));
            txtEmpleado.SelectedValue = "";

            Entity Tramite = new Entity("Tramite");
            Tramite.Attributes.Add(new Models.Attribute("Folio"));
            Tramite.Keys.Add(new Key("Id", PolId.Value, "int"));
            listResult = SiteFunctions.GetValues(Tramite, out errmess);
            if (listResult.Count > 0)
            {
                if (listResult[0].getAttrValueByName("Folio") != "")
                {
                    txtPoliza.Text = listResult[0].getAttrValueByName("Folio");
                }
                else
                {
                    txtPoliza.Text = PolId.Value;
                }
            }
        }
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveInciso();
        }

        private void searchInciso(string _id)
        {
            string errMsg = string.Empty, idCliente = string.Empty, idAseguradora = string.Empty, idRamo = string.Empty;
            List<Entity> entities = new List<Entity>();
            Entity Inciso = new Entity("BienAsegurado");
            Inciso.useAuditable = true;
            Inciso.Attributes.Add(new Models.Attribute("Id"));
            Inciso.Attributes.Add(new Models.Attribute("Inciso"));
            Inciso.Attributes.Add(new Models.Attribute("Estatus"));
            Inciso.Attributes.Add(new Models.Attribute("Prima"));
            Inciso.Attributes.Add(new Models.Attribute("Identificador"));
            Inciso.Attributes.Add(new Models.Attribute("Ubicacion"));
            Inciso.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            Inciso.Attributes.Add(new Models.Attribute("Propietario"));
            Inciso.Attributes.Add(new Models.Attribute("Poliza"));
            Inciso.Attributes.Add(new Models.Attribute("EndosoAlta"));
            Inciso.Attributes.Add(new Models.Attribute("EndosoBaja"));
            Inciso.Attributes.Add(new Models.Attribute("GrupoAsegurados"));
            Inciso.Attributes.Add(new Models.Attribute("BeneficiarioPreferente"));
            Inciso.Attributes.Add(new Models.Attribute("Empleado"));
            Inciso.Attributes.Add(new Models.Attribute("TarjetaBancaria"));
            Inciso.Keys.Add(new Models.Key("id", _id, "int"));

            List<Entity> negResult = SiteFunctions.GetValues(Inciso, out errMsg);
            if (negResult.Count > 0)
            {
                Entity inc = negResult[0];

                txtId.Text = inc.getAttrValueByName("Id");
                txtInciso.Text = inc.getAttrValueByName("Inciso");
                txtEstatus.Text = inc.getAttrValueByName("Estatus");
                txtPrima.Text = inc.getAttrValueByName("Prima");
                txtIdentificador.Text = inc.getAttrValueByName("Identificador");
                txtPoliza.Text = inc.getAttrValueByName("Poliza");
                txtEndosoAlta.Text = inc.getAttrValueByName("EndosoAlta");
                txtEndosoBaja.Text = inc.getAttrValueByName("EndosoBaja");
                txtEmpleado.Text = inc.getAttrValueByName("Empleado");
                txtTarjetabancaria.Text = inc.getAttrValueByName("TarjetaBancaria");
                
            }

            Entity Auto = new Entity("Auto");
            Auto.Keys.Add(new Models.Key("id", _id, "int"));
            
            List<Entity> result = SiteFunctions.GetValues(Auto, out errMsg);
            if (result.Count > 0)
            {
                Entity _auto = result[0];
                try
                {
                    txtCobertura.SelectedValue = _auto.getAttrValueByName("CoberturaAuto");
                }
                catch (Exception)
                {
                }
                txtSerie.Text = _auto.getAttrValueByName("Serie");
                txtMotor.Text = _auto.getAttrValueByName("Motor");
                txtPlacas.Text = _auto.getAttrValueByName("Placas");
                try
                {
                    txtConductorHabitual.Text = _auto.getAttrValueByName("ConductorHabitual");
                }
                catch (Exception)
                {
                }
                txtModelo.Text = _auto.getAttrValueByName("Modelo");
                txtVersion.Text = _auto.getAttrValueByName("Version");
                txtPoliza.Text = _auto.getAttrValueByName("Poliza");
                

                try
                {
                    txtMarca.SelectedValue = _auto.getAttrValueByName("Marca");
                }
                catch (Exception)
                {
                }
            
            }

            

        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
        }


        private void SaveInciso()
        {
            object id = IncisoId.Value.Trim() == "" ? null : IncisoId.Value.Trim();
            string IdInciso = string.Empty;
            Method method = Method.POST;
            string errmess = string.Empty;
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            Auditable audi = new Auditable();
            audi.userId = int.Parse(userId);
            audi.opDate = DateTime.Now;
            
            
            if(txtCentroBeneficio.Text == "")
            {
                string cb = SiteFunctions.getUserCentrodeBeneficio(Context.User.Identity.Name);
                txtCentroBeneficio.Text = cb;
            }
            if (txtUbicacion.Text == "")
            {
                string ub = SiteFunctions.getUserUbicacion(Context.User.Identity.Name);
                txtUbicacion.Text = ub;
            }

            Entity Inciso = new Entity("BienAsegurado");
            Inciso.useAuditable = true;
            Inciso.auditable = audi;
            Inciso.Attributes.Add(new Models.Attribute("Inciso",txtInciso.Text,"string"));
            Inciso.Attributes.Add(new Models.Attribute("Estatus",txtEstatus.SelectedValue,"int"));
            Inciso.Attributes.Add(new Models.Attribute("Prima",txtPrima.Text,"float"));
            Inciso.Attributes.Add(new Models.Attribute("Identificador",txtIdentificador.Text,"string"));
            Inciso.Attributes.Add(new Models.Attribute("Ubicacion",txtUbicacion.Text,"int"));
            Inciso.Attributes.Add(new Models.Attribute("CentroDeBeneficio",txtCentroBeneficio.Text,"int"));
            Inciso.Attributes.Add(new Models.Attribute("Propietario",audi.userId.ToString(),"int"));
            Inciso.Attributes.Add(new Models.Attribute("Poliza",PolId.Value,"string"));
            if (txtEndosoAlta.Text != "")
            {
                Inciso.Attributes.Add(new Models.Attribute("EndosoAlta", EndosoId.Value, "int"));
            }
            //Inciso.Attributes.Add(new Models.Attribute("EndosoAlta",txtEndosoAlta.Text,"int"));
            //Inciso.Attributes.Add(new Models.Attribute("EndosoBaja",txtEndosoBaja.Text,"int"));
            if (txtBeneficiarioPreferente.SelectedValue != "")
            {
                Inciso.Attributes.Add(new Models.Attribute("BeneficiarioPreferente", txtBeneficiarioPreferente.SelectedValue, "int"));
            }
            Inciso.Attributes.Add(new Models.Attribute("Empleado",txtEmpleado.SelectedValue,"int"));
            Inciso.Attributes.Add(new Models.Attribute("TarjetaBancaria",txtTarjetabancaria.Text,"int"));
            Inciso.Attributes.Add(new Models.Attribute("Permiso", "2", "int"));
            if(id != null)
            {
                Inciso.Keys.Add(new Key("Id", id.ToString(), "int"));
                method = Method.PUT;
                IdInciso = id.ToString();
            }
            Entity Auto = new Entity("Auto");
            Auto.Attributes.Add(new Models.Attribute("CoberturaAuto",txtCobertura.SelectedValue,"int"));
            Auto.Attributes.Add(new Models.Attribute("Serie",txtSerie.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("Motor",txtMotor.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("Placas",txtPlacas.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("ConductorHabitual", txtConductorHabitual.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("Modelo",txtModelo.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("Version",txtVersion.Text,"string"));
            Auto.Attributes.Add(new Models.Attribute("Poliza", PolId.Value,"string"));
            Auto.Attributes.Add(new Models.Attribute("Marca",txtMarca.SelectedValue,"int"));
            Inciso.useAuditable = true;
            Inciso.auditable = audi;
            if (id != null)
            {
                Auto.Keys.Add(new Key("Id", IncisoId.Value, "int"));

            }


            JoinEntity joinEntity = new JoinEntity();
            joinEntity.ChildEntity = Auto;
            joinEntity.JoinKey = new Models.Attribute("id", "", "int");
            selectJoinEntity autsj = new selectJoinEntity("Id", 1, "Id");
            joinEntity.selectJoinList.Add(autsj);
            joinEntity.JoinType = JoinType.Inner;
            Inciso.ChildEntities.Add(joinEntity);
            
            if (SiteFunctions.SaveEntity(Inciso, method, out errmess))
            {
                IdInciso = IdInciso == string.Empty ? errmess : IdInciso;
                IncisoId.Value = IdInciso;
                DateTime fechaaudit = audi.opDate;
                if (txtFechaAlta.Text.Trim() == "")
                {
                    txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                }
                txtId.Text = IdInciso;
                fechaaudit = audi.opDate;
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", audi.userId.ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errmess);
                if (AuditUsr.Count > 0)
                {
                    if (txtUsuarioAlta.Text == "")
                    {
                        txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                    }
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                showMessage("Inciso guardado correctamente", false);
            }

        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }


        public void searchContactos(string _id)
        {
            string errmess = string.Empty;
            Entity ContactoSimple = new Entity("ContactoSimple");
            ContactoSimple.Attributes.Add(new Models.Attribute("Id"));
            ContactoSimple.Attributes.Add(new Models.Attribute("NombreCompleto"));
            ContactoSimple.Keys.Add(new Key("BienAsegurado",_id,"int"));

            List<Entity> resultEntity = SiteFunctions.GetValues(ContactoSimple, out errmess);
            DataTable Contacts = SiteFunctions.trnsformEntityToDT(resultEntity);
            GVContactos.DataSource = Contacts;
            GVContactos.DataBind();
           
        }

        protected void GVContactos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "DatosAuto";
            GVContactos.PageIndex = e.NewPageIndex;
            GVContactos.DataBind();
            string _id = IncisoId.Value;
            if ( !String.IsNullOrEmpty(_id))
            {
                searchContactos(_id.ToString());
            }
        }
    }
}