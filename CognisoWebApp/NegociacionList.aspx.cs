﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class NegociacionList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                //searchForNegociacion();
            }
        }

        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVNegociacionList.PageIndex = e.NewPageIndex;
            GVNegociacionList.DataBind();
            searchForNegociacion();
        }

        private void searchForNegociacion()
        {

            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity negociacion = new Entity("Negociacion");
            negociacion.Attributes.Add(new Models.Attribute("id", "", "string"));
            negociacion.Attributes.Add(new Models.Attribute("NumeroNegociacion", "", "string"));
            negociacion.Attributes.Add(new Models.Attribute("Cliente", "", "string"));
            negociacion.Attributes.Add(new Models.Attribute("Aseguradora", "", "string"));
            negociacion.Attributes.Add(new Models.Attribute("Ramo", "", "string"));
            negociacion.useAuditable = false;

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto", "", "string"));
            cliente.useAuditable = false;
            entities.Add(cliente);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;
            entities.Add(aseguradora);

            Entity ramo = new Entity("Ramo");
            ramo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ramo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ramo.useAuditable = false;
            entities.Add(ramo);
            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            negociacion.logicalOperator = 2;
            if (SearchId.Value != "" && SearchId.Value != "%%")
            {
                Int64 id = 0;

                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        if(opResult.RetVal.Count > 0)
                        { 
                            Entity entity = opResult.RetVal[0];
                            switch (entity.EntityName.ToLower())
                            {
                                case "cliente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("NombreCompleto").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            negociacion.Keys.Add(new Models.Key("Cliente", nameId, "string", 2));
                                            break;
                                        }

                                    }
                                    break;
                                case "aseguradora":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Clave").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            negociacion.Keys.Add(new Models.Key("Aseguradora", nameId, "string", 2));
                                            break;
                                        }
                                    }
                                    break;
                                case "ramo":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Nombre").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            negociacion.Keys.Add(new Models.Key("Ramo", nameId, "string", 2));
                                            break;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }


                negociacion.Keys.Add(new Models.Key("Id", SearchId.Value.Trim().ToString() + "%", "string", 2));
                negociacion.Keys.Add(new Models.Key("NumeroNegociacion", SearchId.Value.Trim(), "string", 2));

                //negociacion.Keys.Add(new Models.Key("Estatus", "1", "int", 1, 1, "Negociacion"));

            }




            List<Entity> listLeads = SiteFunctions.GetValues(negociacion, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);
            foreach (DataRow row in dtLeadList.Rows)
            {

                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        if(opResult.RetVal.Count > 0)
                        { 
                            Entity entity = opResult.RetVal[0];
                            switch (entity.EntityName.ToLower())
                            {
                                case "cliente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[2].ToString().Trim() == nameId.Trim())
                                        {
                                            row[2] = item.getAttrValueByName("NombreCompleto");
                                            break;
                                        }
                                    }
                                    break;
                                case "aseguradora":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[3].ToString().Trim() == nameId.Trim())
                                        {
                                            row[3] = item.getAttrValueByName("Clave");
                                            break;
                                        }
                                    }
                                    break;
                                case "ramo":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[4].ToString().Trim() == nameId.Trim())
                                        {
                                            row[4] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;
                            }

                        }
                    }
                }
            }

            GVNegociacionList.DataSource = dtLeadList;
            GVNegociacionList.DataBind();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in GVNegociacionList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string id = GVNegociacionList.Rows[row.RowIndex].Cells[1].Text;
                        Response.Redirect(string.Format("Negociacion.aspx?Id={0}", id));
                    }
                }
            }
        }
        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Negociacion.aspx");
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForNegociacion();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForNegociacion();
        }
    }
}