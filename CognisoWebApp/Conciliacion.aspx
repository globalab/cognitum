﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Conciliacion.aspx.cs" Inherits="CognisoWebApp.Conciliacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtLiquidacion.ClientID%>").select2({
                placeholder: "Seleccione una Liquidación",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(function () {
            $('[id*=txtFechaCon]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(function () {
            $('[id*=txtFechaAbono]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $(function () {
            $('[id*=txtAplicacionCia]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });



        $('body').on('click', '#popupReciboLiq', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var page = $(this).attr("data-image")  //get url of link

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: 610,
                width: 800,
                title: "Recibos",
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $("#<%=btnRefreshRecibos.ClientID%>").click();
                }
            });
            $dialog.dialog('open');
        });

            $('body').on('click', '#popupRecibos', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibos",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {


                    }
                });
                $dialog.dialog('open');

            });

            function openpopup(url, title, btnname) {
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.8;

                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: title,
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {
                        $(btnname).click();
                    }
                });
                $dialog.dialog('open');
            }
    </script>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Conciliación</asp:Label></h2>
            </div>
            <hr class="float-left">
            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="ConciliacionList.aspx">Lista de Conciliaciones</a></li>
                    <li class="breadcrumb-item active">Conciliación <%=ConId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" />
                    <asp:Button ID="btnRefreshRecibos" runat="server" CssClass="btn btn-outline-secondary" type="button" Visible="true" OnClick="btnRefreshRecibos_Click"></asp:Button>
                    <asp:Button ID="btnAplicar" runat="server" CssClass="btn btn-outline-secondary" type="button" Visible="true" Text="Aplicar" OnClick="btnAplicar_Click"></asp:Button>
                    <%--<asp:Button ID="btnPrint" OnClientClick="javascript:return false;" data-toggle="modal" data-target="#exampleModal" runat="server" Text="Imprimir" />--%>
                </div>
            </div>

            <div class="containerRT3" runat="server" id="ConciliacionGral">
                <div id="errMess" >

                </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item" id="RecibosDiv" runat="server">
                            <a class="nav-link" id="Recibos-tab" data-toggle="tab" href="#Recibos" role="tab" aria-controls="Recibos" aria-selected="false">Recibos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Totales-tab" data-toggle="tab" href="#Totales" role="tab" aria-controls="Totales" aria-selected="false">Totales</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Observaciones-tab" data-toggle="tab" href="#Observaciones" role="tab" aria-controls="Observaciones" aria-selected="false">Observaciones</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Bitacora-tab" data-toggle="tab" href="#Bitacora" role="tab" aria-controls="Bitacora" aria-selected="false">Bitacora</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT6">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaConciliacion" runat="server" Text="Fecha Conciliación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaCon" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblConcilio" runat="server" Text="Concilio"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtConcilio" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" runat="server" class="form-control" OnSelectedIndexChanged="txtAseguradora_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Aseguradora" runat="server" ErrorMessage="Aseguradora" ForeColor="Red" ControlToValidate="txtAseguradora" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMoneda" runat="server" Text="Moneda"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMoneda" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valMoneda" runat="server" ErrorMessage="Moneda" ForeColor="Red" ControlToValidate="txtMoneda" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFactura" runat="server" Text="Factura"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFactura" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblAplicacionCia" runat="server" Text="Aplicación Cia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtAplicacionCia" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaAbono" runat="server" Text="Fecha Abono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaAbono" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group input-group">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Recibos" role="tabpanel" aria-labelledby="Recibos-tab">
                            <div class="containerRT4">
                                <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col">
                                            <asp:Label ID="lblliquidacion" runat="server" Text="liquidación"></asp:Label>
                                            <div class="form-group input-group">
                                                <asp:DropDownList ID="txtLiquidacion" Style="width: 90%" runat="server" class="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group-append">
                                                <div class="input-group-append">
                                                    &nbsp;<a id="popupReciboLiq" href='#' data-image='searchRecibosCon.aspx?popup=1&conId=<%= ConId.Value %>&liq=<%= txtLiquidacion.SelectedValue %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group-append">
                                                <p>
                                                    <asp:FileUpload ID="fileUploadRecibos" CssClass="btn btn-outline-secondary" runat="server" />
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group-append">
                                                <p>
                                                    <asp:Button ID="BtnImportRecibos" CssClass="btn btn-default btn-sm" Text="Importar Recibos" runat="server" OnClick="BtnImportRecibos_Click" />
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="gvRecibos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a id="popupRecibos" href='#' data-image='Recibo.aspx?Id=<%# Eval("Id") %>'>
                                                            <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="tramiteFolio" HeaderText="Tramite" />
                                                    <asp:BoundField DataField="Concepto" HeaderText="Concepto" />
                                                    <asp:BoundField DataField="numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="Cobertura" HeaderText="Cobertura" />
                                                    <asp:BoundField DataField="Vencimiento" HeaderText="Vencimiento" />
                                                    <asp:BoundField DataField="Total" HeaderText="Total" />
                                                    <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                                                </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                        </div>  
                        <div class="tab-pane fade" id="Totales" role="tabpanel" aria-labelledby="Totales-tab">
                            <div class="containerRT7">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalCRecibos" runat="server" Text="Total C. Recibos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCRecibos" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalCRecibosVal" controltovalidate="txtTotalCRecibos" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalNotas" runat="server" Text="Total C. Notas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalNotas" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalNotasVal" controltovalidate="txtTotalNotas" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotalComision" runat="server" Text="Total Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalComision" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalComisionVal" controltovalidate="txtTotalComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblTotalComisionPagada" runat="server" Text="Total Comisión Pagada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalComisionPagada" runat="server" class="form-control">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtTotalComisionPagadaVal" controltovalidate="txtTotalComisionPagada" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                            </div>

                        </div>  
                        <div class="tab-pane fade" id="Observaciones" role="tabpanel" aria-labelledby="Observaciones-tab">
                            <div class="containerRT8">

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblObservaciones" runat="server" Text="Observaciones"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtObservaciones" TextMode="MultiLine" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Bitacora" role="tabpanel" aria-labelledby="Bitacora-tab">
                            <div class="containerRT9">
                                <div class="row">
                                        <div class="col">
                                            <div class="input-group-append">
                                                <div class="input-group-append">
                                                    &nbsp;<a onclick="openpopup('bitacoraTramite.aspx?popup=1&RelEntityId=<%= ConId.Value %>&Tipo=1','Bitacora','#<%=btnBitacora.ClientID %>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col">
                                            <asp:GridView ID="GVBitacora" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                        <a  onclick="openpopup('BitacoraTramite.aspx?popup=1&id=<%# Eval("Id")%>&RelEntityId=<%# Eval("Conciliacion") %>&Tipo=1','Bitácora','#<%=btnBitacora.ClientID%>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="UsuarioNombre" HeaderText="Usuario" />
                                                    <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                                                    <asp:BoundField DataField="Conciliacion" HeaderText="Conciliacion" Visible="false" />
                                                    </Columns>
                                                <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                            </asp:GridView>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT10">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:TextBox ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:TextBox>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="ConId" runat="server" />
                <asp:HiddenField ID="Estatus" runat="server" />
                <asp:Button ID="btnBitacora" runat="server" OnClick="btnBitacora_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
            </div>
        </div>
    </div>
</asp:Content>
