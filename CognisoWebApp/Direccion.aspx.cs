﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Direccion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                txtDelegacion.Enabled = false;
                txtEstado.Enabled = false;
                txtPais.Enabled = false;
                object CustId = Request.QueryString["custid"];
                object created = Request.QueryString["created"];
                if(CustId != null)
                {
                    CId.Value = CustId.ToString();
                }
                showPanels();
                initCatalog();
                object id = Request.QueryString["id"];

                if (id != null)
                {
                    DirId.Value = id.ToString();
                    this.searchDir(id.ToString());
                    
                }
                if (created != null)
                {
                    this.showMessage("Se guardo correctamente el contacto", false);
                }
                
            }
        }

        private void initCatalog()
        {
            string errmess=string.Empty;
            Array itemValues = System.Enum.GetValues(typeof(TipoDireccionEnum));
            Array itemNames = System.Enum.GetNames(typeof(TipoDireccionEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                cmbTipo.Items.Add(item);
            }

            Entity Cliente = new Entity("Cliente");
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Cliente.Attributes.Add(new CognisoWebApp.Models.Attribute("NombreCompleto"));
            Cliente.Keys.Add(new Key("Id", CId.Value, "int64"));
            Cliente.useAuditable = false;

            List<Entity> listResult = SiteFunctions.GetValues(Cliente, out errmess);
            if (listResult.Count > 0)
            {
                txtCust.Text = listResult[0].getAttrValueByName("NombreCompleto").ToString();
            }

            Entity Estado = new Entity("Estado");
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Estado.useAuditable = false;
            listResult = SiteFunctions.GetValues(Estado, out errmess);
            DataTable Edos = SiteFunctions.trnsformEntityToDT(listResult);
            txtEstado.DataSource = Edos;
            txtEstado.DataValueField = "Id";
            txtEstado.DataTextField = "Nombre";
            txtEstado.DataBind();

        }
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
           
            SaveDir();
            
        }

        private void searchDir(string _id)
        {
            string errMsg = string.Empty, idCliente = string.Empty, idAseguradora = string.Empty, idRamo = string.Empty;
            
            Entity Direccion = new Entity("Direccion");
            Direccion.useAuditable = true;
            Direccion.Keys.Add(new Models.Key("id", _id, "int64"));

            List<Entity> dirResult = SiteFunctions.GetValues(Direccion, out errMsg);
            if (dirResult.Count > 0)
            {
                Entity dir = dirResult[0];

                idCliente = dir.getAttrValueByName("Cliente");
                txtCP.Text = dir.getAttrValueByName("CP");
                settxtCP(txtCP.Text);
                txtName.Text = dir.getAttrValueByName("Nombre");
                cmbTipo.Text = dir.getAttrValueByName("Tipo");
                txtNumeroExt.Text = dir.getAttrValueByName("NumExterior");
                txtNumeroInt.Text = dir.getAttrValueByName("NumInterior");
                txtCalle.Text = dir.getAttrValueByName("Calle");
                CId.Value = idCliente;
                try
                {
                    DateTime fechaaudit = Convert.ToDateTime(dir.getAttrValueByName("FechaAdd"));

                    txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                    fechaaudit = Convert.ToDateTime(dir.getAttrValueByName("FechaUMod"));
                    txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                }
                catch (Exception)
                {

                }
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", dir.getAttrValueByName("UsuarioAdd"), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", dir.getAttrValueByName("UsuarioUMod"), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtId.Text = _id;
            }

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto"));
            cliente.Keys.Add(new Models.Key("id", idCliente, "int"));
            string errmess = string.Empty;
            List<Entity> listResult = SiteFunctions.GetValues(cliente, out errmess);
            if (listResult.Count > 0)
            {
                txtCust.Text = listResult[0].getAttrValueByName("NombreCompleto").ToString();
            }
            
        }

        protected void settxtCP(string zip)
        {
            if (zip != "" && zip.Length == 5)
            {
                
                PaneName.Value = "collapseFour";
                string errmess = "";
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", zip, "string"));
                List<Entity> listResult = SiteFunctions.GetValues(CP, out errmess);
                DataTable Dt = SiteFunctions.trnsformEntityToDT(listResult);
                if (errmess == "" && Dt.Rows.Count > 0)
                {
                    txtColonia.DataSource = Dt;
                    txtColonia.DataTextField = "Colonia";
                    txtColonia.DataValueField = "Colonia";
                    txtColonia.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacion.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstado.Items.FindByValue(txtEstado.SelectedValue).Selected = false;
                        txtEstado.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;
                        txtPais.Text = "México";

                    }
                }


            }


        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
        }

        private void SaveDir()
        {

            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            Entity direccion = new Entity("Direccion");
            Auditable audi = new Auditable();
            audi.userId = userid;
            audi.opDate = DateTime.Now;


            direccion.Attributes.Add(new Models.Attribute("Cliente", CId.Value, "int"));
            direccion.Attributes.Add(new Models.Attribute("Nombre", txtName.Text, "string"));
            direccion.Attributes.Add(new Models.Attribute("Tipo", cmbTipo.SelectedValue, "int"));
            direccion.Attributes.Add(new Models.Attribute("CP", txtCP.Text.Trim(), "string"));
            direccion.Attributes.Add(new Models.Attribute("Calle", txtCalle.Text.Trim(), "string"));
            direccion.Attributes.Add(new Models.Attribute("NumExterior", txtNumeroExt.Text.Trim(), "string"));
            direccion.Attributes.Add(new Models.Attribute("NumInterior", txtNumeroInt.Text.Trim(), "string"));
            direccion.Attributes.Add(new Models.Attribute("Colonia", txtColonia.SelectedItem.Text, "string"));
            direccion.Attributes.Add(new Models.Attribute("Pais", "2412", "int"));
            direccion.Attributes.Add(new Models.Attribute("Estado", txtEstado.Text, "int"));

            direccion.useAuditable = true;

           string errmess = "";
            object id = Request.QueryString["id"];
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                direccion.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);

            }
            direccion.auditable = audi;
            if (SiteFunctions.SaveEntity(direccion, method, out errmess))
            {
                if(id!= null)
                {
                    errmess = id.ToString();
                }
                showMessage("Se guardo correctamente el contacto", false);
                Response.Redirect(string.Format("Direccion.aspx?id={0}&custid={1}&created=1", errmess, CId.Value));
            }
            else
            {
                showMessage(errmess, true);
            }
        }


        

        
        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void txtCP_TextChanged(object sender, EventArgs e)
        {
            if (txtCP.Text != "" && txtCP.Text.Length == 5)
            {
                //txtEstadoFacturacion.Enabled = true;
                //txtPaisFacturacion.Enabled = true;
                //txtDelegacionFacturacion.Enabled = true;
                PaneName.Value = "Direcciones";
                string errmess = "";
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", txtCP.Text, "string"));
                List<Entity> listResult = SiteFunctions.GetValues(CP, out errmess);
                DataTable Dt = SiteFunctions.trnsformEntityToDT(listResult);
                if (errmess == "" && Dt.Rows.Count > 0)
                {

                    txtColonia.DataSource = Dt;
                    txtColonia.DataTextField = "Colonia";
                    txtColonia.DataValueField = "Colonia";
                    txtColonia.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacion.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstado.Items.FindByValue(txtEstado.SelectedValue).Selected = false;
                        txtEstado.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;
                    }

                    txtPais.Text = "México";

                }
                else
                {
                    this.showMessage("El CP. " + txtCP.Text + " no existe favor de validar o darlo de alta.", true);
                    txtCP.Text = string.Empty;
                }

            }
            else
            {
                PaneName.Value = "Direcciones";
                if (txtCP.Text.Length != 5 && txtCP.Text != "")
                {
                    this.showMessage("El CP. debe ser de 5 números.", true);
                    txtCP.Text = string.Empty;
                }
            }

        }
    }
}