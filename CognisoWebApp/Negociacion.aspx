﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Negociacion.aspx.cs" Inherits="CognisoWebApp.Negociacion" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <script src="/Scripts/select2.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCliente.ClientID%>").select2({
                placeholder: "Seleccione un Cliente",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtAseguradora.ClientID%>").select2({
                placeholder: "Seleccione una Aseguradora",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtRamo.ClientID%>").select2({
                placeholder: "Seleccione un Ramo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        function openpopup(url,title,btnname) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: title,
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $(btnname).click();
                }
            });
            $dialog.dialog('open');
        }
    </script>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
        <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Negociación</asp:Label></h2>
            </div>
            <hr class="float-left">
            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="NegociacionList.aspx">Lista de Negociaciones</a></li>
                    <li class="breadcrumb-item active">Negociacion <%=NegId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClick="BtnSave_Click" OnClientClick="return CheckIsRepeat();" />
                     <asp:ImageButton ID="btnAttachment" ImageUrl="/Content/Imgs/clip.png" runat="server" OnClick="BtnAttachment_Click" CausesValidation="false"/>
                    <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" CausesValidation="false" />
                </div>
            </div>

            <div class="containerRT3" runat="server" id="NegociacionGral">
                <div id="errMess" >

                </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Polizas" role="tab" aria-controls="Polizas" aria-selected="false">Polizas</a>
                        </li>
                    
                        <li class="nav-item">
                            <a class="nav-link" id="Contacto-tab" data-toggle="tab" href="#Comentarios" role="tab" aria-controls="Comentarios" aria-selected="false">Comentarios</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">


                            <div class="containerRT4">

                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblCliente" runat="server" Text="Cliente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCliente" runat="server" class="form-control" OnSelectedIndexChanged="txtCliente_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ValidateCustomer" runat="server" ErrorMessage="Cliente" ForeColor="Red" ControlToValidate="txtCliente" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ValidateAseguradora" runat="server" ErrorMessage="Aseguradora" ForeColor="Red" ControlToValidate="txtAseguradora" Text="*"></asp:RequiredFieldValidator>
                                          <!--  <div class="input-group-append">
                                                &nbsp;<a id="modifyAseguradora" href='#' onclick="openpopup('Aseguradora.aspx?id=<%=txtAseguradora.SelectedValue%>','Aseguradora','#<%=btnAseguradora.ClientID%>');" ><img src="/Content/Imgs/edit.png" width="25" height="25" /></a>     
                                                &nbsp;<a id="createAseguradora" href='#' onclick="openpopup('Aseguradora.aspx','Aseguradora','#<%=btnAseguradora.ClientID%>');" ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRamo" runat="server" Text="Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtRamo" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ValidateRamo" runat="server" ErrorMessage="Ramo" ForeColor="Red"  ControlToValidate="txtRamo" Text="*"></asp:RequiredFieldValidator>
                                            <!--<div class="input-group-append">
                                                &nbsp;<a href='#' onclick="openpopup('Ramo.aspx?id=<%=txtRamo.SelectedValue%>','Ramo','#<%=btnRamo.ClientID%>');" ><img src="/Content/Imgs/edit.png" width="25" height="25" /></a>     
                                                &nbsp;<a href='#' onclick="openpopup('Ramo.aspx','Ramo','#<%=btnRamo.ClientID%>');" ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorComision" runat="server" Text="% Comisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorComision" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                            
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorComisionVal" controltovalidate="txtPorComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorBono" runat="server" Text="% Bono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorBono" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorBonoVal" controltovalidate="txtPorBono" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPorSobreComision" runat="server" Text="% Sobrecomisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPorSobreComision" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPorSobreComisionVal" controltovalidate="txtPorSobreComision" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPrimaNetaAprox" runat="server" Text="PNA Aproximada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrimaNetaAprox" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" id="txtPrimaNetaAproxVal" controltovalidate="txtPrimaNetaAprox" validationexpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" errormessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDiasNegociacionEntrega" runat="server" Text="Días negociación de entrega"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDiasNegociacionEntrega" TextMode="Number" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNoNegociacion" runat="server" Text="Número de negociación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNoNegociacion" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtNoNegociacion" runat="server" ErrorMessage="Número de Negociación" ForeColor="Red" ControlToValidate="txtNoNegociacion" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Polizas" role="tabpanel" aria-labelledby="Polizas-tab">
                            <div class="containerRT5">
                                <div class="panel-body" runat="server">
                                    <div class="table-responsive" runat="server">
                                        <asp:GridView ID="GVPolizasList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Contratante" HeaderText="Contratante" />
                                                <asp:BoundField DataField="Agente" HeaderText="Agente" />
                                                <asp:BoundField DataField="Ramo" HeaderText="Ramo" />
                                                <asp:BoundField DataField="Aseguradora" HeaderText="Aseguradora" />
                                                <asp:BoundField DataField="Ejecutivo" HeaderText="Ejecutivo" />
                                                <asp:BoundField DataField="EstatusPoliza" HeaderText="Estatus" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>

                        </div>
                    
                        <div class="tab-pane fade" id="Comentarios" role="tabpanel" aria-labelledby="Comentarios-tab">
                            <div class="containerRT6">

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblComentarios" runat="server" Text="Comentarios"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComentarios" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                          <div class="containerRT7">
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row" >
                                                <div class="col">
                                                    <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                             <div class="row">
                                                <div class="col">
                                                    <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                                    <div class="form-group input-group">
                                                        <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                            <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                              <div class="row">
                                <div class="col">
                                    <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                    <div class="form-group input-group">
                                        <asp:DropDownList ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                               </div>
                          </div>
                      </div> 
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="NegId" runat="server" />
                <asp:HiddenField ID="CustRFC" runat="server" />
                <asp:HiddenField ID="Owner" runat="server" />
                <asp:Button ID="btnAseguradora" runat="server" OnClick="btnAseguradora_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
                <asp:Button ID="btnRamo" runat="server" OnClick="btnRamo_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false"/>
            </div>
        </div>
    </div>
</asp:Content>
