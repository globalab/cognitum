﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class BitacoraTramite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                object id = Request.QueryString["id"];
                object RelEntityid = Request.QueryString["RelEntityId"];
                object Tipo = Request.QueryString["Tipo"];
                if (RelEntityid != null)
                {
                    EntityId.Value = RelEntityid.ToString();
                    TipoBit.Value = Tipo.ToString();
                    initCatalog();
                }
                
                
                
                if (id != null)
                {
                    BitId.Value = id.ToString();
                    this.searchBitacora(id.ToString());
                }
            }
        }

        private void initCatalog()
        {

            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Usuario = new Entity("Usuario");
            Usuario.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Usuario.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Usuario.Keys.Add(new Key("Username", Context.User.Identity.Name, "string"));
            Usuario.useAuditable = false;
            listResult = SiteFunctions.GetValues(Usuario, out errmess);
            txtUsuario.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtUsuario.DataValueField = "Id";
            txtUsuario.DataTextField = "Nombre";
            txtUsuario.DataBind();

            if (TipoBit.Value == "0")
            {
                Entity Tramite = new Entity("Tramite");
                Tramite.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Tramite.Attributes.Add(new CognisoWebApp.Models.Attribute("Folio"));
                Tramite.Keys.Add(new Key("Id", EntityId.Value, "int"));
                Tramite.useAuditable = false;
                listResult = SiteFunctions.GetValues(Tramite, out errmess);
                //txtTramite.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                //txtTramite.DataValueField = "Id";
                //txtTramite.DataTextField = "Id";
                //txtTramite.DataBind();
                string Folio = string.Empty;
                if(listResult.Count > 0)
                {
                    Folio = listResult[0].getAttrValueByName("Folio");
                }
                txtTramite.Text = "[ OT " + EntityId.Value.ToString() + " ] " + Folio;
            }
            if (TipoBit.Value == "1")
            {
                lblTramite.Visible = false;
                txtTramite.Visible = false;
                
            }
            txtFecha.Text = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt");

        }
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveBitacora();
        }

        private void searchBitacora(string _id)
        {
            string errMsg = string.Empty, idCliente = string.Empty, idAseguradora = string.Empty, idRamo = string.Empty;
            List<Entity> entities = new List<Entity>();
            string relatedentity = "BitacoraTramite"; 
            if(TipoBit.Value == "1")
            {
                relatedentity = "BitacoraConciliacion";
            }
            Entity BT = new Entity(relatedentity);
                BT.useAuditable = true;
                BT.Attributes.Add(new Models.Attribute("Id"));
                BT.Attributes.Add(new Models.Attribute("Fecha"));
                BT.Attributes.Add(new Models.Attribute("Observaciones"));
                BT.Attributes.Add(new Models.Attribute("Usuario"));
                    
            
                BT.Keys.Add(new Models.Key("id", _id, "int"));

                List<Entity> listResult = SiteFunctions.GetValues(BT, out errMsg);
                if (listResult.Count > 0)
                {
                    var BitTram = listResult[0];
                    DateTime fecha = Convert.ToDateTime(BitTram.getAttrValueByName("Fecha"));
                    txtFecha.Text = fecha.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                    txtObservaciones.Text = BitTram.getAttrValueByName("Observaciones");
                    listResult = new List<Entity>();

                    Entity Usuario = new Entity("Usuario");
                    Usuario.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                    Usuario.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                    Usuario.Keys.Add(new Key("Id", BitTram.getAttrValueByName("Usuario"), "int64"));
                    Usuario.useAuditable = false;
                    listResult = SiteFunctions.GetValues(Usuario, out errMsg);
                    txtUsuario.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                    txtUsuario.DataValueField = "Id";
                    txtUsuario.DataTextField = "Nombre";
                    txtUsuario.DataBind();

                }
        }

        private void SaveBitacora()
        {
            Method metodo = Method.POST;
            string errmess = String.Empty;
            
            Entity Bitacora  = new Entity("BitacoraTramite");;
            string relatedentity = "Tramite";
            if (TipoBit.Value == "1")
            {
                Bitacora = new Entity("BitacoraConciliacion");
                relatedentity = "Conciliacion";
            }
            Bitacora.Attributes.Add(new Models.Attribute("Fecha", txtFecha.Text, "datetime"));
            Bitacora.Attributes.Add(new Models.Attribute("Observaciones", txtObservaciones.Text, "string"));
            Bitacora.Attributes.Add(new Models.Attribute("Usuario", txtUsuario.SelectedValue, "int"));
            Bitacora.Attributes.Add(new Models.Attribute(relatedentity, EntityId.Value, "int"));
            
            if (BitId.Value != "")
            {
                Bitacora.Keys.Add(new Key("Id", BitId.Value, "int"));
                metodo = Method.PUT;
            }

            if (SiteFunctions.SaveEntity(Bitacora, metodo,out errmess))
            {
                if (BitId.Value != "")
                {
                    BitId.Value = errmess;
                }
                showMessage("Bitacora guardada correctamente", false);
            }
            else
            {
                showMessage(errmess, true);
            }
                    
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

    }
}