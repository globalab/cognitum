﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CognisoWebApp.Models;
using System.Web.Security;
using RestSharp;
using System.Text;

namespace CognisoWebApp
{
    public partial class ProspectosList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                GVLeadList.DataSource = searchForLeads();
                GVLeadList.DataBind();
            }
        }
        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
        protected void GVLeadList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVLeadList.PageIndex = e.NewPageIndex;
            GVLeadList.DataSource = searchForLeads();
            GVLeadList.DataBind();
        }

        private DataTable searchForLeads()
        {
            string errmess = "";
            DataTable returnTable = new DataTable();
            returnTable.Columns.Add("Id");
            returnTable.Columns.Add("RazonSocial");
            returnTable.Columns.Add("RFC");
            returnTable.Columns.Add("Email");
            returnTable.Columns.Add("TipoPersona");
            returnTable.Columns.Add("Estatus");
            Entity lead = new Entity("Prospecto");
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("RazonSocial"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("ApellidoPaterno"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("ApellidoMaterno"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("RFC"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Email"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("TipoPersona"));
            lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus"));
            lead.useAuditable = false;
            lead.logicalOperator = 2;
            if (SearchId.Value != "")
            {
                Int64 id = 0;

                if (Int64.TryParse(SearchId.Value.Replace("%", ""), out id))
                {
                    lead.Keys.Add(new Models.Key("Id", id.ToString() + "%", "string",2));
                }
                else
                {
                    lead.Keys.Add(new Models.Key("Nombre", SearchId.Value, "string"));
                    lead.Keys.Add(new Models.Key("ApellidoPaterno", SearchId.Value, "string",2));
                    lead.Keys.Add(new Models.Key("ApellidoMaterno", SearchId.Value, "string",2));
                    lead.Keys.Add(new Models.Key("RFC", SearchId.Value, "string",2));
                    lead.Keys.Add(new Models.Key("RazonSocial", SearchId.Value, "string", 2));
                }
            }
            string Status = "1";
            switch (ProspectView.SelectedValue)
            {
                case "1":
                    Status = "0";
                    break;
                case "2":
                    Status = "1";
                    break;
                case "3":
                    Status = "2";
                    break;
                case "4":
                    Status = "4";
                    break;
                case "5":
                    Status = "5";
                    break;
                default:
                    break;
            }
            if (Status != "0")
            {
                lead.Keys.Add(new Models.Key("Estatus", Status, "int", 1, 2));
            }
            else
            {
                lead.Keys.Add(new Models.Key("Estatus not ", "99", "int", 1, 2));
            }
         
            List<Entity> listLeads = SiteFunctions.GetValues(lead, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);

            foreach (DataRow row in dtLeadList.Rows)
            {
                DataRow newRow = returnTable.NewRow();
                newRow["Id"] = row["Id"];
                
                newRow["RFC"] = row["RFC"];
                newRow["Email"] = row["Email"];
                newRow["Estatus"] = row["Estatus"];
                if (row["TipoPersona"].ToString() == "1")
                {
                    newRow["TipoPersona"] = "Persona Física";
                    newRow["RazonSocial"] = string.Format("{0} {1} {2}", row["Nombre"].ToString(), row["ApellidoPaterno"].ToString(), row["ApellidoMaterno"].ToString()); 
                }
                else
                {
                    newRow["RazonSocial"] = row["RazonSocial"];
                    newRow["TipoPersona"] = "Persona Moral";
                }
                returnTable.Rows.Add(newRow);
            }
            return returnTable;
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in GVLeadList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string id = GVLeadList.Rows[row.RowIndex].Cells[1].Text;
                        Response.Redirect(string.Format("Prospecto.aspx?Id={0}", id));
                    }
                }
            }
            this.showMessage("Debe Seleccionar un Prospecto.", true);
        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("prospecto.aspx");
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            GVLeadList.DataSource = searchForLeads();
            GVLeadList.DataBind();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            GVLeadList.DataSource = searchForLeads();
            GVLeadList.DataBind();
        }

        protected void ProspectView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            GVLeadList.DataSource = searchForLeads();
            GVLeadList.DataBind();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            bool seleccionado = false;
            string errmess = "";
       
            foreach (GridViewRow row in GVLeadList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                         seleccionado = true;
                    }
                }
            }
            if (seleccionado)
            {
                string confirmValue = Request.Form["confirm_value"];
                if (confirmValue == "Si")
                {
                    foreach (GridViewRow row in GVLeadList.Rows)
                    {
                        CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                        if (chkBox != null)
                        {
                            if (chkBox.Checked)
                            {
                                if (GVLeadList.Rows[row.RowIndex].Cells[7].Text != "Cliente")
                                {
                                    string id = GVLeadList.Rows[row.RowIndex].Cells[1].Text;
                                    string leadId = string.Empty;
                                    Entity lead = new Entity("Prospecto");
                                    lead.Keys.Add(new Key("Id", id, "int64"));
                                    lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus", "99", "string"));
                                    SiteFunctions.SaveEntity(lead, Method.PUT, out errmess);

                                }
                                else
                                {
                                    this.showMessage("No se pueden eliminar prospectos que ya fueron aprobados como clientes.", true);
                                }
                            }
                        }
                    }
                    searchForLeads();
                    GVLeadList.DataSource = searchForLeads();
                    GVLeadList.DataBind();
                }
                
            }
            else
            {
                this.showMessage("Debe Seleccionar un Prospecto.", true);
            }
           
        }
    }
}