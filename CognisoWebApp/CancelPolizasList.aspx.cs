﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class CancelPolizasList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                searchForPolizas();

            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);

            }
        }
        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("poliza.aspx");
        }
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in GVPolizasList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string id = GVPolizasList.Rows[row.RowIndex].Cells[1].Text;
                        Response.Redirect(string.Format("CancelPoliza.aspx?Id={0}", id));
                    }
                }
            }
        }


        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVPolizasList.PageIndex = e.NewPageIndex;
            GVPolizasList.DataBind();
            searchForPolizas();

        }
        private void searchForPolizas()
        {

            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity poliza = new Entity("Poliza");
            poliza.ChildEntities = new List<JoinEntity>();
            switch(ProspectView.SelectedValue)
            {
                case "1":
                    JoinEntity joinAutos = new JoinEntity();
                    joinAutos.ChildEntity = new Entity("PolizaAutoIndividual");
                    joinAutos.JoinKey = new Models.Attribute("Id");
                    selectJoinEntity autossj = new selectJoinEntity("Id",1,"Id");
                    joinAutos.selectJoinList.Add(autossj);
                    poliza.ChildEntities.Add(joinAutos);
                    break;
                case "2":
                    JoinEntity joinVida = new JoinEntity();
                    joinVida.ChildEntity = new Entity("PolizaVidaIndividual");
                    joinVida.JoinKey = new Models.Attribute("Id");
                    joinVida.JoinType = JoinType.Left;
                    selectJoinEntity vidasj = new selectJoinEntity("Id",1,"Id");
                    JoinEntity joinVidaPoblacion = new JoinEntity();
                    joinVidaPoblacion.ChildEntity = new Entity("PolizaVidaPoblacion");
                    joinVidaPoblacion.JoinKey = new Models.Attribute("Id");
                    joinVidaPoblacion.JoinType = JoinType.Left;
                    selectJoinEntity vidapobsj = new selectJoinEntity("Id", 1, "Id");
                    joinVida.selectJoinList.Add(vidasj);
                    joinVidaPoblacion.selectJoinList.Add(vidapobsj);
                    poliza.ChildEntities.Add(joinVida);
                    poliza.ChildEntities.Add(joinVidaPoblacion);
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 1, 1, "PolizaVidaIndividual"));
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 2, 1, "PolizaVidaPoblacion"));

                    break;
                case "3":
                    JoinEntity joinGMM = new JoinEntity();
                    joinGMM.ChildEntity = new Entity("PolizaGmmIndividual");
                    joinGMM.JoinKey = new Models.Attribute("Id");
                    joinGMM.JoinType = JoinType.Left;
                    selectJoinEntity GMMsj = new selectJoinEntity("Id",1,"Id");
                    JoinEntity joinGMMPoblacion = new JoinEntity();
                    joinGMMPoblacion.ChildEntity = new Entity("PolizaGmmPoblacion");
                    joinGMMPoblacion.JoinKey = new Models.Attribute("Id");
                    joinGMMPoblacion.JoinType = JoinType.Left;
                    selectJoinEntity gmmpobsj = new selectJoinEntity("Id", 1, "Id");
                    joinGMMPoblacion.selectJoinList.Add(gmmpobsj);
                    joinGMM.selectJoinList.Add(GMMsj);
                    poliza.ChildEntities.Add(joinGMM);
                    poliza.ChildEntities.Add(joinGMMPoblacion);
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 1, 1, "PolizaGmmIndividual"));
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 2, 1, "PolizaGmmPoblacion"));
                    break;
                case "4":
                    JoinEntity joinDanhos = new JoinEntity();
                    joinDanhos.ChildEntity = new Entity("PolizaDanhosIndividual");
                    joinDanhos.JoinKey = new Models.Attribute("Id");
                    joinDanhos.JoinType = JoinType.Left;
                    selectJoinEntity Danhossj = new selectJoinEntity("Id",1,"Id");
                    JoinEntity joinDanhosPool = new JoinEntity();
                    joinDanhosPool.ChildEntity = new Entity("PolizaDanhosPool");
                    joinDanhosPool.JoinKey = new Models.Attribute("Id");
                    joinDanhosPool.JoinType = JoinType.Left;
                    selectJoinEntity danhospoolsj = new selectJoinEntity("Id", 1, "Id");
                    joinDanhosPool.selectJoinList.Add(danhospoolsj);
                    joinDanhos.selectJoinList.Add(Danhossj);
                    poliza.ChildEntities.Add(joinDanhos);
                    poliza.ChildEntities.Add(joinDanhosPool);
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 1, 1, "PolizaDanhosIndividual"));
                    poliza.Keys.Add(new Key("Id", "Poliza.Id", "int64", 2, 1, "PolizaDanhosPool"));
                    break;
                default:
                    break;
            }
            poliza.Attributes.Add(new Models.Attribute("id", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Contratante", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Agente", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ramo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Aseguradora", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("Ejecutivo", "", "string"));
            poliza.Attributes.Add(new Models.Attribute("EstatusPoliza", "", "string"));
            poliza.Keys.Add(new Models.Key("Estatus", "1", "int",1, 2, "a"));
     

            Entity negociacion = new Entity("Negociacion");
            negociacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
            negociacion.Attributes.Add(new Models.Attribute("NumeroNegociacion", "", "string"));
            negociacion.useAuditable = false;
            entities.Add(negociacion);

            Entity cliente = new Entity("Cliente");
            cliente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            cliente.Attributes.Add(new Models.Attribute("NombreCompleto", "", "string"));
            cliente.useAuditable = false;
            entities.Add(cliente);

            Entity agente = new Entity("Agente");
            agente.Attributes.Add(new Models.Attribute("Id", "", "int"));
            agente.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            agente.useAuditable = false;
            entities.Add(agente);

            Entity ramo = new Entity("Ramo");
            ramo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ramo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ramo.useAuditable = false;
            entities.Add(ramo);

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;
            entities.Add(aseguradora);

            Entity ejecutivo = new Entity("Usuario");
            ejecutivo.Attributes.Add(new Models.Attribute("Id", "", "int"));
            ejecutivo.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            ejecutivo.useAuditable = false;
            entities.Add(ejecutivo);


            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            poliza.logicalOperator = 2;

            /////////////////////////////

           if (SearchId.Value != "" && SearchId.Value != "%%")
            {
                Int64 id = 0;

                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        if (opResult.RetVal.Count > 0)
                        {
                            Entity entity = opResult.RetVal[0];
                            switch (entity.EntityName.ToLower())
                            {
                                case "cliente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("NombreCompleto").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            poliza.Keys.Add(new Models.Key("Contratante", nameId, "string", 2));
                                            break;
                                        }

                                    }
                                    break;
                                case "agente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Clave").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            poliza.Keys.Add(new Models.Key("Agente", nameId, "string", 2));
                                            break;
                                        }

                                    }
                                    break;
                                case "aseguradora":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Clave").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            poliza.Keys.Add(new Models.Key("Aseguradora", nameId, "string", 2));
                                            break;
                                        }
                                    }
                                    break;
                                case "ramo":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Nombre").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            poliza.Keys.Add(new Models.Key("Ramo", nameId, "int", 2));
                                            break;
                                        }
                                    }
                                    break;
                                case "usuario":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string buscar = SearchId.Value.Replace("%", "");
                                        if (item.getAttrValueByName("Nombre").ToLower().Contains(buscar.ToLower()))
                                        {
                                            string nameId = item.getAttrValueByName("Id");
                                            poliza.Keys.Add(new Models.Key("Ejecutivo", nameId, "string", 2));
                                            break;
                                        }
                                    }
                                    break;

                            }
                        }
                    }
                }


                if (Int64.TryParse(SearchId.Value.Replace("%", ""), out id))
                {
                    poliza.Keys.Add(new Models.Key("Id", id.ToString() + "%", "string", 2));
                }
                           

            }
////////////////////

            List<Entity> listLeads = SiteFunctions.GetValues(poliza, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);
            foreach (DataRow row in dtLeadList.Rows)
            {
                foreach (OperationResult opResult in result)
                {
                    if (opResult.RetVal != null)
                    {
                        if(opResult.RetVal.Count > 0)
                        { 
                            Entity entity = opResult.RetVal[0];
                            switch (entity.EntityName.ToLower())
                            {

                                case "cliente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[1].ToString().Trim() == nameId.Trim())
                                        {
                                            row[1] = item.getAttrValueByName("NombreCompleto");
                                            break;
                                        }
                                    }
                                    break;
                                case "agente":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[2].ToString().Trim() == nameId.Trim())
                                        {
                                            row[2] = item.getAttrValueByName("Clave");
                                            break;
                                        }
                                    }
                                    break;
                                case "ramo":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[3].ToString().Trim() == nameId.Trim())
                                        {
                                            row[3] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;
                                case "aseguradora":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[4].ToString().Trim() == nameId.Trim())
                                        {
                                            row[4] = item.getAttrValueByName("Clave");
                                            break;
                                        }
                                    }
                                    break;

                                case "usuario":
                                    foreach (var item in opResult.RetVal)
                                    {
                                        string nameId = item.getAttrValueByName("Id");
                                        if (row[5].ToString().Trim() == nameId.Trim())
                                        {
                                            row[5] = item.getAttrValueByName("Nombre");
                                            break;
                                        }
                                    }
                                    break;

                            }

                        }
                    }
                }
            }

            GVPolizasList.DataSource = dtLeadList;
            GVPolizasList.DataBind();
        }
        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
           searchForPolizas();
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForPolizas();
        }

        protected void ProspectView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForPolizas();
        }
    }
}