﻿<%@ Page Title="Prospecto" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProspectosList.aspx.cs" Inherits="CognisoWebApp.ProspectosList" EnableEventValidation="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
</script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Requiere eliminar el Prospecto?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

</script>

    <div class="containerRT1">
        <div class="containerRT2" runat="server">
             <div class="panel-heading">    
                    <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Prospectos</asp:Label></h2>
                </div>
            <hr class="float-left">

                <nav aria-label="breadcrumb" class="clearfix">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                        <li class="breadcrumb-item active"><a href="#">Lista de Prospectos</a></li>
                      </ol>
                </nav>    
                
                
                <div class="panel-heading">
                    <div class="btn-group" >
                        <asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" runat="server" Height="30px" OnClick="btnNew_Click" Width="31px"/>
                        <!--<asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/writing.png" runat="server" OnClick="BtnSave_Click"/>-->
                        <asp:ImageButton ID="btnDelete" ImageUrl="/Content/Imgs/delete.png" runat="server" Height="30px" OnClick="btnDelete_Click" OnClientClick = "Confirm()"  />
                    </div>
                     <div id="errMess" >

                    </div>
                </div>
              <div class="input-group mb-3">
                         <div class="col-md-5"><asp:TextBox Id="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id,Nombre o RFC a buscar" aria-label="Id,Nombre o RFC a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox></div>
                         <div class="input-group-append col-md-7">
                            <div class="col-md-2"><asp:Button Id="btnBuscar" runat="server" Cssclass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click" ></asp:Button></div>
                           <div class="col-md-4"><label class="form-control">Filtro: </label></div>
                           <div class="col-md-6">
                              <asp:DropDownList Id="ProspectView" runat="server" Cssclass="form-control" OnSelectedIndexChanged="ProspectView_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="1">Todos los prospectos</asp:ListItem>
                                <asp:ListItem Value ="2">Prospectos nuevos</asp:ListItem>
                                <asp:ListItem Value="3">Prospectos en validación</asp:ListItem>
                                <asp:ListItem Value="4">Prospectos rechazados</asp:ListItem>
                                <asp:ListItem Value="5">Prospectos convertidos en clientes</asp:ListItem>
                            </asp:DropDownList>
                           </div>
                         </div>
                    </div>
                <div class="panel-body" runat="server">
                    <div class="table-responsive" runat="server">
                        <asp:GridView ID="GVLeadList" runat="server" OnPageIndexChanging="GVLeadList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                            <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a   href='Prospecto.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                            <!--<asp:CheckBox ID="chkEliminar" runat="server"/>-->
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="RazonSocial" HeaderText="Nombre" />
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" />
                                    <asp:BoundField DataField="TipoPersona" HeaderText="Tipo de persona" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                </Columns>
                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="SearchId" runat="server" Value="" />
                 </div>
         </div>    
    </div>    
</asp:Content>
