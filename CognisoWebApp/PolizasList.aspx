﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PolizasList.aspx.cs" Inherits="CognisoWebApp.PolizasList" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
    </script>

    <div class="containerRT1">
        <div class="containerRT2" runat="server">
            <div class="panel-heading ">
                <h2><asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Polizas</asp:Label></h2>
                
            </div>
            <hr class="float-left">

            <nav aria-label="breadcrumb" class="clearfix">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="#">Lista de Polizas</a></li>
                </ol>
            </nav>

            <div class="panel-heading float-right btn-green">
                <div class="btn-group">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nuevo<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=0">Autos Individual</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=6">Autos Flotilla</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=4">Vida Individual</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=5">Vida Pool</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=2">GMM Individual</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=3">GMM Pool</a></li>
                            <li><a class="dropdown-item" href="Poliza.aspx?polType=1">Daños</a></li>
                          </ul>
                        </li>
                        <!--<li><asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/writing.png" runat="server" OnClick="BtnSave_Click" /></li>-->
                    </ul>
                    <!--<asp:ImageButton ID="btnNew" ImageUrl="/Content/Imgs/new.png" runat="server" Height="30px" OnClick="btnNew_Click" Width="31px" />
                    <asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" />-->
                </div>
            </div>
            <br />
            
            <div class="input-group mb-3">
                <div class="col-md-5"><asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id,Folio,Contratante a buscar" aria-label="Id,Nombre o RFC a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox></div>
                <div class="input-group-append col-md-7">
                    <div class="col-md-2"><asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-outline-secondary form-control" type="button" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button></div>
                    <div class="col-md-4"><label class="form-control">Filtro: </label></div>
                    <div class="col-md-6">
                              <asp:DropDownList Id="PolizaView" runat="server" Cssclass="form-control " OnSelectedIndexChanged="PolizaView_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="1">Todas las Ot pendientes</asp:ListItem>
                                <asp:ListItem Value ="2">Todas las pólizas renovadas</asp:ListItem>
                                <asp:ListItem Value="3">Todas las pólizas</asp:ListItem>
                                <asp:ListItem Value="4">Todas las pólizas vigentes</asp:ListItem>
                                <asp:ListItem Value="5">Todas las pólizas canceladas</asp:ListItem>
                                  <asp:ListItem Value="6">Todas las pólizas en renovación</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="panel-body" runat="server">

                <div class="table-responsive count" runat="server">
                    <asp:GridView ID="GVPolizasList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <a   href='Poliza.aspx?Id=<%# Eval("Id") %>'><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:BoundField DataField="Id" HeaderText="Id" />
                            <asp:BoundField DataField="TramiteFolio" HeaderText="# Poliza" />
                            <asp:BoundField DataField="ClienteNombreCompleto" HeaderText="Contratante" />
                            <asp:BoundField DataField="AgenteNombre" HeaderText="Agente" />
                            <asp:BoundField DataField="RamoNombre" HeaderText="Ramo" />
                            <asp:BoundField DataField="AseguradoraClave" HeaderText="Aseguradora" />
                            <asp:BoundField DataField="UsuarioNombre" HeaderText="Ejecutivo" />
                            <asp:BoundField DataField="EstatusPoliza" HeaderText="Estatus" />
                        </Columns>
                        <pagersettings mode="NextPreviousFirstLast"
                            FirstPageImageUrl="Content/Imgs/first.png" 
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="SearchId" runat="server" Value="" />
            </div>

        </div>
    </div>
</asp:Content>

