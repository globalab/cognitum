﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Poliza.aspx.cs" Inherits="CognisoWebApp.Poliza" ValidateRequest="false" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/Scripts/select2.min.js"></script>
    <script src="Scripts/tinymce/tinymce.min.js"></script>
    <link href="Content/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            language: 'es_MX',
            language_url: '/Scripts/tinymce/langs/es_MX.js',
            plugins: 'print preview fullpage paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
            imagetools_cors_hosts: ['picsum.photos'],
            menubar: 'file edit view insert format tools table help',
            toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
            toolbar_sticky: true,
            autosave_ask_before_unload: true,
            autosave_interval: "30s",
            autosave_prefix: "{path}{query}-{id}-",
            autosave_restore_when_empty: false,
            autosave_retention: "2m",
            image_advtab: true,
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tiny.cloud/css/codepen.min.css'
            ],
            link_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_list: [
              { title: 'My page 1', value: 'http://www.tinymce.com' },
              { title: 'My page 2', value: 'http://www.moxiecode.com' }
            ],
            image_class_list: [
              { title: 'None', value: '' },
              { title: 'Some class', value: 'class-name' }
            ],
            importcss_append: true,
            height: 400,
            file_picker_callback: function (callback, value, meta) {
                /* Provide file and text for the link dialog */
                if (meta.filetype === 'file') {
                    callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
                }

                /* Provide image and alt text for the image dialog */
                if (meta.filetype === 'image') {
                    callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
                }

                /* Provide alternative source and posted for the media dialog */
                if (meta.filetype === 'media') {
                    callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
                }
            },
            templates: [
                  { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
              { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
              { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
            ],
            template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
            template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
            height: 600,
            image_caption: true,
            quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
            noneditable_noneditable_class: "mceNonEditable",
            toolbar_drawer: 'sliding',
            contextmenu: "link image imagetools table",
        });
    </script>
    <script lang="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtCliente.ClientID%>").select2({
                placeholder: "Seleccione un cliente",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtTitular.ClientID%>").select2({
                placeholder: "Seleccione un Titular",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtDirector.ClientID%>").select2({
                placeholder: "Seleccione un director",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtRamo.ClientID%>").select2({
                placeholder: "Seleccione un ramo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtSubramo.ClientID%>").select2({
                placeholder: "Seleccione un sub ramo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtAgente.ClientID%>").select2({
                placeholder: "Seleccione un agente",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtAseguradora.ClientID%>").select2({
                placeholder: "Seleccione una aseguradora",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtEjecutivo.ClientID%>").select2({
                placeholder: "Seleccione un ejecutivo",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtNegociacion.ClientID%>").select2({
                placeholder: "Seleccione una negociación",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtSupervisor.ClientID%>").select2({
                placeholder: "Seleccione un supervisor",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(document).ready(function () {
            $("#<%=txtEjeAseuradora.ClientID%>").select2({
                placeholder: "Seleccione un ejecutivo de aseguradora",
                minimumResultsForSearch: 1,
                allowClear: true
            });
        });

        $(function () {
            $('[id*=txtAntiguedad]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtBirthDay]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtInicio]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFin]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFechaEnvio]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
        $(function () {
            $('[id*=txtFechaEmision]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });

        $('body').on('click', '#popupComision', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var page = $(this).attr("data-image")  //get url of link
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.9;
            if ("<%= PoliId.Value %>" == "") {
                $("[id*=openurlatstart]").val(page);

                $("[id*=PaneName]").val("Comisiones");
                $("#<%=BtnSave.ClientID%>").click();
                page = "Comision.aspx?popup=1&poliza=<%= PoliId.Value %>";
            }
            else {

                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Comisiones",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=btnSearchComisiones.ClientID%>").click();
                    }
                });
                    $dialog.dialog('open');
                }
        });

            $('body').on('click', '#popupRecibos', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Recibos ",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {


                    }
                });
                $dialog.dialog('open');

            });

            $('body').on('click', '#popupComAsoc', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                var page = $(this).attr("data-image")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Comisiones",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=btnSearchComisiones.ClientID%>").click();
                    }
                });
                $dialog.dialog('open');

            });

                $('body').on('click', '#popupRecibo', function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    var page = $(this).attr("data-image")  //get url of link
                    var wWidth = $(window).width();
                    var dWidth = wWidth * 0.8;
                    var wHeight = $(window).height();
                    var dHeight = wHeight * 0.9;
                    var $dialog = $('<div></div>')
                    .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                    .dialog({
                        autoOpen: false,
                        modal: true,
                        center: true,
                        height: dHeight,
                        width: dWidth,
                        title: "Recibos",
                        buttons: {
                            "Cerrar": function () { $dialog.dialog('close'); }
                        },
                        close: function (event, ui) {

                            $("#<%=BtnSearchRecibos.ClientID%>").click();
                        }
                    });
                    $dialog.dialog('open');

                });
                    $('body').on('click', '#popupEndosoA', function (e) {

                        e.preventDefault();
                        e.stopImmediatePropagation();
                        $("#<%=txtSearchEndoso.ClientID%>").click();
                        var page = $(this).attr("href")  //get url of link
                        var wWidth = $(window).width();
                        var dWidth = wWidth * 0.8;
                        var wHeight = $(window).height();
                        var dHeight = wHeight * 0.9;
                        if ("<%= PoliId.Value %>" == "") {
                $("[id*=openurlatstart]").val(page);

                $("[id*=PaneName]").val("Endoso");
                $("#<%=BtnSave.ClientID%>").click();
                page = "Endoso.aspx?endType=2198&popup=1&poliza=<%= PoliId.Value %>&formapago=<%=txtFormaPago.SelectedValue%>";
            }
            else {
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Endoso",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=btnBuscarEndoso.ClientID%>").click();
                    }
                });

                    $dialog.dialog('open');
                }
        });
            $('body').on('click', '#popupEndosoB', function (e) {

                e.preventDefault();
                e.stopImmediatePropagation();
                $("#<%=txtSearchEndoso.ClientID%>").click();
                            var page = $(this).attr("href")  //get url of link
                            var wWidth = $(window).width();
                            var dWidth = wWidth * 0.8;
                            var wHeight = $(window).height();
                            var dHeight = wHeight * 0.9;
                            if ("<%= PoliId.Value %>" == "") {
                $("[id*=openurlatstart]").val(page);

                $("[id*=PaneName]").val("Endoso");
                $("#<%=BtnSave.ClientID%>").click();
                page = "Endoso.aspx?endType=2201&popup=1&poliza=<%= PoliId.Value %>&formapago=<%=txtFormaPago.SelectedValue%>";
            }
            else {
                var $dialog = $('<div></div>')
                .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                .dialog({
                    autoOpen: false,
                    modal: true,
                    center: true,
                    height: dHeight,
                    width: dWidth,
                    title: "Endoso",
                    buttons: {
                        "Cerrar": function () { $dialog.dialog('close'); }
                    },
                    close: function (event, ui) {

                        $("#<%=btnBuscarEndoso.ClientID%>").click();
                    }
                });

                    $dialog.dialog('open');
                }
        });
            $('body').on('click', '#popupEndosoD', function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $("#<%=txtSearchEndoso.ClientID%>").click();
                var page = $(this).attr("href")  //get url of link
                var wWidth = $(window).width();
                var dWidth = wWidth * 0.8;
                var wHeight = $(window).height();
                var dHeight = wHeight * 0.9;
                if ("<%= PoliId.Value %>" == "") {
                    $("[id*=openurlatstart]").val(page);

                    $("[id*=PaneName]").val("Endoso");
                    $("#<%=BtnSave.ClientID%>").click();
                    page = "Endoso.aspx?endType=2205&popup=1&poliza=<%= PoliId.Value %>&formapago=<%=txtFormaPago.SelectedValue%>";
                }
                else {
                    var $dialog = $('<div></div>')
                    .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                    .dialog({
                        autoOpen: false,
                        modal: true,
                        center: true,
                        height: dHeight,
                        width: dWidth,
                        title: "Endoso",
                        buttons: {
                            "Cerrar": function () { $dialog.dialog('close'); }
                        },
                        close: function (event, ui) {

                            $("#<%=btnBuscarEndoso.ClientID%>").click();
                        }
                    });

                        $dialog.dialog('open');
                    }
            });

                function openpopup(url, title, btnname) {
                    var wWidth = $(window).width();
                    var dWidth = wWidth * 0.8;
                    var wHeight = $(window).height();
                    var dHeight = wHeight * 0.8;
                    if (title == "Comisiones") {
                        btnname = "#<%=btnSearchComisiones.ClientID%>";

                    }
                    if (title == "Titular")
                    {
                        if (document.getElementById("<%=txtCliente.ClientID%>").value == "") {
                            alert("Debe de capturar el contratante");
                            return;
                        }
                    }
            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: title,
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $(btnname).click();
                }
            });
            $dialog.dialog('open');
        }

        $('body').on('click', '#popupEndoso', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            var page = $(this).attr("data-image")  //get url of link
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.9;
            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: "Endoso",
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {

                    $("#<%=btnBuscarEndoso.ClientID%>").click();
                }
            });
            $dialog.dialog('open');

        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";
            $('#myTab a[href="#' + paneName + '"]').tab('show')

        }

        function selectPane(PaneName) {
            $("[id*=PaneName]").val(PaneName);

        }


    </script>
    <script type="text/javascript">
        function CalculateAmountsNeto() {
            try {
                debugger;
                var amount = 0;
                var total = 0;

                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                var gastos = document.getElementById("<%=txtGastos.ClientID%>").value;
                var recargos = document.getElementById("<%=txtRecargos.ClientID%>").value;
                var descuentos = document.getElementById("<%=txtDescuento.ClientID%>").value;
                var impuestos = document.getElementById("<%=txtImpuesto.ClientID%>").value;

                if (prima.length > 0) {
                    if (isNaN(prima)) {
                        alert("Prima acepta números unicamente !!");
                        return false;
                    }
                    else {
                        amount = amount + parseFloat(prima);
                    }
                }

                if (gastos.length > 0) {
                    if (isNaN(gastos)) {
                        alert("Gastos acepta números unicamente !!");
                        return false;
                    }
                    else {
                        amount = amount + parseFloat(gastos);
                    }
                }
                if (recargos.length > 0) {
                    if (isNaN(recargos)) {
                        alert("Recargos acepta números unicamente !!");
                        return false;
                    }
                    else {
                        var recargoAmount = parseFloat(recargos);
                        amount = amount + recargoAmount;
                    }
                }

                if (descuentos.length > 0) {
                    if (isNaN(descuentos)) {
                        alert("Descuento acepta números unicamente !!");
                        return false;
                    }
                    else {
                        var descuentos = parseFloat(descuentos);
                        amount = amount - descuentos;
                    }
                }

                

                document.getElementById("<%=txtNeto.ClientID%>").value = amount;

                var neto = document.getElementById("<%=txtNeto.ClientID%>").value;
                SetImpuesto();
                total = parseFloat(neto) + parseFloat(impuestos);
                document.getElementById("<%=txtTotal.ClientID%>").value = total.toFixed(4);


            } catch (e) {

            }


        }

        function CalculateAmountRecargoByPercent() {
            var amount = 0;
            var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            var percentRecargo = document.getElementById("<%=txtRecargosPor.ClientID%>").value;

            if (prima.length > 0) {
                if (isNaN(prima)) {
                    alert("Prima acepta números unicamente !!");
                    return false;
                }
                else {

                    if (percentRecargo.length > 0) {
                        if (isNaN(percentRecargo)) {
                            alert("Porcentaje de Recargos acepta números unicamente !!");
                            return false;
                        }
                        else {
                            var result = parseFloat(prima) * parseFloat(percentRecargo);
                            amount = result / 100;
                            document.getElementById("<%=txtRecargos.ClientID%>").value = amount;
                        }
                    }


                }
            }
        }
        function CalculatePercentRecargoByAmount() {
            var percentRecargo = 0;
            var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            var recargos = document.getElementById("<%=txtRecargos.ClientID%>").value;
            if (recargos.length > 0) {
                if (isNaN(recargos)) {
                    alert("Recargos acepta números unicamente !!");
                    return false;
                }
                else {
                    var recargoAmount = parseFloat(recargos);
                    percentRecargo = (recargoAmount / parseFloat(prima)) * 100;
                    percentRecargo = percentRecargo.toFixed(2);
                    document.getElementById("<%=txtRecargosPor.ClientID%>").value = percentRecargo;
                }
            }
            CalculateAmountsNeto();
        }
        function SetImpuesto() {
            try {
                var idporcen = document.getElementById("<%=txtImpuestoPor.ClientID%>").value;
                var neto = document.getElementById("<%=txtNeto.ClientID%>").value;
                $.ajax({
                    type: "POST",

                    url: '<%= ResolveUrl("Poliza.aspx/getAmountImpuesto") %>',
                    data: "{'idporcentaje':'" + idporcen + "', 'neto':'" + neto + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        document.getElementById("<%=txtImpuesto.ClientID%>").value = msg.d;
                        CalculateAmountsNeto();
                    },
                    error: function (msg) {
                        console.log(msg.d + "error");

                    }
                });

                } catch (e) {
                    console.log(e.message);
                }
            }

            function CalculateAmountComision() {
                var amountcomision = 0;
                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                var compercent = document.getElementById("<%=txtComisionPor.ClientID%>").value;
                if (prima.length > 0 && compercent.length > 0) {
                    if (isNaN(prima)) {
                        return false;
                    }
                    else {
                        var primaAmount = parseFloat(prima);
                        amountcomision = ((primaAmount * parseFloat(compercent)) / 100);
                        amountcomision = amountcomision.toFixed(2);
                        document.getElementById("<%=txtComision.ClientID%>").value = amountcomision;
                    }
                }
            }
            function CalculateAmountSobreComision() {
                var amountcomision = 0;
                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                var compercent = document.getElementById("<%=txtSobreComisionPor.ClientID%>").value;
                if (prima.length > 0 && compercent.length > 0) {
                    if (isNaN(prima)) {
                        return false;
                    }
                    else {
                        var primaAmount = parseFloat(prima);
                        amountcomision = ((primaAmount * parseFloat(compercent)) / 100);
                        amountcomision = amountcomision.toFixed(2);
                        document.getElementById("<%=txtSobreComision.ClientID%>").value = amountcomision;
                    }
                    SumTotalCom();
                }
            }
            function CalculateAmountBono() {
                var amountcomision = 0;
                var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
                var compercent = document.getElementById("<%=txtBonoPor.ClientID%>").value;
                if (prima.length > 0 && compercent.length > 0) {
                    if (isNaN(prima)) {
                        return false;
                    }
                    else {
                        var primaAmount = parseFloat(prima);
                        amountcomision = ((primaAmount * parseFloat(compercent)) / 100);
                        amountcomision = amountcomision.toFixed(2);
                        document.getElementById("<%=txtBono.ClientID%>").value = amountcomision;
                    }
                    SumTotalCom();
                }
            }

            function calculateEndDate() {

                var fechaini = document.getElementById("<%=txtInicio.ClientID%>").value;
            var fechainiD = new Date(fechaini.substring(6, 10), parseInt(fechaini.substring(3, 5)) - 1, fechaini.substring(0, 2));

            var fechafin = new Date(fechainiD.getFullYear() + 1, fechainiD.getMonth(), fechainiD.getDate());
            var dd = fechafin.getDate();

            var mm = fechafin.getMonth() + 1;
            var yyyy = fechafin.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '/' + mm + '/' + yyyy;
            document.getElementById("<%=txtFin.ClientID%>").value = today;

        }
        function CalculatePercentComision() {
            var amountcom = 0;
            var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            i
                var comamt = document.getElementById("<%=txtComision.ClientID%>").value;
                if (prima.length > 0 && comamt.length > 0) {
                    if (isNaN(prima)) {
                        return false;
                    }
                    else {
                        var primaAmount = parseFloat(prima);
                        if (primaAmount != 0) {
                            amountcom = (comamt / parseFloat(prima)) * 100;
                            amountcom = amountcom.toFixed(2);
                            document.getElementById("<%=txtComisionPor.ClientID%>").value = amountcom;
                        }
                        else {
                            document.getElementById("<%=txtComisionPor.ClientID%>").value = "0.00";
                        }
                     }
                     SumTotalCom();
                 }
             }
             function CalculatePercentSobreComision() {
                 var amountscom = 0;
                 var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            var scomamt = document.getElementById("<%=txtSobreComision.ClientID%>").value;
            if (prima.length > 0 && scomamt.length > 0) {
                if (isNaN(prima)) {
                    return false;
                }
                else {
                    var primaAmount = parseFloat(prima);
                    amountscom = (scomamt / parseFloat(prima)) * 100;
                    amountscom = amountscom.toFixed(2);
                    document.getElementById("<%=txtSobreComisionPor.ClientID%>").value = amountscom;
                }
                SumTotalCom();
            }
        }
        function CalculatePercentBono() {
            var amountbono = 0;
            var prima = document.getElementById("<%=txtPrima.ClientID%>").value;
            var bonoamt = document.getElementById("<%=txtBono.ClientID%>").value;
            if (prima.length > 0 && bonoamt.length > 0) {
                if (isNaN(prima)) {
                    return false;
                }
                else {
                    var primaAmount = parseFloat(prima);
                    if (primaAmount != 0) {
                        amountbono = (bonoamt / parseFloat(prima)) * 100;
                        amountbono = amountbono.toFixed(2);
                        document.getElementById("<%=txtBonoPor.ClientID%>").value = amountbono;
                    }
                    else
                    {
                        document.getElementById("<%=txtBonoPor.ClientID%>").value = "0.00";
                    }
                }
                SumTotalCom();
            }
        }
        function SumTotalCom() {
            try {
                var sumtotal = 0;
                var comamt = document.getElementById("<%=txtComision.ClientID%>").value;
                var scomamt = document.getElementById("<%=txtSobreComision.ClientID%>").value;
                var bonoamt = document.getElementById("<%=txtBono.ClientID%>").value;

                if (comamt.length > 0) {
                    sumtotal = parseFloat(comamt);
                }
                if (scomamt.length > 0) {
                    sumtotal = sumtotal + parseFloat(scomamt);
                }
                if (bonoamt.length > 0) {
                    sumtotal = sumtotal + parseFloat(bonoamt);
                }

                document.getElementById("<%=txtTotalCom.ClientID%>").value = sumtotal;

            } catch (e) {

            }

        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Desea eliminar los Incisos seleccionados?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
        function ConfirmCD() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_valuecd";
            if (confirm("Desea eliminar las Comisiones de Director seleccionados?")) {
                confirm_value.value = "Si";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        function generaCarta(idplantilla,tipoplantilla) {
            try {
             
                document.getElementById("<%=idplantilla.ClientID%>").value = idplantilla;
                

            } catch (e) {
                console.log(e.message);
            }
        }

    </script>
    <div class="containerRT1" style="width:100%">

        <div class="containerRT2" runat="server">
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Poliza</asp:Label>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="PolizasList.aspx">Lista de Polizas</a></li>
                    <li class="breadcrumb-item active"><%=PolizaOT.Value %> <%=PolizaTypeStr.Value %> <%=PoliId.Value %> </li>
                </ol>
            </nav>
            <div class="panel-heading">
                <div class="row">
                    <div class="col">
                        <asp:ImageButton ID="BtnSave" ToolTip="Guardar" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" OnClick="BtnSave_Click"  OnClientClick="return CheckIsRepeat();"/>
                        <asp:ImageButton ID="btnAttachment" ImageUrl="/Content/Imgs/clip.png" runat="server" OnClick="BtnAttachment_Click" CausesValidation="false" />
                        <asp:Button ID="BtnDescarte"  OnClientClick="javascript:return false;" data-toggle="modal" data-target="#exampleModal" runat="server" Text="Descarte" width="30%" style="display:inline-block"/>
                        <asp:Button ID="BtnRenovar"  data-toggle="modal" runat="server" Text="Renovar" OnClick="BtnRenovar_Click" width="30%" style="display:inline-block" />
                        
                    </div>
                    <div class="col">
                        <asp:LinkButton ID="BtnCancel" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;" OnClick="BtnCancel_Click">
                            <asp:Image ID="Image1" runat="server" ImageUrl="/Content/Imgs/cancel.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                            <asp:Label ID="Label5" runat="server" Text="Iniciar Proceso de Cancelacion de Póliza"></asp:Label>
                         </asp:LinkButton>
                        <asp:LinkButton ID="btnAprove" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;" OnClick="btnAprove_Click">
                            <asp:Image ID="Image2" runat="server" ImageUrl="/Content/Imgs/ok.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                            <asp:Label ID="Label2" runat="server" Text="Aprobar Cancelación"></asp:Label>
                         </asp:LinkButton>
                        <asp:LinkButton ID="btnReject" runat="server" Width="100%" onMouseOver="window.status='New Panel'; return true;" onMouseOut="window.status='Menu ready'; return true;"  data-toggle="modal" data-target="#exampleModal2">
                            <asp:Image ID="Image3" runat="server" ImageUrl="/Content/Imgs/Reject.png" BackColor="Transparent" Width="30" Height="30" ImageAlign="AbsMiddle"/>
                            <asp:Label ID="Label3" runat="server" Text="RechazarCancelación"></asp:Label>
                         </asp:LinkButton>
                    </div>
                    <div class="col">
                        <ul class="nav navbar-nav">
                            <li class="dropdown" id="Li1" runat="server">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cartas<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <asp:Repeater ID="rptCartas" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#ConfirmCartas" onclick="generaCarta(<%# Eval("Id") %>,<%# Eval("TipoPlantilla") %>)" ><%# Eval("Nombre") %><span class="caret"></span></a>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="containerRT3" runat="server" id="PolizaGral">
                <div id="errMess">
                </div>
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">Comentarios de Rechazo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Width="400px" Height="200px"></asp:TextBox>
                    </div>
            
                 <div class="modal-footer">
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <asp:Button ID="btnSaveReject" runat="server" Text="Rechazar" OnClick="btnSaveReject_Click"  Cssclass="btn btn-outline-secondary" />
                  </div>
                </div>
              </div>
            </div>
                <asp:ValidationSummary 
                              id="valSum" 
                              DisplayMode="BulletList" 
                              runat="server"
                              HeaderText="Debe de llenar los siguientes campos requeridos:"
                              CssClass='alert alert-danger alert-dismissible fade show'
                            />
                <div class="accordion" id="accordiontable" style="visibility: hidden" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item" onclick="selectPane('General');">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Direcciones');">
                            <a class="nav-link" id="Direcciones-tab" data-toggle="tab" href="#Direcciones" role="tab" aria-controls="Direcciones" aria-selected="false">Direcciones</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Gastos');">
                            <a class="nav-link" id="Gastos-tab" data-toggle="tab" href="#Gastos" role="tab" aria-controls="Gastos" aria-selected="false">Gastos</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Admin');">
                            <a class="nav-link" id="Admin-tab" data-toggle="tab" href="#Admin" role="tab" aria-controls="Admin" aria-selected="false">Administración</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('ComisionesPoliza');">
                            <a class="nav-link" id="ComisionesPoliza-tab" data-toggle="tab" href="#ComisionesPoliza" role="tab" aria-controls="ComisionesPoliza" aria-selected="false">Comisiones</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Comisiones');">
                            <a class="nav-link" id="Comisiones-tab" data-toggle="tab" href="#Comisiones" role="tab" aria-controls="Comisiones" aria-selected="false">Comisiones Director</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Bitacora');" runat="server" id="BitacoraNav">
                            <a class="nav-link" id="Bitacora-tab" data-toggle="tab" href="#Bitacora" role="tab" aria-controls="Bitacora" aria-selected="false">Bitácora</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Slip');">
                            <a class="nav-link" id="Slip-tab" data-toggle="tab" href="#Slip" role="tab" aria-controls="Slip" aria-selected="false">Slip</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Coberturas');">
                            <a class="nav-link" id="Coberturas-tab" data-toggle="tab" href="#Coberturas" role="tab" aria-controls="Coberturas" aria-selected="false">Coberturas</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('CondSpec');">
                            <a class="nav-link" id="CondSpec-tab" data-toggle="tab" href="#CondSpec" role="tab" aria-controls="CondSpec" aria-selected="false">Condiciones Especiales</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Endosos');">
                            <a class="nav-link" id="Endosos-tab" data-toggle="tab" href="#Endosos" role="tab" aria-controls="Endosos" aria-selected="false">Endosos</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Siniestros');">
                            <a class="nav-link" id="Siniestros-tab" data-toggle="tab" href="#Siniestros" role="tab" aria-controls="Siniestros" aria-selected="false">Siniestros</a>
                        </li>
                        <li class="nav-item" onclick="selectPane('Recibos');">
                            <a class="nav-link" id="Recibos-tab" data-toggle="tab" href="#Recibos" role="tab" aria-controls="Recibos" aria-selected="false">Recibos</a>
                        </li>
                        <li class="nav-item" id="IncisoAutosDiv" runat="server" onclick="selectPane('IncisosAutos');">
                            <a class="nav-link" id="IncisosAutos-tab" data-toggle="tab" href="#IncisosAutos" role="tab" aria-controls="IncisosAutos" aria-selected="false">Incisos</a>
                        </li>
                        <li class="nav-item" id="AutoIndivDiv" runat="server" onclick="selectPane('AutoIndiv');">
                            <a class="nav-link" id="AutoIndiv-tab" data-toggle="tab" href="#AutoIndiv" role="tab" aria-controls="AutoIndiv" aria-selected="false">Datos del auto</a>
                        </li>
                        <li class="nav-item" id="VidaIndividualDiv" runat="server" onclick="selectPane('VidaIndividual');">
                            <a class="nav-link" id="VidaIndividual-tab" data-toggle="tab" href="#VidaIndividual" role="tab" aria-controls="VidaIndividual" aria-selected="false">Datos del asegurado</a>
                        </li>
                        <li class="nav-item" id="GMMIndividualDiv" runat="server" onclick="selectPane('GMMIndividual');">
                            <a class="nav-link" id="GMMIndividual-tab" data-toggle="tab" href="#GMMIndividual" role="tab" aria-controls="GMMIndividual" aria-selected="false">Condiciones de la poliza</a>
                        </li>
                        <li class="nav-item" id="VidaPoolDiv" runat="server" onclick="selectPane('VidaPool');">
                            <a class="nav-link" id="VidaPool-tab" data-toggle="tab" href="#VidaPool" role="tab" aria-controls="VidaPool" aria-selected="false">Condiciones de la poliza</a>
                        </li>
                        <li class="nav-item" id="GMMPoolDiv" runat="server" onclick="selectPane('GMMPool');">
                            <a class="nav-link" id="GMMPool-tab" data-toggle="tab" href="#GMMPool" role="tab" aria-controls="GMMPool" aria-selected="false">Condiciones de la poliza</a>
                        </li>
                        <li class="nav-item" id="VPGruposDiv" runat="server" onclick="selectPane('VPGrupos');">
                            <a class="nav-link" id="VPGrupos-tab" data-toggle="tab" href="#VPGrupos" role="tab" aria-controls="VPGrupos" aria-selected="false">Grupos</a>
                        </li>
                        <li class="nav-item" id="Audit" runat="server" onclick="selectPane('Auditoria');">
                            <a class="nav-link" id="Auditoria-tab" data-toggle="tab" href="#Auditoria" role="tab" aria-controls="Auditoria" aria-selected="false">Auditoria</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT4">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblOt" runat="server" Text="OT"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtOt" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblNoPoliza" runat="server" Text="No. Poliza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNopoliza" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblCliente" runat="server" Text="Contratante"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCliente" AutoPostBack="true" Style="width: 90%" runat="server" class="form-control" OnSelectedIndexChanged="txtCliente_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valContratante" runat="server" ErrorMessage="Contratante" ForeColor="Red" ControlToValidate="txtCliente" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblTitular" runat="server" Text="Titular"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtTitular" runat="server" Style="width: 80%" class="form-control" CausesValidation="false">
                                            </asp:DropDownList>
                                            <div class="input-group-append">
                                                &nbsp;<a id="modifyTitular" href='#' onclick="openpopup('ContactoSimple.aspx?id='+$('#<%=txtTitular.ClientID %> option:selected').val(),'Titular','#<%=btnTitular.ClientID%>');" ><img src="/Content/Imgs/edit.png" width="25" height="25" /></a>     
                                                &nbsp;<a id="createTitular" href='#' onclick="openpopup('ContactoSimple.aspx?custid='+$('#<%=txtCliente.ClientID %> option:selected').val(),'Titular','#<%=btnTitular.ClientID%>');" ><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>     
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFormaPago" runat="server" Text="F. de Pago"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtFormaPago" runat="server" class="form-control" OnSelectedIndexChanged="txtFormaPago_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valFPago" runat="server" ErrorMessage="Forma de Pago" ForeColor="Red" ControlToValidate="txtFormaPago" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblAseguradora" runat="server" Text="Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAseguradora" Style="width: 90%" AutoPostBack="true" OnSelectedIndexChanged="txtAseguradora_SelectedIndexChanged" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valAseguradora" runat="server" ErrorMessage="Aseguradora" ForeColor="Red" ControlToValidate="txtAseguradora" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblInicio" runat="server" Text="Inicio de Vigencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtInicio" runat="server" onchange="calculateEndDate();"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblFin" runat="server" Text="Fin de Vigencia"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox class="form-group input-group" ID="txtFin" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRamo" runat="server" Text="Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtRamo" Style="width: 90%" OnSelectedIndexChanged="txtRamo_SelectedIndexChanged" AutoPostBack="true" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valRamo" runat="server" ErrorMessage="Ramo" ForeColor="Red" ControlToValidate="txtRamo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblSubramo" runat="server" Text="Sub Ramo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtSubramo" Style="width: 90%" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valSubramo" runat="server" ErrorMessage="SubRamo" ForeColor="Red" ControlToValidate="txtSubramo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEstatus" runat="server" Text="Estatus"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatus" runat="server" class="form-control" Enabled="false"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblAgente" runat="server" Text="Agente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtAgente" Style="width: 90%" runat="server" class="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valAgente" runat="server" ErrorMessage="Agente" ForeColor="Red" ControlToValidate="txtAgente" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRenovable" runat="server" Text="Renovable"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:CheckBox ID="chckRenovable" runat="server" OnCheckedChanged="chckRenovable_CheckedChanged" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblPolOriginal" runat="server" Text="Poliza original"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList class="form-group input-group" ID="txtPolOriginal" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblPolNueva" runat="server" Text="Poliza nueva"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList class="form-group input-group" ID="txtPolNueva" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                        <div class="tab-pane fade" id="Direcciones" role="tabpanel" aria-labelledby="Direcciones-tab">
                            <div class="containerRT5">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDirFiscal" runat="server" Text="Dirección Fiscal"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtDirFiscal" runat="server" class="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ValDirFisc" runat="server" ErrorMessage="Dirección Fiscal" ForeColor="Red" ControlToValidate="txtDirFiscal" Text="*"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblDirEnvio" runat="server" Text="Dirección Envío"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtDirEnvio" runat="server" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblDirCobro" runat="server" Text="Dirección Cobro"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtDirCobro" runat="server" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Gastos" role="tabpanel" aria-labelledby="Gastos-tab">
                            <div class="containerRT6">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblMoneda" runat="server" Text="Moneda"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMoneda" runat="server" class="form-control">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valMoneda" runat="server" ErrorMessage="Moneda" ForeColor="Red" ControlToValidate="txtMoneda" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblPrima" runat="server" Text="Prima"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPrima" onchange="CalculateAmountsNeto();CalculateAmountComision();CalculateAmountBono();CalculateAmountSobre();" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblGastos" runat="server" Text="Gastos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtGastos" onchange="CalculateAmountsNeto()" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblRecargosPor" runat="server" Text="Recargos %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRecargosPor" onchange="CalculateAmountRecargoByPercent();" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblRecargos" runat="server" Text="Recargos"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtRecargos" onchange="CalculatePercentRecargoByAmount();" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNeto" runat="server" Text="Neto"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNeto" runat="server" CssClass="form-control" TabIndex="5" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblDescuento" runat="server" Text="Descuento"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDescuento" runat="server" CssClass="form-control" TabIndex="6" onchange="CalculateAmountsNeto();">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblImpuestoPor" runat="server" Text="Impuesto %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtImpuestoPor" runat="server" CssClass="form-control" TabIndex="7" onchange="SetImpuesto();">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valImpuesto" runat="server" ErrorMessage="Impuesto" ForeColor="Red" ControlToValidate="txtImpuestoPor" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblImpuesto" runat="server" Text="Impuesto"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtImpuesto" runat="server" CssClass="form-control" TabIndex="8">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" TabIndex="7">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblEstatusCobranza" runat="server" Text="Estatus Cobranza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEstatusCobranza" runat="server" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="ComisionesPoliza" role="tabpanel" aria-labelledby="ComisionesPoliza-tab">
                            <div class="containerRT7">

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblComisionPor" runat="server" Text="Comision %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComisionPor" onchange="CalculateAmountComision();SumTotalCom();" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblComision" runat="server" Text="Comision"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtComision" onchange="CalculatePercentComision();SumTotalCom();" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSobreComisionPor" runat="server" Text="SobreComision %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSobreComisionPor" onchange="CalculateAmountSobreComision();SumTotalCom();" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblSobreComision" runat="server" Text="SobreComision"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSobreComision" onchange="CalculatePercentSobreComision();SumTotalCom();" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBonoPor" runat="server" Text="Bono %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBonoPor" runat="server" onchange="CalculateAmountBono();SumTotalCom();" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblBono" runat="server" Text="Bono"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBono" runat="server" onchange="CalculatePercentBono();SumTotalCom();" CssClass="form-control" TabIndex="6">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="LblTotalCom" runat="server" Text="Total Comisiones"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTotalCom" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <%--<div class="col">
                                        <asp:Label ID="lblMesa" runat="server" Text="Mesa de Control"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMesa" runat="server" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>

                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Admin" role="tabpanel" aria-labelledby="Admin-tab">
                            <div class="containerRT8">

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDirector" runat="server" Text="Director"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtDirector" Style="width: 90%" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valDirector" runat="server" ErrorMessage="Director" ForeColor="Red" ControlToValidate="txtDirector" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblEjecutivo" runat="server" Text="Ejecutivo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEjecutivo" Style="width: 90%" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valEjecutivo" runat="server" ErrorMessage="Ejecutivo" ForeColor="Red" ControlToValidate="txtEjecutivo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtSupervisor" Style="width: 90%" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valSupervisor" runat="server" ErrorMessage="Supervisor" ForeColor="Red" ControlToValidate="txtSupervisor" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <asp:Label ID="lblGrupo" runat="server" Text="Grupo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtGrupo" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEjeAseuradora" runat="server" Text="Ejecutivo Aseguradora"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtEjeAseuradora" Style="width: 90%" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valEjeAse" runat="server" ErrorMessage="Ejecutivo Aseguradora" ForeColor="Red" ControlToValidate="txtEjeAseuradora" Text="*"></asp:RequiredFieldValidator>
                                            <div class="input-group-append">
                                                &nbsp;<a href='#' onclick="openpopup('Contacto.aspx?popup=1&custid=<%=txtAseguradora.Text%>','Ejecutivo Aseguradora','#<%=btnEjecutivoAseguradora.ClientID%>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblNegociacion" runat="server" Text="Negociación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtNegociacion" AutoPostBack="true" Style="width: 90%" runat="server" CssClass="form-control" TabIndex="6" OnSelectedIndexChanged="txtNegociacion_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="valNegociacion" runat="server" ErrorMessage="Negociacion" ForeColor="Red" ControlToValidate="txtNegociacion" Text="*"></asp:RequiredFieldValidator>
                                            <div class="input-group-append">
                                                &nbsp;<a id="openpopup" href='#' onclick="openpopup('NegociacionModal.aspx?id='+$('#<%=txtNegociacion.ClientID %> option:selected').val(),'Negociación','#<%=txtNegociacion.ClientID%>');"><img src="/Content/Imgs/writing.png" width="25" height="25" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNumCobranza" runat="server" Text="# Cobranza"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNumCobranza" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaEmision" runat="server" Text="Fecha de Emisión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaEmision" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblFechaEnvio" runat="server" Text="Fecha de Envío"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaEnvio" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="Bitacora" role="tabpanel" aria-labelledby="Bitacora-tab">
                            <div class="containerRT9">
                                <div class="row">
                                    <div class="col">
                                        <div class="input-group-append">
                                            <div class="input-group-append">
                                                &nbsp;<a onclick="openpopup('BitacoraTramite.aspx?popup=1&RelEntityId=<%=PoliId.Value %>&Tipo=0','Bitácora','#<%=btnBitacora.ClientID%>')"><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                                <div class="row">
                                     <div class="col">
                                        <asp:GridView ID="GVBitacora" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <%-- <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>--%>
                                                    <ItemTemplate>
                                                        <a  onclick="openpopup('BitacoraTramite.aspx?popup=1&id=<%# Eval("Id")%>&RelEntityId=<%# Eval("Tramite") %>&Tipo=0','Bitácora','#<%=btnBitacora.ClientID%>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="UsuarioNombre" HeaderText="Usuario" />
                                                <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
                                                <asp:BoundField DataField="Tramite" HeaderText="Tramite" Visible="false"/>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Slip" role="tabpanel" aria-labelledby="Slip-tab">
                            <div class="containerRT27">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSlip" runat="server" Text="Slip"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSlip" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="10" Columns="10" Width="100%"></asp:TextBox>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Coberturas" role="tabpanel" aria-labelledby="Coberturas-tab">
                            <div class="containerRT11">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCoberturas" runat="server" Text="Coberturas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCoberturas" CssClass="form-control" runat="server" TextMode="MultiLine" Columns="10" Rows="10" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="CondSpec" role="tabpanel" aria-labelledby="CondSpec-tab">
                            <div class="containerRT12">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCondSpec" runat="server" Text="Condiciones especiales"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCondSpec" CssClass="form-control" runat="server" TextMode="MultiLine"  Columns="10" Rows="10" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Endosos" role="tabpanel" aria-labelledby="Endosos-tab">
                            <div class="containerRT13">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchEndoso" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnBuscarEndoso" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscarEndoso_Click" CausesValidation= "false"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown" id="NuevoEndoso" runat="server">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nuevo<span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a class="dropdown-item" id="popupEndosoA" href="Endoso.aspx?endType=2198&popup=1&poliza=<%= PoliId.Value %>">Endoso A</a></li>
                                                    <li><a class="dropdown-item" id="popupEndosoB" href="Endoso.aspx?endType=2201&popup=1&poliza=<%= PoliId.Value %>">Endoso B</a></li>
                                                    <li><a class="dropdown-item" id="popupEndosoD" href="Endoso.aspx?endType=2205&popup=1&poliza=<%= PoliId.Value %>">Endoso D</a></li>
                                                </ul>
                                            </li>

                                        </ul>
                                    </div>
                                    <br />
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvEndosos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <%-- <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>--%>
                                                    <ItemTemplate>
                                                        <a id="popupEndoso" href='#' data-image='Endoso.aspx?Id=<%# Eval("Id") %>&poliza=<%= PoliId.Value %>&endType=<%# Eval("Tipo")%>'>
                                                            <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="TramiteFolio" HeaderText="# Endoso" />
                                                <asp:BoundField DataField="EstatusEndoso" HeaderText="Estatus" />
                                                <asp:BoundField DataField="TramiteVigenciaInicio" HeaderText="Inicio Vigencia" />
                                                <asp:BoundField DataField="TramiteVigenciaFin" HeaderText="Fin Vigencia" />
                                                <asp:BoundField DataField="TipoEndosoNombre" HeaderText="Tipo" />
                                                <asp:BoundField DataField="TipoMovimientoDescripcion" HeaderText="Tipo Movimiento" />
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="false" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Siniestros" role="tabpanel" aria-labelledby="Siniestros-tab">
                            <div class="containerRT14">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchSiniestro" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnBuscarSiniestro" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscarSiniestro_Click"></asp:Button>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvSiniestros" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Numero" HeaderText="Numero" />
                                                <asp:BoundField DataField="FechaSiniestro" HeaderText="Fecha Siniestro" />
                                                <asp:BoundField DataField="FechaReporte" HeaderText="Fecha Reporte" />
                                                <asp:BoundField DataField="FechaFiniquito" HeaderText="Fecha Finiquito" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Recibos" role="tabpanel" aria-labelledby="Recibos-tab">
                            <div class="containerRT15">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchRecibos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="BtnSearchRecibos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscarRecibos_Click"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <div class="input-group-append" id="divinsrecibo" runat="server">
                                                &nbsp;<a href='#' id="popupRecibo" data-image='Recibo.aspx?popup=1&poliza=<%= PoliId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvRecibos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a id="popupRecibos" href='#' data-image='Recibo.aspx?Id=<%# Eval("Id") %>'>
                                                            <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                    <asp:BoundField DataField="tramiteFolio" HeaderText="Tramite" />
                                                    <asp:BoundField DataField="numero" HeaderText="Numero" />
                                                    <asp:BoundField DataField="Total" HeaderText="Total" />    
                                                    <asp:BoundField DataField="Cobertura" HeaderText="Cobertura" />
                                                    <asp:BoundField DataField="Vencimiento" HeaderText="Vencimiento" />
                                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                                                    <asp:BoundField DataField="estatus" HeaderText="Estatus" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Comisiones" role="tabpanel" aria-labelledby="Comisiones-tab">
                            <div class="containerRT16">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col" style="visibility:hidden">
                                        <asp:TextBox ID="txtSearchComisiones" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col" style="visibility:hidden">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnSearchComisiones" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnSearchComisiones_Click" CausesValidation= "false"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <div class="input-group-append">
                                                <asp:ImageButton ID="btndeleteComisiones" data-target="#pnlModal" data-toggle="modal" OnClick="btndeleteComisiones_Click" OnClientClick="ConfirmCD();" ImageUrl="/Content/Imgs/delete.png" runat="server"  CausesValidation= "false"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <div class="input-group-append">
                                                &nbsp;<a id="popupComision" href='#' data-image='Comision.aspx?popup=1&poliza=<%= PoliId.Value %>'><img src="/Content/Imgs/add-square-button.png" width="25" height="25" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvComisiones" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a id="popupComAsoc" href='#' data-image='ComisionesAsociados.aspx?Id=<%# Eval("Id") %>'>
                                                            <img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30" /></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="UsuarioNombre" HeaderText="Nombre" />
                                                <asp:BoundField DataField="Porcentaje" HeaderText="Porcentaje" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="IncisosAutos" role="tabpanel" aria-labelledby="IncisosAutos-tab">
                            <div class="container">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col" style="flex-basis:30%" >
                                        <asp:TextBox ID="txtSearchIncisosAutos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col" style="flex-basis:5%">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnSearchIncisosAutos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnSearchIncisosAutos_Click" CausesValidation= "false"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%" >
                                        <div class="input-group-append">
                                            <asp:ImageButton ID="btnDeleteIncisos" data-target="#pnlModal" data-toggle="modal" OnClick="btnDeleteIncisos_Click" OnClientClick="Confirm();" ImageUrl="/Content/Imgs/delete.png" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:2%;width:4%">
                                        <div class="input-group-append">
                                            <a  data-target="#pnlModal" data-toggle="modal"  Onclick="openpopup('Inciso.aspx?popup=1&polizaid= <%= PoliId.Value %>','Inciso','#<%=btnrecalcIncisosAutos.ClientID%>')"><img src="/Content/Imgs/add-square-button.png"  /></a>
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:35%" >
                                        <div class="input-group-append">
                                            <p>
                                                <asp:FileUpload ID="fileUpload" CssClass="btn btn-outline-secondary" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col" style="flex-basis:15%">
                                        <div class="input-group-append">
                                            <p>
                                                <asp:Button ID="BtnImportIncisos" CssClass="btn btn-default btn-sm" Text="Importar incisos" runat="server" OnClick="BtnImportarIncisos_Click" CausesValidation= "false" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Button ID="btnSeleccionarTodos" CssClass="btn btn-default btn-sm" Text="Seleccionar Todos" runat="server" OnClick="btnSeleccionarTodos_Click" CausesValidation= "false" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnDeseleccionarTodos" CssClass="btn btn-default btn-sm" Text="Deseleccionar Todos" runat="server" OnClick="btnDeseleccionarTodos_Click" CausesValidation= "false" />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvIncisosAutos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <a  onclick="openpopup('Inciso.aspx?popup=1&id=<%# Eval("Id")%>&polizaid=<%# Eval("Poliza") %>','Inciso','#<%= btnSearchIncisosAutos.ClientID%>')"><img src="/Content/Imgs/edit.png" alt="Editar" height="30" width="30"/></a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="AutoSerie" HeaderText="Serie" />
                                                <asp:BoundField DataField="AutoMotor" HeaderText="Motor" />
                                                <asp:BoundField DataField="AutoPlacas" HeaderText="Placas" />
                                                <asp:BoundField DataField="AutoModelo" HeaderText="Modelo" />
                                                <asp:BoundField DataField="Prima" HeaderText="Prima" />
                                                <asp:BoundField DataField="Estatus" HeaderText ="Estatus" />
                                                <asp:BoundField DataField="Poliza" Visible ="false" />
                                               

                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="AutoIndiv" role="tabpanel" aria-labelledby="AutoIndiv-tab">
                            <div class="containerRT18">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblInciso" runat="server" Text="Inciso"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtInciso" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSerie" runat="server" Text="Serie"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSerie" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RequiredFieldValidator ID="ReqtxtSerie" runat="server" ErrorMessage="Serie" ForeColor="Red" ControlToValidate="txtSerie" Text="*"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblMotor" runat="server" Text="Motor"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtMotor" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtMotor" runat="server" ErrorMessage="Motor" ForeColor="Red" ControlToValidate="txtMotor" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblPlacas" runat="server" Text="Placas"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtPlacas" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblConductor" runat="server" Text="Conductor Habitual"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtConductor" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblMarca" runat="server" Text="Marca"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtMarca" runat="server" CssClass="form-control" TabIndex="6">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReqtxtMarca" runat="server" ErrorMessage="Marca" ForeColor="Red" ControlToValidate="txtMarca" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblVersion" runat="server" Text="Versión"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVersion" runat="server" CssClass="form-control" TabIndex="7">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtVersion" runat="server" ErrorMessage="Versión" ForeColor="Red" ControlToValidate="txtVersion" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblModelo" runat="server" Text="Modelo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtModelo" runat="server" CssClass="form-control" TabIndex="8">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtModelo" runat="server" ErrorMessage="Modelo" ForeColor="Red" ControlToValidate="txtModelo" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBeneficiarioAuto" runat="server" Text="Beneficiario preferente"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtBeneficiarioAuto" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group input-group">
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="VidaIndividual" role="tabpanel" aria-labelledby="VidaIndividual-tab">
                            <div class="containerRT19">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblNumCertificado" runat="server" Text="Número certificado"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtNumCertificado" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtNumCertificado" runat="server" ErrorMessage="Número certificado" ForeColor="Red" ControlToValidate="txtNumCertificado"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblName" runat="server" Text="VINombre"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVINombre" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtVINombre" runat="server" ErrorMessage="VINombre" ForeColor="Red" ControlToValidate="txtVINombre" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblVIAPaterno" runat="server" Text="Apellido Paterno"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVIAPaterno" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtVIAPaterno" runat="server" ErrorMessage="VI Apellido Paterno" ForeColor="Red" ControlToValidate="txtVIAPaterno" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblVIAMaterno" runat="server" Text="Apellido Materno"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtVIAMaterno" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtVIAMaterno" runat="server" ErrorMessage="VI Apellido Materno" ForeColor="Red" ControlToValidate="txtVIAMaterno" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblBirthDay" runat="server" Text="Fecha de Nacimiento"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtBirthDay" runat="server" CssClass="form-control" TabIndex="5">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtBirthDay" runat="server" ErrorMessage="Fecha de Nacimiento" ForeColor="Red" ControlToValidate="txtBirthDay" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSex" runat="server" Text="Sexo"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtSex" runat="server" CssClass="form-control" TabIndex="6">
                                                <asp:ListItem Value="0" Text="Masculino"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Femenino"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblAntiguedad" runat="server" Text="Antigüedad "></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtAntiguedad" runat="server" CssClass="form-control" TabIndex="7">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtAntiguedad" runat="server" ErrorMessage="Antigüedad" ForeColor="Red" ControlToValidate="txtAntiguedad" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <div class="form-group input-group">
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divDependientes" runat="server" class="container">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchDependientes" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnSearchDependientes" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnSearchDependientes_Click"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <p>
                                                <asp:FileUpload ID="fUpDependientes" CssClass="btn btn-outline-secondary" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <p>
                                                <asp:Button ID="btnUplDependientes" CssClass="btn btn-default btn-sm" OnClick="btnUplDependientes_Click" Text="Importar Dependientes" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvDependientes" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="CertificadoNombre" HeaderText="Nombre" />
                                                <asp:BoundField DataField="CertificadoApellidoPaterno" HeaderText="A. Paterno" />
                                                <asp:BoundField DataField="CertificadoApellidoMaterno" HeaderText="A. Materno" />
                                                <asp:BoundField DataField="CertificadoFechaNacimiento" HeaderText="Fecha Nacimiento" />
                                                <asp:BoundField DataField="CertificadoParentesco" HeaderText="Parentesco" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="GMMIndividual" role="tabpanel" aria-labelledby="GMMIndividual-tab">
                            <div class="containerRT20">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblDeduciblePrc" runat="server" Text="Deducible %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDeduciblePrc" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtDeduciblePrc" runat="server" ErrorMessage="Deducible %" ForeColor="Red" ControlToValidate="txtDeduciblePrc" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtDeduciblePrcVal" ControlToValidate="txtDeduciblePrc" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblCoaseguroPrc" runat="server" Text="Coaseguro %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCoaseguroPrc" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtCoaseguroPrc" runat="server" ErrorMessage="Coaseguro %" ForeColor="Red" ControlToValidate="txtCoaseguroPrc" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtCoaseguroPrcVal" ControlToValidate="txtCoaseguroPrc" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSumAsegurada" runat="server" Text="Suma asegurada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAsegurada" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumAsegurada" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtSumAsegurada"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumAseguradaVal" ControlToValidate="txtSumAsegurada" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSumAsegSMGM" runat="server" Text="Suma asegurada SMGM"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAsegSMGM" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumAsegSMGM" runat="server" ErrorMessage="Suma asegurada SMGM" ForeColor="Red" ControlToValidate="txtSumAsegSMGM" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumAsegSMGMVal" ControlToValidate="txtSumAsegSMGM" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="VidaPool" role="tabpanel" aria-labelledby="VidaPool-tab">
                            <div class="containerRT21">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSumAsegVP" runat="server" Text="Suma Asegurada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAsegVP" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumAsegVP" runat="server" ErrorMessage="Suma Asegurada" ForeColor="Red" ControlToValidate="txtSumAsegVP" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumAsegVPVal" ControlToValidate="txtDeduciblePrc" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSumAsegSMGMVP" runat="server" Text="Suma Asegurada SMGM"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAsegSMGMVP" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumAsegSMGMVP" runat="server" ErrorMessage="Suma Asegurada SMGM" ForeColor="Red" ControlToValidate="txtSumAsegSMGMVP" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumAsegSMGMVPVal" ControlToValidate="txtSumAsegSMGMVP" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSumaAsegTopadaVP" runat="server" Text="Suma asegurada topada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumaAsegTopadaVP" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumaAsegTopadaVP" runat="server" ErrorMessage="Suma asegurada topada" ForeColor="Red" ControlToValidate="txtSumaAsegTopadaVP" Text="*"></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumaAsegTopadaVPVal" ControlToValidate="txtSumaAsegTopadaVP" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSumaAsegTopadaSMGMVP" runat="server" Text="Suma asegurada SMGM"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumaAsegTopadaSMGMVP" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqtxtSumaAsegTopadaSMGMVP" runat="server" ErrorMessage="Suma asegurada SMGM" ForeColor="Red" ControlToValidate="txtSumaAsegTopadaSMGMVP" Text="*" ></asp:RequiredFieldValidator>
                                        </div>
                                        
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumaAsegTopadaSMGMVPVal" ControlToValidate="txtSumaAsegTopadaSMGMVP" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="GMMPool" role="tabpanel" aria-labelledby="GMMPool-tab">
                            <div class="containerRT22">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblEsqPrimaMinGMMPool" runat="server" Text="Esquema prima mínima"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtEsqPrimaMinPool" Text="0" runat="server" CssClass="form-control" TabIndex="1">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" ID="txtEsqPrimaMinGMMPoolVal" ControlToValidate="txtEsqPrimaMinPool" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblDeducPctGMMPool" runat="server" Text="Deducible %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtDeducPctGMMPool" Text="0" runat="server" CssClass="form-control" TabIndex="2">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" ID="txtDeducPctGMMPoolVal" ControlToValidate="txtDeducPctGMMPool" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCoaseguroPctGMMPool" runat="server" Text="Coaseguro %"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtCoaseguroPctGMMPool" Text="0" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" ID="CoaseguroPctGMMPoolVal" ControlToValidate="txtCoaseguroPctGMMPool" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <asp:Label ID="lblSumAseguradaGMMPool" runat="server" Text="Suma asegurada"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAseguradaGMMPool" Text="0" runat="server" CssClass="form-control" TabIndex="4">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" ID="SumAseguradaGMMPoolVal" ControlToValidate="txtSumAseguradaGMMPool" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblSumAsegSMGMGMMPool" runat="server" Text="Suma asegurada SMGM"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtSumAsegSMGMGMMPool" Text="0" runat="server" CssClass="form-control" TabIndex="3">
                                            </asp:TextBox>
                                        </div>
                                        <asp:RegularExpressionValidator runat="server" ID="txtSumAsegSMGMGMMPoolVal" ControlToValidate="txtSumAsegSMGMGMMPool" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                    </div>
                                    <div class="col">
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="VPGrupos" role="tabpanel" aria-labelledby="VPGrupos-tab">
                            <div class="container23">
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:TextBox ID="txtSearchVPGrupos" runat="server" CssClass="form-control" type="search" placeholder="Id a buscar" aria-label="Id a buscar"></asp:TextBox>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <asp:Button ID="btnSearchVPGrupos" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnSearchVPGrupos_Click"></asp:Button>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <p>
                                                <asp:FileUpload ID="fUploadGrupos" CssClass="btn btn-outline-secondary" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group-append">
                                            <p>
                                                <asp:Button ID="btnImportVPGrupos" CssClass="btn btn-default btn-sm" OnClick="btnImportVPGrupos_Click" Text="Importar Grupos" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <br />
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:GridView ID="gvVPGrupos" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true" runat="server">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkEliminar" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" />
                                            </Columns>
                                            <pagersettings mode="NextPreviousFirstLast"
                                    FirstPageImageUrl="Content/Imgs/first.png"
                                    LastPageImageUrl="Content/Imgs/last.png"
                                    NextPageImageUrl="Content/Imgs/next.png"
                                    PreviousPageImageUrl="Content/Imgs/previous.png"   
                                    position="Bottom"/> 
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="Auditoria" role="tabpanel" aria-labelledby="Auditoria-tab">
                            <div class="container24">
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblID" runat="server" Text="Id"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtId" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUsuarioAlta" runat="server" Text="UsuarioAlta"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUsuarioAlta" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaAlta" runat="server" Text="Fecha Alta"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaAlta" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUSuarioUltimaMod" runat="server" Text="Usuario Última Modificacion"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtUsuarioUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFechaUltimaMod" runat="server" Text="Fecha Última Modificacion"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFechaUltimaMod" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblUbicacion" runat="server" Text="Ubicación"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtUbicacion" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblCentroBeneficio" runat="server" Text="Centro Beneficios"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtCentroBeneficio" runat="server" CssClass="form-control" TabIndex="9">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="PaneName" runat="server" />
                <asp:HiddenField ID="PoliId" runat="server" />
                <asp:HiddenField ID="openurlatstart" runat="server"/>
                <asp:HiddenField ID="CustRFC" runat="server" />
                <asp:HiddenField ID="SearchIdEndoso" runat="server" />
                <asp:HiddenField ID="SearchIdRecibo" runat="server" />
                <asp:HiddenField ID="SearchIdSiniestro" runat="server" />
                <asp:HiddenField ID="SearchIdComisiones" runat="server" />
                <asp:HiddenField ID="SearchIdInciso" runat="server" />
                <asp:HiddenField ID="SearchVPGrupo" runat="server" />
                <asp:HiddenField ID="SearchDependientes" runat="server" />
                <asp:HiddenField ID="PolizaType" runat="server" />
                <asp:HiddenField ID="PolizaTypeStr" runat="server" />
                <asp:HiddenField ID="PolizaOT" runat="server" />
                <asp:HiddenField ID="IncisoImportMsg" runat="server" />
                <asp:HiddenField ID="idplantilla" runat="server" />
                <asp:Button ID="btnTitular" runat="server" OnClick="btnTitular_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation="false"/>
                <asp:Button ID="btnEjecutivoAseguradora" runat="server" OnClick="btnEjecutivoAseguradora_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
                <asp:Button ID="btnBitacora" runat="server" OnClick="btnBitacora_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
                <asp:Button ID="btnrecalcIncisosAutos" runat="server" OnClick="btnrecalcIncisosAutos_Click" style="visibility: hidden; display: none;" UseSubmitBehavior="false" CausesValidation= "false"/>
                
                <div class="modal" id="ConfirmDelete">
           
                </div>  
                <div class="modal fade" id="exampleModal" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Descartar orden de trabajo</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container" runat="server">
                                    <div class="panel-heading">
                                        <asp:Label ID="Label1" CssClass="panel-title" runat="server">Descartar orden de trabajo</asp:Label>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblPoliza" runat="server" Text="No. Póliza:"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtPoliza" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                                <asp:RequiredFieldValidator ID="NoPoliza" runat="server" ErrorMessage="Campo Requerido" ForeColor="Red" ControlToValidate="txtPoliza" ValidationGroup="Descarte"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblrecibos" runat="server" Text="¿Generar recibos?"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:DropDownList ID="txtgenRecibos" runat="server" CssClass="form-control" >
                                                        <asp:ListItem Value="1">Si</asp:ListItem>
                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblNumRecibos" runat="server" Text="¿Cuántos recibos?"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtNumRecibos" runat="server" TextMode="Number" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblPrimaNeta" runat="server" Text="P. Neta primer recibo"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtPrimaNeta" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                                <asp:RegularExpressionValidator runat="server" ID="txtPrimaNetaVal" ControlToValidate="txtPrimaNeta" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblGastosRecibo" runat="server" Text="Gastos primer recibo"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtGastosRecibo" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                                <asp:RegularExpressionValidator runat="server" ID="txtGastosReciboVal" ControlToValidate="txtGastosRecibo" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$" ErrorMessage="Por favor capturar un valor numérico" ForeColor="Red" />
                                            </div>
                                        </div>
                                        <div class="row" id ="Prorrateo" runat="server">
                                            <div class="col">
                                                <asp:Label ID="lblProrrateo" runat="server" Text="Prorratear Derechos"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:CheckBox ID="txtProrratear" runat="server" class="form-control"></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Button ID="btnDescarteOk" OnClick="btnDescarteOk_Click" runat="server" Text="Descarte" CausesValidation= "true" ValidationGroup="Descarte" />
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="ConfirmCartas" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ConfirmCartasLabel">Generar Cartas</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="containerRT22" runat="server">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblSendEmail" runat="server" Text="Enviar por correo electronico"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtEmailSend" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <asp:Label ID="lblSubject" runat="server" Text="Asunto"></asp:Label>
                                                <div class="form-group input-group">
                                                    <asp:TextBox ID="txtSubject" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <asp:Button ID="btnSendEmail" OnClick="btnSendEmail_Click" runat="server" Text="Enviar correo" />
                                            </div>
                                            <div class="col">
                                                <asp:Button ID="btnDescargar" OnClick="btnDescargar_Click" runat="server" Text="Descargar" />
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
