﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class CustomerList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                searchForCust();
            }
        }

        protected void GridList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVCustList.PageIndex = e.NewPageIndex;
            GVCustList.DataBind();
            searchForCust();
        }

        private void searchForCust()
        {
            string errmess = "";
            Entity cust = new Entity("Cliente");
            
            cust.Attributes.Add(new Models.Attribute("id", "", "string"));
            cust.Attributes.Add(new Models.Attribute("NombreCompleto", "", "string"));
            cust.Attributes.Add(new Models.Attribute("rfc", "", "string"));
            //cust.Attributes.Add(new Models.Attribute("Audit_Estatus", "", "string"));
            cust.logicalOperator = 2;
            if (SearchId.Value != "")
            {
                Int64 id = 0;

                if (Int64.TryParse(SearchId.Value.Replace("%", ""), out id))
                {
                    cust.Keys.Add(new Models.Key("Id", id.ToString()+"%", "string",2));
                }
                else
                {
                    cust.Keys.Add(new Models.Key("NombreCompleto", SearchId.Value, "string",2));
                    cust.Keys.Add(new Models.Key("RFC", SearchId.Value, "string",2));
                }
            }
            //Se agrega correccion no existe la columna Estatus
                cust.Keys.Add(new Models.Key("Estatus", "1", "int",1,1,"a"));
                //cust.Keys.Add(new Models.Key("EstatusCliente", "1", "int", 1, 1, "cliente"));
            
            List<Entity> listLeads = SiteFunctions.GetValues(cust, out errmess);
            DataTable dtLeadList = SiteFunctions.trnsformEntityToDT(listLeads);
            GVCustList.DataSource = dtLeadList;
            GVCustList.DataBind();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            foreach (GridViewRow row in GVCustList.Rows)
            {
                CheckBox chkBox = row.FindControl("chkEliminar") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string id = GVCustList.Rows[row.RowIndex].Cells[1].Text;
                        Response.Redirect(string.Format("Cliente.aspx?Id={0}", id));
                    }
                }
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text +'%';
            searchForCust();
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = '%' + txtSearch.Text + '%';
            searchForCust();
        }

        
    }
}