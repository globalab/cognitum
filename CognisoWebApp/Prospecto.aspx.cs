﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class Prospecto : Page
    {
        const string ENTITYNAME = "prospecto";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.Page.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
            }
            catch (Exception)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                OwnerId.Value = string.Empty;
                btnRechazar.Attributes["onclick"] = "ShowModal(); return false;";  
              
                btnConvLead.Visible = false;
                btnRechazar.Visible = false;
                btnEnviarValidacionCobranza.Visible = false;
                btnDelete.Visible = false;
                //  txtEstatusCliente.Enabled = false;
                initCatalog();
                object id = Request.QueryString["id"];
                object created = Request.QueryString["created"];
                //LeadId.Value = id.ToString(); Se asigna antes de validar si no es null
                BtnSave.Visible = true;
                if (id != null)
                {
                    BtnSave.Visible = true;
                    btnEnviarValidacionCobranza.Visible = true;
                    btnDelete.Visible = true;
                    //Se agrega correción
                    LeadId.Value = id.ToString();
                    this.mapCurrentLead(id.ToString());
                    if (created != null)
                    {
                        if (created.ToString() == "1")
                        {
                            this.showMessage("Prospecto guardado correctamente", false);
                        }
                        else
                        {
                            this.showMessage("El prospecto fue enviado para validación", false);
                        }
                    }
                }
            }
            else
            {
                this.showTab();
                
             
            }
        }

        

        private void mapCurrentLead(string _id)
        {
            List<Entity> entities = new List<Entity>();
            string errMsg = string.Empty;
            Entity search = new Entity(ENTITYNAME);
            search.Keys.Add(new Models.Key("id", _id, "int"));
            Entity ContactS = new Entity("ContactoSimple");
            ContactS.Keys.Add(new Models.Key("cliente", _id, "int"));
            
            entities.Add(search);
            entities.Add(ContactS);
            

            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            //List<Entity> leadResult = SiteFunctions.GetValues(search, out errMsg);
            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if(opResult.RetVal.Count > 0)
                    { 
                        Entity lead = opResult.RetVal[0];
                        switch (lead.EntityName.ToLower())
                        {
                            case "prospecto":
                                txtRFC.Text = lead.getAttrValueByName("RFC");
                                txtExtranjero.Checked = lead.getAttrValueByName("Tipo") == "2";
                                cmbTipoPersona.Text = lead.getAttrValueByName("TipoPersona");

                                if (cmbTipoPersona.SelectedValue == "2")
                                {
                                    txtRazonSocial.Text = lead.getAttrValueByName("RazonSocial");



                                }
                                else
                                {
                                    txtName.Text = lead.getAttrValueByName("Nombre");
                                    txtMiddleName.Text = lead.getAttrValueByName("ApellidoPaterno");
                                    txtLastName.Text = lead.getAttrValueByName("ApellidoMaterno");
                                    DateTime fecha = Convert.ToDateTime(lead.getAttrValueByName("FechaNacimiento"));
                                    txtDate.Text = fecha.ToString("dd/MM/yyyy");
                                    txtCurp.Text = lead.getAttrValueByName("Curp");
                                    txtEmail.Text = lead.getAttrValueByName("Email");
                                }
                                txtEstatusCliente.SelectedValue = lead.getAttrValueByName("Estatus");
                                txtGroup.SelectedValue = lead.getAttrValueByName("Grupo");
                                txtGiro.Text = lead.getAttrValueByName("Giro");
                                txtCPFiscal.Text = lead.getAttrValueByName("CP");
                                txtCPEnvio.Text = lead.getAttrValueByName("CPEnv");
                                txtCalleFiscal.Text = lead.getAttrValueByName("Calle");
                                txtCalleEnvio.Text = lead.getAttrValueByName("CalleEnv");
                                txtNumeroExt.Text = lead.getAttrValueByName("NumExterior");
                                txtNumeroExtEnv.Text = lead.getAttrValueByName("NumExteriorEnv");
                                txtNumeroInt.Text = lead.getAttrValueByName("NumInterior");
                                txtNumeroIntEnv.Text = lead.getAttrValueByName("NumInteriorEnv");

                                settxtCPFiscal(txtCPFiscal.Text);
                                txtColoniaFiscal.SelectedValue = lead.getAttrValueByName("Colonia");
                                settxtCPEnvio(txtCPEnvio.Text);
                                txtColoniaEnvio.SelectedValue = lead.getAttrValueByName("ColoniaEnv");

                                // txtPaisEnvio.Text = lead.getAttrValueByName("PaisEnv");
                                // txtPaisFacturacion.Text = lead.getAttrValueByName("Pais");
                                txtPaisFacturacion.Text = "México";
                                txtPaisEnvio.Text = "México";

                                txtEstadoFacturacion.Text = lead.getAttrValueByName("Estado");
                                txtEstadoEnvio.Text = lead.getAttrValueByName("EstadoEnv");
                                txtDelegacionFacturacion.Text = lead.getAttrValueByName("Delegacion");
                                txtDelegacionEnvio.Text = lead.getAttrValueByName("DelegacionEnv");
                                txtOficina.Text = lead.getAttrValueByName("TelOficina");
                                txtCelular.Text = lead.getAttrValueByName("TelCelular");
                                txtExtension.Text = lead.getAttrValueByName("Ext");
                                txtNombreCA.Text = lead.getAttrValueByName("CAltNombre");
                                txtEmailCA.Text = lead.getAttrValueByName("CALTEmial");
                                txtTelefonoCA.Text = lead.getAttrValueByName("CALTTelefono");
                                //txtPuestoCA.Text = lead.getAttrValueByName("CALTPuesto");
                                OwnerId.Value = lead.getAttrValueByName("Propietario");
                                txtId.Text = _id;
                                try
                                {
                                    DateTime fechaaudit = Convert.ToDateTime(lead.getAttrValueByName("FechaAdd"));

                                    txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                    fechaaudit = Convert.ToDateTime(lead.getAttrValueByName("FechaUMod"));
                                    txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                }
                                catch (Exception)
                                {

                                }
                                Entity UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", lead.getAttrValueByName("UsuarioAdd"), "int64"));
                                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", lead.getAttrValueByName("UsuarioUMod"), "int64"));
                                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }


                                break;

                        }
                    }
                    
                }
            }

            loadcomments(_id);


            showPanels();

            if (txtEstatusCliente.SelectedValue == "3")
            {
                btnConvLead.Visible = false;
                btnRechazar.Visible = false;
                btnEnviarValidacionCobranza.Visible = false;
                BtnSave.Visible = false;
                //txtEstatusCliente.Enabled = false;
            }
            else
            {
                if (txtEstatusCliente.SelectedValue == "2")
                {
                    if (SiteFunctions.IsUsuarioCobranza(Context.User.Identity.Name))
                    {
                        btnConvLead.Visible = true;
                        btnRechazar.Visible = true;
                        btnEnviarValidacionCobranza.Visible = false;
                    }
                    // txtEstatusCliente.Enabled = false;
                }
                else
                {
                    
                    btnConvLead.Visible = false;
                    btnRechazar.Visible = false;
                    //txtEstatusCliente.Enabled = true;
                }
            }

        }

        private void loadcomments(string id)
        {
            string errmess = string.Empty;
            Entity Comments = new Entity("ComentariosAut");
            Comments.useAuditable = false;
            
            Comments.Keys.Add(new Models.Key("IdRegistroRelacionado", id, "int"));
            JoinEntity UserE = new JoinEntity();
            UserE.ChildEntity = new Entity("Usuario");
            UserE.ChildEntity.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            UserE.JoinKey = new Models.Attribute("Id");
            UserE.JoinType = JoinType.Left;
            selectJoinEntity useresj = new selectJoinEntity("Usuario", 1, "Id");
            UserE.selectJoinList.Add(useresj);
            Comments.ChildEntities.Add(UserE);
            List<Entity> retComment = SiteFunctions.GetValues(Comments, out errmess);
            DataTable dt = new DataTable();
            dt = SiteFunctions.trnsformEntityToDT(retComment);
            GVCommentList.DataSource = dt;
            GVCommentList.DataBind();
        }

        private void initCatalog()
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Giro = new Entity("Giro");
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Giro.useAuditable = false;
            listResult = SiteFunctions.GetValues(Giro, out errmess);
            txtGiro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGiro.DataValueField = "Id";
            txtGiro.DataTextField = "Nombre";
            txtGiro.DataBind();

            Entity Grupo = new Entity("Grupo");
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Grupo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Grupo, out errmess);
            txtGroup.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGroup.DataValueField = "Id";
            txtGroup.DataTextField = "Nombre";
            txtGroup.DataBind();

            Entity Estado = new Entity("Estado");
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Estado.useAuditable = false;
            listResult = SiteFunctions.GetValues(Estado, out errmess);
            DataTable Edos = SiteFunctions.trnsformEntityToDT(listResult);
            txtEstadoFacturacion.DataSource = Edos;
            txtEstadoFacturacion.DataValueField = "Id";
            txtEstadoFacturacion.DataTextField = "Nombre";
            txtEstadoFacturacion.DataBind();

            txtEstadoEnvio.DataSource = Edos;
            txtEstadoEnvio.DataValueField = "Id";
            txtEstadoEnvio.DataTextField = "Nombre";
            txtEstadoEnvio.DataBind();

            Array itemValues = System.Enum.GetValues(typeof(EstatusProspectoEnum));
            Array itemNames = System.Enum.GetNames(typeof(EstatusProspectoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                txtEstatusCliente.Items.Add(item);
            }
        }

        protected void btnValidate_Click1(object sender, EventArgs e)
        {
            valLead();
        }

        private void showPanels()
        {
            Prospectodiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
            //General.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //Direcciones.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //Telefono.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //Contacto.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            //ContactoAlterno.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";

            if (cmbTipoPersona.SelectedValue == "2")
            {
                PF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                //PF.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "0px";
                PM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                //PFisica.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "10px";
                valRazSoc.Enabled = true;
                contalt.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                valName.Enabled = false;
                valApPat.Enabled = false;
                tel.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            }
            else
            {
                PF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                ////PFisica.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "10px";
                PM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                //PMoral.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "0px";
                tel.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                valName.Enabled = true;
                valApPat.Enabled = true;
                valRazSoc.Enabled = false;
                contalt.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";

            }
        }

        public bool rfcValido(string rfc, out string errmess, bool aceptarGenerico = true)
        {
            string re = @"^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$";
            Match validado = Regex.Match(rfc, re, RegexOptions.IgnoreCase);

            errmess = "";
            if (!validado.Success)  //Coincide con el formato general del regex?
            {
                errmess = "El RFC no coincide con la estructura requerida";
                return false;
            }

            //Separar el dígito verificador del resto del RFC
            string digitoVerificador = rfc.Substring(rfc.Length - 1, 1);
            string rfcSinDigito = rfc.Substring(0, rfc.Length - 1) + "";
            int len = rfcSinDigito.Length;

            //Obtener el digito esperado
            string diccionario = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ";
            int indice = len + 1;
            int suma = 0;
            int digitoEsperado;
            string digitoEsperados = "";
            if (len == 12) suma = 0;
            else suma = 481; //Ajuste para persona moral

            if (len != 11 && cmbTipoPersona.SelectedValue == "2")
            {
                errmess = "El RFC no coincide con la longitud de RFC de personas morales";
                return false;
            }

            if (len != 12 && cmbTipoPersona.SelectedValue == "1")
            {
                errmess = "El RFC no coincide con la longitud de RFC de personas físicas";
                return false;
            }
            for (var i = 0; i < len; i++)
                suma += diccionario.IndexOf(rfcSinDigito[i]) * (indice - i);
            digitoEsperado = (11 - suma % 11);
            digitoEsperados = diccionario[digitoEsperado].ToString();
            if (digitoEsperado == 11) digitoEsperados = "0";
            else if (digitoEsperado == 10) digitoEsperados = "A";

            if (digitoVerificador != digitoEsperados)
            {
                
                errmess = "El RFC no coincide con la estructura de RFC";
                
                return false;
            }
            //El dígito verificador coincide con el esperado?
            // o es un RFC Genérico (ventas a público general)?
            if ((digitoVerificador != digitoEsperados)
             && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
            {
                errmess = "El RFC no coincide con la estructura de RFC";
                return false;
            }
            else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
            {
                return false;
            }
            return true;
        }

        private void valLead()
        {
            string errmes = "";
            if (txtRFC.Text != "")
            {
                if (rfcValido(txtRFC.Text.Trim(), out errmes, true))
                {
                    if (SiteFunctions.ValidaProspecto(txtRFC.Text, txtExtranjero.Checked ? 2 : 1, cmbTipoPersona.SelectedValue == "2" ? 2 : 1, out errmes))
                    {
                        //BtnSave.Visible = true;
                        showPanels();
                    }
                    else
                    {
                        showMessage(errmes, true);
                        Prospectodiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                    }
                }
                else
                {

                    showMessage(errmes, true);
                    Prospectodiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                }
            }
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        private void showTab()
        {
            
            // Define the name and type of the client scripts on the page.
            String csname1 = "TabScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> collapsediv(); </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            saveProspecto();
        }

        protected void txtCPFiscal_TextChanged(object sender, EventArgs e)
        {
            if (txtCPFiscal.Text != "" && txtCPFiscal.Text.Length == 5)
            {
                //txtEstadoFacturacion.Enabled = true;
                //txtPaisFacturacion.Enabled = true;
                //txtDelegacionFacturacion.Enabled = true;
                PaneName.Value = "Direcciones";
                string errmess = "";
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", txtCPFiscal.Text, "string"));
                List<Entity> listResult = SiteFunctions.GetValues(CP, out errmess);
                DataTable Dt = SiteFunctions.trnsformEntityToDT(listResult);
                if (errmess == "" && Dt.Rows.Count > 0)
                {

                    txtColoniaFiscal.DataSource = Dt;
                    txtColoniaFiscal.DataTextField = "Colonia";
                    txtColoniaFiscal.DataValueField = "Colonia";
                    txtColoniaFiscal.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacionFacturacion.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstadoFacturacion.Items.FindByValue(txtEstadoFacturacion.SelectedValue).Selected = false;
                        txtEstadoFacturacion.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;
                    }

                    txtPaisFacturacion.Text = "México";
                 
                }
                else
                {
                    this.showMessage("El CP. " + txtCPFiscal.Text + " no existe favor de validar o darlo de alta.", true);
                    txtCPFiscal.Text = string.Empty;
                }

            }
            else
            {
                PaneName.Value = "Direcciones";
                if (txtCPFiscal.Text.Length != 5 && txtCPFiscal.Text != "")
                {
                    this.showMessage("El CP. debe ser de 5 números.", true);
                    txtCPFiscal.Text = string.Empty;
                }
            }


        }

        protected void txtCPEnvio_TextChanged(object sender, EventArgs e)
        {
            if (txtCPEnvio.Text != "" && txtCPEnvio.Text.Length == 5)
            {
             

                PaneName.Value = "Direcciones";
                string errmess = ""; ;
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", txtCPEnvio.Text, "string"));
                List<Entity> listEntity = SiteFunctions.GetValues(CP, out errmess);

                DataTable Dt = SiteFunctions.trnsformEntityToDT(listEntity);
                if (errmess == "" && Dt.Rows.Count > 0)
                {
                    txtColoniaEnvio.DataSource = Dt;
                    txtColoniaEnvio.DataTextField = "Colonia";
                    txtColoniaEnvio.DataValueField = "Colonia";
                    txtColoniaEnvio.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacionEnvio.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstadoEnvio.Items.FindByValue(txtEstadoEnvio.SelectedValue).Selected = false;
                        txtEstadoEnvio.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;

                    }

                    txtPaisEnvio.Text = "México";
              
                }
                else
                {
                    this.showMessage("El CP. " + txtCPEnvio.Text.Trim() + " no existe favor de validar o darlo de alta.", true);
                    txtCPEnvio.Text = string.Empty;
                }

            }
            else
            {
                PaneName.Value = "Direcciones";
                if (txtCPEnvio.Text.Length != 5 && txtCPEnvio.Text != "")
                {
                    this.showMessage("El CP. debe ser de 5 números.", true);
                    txtCPEnvio.Text = string.Empty;
                }
            }
        }

        protected void settxtCPFiscal(string cpFiscal)
        {
            if (cpFiscal != "" && cpFiscal.Length == 5)
            {
                //txtEstadoFacturacion.Enabled = true;
                //txtPaisFacturacion.Enabled = true;
                //txtDelegacionFacturacion.Enabled = true;
                PaneName.Value = "collapseFour";
                string errmess = "";
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", cpFiscal, "string"));
                List<Entity> listResult = SiteFunctions.GetValues(CP, out errmess);
                DataTable Dt = SiteFunctions.trnsformEntityToDT(listResult);
                if (errmess == "" && Dt.Rows.Count > 0)
                {
                    txtColoniaFiscal.DataSource = Dt;
                    txtColoniaFiscal.DataTextField = "Colonia";
                    txtColoniaFiscal.DataValueField = "Colonia";
                    txtColoniaFiscal.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacionFacturacion.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstadoFacturacion.Items.FindByValue(txtEstadoFacturacion.SelectedValue).Selected = false;
                        txtEstadoFacturacion.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;
                        txtPaisFacturacion.Text = "México";
                    }

                    //txtEstadoFacturacion.Enabled = false;
                    //txtPaisFacturacion.Enabled = false;
                    //txtDelegacionFacturacion.Enabled = false;
                }


            }


        }
        protected void settxtCPEnvio(string cpEnvio)
        {
            if (cpEnvio != "" && cpEnvio.Length == 5)
            {
               

                PaneName.Value = "collapseFour";
                string errmess = ""; ;
                Entity CP = new Entity("CodigoPostal");
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Municipio"));
                CP.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado"));
                CP.Keys.Add(new CognisoWebApp.Models.Key("Id", cpEnvio, "string"));
                List<Entity> listEntity = SiteFunctions.GetValues(CP, out errmess);
                DataTable Dt = SiteFunctions.trnsformEntityToDT(listEntity);
                if (errmess == "")
                {
                    txtColoniaEnvio.DataSource = Dt;
                    txtColoniaEnvio.DataTextField = "Colonia";
                    txtColoniaEnvio.DataValueField = "Colonia";
                    txtColoniaEnvio.DataBind();

                    if (Dt.Rows.Count > 0)
                    {
                        txtDelegacionEnvio.Text = Dt.Rows[0]["Municipio"].ToString();
                        txtEstadoEnvio.Items.FindByValue(txtEstadoEnvio.SelectedValue).Selected = false;
                        txtEstadoEnvio.Items.FindByValue(Dt.Rows[0]["Estado"].ToString()).Selected = true;
                        txtPaisEnvio.Text = "México";
                    }

                 
                }

            }
        }


        protected void btnConvLead_Click(object sender, ImageClickEventArgs e)
        {
            Entity lead = new Entity("prospecto");
            object id = Request.QueryString["id"];
            int userId = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userId);
            if (id != null)
            {
                string errMsg = string.Empty, idCust = string.Empty;

                lead.Keys.Add(new Models.Key("id", id.ToString(), "int"));
                lead.Action = 1;

                lead.auditable = new Auditable(userId, DateTime.Now, 1);
                bool Result = SiteFunctions.SaveProspect(lead, Method.PUT, out errMsg, out idCust);
                if (Result)
                {
                    //actualizamos prospecto cmapo estatus
                    lead = new Entity("prospecto");
                    lead.Keys.Add(new Models.Key("id", id.ToString(), "int"));
                    lead.Attributes.Add(new Models.Attribute("Estatus", "3", "int"));
                    lead.Attributes.Add(new Models.Attribute("Cliente", idCust.ToString(), "int"));
                    SiteFunctions.SaveEntity(lead, Method.PUT, out errMsg);

                    Entity UserOWner = new Entity("Usuario");
                    UserOWner.Attributes.Add(new Models.Attribute("Email"));
                    UserOWner.Keys.Add(new Key("id", OwnerId.Value, "int64"));
                    List<Entity> listEntity = SiteFunctions.GetValues(UserOWner, out errMsg);
                    if (listEntity.Count > 0)
                    {
                        string email = listEntity[0].getAttrValueByName("Email");
                        if (email != "")
                        {
                            SiteFunctions funct = new SiteFunctions();
                            string body = string.Empty;
                            try
                            {
                                string leadName = string.Empty;
                                if (cmbTipoPersona.SelectedValue == "2")
                                {
                                    leadName = txtRazonSocial.Text;
                                }
                                else
                                {
                                    leadName = txtName.Text.Trim() + (" " + txtMiddleName.Text).Trim() + (" " + txtLastName.Text).Trim();
                                }

                                body = File.ReadAllText(HttpContext.Current.Server.MapPath("HtmlTemplates/leadEmailTemplate.html"));

                                body = body.Replace("%NombreCompleto%", leadName);
                                body = body.Replace("%RFC%", txtRFC.Text.Trim());
                                body = body.Replace("%Email%", txtEmail.Text.Trim());
                                //body = body.Replace("%URL%", ConfigurationManager.AppSettings["APPUrl"].ToString()+ "Prospecto.aspx?Id="+LeadId.Value.ToString());
                                //body = body.Replace("%id%", LeadId.Value.ToString());
                                body = body.Replace("%TipoProspecto%", cmbTipoPersona.SelectedValue == "2" ? "Persona Moral" : "Persona Física");
                                body = body.Replace("%Giro%", txtGiro.SelectedItem.Text);
                                body = body.Replace("%Grupo%", txtGroup.SelectedItem.Text);
                                body = body.Replace("%Comment%", "");
                                body = body.Replace("Motivo de Rechazo:", "");
                                body = body.Replace("%Owner%", txtUsuarioAlta.Text);
                                funct.sendEmail(email, "Aprobación de Prospecto", body);


                            }
                            catch (Exception)
                            {

                            }

                        }
                    }
                    showMessage("Prospecto convertido a Cliente correctamente.", false);
                    //Response.Redirect(string.Format("Cliente.aspx?Id={0}", idCust.ToString()));
                }
                else 
                {
                    this.showMessage(errMsg, true);
                }
                
                    
                
            }
        }

        protected void txtMoral_CheckedChanged(object sender, EventArgs e)
        {
            showPanels();
        }

        protected void txtRFC_TextChanged(object sender, EventArgs e)
        {
            Prospectodiv.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            //BtnSave.Visible = false;
            txtRFC.Text = txtRFC.Text.ToUpper();
        }
        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {

            string errmess = "";

            if (txtEstatusCliente.SelectedValue != "3")
            {
                string confirmValue = Request.Form["confirm_value"];
                if (confirmValue == "Si")
                {
                    string id = LeadId.Value;
                    string leadId = string.Empty;
                    Entity lead = new Entity("Prospecto");
                    lead.Keys.Add(new Key("Id", id, "int64"));
                    lead.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus", "99", "string"));
                    SiteFunctions.SaveEntity(lead, Method.PUT, out errmess);
                    Response.Redirect(string.Format("ProspectosList.aspx"));

                }
            }
            else
            {
                this.showMessage("No se puede borrar un prospecto aprobado como cliente.",true);
            }



        }

        protected void btnEnviarValidacionCobranza_Click(object sender, ImageClickEventArgs e)
        {
            txtEstatusCliente.SelectedValue = "2";

            saveProspecto();

        }
        protected void saveProspecto()
        {
            Entity Prospecto = new Entity(ENTITYNAME);
            int userid = 0;
            int.TryParse( SiteFunctions.getuserid(Context.User.Identity.Name, false),out userid );
            Prospecto.auditable.userId = userid;
            Prospecto.useAuditable = true;
            Prospecto.auditable.opDate = DateTime.Now;
            JoinEntity joinEntity = new JoinEntity();
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("RFC", txtRFC.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Tipo", txtExtranjero.Checked ? "2" : "1", "int"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("TipoPersona", cmbTipoPersona.SelectedValue, "int"));
            if (cmbTipoPersona.SelectedValue == "2")
            {
                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("RazonSocial", txtRazonSocial.Text, "string"));

               
            }
            else
            {
                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre", txtName.Text, "string"));

                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("ApellidoPaterno", txtMiddleName.Text, "string"));
                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("ApellidoMaterno", txtLastName.Text, "string"));

                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("FechaNacimiento", txtDate.Text, "datetime"));
                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Curp", txtCurp.Text, "string"));
                Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Email", txtEmail.Text, "string"));


            }

            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Estatus", txtEstatusCliente.SelectedValue, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Grupo", txtGroup.SelectedValue, "int64"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Giro", txtGiro.Text, "int64"));

            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CP", txtCPFiscal.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CPEnv", txtCPEnvio.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Calle", txtCalleFiscal.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CalleEnv", txtCalleEnvio.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("NumExterior", txtNumeroExt.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("NumExteriorEnv", txtNumeroExtEnv.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("NumInterior", txtNumeroInt.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("NumInteriorEnv", txtNumeroIntEnv.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Colonia", txtColoniaFiscal.SelectedValue, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("ColoniaEnv", txtColoniaEnvio.SelectedValue, "string"));
            // Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Pais", txtPaisFacturacion.Text, "string"));
            // Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("PaisEnv", txtPaisEnvio.Text, "string"));
            //Se coloca temporalmente pais méxico
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Pais", "2412", "int64"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("PaisEnv", "2412", "int64"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Estado", txtEstadoFacturacion.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("EstadoEnv", txtEstadoEnvio.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Delegacion", txtDelegacionFacturacion.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("DelegacionEnv", txtDelegacionEnvio.Text, "string"));

            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("TelOficina", txtOficina.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("TelCelular", txtCelular.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Ext", txtExtension.Text, "string"));

            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CAltNombre", txtNombreCA.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CALTEmial", txtEmailCA.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CALTTelefono", txtTelefonoCA.Text, "string"));
            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("CALTPuesto", "", "string"));
            string propietario = OwnerId.Value;
            if(propietario == string.Empty)
            {
                propietario =   Prospecto.auditable.userId.ToString();
            }


            Prospecto.Attributes.Add(new CognisoWebApp.Models.Attribute("Propietario", propietario, "string"));


            string errmess = "";
            object id = Request.QueryString["id"];
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                Prospecto.Keys.Add(new Models.Key("Id", id.ToString(), "int"));
            }

            // txtEstatusCliente.Enabled = true;
            if (txtEstatusCliente.SelectedValue == "2")
            {

                btnConvLead.Visible = true;
                btnRechazar.Visible = true;
                //txtEstatusCliente.Enabled = false;
                Prospecto.Action = 2;
            }
            string leadId = string.Empty;



            if (SiteFunctions.SaveProspect(Prospecto, method, out errmess, out leadId))
            {
                string bandera = "created=1";
                if (leadId == "0")
                {
                    if (id != null)
                        leadId = id.ToString();
                }
                if (Prospecto.Action == 2)
                {
                    bandera = "created=2";
                }

                Response.Redirect(string.Format("Prospecto.aspx?Id={0}&"+bandera, leadId));


            }
            else
            {
                if (errmess != null && errmess != string.Empty && errmess != "")
                {
                    this.showMessage(errmess, true);
                }
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            string errmess = string.Empty;
            txtEstatusCliente.SelectedValue = "4";
            string id = Request.QueryString["id"];

            Entity Comment = new Entity("ComentariosAut");
            Comment.Attributes.Add(new Models.Attribute("IdRegistroRelacionado", id, "int64"));
            Comment.Attributes.Add(new Models.Attribute("Comentario", txtComments.Text.Trim(), "string"));
            Comment.Attributes.Add(new Models.Attribute("Usuario", SiteFunctions.getuserid(Context.User.Identity.Name, false), "string"));
            errmess = string.Empty;
            if (SiteFunctions.SaveEntity(Comment, Method.POST, out errmess))
            {
                this.showMessage(errmess, true);
            }
            
            Entity UserOWner = new Entity("Usuario");
            UserOWner.Attributes.Add(new Models.Attribute("Email"));
            UserOWner.Keys.Add(new Key("id", OwnerId.Value, "int64"));
            List<Entity> listEntity  = SiteFunctions.GetValues(UserOWner, out errmess);
            if (listEntity.Count > 0)
            {
                string email = listEntity[0].getAttrValueByName("Email");
                if(email != "")
                {
                    SiteFunctions funct = new SiteFunctions();
                    string body = string.Empty;
                    try
                    {
                        string leadName = string.Empty;
                        if (cmbTipoPersona.SelectedValue == "2")
                        {
                            leadName = txtRazonSocial.Text;
                        }
                        else
                        {
                            leadName = txtName.Text.Trim() + (" " + txtMiddleName.Text).Trim() + (" " + txtLastName.Text).Trim(); 
                        }

                        body = File.ReadAllText(HttpContext.Current.Server.MapPath("HtmlTemplates/leadEmailTemplate.html"));
                        
                        body = body.Replace("%NombreCompleto%", leadName);
                        body = body.Replace("%RFC%", txtRFC.Text.Trim());
                        body = body.Replace("%Email%", txtEmail.Text.Trim());
                        //body = body.Replace("%URL%", ConfigurationManager.AppSettings["APPUrl"].ToString()+ "Prospecto.aspx?Id="+LeadId.Value.ToString());
                        //body = body.Replace("%id%", LeadId.Value.ToString());
                        body = body.Replace("%TipoProspecto%", cmbTipoPersona.SelectedValue == "2" ?"Persona Moral":"Persona Física");
                        body = body.Replace("%Giro%", txtGiro.SelectedItem.Text);
                        body = body.Replace("%Grupo%", txtGroup.SelectedItem.Text);
                        body = body.Replace("%Comment%", txtComments.Text);
                        body = body.Replace("%Owner%", txtUsuarioAlta.Text);
                        funct.sendEmail(email, "Rechazo de Aprobación de Prospecto", body);


                    }
                    catch (Exception)
                    {

                    }
                    
                }
            }
            saveProspecto();
        }

        protected void GVCommentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            loadcomments(LeadId.Value);
        }

        protected void btnAddGroup_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void reloadGiro_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {
                
                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Giro = new Entity("Giro");
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Giro.useAuditable = false;
                listResult = SiteFunctions.GetValues(Giro, out errmess);
                txtGiro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtGiro.DataValueField = "Id";
                txtGiro.DataTextField = "Nombre";
                txtGiro.DataBind();
                if (Session["retval"] != null)
                {
                    txtGiro.Text = Session["retval"].ToString();
                    Session["retval"] = "";
                }
            }

        }

        protected void reloadGrupo_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {
                
                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Giro = new Entity("Grupo");
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Giro.useAuditable = false;
                listResult = SiteFunctions.GetValues(Giro, out errmess);
                txtGroup.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtGroup.DataValueField = "Id";
                txtGroup.DataTextField = "Nombre";
                txtGroup.DataBind();
                txtGroup.Text = Session["retval"].ToString();
                Session["retval"] = "";
                
            }
        }

      

    }
}