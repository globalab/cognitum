﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CognisoWebApp.Startup))]
namespace CognisoWebApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
