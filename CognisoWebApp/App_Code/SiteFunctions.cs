﻿using Microsoft.AspNet.Identity.Owin;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;

using System.Net;
using System.Data;
using System.Configuration;
using CognisoWA.Controllers;
using System.Net.Mail;
using System.IO;
using System.Data.OleDb;
using ExcelDataReader;
using System.Text;
using System.Collections;
using System.Xml.Linq;
using System.Xml.Xsl;

namespace CognisoWebApp.Models
{
    public class SiteFunctions
    {
        private string emailFrom, emailTo, port, usrEmail, pwdEmail, host, subject;
        public SiteFunctions()
        {

        }

        public static SignInStatus Login(string Username, string Password, out string errmes)
        {
            errmes = "";                                                                                                            
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            var client = new RestClient(server+"/api/Login?Username=" + Username + "&Password=" + Password.Replace("&","amp;").Replace(" ","esp;").Replace("?","quest;").Replace("<","lt;").Replace(">","gt;").Replace("*","ast;").Replace("%","mod;").Replace(":","doublepoint;").Replace(@"\","backs;"));
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);
            OperationResult opResult = new JavaScriptSerializer().Deserialize(response.Content, typeof(OperationResult)) as OperationResult;
            SignInStatus retval = SignInStatus.Failure;
            if (opResult.Success)
            {
                retval = SignInStatus.Success;
            }
            else
            {
                errmes = opResult.ErrMessage;
            }
           return retval;
        }

        private static DataTable ExcelToDataTableUsingExcelDataReader(Stream _stream, bool _isXlsx)
        {
            IExcelDataReader excelReader = null;
            if (!_isXlsx)
            {
                excelReader = ExcelReaderFactory.CreateBinaryReader(_stream);
            }
            else
            {
                excelReader = ExcelReaderFactory.CreateReader(_stream);
            }

            DataSet result = excelReader.AsDataSet();
            var test = result.Tables[0];
            return result.Tables[0];
        }

        public static bool importRecibosConciliacion(Stream _stream, string _liqId, bool _isXlsx, string userId, out List<string> listErrors)
        {
            listErrors = new List<string>();
            bool retVal = true;
            string errMess = string.Empty;
            bool passFirstRow = false;
            DataTable dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            if (dtResult.Columns.Count == 9)
            {
                int _userid = 0;
                int.TryParse(userId, out _userid);
                Auditable audi = new Auditable();
                audi.userId = _userid;
                audi.opDate = DateTime.Now;
                foreach (DataRow row in dtResult.Rows)
                {
                    if (passFirstRow)
                    {
                        #region busca recibo
                        Entity recibo = new Entity("recibo");
                        recibo.Attributes.Add(new Attribute("Id"));
                        recibo.Attributes.Add(new Attribute("Estatus"));
                        Key filter = new Key("Numero", row[1].ToString(), "string", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        recibo.Keys.Add(filter);

                        filter = new Key("Total", row[2].ToString(), "decimal", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        recibo.Keys.Add(filter);

                        recibo.logicalOperator = 1;

                        Entity tramite = new Entity("Tramite");
                        tramite.Attributes.Add(new Attribute("Id"));

                        filter = new Key("Folio", row[0].ToString(), "string", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        tramite.Keys.Add(filter);
                        tramite.logicalOperator = 1;

                        selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
                        JoinEntity joinEnt = new JoinEntity();
                        joinEnt.selectJoinList.Add(selJoin);
                        joinEnt.JoinType = JoinType.Inner;
                        joinEnt.ChildEntity = tramite;

                        recibo.ChildEntities.Add(joinEnt);
                        List<Entity> results = SiteFunctions.GetValues(recibo, out errMess);
                        #endregion
                        if (results.Count > 0)
                        {
                            Entity reciboRead = results[0];
                            if (reciboRead.getAttrValueByName("Estatus") == "3")
                            {
                                Entity reciboUpdate = new Entity("recibo");
                                reciboUpdate.Attributes.Add(new Attribute("Estatus", "4", "int"));
                                reciboUpdate.Attributes.Add(new Attribute("ObservacionPago", row[3].ToString(), "string"));
                                reciboUpdate.Attributes.Add(new Attribute("TipoIngreso", row[4].ToString(), "int"));
                                reciboUpdate.Attributes.Add(new Attribute("ComisionPagada", row[5].ToString(), "float"));
                                reciboUpdate.Attributes.Add(new Attribute("FechaPagoComision", row[6].ToString(), "datetime"));
                                reciboUpdate.Attributes.Add(new Attribute("TipoCambio", row[7].ToString(), "float"));
                                reciboUpdate.Attributes.Add(new Attribute("TipoCambioAbono", row[8].ToString(), "float"));
                                reciboUpdate.Attributes.Add(new Attribute("conciliacion", _liqId, "int"));
                                reciboUpdate.Keys.Add(new Key("id", reciboRead.getAttrValueByName("id"), "int"));
                                if (!SiteFunctions.SaveEntity(reciboUpdate, Method.PUT, out errMess))
                                {
                                    listErrors.Add(errMess);
                                    retVal = false;
                                }
                            }
                            else
                            {
                                listErrors.Add(string.Format("Recibo {0} no se encuentra en estado Aplicado", row[1].ToString()));
                                retVal = false;
                            }
                        }
                        else
                        {
                            listErrors.Add(string.Format("Recibo {0} con póliza {1} no se encuentra en la base de datos", row[1].ToString(), row[0].ToString()));
                            retVal = false;
                        }
                    }
                    passFirstRow = true;
                }
            }

            return retVal;
        }

        private static int mapTipoIngreso(string tipoIngresoStr)
        {
            switch (tipoIngresoStr.ToLower())
            {
                case "comision":
                    return (int)TipoIngresoEnum.Comision;
                case "sobrecomision":
                    return (int)TipoIngresoEnum.SobreComision;
                case "usoderechoinstalacion":
                    return (int)TipoIngresoEnum.UsoDerechoInstalacion;
                case "honorarios":
                    return (int)TipoIngresoEnum.Honorarios;
                case "bono":
                    return (int)TipoIngresoEnum.Bono;
                default:
                    return (int)TipoIngresoEnum.NoEspecificado;
            }
        }

        public static bool importRecibosLiquidacion(Stream _stream, string _liqId, bool _isXlsx, string userId, out List<string> listErrors)
        {
            listErrors = new List<string>();
            bool retVal = true;
            string errMess = string.Empty;
            bool passFirstRow = false;
            DataTable dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            if (dtResult.Columns.Count == 3)
            {
                int _userid = 0;
                int.TryParse(userId, out _userid);
                Auditable audi = new Auditable();
                audi.userId = _userid;
                audi.opDate = DateTime.Now;
                foreach (DataRow row in dtResult.Rows)
                {
                    if (passFirstRow)
                    {
                        #region busca recibo
                        Entity recibo = new Entity("recibo");
                        recibo.Attributes.Add(new Attribute("Id"));
                        recibo.Attributes.Add(new Attribute("Estatus"));
                        Key filter = new Key("Numero", row[1].ToString(), "string", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        recibo.Keys.Add(filter);

                        filter = new Key("Total", row[2].ToString(), "decimal", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        recibo.Keys.Add(filter);

                        recibo.logicalOperator = 1;

                        Entity tramite = new Entity("Tramite");
                        tramite.Attributes.Add(new Attribute("Id"));

                        filter = new Key("Folio", row[0].ToString(), "string", 1, 1);
                        filter.WhereOperator = whereOperator.Equal;
                        tramite.Keys.Add(filter);
                        tramite.logicalOperator = 1;

                        selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
                        JoinEntity joinEnt = new JoinEntity();
                        joinEnt.selectJoinList.Add(selJoin);
                        joinEnt.JoinType = JoinType.Inner;
                        joinEnt.ChildEntity = tramite;

                        recibo.ChildEntities.Add(joinEnt);
                        List<Entity> results = SiteFunctions.GetValues(recibo, out errMess);
                        #endregion
                        if (results.Count > 0)
                        {
                            Entity reciboRead = results[0];
                            if (reciboRead.getAttrValueByName("Estatus") == "0")
                            {
                                Entity reciboUpdate = new Entity("recibo");
                                reciboUpdate.Attributes.Add(new Attribute("Estatus", "2", "int"));
                                reciboUpdate.Attributes.Add(new Attribute("liquidacion", _liqId, "int"));
                                reciboUpdate.Keys.Add(new Key("id", reciboRead.getAttrValueByName("id"), "int"));
                                if (!SiteFunctions.SaveEntity(reciboUpdate, Method.PUT, out errMess))
                                {
                                    listErrors.Add(errMess);
                                    retVal = false;
                                }
                            }
                            else
                            {
                                listErrors.Add(string.Format("Recibo {0} no se encuentra en estado pendiente", row[1].ToString()));
                                retVal = false;
                            }
                        }
                        else
                        {
                            listErrors.Add(string.Format("Recibo {0} con póliza {1} no se encuentra en la base de datos", row[1].ToString(), row[0].ToString()));
                            retVal = false;
                        }
                    }
                    passFirstRow = true;
                }
            }

            return retVal;
        }

        public static string formatListErrors(List<string> listErrors)
        {
            string errMsg = string.Empty;
            foreach (string line in listErrors)
            {
                errMsg = errMsg + line + "<br />";
            }
            return errMsg;
        }

        public static bool importIncisos(Stream _stream, string _polizaId, bool _isXlsx, string userId, out decimal totalIncisos,out string errmess,out int NoIncisos)
        {
            
            totalIncisos = 0;
            NoIncisos = 0;
            bool retVal = true;
            bool passFirstRow = false;
            errmess = string.Empty;
            DataTable dtResult = null;
            try
            {
                dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            }
            catch (Exception)
            {
                errmess = "El archivo no está en el formato correcto, favor de verificar.";
            }
            if (dtResult != null)
            {
                if (dtResult.Columns.Count == 20)
                {
                    int _userid = 0;
                    int.TryParse(userId, out _userid);
                    Auditable audi = new Auditable();
                    audi.userId = _userid;
                    audi.opDate = DateTime.Now;
                    if (ValidateLayout(dtResult, _polizaId, out errmess))
                    {
                        
                        foreach (DataRow row in dtResult.Rows)
                        {
                            if (passFirstRow)
                            {
                                NoIncisos++;
                                decimal prima = 0;
                                #region mapea bien asegurado
                                Entity bienAsegurado = new Entity("BienAsegurado");
                                bienAsegurado.useAuditable = true;
                                bienAsegurado.auditable = audi;
                                bienAsegurado.Attributes.Add(new Attribute("Permiso", "2", "int"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("Poliza", _polizaId, "int"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("Inciso", row[8].ToString(), "string"));
                                if (row[9].ToString().Trim().ToLower() == "activo")
                                {
                                    bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
                                }
                                else
                                {
                                    bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "0", "int"));
                                }
                                bienAsegurado.Attributes.Add(new Models.Attribute("Prima", row[10].ToString(), "float"));
                                bienAsegurado.Attributes.Add(new Models.Attribute("Identificador", row[11].ToString(), "string"));
                                if (row[14].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("EndosoAlta", row[14].ToString(), "int"));
                                if (row[15].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("EndosoBaja", row[15].ToString(), "int"));
                                if (row[16].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("GrupoAsegurados", row[16].ToString(), "int"));
                                if (row[17].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("BeneficiarioPreferente", row[17].ToString(), "int"));
                                if (row[18].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("Empleado", row[18].ToString(), "int"));
                                if (row[19].ToString() != string.Empty)
                                    bienAsegurado.Attributes.Add(new Models.Attribute("TarjetaBancaria", row[19].ToString(), "int"));
                                #endregion
                                #region mapea auto
                                Entity auto = new Entity("auto");
                                auto.Attributes.Add(new Attribute("Serie", row[1].ToString(), "string"));
                                auto.Attributes.Add(new Attribute("Motor", row[2].ToString(), "string"));
                                auto.Attributes.Add(new Attribute("Placas", row[3].ToString(), "string"));
                                auto.Attributes.Add(new Attribute("ConductorHabitual", row[4].ToString(), "string"));
                                if (row[5].ToString().ToLower() == "amplia")
                                {
                                    auto.Attributes.Add(new Attribute("CoberturaAuto", "1", "string"));
                                }
                                auto.Attributes.Add(new Attribute("Modelo", row[6].ToString(), "string"));
                                auto.Attributes.Add(new Attribute("Version", row[7].ToString(), "string"));
                                if (row[13].ToString().Trim() != string.Empty)
                                {
                                    errmess = string.Empty;
                                    Entity marca = new Entity("Marca");
                                    marca.Attributes.Add(new Attribute("id", "", "int"));
                                    marca.Keys.Add(new Key("Descripcion", row[13].ToString(), "string"));
                                    List<Entity> resEntities = GetValues(marca, out errmess);
                                    if (resEntities.Count > 0)
                                    {
                                        Entity entMarca = resEntities[0];
                                        auto.Attributes.Add(new Attribute("Marca", entMarca.getAttrValueByName("id"), "string"));
                                    }
                                }
                                auto.Attributes.Add(new Attribute("Poliza", _polizaId, "int"));

                                JoinEntity joinEntity = new JoinEntity();
                                joinEntity.ChildEntity = auto;
                                joinEntity.JoinKey = new Attribute("id", "", "int");
                                bienAsegurado.ChildEntities.Add(joinEntity);
                                #endregion
                                decimal.TryParse(row[10].ToString(), out prima);
                                totalIncisos += prima;
                                retVal = SaveEntity(bienAsegurado, Method.POST, out errmess);
                            }
                            passFirstRow = true;
                        }
                    }
                    else
                    {
                        retVal = false;
                    }

                    
                }
                else
                {
                    errmess = "El número de columnas que contiene el archivo no concuerdan con el layout solicitado (20 Columnas).";
                }
            }
            return retVal;
        }

        public static bool ValidateLayout(DataTable dtResults,string Poliza,out string errmess)
        {
            errmess = string.Empty;
            bool retval = true;
            Hashtable hTable = new Hashtable();
            Dictionary<string, string> duplicateList= new Dictionary<string,string>();
            int x = 0;
            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dtResults.Rows)
            {
                x++;
                if (x > 1)
                {
                    if (hTable.Contains(drow[1]))
                    {
                        duplicateList.Add(x.ToString(), string.Format("La Linea {0} tiene el número de serie duplicado No Serie: {1}", x.ToString(), drow[1].ToString()));
                    }
                    else
                    {
                        hTable.Add(drow[1], string.Empty);
                    }
                    if (drow[12].ToString() != Poliza)
                    {
                        if (duplicateList.ContainsKey(x.ToString()))
                        {
                            duplicateList[x.ToString()] +=  "<br/>" + string.Format("La Linea {0} tiene incorrecta la póliza: {1}", x.ToString(), drow[12].ToString());

                        }
                        else
                        {
                            duplicateList.Add(x.ToString(), string.Format("La Linea {0} tiene incorrecta la póliza: {1}", x.ToString(), drow[12].ToString()));
                        }

                    }
                    if (drow[5].ToString() != "Amplia" && drow[5].ToString() != "RC" && drow[5].ToString() != "Limitada")
                    {
                        if (duplicateList.ContainsKey(x.ToString()))
                        {
                            duplicateList[x.ToString()] += "<br/>" +string.Format("La Linea {0} tiene incorrecta la Cobertura, los valores admitidos son (Amplia,RC o Limitada): {1}", x.ToString(), drow[5].ToString());

                        }
                        else
                        {
                            duplicateList.Add(x.ToString(), string.Format("La Linea {0} tiene incorrecta la Cobertura, los valores admitidos son (Amplia,RC o Limitada): {1}", x.ToString(), drow[5].ToString()));
                        }
                    }
                }
            }

            if (duplicateList.Count > 0)
            {
                foreach(KeyValuePair<string,string> linea in  duplicateList)
                {
                    errmess += linea.Value + "<br/>" ;
 
                }
                retval= false;
            }


           
                    
            
            return retval;
        }

        public static bool ValidateLayoutIntegraciones(DataTable dtResults, Dictionary<string,string> Poliza, out string errmess)
        {
            errmess = string.Empty;
            bool retval = true;
            Hashtable hTable = new Hashtable();
            Dictionary<string, string> duplicateList = new Dictionary<string, string>();
            int x = 0;
            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dtResults.Rows)
            {
                x++;
                if (x > 1)
                {
                    
                    if (Poliza.ContainsKey(drow[0].ToString().Trim()))
                    {
                        if (duplicateList.ContainsKey(x.ToString()))
                        {
                            duplicateList[x.ToString()] += "<br/>" + string.Format("La Linea {0} tiene incorrecta la póliza: {1}", x.ToString(), drow[0].ToString());

                        }
                        else
                        {
                            duplicateList.Add(x.ToString(), string.Format("La Linea {0} tiene incorrecta la póliza: {1}", x.ToString(), drow[0].ToString()));
                        }

                    }
                    
                }
            }

            if (duplicateList.Count > 0)
            {
                foreach (KeyValuePair<string, string> linea in duplicateList)
                {
                    errmess += linea.Value + "<br/>";

                }
                retval = false;
            }


          return retval;
        }
        public static bool importGrupos(Stream _stream, string _polizaId, bool _isXlsx, string userId, out string errmsg)
        {
            errmsg = string.Empty;
            bool retVal = true;
            bool passFirstRow = false;
            string errMess = string.Empty;
            DataTable dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            if (dtResult.Columns.Count == 20)
            {
                int _userid = 0;
                int.TryParse(userId, out _userid);
                Auditable audi = new Auditable();
                audi.userId = _userid;
                audi.opDate = DateTime.Now;
                Dictionary<int, string> ExistingGrps = new Dictionary<int,string>();
                foreach (DataRow row in dtResult.Rows)
                {
                    if (passFirstRow)
                    {
                        string grpId = string.Empty;
                        string nombre = row[0].ToString();
                        if (!ExistingGrps.ContainsValue(nombre))
                        {
                            #region crea Grupo Asegurado
                            Entity GrupoAsegurados = new Entity("GrupoAsegurados");
                            GrupoAsegurados.useAuditable = true;
                            GrupoAsegurados.auditable = audi;
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("Poliza", _polizaId, "int"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("Nombre", row[0].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("Descripcion", row[1].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("Deducible", row[2].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("Coaseguro", row[3].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("SumaAsegurada", row[4].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("DeduciblePct", row[5].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("CoaseguroPct", row[6].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("SumaAseguradaPct", row[7].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("DeducibleSMGM", row[8].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("CoaseguroSMGM", row[9].ToString(), "string"));
                            GrupoAsegurados.Attributes.Add(new Models.Attribute("SumaAseguradaSMGM", row[10].ToString(), "string"));
                            if (SiteFunctions.SaveEntity(GrupoAsegurados, Method.POST, out grpId))
                            {
                                ExistingGrps.Add(int.Parse(grpId), nombre);
                            }
                            else
                            {
                                errMess = grpId;
                                return false;
                            }
                            #endregion
                        }
                        else
                        {
                            KeyValuePair<int, string> result = ExistingGrps.First(x => x.Value == nombre);
                            grpId = result.Key.ToString();
                        }

                        #region mapea bien asegurado
                        Entity bienAsegurado = new Entity("BienAsegurado");
                        bienAsegurado.useAuditable = true;
                        bienAsegurado.auditable = audi;
                        bienAsegurado.Attributes.Add(new Attribute("Permiso", "2", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Poliza", _polizaId, "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Inciso", row[11].ToString(), "string"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Prima", "0", "float"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Identificador", row[12].ToString(), "string"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("GrupoAsegurados", grpId.ToString(), "int"));
                        #endregion
                        #region mapea certificado
                        Entity certificado = new Entity("Certificado");
                        certificado.Attributes.Add(new Attribute("Tipo", "0", "int"));
                        certificado.Attributes.Add(new Attribute("Nombre", row[13].ToString(), "string"));
                        certificado.Attributes.Add(new Attribute("ApellidoPaterno", row[14].ToString(), "string"));
                        certificado.Attributes.Add(new Attribute("ApellidoMaterno", row[15].ToString(), "string"));
                        if (row[16].ToString().ToLower() != "")
                        {
                            certificado.Attributes.Add(new Attribute("FechaNacimiento", row[16].ToString(), "datetime"));
                        }
                        if (row[17].ToString().ToLower() == "masculino")
                        {
                            certificado.Attributes.Add(new Attribute("Sexo", "0", "int"));
                        }
                        else
                        {
                            certificado.Attributes.Add(new Attribute("Sexo", "1", "int"));
                        }
                        switch (row[18].ToString().ToLower())
                        {
                            case "hijo":
                                certificado.Attributes.Add(new Attribute("parentesco", "1", "int"));
                                break;
                            case "conyuge":
                                certificado.Attributes.Add(new Attribute("parentesco", "2", "int"));
                                break;
                            case "madre":
                                certificado.Attributes.Add(new Attribute("parentesco", "3", "int"));
                                break;
                            case "padre":
                                certificado.Attributes.Add(new Attribute("parentesco", "4", "int"));
                                break;
                            default:
                                certificado.Attributes.Add(new Attribute("parentesco", "0", "int"));
                                break;
                        }
                        if (row[19].ToString().ToLower() != "")
                        {
                            certificado.Attributes.Add(new Attribute("antiguedad", row[19].ToString().ToLower(), "datetime"));
                        }
                        JoinEntity joinEntity = new JoinEntity();
                        joinEntity.ChildEntity = certificado;
                        joinEntity.JoinKey = new Attribute("id", "", "int");
                        bienAsegurado.ChildEntities.Add(joinEntity);
                        #endregion
                        retVal = SaveEntity(bienAsegurado, Method.POST, out errMess);
                    }
                    passFirstRow = true;
                }
            }

            return retVal;
        }

        public static bool importDependientes(Stream _stream, string _polizaId, string _idTitular, bool _isXlsx, string userId, out string errmsg)
        {
            errmsg = string.Empty;
            bool retVal = true;
            bool passFirstRow = false;
            string errMess = string.Empty;
            DataTable dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            if (dtResult.Columns.Count == 11)
            {
                int _userid = 0;
                int.TryParse(userId, out _userid);
                Auditable audi = new Auditable();
                audi.userId = _userid;
                audi.opDate = DateTime.Now;
                Dictionary<int, string> ExistingGrps = new Dictionary<int, string>();
                foreach (DataRow row in dtResult.Rows)
                {
                    if (passFirstRow)
                    {
                        string grpId = string.Empty;
                        #region mapea bien asegurado
                        Entity bienAsegurado = new Entity("BienAsegurado");
                        bienAsegurado.useAuditable = true;
                        bienAsegurado.auditable = audi;
                        bienAsegurado.Attributes.Add(new Attribute("Permiso", "2", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Poliza", _polizaId, "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Inciso", row[0].ToString(), "string"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Estatus", "1", "int"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Prima", "0", "float"));
                        bienAsegurado.Attributes.Add(new Models.Attribute("Identificador", row[1].ToString(), "string"));
                        if (!string.IsNullOrEmpty(row[8].ToString()))
                        {
                            bienAsegurado.Attributes.Add(new Models.Attribute("EndosoAlta", row[8].ToString(), "string"));
                        }
                        if (!string.IsNullOrEmpty(row[9].ToString()))
                        {
                            bienAsegurado.Attributes.Add(new Models.Attribute("EndosoBaja", row[9].ToString(), "string"));
                        }
                        #endregion
                        #region mapea certificado
                        Entity certificado = new Entity("Certificado");
                        certificado.Attributes.Add(new Attribute("Tipo", "0", "int"));
                        certificado.Attributes.Add(new Attribute("Nombre", row[2].ToString(), "string"));
                        certificado.Attributes.Add(new Attribute("ApellidoPaterno", row[3].ToString(), "string"));
                        certificado.Attributes.Add(new Attribute("ApellidoMaterno", row[4].ToString(), "string"));
                        if (row[5].ToString().ToLower() != "")
                        {
                            certificado.Attributes.Add(new Attribute("FechaNacimiento", row[5].ToString(), "datetime"));
                        }
                        if (row[6].ToString().ToLower() == "masculino")
                        {
                            certificado.Attributes.Add(new Attribute("Sexo", "0", "int"));
                        }
                        else
                        {
                            certificado.Attributes.Add(new Attribute("Sexo", "1", "int"));
                        }
                        if (row[10].ToString().ToLower() != "")
                        {
                            certificado.Attributes.Add(new Attribute("Antiguedad", row[10].ToString(), "datetime"));
                        }

                        switch (row[7].ToString().ToLower())
                        {
                            case "hijo":
                                certificado.Attributes.Add(new Attribute("parentesco", "1", "int"));
                                break;
                            case "conyuge":
                                certificado.Attributes.Add(new Attribute("parentesco", "2", "int"));
                                break;
                            case "madre":
                                certificado.Attributes.Add(new Attribute("parentesco", "3", "int"));
                                break;
                            case "padre":
                                certificado.Attributes.Add(new Attribute("parentesco", "4", "int"));
                                break;
                            default:
                                certificado.Attributes.Add(new Attribute("parentesco", "0", "int"));
                                break;
                        }
                        certificado.Attributes.Add(new Attribute("Titular", _idTitular, "int"));
                        JoinEntity joinEntity = new JoinEntity();
                        joinEntity.ChildEntity = certificado;
                        joinEntity.JoinKey = new Attribute("id", "", "int");
                        bienAsegurado.ChildEntities.Add(joinEntity);
                        #endregion
                        retVal = SaveEntity(bienAsegurado, Method.POST, out errMess);
                    }
                    passFirstRow = true;
                }
            }

            return retVal;
        }

        public static bool ValidaProspecto(string RFC, int extranjero, int PersonaMoral, out string errmes)
        {
            errmes = "";
            Entity lead = new Entity("Prospecto");
            lead.Keys.Add(new Key("RFC", RFC, "string"));
            lead.Keys.Add(new Key("Estatus not", "99", "int64"));
            //lead.Keys.Add(new Key("Tipo", extranjero == 0 ? "1" : "2", "int"));
            //lead.Keys.Add(new Key("TipoPersona", PersonaMoral == 0 ? "2" : "1", "int"));
            lead.Action = 1;
            lead.Attributes.Add(new Attribute("Id"));
            lead.useAuditable = true;
            string entidad = new JavaScriptSerializer().Serialize(lead);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = sendMessage(server+"/api/IncomingMessage/GetEntity", entidad, Method.POST);
            bool retval = true;
            if (!opResult.Success)
            {
                retval = false;
                errmes = opResult.ErrMessage;
            }
            else
            {
                if (opResult.RetVal != null)
                {
                    if (opResult.RetVal.Count > 0)
                    {
                        retval = false;
                        errmes = "Ya existe un prospecto con ese RFC";
                    }
                }
            }
            if (retval)
            {
                lead.EntityName = "Cliente";
                lead.EntityAlias = "Cliente";
                lead.Keys.Clear();
                lead.Keys.Add(new Key("RFC", RFC, "string"));
                entidad = new JavaScriptSerializer().Serialize(lead);
                opResult = sendMessage(server+"/api/IncomingMessage/GetEntity", entidad, Method.POST);

                if (!opResult.Success)
                {
                    retval = false;
                    errmes = opResult.ErrMessage;
                }
                else
                {
                    if (opResult.RetVal != null)
                    {
                        if (opResult.RetVal.Count > 0)
                        {
                            retval = false;
                            errmes = "Ya existe un cliente con ese RFC";
                        }
                    }
                }

            }


            return retval;
        }

        public static bool SaveEntity(Entity Entidad, Method _method, out string errmess)
        {
            errmess = "";
            string entidad = new JavaScriptSerializer().Serialize(Entidad);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = sendMessage(server + "/api/IncomingMessage", entidad, _method);
            bool retval = true;
            //si entidad que entra es prospect y trae action=1 y en el poresult=true 
            //y en el retval de opResult trae un entity customer

            if (!opResult.Success)
            {
                retval = false;
                errmess = opResult.ErrMessage;
            }
            else
            {
                errmess = opResult.returnedId.ToString();
            }

            return retval;
        }

        public static Entity getUserEntity(string userid, string[] _columns)
        {
            string errmess = "";

            Entity user = new Entity("Usuario");
            foreach (string field in _columns)
            {
                user.Attributes.Add(new Models.Attribute(field));
            }
            user.Keys.Add(new Key("Username", userid, "string"));

            Entity puesto = new Entity("Puesto");
            puesto.Attributes.Add(new Attribute("Nombre"));

            selectJoinEntity selJoin = new selectJoinEntity("puesto", 1, "Id");
            JoinEntity entityJoin = new JoinEntity();
            entityJoin.ChildEntity = puesto;
            entityJoin.JoinType = JoinType.Left;
            entityJoin.selectJoinList.Add(selJoin);

            user.ChildEntities.Add(entityJoin);

            List<Entity> estatus = SiteFunctions.GetValues(user, out errmess);
            string Id = string.Empty;
            if (estatus.Count > 0)
            {
                Entity Usuario = estatus[0];
                return Usuario;
            }

            return new Entity("usuario");
        }

        public static string getuserid(string userid, bool idCobranza)
        {
            string errmess = "";

            Entity user = new Entity("Usuario");
            user.Attributes.Add(new Models.Attribute("Id"));
            user.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            user.Keys.Add(new Key("Username", userid, "string"));

            List<Entity> estatus = SiteFunctions.GetValues(user, out errmess);
            string Id = string.Empty;
            if (estatus.Count > 0)
            {
                Entity Usuario = estatus[0];
                if (Usuario.getAttrValueByName("CentroDeBeneficio") == ConfigurationManager.AppSettings["UsuarioCobranza"].ToString() && idCobranza)
                {
                    Id = ConfigurationManager.AppSettings["UsuarioCobranza"].ToString();
                }
                else
                {
                    Id = Usuario.getAttrValueByName("Id");
                }
            }

            return Id;


        }

        public static string getUserCentrodeBeneficio(string userid)
        {
            string errmess = "";

            Entity user = new Entity("Usuario");
            user.Attributes.Add(new Models.Attribute("Id"));
            user.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            user.Keys.Add(new Key("Username", userid, "string"));

            List<Entity> estatus = SiteFunctions.GetValues(user, out errmess);
            string Id = string.Empty;
            if (estatus.Count > 0)
            {
                Entity Usuario = estatus[0];
                Id = Usuario.getAttrValueByName("CentroDeBeneficio");
                
            }

            return Id;


        }

        public static string getUserUbicacion(string userid)
        {
            string errmess = "";

            Entity user = new Entity("Usuario");
            user.Attributes.Add(new Models.Attribute("Id"));
            user.Attributes.Add(new Models.Attribute("Ubicacion"));
            user.Keys.Add(new Key("Username", userid, "string"));

            List<Entity> estatus = SiteFunctions.GetValues(user, out errmess);
            string Id = string.Empty;
            if (estatus.Count > 0)
            {
                Entity Usuario = estatus[0];
                Id = Usuario.getAttrValueByName("Ubicacion");

            }

            return Id;


        }

        public static bool IsUsuarioCobranza(string userid)
        {
            string errmess = "";
            Entity user = new Entity("Usuario");
            user.Attributes.Add(new Models.Attribute("Id"));
            user.Attributes.Add(new Models.Attribute("CentroDeBeneficio"));
            user.Keys.Add(new Key("Username", userid, "string"));

            List<Entity> estatus = SiteFunctions.GetValues(user, out errmess);
            bool retval = false;
            if (estatus.Count > 0)
            {
                Entity Usuario = estatus[0];
                if (Usuario.getAttrValueByName("CentroDeBeneficio") == ConfigurationManager.AppSettings["UsuarioCobranza"].ToString())
                {
                    retval = true;
                }
                
            }

            return retval;
        }

        public bool sendEmailWAttachtment(string _to, string _subject, string _body, Stream _fileContent, string fileName)
        {
            bool retval = false;
            port = ConfigurationManager.AppSettings["Port"].ToString();
            host = ConfigurationManager.AppSettings["EmailHost"].ToString();
            usrEmail = ConfigurationManager.AppSettings["muser"].ToString();
            pwdEmail = ConfigurationManager.AppSettings["mpwd"].ToString();
            emailFrom = ConfigurationManager.AppSettings["emailfrom"].ToString();
            
            OperationResult opRes = new OperationResult();
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailFrom);
                foreach (var address in _to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(this.port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = this.host;
                mail.Subject = _subject;

                mail.Body = _body;
                mail.IsBodyHtml = true;
                System.Net.Mail.Attachment item = new System.Net.Mail.Attachment(_fileContent, fileName);
                mail.Attachments.Add(item);
                client.Credentials = new System.Net.NetworkCredential(this.usrEmail, this.pwdEmail);
                client.EnableSsl = true;
                client.Send(mail);
                retval = true;
            }
            catch (Exception exc)
            {
                opRes.Success = false;
                opRes.ErrMessage = exc.Message;
            }
            return retval;
        }
        public bool sendEmail(string _to, string _subject, string _body,string attachmentname = "")
        {
            bool retval = false;
            port = ConfigurationManager.AppSettings["Port"].ToString();
            host = ConfigurationManager.AppSettings["EmailHost"].ToString();
            usrEmail = ConfigurationManager.AppSettings["muser"].ToString();
            pwdEmail = ConfigurationManager.AppSettings["mpwd"].ToString();
            emailFrom = ConfigurationManager.AppSettings["emailfrom"].ToString();
            
            OperationResult opRes = new OperationResult();
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailFrom);
                foreach (var address in _to.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.To.Add(address);
                }
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(this.port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Host = this.host;
                mail.Subject = _subject;
                Attachment att = new Attachment();

                if (!String.IsNullOrEmpty(attachmentname))
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(attachmentname);
                    mail.Attachments.Add(attachment);
                }
                mail.Body = _body;
                mail.IsBodyHtml = true;                
                client.Credentials = new System.Net.NetworkCredential(this.usrEmail, this.pwdEmail);
                client.EnableSsl = true;
                client.Send(mail);
                retval = true;
            }
            catch (Exception exc)
            {
                opRes.Success = false;
                opRes.ErrMessage = exc.Message;
            }
            return retval;
        }
        public static bool SaveProspect(Entity Entidad, Method _method, out string errmess, out string idCust)
        {
            errmess = "";
            idCust = "";
            List<Entity> entities = new List<Entity>();
            string entidad = new JavaScriptSerializer().Serialize(Entidad);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = sendMessage(server + "/api/IncomingMessage", entidad, _method);
            bool retval = true;


            if (!opResult.Success)
            {
                retval = false;
                errmess = opResult.ErrMessage;
            }
            else
            {
                idCust = opResult.returnedId.ToString();
                if (opResult.RetVal != null)
                {
                    entities = opResult.RetVal;
                    if (entities.Count > 0)
                    {
                        Entity customer = entities[0];
                        idCust = customer.getAttrValueByName("Id");
                    }
                }
            }

            return retval;
        }

        

        public static bool DeleteEntity(Entity Entidad, Method _method, out string errmess)
        {
            errmess = "";
            
            List<Entity> entities = new List<Entity>();
            string entidad = new JavaScriptSerializer().Serialize(Entidad);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = sendMessage(server + "/api/IncomingMessage/DeleteEntity", entidad, _method);
            bool retval = true;


            if (!opResult.Success)
            {
                retval = false;
                errmess = opResult.ErrMessage;
            }
            

            return retval;
        }

        public static DataTable trnsformEntityToDT(List<Entity> _entities, bool _addEmpty = false)
        {
            DataTable retval = new DataTable();
            if (_entities != null)
            {
                
                foreach (Entity ent in _entities)
                {

                    if (retval.Columns.Count == 0)
                    {
                        foreach (Attribute attr in ent.Attributes)
                        {

                            retval.Columns.Add(attr.AttrName);
                        }
                    }
                    
                    if (_addEmpty)
                    {
                        DataRow emptyRow = retval.NewRow();
                        retval.Rows.Add(emptyRow);
                    }
                    DataRow dr = retval.NewRow();
                    foreach (Attribute attr in ent.Attributes)
                    {
                        if (attr.AttrName == "Audit_Estatus")
                        {
                            dr[attr.AttrName] = attr.AttrValue == "0" ? "Inactivo" : attr.AttrValue == "1" ? "Activo" : "Eliminado" ;
                        }
                         if(attr.AttrName.Contains("Estatus"))
                        {
                            switch (ent.EntityName.ToLower())
                            {
                                case "endoso":
                                    EstatusEndosoEnum enumDisplayEndoso = (EstatusEndosoEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayEndoso.ToString();
                                    break;
                                case "liquidacion":
                                    EstatusLiquidacionEnum enumDisplayLiquidacion = (EstatusLiquidacionEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayLiquidacion.ToString();
                                    break;
                                case "conciliacion":
                                    EstatusConciliacionEnum enumDisplayConciliacion = (EstatusConciliacionEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayConciliacion.ToString();
                                    break;
                                case "recibo":
                                    EstatusReciboEnum enumDisplayStatus = (EstatusReciboEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayStatus.ToString();
                                    break;
                                case "poliza":
                                    EstatusPolizaEnum enumDisplayPoliza = (EstatusPolizaEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayPoliza.ToString();
                                    break;
                                case "prospecto":
                                    EstatusProspectoEnum enumDisplayProspecto = (EstatusProspectoEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayProspecto.ToString();
                                    break;
                                case "cliente":
                                    if (attr.AttrName.ToLower() == "estatuscliente")
                                    {
                                        EstatusClienteEnum enumDisplayCliente = (EstatusClienteEnum)int.Parse(attr.AttrValue);
                                        dr[attr.AttrName] = enumDisplayCliente.ToString();
                                    }
                                    else
                                    {
                                        dr[attr.AttrName] = attr.AttrValue == "0" ? "Inactivo" : attr.AttrValue == "1" ? "Activo" : "Eliminado";
                                    }
                                    break;
                                case "bienasegurado":
                                    EstatusBienAseguradoEnum enumDisplayBA = (EstatusBienAseguradoEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumDisplayBA.ToString();
                                    break;
                                default:
                                    dr[attr.AttrName] = attr.AttrValue;
                                    break;
                            }
                        }
                        else
                        {
                            if (attr.AttrName == "CertificadoParentesco")
                            {
                                string parentesco = string.Empty;
                                switch (attr.AttrValue)
                                {
                                    case "1":
                                        parentesco = "hijo";
                                        break;
                                    case "2":
                                        parentesco = "conyuge";
                                        break;
                                    case "3":
                                        parentesco = "madre";
                                        break;
                                    case "4":
                                        parentesco = "padre";
                                        break;
                                    default:
                                        parentesco = "titular";
                                        break;
                                }
                                dr[attr.AttrName] = parentesco;
                            }
                            else
                            {
                                if (ent.EntityName == "Direccion" && attr.AttrName == "Tipo")
                                {
                                    TipoDireccionEnum enumTipoDir = (TipoDireccionEnum)int.Parse(attr.AttrValue);
                                    dr[attr.AttrName] = enumTipoDir.ToString();
                                }
                                else
                                {
                                    if ((ent.EntityName == "TelefonoContacto" || ent.EntityName == "TelefonoPF") && attr.AttrName == "Tipo")
                                    {
                                        TipoTelefonoEnum enumTipoTel = (TipoTelefonoEnum)int.Parse(attr.AttrValue);
                                        dr[attr.AttrName] = enumTipoTel.ToString();
                                    }
                                    else
                                    {
                                        if (ent.EntityName.ToLowerInvariant() == "impuesto" && attr.AttrName.ToLowerInvariant() == "porcentaje")
                                        {
                                            dr["Nombre"] += " " + attr.AttrValue+"%";
                                        }
                                        else
                                        {
                                            if (ent.EntityName.ToLowerInvariant() == "ramo" && attr.AttrName.ToLowerInvariant() == "clave")
                                            {
                                                dr["Nombre"] += " [" + attr.AttrValue + "]";
                                            }
                                            else
                                            {
                                                if (ent.EntityName.ToLowerInvariant() == "recibo" && attr.AttrName.ToLowerInvariant() == "tipo")
                                                {
                                                    TipoReciboEnum enumTipoTel = (TipoReciboEnum)int.Parse(attr.AttrValue);
                                                    dr[attr.AttrName] = enumTipoTel.ToString();
                                                }
                                                else
                                                {
                                                    if (ent.EntityName.ToLowerInvariant() == "subramo" && attr.AttrName.ToLowerInvariant() == "clave")
                                                    {
                                                        dr["Nombre"] += " [" + attr.AttrValue + "]";
                                                    }
                                                    else
                                                    {
                                                        if (ent.EntityName.ToLowerInvariant() == "recibo" && (attr.AttrName.ToLowerInvariant() == "cobertura" || attr.AttrName.ToLowerInvariant() == "vencimiento" || attr.AttrName.ToLowerInvariant() == "fechapagocomision"))
                                                        {
                                                            DateTime daterecibo = DateTime.Now;
                                                            DateTime.TryParse(attr.AttrValue,out daterecibo);
                                                            dr[attr.AttrName] = daterecibo.ToShortDateString();
                                                        }
                                                        else
                                                        {
                                                            dr[attr.AttrName] = attr.AttrValue;
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                    retval.Rows.Add(dr);

                }
            }
            return retval;
        }

        public static List<Entity> GetValues(Entity Entidad, out string errmess)
        {
            errmess = "";
            string entidad = new JavaScriptSerializer().Serialize(Entidad);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = SiteFunctions.sendMessage(server + "/api/IncomingMessage/GetEntity", entidad, Method.POST);

            if (!opResult.Success)
            {
                errmess = opResult.ErrMessage;
                return new List<Entity>();
            }
            else
            {
                if (opResult.RetVal == null)
                {
                    return new List<Entity>();
                }
                return opResult.RetVal;
            }
        }

        public static Entity GetEntity(Entity Entidad, out string errmess)
        {
            errmess = "";
            string entidad = new JavaScriptSerializer().Serialize(Entidad);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            OperationResult opResult = SiteFunctions.sendMessage(server + "/api/IncomingMessage/GetEntity", entidad, Method.POST);

            if (!opResult.Success)
            {
                errmess = opResult.ErrMessage;
                return null;
            }
            else
            {
                if (opResult.RetVal == null)
                {
                    return null;
                }
                if(opResult.RetVal.Count == 0)
                {
                    return null;
                }
                return opResult.RetVal[0];
            }
        }

        public static List<OperationResult> execListSelect(List<Entity> _listEntity)
        {
            string errmess = "";
            string entidad = new JavaScriptSerializer().Serialize(_listEntity);
            string server = ConfigurationManager.AppSettings["Host"].ToString();
            List<OperationResult> listResults = sendListMessage(server + "/api/IncomingMessage/GetList", entidad);

            return listResults;
        }

        private static List<OperationResult> sendListMessage(string _urlApi, object _bodyParam)
        {
            try
            {
                var client = new RestClient(_urlApi);

                var request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");

                request.AddHeader("accept-encoding", "gzip, deflate");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "application/json");

                if (_bodyParam != null)
                    request.AddParameter("undefined", _bodyParam, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                serializer.MaxJsonLength = int.Parse(response.ContentLength.ToString());
                List<OperationResult> opResult = serializer.Deserialize(response.Content, typeof(List<OperationResult>)) as List<OperationResult>;
                return opResult;
            }
            catch (Exception exc)
            {
                List<OperationResult> opList = new List<OperationResult>();
                OperationResult opResult = new OperationResult();
                opResult.Success = false;
                opResult.ErrMessage = exc.Message;
                opList.Add(opResult);
                return opList;
            }
        }
        private static OperationResult sendMessage(string _urlApi, object _bodyParam, Method _method, bool _isAttachment = false)
        {
            try
            {
                var client = new RestClient(_urlApi);

                var request = new RestRequest(_method);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");

                request.AddHeader("accept-encoding", "gzip, deflate");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "application/json");

                if (_bodyParam != null)
                    request.AddParameter("undefined", _bodyParam, ParameterType.RequestBody);
                try
                {
                    if (ConfigurationManager.AppSettings["debug"].ToString() == "1")
                    {
                        System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath(@"\Logs\Request" + _method.ToString() + DateTime.Now.ToString("ddMMyyyyHHmmss")), _bodyParam.ToString(), Encoding.UTF8);
                    }
                }
                catch (Exception)
                {
                }
                IRestResponse response = client.Execute(request);
                OperationResult opResult = new OperationResult();
                if (!_isAttachment)
                {
                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    serializer.MaxJsonLength = int.Parse(response.ContentLength.ToString());

                    opResult = serializer.Deserialize(response.Content, typeof(OperationResult)) as OperationResult;
                }
                else
                {
                    opResult.Success = response.Content.ToLower() == "true" ? true : false;
                    if (!opResult.Success)
                    {
                        opResult.ErrMessage = "Error al cargar el documento a Sharepoint";
                    }
                }
                
                return opResult;
            }
            catch (Exception exc)
            {
                OperationResult opResult = new OperationResult();
                opResult.Success = false;
                opResult.ErrMessage = exc.Message;
                return opResult;
            }
        }
        public static void getDocumentSP(string entityName, string id, string parentId, string url)
        {
            AttachmentSP attachment = new AttachmentSP();
            attachment.EntityName = entityName;
            attachment.id = id;
            attachment.parentId = parentId;
            attachment.spUrlFile = url;
            attachment.folderToSave = ConfigurationManager.AppSettings["filesFolder"].ToString();
            string msgg = new JavaScriptSerializer().Serialize(attachment);

            string server = ConfigurationManager.AppSettings["Host"].ToString();
            IRestResponse response = sendSPMessage(server + "/api/Attachment/getAttachment", msgg, Method.POST);

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            serializer.MaxJsonLength = int.Parse(response.ContentLength.ToString());
        }
        public static AttachmentList execListAttachment(string entityName, string id, string parentId)
        {
            AttachmentSP attachment = new AttachmentSP();
            attachment.EntityName = entityName;
            attachment.id = id;
            attachment.parentId = parentId;
            string msgg = new JavaScriptSerializer().Serialize(attachment);

            string server = ConfigurationManager.AppSettings["Host"].ToString();
            IRestResponse response = sendSPMessage(server + "/api/Attachment/getDocumentList", msgg, Method.POST);

            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            serializer.MaxJsonLength = int.Parse(response.ContentLength.ToString());
            //string content = string.Format("{0}{1}AttachmentList{1}:{2}{3}", (char)123, (char)34, response.Content, (char)125);

            AttachmentList opResult = serializer.Deserialize<AttachmentList>(response.Content);
            return opResult;
        }
        private static IRestResponse sendSPMessage(string _urlApi, object _bodyParam, Method _method)
        {
            try
            {
                var client = new RestClient(_urlApi);

                var request = new RestRequest(_method);
                request.RequestFormat = DataFormat.Json;
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");

                request.AddHeader("accept-encoding", "gzip, deflate");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "application/json");

                if (_bodyParam != null)
                    request.AddParameter("undefined", _bodyParam, ParameterType.RequestBody);


                IRestResponse response = client.Execute(request);
                return response;
            }
            catch (Exception exc)
            {
                OperationResult opResult = new OperationResult();
                opResult.Success = false;
                opResult.ErrMessage = exc.Message;
                return null;
            }
        }
        public static OperationResult uploadfile(string _entityName, string _id, string _parentId, string _fileName, byte[] fileContent)
        {
            try
            {
                AttachmentSP attachment = new AttachmentSP();
                attachment.EntityName = _entityName;
                attachment.FileName = _fileName;
                attachment.FileContent = fileContent;
                attachment.id = _id;
                attachment.parentId = _parentId;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = int.MaxValue;
                string msgg = serializer.Serialize(attachment);

                string server = ConfigurationManager.AppSettings["Host"].ToString();
                OperationResult opResult = sendMessage(server + "/api/Attachment/uploadFile", msgg, Method.POST, true);
                return opResult;
            }
            catch (Exception exc)
            {
                OperationResult opResult = new OperationResult();
                opResult.Success = false;
                opResult.ErrMessage = exc.Message;
                return opResult;
            }
        }

        public static bool proccessTickets(float _totalPolicy, int _noTickets, DateTime _initDate, int _intervalPerTicket, float _totalCommission, float _totalExpenses, float _firstPrima, float _firstExpenses, float _taxPrc, int _serie, int _curyId, int _taxId, int _poliza, int _location, int _benefitCenter, int _owner, Auditable audit, out string errMsg, bool prorratearDerechos,string ComisionPor,string RecargoPor,string SobrecomisionPor,string BonoPor, float totalrecargo, bool VP, float _curyRate = 1,string Tipo = "0")
        {
            errMsg = string.Empty;
            bool retVal = true;
            int _noIncisos = 1;
            int bienasegurado = 0;
            float ComPor=0, SobComPor=0, BoPor=0,RecPor=0;
            bool hasFirstPrima = false, hasFirstExpense = false;
            float commissionPerTicket = 0, expensePerTicket = 0;
            float primaPerTicket = (_totalPolicy) / _noTickets;
            float recargosperticket = (totalrecargo / _noTickets);
            if (ComisionPor != "")
            {
                float.TryParse(ComisionPor, out ComPor);
            }
            if (SobrecomisionPor != "")
            {
                float.TryParse(SobrecomisionPor, out SobComPor);
            }
            if (BonoPor != "")
            {
                float.TryParse(BonoPor, out BoPor);
            }
            if (RecargoPor != "")
            {
                float.TryParse(RecargoPor, out RecPor);
            }
            if (_firstPrima != 0)
            {
                if (_noTickets > 1)
                {
                    primaPerTicket = (_totalPolicy - _firstPrima) / (_noTickets - 1);
                }
                else
                {
                    primaPerTicket = _totalPolicy;
                }
                hasFirstPrima = true;
            }

            if (_firstExpenses != 0)
            {
                _totalExpenses = _totalExpenses - _firstExpenses;
                hasFirstExpense = true;
            }
            List<Entity> Bienes = new List<Entity>();
            if (VP)
            {
                Entity BienAsegurado = new Entity("BienAsegurado");
                BienAsegurado.Attributes.Add(new Attribute("Id"));
                BienAsegurado.Attributes.Add(new Attribute("Prima"));
                BienAsegurado.Keys.Add(new Key("Poliza",_poliza.ToString(),"int"));
                BienAsegurado.Keys.Add(new Key("Estatus", "1", "int"));
                try
                {
                    Bienes = SiteFunctions.GetValues(BienAsegurado, out errMsg);
                    _noIncisos = Bienes.Count();
                }
                catch (Exception)
                {
                    _noIncisos = 1;
                }

            }
            
            if (_noIncisos == 0)
            {
                _noIncisos = 1;
            }

            for (int counterinciso = 0; counterinciso < _noIncisos;counterinciso++ )
            {
                if (VP)
                {
                    float.TryParse(Bienes[counterinciso].getAttrValueByName("Prima"), out primaPerTicket);
                    int.TryParse(Bienes[counterinciso].getAttrValueByName("Id"), out bienasegurado);
                    if (_firstPrima != 0)
                    {
                        if (_noTickets > 1)
                        {
                            primaPerTicket = (primaPerTicket - _firstPrima) / (_noTickets - 1);
                        }
                        
                    }
                    else
                    {
                        primaPerTicket = (primaPerTicket) / _noTickets;
                    }
                }
                commissionPerTicket = _totalCommission / _noTickets;
                if (VP && !prorratearDerechos)
                {
                    expensePerTicket = _totalExpenses / _noIncisos;
                }
                else
                {
                    if (VP && prorratearDerechos)
                    {
                        expensePerTicket = _totalExpenses / _noIncisos / _noTickets;
                    }
                    else
                    {
                        if (_noTickets > 1 && hasFirstExpense)
                        {
                            expensePerTicket = _totalExpenses / _noIncisos / (_noTickets - 1);
                        }
                        else
                        {
                            expensePerTicket = _totalExpenses/_noIncisos/_noTickets;
                        }
                    }
                }

                for (int counter = 0; counter < _noTickets; counter++)
                {
                    DateTime ticketDate = _initDate;
                    
                    float expense = 0;
                    if (VP && !prorratearDerechos)
                    {
                        if (counter == 0)
                        {
                            expense = expensePerTicket;
                        }
                    }
                    else
                    {
                        if (VP && prorratearDerechos)
                        {
                            expense = expensePerTicket;
                        }
                        else
                        {
                            expense = expensePerTicket;
                            
                        }
                    }
                    float prima = primaPerTicket;
                    if (counter == 0)
                    {
                        ticketDate = _initDate;
                        if (hasFirstPrima && _noTickets > 1)
                        {
                            prima = _firstPrima;
                        }

                        if (hasFirstExpense && _noTickets > 1)
                        {
                            expense = _firstExpenses;
                        }

                        if (hasFirstExpense && _noTickets == 1)
                        {
                            expense = _firstExpenses;
                        }
                    }
                    float commission = commissionPerTicket;
                    commission = (prima * (ComPor / 100)) + (prima * (SobComPor / 100)) + (prima * (BoPor / 100));
                    recargosperticket = prima * (RecPor / 100);
                    DateTime dueDate = _initDate.AddMonths(_intervalPerTicket);
                    float totalTicket = primaPerTicket + expense+recargosperticket;
                    if(hasFirstExpense)
                    {
                        totalTicket = prima + expense + recargosperticket;
                    }
                    float taxAmt = 0;
                    if (_taxPrc != 0)
                    {
                        taxAmt = (totalTicket * (_taxPrc/100));
                    }
                    string ticketNum = string.Format("{0}/{1}", counter + 1, _noTickets);

                    if (!SiteFunctions.createTicket(commission, expense, ticketDate, dueDate, taxAmt, ticketNum, prima, _serie, _curyId, _taxId, _poliza, _location, _benefitCenter, _owner, _curyRate, audit, out errMsg,recargosperticket,Tipo,bienasegurado))
                    {
                        retVal = false;
                        break;
                    }

                    _initDate = dueDate;
                }
            }

            return retVal;
        }

        public static bool createTicket(float _commission, float _expense, DateTime _dateInit, DateTime _dueDateTicket, float _taxAmt, string _ticketNum, float _prima, int _serie, int _curyId, int _taxId, int _poliza, int _location, int _benefitCenter, int _owner, float _curyRate, Auditable audit, out string errmsg,float recargosperticket,string Tipo,int _bienasegurado=0)
        {
            Entity recibo = new Entity("Recibo");

            recibo.auditable = audit;
            recibo.Attributes.Add(new Attribute("Permiso", "2", "int"));
            recibo.Attributes.Add(new Attribute("Tipo", Tipo, "int"));
            recibo.Attributes.Add(new Attribute("Estatus", "0", "int"));
            recibo.Attributes.Add(new Attribute("Ubicacion", _location.ToString(), "int"));
            recibo.Attributes.Add(new Attribute("CentroDeBeneficio", _benefitCenter.ToString(), "int"));
            recibo.Attributes.Add(new Attribute("Propietario", _owner.ToString(), "int"));

            recibo.Attributes.Add(new Attribute("Comision", _commission.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Recargo", recargosperticket.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Comisioncobranza", "0", "float"));
            recibo.Attributes.Add(new Attribute("comisionPagada", "0", "float"));
            recibo.Attributes.Add(new Attribute("Cobertura", _dateInit.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new Attribute("Expedicion", _dateInit.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new Attribute("Vencimiento", _dueDateTicket.ToShortDateString(), "datetime"));
            recibo.Attributes.Add(new Attribute("Gastos", _expense.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("ImpuestoImporte", _taxAmt.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Numero", _ticketNum, "string"));
            recibo.Attributes.Add(new Attribute("PrimaNeta", _prima.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Serie", _serie.ToString(), "int"));
            recibo.Attributes.Add(new Attribute("Total", (_prima + _expense +recargosperticket+ _taxAmt).ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Concepto", _ticketNum, "float"));
            recibo.Attributes.Add(new Attribute("Moneda", _curyId.ToString(), "int"));
            recibo.Attributes.Add(new Attribute("Impuesto", _taxId.ToString(), "int"));
            recibo.Attributes.Add(new Attribute("TipoCambio", _curyRate.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("TipoCambioAbono", _curyRate.ToString(), "float"));
            recibo.Attributes.Add(new Attribute("Tramite", _poliza.ToString(), "int"));
            if (_bienasegurado != 0)
            {
                recibo.Attributes.Add(new Attribute("Inciso", _bienasegurado.ToString(), "int"));
            }

            errmsg = string.Empty;
            bool success = SiteFunctions.SaveEntity(recibo, Method.POST, out errmsg);

            return success;
        }

        public static bool importLiq(Stream _stream, Dictionary<string,string> _polizaId,string LiquidacionId, bool _isXlsx, string userId, out decimal totalIncisos, out string errmess, out decimal totalComisiones)
        {
            totalComisiones = 0;
            totalIncisos = 0;
            
            bool retVal = true;
            bool passFirstRow = false;
            errmess = string.Empty;
            DataTable dtResult = null;
            string polid = string.Empty;
            string intpolid = "0";
            try
            {
                dtResult = ExcelToDataTableUsingExcelDataReader(_stream, _isXlsx);
            }
            catch (Exception)
            {
                errmess = "El archivo no está en el formato correcto, favor de verificar.";
            }
            if (dtResult != null)
            {
                if (dtResult.Columns.Count == 7)
                {
                    int _userid = 0;
                    int.TryParse(userId, out _userid);

                    Auditable audi = new Auditable();
                    audi.userId = _userid;
                    audi.opDate = DateTime.Now;
                    Entity Poliza = new Entity("Tramite"); 
                    if (ValidateLayoutIntegraciones(dtResult, _polizaId, out errmess))
                    {
                        foreach (DataRow row in dtResult.Rows)
                        {
                            if (passFirstRow)
                            {
                                if (row[0].ToString() != polid)
                                {
                                    polid = row[0].ToString();
                                    Poliza = new Entity("Tramite");
                                    Poliza.Attributes.Add(new Attribute("Id"));
                                    Poliza.Keys.Add(new Key("Folio", polid, "string"));
                                    Poliza = SiteFunctions.GetEntity(Poliza, out errmess);
                                }
                                
                                decimal prima = 0;
                                decimal comision = 0;
                                #region mapea bien asegurado
                                Entity IntegracionPago = new Entity("IntegracionPagoPaso");
                                IntegracionPago.useAuditable = false;
                                //IntegracionPago.auditable = audi;
                                IntegracionPago.Attributes.Add(new Attribute("Permiso", "2", "int"));
                                IntegracionPago.Attributes.Add(new Attribute("Estatus", "1", "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Ubicacion", "2185", "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("CentroDeBeneficio", "2230", "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Propietario", userId, "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Poliza", Poliza.getAttrValueByName("Id"), "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Inciso", row[1].ToString(), "string"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("SecuenciaRecibo", row[2].ToString(), "string"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("NombreCliente", row[4].ToString(), "string"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("FormaPago", row[3].ToString(), "string"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Prima", row[5].ToString(), "float"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("ImporteComision", row[6].ToString(), "float"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Liquidacion", LiquidacionId, "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("FechaAdd", DateTime.Now.ToString("dd/MM/yyyy"), "datetime"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("FechaUMod", DateTime.Now.ToString("dd/MM/yyyy"), "datetime"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("UsuarioAdd", userId, "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("UsuarioUMod", userId, "int"));
                                IntegracionPago.Attributes.Add(new Models.Attribute("Id", "null", "int"));
                                
                                #endregion
                                
                                decimal.TryParse(row[5].ToString(), out prima);
                                decimal.TryParse(row[6].ToString(), out comision);
                                totalIncisos += prima;
                                totalComisiones += comision;
                                retVal = SaveEntity(IntegracionPago, Method.POST, out errmess);
                            }
                            passFirstRow = true;
                        }
                    }
                    else
                    {
                        retVal = false;
                    }


                }
                else
                {
                    errmess = "El número de columnas que contiene el archivo no concuerdan con el layout solicitado (7 Columnas).";
                    retVal = false;
                }
            }
            return retVal;
        }

       /* public string GeneraCartaPoliza(object o, Plantilla plantilla, Usuario usuarioFirmado)
        {
            var poliza = o as PolizaGeneral;
            var xml = PolizaToXML.serializaPolizaGeneral(poliza);

            if (poliza.Endosos != null)
                xml.FirstNode.AddAfterSelf(EndosoToXML.serializaEndosos(poliza));

            if (poliza.Siniestros != null)
                xml.FirstNode.AddAfterSelf(SiniestroToXML.serializaSiniestros(poliza));

            ///***Individuales
            if (typeof(PolizaIndividual).IsAssignableFrom(o.GetType()))
            {
                var polizaInd = o as PolizaIndividual;

                //*****GMM 
                if (typeof(PolizaGmmIndividual).IsAssignableFrom(o.GetType()))
                {
                    var polAutoInd = polizaInd as PolizaGmmIndividual;
                    PolizaToXML.serializaPolizaGmmIndividual(polAutoInd, xml);
                }

                //****Vida
                if (typeof(PolizaVidaIndividual).IsAssignableFrom(o.GetType()) && polizaInd.Movimiento != null)
                {
                    var polVidaInd = polizaInd as PolizaVidaIndividual;
                    xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimiento(polVidaInd.Movimiento));
                }

                //****Autos
                if (typeof(PolizaAutoIndividual).IsAssignableFrom(o.GetType()) && polizaInd.Movimiento != null)
                {
                    var polAutoInd = polizaInd as PolizaAutoIndividual;
                    xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimiento(polAutoInd.Movimiento));
                }


            }

            ///***Pool
            else if (typeof(PolizaPool).IsAssignableFrom(o.GetType()))
            {
                var polizaPool = o as PolizaPool;
                if (polizaPool.Movimientos != null)
                    xml.FirstNode.AddAfterSelf(PolizaToXML.serializaMovimientos(polizaPool));

                //*****GMM
                if (typeof(PolizaGmmPoblacion).IsAssignableFrom(o.GetType()))
                {
                    var polGmmPool = polizaPool as PolizaGmmPoblacion;
                    PolizaToXML.serializaPolizaGmmPoblacion(polGmmPool, xml);
                }

                //*****VIDA
                if (typeof(PolizaVidaPoblacion).IsAssignableFrom(o.GetType()))
                {
                    var polVidaPool = polizaPool as PolizaVidaPoblacion;
                    PolizaToXML.serializaPolizaVidaPoblacion(polVidaPool, xml);
                }
            }
            else
            {
                throw new Exception("El objeto " + o.GetType() + " no es asignable para nigun tipo.");
            }

            xml.Add(new XElement("UsuarioFirmado",
                new XElement("Nombre", usuarioFirmado.Nombre),
                new XElement("Email", usuarioFirmado.Email),
                new XElement("Puesto", usuarioFirmado.Puesto.Nombre))
            );

            xml.Add(new XElement("FechaOperacion", DateTime.Now.ToString("dd/MM/yyyy")));

            if (Logger.IsDebugEnabled)
            {
                Logger.DebugFormat("XML generado: {0}", xml);
            }

            ///***XSL Transformation
            var result = XSLRenderer.Transform(xml, plantilla);

            if (poliza.Ramo.DivisionOperativa == DivisionOperativaEnum.Autos)
            {
                if (poliza.Coberturas != null && !string.IsNullOrEmpty(poliza.Coberturas.Descripcion))
                {
                    result = result.Replace("?Slip",
                        string.Format(@"
                ?Slip
            </td>
          </tr>
        </table><table class='slip'>
          <tr>
            <td>
              Coberturas:
            </td>
          </tr>
          <tr>
            <td>
              {0}", poliza.Coberturas.Descripcion));
                }
            }

            if (poliza.Ramo.DivisionOperativa == DivisionOperativaEnum.Autos)
            {
                if (poliza.CondicionesEspeciales != null && !string.IsNullOrEmpty(poliza.CondicionesEspeciales.Descripcion))
                {
                    result = result.Replace("?Slip",
                        string.Format(@"
                ?Slip
            </td>
          </tr>
        </table><table class='slip'>
          <tr>
            <td>
              Condiciones Especiales:
            </td>
          </tr>
          <tr>
            <td>
              {0}", poliza.CondicionesEspeciales.Descripcion));
                }
            }

            result = result.Replace("?Slip", poliza.Slip.Descripcion);

            return WebUtility.HtmlEncode(result);
        }
        */
        public void GetPolizaTramite(ref Entity poliza,ref Entity tramite)
        {
            
            string errmsg = String.Empty;
            poliza = GetEntity(poliza, out  errmsg);
            tramite = GetEntity(tramite, out errmsg);
        }

        
        /// <summary>
        /// Genera la cadena HTML de una carta utilizando la plantilla XSL especificada
        /// y los elementos XML para complementar la plantilla.
        /// </summary>
        /// <param name="xml">XML con los datos a poner en la plantilla.</param>
        /// <param name="plantilla">Plantilla de la que se obtendra el XSL.</param>
        /// <returns>Devuelve cadena HTML con la estructura de la carta y sus datos.</returns>
        public static string Transform(XElement xml, Entity plantilla)
        {
            var polizaXMLString = xml.ToString();
            var xmlData = XDocument.Parse(polizaXMLString);
            var xsl = XDocument.Parse(plantilla.getAttrValueByName("Valor").ToString());
            var transformedDoc = new XDocument();

            using (var writer = transformedDoc.CreateWriter())
            {
                var transformer = new XslCompiledTransform();
                transformer.Load(xsl.CreateReader());
                transformer.Transform(xmlData.CreateReader(), writer);
            }

            return transformedDoc.ToString();
        }
        
    }
}