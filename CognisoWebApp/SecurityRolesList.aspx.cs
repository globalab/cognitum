﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class SecurityRolesList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                searchRoles();
            }
        }

        private void searchRoles()
        {
            string errmsg = string.Empty;
            Entity roles = new Entity("securityrole");
            roles.Attributes.Add(new Models.Attribute("id"));
            roles.Attributes.Add(new Models.Attribute("rolename"));
            roles.useAuditable = false;
            List<Entity> result = SiteFunctions.GetValues(roles, out errmsg);
            DataTable dtResults = SiteFunctions.trnsformEntityToDT(result);
            GVRoles.DataSource = dtResults;
            GVRoles.DataBind();
        }

        protected void GVRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVRoles.PageIndex = e.NewPageIndex;
            GVRoles.DataBind();
            searchRoles();
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("SecurityRole.aspx");
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }
    }
}