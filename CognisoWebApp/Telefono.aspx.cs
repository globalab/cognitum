﻿using CognisoWA.Controllers;
using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CognisoWebApp
{
    public partial class Telefono : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {

                object ContactId = Request.QueryString["contactid"];
                object CustomerId = Request.QueryString["custid"];
                object created = Request.QueryString["created"];
                if (ContactId != null)
                {
                    ContactoPM.Value = ContactId.ToString();
                    CustId.Value = string.Empty;
                }
                if (CustomerId != null)
                {
                    ContactoPM.Value = string.Empty;
                    CustId.Value = CustomerId.ToString();
                }
                initCatalog();
                object id = Request.QueryString["id"];

                if (id != null)
                {
                    TelId.Value = id.ToString();
                    txtId.Text = id.ToString();
                    this.searchTel(id.ToString());

                }
                if (created != null)
                {
                    this.showMessage("Se guardo correctamente la dirección", false);
                }
                
                
            }
        }

        private void searchTel(string _id)
        {
            string errMsg = string.Empty, idContactoPM = string.Empty;
            string EntityName = CustId.Value != string.Empty ? "TelefonoPF" : "TelefonoContacto";
            Entity Tel = new Entity(EntityName);
            Tel.useAuditable = true;
            Tel.Keys.Add(new Models.Key("id", _id, "int64"));

            List<Entity> dirResult = SiteFunctions.GetValues(Tel, out errMsg);
            if (dirResult.Count > 0)
            {
                Entity tl = dirResult[0];

                idContactoPM = tl.getAttrValueByName("Contacto");
                cmbTipo.Text = tl.getAttrValueByName("Tipo");
                txtExt.Text = tl.getAttrValueByName("Extension");
                txtLadaI.Text = tl.getAttrValueByName("LadaInternacional");
                txtLadaN.Text = tl.getAttrValueByName("LadaNacional");
                txtNombre.Text = tl.getAttrValueByName("Nombre");
                txtNumero.Text = tl.getAttrValueByName("Numero");
                DateTime fechaaudit = Convert.ToDateTime(tl.getAttrValueByName("FechaAdd"));
                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                fechaaudit = Convert.ToDateTime(tl.getAttrValueByName("FechaUMod"));
                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", tl.getAttrValueByName("UsuarioAdd"), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", tl.getAttrValueByName("UsuarioUMod"), "int64"));
                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtId.Text = _id;
            }

            //Entity contacto = new Entity("ContactoPersonaMoral");
            //contacto.Attributes.Add(new Models.Attribute("Nombre"));
            //contacto.Keys.Add(new Models.Key("id", idContactoPM, "int"));
            //string errmess = string.Empty;
            //List<Entity> listResult = SiteFunctions.GetValues(contacto, out errmess);
            //if (listResult.Count > 0)
            //{
            //    txtContacto.Text = listResult[0].getAttrValueByName("Nombre").ToString();
            //}

            
        }

        public void initCatalog()
        {

            Array itemValues = System.Enum.GetValues(typeof(TipoTelefonoEnum));
            Array itemNames = System.Enum.GetNames(typeof(TipoTelefonoEnum));

            for (int i = 0; i <= itemNames.Length - 1; i++)
            {
                ListItem item = new ListItem(itemNames.GetValue(i).ToString(), ((int)itemValues.GetValue(i)).ToString());
                cmbTipo.Items.Add(item);
            }

            if (ContactoPM.Value != string.Empty)
            {
                Entity contacto = new Entity("ContactoPersonaMoral");
                contacto.Attributes.Add(new Models.Attribute("Nombre"));
                contacto.Keys.Add(new Models.Key("id", ContactoPM.Value, "int"));
                string errmess = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(contacto, out errmess);
                if (listResult.Count > 0)
                {
                    txtContacto.Text = listResult[0].getAttrValueByName("Nombre").ToString();
                }
            }
            else
            {
                Entity Cliente = new Entity("PersonaFisica");
                Cliente.Attributes.Add(new Models.Attribute("Nombre"));
                Cliente.Attributes.Add(new Models.Attribute("ApellidoMaterno"));
                Cliente.Attributes.Add(new Models.Attribute("ApellidoPaterno"));
                Cliente.Keys.Add(new Models.Key("id", CustId.Value, "int"));
                string errmess = string.Empty;
                List<Entity> listResult = SiteFunctions.GetValues(Cliente, out errmess);
                if (listResult.Count > 0)
                {
                    txtContacto.Text = listResult[0].getAttrValueByName("Nombre").ToString() + " " + listResult[0].getAttrValueByName("ApellidoPaterno").ToString() + " " + listResult[0].getAttrValueByName("ApellidoMaterno").ToString();
                }
            }
        }

        
        
        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveTel();
        }

        
        private void SaveTel()
        {

            JoinEntity joinEntity = new JoinEntity();
            string EntityName = CustId.Value != string.Empty ? "TelefonoPF" : "TelefonoContacto";
            Entity Tel = new Entity(EntityName);
            Auditable audi = new Auditable();
            int userid = 0;
            int.TryParse(SiteFunctions.getuserid(Context.User.Identity.Name, false), out userid);
            audi.userId = userid;
            audi.opDate = DateTime.Now;


            Tel.Attributes.Add(new Models.Attribute("Nombre", txtNombre.Text, "string"));
            Tel.Attributes.Add(new Models.Attribute("Tipo", cmbTipo.Text, "int"));
            Tel.Attributes.Add(new Models.Attribute("Extension", txtExt.Text, "string"));
            Tel.Attributes.Add(new Models.Attribute("LadaInternacional", txtLadaI.Text, "string"));
            Tel.Attributes.Add(new Models.Attribute("LadaNacional", txtLadaN.Text, "string"));
            Tel.Attributes.Add(new Models.Attribute("Numero", txtNumero.Text, "string"));
            if (ContactoPM.Value != string.Empty)
            {
                Tel.Attributes.Add(new Models.Attribute("Contacto", ContactoPM.Value, "int64"));
            }
            else
            {
                Tel.Attributes.Add(new Models.Attribute("Contacto", CustId.Value, "int64"));
            }
            
            
           
            string errmess = "";
            object id = TelId.Value == ""?null:TelId.Value;
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                Tel.Keys.Add(new Models.Key("Id", id.ToString(), "int"));

                audi.entityId = Convert.ToInt32(id);

            }
            Tel.auditable = audi;
            if (SiteFunctions.SaveEntity(Tel, method, out errmess))
            {
                showMessage("Se guardo correctamente el Teléfono", false);
                Session["retVal"] = errmess;
                TelId.Value = errmess;
                
                
            }
            else
            {
                showMessage(errmess, true);
            }
        }


        

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                 cstext1.Append("script>");
                
                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
    }
}