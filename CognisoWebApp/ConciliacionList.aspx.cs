﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class ConciliacionList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                searchForConciliacion();
            }
        }

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Conciliacion.aspx");
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForConciliacion(SearchId.Value);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForConciliacion(SearchId.Value);
        }

        protected void GVConciliacion_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVConciliacion.PageIndex = e.NewPageIndex;
            GVConciliacion.DataBind();
            searchForConciliacion();
        }
        private void searchForConciliacion(string searchVal = "")
        {
            JoinEntity joinEntity = new JoinEntity();
            selectJoinEntity selJoin = new selectJoinEntity();
            List<Entity> entities = new List<Entity>();
            string errmess = "";
            Entity Conciliacion = new Entity("Conciliacion");
            Conciliacion.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Conciliacion.Attributes.Add(new Models.Attribute("Aseguradora", "", "int"));
            Conciliacion.Attributes.Add(new Models.Attribute("Moneda", "", "int"));
            Conciliacion.Attributes.Add(new Models.Attribute("Estatus", "", "int"));

            if (searchVal != string.Empty)
            {
                Conciliacion.Keys.Add(new Key("Id", searchVal, "int"));
            }
            switch (ConciliacionesView.SelectedValue)
            {
                case "2":
                    Conciliacion.Keys.Add(new Key("Estatus", "0", "int", 1));
                    break;
                case "3":
                    Conciliacion.Keys.Add(new Key("Estatus", "1", "int", 1));
                    break;
                case "4":
                    Conciliacion.Keys.Add(new Key("Estatus", "3", "int", 1));
                    break;
                case "5":
                    Conciliacion.Keys.Add(new Key("Estatus", "2", "int", 1));
                    break;

            }

            Conciliacion.useAuditable = false;

            Entity aseguradora = new Entity("Aseguradora");
            aseguradora.Attributes.Add(new Models.Attribute("Id", "", "int"));
            aseguradora.Attributes.Add(new Models.Attribute("Clave", "", "string"));
            aseguradora.useAuditable = false;

            selJoin = new selectJoinEntity("Aseguradora", 1, "id");

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = aseguradora;
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList.Add(selJoin);

            Conciliacion.ChildEntities.Add(joinEntity);

            Entity Moneda = new Entity("Moneda");
            Moneda.Attributes.Add(new Models.Attribute("Id", "", "int"));
            Moneda.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
            Moneda.useAuditable = false;

            selJoin = new selectJoinEntity("Moneda", 1, "id");

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = Moneda;
            joinEntity.JoinType = JoinType.Left;
            joinEntity.selectJoinList.Add(selJoin);

            Conciliacion.ChildEntities.Add(joinEntity);


            List<Entity> listLiq = SiteFunctions.GetValues(Conciliacion, out errmess);
            DataTable dtLiqList = SiteFunctions.trnsformEntityToDT(listLiq);
            GVConciliacion.DataSource = dtLiqList;
            GVConciliacion.DataBind();
        }

        protected void ConciliacionesView_SelectedIndexChanged(object sender, EventArgs e)
        {
            SearchId.Value = txtSearch.Text;
            searchForConciliacion(SearchId.Value);
        }
    }
}