﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class searchRecibosCon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string liqId = Request.QueryString["liq"];
                string ConId = Request.QueryString["conId"];
                if (!string.IsNullOrEmpty(liqId))
                {
                    searchRecibosNoConciliacion(liqId);
                }
            }
        }

        private Entity initRecibosSearch()
        {
            Entity recibos = new Entity("recibo");
            recibos.useAuditable = false;
            recibos.Attributes.Add(new Models.Attribute("Id"));
            recibos.Attributes.Add(new Models.Attribute("Cobertura"));
            recibos.Attributes.Add(new Models.Attribute("Vencimiento"));
            recibos.Attributes.Add(new Models.Attribute("Concepto"));
            recibos.Attributes.Add(new Models.Attribute("Total"));
            recibos.Attributes.Add(new Models.Attribute("Numero"));
            recibos.Attributes.Add(new Models.Attribute("Estatus"));

            Entity tramite = new Entity("tramite");
            tramite.Attributes.Add(new Models.Attribute("Folio"));

            selectJoinEntity selJoin = new selectJoinEntity("tramite", 1, "id");
            JoinEntity joinEnt = new JoinEntity();
            joinEnt.ChildEntity = tramite;
            joinEnt.JoinType = JoinType.Inner;
            joinEnt.selectJoinList.Add(selJoin);

            recibos.ChildEntities.Add(joinEnt);

            return recibos;
        }

        private void searchRecibosNoConciliacion(string liqId)
        {
            string ErrMsg = string.Empty;
            Entity recibos = initRecibosSearch();

            recibos.Keys.Add(new Key("Estatus", "3", "int",1,1)); //Aplicados
            recibos.Keys.Add(new Key("liquidacion", liqId, "int",1,1)); //liquidación
            if (txtSearchRecibos.Text != string.Empty)
            {
                recibos.Keys.Add(new Key(""));
            }
            List<Entity> result = SiteFunctions.GetValues(recibos, out ErrMsg);
            gvSearchRecibo.DataSource = SiteFunctions.trnsformEntityToDT(result);
            gvSearchRecibo.DataBind();
        }

        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            string Id = Request.QueryString["conId"];
            foreach (GridViewRow row in gvSearchRecibo.Rows)
            {
                CheckBox chkBox = row.FindControl("chkSel") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string ReciboId = gvSearchRecibo.Rows[row.RowIndex].Cells[1].Text;
                        Entity recibo = new Entity("Recibo");
                        recibo.Attributes.Add(new Models.Attribute("Conciliacion", Id, "int"));
                        recibo.Attributes.Add(new Models.Attribute("Estatus", "4", "int"));
                        recibo.Keys.Add(new Key("Id", ReciboId, "int"));
                        SiteFunctions.SaveEntity(recibo, RestSharp.Method.PUT, out errMsg);
                        
                    }
                }
            }
            showMessage("Recibos agregados correctamente", false);
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        protected void gvSearchRecibo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string Id = Request.QueryString["liq"];
            gvSearchRecibo.PageIndex = e.NewPageIndex;
            gvSearchRecibo.DataBind();
            searchRecibosNoConciliacion(Id);
        }

        protected void BtnSearchRecibos_Click(object sender, EventArgs e)
        {
            string Id = Request.QueryString["liq"];
            searchRecibosNoConciliacion(Id);
        }

       

      

    }
}