﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class SearchIncisoEndosoD : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string _endosoId = Request.QueryString["EndosoId"];
                string _polizaId = Request.QueryString["polizaid"];
                if (!string.IsNullOrEmpty(_endosoId))
                {
                    EndosoId.Value = _endosoId;
                    PolizaId.Value = _polizaId;
                    searchIncisoNoEndoso();
                }
            }
        }

        private Entity initIncisoSearch()
        {
            Entity entity = new Entity("BienAsegurado");
            entity.EntityName = "BienAsegurado";
            entity.Attributes.Add(new Models.Attribute("Id", "", "int"));
            entity.Attributes.Add(new Models.Attribute("Prima", "", "float"));
            entity.Attributes.Add(new Models.Attribute("Poliza"));
            entity.Keys.Add(new Key("Poliza", PolizaId.Value, "int"));
            //entity.Keys.Add(new Key("EndosoAlta", EndosoId.Value, "int"));
            entity.Keys.Add(new Key("Estatus", "1", "int", 1, 1)); //Activos
            //if (_searchText != string.Empty)
            //{
            //    entity.Keys.Add(new Key("Id", _searchText, "string", 1));
            //}
            Entity auto = new Entity("Auto");
            auto.Attributes.Add(new Models.Attribute("Serie", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Motor", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Placas", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Modelo", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Marca", "", "string"));
            auto.Attributes.Add(new Models.Attribute("ConductorHabitual", "", "string"));
            auto.Attributes.Add(new Models.Attribute("Version", "", "string"));

            selectJoinEntity selJoinAuto = new selectJoinEntity("id", 1, "id");
            JoinEntity joinEntityAuto = new JoinEntity();
            joinEntityAuto.ChildEntity = auto;
            joinEntityAuto.selectJoinList = new List<selectJoinEntity>();
            joinEntityAuto.selectJoinList.Add(selJoinAuto);
            joinEntityAuto.JoinType = JoinType.Inner;
            entity.ChildEntities.Add(joinEntityAuto);

            return entity;
        }

        private void searchIncisoNoEndoso()
        {
            string ErrMsg = string.Empty;
            Entity Incisos = initIncisoSearch();

            //Incisos.Keys.Add(new Key("Estatus", "1", "int", 1, 1)); //Activos
            
            if (txtSearchIncisos.Text != string.Empty)
            {
                Incisos.Keys.Add(new Key(""));
            }
            List<Entity> result = SiteFunctions.GetValues(Incisos, out ErrMsg);
            gvSearchInciso.DataSource = SiteFunctions.trnsformEntityToDT(result);
            gvSearchInciso.DataBind();
        }

        protected void BtnAceptar_Click(object sender, EventArgs e)
        {
            string errMsg = string.Empty;
            string Id = PolizaId.Value;
            foreach (GridViewRow row in gvSearchInciso.Rows)
            {
                CheckBox chkBox = row.FindControl("chkBxSelect") as CheckBox;
                if (chkBox != null)
                {
                    if (chkBox.Checked)
                    {
                        string IncisoId = gvSearchInciso.Rows[row.RowIndex].Cells[1].Text;
                        Entity Inciso = new Entity("BienAsegurado");
                        Inciso.Attributes.Add(new Models.Attribute("EndosoBaja", EndosoId.Value, "int"));
                        Inciso.Attributes.Add(new Models.Attribute("Estatus", "4", "int"));
                        Inciso.Keys.Add(new Key("Id", IncisoId, "int"));
                        SiteFunctions.SaveEntity(Inciso, RestSharp.Method.PUT, out errMsg);
                        
                    }
                }
            }
            showMessage("Incisos agregados correctamente", false);
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }

        

      
        protected void gvSearchInciso_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            gvSearchInciso.PageIndex = e.NewPageIndex;
            gvSearchInciso.DataBind();
            searchIncisoNoEndoso();
        }

        protected void BtnSearchIncisos_Click(object sender, EventArgs e)
        {
            
            searchIncisoNoEndoso();
        }

       

      

    }
}