﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CancelPolizasList.aspx.cs" Inherits="CognisoWebApp.CancelPolizasList" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=txtDate]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
    <script type="text/javascript">
        function collapsediv() {
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

            //Remove the previous selected Pane.
            $("#accordion .in").removeClass("in");

            //Set the selected Pane.
            $("#" + paneName).collapse("show");

        }


        function registerdivName(divname) {

            $("[id*=PaneName]").val(divname);
            var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";

        }
    </script>

    <div class="container">
        <div class="container" runat="server">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="Default.aspx">Inicio</a></li>
                    <li class="breadcrumb-item active"><a href="#">Lista de Polizas</a></li>
                </ol>
            </nav>
            <div class="panel-heading">
                <asp:Label ID="lblTitle" CssClass="panel-title" runat="server">Lista de Polizas</asp:Label>
            </div>

            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ImageUrl="/Content/Imgs/writing.png" runat="server" OnClick="BtnSave_Click" ToolTip="Abrir"/>
                    <%--<asp:ImageButton ID="btnDelete" data-target="#pnlModal" data-toggle="modal" OnClientClick="javascript:return false;" ImageUrl="/Content/Imgs/delete.png" runat="server" /> --%>
                </div>
            </div>
            <div class="input-group mb-3">
                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" type="search" placeholder="Id,Nombre o RFC a buscar" aria-label="Id,Nombre o RFC a buscar" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
                <div class="input-group-append">
                    <asp:Button ID="btnBuscar" runat="server" CssClass="btn btn-outline-secondary" type="button" Text="Buscar" OnClick="btnBuscar_Click"></asp:Button>
                    <label class="form-control">Linea de negocio: </label>
                              <asp:DropDownList Id="ProspectView" runat="server" Cssclass="form-control" OnSelectedIndexChanged="ProspectView_SelectedIndexChanged" AutoPostBack="true" >
                                <asp:ListItem Value="1">Autos</asp:ListItem>
                                <asp:ListItem Value ="2">Vida</asp:ListItem>
                                <asp:ListItem Value="3">GMM</asp:ListItem>
                                <asp:ListItem Value="4">Daños</asp:ListItem>
                            </asp:DropDownList>
                </div>
            </div>
            <div class="panel-body" runat="server">

                <div class="table-responsive" runat="server">
                    <asp:GridView ID="GVPolizasList" runat="server" OnPageIndexChanging="GridList_PageIndexChanging" AllowPaging="true" AutoGenerateColumns="False" CssClass="table table-hover table-striped" EnableModelValidation="True" ShowHeader="true">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkEliminar" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Id" HeaderText="Id" />
                            <asp:BoundField DataField="Contratante" HeaderText="Contratante" />
                            <asp:BoundField DataField="Agente" HeaderText="Agente" />
                            <asp:BoundField DataField="Ramo" HeaderText="Ramo" />
                            <asp:BoundField DataField="Aseguradora" HeaderText="Aseguradora" />
                            <asp:BoundField DataField="Ejecutivo" HeaderText="Ejecutivo" />
                            <asp:BoundField DataField="EstatusPoliza" HeaderText="Estatus" />
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:HiddenField ID="SearchId" runat="server" Value="" />
            </div>

        </div>
    </div>
</asp:Content>

