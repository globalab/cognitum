﻿using CognisoWebApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class SecurityRole : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            object send = Request.QueryString["send"];
            if (send != null)
            {
                showMessage("Rol guardado correctamente", false);
            }
            if (!IsPostBack)
            {
                object roleId = Request.QueryString["id"];
                if (roleId != null)
                {
                    gridtitle.Visible = true;
                    gridDiv.Visible = true;
                    this.getRoleInfo(roleId.ToString());
                }
                else
                {
                    gridtitle.Visible = false;
                    gridDiv.Visible = false;
                }
            }
        }

        private void getRoleInfo(string _id)
        {
            string errMsg=string.Empty;
            Entity entityRole = new Entity("securityrole");
            entityRole.Attributes.Add(new Models.Attribute("Id"));
            entityRole.Attributes.Add(new Models.Attribute("rolename"));
            entityRole.useAuditable = false;
            entityRole.Keys.Add(new Key("Id", _id, "int"));

            List<Entity> results = SiteFunctions.GetValues(entityRole, out errMsg);
            if (results.Count > 0)
            {
                txtRoleName.Text = results[0].getAttrValueByName("rolename");

                this.getsecurityPriv(_id);
            }
        }

        private void getsecurityPriv(string _id)
        {
            string errMsg = string.Empty;

            Entity secPriv = new Entity("securityprivilege");
            secPriv.Attributes.Add(new Models.Attribute("entityid"));
            secPriv.Attributes.Add(new Models.Attribute("privilege"));
            secPriv.Keys.Add(new Key("roleid", _id, "int"));
            secPriv.useAuditable = false;

            Entity entityTable = new Entity("EntityTable");
            entityTable.Attributes.Add(new Models.Attribute("EntityName"));

            JoinEntity joinent = new JoinEntity();

            selectJoinEntity selJoin = new selectJoinEntity("entityid", 1, "entityid");
            joinent.JoinType = JoinType.Inner;
            joinent.selectJoinList.Add(selJoin);
            joinent.ChildEntity = entityTable;

            secPriv.ChildEntities.Add(joinent);


            List<Entity> results = SiteFunctions.GetValues(secPriv, out errMsg);
            DataTable dtsecPriv = SiteFunctions.trnsformEntityToDT(results);
            gvPermisos.DataSource = dtsecPriv;
            gvPermisos.DataBind();
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            this.saveRole();
        }

        private void saveRole()
        {
            string errMsg=string.Empty;
            Entity role = new Entity("securityrole");
            role.Attributes.Add(new Models.Attribute("rolename", txtRoleName.Text, "string"));
            RestSharp.Method methodSave = RestSharp.Method.POST;
            object roleId = Request.QueryString["id"];
            if (roleId != null)
            {
                methodSave = RestSharp.Method.PUT;
                role.Keys.Add(new Key("id", roleId.ToString(), "int"));
            }

            if (SiteFunctions.SaveEntity(role, methodSave, out errMsg))
            {

                if (methodSave == RestSharp.Method.PUT)
                {
                    errMsg = roleId.ToString();
                    this.savePriviledge(roleId.ToString());
                }
                Response.Redirect("SecurityRole?send=1&id=" + errMsg);
            }
            else
            {
                this.showMessage(errMsg, true);
            }
        }

        private void savePriviledge(string id)
        {
            string errMsg = string.Empty;
            for (int i = 0; i < gvPermisos.Rows.Count; i++)
            {
                GridViewRow select = gvPermisos.Rows[i];
                Label lblEntity = (Label)select.Cells[1].FindControl("entity");
                DropDownList privSec = (DropDownList)select.Cells[1].FindControl("securityPriv");

                Entity secPriv = new Entity("securityprivilege");
                secPriv.Attributes.Add(new Models.Attribute("privilege", privSec.SelectedValue, "int"));
                secPriv.Keys.Add(new Key("roleid", id, "int"));
                secPriv.Keys.Add(new Key("entityid", lblEntity.Text, "int"));

                if(!SiteFunctions.SaveEntity(secPriv, RestSharp.Method.PUT, out errMsg))
                {
                    this.showMessage(errMsg, true);
                }
            }
        }

        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();

                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
    }
}