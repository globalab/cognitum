﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BitacoraTramite.aspx.cs" Inherits="CognisoWebApp.BitacoraTramite" %>
<!DOCTYPE html>  
  
<html lang="en">  
<head>  
    <title>Bitácora Trámite</title>
    <link href="/Content/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="/Content/bootstrap.min.css" rel="stylesheet"/>
    <link href="/Content/cogniso.css" rel="stylesheet"/>
   <script src="/Scripts/jquery-3.2.1.min.js" type="text/javascript"></script> 
    <script src="/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
   <script lang="javascript" type="text/javascript">
       var submit = 0;
       function CheckIsRepeat() {
           if (++submit > 1) {
               return false;
           }
       }
    </script>

    <script type="text/javascript">

        function openpopup(url, title, btnname) {
            var wWidth = $(window).width();
            var dWidth = wWidth * 0.8;
            var wHeight = $(window).height();
            var dHeight = wHeight * 0.8;

            var $dialog = $('<div></div>')
            .html('<iframe id="images" style="border: 0px; " src="' + url + '" width="100%" height="100%"></iframe>')
            .dialog({
                autoOpen: false,
                modal: true,
                center: true,
                height: dHeight,
                width: dWidth,
                title: title,
                buttons: {
                    "Cerrar": function () { $dialog.dialog('close'); }
                },
                close: function (event, ui) {
                    $(btnname).click();
                }
            });
            $dialog.dialog('open');
        }

        $(function () {
            $('[id*=txtFecha]').datepicker({
                changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
                language: "tr"
            });
        });
    </script>
</head>  
<body>
    <form id="form1" runat="server">
        <div class="container">
        <div class="containerRt1" runat="server">
                 
            <div class="panel-heading">
                <div class="btn-group">
                    <asp:ImageButton ID="BtnSave" ToolTip="Guardar" ImageUrl="/Content/Imgs/save-file-option.png" runat="server" ValidationGroup="OnSave" OnClick="BtnSave_Click"  OnClientClick="return CheckIsRepeat();"/>
                </div>
            </div>

            <div class="containerRT2" runat="server" id="IncisoGral">
                <div id="errMess" >

                </div>
                <div class="accordion" id="accordiontable" runat="server">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link" id="General-tab" data-toggle="tab" href="#General" role="tab" aria-controls="General" aria-selected="false">General</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="General" role="tabpanel" aria-labelledby="General-tab">
                            <div class="containerRT3">
                                <div class="row">

                                    <div class="col">
                                        <asp:Label ID="lblUsuario" runat="server" Text="Usuario"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:DropDownList ID="txtUsuario" runat="server" class="form-control">
                                            </asp:DropDownList>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblTramite" runat="server" Text="Trámite"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtTramite" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblFecha" runat="server" Text="Fecha"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtFecha" runat="server" class="form-group input-group"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <asp:Label ID="lblObservaciones" runat="server" Text="Observaciónes"></asp:Label>
                                        <div class="form-group input-group">
                                            <asp:TextBox ID="txtObservaciones" runat="server" CssClass="form-control" TabIndex="3" TextMode="MultiLine" Rows="10">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <asp:HiddenField ID="EntityId" runat="server" />
                <asp:HiddenField ID="BitId" runat="server" />
                <asp:HiddenField ID="TipoBit" runat="server" />
            </div>
        </div>
    </div>
 </form>
</body>
</html>  
