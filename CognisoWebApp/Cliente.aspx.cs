﻿using CognisoWebApp.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CognisoWebApp
{
    public partial class Cliente : System.Web.UI.Page
    {
        const string ENTITYNAME = "Cliente";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            if (!IsPostBack)
            {
                initCatalog();
               
                object id = Request.QueryString["id"];
                CustId.Value = id.ToString();
                if (id != null)
                {
                    disablefields();
                    this.searchCustomer(id.ToString());
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "collapsediv", "collapsediv()", true);

            }
        }
        private void disablefields()
        {
            if (!SiteFunctions.IsUsuarioCobranza(Context.User.Identity.Name))
            {
                txtRFC.Enabled = false;
                txtRazonSocial.Enabled = false;
                txtName.Enabled = false;
                txtMiddleName.Enabled = false;
                txtLastName.Enabled = false;
                txtGroup.Enabled = false;
                txtGiro.Enabled = false;
            }
        }
        private void initCatalog()
        {
            List<Entity> listResult = new List<Entity>();
            string errmess = "";
            Entity Giro = new Entity("Giro");
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Giro.useAuditable = false;
            listResult = SiteFunctions.GetValues(Giro, out errmess);
            txtGiro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGiro.DataValueField = "Id";
            txtGiro.DataTextField = "Nombre";
            txtGiro.DataBind();

            Entity Grupo = new Entity("Grupo");
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Grupo.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Grupo.useAuditable = false;
            listResult = SiteFunctions.GetValues(Grupo, out errmess);
            txtGroup.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
            txtGroup.DataValueField = "Id";
            txtGroup.DataTextField = "Nombre";
            txtGroup.DataBind();

            Entity Estado = new Entity("Estado");
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
            Estado.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
            Estado.useAuditable = false;
            listResult = SiteFunctions.GetValues(Estado, out errmess);

            

            
        }

        protected void BtnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveCustomer();
        }

        private void searchCustomer(string _id)
        {
            List<Entity> entities = new List<Entity>();
            Entity cliente = new Entity(ENTITYNAME);
            cliente.useAuditable = true;
            cliente.Keys.Add(new Models.Key("id", _id, "int"));

            

            Entity PFisica = new Entity("PersonaFisica");
            PFisica.Keys.Add(new Models.Key("id", _id, "int"));

            Entity PMoral = new Entity("PersonaMoral");
            PMoral.Keys.Add(new Models.Key("id", _id, "int"));

            Entity ContactPM = new Entity("ContactoPersonaMoral");
            ContactPM.Keys.Add(new Models.Key("PersonaMoral", _id, "int"));

            Entity ContactS = new Entity("ContactoSimple");
            ContactS.Keys.Add(new Models.Key("cliente", _id, "int"));

            entities.Add(cliente);
            entities.Add(PFisica);
            entities.Add(PMoral);
            entities.Add(ContactPM);
            entities.Add(ContactS);

            List<OperationResult> result = SiteFunctions.execListSelect(entities);
            mapCustSearch(result, _id);
            loadDir(_id);
            loadCont(_id);
        }

        private void loadDir(string _id)
        {
            string errmess = string.Empty;
            Entity Dir = new Entity("Direccion");
            Dir.useAuditable = false;

            Dir.Keys.Add(new Models.Key("Cliente", _id, "int64"));

            List<Entity> retDir = SiteFunctions.GetValues(Dir, out errmess);
            DataTable dt = new DataTable();
            dt = SiteFunctions.trnsformEntityToDT(retDir);
            GVDirList.DataSource = dt;
            GVDirList.DataBind();
        }

        private void loadCont(string _id)
        {
            string errmess = string.Empty;
            Entity Cont = new Entity("ContactoPersonaMoral");
            Cont.useAuditable = false;

            Cont.Keys.Add(new Models.Key("PersonaMoral", _id, "int64"));

            List<Entity> retCont = SiteFunctions.GetValues(Cont, out errmess);
            DataTable dt = new DataTable();
            dt = SiteFunctions.trnsformEntityToDT(retCont);
            GVContacto.DataSource = dt;
            GVContacto.DataBind();
        }

        private void showPanels()
        {
            accordiontable.Attributes.CssStyle[HtmlTextWriterStyle.Visibility] = "visible";
            
            if (txtMoral.Checked)
            {
                PF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                CPF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                PM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                CPM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                telPF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                ContactPM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
            }
            else
            {
                PF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                CPF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                ////PFisica.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "10px";
                PM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                CPM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
                //PMoral.Attributes.CssStyle[HtmlTextWriterStyle.Height] = "0px";
                telPF.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "block";
                ContactPM.Attributes.CssStyle[HtmlTextWriterStyle.Display] = "none";
            }
        }
        private void mapCustSearch(List<OperationResult> result,string id)
        {
            string errMsg;
            foreach (OperationResult opResult in result)
            {
                if (opResult.RetVal != null)
                {
                    if (opResult.RetVal.Count > 0)
                    {
                        Entity entity = opResult.RetVal[0];
                        switch (entity.EntityName.ToLower())
                        {
                            case "cliente":
                                txtRFC.Text = entity.getAttrValueByName("RFC");
                                txtGiro.Text = entity.getAttrValueByName("Giro");
                                txtRazonSocial.Text = entity.getAttrValueByName("NombreCompleto");
                                txtGroup.SelectedValue = entity.getAttrValueByName("Grupo");
                                txtEstatusCliente.SelectedValue = entity.getAttrValueByName("EstatusCliente");
                                CustRFC.Value = entity.getAttrValueByName("Rfc");
                                txtId.Text = id;
                                DateTime fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaAdd"));
                                txtFechaAlta.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                fechaaudit = Convert.ToDateTime(entity.getAttrValueByName("FechaUMod"));
                                txtFechaUltimaMod.Text = fechaaudit.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss");
                                Entity UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioAdd"), "int64"));
                                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioAlta.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                UserAudit = new Entity("Usuario");
                                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                                UserAudit.Keys.Add(new Key("Id", entity.getAttrValueByName("UsuarioUMod"), "int64"));
                                AuditUsr = SiteFunctions.GetValues(UserAudit, out errMsg);
                                if (AuditUsr.Count > 0)
                                {
                                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                                }
                                Entity Auditfields = new Entity("CentroDeBeneficio");
                                Auditfields.Attributes.Add(new Models.Attribute("Nombre"));
                                Auditfields.Keys.Add(new Key("Id", entity.getAttrValueByName("CentroDeBeneficio"), "int64"));
                                List<Entity> Auditval = SiteFunctions.GetValues(Auditfields, out errMsg);
                                if (Auditval.Count > 0)
                                {
                                    txtCentroBeneficio.Text = Auditval[0].getAttrValueByName("Nombre");
                                }
                                Entity Ubicacion = new Entity("Ubicacion");
                                Ubicacion.Attributes.Add(new Models.Attribute("Nombre", "", "string"));
                                Ubicacion.Keys.Add(new Key("Id", entity.getAttrValueByName("Ubicacion"), "int"));
                                Auditval = SiteFunctions.GetValues(Ubicacion, out errMsg);


                                if (Auditval.Count > 0)
                                {
                                    txtUbicacion.Text = Auditval[0].getAttrValueByName("Nombre");
                                }
                                break;
                            case "personamoral":
                                txtRazonSocial.Text = entity.getAttrValueByName("RazonSocial");
                                txtMoral.Checked = true;
                                break;
                            case "personafisica":
                                txtMiddleName.Text = entity.getAttrValueByName("ApellidoMaterno");
                                txtLastName.Text = entity.getAttrValueByName("ApellidoPaterno");
                                txtCurp.Text = entity.getAttrValueByName("Curp");
                                txtEmail.Text = entity.getAttrValueByName("Email");
                                txtDate.Text = entity.getAttrValueByName("FechaNacimiento");
                                txtName.Text = entity.getAttrValueByName("Nombre");
                                loadTel(CustId.Value);
                                break;

                            case "contactosimple":
                                txtNombreCA.Text = entity.getAttrValueByName("NombreCompleto");
                                txtEmailCA.Text = entity.getAttrValueByName("Email");
                                break;
                        }
                    }
                }
            }
            showPanels();
        }

        private void SaveCustomer()
        {
            Auditable audi = new Auditable();
            string userId = SiteFunctions.getuserid(Context.User.Identity.Name, false);
            audi.userId = int.Parse(userId);
            audi.opDate = DateTime.Now;
            JoinEntity joinEntity = new JoinEntity();
            Entity Cliente = new Entity(ENTITYNAME);
            Cliente.Attributes.Add(new Models.Attribute("RFC", txtRFC.Text, "string"));
            Cliente.Attributes.Add(new Models.Attribute("NombreCompleto", txtRazonSocial.Text, "string"));
            Cliente.Attributes.Add(new Models.Attribute("Giro", txtGiro.Text, "int"));
            Cliente.Attributes.Add(new Models.Attribute("Grupo", txtGroup.SelectedValue, "int"));
            //Cliente.Attributes.Add(new Models.Attribute("IdSap", txtMoral.Checked ? "2" : "1", "int"));
            Cliente.Attributes.Add(new Models.Attribute("EstatusCliente", txtEstatusCliente.SelectedValue, "int"));

            
            //Validar tipo de persona
            if (!txtMoral.Checked)
            {
                Entity PFisica = new Entity("PersonaFisica");
                PFisica.Attributes.Add(new Models.Attribute("EstadoCivil", "0", "int"));
                PFisica.Attributes.Add(new Models.Attribute("Sexo", "0", "int"));
                PFisica.Attributes.Add(new Models.Attribute("ApellidoMaterno", txtMiddleName.Text, "string"));
                PFisica.Attributes.Add(new Models.Attribute("ApellidoPaterno", txtLastName.Text, "string"));
                PFisica.Attributes.Add(new Models.Attribute("Curp", txtCurp.Text, "string"));
                PFisica.Attributes.Add(new Models.Attribute("Email", txtEmail.Text, "string"));
                PFisica.Attributes.Add(new Models.Attribute("FechaNacimiento", txtDate.Text, "datetime"));
                PFisica.Attributes.Add(new Models.Attribute("Nombre", txtName.Text, "string"));

                joinEntity = new JoinEntity();
                joinEntity.ChildEntity = PFisica;
                joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                Cliente.ChildEntities.Add(joinEntity);
            }
            else
            {
                Entity PMoral = new Entity("PersonaMoral");
                PMoral.Attributes.Add(new Models.Attribute("RazonSocial", txtRazonSocial.Text, "string"));

                joinEntity = new JoinEntity();
                joinEntity.ChildEntity = PMoral;
                joinEntity.JoinKey = new Models.Attribute("id", "", "int");
                Cliente.ChildEntities.Add(joinEntity);

            }

            Entity contactoAlt = new Entity("ContactoSimple");
            contactoAlt.Attributes.Add(new Models.Attribute("NombreCompleto", txtNombreCA.Text, "string"));
            contactoAlt.Attributes.Add(new Models.Attribute("Email", txtEmailCA.Text, "string"));

            joinEntity = new JoinEntity();
            joinEntity.ChildEntity = contactoAlt;
            joinEntity.JoinKey = new Models.Attribute("cliente", "", "int");
            Cliente.ChildEntities.Add(joinEntity);

            string errmess = "";
            object id = Request.QueryString["id"];
            Method method = Method.POST;
            if (id != null)
            {
                method = Method.PUT;
                Cliente.Keys.Add(new Models.Key("Id", id.ToString(), "int"));
            }
            Cliente.useAuditable = true;
            Cliente.auditable = audi;
            if (SiteFunctions.SaveEntity(Cliente, method, out errmess))
            {

                Entity UserAudit = new Entity("Usuario");
                UserAudit.Attributes.Add(new Models.Attribute("Nombre"));
                UserAudit.Keys.Add(new Key("Id", audi.userId.ToString(), "int64"));
                List<Entity> AuditUsr = SiteFunctions.GetValues(UserAudit, out errmess);
                if (AuditUsr.Count > 0)
                {
                    txtUsuarioUltimaMod.Text = AuditUsr[0].getAttrValueByName("Nombre");
                }
                txtFechaUltimaMod.Text = audi.opDate.ToString("dd/MM/yyyy HH:mm:ss");
                this.showMessage("Cambios guardados correctamente.", false);
            }
            else
            {
                this.showMessage(errmess, true);
            }
        }
        private void showMessage(string errmes, bool error)
        {
            string popuptype = error == true ? "alert-danger" : "alert-success";
            string popup = "<div class='alert " + popuptype + " alert-dismissible fade show' role='alert'><strong>" + errmes + "</strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";

            // Define the name and type of the client scripts on the page.
            String csname1 = "PopupScript";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered(cstype, csname1))
            {
                StringBuilder cstext1 = new StringBuilder();
                cstext1.Append(@"<script type='text/javascript'> $('#errMess').html(""" + popup.Trim() + @""") </");
                cstext1.Append("script>");

                cs.RegisterStartupScript(cstype, csname1, cstext1.ToString());
            }
        }
        protected void btnValidate_Click1(object sender, EventArgs e)
        {

        }

        protected void BtnAttachment_Click(object sender, ImageClickEventArgs e)
        {
            var sURL = "Attachment.aspx?id=" + CustRFC.Value + "&entityName=Cliente" + "&parentId=0";

            string vtn = "window.open(" + "'" + sURL + "'" + ",'scrollbars=yes,resizable=yes','height=100', 'width=100')";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "popupattch", vtn, true);
        }

        protected void GVDirList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Direcciones";
            GVDirList.PageIndex = e.NewPageIndex;
            GVDirList.DataBind();
            string id = Request.QueryString["id"];
            loadDir(id);
        }

        protected void reloadDir_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Direcciones";
            string id = Request.QueryString["id"];
            loadDir(id);
        }

        protected void GVContacto_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Contacto";
            string id = Request.QueryString["id"];
            GVContacto.PageIndex = e.NewPageIndex;
            GVContacto.DataBind();
            loadCont(id);
        }

        protected void reloadCont_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Contacto";
            string id = Request.QueryString["id"];
            loadCont(id);
        }

        protected void reloadTel_Click(object sender, EventArgs e)
        {
            PaneName.Value = "Telefono";
            string id = Request.QueryString["id"];
            loadTel(id);
        }

        protected void GVTelList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PaneName.Value = "Telefono";
            GVTelList.PageIndex = e.NewPageIndex;
            GVTelList.DataBind();
            object _id = Request.QueryString["id"];
            if (_id != null)
            {
                loadTel(_id.ToString());
            }
        }

        protected void loadTel(string id)
        {
            string errmess = string.Empty;
            Entity Tel = new Entity("TelefonoPF");
            Tel.useAuditable = false;

            Tel.Keys.Add(new Models.Key("Contacto", id, "int64"));

            List<Entity> retTel = SiteFunctions.GetValues(Tel, out errmess);
            DataTable dt = new DataTable();
            dt = SiteFunctions.trnsformEntityToDT(retTel);
            GVTelList.DataSource = dt;
            GVTelList.DataBind();
        }

        protected void reloadGiro_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {

                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Giro = new Entity("Giro");
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Giro.useAuditable = false;
                listResult = SiteFunctions.GetValues(Giro, out errmess);
                txtGiro.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtGiro.DataValueField = "Id";
                txtGiro.DataTextField = "Nombre";
                txtGiro.DataBind();
                if (Session["retval"] != null)
                {
                    txtGiro.Text = Session["retval"].ToString();
                    Session["retval"] = "";
                }
            }
        }

        protected void reloadGrupo_Click(object sender, EventArgs e)
        {
            PaneName.Value = "General";
            if (Session["retval"] != "" && Session["retval"] != null)
            {

                List<Entity> listResult = new List<Entity>();
                string errmess = string.Empty;
                Entity Giro = new Entity("Grupo");
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Id"));
                Giro.Attributes.Add(new CognisoWebApp.Models.Attribute("Nombre"));
                Giro.useAuditable = false;
                listResult = SiteFunctions.GetValues(Giro, out errmess);
                txtGroup.DataSource = SiteFunctions.trnsformEntityToDT(listResult);
                txtGroup.DataValueField = "Id";
                txtGroup.DataTextField = "Nombre";
                txtGroup.DataBind();
                txtGroup.Text = Session["retval"].ToString();
                Session["retval"] = "";

            }
        }

        

        
    }
}